PROJECT(Taglut)
cmake_minimum_required(VERSION 2.8)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

SET(EXECUTABLE_OUTPUT_PATH bin/)
MAKE_DIRECTORY(${EXECUTABLE_OUTPUT_PATH})

SET(LIBRARY_OUTPUT_PATH lib/)
MAKE_DIRECTORY(${LIBRARY_OUTPUT_PATH})

SET(BINDING_OUTPUT_PATH src/binding/)
MAKE_DIRECTORY(${BINDING_OUTPUT_PATH})

SET(PYTHON_OUTPUT_PATH python/)
SET(SRC ${Taglut_SOURCE_DIR}/src/)
SET(Taglut_VERSION 0.0.2)
SET(Taglut_BINARY_DIR ${EXECUTABLE_OUTPUT_PATH})
SET(Taglut_LIBRARY_DIR ${LIBRARY_OUTPUT_PATH})
SET(Taglut_DOC_DIR doc/)
SET(Taglut_PYTHON_DIR python/)
SET(Taglut_BINDING_DIR ${SRC}/binding/)
SET(Taglut_CMAKE_DIR cmake/)


#--------------------------#
#define include directories
INCLUDE_DIRECTORIES(.
                    ${SRC}/utils ${SRC}/algos ${SRC}/utils/files ${SRC}/utils/meshes
                    ${SRC}/parameterization ${SRC}/parameterization/utils ${SRC}/parameterization/circlepacking
		    ${SRC}/topology ${SRC}/display ${SRC}/cimg_plugins ${SRC}/apps/medical
		    ${SRC}/parameterization/abf ${SRC}/include/opennl/extern ${SRC}/include/opennl/intern
		    ${SRC}/include/opennl/superlu ${SRC}/statistics ${SRC}/textures ${SRC}/maths
		    ${SRC}/core ${SRC}/geodesic ${SRC}/parameterization/barycentric ${SRC}/parameterization/cylinder
		    ${SRC}/include ${SRC}/tiling ${SRC}/include/gotools/include/)


SET(CMAKE_CXX_FLAGS_FULL "-ansi -pedantic")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wunused -Wno-long-long -Wno-deprecated -Wnon-virtual-dtor -Wcast-align -Wchar-subscripts -Wall -Wextra -Wpointer-arith -Wformat-security -Wunused-parameter -Woverloaded-virtual -Wno-shadow")
# adding a visibility directive to make opennl be an internal tool (removing blender conflicts)
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fvisibility=internal")

#look for libraries
FIND_PACKAGE(X11)
FIND_PACKAGE(Threads)
FIND_PACKAGE(PNG)
FIND_PACKAGE(JPEG)
FIND_PACKAGE(GLUT)
SET(Python_ADDITIONAL_VERSIONS 3.2)
FIND_PACKAGE(LibXml2)
FIND_PACKAGE(SWIG)
FIND_PACKAGE(PythonLibs)
INCLUDE(${Taglut_CMAKE_DIR}/FindPOPT.cmake)
INCLUDE(${Taglut_CMAKE_DIR}/FindCIMG.cmake)
INCLUDE(${Taglut_CMAKE_DIR}/FindOMP.cmake)
INCLUDE(${Taglut_CMAKE_DIR}/FindGSL.cmake)

IF(NOT CIMG_FOUND)
  MESSAGE(FATAL_ERROR "CImg is not available. Precompilation failed")
ENDIF(NOT CIMG_FOUND)


INCLUDE_DIRECTORIES(${CIMG_INCLUDE_DIR})

SET(LIBRARIES_X ${X11_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
SET(LIBRARIES_SOFTWARE ${POPT_LIBRARIES})


#add conditional include directories
IF(POPT_FOUND)
  INCLUDE_DIRECTORIES(${POPT_INCLUDE_DIR})
ELSE(POPT_FOUND)
  MESSAGE(FATAL_ERROR "libpopt is not available. Binaries will not be compiled")
ENDIF(POPT_FOUND)
IF(LIBXML2_LIBRARIES)
  INCLUDE_DIRECTORIES(${LIBXML2_INCLUDE_DIR})
ELSE(LIBXML2_LIBRARIES)
  MESSAGE(STATUS "libxml2 not found. The xml script interpreter will not be compiled")
ENDIF(LIBXML2_LIBRARIES)

IF(X11_FOUND AND CMAKE_HAVE_THREADS_LIBRARY)
  INCLUDE_DIRECTORIES(${X11_INCLUDE_DIR})
ELSE(X11_FOUND AND CMAKE_HAVE_THREADS_LIBRARY)
  MESSAGE(STATUS "X11 not found. No graphical include")
ENDIF(X11_FOUND AND CMAKE_HAVE_THREADS_LIBRARY)


#--------------------------#
# copy script
IF(LIBXML2_LIBRARIES)
  file(COPY ${SRC}/utils/ScriptSchema.xsd DESTINATION ${EXECUTABLE_OUTPUT_PATH}/)
  INSTALL(FILES ${SRC}/utils/ScriptSchema.xsd DESTINATION share/taglut/bin)
ENDIF(LIBXML2_LIBRARIES)


#--------------------------#
# Libraries


IF(CIMG_FOUND)
  SET(CPP_Taglut_LIST ${SRC}/utils/files/FileTools.cpp
  ${SRC}/core/Mesh.cpp ${SRC}/topology/MeshManipulator.cpp
  ${SRC}/topology/MeshPart.cpp ${SRC}/topology/MeshCut.cpp
  ${SRC}/parameterization/circlepacking/CPMethod.cpp ${SRC}/display/Display3D.cpp
  ${SRC}/topology/MeshPathes.cpp ${SRC}/topology/MeshPatches.cpp
  ${SRC}/display/DDisplay.cpp ${SRC}/display/DDScrollBar.cpp
  ${SRC}/display/DDBox.cpp ${SRC}/utils/MeshBExpr.cpp
  ${SRC}/display/DisplayGL.cpp ${SRC}/topology/MeshUncut.cpp
  ${SRC}/core/Triangle.cpp ${SRC}/algos/ClusterGenerator.cpp
  ${SRC}/core/Vertex.cpp
  ${SRC}/utils/SurfaceToolbox.cpp ${SRC}/utils/Messages.cpp
  ${SRC}/core/Length.cpp ${SRC}/utils/Exception.cpp
  ${SRC}/utils/files/FileExceptions.cpp ${SRC}/algos/VerticesManipulator.cpp
  ${SRC}/utils/ColorChooser.cpp ${SRC}/topology/MeshList.cpp
  ${SRC}/topology/SpaceSpliter.cpp ${SRC}/topology/PlaneSpliter.cpp
  ${SRC}/utils/XMLSimpleNode.cpp
  ${SRC}/algos/MeshPrune.cpp ${SRC}/utils/IndentManager.cpp
  ${SRC}/apps/medical/Electrodes.cpp ${SRC}/topology/MeshRectPatcher.cpp
  ${SRC}/topology/Polygon.cpp ${SRC}/parameterization/Mapping2D3DList.cpp
  ${SRC}/parameterization/Mapping2D3D.cpp ${SRC}/parameterization/abf/ABFMethod.cpp
  ${SRC}/utils/PointSelector.cpp ${SRC}/utils/StringManipulation.cpp
  ${SRC}/topology/PointModifier.cpp ${SRC}/parameterization/MappingRegistration.cpp
  ${SRC}/topology/MeshMatcher.cpp ${SRC}/parameterization/MappingComparator.cpp
  ${SRC}/topology/MeshComparator.cpp ${SRC}/parameterization/MultiscaleUnfolding.cpp
  ${SRC}/parameterization/UnfoldingMethod.cpp ${SRC}/algos/MeshSmoother.cpp
  ${SRC}/utils/MeshVoxelizer.cpp
  ${SRC}/core/Node.cpp ${SRC}/core/NLoop.cpp ${SRC}/algos/WaveFront.cpp
  ${SRC}/algos/Graph.cpp ${SRC}/topology/TGraph.cpp ${SRC}/utils/files/FileManipulator.cpp
  ${SRC}/algos/FlagLogger.cpp ${SRC}/algos/BorderSelector.cpp
  ${SRC}/textures/MeshCPCNaive.cpp ${SRC}/statistics/RandomGenerator.cpp
  ${SRC}/statistics/RandomVertexSelector.cpp ${SRC}/algos/MeshSegmentation.cpp
  ${SRC}/algos/SimulatedAnnealingNLoop.cpp ${SRC}/core/PointCloud.cpp
  ${SRC}/algos/PointCloudShapeEstimator.cpp ${SRC}/maths/Matrix.cpp
  ${SRC}/topology/PointCloudOctree.cpp ${SRC}/topology/MeshTubularDetector.cpp
  ${SRC}/utils/VectorField.cpp ${SRC}/geodesic/PLPath.cpp ${SRC}/core/NLoopFactory.cpp
  ${SRC}/geodesic/GeodesicDistance.cpp ${SRC}/core/PreNLoop.cpp ${SRC}/algos/WeightedEdges.cpp
  ${SRC}/parameterization/cylinder/CylinderParameterization.cpp
  ${SRC}/parameterization/cylinder/CylinderProjection.cpp
  ${SRC}/parameterization/barycentric/BarycentricMapping.cpp
  ${SRC}/topology/MeshPLCut.cpp ${SRC}/tiling/MTiling.cpp ${SRC}/core/Path3D.cpp
  ${SRC}/reconstruction/MeshBySlices.cpp ${SRC}/core/Path2D.cpp
  ${SRC}/reconstruction/PlanarPath.cpp ${SRC}/reconstruction/Layer.cpp
  ${SRC}/reconstruction/SceneDescriptor.cpp ${SRC}/reconstruction/MergeDescriptor.cpp
  ${SRC}/reconstruction/MergeStep.cpp ${SRC}/utils/IDTranslator.cpp
  ${SRC}/core/PLY.cpp)

  # for QtCreator compatibility
  SET(HEADERS_Taglut_LIB ${SRC}/tiling/MTiling.h
${SRC}/maths/Matrix.h
${SRC}/maths/Coord2D.h
${SRC}/maths/Coord3D.h
${SRC}/display/Display3D.h
${SRC}/display/DDisplay.h
${SRC}/display/DisplayGL.h
${SRC}/display/DDBox.h
${SRC}/display/DDScrollBar.h
${SRC}/reconstruction/MergeDescriptor.h
${SRC}/reconstruction/PlanarPath.h
${SRC}/reconstruction/SceneDescriptor.h
${SRC}/reconstruction/Layer.h
${SRC}/reconstruction/MeshBySlices.h
${SRC}/reconstruction/MergeStep.h
${SRC}/utils/SurfaceToolbox.h
${SRC}/utils/StringManipulation.h
${SRC}/utils/PointSelector.h
${SRC}/utils/IndentManager.h
${SRC}/utils/MeshBExpr.h
${SRC}/utils/IDTranslator.h
${SRC}/utils/Messages.h
${SRC}/utils/files/FileExceptions.h
${SRC}/utils/files/FileTools.h
${SRC}/utils/files/FileManipulator.h
${SRC}/utils/MeshVoxelizer.h
${SRC}/utils/XMLSimpleNode.h
${SRC}/utils/ColorChooser.h
${SRC}/utils/VectorField.h
${SRC}/utils/Exception.h
${SRC}/utils/ScriptInterpreter.h
${SRC}/utils/MeshMap.h
${SRC}/utils/CImgUse.h
${SRC}/apps/medical/Electrodes.h
${SRC}/core/Length.h
${SRC}/core/PointCloud.h
${SRC}/core/IDTypes.h
${SRC}/core/Path3D.h
${SRC}/core/NLoop.h
${SRC}/core/Point3D.h
${SRC}/core/Vertex.h
${SRC}/core/Path2D.h
${SRC}/core/Triangle.h
${SRC}/core/Mesh.h
${SRC}/core/Point2D.h
${SRC}/core/PreNLoop.h
${SRC}/core/Node.h
${SRC}/core/PLY.h
${SRC}/core/NLoopFactory.h
${SRC}/geodesic/PLPath.h
${SRC}/geodesic/GeodesicDistance.h
${SRC}/geodesic/PointOnEdge.h
${SRC}/algos/MeshPrune.h
${SRC}/algos/MeshSmoother.h
${SRC}/algos/ClusterGenerator.h
${SRC}/algos/MeshSegmentation.h
${SRC}/algos/SimulatedAnnealingNLoop.h
${SRC}/algos/VerticesManipulator.h
${SRC}/algos/WeightedEdges.h
${SRC}/algos/EdgeManipulator.h
${SRC}/algos/Graph.h
${SRC}/algos/PointCloudShapeEstimator.h
${SRC}/algos/FlagLogger.h
${SRC}/algos/BorderSelector.h
${SRC}/algos/WaveFront.h
${SRC}/cimg_plugins/io_fsf.h
${SRC}/topology/MeshUncut.h
${SRC}/topology/MeshManipulator.h
${SRC}/topology/PointCloudOctree.h
${SRC}/topology/PlaneSpliter.h
${SRC}/topology/MeshPLCut.h
${SRC}/topology/TGraph.h
${SRC}/topology/MeshRectPatcher.h
${SRC}/topology/SpaceSpliter.h
${SRC}/topology/MeshList.h
${SRC}/topology/MeshPatches.h
${SRC}/topology/Polygon.h
${SRC}/topology/MeshComparator.h
${SRC}/topology/PointModifier.h
${SRC}/topology/MeshCut.h
${SRC}/topology/MeshPathes.h
${SRC}/topology/MeshTubularDetector.h
${SRC}/topology/MeshPart.h
${SRC}/topology/MeshMatcher.h
${SRC}/textures/MeshCPCNaive.h
${SRC}/parameterization/UnfoldingMethod.h
${SRC}/parameterization/abf/ABFMethod.h
${SRC}/parameterization/utils/Point2D3D.h
${SRC}/parameterization/Mapping2D3DList.h
${SRC}/parameterization/Mapping2D3D.h
${SRC}/parameterization/circlepacking/CPMethod.h
${SRC}/parameterization/MappingComparator.h
${SRC}/parameterization/cylinder/CylinderParameterization.h
${SRC}/parameterization/cylinder/CylinderProjection.h
${SRC}/parameterization/MappingRegistration.h
${SRC}/parameterization/MultiscaleUnfolding.h
${SRC}/parameterization/barycentric/FloaterMethod.h
${SRC}/parameterization/barycentric/BarycentricMapping.h
${SRC}/statistics/RandomGenerator.h
${SRC}/statistics/RandomVertexSelector.h)

  SET(CMAKE_SKIP_RPATH ON)

  SET(LIBRARIES_SOFTWARE ${LIBRARIES_SOFTWARE} Taglut)

  # adding OpenNLfiles
  FILE(GLOB CPP_OpenNL_LIST ${SRC}/include/opennl/intern/*.c ${SRC}/include/opennl/superlu/*.c ${SRC}/include/OpenNLSolver.cpp)
  SET(CPP_Taglut_LIST ${CPP_Taglut_LIST} ${CPP_OpenNL_LIST})
  ADD_LIBRARY(Taglut SHARED ${CPP_Taglut_LIST} ${HEADERS_Taglut_LIB})

  # compile the GoTools library
  ADD_SUBDIRECTORY(src/include/gotools/)
  TARGET_LINK_LIBRARIES(Taglut GoToolsCore)


  # Installation
  INSTALL(TARGETS Taglut LIBRARY DESTINATION lib)
ENDIF(CIMG_FOUND)



#--------------------------#
# Flags
IF(LIBXML2_LIBRARIES)
  SET(Taglut_flags "${Taglut_flags} -DUSE_LIBXML")
  SET(python_binding_flags "${python_binding_flags} -DUSE_LIBXML")
ENDIF(LIBXML2_LIBRARIES)

IF(GSL_FOUND)
  SET(Taglut_flags "${Taglut_flags} -DUSE_GSL")
  SET(python_binding_flags "${python_binding_flags} -DUSE_GSL")
  SET(LIBRARIES_SOFTWARE ${LIBRARIES_SOFTWARE} ${GSL_LIBRARIES} ${CMAKE_GSL_CXX_FLAGS})
ENDIF(GSL_FOUND)

IF(GLUT_FOUND)
  SET(Taglut_flags "${Taglut_flags} -DUSE_GL")
  SET(python_binding_flags "${python_binding_flags} -DUSE_GL")
  SET(LIBRARIES_SOFTWARE ${LIBRARIES_SOFTWARE} ${GLUT_LIBRARIES})
ENDIF(GLUT_FOUND)

IF(X11_FOUND AND CMAKE_HAVE_THREADS_LIBRARY)
  SET(Taglut_flags "${Taglut_flags} -DUSE_X")
  SET(python_binding_flags "${python_binding_flags} -DUSE_X")
  SET(LIBRARIES_CIMG ${LIBRARIES_CIMG} ${LIBRARIES_X})
  SET(LIBRARIES_SOFTWARE ${LIBRARIES_SOFTWARE} ${LIBRARIES_CIMG})
ENDIF(X11_FOUND AND CMAKE_HAVE_THREADS_LIBRARY)

IF(LIBXML2_LIBRARIES)
  SET(LIBRARIES_SOFTWARE ${LIBRARIES_SOFTWARE} ${LIBXML2_LIBRARIES})
ENDIF(LIBXML2_LIBRARIES)

IF(OPENMP_FLAG_FOUND)
  SET(Taglut_flags "${Taglut_flags} ${OPENMP_FLAG} -DUSE_OMP")
  SET(python_binding_flags "${python_binding_flags} ${OPENMP_FLAG} -DUSE_OMP")
  SET(LIBRARIES_SOFTWARE ${LIBRARIES_SOFTWARE} ${OPENMP_FLAG} "-DUSE_OMP")
ENDIF(OPENMP_FLAG_FOUND)

SET_TARGET_PROPERTIES(Taglut PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")



#--------------------------#
# Softwares
IF(POPT_FOUND AND CIMG_FOUND)
  TARGET_LINK_LIBRARIES(Taglut ${LIBRARIES_SOFTWARE})

  # meshViewer
  ADD_EXECUTABLE(meshViewer ${SRC}/meshViewer.cpp)
  SET_TARGET_PROPERTIES(meshViewer PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshViewer ${LIBRARIES_CIMG} Taglut)

  # maskViewer
  ADD_EXECUTABLE(maskViewer ${SRC}/maskViewer.cpp)
  SET_TARGET_PROPERTIES(maskViewer PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(maskViewer ${LIBRARIES_CIMG} Taglut)

  # imageBinarize
  ADD_EXECUTABLE(imageBinarize ${SRC}/imageBinarize.cpp)
  SET_TARGET_PROPERTIES(imageBinarize PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(imageBinarize ${LIBRARIES_CIMG} Taglut)

  # imageAddMargin
  ADD_EXECUTABLE(imageAddMargin ${SRC}/imageAddMargin.cpp)
  SET_TARGET_PROPERTIES(imageAddMargin PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(imageAddMargin ${LIBRARIES_CIMG} Taglut)

  # maskSaver
  ADD_EXECUTABLE(maskSaver ${SRC}/maskSaver.cpp)
  SET_TARGET_PROPERTIES(maskSaver PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(maskSaver ${LIBRARIES_CIMG} Taglut)

  # meshCropDisc
  ADD_EXECUTABLE(meshCropDisc ${SRC}/meshCropDisc.cpp)
  SET_TARGET_PROPERTIES(meshCropDisc PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshCropDisc ${LIBRARIES_CIMG} Taglut)

  # distortImage
  ADD_EXECUTABLE(distortImage ${SRC}/distortImage.cpp)
  SET_TARGET_PROPERTIES(distortImage PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(distortImage ${LIBRARIES_CIMG} Taglut)

  # circleSetConverter
  ADD_EXECUTABLE(circleSetConverter ${SRC}/circleSetConverter.cpp)
  SET_TARGET_PROPERTIES(circleSetConverter PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(circleSetConverter ${LIBRARIES_CIMG} Taglut)

  # meshUnfolder
  ADD_EXECUTABLE(meshUnfolder ${SRC}/meshUnfolder.cpp)
  SET_TARGET_PROPERTIES(meshUnfolder PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshUnfolder ${LIBRARIES_CIMG} Taglut)

  # meshTriangulator
  ADD_EXECUTABLE(meshTriangulator ${SRC}/meshTriangulator.cpp)
  SET_TARGET_PROPERTIES(meshTriangulator PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshTriangulator ${LIBRARIES_CIMG} Taglut)

  # circleSetViewer
  ADD_EXECUTABLE(circleSetViewer ${SRC}/circleSetViewer.cpp)
  SET_TARGET_PROPERTIES(circleSetViewer PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(circleSetViewer ${LIBRARIES_CIMG} Taglut)

  # mappingViewer
  ADD_EXECUTABLE(mappingViewer ${SRC}/mappingViewer.cpp)
  SET_TARGET_PROPERTIES(mappingViewer PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(mappingViewer ${LIBRARIES_CIMG} Taglut)

  # meshInfos
  ADD_EXECUTABLE(meshInfos ${SRC}/meshInfos.cpp)
  TARGET_LINK_LIBRARIES(meshInfos ${LIBRARIES_CIMG} Taglut)

  # meshSelector
  ADD_EXECUTABLE(meshSelector ${SRC}/meshSelector.cpp)
  SET_TARGET_PROPERTIES(meshSelector PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshSelector ${LIBRARIES_CIMG} Taglut)

  # meshCutter
  ADD_EXECUTABLE(meshCutter ${SRC}/meshCutter.cpp)
  SET_TARGET_PROPERTIES(meshCutter PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshCutter ${LIBRARIES_CIMG} Taglut)

  # meshPatcher
  ADD_EXECUTABLE(meshPatcher ${SRC}/meshPatcher.cpp)
  SET_TARGET_PROPERTIES(meshPatcher PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshPatcher ${LIBRARIES_CIMG} Taglut)

  # imageViewer
  ADD_EXECUTABLE(imageViewer ${SRC}/imageViewer.cpp)
  SET_TARGET_PROPERTIES(imageViewer PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(imageViewer ${LIBRARIES_CIMG} Taglut)

  # computeBarycenter
  ADD_EXECUTABLE(computeBarycenter ${SRC}/computeBarycenter.cpp)
  SET_TARGET_PROPERTIES(computeBarycenter PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(computeBarycenter ${LIBRARIES_CIMG} Taglut)

  # meshPruner
  ADD_EXECUTABLE(meshPruner ${SRC}/meshPruner.cpp)
  SET_TARGET_PROPERTIES(meshPruner PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshPruner ${LIBRARIES_CIMG} Taglut)

  # meshSimplifier
  ADD_EXECUTABLE(meshSimplifier ${SRC}/meshSimplifier.cpp)
  SET_TARGET_PROPERTIES(meshSimplifier PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshSimplifier ${LIBRARIES_CIMG} Taglut)

  # meshSmoother
  ADD_EXECUTABLE(meshSmoother ${SRC}/meshSmoother.cpp)
  SET_TARGET_PROPERTIES(meshSmoother PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshSmoother ${LIBRARIES_CIMG} Taglut)

  # meshNoiser
  ADD_EXECUTABLE(meshNoiser ${SRC}/meshNoiser.cpp)
  SET_TARGET_PROPERTIES(meshNoiser PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshNoiser ${LIBRARIES_CIMG} Taglut)

  # textureGenerator
  ADD_EXECUTABLE(textureGenerator ${SRC}/textureGenerator.cpp)
  SET_TARGET_PROPERTIES(textureGenerator PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(textureGenerator ${LIBRARIES_CIMG} Taglut)

  # textureGenerator2D
  ADD_EXECUTABLE(textureGenerator2D ${SRC}/textureGenerator2D.cpp)
  SET_TARGET_PROPERTIES(textureGenerator2D PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(textureGenerator2D ${LIBRARIES_CIMG} Taglut)

  # adjustSegmentation
  ADD_EXECUTABLE(adjustSegmentation ${SRC}/adjustSegmentation.cpp)
  SET_TARGET_PROPERTIES(adjustSegmentation PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(adjustSegmentation ${LIBRARIES_CIMG} Taglut)

  # brainCorrection
  ADD_EXECUTABLE(brainCorrection ${SRC}/brainCorrection.cpp)
  SET_TARGET_PROPERTIES(brainCorrection PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(brainCorrection ${LIBRARIES_CIMG} Taglut)

  # meshNLoopComputation
  ADD_EXECUTABLE(meshNLoopComputation ${SRC}/meshNLoopComputation.cpp)
  SET_TARGET_PROPERTIES(meshNLoopComputation PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshNLoopComputation ${LIBRARIES_CIMG} Taglut)

  # meshGlobalCurvature
  ADD_EXECUTABLE(meshGlobalCurvature ${SRC}/meshGlobalCurvature.cpp)
  SET_TARGET_PROPERTIES(meshGlobalCurvature PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshGlobalCurvature ${LIBRARIES_CIMG} Taglut)

  # pointCloudNormalEstimator
  ADD_EXECUTABLE(pointCloudNormalEstimator ${SRC}/pointCloudNormalEstimator.cpp)
  SET_TARGET_PROPERTIES(pointCloudNormalEstimator PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(pointCloudNormalEstimator ${LIBRARIES_CIMG} Taglut)

  # pointCloudDistortionEstimator
  ADD_EXECUTABLE(pointCloudDistortionEstimator ${SRC}/pointCloudDistortionEstimator.cpp)
  SET_TARGET_PROPERTIES(pointCloudDistortionEstimator PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(pointCloudDistortionEstimator ${LIBRARIES_CIMG} Taglut)

  # pointCloudBoundaryEstimator
  ADD_EXECUTABLE(pointCloudBoundaryEstimator ${SRC}/pointCloudBoundaryEstimator.cpp)
  SET_TARGET_PROPERTIES(pointCloudBoundaryEstimator PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(pointCloudBoundaryEstimator ${LIBRARIES_CIMG} Taglut)

  # pointCloudViewer
  ADD_EXECUTABLE(pointCloudViewer ${SRC}/pointCloudViewer.cpp)
  SET_TARGET_PROPERTIES(pointCloudViewer PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(pointCloudViewer ${LIBRARIES_CIMG} Taglut)

  # meshBuildEigenProblemLB
  ADD_EXECUTABLE(meshBuildEigenProblemLB ${SRC}/meshBuildEigenProblemLB.cpp)
  SET_TARGET_PROPERTIES(meshBuildEigenProblemLB PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(meshBuildEigenProblemLB ${LIBRARIES_CIMG} Taglut)

  # check_mc
  ADD_EXECUTABLE(check_mc ${SRC}/unittesting/check_mc.cpp)
  SET_TARGET_PROPERTIES(check_mc PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(check_mc ${LIBRARIES_CIMG} Taglut)

  # modulo_check
  ADD_EXECUTABLE(modulo_check ${SRC}/unittesting/modulo_check.cpp)
  SET_TARGET_PROPERTIES(modulo_check PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(modulo_check ${LIBRARIES_CIMG} Taglut)

  # computeLocalEdgeSize
  ADD_EXECUTABLE(computeLocalEdgeSize ${SRC}/computeLocalEdgeSize.cpp)
  SET_TARGET_PROPERTIES(computeLocalEdgeSize PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(computeLocalEdgeSize ${LIBRARIES_CIMG} Taglut)

  # compareMeshes
  ADD_EXECUTABLE(compareMeshes ${SRC}/compareMeshes.cpp)
  SET_TARGET_PROPERTIES(compareMeshes PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(compareMeshes ${LIBRARIES_CIMG} Taglut)

  # mesh2Image
  ADD_EXECUTABLE(mesh2Image ${SRC}/mesh2Image.cpp)
  SET_TARGET_PROPERTIES(mesh2Image PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(mesh2Image ${LIBRARIES_CIMG} Taglut)

  # example
  ADD_EXECUTABLE(example ${SRC}/example.cpp)
  SET_TARGET_PROPERTIES(example PROPERTIES COMPILE_FLAGS "${SHARED_FLAGS} ${Taglut_flags}")
  TARGET_LINK_LIBRARIES(example ${LIBRARIES_CIMG} Taglut)

  IF(LIBXML2_LIBRARIES)
    # mScript
    ADD_EXECUTABLE(mScript ${SRC}/mScript.cpp ${SRC}/utils/ScriptInterpreter.cpp)
    TARGET_LINK_LIBRARIES(mScript ${LIBRARIES_CIMG} ${LIBRARIES_SOFTWARE})
    INSTALL(PROGRAMS ${Taglut_BINARY_DIR}/mScript DESTINATION share/taglut/bin)
  ENDIF(LIBXML2_LIBRARIES)

  # Installation
  INSTALL(PROGRAMS ${Taglut_BINARY_DIR}/meshViewer
                ${Taglut_BINARY_DIR}/maskViewer ${Taglut_BINARY_DIR}/maskSaver
		${Taglut_BINARY_DIR}/meshCropDisc ${Taglut_BINARY_DIR}/distortImage
		${Taglut_BINARY_DIR}/circleSetConverter ${Taglut_BINARY_DIR}/meshUnfolder
		${Taglut_BINARY_DIR}/meshTriangulator ${Taglut_BINARY_DIR}/circleSetViewer
		${Taglut_BINARY_DIR}/mappingViewer ${Taglut_BINARY_DIR}/meshInfos
		${Taglut_BINARY_DIR}/meshSelector ${Taglut_BINARY_DIR}/meshCutter
		${Taglut_BINARY_DIR}/meshPatcher ${Taglut_BINARY_DIR}/imageViewer
		${Taglut_BINARY_DIR}/computeBarycenter ${Taglut_BINARY_DIR}/meshPruner
		${Taglut_BINARY_DIR}/meshSimplifier
		${Taglut_BINARY_DIR}/meshSmoother ${Taglut_BINARY_DIR}/meshNoiser
		DESTINATION share/taglut/bin)
ENDIF(POPT_FOUND AND CIMG_FOUND)


#--------------------------#
# Python binding
IF(CIMG_FOUND AND SWIG_DIR)
  SET(PYTHON_TAGLUT_FILES cylinderCutter.py
    meshNLoopComputation.py
    extractBorder.py meshPatcher.py
    fastMarchingViewer.py meshPLCutter.py
    hausdorffDistanceMesh.py meshSmoother.py
    mappingConverter.py meshStats.py
    meshCleaner.py meshUncut.py
    meshConverter.py meshUnfolder.py
    meshCreateSlices.py meshViewer.py
    meshVoxelizer.py meshCutter.py
    normalComparison.py
    pre-n-loops.py
    meshCutUsingNLoops.py scalarFunctionConverter.py
    meshDistanceMatrix.py
    meshReconstruction.py
    meshGetRegionOfInterest.py unitTestingFields.py
    unitTestingPath2D.py unitTestingReconstruction.py
    meshGetPathsBetweenBorderPoints.py meshGetScalarFunctionFromCylinders.py
    scalarFunctionInfos.py meshVeinsPipeline.py
    )

  # python binding

  SET(Taglut_LIBRARY_DIR ${PYTHON_OUTPUT_PATH})
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${python_binding_flags}")
  SET(CMAKE_SWIG_OUTDIR ${PYTHON_OUTPUT_PATH})

  SET(out_files_python)
  FOREACH(ptf ${PYTHON_TAGLUT_FILES})
    SET(py_dst ${PYTHON_OUTPUT_PATH}/${ptf})
    ADD_CUSTOM_COMMAND(OUTPUT ${py_dst}
      DEPENDS ${Taglut_SOURCE_DIR}/python/${ptf}
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Taglut_SOURCE_DIR}/python/${ptf} ${py_dst})
    LIST(APPEND in_files_python ${Taglut_SOURCE_DIR}/python/${ptf})
    LIST(APPEND out_files_python ${py_dst})
  ENDFOREACH(ptf)

  INCLUDE(${SWIG_USE_FILE})

  FIND_PACKAGE(PythonLibs)
  INCLUDE_DIRECTORIES(${PYTHON_INCLUDE_PATH})
  SET(CMAKE_SWIG_FLAGS "")

  MAKE_DIRECTORY(${Taglut_BINDING_DIR})

  SET_SOURCE_FILES_PROPERTIES(${SRC}/binding/taglut.i PROPERTIES CPLUSPLUS ON)
  SET_SOURCE_FILES_PROPERTIES(${SRC}/binding/taglut.i PROPERTIES SWIG_FLAGS "")
  SWIG_ADD_MODULE(taglut python ${SRC}/binding/taglut.i)
  ADD_CUSTOM_TARGET(PYTHON_FILES ALL DEPENDS ${out_files_python} ${SWIG_MODULE_taglut_REAL_NAME} SOURCES ${in_files_python})

  SWIG_LINK_LIBRARIES(taglut ${PYTHON_LIBRARIES} Taglut ${LIBRARIES_CIMG})

  # Installation
  STRING(REGEX MATCH "python[0-9].[0-9]" PYTHON_DIR ${PYTHON_LIBRARIES})
  INSTALL(TARGETS ${SWIG_MODULE_taglut_REAL_NAME} LIBRARY DESTINATION lib/${PYTHON_DIR}/site-packages/)
  INSTALL(FILES  ${PYTHON_OUTPUT_PATH}/taglut.py DESTINATION lib/${PYTHON_DIR}/site-packages/)
  FILE(GLOB python_scripts "${PYTHON_OUTPUT_PATH}*.py")
  INSTALL(FILES ${python_scripts} DESTINATION share/taglut/python)
ELSE(CIMG_FOUND AND SWIG_DIR)
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_FULL}")
ENDIF(CIMG_FOUND AND SWIG_DIR)


#--------------------------#
# Documentation
FIND_PACKAGE(Doxygen)
IF(DOXYGEN)
  SET(HTML_DIR ${Taglut_DOC_DIR}/html)

  MAKE_DIRECTORY(${HTML_DIR})
  SET(Taglut_DOCUMENTATION_INPUT_COMMON ${SRC})

  CONFIGURE_FILE(${SRC}/doxygen.config.in ${Taglut_BINARY_DIR}/doxygen.config @ONLY IMMEDIATE)

  ADD_CUSTOM_TARGET(doc COMMAND ${DOXYGEN_EXECUTABLE} ${Taglut_BINARY_DIR}/doxygen.config SOURCES ${Taglut_BINARY_DIR}/doxygen.config)
ENDIF(DOXYGEN)



#-------------------------#
# Edit
ADD_CUSTOM_TARGET(edit emacs src/binding/*.i `find python/ -name "[!u]*.py"` `find . -name \"*.h\"` `find . -name \"*.cpp\"` ${CIMG_INCLUDE_DIR}/CImg.h&)
