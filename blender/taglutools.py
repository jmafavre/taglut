# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Taglutools",
    "author": "Jean-Marie Favreau",
    "version": (0,1),
    "blender": (2, 5, 8),
    "api": 37702,
    "location": "View3D > Toolbar and View3D > Specials",
    "description": "Taglut tools",
    "warning": "",
    "category": "Add Mesh"}

"""
This script is a frontend to taglut.
"""

import bpy
import mathutils
import tempfile


from bpy.props import *
from bpy_extras import *

import sys
import traceback
import math
import re
from math import *
import sys
import random
import os.path

import taglut

def get_color_name(color):
    color256=[int(v * 256) for v in color]
    return 'TM({:x},{:x},{:x})'.format(color256[0], color256[1], color256[2])


def get_vcol_material():
    name = "TM(vcol)"
    materials = bpy.data.materials
    if name in materials:
        return materials[name]
    else:
        mat = bpy.data.materials.new(name=name)
        mat.use_vertex_color_paint = True
        return mat

def get_uv_material(mesh):
    name = "TM(uv)"
    materials = bpy.data.materials
    if name in materials:
        return materials[name]
    else:
        tex = bpy.data.textures.new('TT(testgrid)', type = 'IMAGE')
        name_img = "TI(testgrid)"
        bpy.ops.image.new(name=name_img, width=1024, height=1024, uv_test_grid=True)
        tex.image = bpy.data.images[name_img]

        mat = bpy.data.materials.new(name=name)
        mtex = mat.texture_slots.add()
        mtex.texture = tex
        mtex.texture_coords = 'UV'
        mtex.use_map_color_diffuse = True

        mat.use_face_texture = True

        return mat

def get_material_from_color(color, emit = False):
    materials = bpy.data.materials
    name = get_color_name(color)
    if emit:
        name = name + "_emit"

    if name in materials:
        return materials[name]
    else:
        mat = bpy.data.materials.new(name=name)
        mat.diffuse_color = color
        mat.diffuse_intensity = .98
        if emit:
            mat.emit = .4
        return mat

def updateFromTaglut(bmesh, tmesh):
    vertices = []
    for c in tmesh.getPoints():
        vertices.append([c.getX(), c.getY(), c.getZ()])
    triangles = []
    for c in tmesh.getTriangles():
        triangles.append([c.getP1(), c.getP2(), c.getP3()])

    bpy.ops.object.mode_set(mode='EDIT')

    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.delete(type='ALL')
    bpy.ops.object.mode_set(mode='OBJECT')

    bmesh.from_pydata(vertices, [], triangles)
    bmesh.update()

def set_uv(mesh, object, mapping, texture=True):
    mapping.resize2DCoords(0, 0, 1., 1.)

    if texture:
        for i in range(len(object.material_slots)):
            object.active_material_index = i - 1
            bpy.ops.object.material_slot_remove()
        object.data.materials.append(get_uv_material(mesh))

    uvlay = mesh.uv_textures.new()
    mesh.uv_textures.active = uvlay
    for i, f in enumerate(uvlay.data):
        f.image = bpy.data.images["TI(testgrid)"]
        f.use_image = True
        for j, uv in enumerate(f.uv):
            uv[0] = mapping[mesh.faces[i].vertices[j]].get2DX()
            uv[1] = mapping[mesh.faces[i].vertices[j]].get2DY()



def toTaglut(bmesh):
    result = taglut.Mesh()
    vertices = []
    idConv = {}
    idConvFaces = {}
    selected = []
    for c in bmesh.vertices:
        if c.select:
            selected.append(len(vertices))
        idConv[c.index] = len(vertices)
        vertices.append(taglut.Coord3D(c.co.x, c.co.y, c.co.z))
    triangles = []
    for c in bmesh.faces:
        if len(c.vertices) == 3:
            idConvFaces[c.index] = len(triangles)
            triangles.append(taglut.Triangle(idConv[c.vertices[0]], idConv[c.vertices[1]], idConv[c.vertices[2]]))
    result.buildMesh(vertices, triangles)
    if result.getNbBadBPoints() != 0:
        return False, False, False
    return result, selected, idConv, idConvFaces


def add_cylinder(v1, v2, vertices, faces, radius):

  p1 = mathutils.Vector((v1.getX(), v1.getY(), v1.getZ()))
  p2 = mathutils.Vector((v2.getX(), v2.getY(), v2.getZ()))

  direction = p2 - p1
  length = direction.length

  if length > 1e-3:
    bpy.ops.mesh.primitive_cylinder_add(vertices = 32,
                                        radius = radius,
                                        depth = length,
                                        cap_ends = False,
                                        view_align=False,
                                        enter_editmode=False,
                                        location= (0.0, 0.0, 0.0),
                                        rotation= (0.0, 0.0, 0.0))
    c = bpy.context.active_object.data
    direction.normalize()
    u = direction
    uu = mathutils.Vector((0., 0., 1.0))
    if abs(mathutils.Vector.angle(u, uu)) > 1e-6:
        v = u.cross(uu).normalized()
        w = u.cross(v).normalized()
        A = mathutils.Matrix([[w[0], w[1], w[2], 0],
                              [v[0], v[1], v[2], 0],
                              [u[0], u[1], u[2], 0],
                              [direction[0] / 2.0 * length + p1[0],
                               direction[1] / 2.0 * length + p1[1],
                               direction[2] / 2.0 * length + p1[2], 1]])
    else:
        A = mathutils.Matrix([[1, 0, 0, 0],
                              [0, 1, 0, 0],
                              [0, 0, 1, 0],
                              [direction[0] / 2.0 * length + p1[0],
                               direction[1] / 2.0 * length + p1[1],
                               direction[2] / 2.0 * length + p1[2], 1]])
    # since 2.62: inversion of matrices
    A.transpose()
    c.transform(A)
    idtr=len(vertices)
    for vs in c.vertices:
        vertices.append([vs.co.x, vs.co.y, vs.co.z])
    for f in c.faces:
        if len(f.vertices) == 3:
            faces.append([idtr + f.vertices[0],
                          idtr + f.vertices[1],
                          idtr + f.vertices[2]])
        else:
            faces.append([idtr + f.vertices[0],
                          idtr + f.vertices[1],
                          idtr + f.vertices[2],
                          idtr + f.vertices[3]])
    bpy.ops.object.delete()


def create_spheres(context, points, radius, object, color=False):
    for point in points:
        create_sphere(context, point, radius, object, color)

def create_sphere(context, point, radius, object, color=False):
    # create sphere
    bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=2, size=radius,
                                          view_align=False,
                                          enter_editmode=False,
                                          location=mathutils.Vector((point.getX(), point.getY(), point.getZ())) * object.matrix_world,
                                          rotation=(0.0, 0.0, 0.0))
    obj = bpy.context.active_object

    if color:
        mat = get_material_from_color(color, True)
        obj.data.materials.append(mat)


    return obj

def create_border_paths(context, tmesh, object, radius = False, color = False):
    tmeshr = taglut.Mesh()
    bc = taglut.BorderSelector(tmeshr, False)
    tmesh.forEachEdge(bc)
    paths = tmeshr.getSqueletonPaths()
    if color and radius:
        create_tubular_paths(context, paths, radius, object, color)
    else:
        create_paths(context, paths, object)


def create_tubular_paths(context, paths, radius, object, color=False):
    for path in paths:
        create_tubular_path(context, path, radius, object, color)

def create_tubular_path(context, path, radius, object, color=False):
    mesh = bpy.data.meshes.new('tubular_path')
    vertices = []
    faces = []

    # convert path if it's a PLPath
    if path.__class__.__name__ == "PLPath":
        path = path.getCoordsPath()

    # create spheres
    bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=2, size=radius,
                                          view_align=False,
                                          enter_editmode=False,
                                          location=(0.0, 0.0, 0.0),
                                          rotation=(0.0, 0.0, 0.0))
    s = bpy.context.active_object.data
    for v in path:
        idtr=len(vertices)
        for vs in s.vertices:
            vertices.append([vs.co.x + v.getX(),
                             vs.co.y + v.getY(),
                             vs.co.z + v.getZ()])
        for f in s.faces:
            faces.append([idtr + f.vertices[0],
                          idtr + f.vertices[1],
                          idtr + f.vertices[2]])
    bpy.ops.object.delete()

    # create cylinders
    for i in range(len(path) - 1):
        add_cylinder(path[i], path[i + 1], vertices, faces, radius)

    # generate the corresponding mesh
    mesh.from_pydata(vertices, [], faces)
    mesh.update()

    obj = object_utils.object_data_add(context, mesh, operator=None).object
    obj.matrix_world = object.matrix_world

    if color:
        mat = get_material_from_color(color, True)
        obj.data.materials.append(mat)

    return obj

def create_paths(context, paths, object):
    for path in paths:
        create_path(context, path, object)

def create_path(context, path, object):
    mesh = bpy.data.meshes.new('path')
    vertices = []
    edges = []
    i = 0

    # convert path if it's a PLPath
    if path.__class__.__name__ == "PLPath":
        path = path.getCoordsPath()

    for p in path:
        vertices.append([p.getX(), p.getY(), p.getZ()])
        if i != 0:
            edges.append([i - 1, i])
        i = i + 1

    mesh.from_pydata(vertices, edges, [])

    mesh.update()

    obj = object_utils.object_data_add(context, mesh, operator=None).object
    obj.matrix_world = object.matrix_world
    return obj


global_sf_file = ""

def load_scalar_function(filename):
    global global_sf_file
    global global_sf
    global global_sf_time
    if filename == "":
        return False

    filename = os.path.normpath(bpy.path.abspath(filename))
    ftime = os.path.getmtime(filename)

    if filename != global_sf_file or ftime > global_sf_time:
        global_sf_file = filename
        global_sf = taglut.MeshMap(filename)
        global_sf_time = ftime


    return global_sf

def initialise():
    global_undo = bpy.context.user_preferences.edit.use_global_undo
    bpy.context.user_preferences.edit.use_global_undo = False
    previous_mode = bpy.context.active_object.mode
    bpy.ops.object.mode_set(mode='OBJECT')
    object = bpy.context.active_object
    mesh = bpy.context.active_object.data

    return(global_undo, previous_mode, object, mesh)

# clean up and set settings back to original state
def terminate(global_undo, previous_mode, object):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.active_object.select = False
    bpy.context.scene.objects.active = object
    bpy.context.active_object.select = True
    bpy.ops.object.mode_set(mode=previous_mode)
    bpy.context.user_preferences.edit.use_global_undo = global_undo


class ScalarValues(bpy.types.Operator):
    bl_idname = "mesh.taglutools_scalarvalues"
    bl_label = "scalar values"
    bl_description = "draw scalar values (color or isolevels)"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        meshmap = load_scalar_function(tt.sf_file)
        if not meshmap:
            self.report({'ERROR'}, "No scalar function defined")
            return {'CANCELLED'}

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        if tt.scalarvalues_isolevels:
            vf = taglut.VectorField(meshmap, tmesh)
            minValue = meshmap.getMinValue()
            step = (meshmap.getMaxValue() - minValue) / (tt.isolevels_number + 1)
            color = tt.scalarvalues_color.copy()
            for i in range(1, tt.isolevels_number):
                isolevel = meshmap.computeLevelSet(minValue + i * step, tt.scalarvalues_oriented)
                if tt.tubular:
                    if tt.scalarvalues_two_colors:
                        ratio = i / (tt.isolevels_number + 1)
                        color.r = tt.scalarvalues_color.r * (1 - ratio) + tt.scalarvalues_color2.r * ratio
                        color.g = tt.scalarvalues_color.g * (1 - ratio) + tt.scalarvalues_color2.g * ratio
                        color.b = tt.scalarvalues_color.b * (1 - ratio) + tt.scalarvalues_color2.b * ratio
                    create_tubular_paths(context, isolevel, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, color)
                else:
                    create_paths(context, isolevel, object)
        else:
            vcol_lay = mesh.vertex_colors.new()
            minValue = meshmap.getMinValue()
            maxValue = meshmap.getMaxValue()
            for i, f in enumerate(vcol_lay.data):
                f_col = f.color1, f.color2, f.color3
                for j, col in enumerate(f_col):
                    ratio = (meshmap.getValue(mesh.faces[i].vertices[j])  - minValue) / (maxValue - minValue)
                    col.r = tt.scalarvalues_color.r * (1 - ratio) + tt.scalarvalues_color2.r * ratio
                    col.g = tt.scalarvalues_color.g * (1 - ratio) + tt.scalarvalues_color2.g * ratio
                    col.b = tt.scalarvalues_color.b * (1 - ratio) + tt.scalarvalues_color2.b * ratio

            for i in range(len(object.material_slots)):
                object.active_material_index = i - 1
                bpy.ops.object.material_slot_remove()

            object.data.materials.append(get_vcol_material())
            mesh.validate()
            mesh.update()


        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}


class Borders(bpy.types.Operator):
    bl_idname = "mesh.taglutools_borders"
    bl_label = "border"
    bl_description = "draw borders"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}

        if tt.tubular:
            create_border_paths(context, tmesh, object, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), tt.border_color)
        else:
            create_border_paths(context, tmesh, object)


        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}


class TopologicalPropertiesTopology(bpy.types.Operator):
    bl_idname = "mesh.taglutools_infos_topology"
    bl_label = "Topological properties"
    bl_description = "Topological properties"
    bl_options = {'REGISTER', 'UNDO'}


    # ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def invoke(self, context, event):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)

        self.genus = tmesh.getGenus()
        self.nbCC = tmesh.getNbCC()

        terminate(global_undo, previous_mode, object)

        wm = context.window_manager
        wm.invoke_props_dialog(self)

        return {'RUNNING_MODAL'}

    def execute(self, context):
        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        tt = context.window_manager.taglutools

        # genus
        col.label('Genus: '+str(self.genus))
        col.label('Connected components: '+str(self.nbCC))

class TopologicalPropertiesSelection(bpy.types.Operator):
    bl_idname = "mesh.taglutools_infos_selection"
    bl_label = "Selection properties"
    bl_description = "Selection properties"
    bl_options = {'REGISTER', 'UNDO'}


    # ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def invoke(self, context, event):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)

        self.selected = selected
        self.mesh = mesh

        terminate(global_undo, previous_mode, object)

        wm = context.window_manager
        wm.invoke_props_dialog(self)

        return {'RUNNING_MODAL'}

    def execute(self, context):
        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        tt = context.window_manager.taglutools

        if len(self.selected) < 20:
            meshmap = load_scalar_function(tt.sf_file)
            if meshmap and len(self.mesh.vertices) == meshmap.getNbValues():
                col.label(str(len(self.selected)) + ' selected points: ')
                for l in [" * " + str(i) + ": " + str(meshmap.getValue(i)) for i in self.selected]:
                    col.label(l)
            else:
                col.label(str(len(self.selected)) + ' selected points: '+str(self.selected))
        else:
            col.label(str(len(self.selected)) + ' selected points')


class PointSelection(bpy.types.Operator):
    bl_idname = "mesh.taglutools_point_selection"
    bl_label = "point selection"
    bl_description = "select points by ID"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        if tt.p_s_replace:
            for i in range(len(mesh.vertices)):
                mesh.vertices[i].select = False


        points = [int(s) for s in tt.p_selection.split(' ') if re.match("[0-9]+", s) and int(s) < len(mesh.vertices)]
        for p in points:
            mesh.vertices[p].select = True
        mesh.update()

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}

class Color(bpy.types.Operator):
    bl_idname = "mesh.taglutools_color"
    bl_label = "color"
    bl_description = "paint objects using taglut color"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        for i in range(len(object.material_slots)):
            object.active_material_index = i - 1
            bpy.ops.object.material_slot_remove()

        colors = { 'GREY' : [0.854, 0.84, 0.8],
                   'YELLOW' : [0.89, 0.643, 0.106],
                   'BLUE': [0.16, 0.394, 0.617],
                   'PINK' : [0.796, 0.277, 0.769]}

        mat = get_material_from_color(colors[tt.color])
        object.data.materials.append(mat)

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}

class CriticalPoints(bpy.types.Operator):
    bl_idname = "mesh.taglutools_critical_points"
    bl_label = "critical points"
    bl_description = "create critical points"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        meshmap = load_scalar_function(tt.sf_file)
        if not meshmap:
            self.report({'ERROR'}, "No scalar function defined")
            return {'CANCELLED'}

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        if tt.saddle_points:
            saddles = meshmap.getSaddlePoints()
            saddle_p = [tmesh.point(p[0]) for p in saddles]
            create_spheres(context, saddle_p, 2 * tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.saddle_color)

        if tt.extrema:
            extrema = meshmap.getLocalExtrema()
            extrema_p = [tmesh.point(p) for p in extrema]
            create_spheres(context, extrema_p, 2 * tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.extrema_color)

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}


class SFGeneration(bpy.types.Operator):
    bl_idname = "mesh.taglutools_sf_generation"
    bl_label = "s.f. generation"
    bl_description = "scalar function generation"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}

        mapping = taglut.MeshMap(tmesh)
        if tt.sf_generation == 'Z':
            mapping.setValuesFromAxis(taglut.DTop)
        elif tt.sf_generation == 'Y':
            mapping.setValuesFromAxis(taglut.DLeft)
        elif tt.sf_generation == 'X':
            mapping.setValuesFromAxis(taglut.DFront)
        elif tt.sf_generation == 'DISTANCE':
            fastmarching = taglut.FastMarching()
            fastmarching.setDefaultValue(-.1)
            fastmarching.computeDistanceFromPoints(mapping, tmesh, taglut.VertexIDVector(selected))
        elif tt.sf_generation == 'GEODESIC':
            points = tmesh.findExtrema(1e-3)
            trpoints = taglut.VertexIDVector(points)
            print(str(len(trpoints)) + " initial points")
            fastmarching = taglut.FastMarching()
            fastmarching.setDefaultValue(-.1)
            fastmarching.computeDistanceFromPoints(mapping, tmesh, trpoints)
            mapping.removeFlatEdges()
        else:
            self.report({'ERROR'}, "Unknown s.f. generation method")
            return {'CANCELLED'}

        try:
            mapping.save(tt.sf_output)
            if tt.sf_replace:
                tt["sf_file"] = tt.sf_output
        except:
            self.report({'ERROR'}, "Error: No scalar function defined")
            return {'CANCELLED'}

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}

class SFSmoothing(bpy.types.Operator):
    bl_idname = "mesh.taglutools_sf_smoothing"
    bl_label = "s.f. smoothing"
    bl_description = "scalar function smoothing"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}

        meshmap = load_scalar_function(tt.sf_file)

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        if tt.sf_smooth_stop == 'IT':
            meshmap.smoothValues(tt.sf_smooth_it)
        else:
            nbext = meshmap.getNbLocalExtrema()
            while nbext > tt.sf_smooth_nb_ext:
                meshmap.smoothValues()
                nbext = meshmap.getNbLocalExtrema()
                print(str(nbext) + " (target: " + str(tt.sf_smooth_nb_ext) + ")")
        try:
            meshmap.save(tt.sf_smooth_output)
            if tt.sf_smooth_replace:
                tt["sf_file"] = tt.sf_smooth_output
        except:
            self.report({'ERROR'}, "Error: No scalar function defined")
            return {'CANCELLED'}

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}


class LevelSet(bpy.types.Operator):
    bl_idname = "mesh.taglutools_levelset"
    bl_label = "level set"
    bl_description = "create level set"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH' and context.mode == 'EDIT_MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        meshmap = load_scalar_function(tt.sf_file)
        if not meshmap:
            self.report({'ERROR'}, "No scalar function defined")
            return {'CANCELLED'}

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        vf = taglut.VectorField(meshmap, tmesh)

        for point in selected:
            if not meshmap.isSaddlePoint(point):
                path = meshmap.computeDirectLevelSetCC(taglut.PointOnEdge(tmesh.point(point)))
                if tt.tubular:
                    create_tubular_path(context, path, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.ls_color)
                else:
                    create_path(context, path, object)

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}

class IntegralLines(bpy.types.Operator):
    bl_idname = "mesh.taglutools_integrallines"
    bl_label = "integral lines"
    bl_description = "create integral lines"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH' and context.mode == 'EDIT_MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        meshmap = load_scalar_function(tt.sf_file)
        if not meshmap:
            self.report({'ERROR'}, "No scalar function defined")
            return {'CANCELLED'}

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        vf = taglut.VectorField(meshmap, tmesh)

        for point in selected:
            if meshmap.isSaddlePoint(point):
                try:
                    paths = taglut.PLPath.getIntegralLinesFromSaddlePoint(tmesh.point(point), tmesh, meshmap, vf, -1., True)
                except:
                    self.report({'ERROR'}, "Cannot find integral lines from a given saddle point")
                    return {'CANCELLED'}

                if tt.tubular:
                    create_tubular_paths(context, paths, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.integralline_color)
                    if tt.draw_points:
                        create_sphere(context, tmesh.point(point), 2 * tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.integralline_color)
                else:
                    create_paths(context, paths, object)
            else:
                if tt.direction == 'UP':
                    path = taglut.PLPath(taglut.PointOnEdge(tmesh.point(point)), True, tmesh, meshmap, vf)
                elif tt.direction == 'DOWN':
                    path = taglut.PLPath(taglut.PointOnEdge(tmesh.point(point)), False, tmesh, meshmap, vf)
                else:
                    path = taglut.PLPath.computeIntegralLine(taglut.PointOnEdge(tmesh.point(point)), tmesh, meshmap, vf)

                if tt.tubular:
                    create_tubular_path(context, path, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.integralline_color)
                    if tt.draw_points:
                        create_sphere(context, tmesh.point(point), 2 * tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.integralline_color)
                else:
                    create_path(context, path, object)

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}


class IntegralLinesSaddlePoints(bpy.types.Operator):
    bl_idname = "mesh.taglutools_integrallines_saddlepoints"
    bl_label = "integral lines (from s.p.)"
    bl_description = "create integral lines from saddle points"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        meshmap = load_scalar_function(tt.sf_file)
        if not meshmap:
            self.report({'ERROR'}, "No scalar function defined")
            return {'CANCELLED'}

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        vf = taglut.VectorField(meshmap, tmesh)

        for point in meshmap.getSaddlePoints():
            if point[1] == 2:
                try:
                    paths = taglut.PLPath.getIntegralLinesFromSaddlePoint(tmesh.point(point[0]), tmesh, meshmap, vf, -1., True)
                except:
                    self.report({'ERROR'}, "Cannot find integral lines from a given saddle point")
                    return {'CANCELLED'}

                if tt.tubular:
                    create_tubular_paths(context, paths, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.integralline_color)
                    if tt.draw_points:
                        create_sphere(context, tmesh.point(point[0]), 2 * tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.integralline_color)
                else:
                    create_paths(context, paths, object)

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}


class NLoops(bpy.types.Operator):
    bl_idname = "mesh.taglutools_nloops"
    bl_label = "n-loops"
    bl_description = "create n-loops"
    bl_options = {'REGISTER', 'UNDO'}

    ## Check for mesh
    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    ## execute
    def execute(self, context):
        tt = context.window_manager.taglutools

        if tt.nl_cut and tt.nl_scalar_partial_output == "" and tt.nl_cylinders == "SCALAR":
            self.report({'ERROR'}, "No output file defined")
            return {'CANCELLED'}

        meshmap = load_scalar_function(tt.sf_file)
        if not meshmap:

            self.report({'ERROR'}, "No scalar function defined")
            return {'CANCELLED'}

        # initialise
        global_undo, previous_mode, object, mesh = initialise()

        tmesh, selected, idConv, idConvFaces = toTaglut(mesh)
        if not tmesh:
            self.report({'ERROR'}, "Error during mesh transfer to taglut")
            return {'CANCELLED'}
        if tmesh.getNbPoints() != meshmap.getNbValues():
            self.report({'ERROR'}, "Bad number of values in scalar function (" + str(tmesh.getNbPoints())
                        + " vs " + str(meshmap.getNbValues()) +")")
            return {'CANCELLED'}

        if meshmap.hasMesh():
            # assume that flat edges has been removed before
            meshmap.setMesh(tmesh)
        else:
            meshmap.setMesh(tmesh)
            meshmap.removeFlatEdges()

        vf = taglut.VectorField(meshmap, tmesh)

        nlf = taglut.NLoopFactory()
        nlf.setSplitForNLoop(not tt.nl_preserve_3loops)
        nlf.setMerge(not tt.nl_preserve_3loops)
        nlf.setAdjustByAngles(tt.nl_adjust_angles)
        nlf.setEpsilonForMerge(tt.nl_epsilon_intersection)
        if tt.snapToVertices:
            nlf.setSnapToVertices(tt.snapToVerticesEpsilon)
        if tt.nl_use_prenloops:
            nl = nlf.getPreNLoopsFromSaddlePoints(tmesh, meshmap, vf)
        else:
            nl = nlf.getNLoopsAsPLPathFromSaddlePoints(tmesh, meshmap, vf)

        if tt.nl_remove_discs:
            if tt.nl_use_prenloops:
                nl = taglut.PreNLoop.removeFlatDiscs(nl, tmesh, meshmap, tt.nl_discs_ratio)
            else:
                nl = taglut.PLPath.removeFlatDiscs(nl, tmesh, tt.nl_discs_ratio)

        if not tt.nl_preserve_3loops and tt.nl_cut:
            plCut = taglut.MeshPLCut()
            newMM = taglut.MeshMap(meshmap)
            if tt.nl_use_prenloops:
                tmesh1 = plCut.cutMeshPNLUpdateScalarFunction(tmesh, nl, newMM)
            else:
                tmesh1 = plCut.cutMeshUpdateScalarFunction(tmesh, nl, newMM)
            newMM.setMesh(tmesh1)

            if tt.nl_cut_extrema_discs:
                tmesh2 = plCut.cutExtremaDiscsUpdateScalarFunction(tmesh1, newMM)
                newMM.setMesh(tmesh2)
            else:
                tmesh2 = tmesh1

            if tt.nl_cylinders == 'CUT':
                paramMethod = tt.CylinderToQuadMethodTaglut[tt.nl_c2q]
                paramChoice = tt.C2QParamChoiceTaglut[tt.nl_c2q_pchoice]
                epsilonSnap = 0.
                if tt.snapToVertices:
                    epsilonSnap = tt.snapToVerticesEpsilon
                if tt.nl_subcylinders and tt.nl_subcylinders_method == 'CYLPARAM':
                    tmesh3 = plCut.cutMeshInQuadranglesFromCylindricalTilingUpdateScalarFunction(tmesh2, newMM, tt.nl_subcylinders_ratio, epsilonSnap, paramChoice, paramMethod)
                else:
                    tmesh3 = plCut.cutMeshInQuadranglesFromCylindricalTilingUpdateScalarFunction(tmesh2, newMM, -1, epsilonSnap, paramChoice, paramMethod)
                newMM.setMesh(tmesh3)

                if tt.nl_subcylinders:
                    if tt.nl_subcylinders_method == 'FLOATERPARAM':
                        tmesh4 = plCut.cutCylindersByQuadParamUpdateScalarFunction(tmesh3, tt.nl_subcylinders_ratio, newMM)
                        tmesh3 = tmesh4
                        newMM.setMesh(tmesh3)
                    elif tt.nl_subcylinders_method == 'SHPATH':
                        tmesh4 = plCut.cutCylindersByShPathParamUpdateScalarFunction(tmesh3, tt.nl_subcylinders_ratio, newMM)
                        tmesh3 = tmesh4
                        newMM.setMesh(tmesh3)

            elif tt.nl_cylinders == 'PRESERVE':
                tmesh3 = tmesh2
            else: # SCALAR
                tmesh3 = tmesh2

            if (tt.nl_cylinders == 'CUT' and tt.nl_quad_param) or tt.nl_nurbs_remesh:
                mtiling = taglut.MTiling(tmesh3)
                if tt.nl_quad_discs_by_nl:
                    mtiling.adjustQuads(newMM)
                else:
                    mtiling.adjustQuads()
                if tt.nl_cylinders == 'CUT' and tt.nl_nurbs_remesh:
                    tmesh4 = mtiling.remeshByBSplines(tt.nl_nurbs_remesh_distance,
                                                      tt.nl_nurbs_degree, tt.nl_nurbs_nbiter,
                                                      tt.nl_nurbs_nbiter_bd)
                    tmesh3 = tmesh4

            updateFromTaglut(mesh, tmesh3)

            if tt.nl_scalar_update and not (tt.nl_cylinders == 'CUT' and tt.nl_nurbs_remesh):
                try:
                    newMM.save(tt.nl_scalar_output)
                    if tt.nl_scalar_replace:
                        tt["sf_file"] = tt.nl_scalar_output
                except:
                    self.report({'ERROR'}, "Error: No scalar function defined")
                    return {'CANCELLED'}

            if tt.nl_cylinders == "SCALAR":
                mapping = taglut.MeshMap(tmesh3)
                plCut.updateForCylindricalTiling(mapping, tmesh3, tt.nl_scalar_partial_twist)
                try:
                    mapping.save(tt.nl_scalar_partial_output)
                    if tt.nl_scalar_partial_replace:
                        tt["sf_file"] = tt.nl_scalar_partial_output
                except:
                    self.report({'ERROR'}, "Error: No scalar function defined")
                    return {'CANCELLED'}

            elif tt.nl_quad_param and not tt.nl_nurbs_remesh:
                mapping = mtiling.parameterize()
                set_uv(mesh, object, mapping)

            if tt.nl_display_borders:
                if tt.tubular:
                    create_border_paths(context, tmesh3, object, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), tt.nl_color)
                else:
                    create_border_paths(context, tmesh3, object)

        else:
            if tt.nl_use_prenloops:
                print("Not yet implemented")
            else:
                paths = taglut.PLPath.getCoordsPaths(nl)
                if tt.tubular:
                    create_tubular_paths(context, paths, tt.tubular_radius * tmesh.getMaxDistanceXYZ(), object, tt.nl_color)
                else:
                    create_paths(context, paths, object)

        terminate(global_undo, previous_mode, object)
        return {'FINISHED'}

class tools_taglutools():
    def draw_integrallines_saddlepoints(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_integrallines:
            split.prop(tt, "display_integrallines", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_integrallines", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_integrallines_saddlepoints", text="Integral lines")
        # integral-lines - settings
        if tt.display_integrallines:
            col_il = col.column(align=True).box().column()
            col_il.enabled = tt.tubular
            col_il.prop(tt, 'draw_points', text='Draw points')
            col_il.prop(tt, 'integralline_color', text='Color')

    def draw_integrallines(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_integrallines:
            split.prop(tt, "display_integrallines", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_integrallines", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_integrallines", text="Integral lines")
        # integral-lines - settings
        if tt.display_integrallines:
            col_il = col.column(align=True).box().column()
            col_il.prop(tt, 'direction', text='Direction')
            col_il_il = col_il.column()
            col_il_il.enabled = tt.tubular
            col_il_il.prop(tt, 'draw_points', text='Draw points')
            col_il_il.prop(tt, 'integralline_color', text='Color')

    def draw_levelset(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_levelset:
            split.prop(tt, "display_levelset", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_levelset", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_levelset", text="Level sets")
        # levelset - settings
        if tt.display_levelset:
            col_ls = col.column(align=True).box().column()

            col_ls_col = col_ls.column()
            col_ls_col.enabled = tt.tubular

            col_ls_col.prop(tt, 'ls_color', text='Color')

    def draw_scalarvalues(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_scalarvalues:
            split.prop(tt, "display_scalarvalues", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_scalarvalues", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_scalarvalues", text="Scalar values")
        # scalarvalues - settings
        if tt.display_scalarvalues:
            col_sv = col.column(align=True).box().column()

            if tt.scalarvalues_isolevels:
                col_sv.prop(tt, 'scalarvalues_isolevels', text='Draw isolevels', icon='DOWNARROW_HLT')
                box_sv = col_sv.box()
                box_sv.prop(tt, 'isolevels_number', text='Number')
                box_sv.prop(tt, 'scalarvalues_oriented', text='Use surface orientation')
            else:
                col_sv.prop(tt, 'scalarvalues_isolevels', text='Draw isolevels', icon='RIGHTARROW')

            col_scal_col = col_sv.column()
            col_scal_col.enabled = (not tt.scalarvalues_isolevels) or tt.tubular


            if tt.scalarvalues_isolevels:
                col_scal_col.prop(tt, 'scalarvalues_color', text='Color')
                if tt.scalarvalues_two_colors:
                    col_scal_col.prop(tt, 'scalarvalues_two_colors', text='Gradation', icon='DOWNARROW_HLT')
                    box = col_scal_col.box()
                    box.prop(tt, 'scalarvalues_color2', text='Second color')
                else:
                    col_scal_col.prop(tt, 'scalarvalues_two_colors', text='Gradation', icon='RIGHTARROW')
            else:
                col_scal_col.prop(tt, 'scalarvalues_color', text='Color')
                col_scal_col.prop(tt, 'scalarvalues_color2', text='Second color')

    def draw_color(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_color:
            split.prop(tt, "display_color", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_color", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_color", text="Color")
        # color - settings
        if tt.display_color:
            ccol = col.column(align=True).box().column()
            ccol.prop(tt, 'color', text='Color')

    def draw_borders(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_borders:
            split.prop(tt, "display_borders", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_borders", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_borders", text="Borders")
        # borders - settings
        if tt.display_borders:
            bbox = col.column(align=True).box().column()
            bbox.enabled = tt.tubular
            bbox.prop(tt, 'border_color', text='Color')

    def draw_point_selection(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_color:
            split.prop(tt, "display_point_selection", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_point_selection", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_point_selection", text="Point selection")
        # point selection - settings
        if tt.display_point_selection:
            ccol = col.column(align=True).box().column()
            ccol.prop(tt, 'p_selection', text='Points')
            ccol.prop(tt, 'p_s_replace', text='Replace')

    def draw_sf_generation(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_sf_generation:
            split.prop(tt, "display_sf_generation", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_sf_generation", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_sf_generation", text="S.F. generation")
        # scalar function generation - settings
        if tt.display_sf_generation:
            col_sf_gen = col.column(align=True).box().column()

            col_sf_gen.prop(tt, 'sf_generation', text='Method')
            col_sf_gen.prop(tt, 'sf_output', text='Output file')
            col_sf_gen.prop(tt, 'sf_replace', text='Replace')

    def draw_sf_smoothing(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_sf_smoothing:
            split.prop(tt, "display_sf_smoothing", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_sf_smoothing", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_sf_smoothing", text="S.F. smoothing")
        # scalar function smoothing - settings
        if tt.display_sf_smoothing:
            col_sf_s_gen = col.column(align=True).box().column()

            col_sf_s_gen.prop(tt, 'sf_smooth_stop', text='Stop of smoothing')
            if tt.sf_smooth_stop == 'IT':
                col_sf_s_gen.prop(tt, 'sf_smooth_it', text='Number of iterations')
            else:
                col_sf_s_gen.prop(tt, 'sf_smooth_nb_ext', text='Number of extrema')
            col_sf_s_gen.prop(tt, 'sf_smooth_output', text='Output file')
            col_sf_s_gen.prop(tt, 'sf_smooth_replace', text='Replace')

    def draw_critical_points(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_critical_points:
            split.prop(tt, "display_critical_points", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_critical_points", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_critical_points", text="Critical points")
        # critical points - settings
        if tt.display_critical_points:
            col2 = col.column(align=True).box().column()
            if tt.extrema:
                col2.prop(tt, 'extrema', text='Extrema', icon='DOWNARROW_HLT')
                box = col2.box()
                box.prop(tt, 'extrema_color', text='Extrema color')
            else:
                col2.prop(tt, 'extrema', text='Extrema', icon='RIGHTARROW')

            if tt.saddle_points:
                col2.prop(tt, 'saddle_points', text='Saddle points', icon='DOWNARROW_HLT')
                box = col2.box()
                box.prop(tt, 'saddle_color', text='Saddle points color')

            else:
                col.prop(tt, 'saddle_points', text='Saddle points', icon='RIGHTARROW')

    def draw_nloops(self, col, tt):
        split = col.split(percentage=0.15)
        if tt.display_nloops:
            split.prop(tt, "display_nloops", text="", icon='DOWNARROW_HLT')
        else:
            split.prop(tt, "display_nloops", text="", icon='RIGHTARROW')
        split.operator("mesh.taglutools_nloops", text="N-loops")
        # n-loops - settings
        if tt.display_nloops:

            precol2 = col.column(align=True).box().column()
            precol2.prop(tt, 'nl_use_prenloops', text="Use preNLoops")
            precol2.prop(tt, 'nl_preserve_3loops', text='Preserve 3-loops')
            precol2.prop(tt, 'nl_remove_discs', text='Remove discs')
            col2_dr = precol2.column()
            col2_dr.enabled = tt.nl_remove_discs
            col2_dr.prop(tt, 'nl_discs_ratio', text='Minimal ratio')

            col2 = precol2.column(align=True).column()
            col2.enabled = not tt.nl_preserve_3loops

            col2.prop(tt, 'nl_adjust_angles', text='Adjust by angles')
            col2.prop(tt, 'nl_epsilon_intersection', text='Acceptance for merge')

            if tt.nl_cut and not tt.nl_preserve_3loops:
                col2.prop(tt, 'nl_cut', text='Cut mesh', icon="DOWNARROW_HLT")
                box = col2.column().box()
                col3 = box.column()

                col3.prop(tt, 'nl_cut_extrema_discs', text='Cut extrema discs')
                col3.prop(tt, 'nl_cylinders', text='Cut cylinders')
                if tt.nl_cylinders == 'CUT':
                    col4 = col3.box()
                    col4.prop(tt, 'nl_c2q', text='Quad method')
                    col4.prop(tt, 'nl_c2q_pchoice', text='Param. choice')

                    if tt.nl_subcylinders:
                        col4.prop(tt, 'nl_subcylinders', text='Subdivise cylinders', icon="DOWNARROW_HLT")
                        ccol4 = col4.box()
                        ccol4.prop(tt, 'nl_subcylinders_ratio', text="Ratio")
                        ccol4.prop(tt, 'nl_subcylinders_method', text="Method")
                    else:
                        col4.prop(tt, 'nl_subcylinders', text='Subdivise cylinders', icon="RIGHTARROW")

                    if tt.nl_nurbs_remesh:
                        col4.prop(tt, 'nl_nurbs_remesh', text='Remesh by NURBS', icon="DOWNARROW_HLT")
                        col5 = col4.box()
                        col5.prop(tt, 'nl_nurbs_remesh_distance', text='Distance between points')
                        col5.prop(tt, 'nl_nurbs_degree', text='Degree')
                        col5.prop(tt, 'nl_nurbs_nbiter', text='Nb iterations')
                        col5.prop(tt, 'nl_nurbs_nbiter_bd', text='Nb iter. for borders')
                        col5.prop(tt, 'nl_quad_discs_by_nl', text='Use scalar function for quads on discs')
                    else:
                        col4.prop(tt, 'nl_nurbs_remesh', text='Remesh by NURBS', icon="RIGHTARROW")
                        if tt.nl_quad_param:
                            col4.prop(tt, 'nl_quad_param', text='Texture mapping', icon="DOWNARROW_HLT")
                            col4col = col4.box()
                            col4col.prop(tt, 'nl_quad_discs_by_nl', text='Use scalar function for quads on discs')
                        else:
                            col4.prop(tt, 'nl_quad_param', text='Texture mapping', icon="RIGHTARROW")


                elif tt.nl_cylinders == 'SCALAR':
                    col4 = col3.box()
                    col4.prop(tt, 'nl_scalar_partial_twist', text='Twist direction')
                    col4.prop(tt, 'nl_scalar_partial_output', text="Output")
                    ccol4 = col4.column()
                    ccol4.prop(tt, 'nl_scalar_partial_replace', text="Replace input")
                    ccol4.enabled = not tt.nl_scalar_replace

                if tt.nl_display_borders:
                    col3.prop(tt, 'nl_display_borders', text='Draw borders after cut', icon="DOWNARROW_HLT")
                    col_bd_col = col3.column()
                    col_bd_col.enabled = tt.tubular

                    col4 = col_bd_col.box()
                    col4.prop(tt, 'nl_color', text='Border color')
                else:
                    col3.prop(tt, 'nl_display_borders', text='Draw borders after cut', icon="RIGHTARROW")

                if tt.nl_scalar_update:
                    col3.prop(tt, 'nl_scalar_update', text='Update the scalar function', icon="DOWNARROW_HLT")
                    col_sf_update = col3.column()

                    col_sf_update.enabled = not (tt.nl_nurbs_remesh and (tt.nl_cylinders == 'CUT'))
                    col5 = col_sf_update.box()
                    col5.prop(tt, 'nl_scalar_output', text='Output file')
                    col5.prop(tt, 'nl_scalar_replace', text='Replace original')
                else:
                    col3.prop(tt, 'nl_scalar_update', text='Update the scalar function', icon="RIGHTARROW")
            else:
                col2.prop(tt, 'nl_cut', text='Cut mesh', icon="RIGHTARROW")
                col_nl_col = precol2.column()
                col_nl_col.enabled = tt.tubular
                col_nl_col.prop(tt, 'nl_color', text='n-loops color')



class VIEW3D_PT_edit_tools_taglutools(bpy.types.Panel, tools_taglutools):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = "mesh_edit"
    bl_label = "Taglutools (edit mode)"

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        tt = context.window_manager.taglutools

        col.label('General:')
        box = col.column(align=True).box()
        colbox = box.column()
        colbox.prop(tt, 'tubular', text='Tubular paths')
        colbox_rad = colbox.column()
        colbox_rad.enabled = tt.tubular
        colbox_rad.prop(tt, 'tubular_radius', text='Radius')
        colbox.prop(tt, 'snapToVertices', text='Snap to vertices')
        colbox_snap = colbox.column()
        colbox_snap.enabled = tt.snapToVertices
        colbox_snap.prop(tt, 'snapToVerticesEpsilon', text='Epsilon')

        # color
        self.draw_color(col, tt)
        # border
        self.draw_borders(col, tt)
        # point selection
        self.draw_point_selection(col, tt)
        # properties
        col.operator("mesh.taglutools_infos_topology", text="Topological properties")
        col.operator("mesh.taglutools_infos_selection", text="Selection properties")

        col.label('Scalar funtions:')
        # global properties
        box = col.column(align=True).box().column()
        box.prop(tt, 'sf_file', text='Scalar function')

        # sf generation
        self.draw_sf_generation(col, tt)

        # sf smoothing
        self.draw_sf_smoothing(col, tt)

        # integral-lines
        self.draw_integrallines(col, tt)

        # critical points
        self.draw_critical_points(col, tt)

        # scalar values
        self.draw_scalarvalues(col, tt)

        # level set
        self.draw_levelset(col, tt)

        # n-loops
        self.draw_nloops(col, tt)

class VIEW3D_PT_tools_taglutools(bpy.types.Panel, tools_taglutools):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = "objectmode"
    bl_label = "Taglutools"

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        tt = context.window_manager.taglutools

        col.label('General')
        colbox = col.column(align=True).box().column()
        colbox.prop(tt, 'tubular', text='Tubular paths')
        colbox_rad = colbox.column()
        colbox_rad.enabled = tt.tubular
        colbox_rad.prop(tt, 'tubular_radius', text='Radius')
        colbox.prop(tt, 'snapToVertices', text='Snap to vertices')
        colbox_snap = colbox.column()
        colbox_snap.enabled = tt.snapToVertices
        colbox_snap.prop(tt, 'snapToVerticesEpsilon', text='Epsilon')

        # color
        self.draw_color(col, tt)
        # border
        self.draw_borders(col, tt)
        # point selection
        self.draw_point_selection(col, tt)
        # properties
        col.operator("mesh.taglutools_infos_topology", text="Topological properties")
        col.operator("mesh.taglutools_infos_selection", text="Selection properties")

        col.label('Scalar functions:')

        # global properties
        box = col.column(align=True).box().column()
        box.prop(tt, 'sf_file', text='Scalar function')

        # sf generation
        self.draw_sf_generation(col, tt)

        # sf smoothing
        self.draw_sf_smoothing(col, tt)

        # integral-lines
        self.draw_integrallines_saddlepoints(col, tt)

        # critical points
        self.draw_critical_points(col, tt)

        # scalar values
        self.draw_scalarvalues(col, tt)

        # n-loops
        self.draw_nloops(col, tt)

class TaglutoolsProps(bpy.types.PropertyGroup):
    """
    Fake module like class
    bpy.context.window_manager.taglutools
    """
    # display properties
    display_integrallines = bpy.props.BoolProperty(name = "IL settings",
        description = "Display settings of the integral lines tool",
        default = False)
    display_scalarvalues = bpy.props.BoolProperty(name = "Scalar values settings",
        description = "Display settings of the scalar values drawing tool",
        default = False)
    display_color = bpy.props.BoolProperty(name = "Color settings",
        description = "Display settings of the color tool",
        default = False)
    display_borders = bpy.props.BoolProperty(name = "Border settings",
        description = "Display settings of the borders tool",
        default = False)
    display_point_selection = bpy.props.BoolProperty(name = "Point selection settings",
        description = "Display settings of the point selection tool",
        default = False)
    display_critical_points = bpy.props.BoolProperty(name = "Critical points",
        description = "Display settings of critical points",
        default = False)
    display_nloops = bpy.props.BoolProperty(name = "N-loops",
        description = "Display settings of n-loops",
        default = False)
    display_sf_generation = bpy.props.BoolProperty(name = "S.F. generation",
        description = "Display settings of the scalar function generation",
        default = False)
    display_sf_smoothing = bpy.props.BoolProperty(name = "S.F. smoothing",
        description = "Display settings of the scalar function smoothing",
        default = False)
    display_levelset = bpy.props.BoolProperty(name = "Level set",
        description = "Display settings of the level set tool",
        default = False)

    # global parameters
    sf_file = bpy.props.StringProperty(subtype="FILE_PATH", name="Scalar function", description="Input scalar function")
    tubular = BoolProperty(name="Tubular paths",
                           description="Draw tubular paths rather than wired paths",
                           default=True)
    tubular_radius = FloatProperty(name="Tube radius",
                                     default=.004, min=0., max=.5,
                                     description="Set the radius to draw tubes (given as a ratio of the maximum bounded box dimension of the manipulated object)")
    snapToVertices = BoolProperty(name="Snap to vertices",
                                  description="If a new point is close to an original vertex, the produced point is adjusted on the original one",
                                  default=False)
    snapToVerticesEpsilon = FloatProperty("Epsilon (snap to vertices)",
                                          default = .1, min=0., max=.5,
                                          description="Epsilon value corresponding to the threshold for snapping a point on an original vertex")


    # integral lines properties
    Directions = [ ('BOTH', 'both', 'direction: up and down'),
                   ('UP', 'up', 'direction: up'),
                   ('DOWN', 'down', 'direction: down')
                   ]
    direction = EnumProperty(name="Output direction",
                             description="Direction for the integral line",
                             default='BOTH',
                             items=Directions)
    draw_points = BoolProperty(name="Draw points",
                           description="Draw starting points (integral line)",
                           default=True)
    integralline_color = FloatVectorProperty(name="Integral lines color",
                                             default=(0.133, 0.756, 0.0),
                                             min=0., max=1.,
                                             description="Color of the integral lines",
                                             subtype='COLOR')

    # scalarvalues properties
    scalarvalues_isolevels = BoolProperty(name="Isolevels",
                                          default=False,
                                          description="Draw isolevels rather than painting the mesh")
    isolevels_number = IntProperty(name="Number",
                                   default=15, min=1,
                                   description="Number of isolevels")
    scalarvalues_color = FloatVectorProperty(name="Isolevel color (1)",
                                          default=(0.756, 0.070, 0.0),
                                          min=0., max=1.,
                                          description="Color of the scalar function (minima)",
                                          subtype='COLOR')
    scalarvalues_two_colors = BoolProperty(name="Two colors",
                                        default=True,
                                        description="Use a gradation")
    scalarvalues_color2 = FloatVectorProperty(name="Isolevel color (2)",
                                          default=(1., 0.756, 0.),
                                           min=0., max=1.,
                                          description="Color of the scalar function (maxima)",
                                          subtype='COLOR')
    scalarvalues_oriented = BoolProperty(name="Oriented",
                                          default=False,
                                          description="Isolevel curves are oriented according to the surface's orientation")

    # critical points
    extrema = BoolProperty(name="Extrema",
                           default = True,
                           description = "Draw extrema points")
    extrema_color = FloatVectorProperty(name="Extrema color",
                                        default=(0.756, 0.070, 0.0),
                                        min=0., max=1.,
                                        description="Color of the extrema points",
                                        subtype='COLOR')
    saddle_points = BoolProperty(name="Saddle points",
                                 default = True,
                                 description = "Draw saddle points")
    saddle_color = FloatVectorProperty(name="Saddle points color",
                                       default=(0.070, 0.0, 0.756),
                                       min=0., max=1.,
                                       description="Color of the saddle points",
                                       subtype='COLOR')

    # border properties
    border_color = FloatVectorProperty(name="Border color",
                                       default=(0.756, 0.070, 0.0),
                                       min=0., max=1.,
                                       description="Color of the border lines",
                                       subtype='COLOR')

    # color properties
    Colors = [ ('GREY', 'grey', 'Grey color'),
               ('YELLOW', 'yellow', 'Yellow color'),
               ('BLUE', 'blue', 'Blue color'),
               ('PINK', 'pink', 'Pink color') ]
    color = EnumProperty(name="Selected color",
                         description="Color to paint object",
                         default='GREY',
                         items=Colors)

    # selection properties
    p_selection = StringProperty(name="Selection",
                                 default="0",
                                 description="A list of point IDs separated by spaces")
    p_s_replace = BoolProperty(name="Replace",
                               default=True,
                               description="Replace the current selection")

    # n-loops
    nl_preserve_3loops = BoolProperty(name="Preserve 3-loops",
                                      default = False,
                                      description="Preserve the 3-loops rather than merging/splitting them.")
    nl_use_prenloops = BoolProperty(name="Use pre-n-loops",
                                      default = True,
                                      description="Use pre-n-loop rather than approximating it with n-loops (that have base points on vertices).")
    nl_adjust_angles = BoolProperty(name="Adjust by angles",
                                    default = False,
                                    description="Adjust the shape of the n-loops using angles")
    nl_epsilon_intersection = FloatProperty(name="Acceptance for intersection",
                                            default=0., min=0., max=50.,
                                            description="Epsilon value as acceptance for intersection test between n-loops during the merge process")
    nl_cut = BoolProperty(name="Cut using n-loops",
                          default = False,
                          description="Cut mesh rather than drawing paths")
    nl_cut_extrema_discs = BoolProperty(name="Cut the extrema discs",
                          default = True,
                          description="Cut the extrema discs in order to obtain quads")
    CylinderToQuadMethodTaglut = { '2DPARAM' : taglut.MeshPLCut.CPMFloater2D,
                                   '3DPARAM' : taglut.MeshPLCut.CPMFloater3D,
                                   '3DPARAMIL' : taglut.MeshPLCut.CPMFloaterIntegralLines,
                                   '3DPARAM4IL' : taglut.MeshPLCut.CPMFloater4IntegralLines }
    CylinderToQuadMethod = [ #('2DPARAM', '2D parameterization', '2D parameterization (Floater)'), experimental
               ('3DPARAM', '3D parameterization', '3D parameterization (Floater)'),
               ('3DPARAMIL', '3D param. with i.l.', '3D parameterization with integral lines'),
               ('3DPARAM4IL', '3D param. with 4 i.l.', '3D parameterization with 4 integral lines') ]
    nl_c2q = EnumProperty(name="Quad method",
                          default='3DPARAM4IL',
                          description="Cylinder-to-quad method",
                          items=CylinderToQuadMethod)
    C2QParamChoiceTaglut = { 'LENGTH' :  taglut.MeshPLCut.CPCByLength,
                             'CONFORMITY' : taglut.MeshPLCut.CPCByConformity,
                             'NORMALS' : taglut.MeshPLCut.CPCByNormals,
                             'DISTANCE' : taglut.MeshPLCut.CPCByDistance,
                             'CONFORMITY+' : taglut.MeshPLCut.CPCByDistanceAndConformity }
    C2QParamChoice = [ ('LENGTH', 'length', 'Length of the resulting paths'),
                       ('CONFORMITY', 'conformity', 'Conformity wrt the original cylinder'),
                       ('NORMALS', 'normals', 'mean square error on normals'),
                       ('DISTANCE', 'distance', 'distance to the cylinder'),
                       ('CONFORMITY+', 'conformity+', 'conformity without absurd twists') ]
    nl_c2q_pchoice = EnumProperty(name = "Parameterization choice",
                                  default = 'CONFORMITY+',
                                  description = 'Choice of the parameterization (twist evaluation)',
                                  items = C2QParamChoice)
    CylinderChoice = [ ('CUT', 'Cut', 'Cut cylinders by quads'),
                       ('SCALAR', 'Compute scalar function', 'Draw a scalar function using a partial parameterization (z)'),
                       ('PRESERVE', 'Preserve', 'Preserve cylinders') ]
    nl_subcylinders = BoolProperty(name="Subcylinders", default=False,
                                     description="Subdivise the cylinders using a maximal ratio between perimeter and length")
    nl_subcylinders_ratio = FloatProperty(name="Subcylinder ratio",
                                          default=.5,
                                          min=0.1, max=10.,
                                          description="Maximal ratio between perimeter and length used to subdivise cylinders")

    SubdivisionMethod = [ ('CYLPARAM', 'Cyl. parameterization', 'Cylindrical parameterization'),
                          ('FLOATERPARAM', 'Floater parameterization', 'Quadrangular parameterization (Floater)'),
                          ('SHPATH', 'Shortest path', 'Shortest path approach') ]
    nl_subcylinders_method = EnumProperty(name = "Subdivision method",
                                          default = 'FLOATERPARAM',
                                          description = 'Subdivision method',
                                          items = SubdivisionMethod)

    nl_scalar_update = BoolProperty(name="Update", default=False,
                                     description="Update the input scalar function")
    nl_scalar_output = StringProperty(subtype="FILE_PATH", name="Output", description="Output updated scalar function", default=os.path.abspath(tempfile.NamedTemporaryFile(suffix=".usr", delete=False).name))
    nl_scalar_replace = BoolProperty(name="Replace", default=True,
                                     description="Replace the input scalar function file by the updated one")


    nl_cylinders = EnumProperty(name="Cylinders",
                                default = 'CUT',
                                description="Action to apply on cylinders",
                                items=CylinderChoice)
    nl_scalar_partial_twist = BoolProperty(name = "Twist direction",
                                   default=True,
                                   description="Choose the twisted direction for the scalar map generated from Z parameterization")
    nl_scalar_partial_output = StringProperty(subtype="FILE_PATH", name="Output", description="Output scalar function", default=os.path.abspath(tempfile.NamedTemporaryFile(suffix=".usr", delete=False).name))
    nl_scalar_partial_replace = BoolProperty(name="Replace", default=True,
                                     description="Replace the input scalar function file by the new one")
    nl_quad_param = BoolProperty(name="Parameterize quadrangles",
                                 default = False,
                                 description="Parameterize quadrangles produced by the cutting method")
    nl_quad_discs_by_nl = BoolProperty(name="Quads by scalar function",
                                 default = False,
                                 description="Adjust quads on quadrangles using scalar function")
    nl_display_borders = BoolProperty(name="Display borders",
                                      default=True,
                                      description="Display borders after cut")
    nl_remove_discs = BoolProperty(name="Remove discs",
                                   default = False,
                                   description="Remove flat discs")
    nl_discs_ratio = FloatProperty(name="Flat description for discs",
                                   default=5., min=0., max=50.,
                                   description="Ratio corresponding to the minimal area/perimeter value to remove disc")
    nl_color = FloatVectorProperty(name="n-loops color",
                                   default=(0.756, 0.070, 0.0),
                                   min=0., max=1.,
                                   description="Color of the n-loop paths",
                                   subtype='COLOR')
    nl_nurbs_remesh = BoolProperty(name="Remeshing by NURBS",
                                   default=False,
                                   description="Remesh each tile using ")
    nl_nurbs_remesh_distance = FloatProperty(name="Distance between points",
                                             default=.1, min=0., max=50.,
                                             description="Distance between points in the new mesh")
    nl_nurbs_degree = IntProperty(name="Degree (NURBS)",
                                  default=4, min=1, max=10,
                                  description="Degree of the NURBS")
    nl_nurbs_nbiter = IntProperty(name="Nb iterations (NURBS)",
                                  default=3, min=0, max=10,
                                  description="Number of iterations during the NURBS computation")
    nl_nurbs_nbiter_bd = IntProperty(name="Nb iterations (NURBS borders)",
                                     default=3, min=1, max=10,
                                     description="Number of iterations during the NURBS computation (borders)")



    # scalar function generation
    SF_GEN_METHOD = [ ('X', 'X axis', 'X coordinate'),
                      ('Y', 'Y axis', 'Y coordinate'),
                      ('Z', 'Z axis', 'Z coordinate'),
                      ('GEODESIC', 'Geodesic (automatic)', 'Geodesic distance using a "diameter" approach'),
                      ('DISTANCE', 'Distance (from selected points)', 'Distance from the selected point') ]
    sf_generation = EnumProperty(name = 'S.F. Generation',
                                 default = 'Z',
                                 description = "Generate a scalar function",
                                 items = SF_GEN_METHOD)
    sf_output = StringProperty(subtype="FILE_PATH", name="Output", description="Output scalar function", default=os.path.abspath(tempfile.NamedTemporaryFile(suffix=".usr", delete=False).name))
    sf_replace = BoolProperty(name="Replace", default=True,
                              description="Replace the input scalar function file by the new one")

    # scalar function smoothing
    SF_SMOOTH_STOP_METHOD = [ ('IT', 'Iteration', 'By iteration number'),
                              ('EX', 'Extrema', 'By extrema number') ]
    sf_smooth_stop = EnumProperty(name = 'S.F. smoothing stop',
                                  default = 'IT',
                                  description = 'Method to stop the smoothing process',
                                  items = SF_SMOOTH_STOP_METHOD)
    sf_smooth_it = IntProperty(name="Iterations",
                               default=3, min=1, max=50,
                               description="Number of iterations for smoothing")
    sf_smooth_nb_ext = IntProperty(name="Number of extrema",
                                   default=3, min=1, max=50,
                                   description="Number of extrema to reach after smoothing")

    sf_smooth_output = StringProperty(subtype="FILE_PATH", name="Output", description="Output scalar function", default=os.path.abspath(tempfile.NamedTemporaryFile(suffix=".usr", delete=False).name))
    sf_smooth_replace = BoolProperty(name="Replace", default=True,
                              description="Replace the input scalar function file by the new one")

    # level set
    ls_color = FloatVectorProperty(name="Isolevel color",
                                   default=(0.756, 0.070, 0.0),
                                   min=0., max=1.,
                                   description="Color of the level set",
                                   subtype='COLOR')


# define classes for registration
classes = [VIEW3D_PT_edit_tools_taglutools,
           VIEW3D_PT_tools_taglutools,
           TaglutoolsProps,
           IntegralLines,
           IntegralLinesSaddlePoints,
           ScalarValues,
           Color,
           CriticalPoints,
           NLoops,
           Borders,
           TopologicalPropertiesTopology,
           TopologicalPropertiesSelection,
           LevelSet,
           SFGeneration,
           SFSmoothing,
           PointSelection]


# registering and menu integration
def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.WindowManager.taglutools = bpy.props.PointerProperty(\
        type = TaglutoolsProps)


# unregistering and removing menus
def unregister():
    global_sf = False
    for c in classes:
        bpy.utils.unregister_class(c)
    try:
        del bpy.types.WindowManager.taglutools
    except:
        pass

if __name__ == "__main__":
    register()
