#!BPY
# -*- coding: utf-8 -*-
"""
Name: 'Solidify'
Blender: 245
Group: 'Mesh'
Tip: 'Solidify the mesh by converting edges onto cylinders, and vertices onto spheres.'
Version:  v0.1
Author: Jean-Marie Favreau
"""
import Blender
import BPyWindow
import bpy
import BPyMesh

try:
	import sys
	import traceback
	import math
	import re
	from math import *
	import sys
	import random
	import os.path
	full=True

except:
	full=False
	Blender.Draw.PupMenu('Error%t|A full python installation is required to run this script.')

if full:
	try:
		import taglut
	except:
		full = False
		Blender.Draw.PupMenu('Error%t|Taglut is required to run this script.')

__author__ = 'Jean-Marie Favreau'
__version__ = '0.1 18062010'
__url__ = ["http://jmfavreau.info/", "blender", "blenderartist"]
__email__ = ["Jean-Marie.Favreau@ens-cachan.org", "scripts"]
__bpydoc__ = """\

Solidify

Solidifies the mesh by converting edges onto cylinders, and vertices onto spheres

Not all meshes can be unfolded,

This program is free software; you can distribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation; version 2

"""

# ***** BEGIN GPL LICENSE BLOCK *****
#
# Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal / CNR IMATI-GE
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

def addCylinder(v1coords, v2coords, PREF_THICK, PREF_NBV_PERIM, vertices, faces):
  p1 = Blender.Mathutils.Vector([v1coords.x, v1coords.y, v1coords.z])
  p2 = Blender.Mathutils.Vector([v2coords.x, v2coords.y, v2coords.z])
  
  direction = p2 - p1
  length = direction.length
  
  c = Blender.Mesh.Primitives.Cylinder(PREF_NBV_PERIM, PREF_THICK, length)
  direction.normalize()
  u = direction
  uu = Blender.Mathutils.Vector([0., 0., 1.0])
  if abs(Blender.Mathutils.AngleBetweenVecs(u, uu)) > 1e-6:
    v = Blender.Mathutils.CrossVecs(u, uu).normalize()
    w = Blender.Mathutils.CrossVecs(u, v).normalize()
    A = Blender.Mathutils.Matrix([w[0] ,w[1] ,w[2], 0], [v[0], v[1], v[2], 0], [u[0], u[1], u[2], 0], [direction[0] / 2.0 * length + p1[0], direction[1] / 2.0 * length + p1[1], direction[2] / 2.0 * length + p1[2], 1])
  else:
    A = Blender.Mathutils.Matrix([1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [direction[0] / 2.0 * length + p1[0], direction[1] / 2.0 * length + p1[1], direction[2] / 2.0 * length + p1[2], 1])
  c.transform(A, True)

  idtr=len(vertices)
  for vs in c.verts:
    vertices.append([vs.co.x, vs.co.y, vs.co.z])
  for f in c.faces:
    if len(f.v) == 3:
      faces.append([idtr + f.v[0].index, idtr + f.v[1].index, idtr + f.v[2].index])
    else:
      faces.append([idtr + f.v[0].index, idtr + f.v[1].index, idtr + f.v[2].index, idtr + f.v[3].index])

def solidify(me, PREF_THICK, PREF_NBV_PERIM):
  result = bpy.data.meshes.new('solid mesh')
  vertices = []
  faces = []
  s = Blender.Mesh.Primitives.Icosphere(1, PREF_THICK)
  # create spheres
  for v in me.verts:
    idtr=len(vertices)
    for vs in s.verts:
      vertices.append([vs.co.x + v.co.x, vs.co.y + v.co.y, vs.co.z + v.co.z])
    for f in s.faces:
      faces.append([idtr + f.v[0].index, idtr + f.v[1].index, idtr + f.v[2].index])

  # create cylinders
  for e in me.edges:
    v1coords=me.verts[e.v1.index].co
    v2coords=me.verts[e.v2.index].co
    addCylinder(v1coords, v2coords, PREF_THICK, PREF_NBV_PERIM, vertices, faces)

  result.verts.extend(vertices)
  result.faces.extend(faces)
  return result


def createCopy(bobj, bmesh):
  meshObj = Blender.Object.New("Mesh")
  meshObj.loc = bobj.loc
  meshObj.rot = bobj.rot
  meshObj.size = bobj.size
  meshObj.link(bmesh)
  meshObj.name = bobj.name + ".solid"
  meshObj.layers = bobj.layers
  sce = Blender.Scene.GetCurrent()
  sce.objects.link(meshObj)
  return meshObj

def main():
	scn = bpy.data.scenes.active
	ob = scn.objects.active
	
	if not ob or ob.type != 'Mesh':
		BPyMessages.Error_NoMeshActive()
		return
	
	me = ob.getData(mesh=1)
	if me.multires:
		BPyMessages.Error_NoMeshMultiresEdit()
		return
	
	# Create the variables.
	PREF_THICK = Blender.Draw.Create(1.)
	PREF_NBV_PERIM = Blender.Draw.Create(8)
	
	pup_block = [\
	('Thick:', PREF_THICK, 0., 100., 'Thickness of cylinders.'),\
	('Number of vertices (perimeter)', PREF_NBV_PERIM, 0, 64, 'Number of vertices on the perimeter.'),\
	]
	
	if not  Blender.Draw.PupBlock('Solid edges selection', pup_block):
		return
	
	is_editmode = Blender.Window.EditMode() 
	if is_editmode: Blender.Window.EditMode(0)
	
	Blender.Window.WaitCursor(1)
	
	me = ob.getData(mesh=1)
	me2 = solidify(me, PREF_THICK.val, PREF_NBV_PERIM.val)
	finalObj = createCopy(ob, me2)

	
	
	Blender.Window.WaitCursor(0)
	if is_editmode:	Blender.Window.EditMode(1)
	
	Blender.Window.RedrawAll()

if __name__ == '__main__':
	main()