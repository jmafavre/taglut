#!BPY
# -*- coding: utf-8 -*-
"""
Name: 'Taglut'
Blender: 245
Group: 'Wizards'
Tip: 'Taglut Toolbox. Manipulations using topological and geometrical properties'
Version:  v0.1
Author: Jean-Marie Favreau
"""
import Blender
import BPyWindow
import bpy
import BPyMesh

try:
	import sys
	import traceback
	import math
	import re
	from math import *
	import sys
	import random
	import os.path


	full = True
except:
	full = False
	Blender.Draw.PupMenu('Error%t|A full python installation is required to run this script.')

if full:
	try:
		import taglut
	except:
		full = False
		Blender.Draw.PupMenu('Error%t|Taglut is required to run this script.')

__author__ = 'Jean-Marie Favreau'
__version__ = '0.2 26072009'
__url__ = ["http://jmfavreau.info/", "blender", "blenderartist"]
__email__ = ["Jean-Marie.Favreau@ens-cachan.org", "scripts"]
__bpydoc__ = """\

Taglut

Manipulates the selected mesh using topological and geometrical properties

Not all meshes can be unfolded,
Meshes must be triangulated.

This program is free software; you can distribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation; version 2

"""

# ***** BEGIN GPL LICENSE BLOCK *****
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****


# developement flag
DEVEL = True

class BTOptions:
	THRESHOLD_NONE = 1
	THRESHOLD_MEDIAN = 2
	THRESHOLD_MEAN = 3
	THRESHOLD_MANUAL = 4
	THRESHOLD_MENU = 'Threshold method %t|Select all %x1|Median %x2|Mean %x3|Manual %x4'
	G0_EXTREMA = 1
	G0_PCA = 2
	G0_VPCA = 3
	G0_MENU = 'Genus 0 method %t|Extrema %x1|PCA %x2|Volumic PCA %x3'
	PATCH_STOP_NONE = 1
	PATCH_STOP_THRESHOLD = 2
	PATCH_STOP_NUMBER = 3
	PATCH_STOP_KEEPLAST = 4
	PATCH_STOP_MENU = 'Stop method %t|Patch all %x1|Threshold %x2|Number %x3|Keep last %x4'
	DEFAULT_MAXITER_CP = 40000
	DEFAULT_MAXITER_ABF = 30
	D_EUCLIDEAN = 1
	D_CURVATURE = 2
	D_MENU = 'Distance method %t|Euclidean %x1|Curvature %x2'
	OMETHOD_MENU = 'Optimization %t|No trunc %x1|Trunc min %x2|Trunc max %x3|Trunc min/max %x4|Simple %x5|By strip %x6|By strip and pred cut %x7'
	OMETHOD_NOTRUNC = 1
	OMETHOD_TRUNCMIN = 2
	OMETHOD_TRUNCMAX = 3
	OMETHOD_TRUNCMINMAX = 4
	OMETHOD_SIMPLE = 5
	OMETHOD_BYSTRIP = 6
	OMETHOD_STRIPPREDCUT = 7
	CMETHOD_MENU = 'Cutting method %t|Erickson %x1|Erickson+ %x2|Old method %x3'
	CMETHOD_ERICKSON = 1
	CMETHOD_ERICKSONPLUS = 2
	CMETHOD_OLD = 3
	MMETHOD_MENU = 'Merging multipoints %t|No merge %x1|Simple %x2|One point %x3'
	MMETHOD_NONE = 1
	MMETHOD_SIMPLE = 2
	MMETHOD_ONEPOINT = 3
	UMETHOD_MENU = 'Unsticking method %t|No unstick %x1|Fast %x2|Preserve %x3'
	UMETHOD_NONE = 1
	UMETHOD_FAST = 2
	UMETHOD_PRESERVE = 3
	TCUT_STOP_MENU = 'Stop method %t|Cut all %x1|Number %x2|Not last %x3'
	TCUT_STOP_NONE = 1
	TCUT_STOP_NUMBER = 2
	TCUT_STOP_NOTLAST = 3
	TCUT_GLOBAL_MENU = 'Global cutting method %t|Optimal (distance) %x1|Naive (+distance) %x2|Naive %x3'
	TCUT_TOPODISTANCE = 1
	TCUT_TOPONAIVEDISTANCE = 2
	TCUT_TOPONAIVE = 3
	GMETHOD_MENU = 'Geometrical cutting method %t|From borders %x1|Simple extrema %x2'
	GMETHOD_FROMBORDERS = 1
	GMETHOD_SIMPLEEXTREMA = 2
	TEXTURE_MENU = 'Texture %t|None %x1|Grid %x2|Distortion %x3'
	TEXTURE_NONE = 1
	TEXTURE_GRID = 2
	TEXTURE_DISTORTION = 3
	OBJECT_COLOR_MENU = 'Object color %t|Grey %x1|Yellow %x2|Blue %x3|Pink %x4'
	OBJECT_COLOR_GREY = 1
	OBJECT_COLOR_YELLOW = 2
	OBJECT_COLOR_BLUE = 3
	OBJECT_COLOR_PINK = 4
	UNWRAP_MENU = 'Unfolding method %t|ABF %x1|Circle packing %x2|Circle packing (dynamic borders)%x3'
	UNWRAP_ABF = 1
	UNWRAP_CP = 2
	UNWRAP_CPA = 3
	def __init__(self):
		self.unwrap_method = Blender.Draw.Create(self.UNWRAP_ABF)
		self.unwrap_precision = Blender.Draw.Create(7)
		self.unwrap_maxiter = Blender.Draw.Create(self.DEFAULT_MAXITER_ABF)
		self.unwrap_texture = Blender.Draw.Create(self.TEXTURE_GRID)
		self.unwrap_texture_size = Blender.Draw.Create(16)
		self.do_patch_border = Blender.Draw.Create(0)
		self.do_patch = Blender.Draw.Create(0)
		self.do_topologicalcut = Blender.Draw.Create(0)
		self.do_geometricalcut = Blender.Draw.Create(0)
		self.cut_copy = Blender.Draw.Create(0)
		self.cut_draw_border = Blender.Draw.Create(0)
		self.patch_border_stop_method = Blender.Draw.Create(self.PATCH_STOP_KEEPLAST)
		self.patch_border_maxlength = Blender.Draw.Create(3.0)
		self.patch_border_maxnb = Blender.Draw.Create(1)
		self.patch_stop_method = Blender.Draw.Create(self.PATCH_STOP_NONE)
		self.patch_maxlength = Blender.Draw.Create(3.0)
		self.patch_maxnb = Blender.Draw.Create(1)
		self.tcut_global_method = Blender.Draw.Create(self.TCUT_TOPODISTANCE)
		self.tcut_naive_break = Blender.Draw.Create(0)
		self.tcut_naive_threshold = Blender.Draw.Create(10)
		self.tcut_naive_startingPoint = Blender.Draw.Create(0)
		self.tcut_naive_draw_startingPoint = Blender.Draw.Create(0)
		self.tcut_g0_method = Blender.Draw.Create(self.G0_PCA)
		self.tcut_vpca_size = Blender.Draw.Create(20)
		self.tcut_distance = Blender.Draw.Create(self.D_EUCLIDEAN)
		self.tcut_distance_ratio = Blender.Draw.Create(0.4)
		self.tcut_distance_weight = Blender.Draw.Create(0)
		self.tcut_stop_method = Blender.Draw.Create(self.TCUT_STOP_NONE)
		self.tcut_maxnb = Blender.Draw.Create(1)
		self.tcut_omethod = Blender.Draw.Create(self.OMETHOD_STRIPPREDCUT)
		self.tcut_cmethod = Blender.Draw.Create(self.CMETHOD_ERICKSONPLUS)
		self.tcut_mmethod = Blender.Draw.Create(self.MMETHOD_NONE)
		self.tcut_umethod = Blender.Draw.Create(self.UMETHOD_PRESERVE)
		self.gcut_geometrical_method = Blender.Draw.Create(self.GMETHOD_FROMBORDERS)
		self.gcut_precision_extrema = Blender.Draw.Create(0.05)
		self.gcut_precision = Blender.Draw.Create(7)
		self.gcut_maxiter = Blender.Draw.Create(self.DEFAULT_MAXITER_ABF)
		self.gcut_use_cp = Blender.Draw.Create(0)
		self.gcut_smooth_ratios = Blender.Draw.Create(0)
		self.gcut_threshold_method = Blender.Draw.Create(self.THRESHOLD_MEAN)
		self.gcut_threshold_value = Blender.Draw.Create(4)
		self.gcut_distance = Blender.Draw.Create(self.D_EUCLIDEAN)
		self.gcut_distance_ratio = Blender.Draw.Create(0.4)
		self.gcut_distance_weight = Blender.Draw.Create(0)
		self.object_color = Blender.Draw.Create(self.OBJECT_COLOR_GREY)
		self.realFileScalarFunction = ""
		self.fileScalarFunction = Blender.Draw.Create(self.realFileScalarFunction)
		self.open_scalar_value_from_file = False
		self.create_scalar_function_by_fast_marching = False
		self.hide_cutting = Blender.Draw.Create(0)
		self.hide_smalltools = Blender.Draw.Create(0)
		self.hide_scalarfunctions = Blender.Draw.Create(0)
		self.levelsets_number = Blender.Draw.Create(10)
		self.levelset_value = Blender.Draw.Create(1.)
		self.vertex_il = Blender.Draw.Create(0)
		self.adjust_by_angle = Blender.Draw.Create(0)
		self.adjust_by_angle_pl = Blender.Draw.Create(0)
		self.sourcePointFM = Blender.Draw.Create(0)
		self.twoSidesIL = Blender.Draw.Create(0)
	def setFileNameSV(self, filename):
		filename = os.path.normpath(filename)
		if os.path.exists(filename) and os.path.isfile(filename):
			self.realFileScalarFunction = filename
			self.fileScalarFunction.val = self.realFileScalarFunction
		else:
			if self.fileScalarFunction.val != self.realFileScalarFunction:
				self.fileScalarFunction.val = self.realFileScalarFunction
				name = "Error%t|Not a file"
				Blender.Draw.PupMenu(name)

	def patchBorder(self):
		return self.do_patch_border.val
	def patch(self):
		return self.do_patch.val
	def topologicalCut(self):
		return self.do_topologicalcut.val
	def geometricalCut(self):
		return self.do_geometricalcut.val
	def patchBorderStopMethod(self):
		return self.patch_border_stop_method.val
	def patchBorderMaxLength(self):
		return self.patch_border_maxlength.val
	def patchBorderMaxNb(self):
		return self.patch_border_maxnb.val
	def patchStopMethod(self):
		return self.patch_stop_method.val
	def patchMaxLength(self):
		return self.patch_maxlength.val
	def patchMaxNb(self):
		return self.patch_maxnb.val
	def g0Method(self):
		return self.tcut_g0_method.val
	def getVolumicPCASize(self):
		return self.tcut_vpca_size.val
	def useCirclePackingGCut(self):
		return self.gcut_use_cp.val
	def smoothRatio(self):
		return self.gcut_smooth_ratios.val
	def thresholdRatioMethod(self):
		return self.gcut_threshold_method.val
	def thresholdValue(self):
		return self.gcut_threshold_value.val
	def epsilonUnfoldingGCut(self):
		return 10.0 ** (-self.gcut_precision.val)
	def maxIterUnfoldingGCut(self):
		return self.gcut_maxiter.val
	def getUnfoldingMethodUV(self):
		return self.unwrap_method.val
	def epsilonUnfoldingUV(self):
		return 10.0 ** (-self.unwrap_precision.val)
	def maxIterUnfoldingUV(self):
		return self.unwrap_maxiter.val
	def testTextureUV(self):
		return self.unwrap_texture.val
	def getTextureSize(self):
		return self.unwrap_texture_size.val
	def getDistanceTCut(self):
		return self.tcut_distance.val
	def getDistanceRatioTCut(self):
		return self.tcut_distance_ratio.val
	def getDistanceWeigthTCut(self):
		return self.tcut_distance_weight.val
	def getDistanceGCut(self):
		return self.tcut_distance.val
	def getDistanceRatioGCut(self):
		return self.tcut_distance_ratio.val
	def getDistanceWeigthGCut(self):
		return self.gcut_distance_weight.val
	def oMethodTCut(self):
		return self.tcut_omethod.val
	def cMethodTCut(self):
		return self.tcut_cmethod.val
	def mMethodTCut(self):
		return self.tcut_mmethod.val
	def uMethodTCut(self):
		return self.tcut_umethod.val
	def tcutStopMethod(self):
		return self.tcut_stop_method.val
	def tcutMaxNb(self):
		return self.tcut_maxnb.val
	def tcutGlobalMethod(self):
		return self.tcut_global_method.val
	def tcutNaiveBreak(self):
		return self.tcut_naive_break.val
	def tcutNaiveThreshold(self):
		return self.tcut_naive_threshold.val
	def tcutNaiveStartingPoint(self):
		return self.tcut_naive_startingPoint.val
	def tcutNaiveDrawStartingPoint(self):
		return self.tcut_naive_draw_startingPoint.val
	def cutDrawBorder(self):
		return self.cut_draw_border.val
	def cutCopy(self):
		return self.cut_copy.val
	def gMethodGCut(self):
		return self.gcut_geometrical_method.val
	def gPrecisionSEGCut(self):
		return self.gcut_precision_extrema.val
	def objColor(self):
		return self.object_color.val
	def adjustByAngle(self):
		return self.adjust_by_angle.val
	def adjustByAnglePL(self):
		return self.adjust_by_angle_pl.val
	def twoSidesIntegralLine(self):
		return self.twoSidesIL.val

class BTTools:
	def toTaglut(self, bmesh):
		result = taglut.Mesh()
		vertices = []
		inEditMode = Blender.Window.EditMode()
		idConv = {}
		idConvFaces = {}
		if inEditMode:
			Blender.Window.EditMode(0)
		for c in bmesh.verts:
			if not inEditMode or c.sel == 1:
				idConv[c.index] = len(vertices)
				vertices.append(taglut.Coord3D(c.co.x, c.co.y, c.co.z))
		triangles = []
		for c in bmesh.faces:
			if len(c.v) == 3:
				if not inEditMode or (c.v[0].sel == 1 and c.v[1].sel == 1 and c.v[2].sel == 1):
					idConvFaces[c.index] = len(triangles)
					triangles.append(taglut.Triangle(idConv[c.v[0].index], idConv[c.v[1].index], idConv[c.v[2].index]))
			else:
				name = "Error%t|The mesh should have only triangulated faces."
				result = Blender.Draw.PupMenu(name)
				return False, False, False
		result.buildMesh(vertices, triangles)
		if result.getNbBadBPoints() != 0:
				name = "Error%t|The mesh should be a manifold."
				result = Blender.Draw.PupMenu(name)
				return False, False, False
		return result, idConv, idConvFaces
	def getMapFromCurrentMesh(self, tmesh):
		result = taglut.MeshMap(tmesh)
		bmesh, bobj = self.getCurrentMesh()
		groupNames, vWeightDict = BPyMesh.meshWeight2Dict(bmesh)
		i = 0
		if (len(groupNames) == 0):
			return result
		gname = groupNames[0]
		for weights in vWeightDict:
			if weights.has_key(gname):
				result.setValue(i, weights[gname])
			i = i + 1
		return result
	def toBlender(self, tmesh):
		result = bpy.data.meshes.new('cutMesh')
		vertices = []
		for c in tmesh.getPoints():
			vertices.append([c.getX(), c.getY(), c.getZ()])
		triangles = []
		for c in tmesh.getTriangles():
			triangles.append([c.getP1(), c.getP2(), c.getP3()])
		result.verts.extend(vertices)
		result.faces.extend(triangles)
		return result
	def getCurrentMesh(self):
		# get the current object
		try:
			bobj = Blender.Object.GetSelected()[0]
			if bobj.getType() != "Mesh":
				Blender.Draw.PupMenu("Error%t|The selected object is not a mesh.")
				return False, False
			bmesh = bobj.getData(mesh = 1)
		except:
			name = "Error%t|No object selected"
			Blender.Draw.PupMenu(name)
			return False, False
		return bmesh, bobj
	def toTaglutCurrentMesh(self):
		bmesh, bobj = self.getCurrentMesh()
		if not bmesh:
			return False, False, False, False
		# convert it into a taglut mesh
		tmesh, idConv, idConvFaces = self.toTaglut(bmesh)
		return bobj, tmesh, idConv, idConvFaces
	def createCopy(self, bobj, bmesh, method = "cut"):
		meshObj = Blender.Object.New("Mesh")
		meshObj.loc = bobj.loc
		meshObj.rot = bobj.rot
		meshObj.size = bobj.size
		meshObj.link(bmesh)
		meshObj.name = bobj.name + "." + method
		meshObj.layers = bobj.layers
		sce = Blender.Scene.GetCurrent()
		sce.objects.link(meshObj)
		return meshObj
	def createScalarFunctionFromPoint(self, center):
		bmesh, bobj = self.getCurrentMesh()
		if not bmesh:
			return False
		# convert it into a taglut mesh
		tmesh, idConv, idConvFaces = self.toTaglut(bmesh)
		if not tmesh:
			return taglut.MeshMap(m)

		dTool = taglut.FastMarching()
		mMap = taglut.MeshMap(tmesh)
		dTool.computeDistanceFromPoint(mMap, tmesh, center)

		return mMap
	def buildUVMapping(self, options):
		bmesh, bobj = self.getCurrentMesh()
		if not bmesh:
			return False
		# convert it into a taglut mesh
		tmesh, idConv, idConvFaces = self.toTaglut(bmesh)
		if not tmesh:
			return False
		# check topology
		if tmesh.getGenus() != 0 or tmesh.getNbBoundaries() != 1:
			Blender.Draw.PupMenu("Error during geometrical cut %t| Bad topological properties. You should select a topological and/or patching preprocessing")
			return False
		# compute unfolding
		mapping = taglut.Mapping2D3D(tmesh)
		if options.getUnfoldingMethodUV() == options.UNWRAP_CP:
			uM = taglut.CPMethod(mapping)
		elif  options.getUnfoldingMethodUV() == options.UNWRAP_CPA:
			uM = taglut.CPMethod(mapping, False)
		else: # == options.UNWRAP_ABF
			uM = taglut.ABFMethod(mapping)
		if uM.processUnfolding(options.epsilonUnfoldingUV(), options.maxIterUnfoldingUV()) < 0:
			Blender.Draw.PupMenu("Error during geometrical cut %t|Cannot compute the extrema (unfolding process failed. Using circle packing may be a solution)")
			return False
		uM.compute2DLocation()
		mapping.resize2DCoords(0, 0, 1, 1)
		# assert to the object the computed 2D locations
		if not bmesh.faceUV:
			bmesh.faceUV = True
		for f in bmesh.faces:
			if idConvFaces.has_key(f.index):
				for i in range(0, 3):
					f.uv[i][0] = mapping[idConv[f.v[i].index]].get2DX()
					f.uv[i][1] = mapping[idConv[f.v[i].index]].get2DY()
				f.uvSel = tuple([1, 1, 1])
			else:
				for i in range(0, 3):
					f.uv[i][0] = 0.0
					f.uv[i][1] = 0.0
				f.uvSel = tuple([0, 0, 0])
		print options.testTextureUV()
		if options.testTextureUV() != options.TEXTURE_NONE:
			mat = Blender.Material.New('TestMat')
			mat.ref = 1.0
			if options.testTextureUV() == options.TEXTURE_GRID:
				name = 'TestGrid'
			else:
				name = 'TestDist'
			tex = Blender.Texture.New(name)
			tex.setType('Image')
			sizex = sizey = 1024
			img = Blender.Image.New(name, sizex, sizey, 32)
			if options.testTextureUV() == options.TEXTURE_GRID:
				squaresize = sizex / options.getTextureSize()
				for i in range(0, sizex):
					for j in range(0, sizey):
						color = ((i / squaresize) + (j / squaresize)) % 2
						img.setPixelF(i, j, (color, color, color, 1))
			elif options.testTextureUV() == options.TEXTURE_DISTORTION:
				result = taglut.UCharCImg(sizey, sizey, 1, 3)
				result.fill(255)
				mapping.toImageDrawDistortions(result)
				for i in range(0, sizex):
					for j in range(0, sizey):
						img.setPixelF(i, j, (float(result.atXYZC(i, j, 0, 0)) / 256, float(result.atXYZC(i, j, 0, 1)) / 256, float(result.atXYZC(i, j, 0, 2)) / 256, 1))
			img.pack()
			tex.image = img
			mat.setTexture(0, tex, Blender.Texture.TexCo.UV, Blender.Texture.MapTo.COL)
			me = bobj.getData()
			me.addMaterial(mat)
			me.update()
			bpy.data.images.active = img
		return True
	def drawPaths(self, paths, bobj, isPath = False):
		mesh = bpy.data.meshes.new('chemins')
		vertices = []
		edges = []
		i = 0
		for path in paths:
			isFirst = True
			if not isPath:
				path = path.getCoordsPath()
			for p in path:
				vertices.append([p.getX(), p.getY(), p.getZ()])
				if not isFirst:
					edges.append([i - 1, i])
				else:
					isFirst = False
				i = i + 1
		mesh.verts.extend(vertices)
		mesh.edges.extend(edges)
		finalObj = self.createCopy(bobj, mesh, "level_set")
		# create a material to display the border in red
		mat = Blender.Material.New('LevelSetMat')
		mat.rgbCol = [0.756, 0.070, 0.0] #c11200
		mat.emit = 1.0
		mat.zOffset = 0.1
		mat.ref = 1.0
		mat.mode |= Blender.Material.Modes.ZTRANSP | Blender.Material.Modes.WIRE
		me = finalObj.getData()
		me.addMaterial(mat)
		me.update()
		return True
	def drawNLoops(self, paths, bobj, tmesh, isNLoop = True):
		mesh = bpy.data.meshes.new('chemins')
		vertices = []
		edges = []
		i = 0
		print paths
		for path in paths:
			isFirst = True
			if not isNLoop:
				path = path.getCoordsPath()
			for p in path:
				if isNLoop:
					vertices.append([tmesh.point(p).getX(), tmesh.point(p).getY(), tmesh.point(p).getZ()])
				else:
					vertices.append([p.getX(), p.getY(), p.getZ()])
				if not isFirst:
					edges.append([i - 1, i])
				else:
					isFirst = False
				i = i + 1
		mesh.verts.extend(vertices)
		mesh.edges.extend(edges)
		finalObj = self.createCopy(bobj, mesh, "n_loop")
		# create a material to display the border in red
		mat = Blender.Material.New('LevelSetMat')
		mat.rgbCol = [0.756, 0.070, 0.0] #c11200
		mat.emit = 1.0
		mat.zOffset = 0.1
		mat.ref = 1.0
		mat.mode |= Blender.Material.Modes.ZTRANSP | Blender.Material.Modes.WIRE
		me = finalObj.getData()
		me.addMaterial(mat)
		me.update()
		return True
	def extractBorder(self, bobj = False, tmesh = False):
		if not bobj or not tmesh:
			[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
			if not bobj or not tmesh:
				return False
		# extract border
		tmeshr = taglut.Mesh()
		bc = taglut.BorderSelector(tmeshr)
		tmesh.forEachEdge(bc)
		if tmeshr.getNbPoints() == 0:
			Blender.Draw.PupMenu("Warning %t|This mesh has no border")
			return False
		# convert the result into a blender mesh
		bmeshr = self.toBlender(tmeshr)
		# create the corresponding object, copying various properties
		finalObj = self.createCopy(bobj, bmeshr, "border")
		# create a material to display the border in red
		mat = Blender.Material.New('BorderMat')
		mat.rgbCol = [0.756, 0.070, 0.0] #c11200
		mat.emit = 1.0
		mat.zOffset = 0.1
		mat.ref = 1.0
		mat.mode |= Blender.Material.Modes.ZTRANSP | Blender.Material.Modes.WIRE
		me = finalObj.getData()
		me.addMaterial(mat)
		me.update()
		return True
	def setColor(self, options):
		[bmesh, bobj] = self.getCurrentMesh()
		if not bobj or not bmesh:
			return False
		# create a material
		mat = Blender.Material.New('TaglutMat')
		if options.objColor() == options.OBJECT_COLOR_GREY:
			mat.rgbCol = [0.854, 0.84, 0.8] #dad6cc
		elif options.objColor() == options.OBJECT_COLOR_YELLOW:
			mat.rgbCol = [0.89, 0.643, 0.106] #e3a41b
		elif options.objColor() == options.OBJECT_COLOR_BLUE:
			mat.rgbCol = [0.16, 0.394, 0.617] #29669e
		elif options.objColor() == options.OBJECT_COLOR_PINK:
			mat.rgbCol = [0.796, 0.277, 0.769] #cc47c5
		mat.ref = 1.0
		me = bobj.getData()
		me.addMaterial(mat)
		me.update()
		return True
	def topoInfos(self):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		# get infos
		infos = tmesh.getInfos().replace("\n", "|")
		if tmesh.getNbCC() == 1:
			infos += "| Genus: "+str(tmesh.getGenus())
		print infos
		name = "Topological information%t|" + infos
		result = Blender.Draw.PupMenu(name)
		return True
	def drawLevelSet(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during level set drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		levelset = scalarFunction.computeLevelSet(options.levelset_value.val)
		self.drawPaths(levelset, bobj, False)
		return True
	def drawLevelSets(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during level sets drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		minValue = scalarFunction.getMinValue()
		step = (scalarFunction.getMaxValue() - minValue) / (options.levelsets_number.val + 1)
		levelsets = tuple()
		for i in range(1, options.levelsets_number.val):
			levelsets = levelsets + scalarFunction.computeLevelSet(minValue + i * step)
		self.drawPaths(levelsets, bobj, False)
		return True
	def drawIntegralLineFromPoint(self, idPoint, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() <= idPoint:
			print tmesh.getNbPoints(), "points on the mesh, id out-of-bound", idPoint
			Blender.Draw.PupMenu("Error during integral line computation (point out-of-bound)")
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		saddlePoints = scalarFunction.getSaddlePoints()
		vfield = taglut.VectorField(scalarFunction, tmesh)

		plPath = taglut.PLPath.computeIntegralLine(taglut.PointOnEdge(tmesh.point(idPoint)), tmesh, scalarFunction, vfield)
		try:
		  print plPath.size()
		  self.drawPaths([plPath], bobj, False)
                  self.drawBalls([taglut.PointOnEdge(tmesh.point(idPoint))], [0.0, 1.0, 0.0], tmesh.getMaxDistanceXYZ() / 40, bobj)
		except:
                  print "Error, cannot build the integral line for this point"
                return True
	def drawIntegralLinesFromSaddlePoints(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		saddlePoints = scalarFunction.getSaddlePoints()
		vfield = taglut.VectorField(scalarFunction, tmesh)
		print len(saddlePoints), "saddle points found"
		for s in saddlePoints:
			print "check for paths from", s[0]
			plPaths = tuple()
			try:
				plPaths = taglut.PLPath.getIntegralLinesFromSaddlePoint(tmesh.point(s[0]), tmesh, scalarFunction, vfield, -1., options.twoSidesIntegralLine())
				print len(plPaths)
				self.drawPaths(plPaths, bobj, False)
			except e:
				print e
				print "Error, cannot build the integral line for this saddle point"
		return True
	def drawPre3LoopsFromSaddlePoints(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		saddlePoints = scalarFunction.getSaddlePoints()
		vfield = taglut.VectorField(scalarFunction, tmesh)
		print len(saddlePoints), "saddle points found"
		for s in saddlePoints:
			print "check for pre-n-loops from", s[0]
			try:
				plPaths = taglut.NLoopFactory.getPre3LoopFromSaddlePoint(tmesh.point(s[0]), tmesh, scalarFunction, vfield)
				self.drawPaths(plPaths.getLines(scalarFunction), bobj, True)
			except:
				print "Error, cannot build the integral line for this saddle point"
		return True
	def drawPreNLoopsFromSaddlePoints(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		vfield = taglut.VectorField(scalarFunction, tmesh)
		pnl = taglut.NLoopFactory.getPreNLoopsFromSaddlePoints(tmesh, scalarFunction, vfield)
		for p in pnl:
			print "Found an n-loop. Arity:", p.arity()
			self.drawPaths(p.getLines(scalarFunction), bobj, True)
		return True
	def drawNLoopsFromSaddlePoints(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		vfield = taglut.VectorField(scalarFunction, tmesh)
		nl = taglut.NLoopFactory.getNLoopsFromSaddlePoints(tmesh, scalarFunction, vfield, options.adjustByAngle())
		for p in nl:
			print "Found an n-loop. Arity:", p.getNbPaths()
			self.drawNLoops(p.getPaths(), bobj, tmesh)
		return True
	def drawPLNLoopsFromSaddlePoints(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		vfield = taglut.VectorField(scalarFunction, tmesh)
		nl = taglut.NLoopFactory.getPLNLoopsFromSaddlePoints(tmesh, scalarFunction, vfield, options.adjustByAnglePL())
		for p in nl:
			print "Found an n-loop. Arity:", p.getNbPaths()
			self.drawNLoops(p.getPaths(), bobj, tmesh, False)
		return True
	def drawGeometrySaddlePoints(self, options):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		points = []
		for c in tmesh.getPoints():
			if tmesh.isSaddlePoint(c.getId()):
				points.append(c)
		self.drawBalls(points, [1.0, 0.0, 0.0], tmesh.getMaxDistanceXYZ() / 40, bobj)
		return True
	def drawSaddlePoints(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during saddle point drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		scalarFunction.removeFlatEdges()
		if scalarFunction.getNbFlatEdges() != 0:
			Blender.Draw.PupMenu("Warning: there is flat edges in the mesh. Saddle point may not be recognized.")
		saddlePoints = scalarFunction.getSaddlePoints()
		print len(saddlePoints), "saddle points found"
		points = []
		for s in saddlePoints:
			points.append(tmesh.point(s[0]))
		self.drawBalls(points, [1.0, 0.0, 0.0], tmesh.getMaxDistanceXYZ() / 40, bobj)
		return True
	def drawExtrema(self, options, scalarFunction):
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if not bobj or not tmesh:
			return False
		if tmesh.getNbPoints() != scalarFunction.getNbValues():
			print tmesh.getNbPoints(), "points on the mesh, but", scalarFunction.getNbValues(), "scalar values"
			Blender.Draw.PupMenu("Error during extrema drawing %t|Wrong number of points in the selected mesh")
			return False
		scalarFunction.setMesh(tmesh)
		extremaPoints = scalarFunction.getLocalExtrema()
		print len(extremaPoints), "extrema found"
		points = []
		for e in extremaPoints:
			points.append(tmesh.point(e))
		self.drawBalls(points, [0.0, 1.0, 0.0], tmesh.getMaxDistanceXYZ() / 40, bobj)
		return True
	def drawBalls(self, points, color, size, bobj):
		for p in points:
			ball = Blender.Mesh.Primitives.Icosphere(2, size)
			# adding the corresponding object
			newObj = self.createCopy(bobj, ball, "point")
			for v in ball.verts:
				v.co[0] += p.getX()
				v.co[1] += p.getY()
				v.co[2] += p.getZ()
			# create a specific material
			mat = Blender.Material.New('BallMat')
			mat.ref = 1.0
			mat.rgbCol = color
			mat.emit = 1.0
			# mat.mode |= Blender.Material.Modes.ZTRANSP
			# mat.alpha = 0.5
			me = newObj.getData()
			me.addMaterial(mat)
			me.update()
	def cutMesh(self, options):
		print "Starting cutting the current mesh"
		inEditMode = Blender.Window.EditMode()
		[bobj, tmesh, idConv, idConvFaces] = self.toTaglutCurrentMesh()
		if options.getDistanceWeigthTCut() or options.getDistanceGCut():
			dMap = self.getMapFromCurrentMesh(tmesh)
		if not bobj or not tmesh:
			return False
		# first apply the patch method if needed
		name = ""
		drawBall = False
		startingPoint = 0
		if options.patchBorder():
			print " Patch border step"
			length = -1.0
			if options.patchBorderStopMethod() == options.PATCH_STOP_THRESHOLD:
				length = options.patchBorderMaxLength()
			nbMax = -1
			if options.patchBorderStopMethod() == options.PATCH_STOP_NUMBER:
				nbMax = options.patchBorderMaxNb()
			if options.patchBorderStopMethod() == options.PATCH_STOP_KEEPLAST:
				nbMax = tmesh.getNbBoundaries() - 1
			mPatch = taglut.MeshPatches(tmesh, length)
			mPatch.closeBoundaries(nbMax)
			name += ".bpatch"
		if options.patch():
			print " Patch step"
			length = -1.0
			if options.patchStopMethod() == options.PATCH_STOP_THRESHOLD:
				length = options.patchMaxLength()
			nbMax = -1
			if options.patchStopMethod() == options.PATCH_STOP_NUMBER:
				nbMax = options.patchMaxNb()
			if options.patchStopMethod() == options.PATCH_STOP_KEEPLAST:
				nbMax = tmesh.getGenus() - 1
			mPatch = taglut.MeshPatches(tmesh, length)
			mPatch.patchMeshInLoops(nbMax)
			name += ".patch"
		# then apply the topological cutting if needed
		if options.topologicalCut():
			print " Topological step"
			lFactory = taglut.LengthFactory()
			if options.getDistanceTCut() == options.D_EUCLIDEAN:
				lFactory.setLength("euclidean")
				print "  Euclidean distance"
			elif options.getDistanceTCut() == options.D_CURVATURE:
				lFactory.setLength("linear-curvature")
				lFactory.setRatio(options.getDistanceRatioTCut())
				print "  Curvature distance (ratio: ", options.getDistanceRatioTCut(), ")"
			if options.getDistanceWeigthTCut():
				meshCut = taglut.MeshCut(tmesh, taglut.LengthMap(dMap, lFactory.getLength(tmesh)))
			else:
				meshCut = taglut.MeshCut(tmesh, lFactory.getLength(tmesh))
			thresholdNaive = 0
			if options.tcutNaiveBreak():
				thresholdNaive = options.tcutNaiveThreshold()
			if options.tcutGlobalMethod() != options.TCUT_TOPODISTANCE:
				if options.tcutNaiveDrawStartingPoint():
					drawBall = True
				if options.tcutNaiveStartingPoint() >= tmesh.getNbPoints():
					Blender.Draw.PupMenu("Warning %t| Wrong starting point. Selecting the first point.")
				else:
					startingPoint = options.tcutNaiveStartingPoint()

			if options.tcutGlobalMethod() == options.TCUT_TOPONAIVE:
				meshCut.cutMeshUsingSimpleCut(startingPoint, thresholdNaive)
				name += ".ntop"
			elif options.tcutGlobalMethod() == options.TCUT_TOPONAIVEDISTANCE:
				meshCut.cutMeshUsingSimpleCutDistance(startingPoint, thresholdNaive)
				name += ".ndtop"
			else:
				if tmesh.getGenus() == 0 and tmesh.getNbBoundaries() == 0 and options.g0Method() != options.G0_EXTREMA:
					if options.g0Method() == options.G0_VPCA:
						meshCut.cutMeshUsingVolumicPCA(options.getVolumicPCASize())
						name += ".vpca"
					else:
						meshCut.cutMeshUsingPCA()
						name += ".pca"
				else:
					if options.cMethodTCut() == options.CMETHOD_ERICKSON:
						cMethod = taglut.MeshCut.CMErickson
					elif options.cMethodTCut() == options.CMETHOD_ERICKSONPLUS:
						cMethod = taglut.MeshCut.CMEricksonPlus
					elif options.cMethodTCut() == options.CMETHOD_OLD:
						cMethod = taglut.MeshCut.CMOld
					if options.oMethodTCut() == options.OMETHOD_NOTRUNC:
						oMethod = taglut.MeshPathes.OECnoTrunc
					elif options.oMethodTCut() == options.OMETHOD_TRUNCMIN:
						oMethod = taglut.MeshPathes.OECTruncMin
					elif options.oMethodTCut() == options.OMETHOD_TRUNCMAX:
						oMethod = taglut.MeshPathes.OECTruncMax
					elif options.oMethodTCut() == options.OMETHOD_TRUNCMINMAX:
						oMethod = taglut.MeshPathes.OECTruncMinMax
					elif options.oMethodTCut() == options.OMETHOD_SIMPLE:
						oMethod = taglut.MeshPathes.OECsimple
					elif options.oMethodTCut() == options.OMETHOD_BYSTRIP:
						oMethod = taglut.MeshPathes.OECbyStrip
					elif options.oMethodTCut() == options.OMETHOD_STRIPPREDCUT:
						oMethod = taglut.MeshPathes.OECbyStripAndPredCut
					if options.mMethodTCut() == options.MMETHOD_NONE:
						mergeMultipoints = taglut.MeshCut.MMPnone
					elif options.mMethodTCut() == options.MMETHOD_SIMPLE:
						mergeMultipoints = taglut.MeshCut.MMPsimple
					elif options.mMethodTCut() == options.MMETHOD_ONEPOINT:
						mergeMultipoints = taglut.MeshCut.MMPonePoint
					if options.uMethodTCut() == options.UMETHOD_NONE:
						uMethod = taglut.MeshManipulator.UMNone
					elif options.uMethodTCut() == options.UMETHOD_FAST:
						uMethod = taglut.MeshManipulator.UMFast
					elif options.uMethodTCut() == options.UMETHOD_PRESERVE:
						uMethod = taglut.MeshManipulator.UMPreserveMesh
					number = -1
					if options.tcutStopMethod() == options.TCUT_STOP_NUMBER:
						number = options.tcutMaxNb()
					meshCut.cutMeshUsingMinimalCut(number, cMethod, mergeMultipoints, uMethod, oMethod)
					name += ".top"
		# then apply the geometrical cut
		if options.geometricalCut():
			if options.gMethodGCut() == options.GMETHOD_FROMBORDERS:
				print " Geometrical step (from borders)"
				if tmesh.getGenus() != 0 or tmesh.getNbBoundaries() != 1:
					Blender.Draw.PupMenu("Error during geometrical cut %t| Bad topological properties. You should select a topological and/or patching preprocessing")
					return False
				mapping = taglut.Mapping2D3D(tmesh)
				if options.useCirclePackingGCut():
					uM = taglut.CPMethod(mapping)
				else:
					uM = taglut.ABFMethod(mapping)
				if uM.processUnfolding(options.epsilonUnfoldingGCut(), options.maxIterUnfoldingGCut()) < 0:
					Blender.Draw.PupMenu("Error during UV mapping %t|Unfolding process failed. Using circle packing may be a solution")
					return False
				uM.compute2DLocation()
				threshold = -1
				if options.thresholdRatioMethod() == options.THRESHOLD_MANUAL:
					threshold = options.thresholdValue()
				mapping.computeExtremaFromBorder(taglut.LengthEdgeMappingRatio(mapping), options.smoothRatio(), options.thresholdRatioMethod() == options.THRESHOLD_MEAN, options.thresholdRatioMethod() == options.THRESHOLD_MEDIAN, threshold)
				lFactory = taglut.LengthFactory()
				if options.getDistanceGCut() == options.D_EUCLIDEAN:
					print "using euclidean"
					lFactory.setLength("euclidean")
				elif options.getDistanceGCut() == options.D_CURVATURE:
					print "using curvature"
					lFactory.setLength("linear-curvature")
					lFactory.setRatio(options.getDistanceRatioGCut())
				if options.getDistanceWeigthGCut():
					print "with weights"
					meshCut = taglut.MeshCut(tmesh, taglut.LengthMap(dMap, lFactory.getLength(tmesh)))
				else:
					meshCut = taglut.MeshCut(tmesh, lFactory.getLength(tmesh))
				if len(mapping.getExtrema()) != 0:
					print "Extrema: " + str(len(mapping.getExtrema()))
					Blender.Draw.PupMenu("Info %t|Number of extrema: " + str(len(mapping.getExtrema())))
					meshCut.buildCutFromPoints(mapping.getExtrema())
					meshCut.cutMesh()
				else:
					Blender.Draw.PupMenu("Info %t|No extremum found.")
				name += ".geom"
			elif options.gMethodGCut() == options.GMETHOD_SIMPLEEXTREMA:
				print " Geometrical step (simple extrema)"
				extrema = tmesh.findExtrema(options.gPrecisionSEGCut())
				print "Number of extrema: ", len(extrema)
                		meshCut = taglut.MeshCut(tmesh)
                		meshCut.buildCutFromPoints(extrema)
				meshCut.cutMesh()
				name += ".sgeom"
		if drawBall:
			# create a ball located on the starting point
			center = tmesh.point(startingPoint)
			radius = tmesh.getMeanEdges(startingPoint) / 2.0
			if radius == 0.0:
				print "Radius of the selected point not defined"
				radius = 1.0
			ball = Blender.Mesh.Primitives.Icosphere(2, radius)
			# adding the corresponding object
			newObj = self.createCopy(bobj, ball, "center")
			newObj.setLocation(newObj.loc[0] + center.getX(), newObj.loc[1] + center.getY(), newObj.loc[2] + center.getZ())
			# create a specific material
			mat = Blender.Material.New('BallMat')
			mat.ref = 1.0
			mat.rgbCol = [1.0, 0.0, 0.0]
			mat.emit = 1.0
			mat.mode |= Blender.Material.Modes.ZTRANSP
			mat.alpha = 0.5
			me = newObj.getData()
			me.addMaterial(mat)
			me.update()
		if name != "":
			# convert the result into a blender mesh
			bmeshr = self.toBlender(tmesh)
			# create the corresponding object, copying various properties
			if options.cutCopy():
				self.createCopy(bobj, bmeshr, name[1:])
			else:
				if inEditMode:
					Blender.Draw.PupMenu("Warning%t| Object were in selection mode. Cannot modify it. Creating a new one.")
					self.createCopy(bobj, bmeshr, name[1:])
				else:
					bobj.link(bmeshr)
			if options.cutDrawBorder():
				self.extractBorder(bobj, tmesh)
			return True
		else:
			Blender.Draw.PupMenu("Error during cut%t| No method selected.")
			return False


class GUI:
	EVENT_EXIT = 1
	EVENT_ABOUT = 2
	EVENT_RUN_CUT = 3
	EVENT_INFO = 4
	EVENT_EXTRACT_BORDER = 5
	EVENT_UPDATE_GUI = 6
	EVENT_CHANGE_UMETHOD = 7
	EVENT_UVMAPPING = 8
	EVENT_CHANGE_UMETHOD_UV = 9
	EVENT_SET_COLOR = 10
	EVENT_FILE_NAME_SV = 11
	EVENT_SELECT_FILE_SV = 12
	EVENT_LOAD_SV_FROM_FILE = 13
	EVENT_LOAD_FILE_SV = 14
	EVENT_CANCEL_FILE_SV = 15
	EVENT_CLOSE_SV = 16
	EVENT_DRAW_LEVELSET = 17
	EVENT_DRAW_LEVELSETS = 18
	EVENT_DRAW_EXTREMA = 19
	EVENT_DRAW_SADDLE_POINTS = 20
	EVENT_DRAW_INTEGRAL_LINES_FROM_SADDLE_POINTS = 21
	EVENT_DRAW_PRE_3LOOPS_FROM_SADDLE_POINTS = 22
	EVENT_DRAW_PRE_NLOOPS_FROM_SADDLE_POINTS = 23
	EVENT_DRAW_NLOOPS_FROM_SADDLE_POINTS = 24
	EVENT_CREATE_SV_FROM_FM = 25
	EVENT_CANCEL_FM_SV = 26
	EVENT_CREATE_SV_FM = 27
	EVENT_DRAW_SADDLE_POINTS_GEOM = 28
	EVENT_DRAW_PLNLOOPS_FROM_SADDLE_POINTS = 29
	EVENT_DRAW_INTEGRAL_LINE = 30

	I_SIMPLE = 1
	I_ADVANCED = 2
	I_FULL = 3
	I_MENU = 'Interface %t|Simple %x1|Advanced %x2|Full %x3'
	def __init__(self):
		if DEVEL:
			self.advanced = Blender.Draw.Create(self.I_FULL)
		else:
			self.advanced = Blender.Draw.Create(self.I_SIMPLE)
		self.message = " "
		self.btc = BTTools()
		self.options = BTOptions()
		self.scalarFunction = None

	def drawBorder(self, X1, Y1, X2, Y2): # X1,Y1 = Top Left X2,Y2 = Bottom Right
		INDENT = 3
		Blender.BGL.glColor3f(1.0, 1.0, 1.0)
		Blender.BGL.glBegin(Blender.BGL.GL_LINES)

		Blender.BGL.glVertex2i(X1 - INDENT, Y1 + INDENT)     # top line
		Blender.BGL.glVertex2i(X2 + INDENT, Y1 + INDENT)

		Blender.BGL.glVertex2i(X1 - INDENT, Y1 + INDENT)     # left line
		Blender.BGL.glVertex2i(X1 - INDENT, Y2 - INDENT)
		Blender.BGL.glEnd()

		Blender.BGL.glColor3f(0.5, 0.5, 0.5)
		Blender.BGL.glBegin(Blender.BGL.GL_LINES)
		Blender.BGL.glVertex2i(X2 + INDENT, Y1 + INDENT)     # right line
		Blender.BGL.glVertex2i(X2 + INDENT, Y2 - INDENT)

		Blender.BGL.glVertex2i(X1 - INDENT, Y2 - INDENT)     # bottom line
		Blender.BGL.glVertex2i(X2 + INDENT, Y2 - INDENT)
		Blender.BGL.glEnd()

	def makeStandardGUI(self):
		Blender.Draw.Register(self.draw, self.keyOrMouseEvent, self.buttonEvent)
	def keyOrMouseEvent(self, evt, val):
		pass
	def about(self):
		self.say("About...")
		block = []
		name = "About%t|Taglut (topological and geometrical library - a usefull toolkit)"
		result = Blender.Draw.PupMenu(name)
		self.say(" ")
	def buttonEvent(self, evt):
		if evt == self.EVENT_EXIT:
			Blender.Draw.Exit()
		elif evt == self.EVENT_ABOUT:
			self.about()
		elif evt == self.EVENT_RUN_CUT:
			self.say("Cutting the current mesh...")
			self.btc.cutMesh(self.options)
		elif evt == self.EVENT_INFO:
			self.say("Topological information of the current mesh...")
			self.btc.topoInfos()
		elif evt == self.EVENT_EXTRACT_BORDER:
			self.say("Extract border of the current mesh...")
			self.btc.extractBorder()
		elif evt == self.EVENT_SET_COLOR:
			self.say("Set texture color...")
			self.btc.setColor(self.options)
		elif evt == self.EVENT_UVMAPPING:
			self.say("Unwrapping the current mesh...")
			self.btc.buildUVMapping(self.options)
			Blender.Redraw()
		elif evt == self.EVENT_CHANGE_UMETHOD:
			if self.options.gcut_use_cp.val:
				self.options.gcut_maxiter.val = self.options.DEFAULT_MAXITER_CP
			else:
				self.options.gcut_maxiter.val = self.options.DEFAULT_MAXITER_ABF
			self.draw()
		elif evt == self.EVENT_CHANGE_UMETHOD_UV:
			if self.options.getUnfoldingMethodUV() == self.options.UNWRAP_ABF:
				self.options.unwrap_maxiter.val = self.options.DEFAULT_MAXITER_ABF
			else:
				self.options.unwrap_maxiter.val = self.options.DEFAULT_MAXITER_CP
			self.draw()
		elif evt == self.EVENT_UPDATE_GUI:
			self.draw()
		elif evt == self.EVENT_LOAD_SV_FROM_FILE:
			self.options.open_scalar_value_from_file = True
			self.draw()
		elif evt == self.EVENT_CANCEL_FILE_SV:
			self.options.open_scalar_value_from_file = False
			self.draw()
		elif evt == self.EVENT_CREATE_SV_FROM_FM:
			self.options.create_scalar_function_by_fast_marching = True
			self.draw()
		elif evt == self.EVENT_CANCEL_FM_SV:
			self.options.create_scalar_function_by_fast_marching = False
			self.draw()
		elif evt == self.EVENT_SELECT_FILE_SV:
			name = self.options.realFileScalarFunction
			if name == "":
			 	name = "*.usr"
			Blender.Window.FileSelector(self.options.setFileNameSV, "Select scalar function file", name)
			Blender.Draw.Draw()
		elif evt == self.EVENT_LOAD_FILE_SV:
			try:
			 	self.scalarFunction = taglut.MeshMap(self.options.realFileScalarFunction)
				if self.scalarFunction.getNbValues() <= 1:
					Blender.Draw.PupMenu("Error%t| Not anough values.")
					self.scalarFunction = None
				else:
					print "Scalar function loaded:", self.scalarFunction.getNbValues(), "values"
					self.options.levelset_value.val = (self.scalarFunction.getMaxValue() + self.scalarFunction.getMinValue()) / 2
			except:
			 	Blender.Draw.PupMenu("Error%t| Error loading file.")
				self.scalarFunction = None
			self.options.open_scalar_value_from_file = False
		elif evt == self.EVENT_CREATE_SV_FM:
			try:
				self.say("Create scalar function using fast marching...")
				self.scalarFunction = self.btc.createScalarFunctionFromPoint(self.options.sourcePointFM.val)
				if self.scalarFunction.getNbValues() <= 1:
					Blender.Draw.PupMenu("Error%t| Not anough values.")
					self.scalarFunction = None
				else:
					print "Scalar function created:", self.scalarFunction.getNbValues(), "values"
					self.options.levelset_value.val = (self.scalarFunction.getMaxValue() + self.scalarFunction.getMinValue()) / 2
			except:
			 	Blender.Draw.PupMenu("Error%t| Error creating scalar function.")
				self.scalarFunction = None
			self.options.create_scalar_function_by_fast_marching = False
		elif evt == self.EVENT_CLOSE_SV:
			self.scalarFunction = None
		elif evt == self.EVENT_DRAW_LEVELSETS:
			self.say("Drawing levelsets...")
			self.btc.drawLevelSets(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_LEVELSET:
			self.say("Drawing levelset...")
			self.btc.drawLevelSet(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_SADDLE_POINTS:
			self.say("Drawing saddle points")
			self.btc.drawSaddlePoints(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_SADDLE_POINTS_GEOM:
			self.say("Drawing saddle points")
			self.btc.drawGeometrySaddlePoints(self.options)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_EXTREMA:
			self.say("Drawing extrema")
			self.btc.drawExtrema(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_INTEGRAL_LINES_FROM_SADDLE_POINTS:
			self.say("Drawing integral lines from saddle points")
			self.btc.drawIntegralLinesFromSaddlePoints(self.options, self.scalarFunction)
			Blender.Redraw()

		elif evt == self.EVENT_DRAW_PRE_3LOOPS_FROM_SADDLE_POINTS:
			self.say("Drawing pre-n-loops from saddle points")
			self.btc.drawPre3LoopsFromSaddlePoints(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_PRE_NLOOPS_FROM_SADDLE_POINTS:
			self.say("Drawing pre-n-loops from saddle points")
			self.btc.drawPreNLoopsFromSaddlePoints(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_NLOOPS_FROM_SADDLE_POINTS:
			self.say("Drawing n-loops from saddle points")
			self.btc.drawNLoopsFromSaddlePoints(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_PLNLOOPS_FROM_SADDLE_POINTS:
			self.say("Drawing piecewise-linear n-loops from saddle points")
			self.btc.drawPLNLoopsFromSaddlePoints(self.options, self.scalarFunction)
			Blender.Redraw()
		elif evt == self.EVENT_DRAW_INTEGRAL_LINE:
			self.say("Drawing integral line from point")
			self.btc.drawIntegralLineFromPoint(self.options.vertex_il.val, self.scalarFunction)
			Blender.Redraw()
		self.say(" ")
	def draw(self):
		margin = 12
		margin_button = 4
		rect = BPyWindow.spaceRect()
		height_button = 17
		width_button = int((rect[2] - 3 * margin) / 3.0)
		if width_button > 192: width_button = 192
		xcut = 2 * margin + width_button

		cy = rect[3] - height_button - margin

		# draw the header lines
		if self.options.hide_smalltools.val == 1:
			self.options.hide_smalltools = Blender.Draw.Toggle("-", self.EVENT_UPDATE_GUI, margin, cy, height_button, height_button, self.options.hide_smalltools.val, 'Hide small tools interface.')
			Blender.Draw.Label("Small tools", 2 * margin + height_button, cy, width_button, height_button)
		else:
			self.options.hide_smalltools = Blender.Draw.Toggle("Small tools...", self.EVENT_UPDATE_GUI, margin, cy, width_button, height_button, self.options.hide_smalltools.val, 'Show small tools interface.')

		if self.options.hide_cutting.val == 1:
			Blender.Draw.Label("Cutting", xcut + margin + height_button, cy, width_button, height_button)
			self.options.hide_cutting = Blender.Draw.Toggle("-", self.EVENT_UPDATE_GUI, xcut, cy, height_button, height_button, self.options.hide_cutting.val, 'Hide cutting interface.')
		else:
			self.options.hide_cutting = Blender.Draw.Toggle("Cutting...", self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.hide_cutting.val, 'Show cutting interface.')

		cy -= (height_button + margin_button)
		# draw the info buttons
		cytmp = cy

		ybegin = cy + height_button + height_button + margin_button
		if self.options.hide_smalltools.val == 1:
			Blender.Draw.PushButton('UV mapping', self.EVENT_UVMAPPING, margin, cy, width_button, height_button, 'Build the UV mapping of the current mesh, adding an UV test grid.')
			cy -= height_button
			self.options.unwrap_method = Blender.Draw.Menu(self.options.UNWRAP_MENU, self.EVENT_CHANGE_UMETHOD_UV, margin, cy, width_button, height_button, self.options.unwrap_method.val, 'Select the unfolding method to build the UV mapping.')
			if self.advanced.val > self.I_SIMPLE:
				cy -= height_button
				self.options.unwrap_precision = Blender.Draw.Number('Precision: ', self.EVENT_UPDATE_GUI, margin, cy, width_button, height_button, self.options.unwrap_precision.val, 0, 15, 'Maximum iteration of the unfolding method (epsilon = 10^(-n), n = precision)')
				cy -= height_button
				self.options.unwrap_maxiter = Blender.Draw.Number('Maximum iteration: ', self.EVENT_UPDATE_GUI, margin, cy, width_button, height_button, self.options.unwrap_maxiter.val, 0, 100000, 'Maximum iteration of the unfolding method')
			cy -= height_button
			self.options.unwrap_texture = Blender.Draw.Menu(self.options.TEXTURE_MENU, self.EVENT_UPDATE_GUI, margin, cy, width_button, height_button, self.options.unwrap_texture.val, "Method used to stop the patching process.")
			if self.options.unwrap_texture.val == self.options.TEXTURE_GRID and self.advanced.val > self.I_SIMPLE:
				cy -= height_button
				self.options.unwrap_texture_size = Blender.Draw.Number('Grid size: ', self.EVENT_UPDATE_GUI, margin, cy, width_button, height_button, self.options.unwrap_texture_size.val, 0, 128, 'Number of squares on the x-axis of the grid.')

			cy -= (height_button + margin_button)
			Blender.Draw.PushButton('Extract border', self.EVENT_EXTRACT_BORDER, margin, cy, width_button, height_button, 'Extract the border of the current mesh, creating a new object')

			if self.advanced.val == self.I_FULL:
				cy -= (height_button + margin_button)
				Blender.Draw.PushButton('Set texture', self.EVENT_SET_COLOR, margin, cy, width_button, height_button, 'Set a predefined color to the current objet creating a dedicated texture')
				cy -= height_button
				self.options.object_color = Blender.Draw.Menu(self.options.OBJECT_COLOR_MENU, self.EVENT_UPDATE_GUI, margin, cy, width_button, height_button, self.options.object_color.val, "Color to be applied.")

			cy -= (height_button + margin_button)
			Blender.Draw.PushButton('Information', self.EVENT_INFO, margin, cy, width_button, height_button, 'Display some information about the current mesh')
			cy -= (height_button + margin_button)
			Blender.Draw.PushButton('Draw saddle points', self.EVENT_DRAW_SADDLE_POINTS_GEOM, margin, cy, width_button, height_button, 'Draw the geometric saddle points')
			cy -= height_button + margin
		else:
			cy -= margin
		self.drawBorder(margin, ybegin, width_button + margin, cy + height_button + margin)
		#self.drawBorder(xcut, ybegin, xcut + width_button, cy + height_button + margin)

		cyleft = cy
		cy = cytmp

		# draw the cut block

		ybegin = cy + height_button + height_button + margin_button
		largeSize = False
		if self.options.hide_cutting.val == 1:

			Blender.Draw.PushButton('Apply cut', self.EVENT_RUN_CUT, xcut, cy, width_button, height_button, 'Apply the cutting method described below')
			if self.advanced.val > self.I_SIMPLE:
				cy -= height_button
				self.options.cut_copy = Blender.Draw.Toggle('Create copy', self.EVENT_UPDATE_GUI, xcut, cy, width_button / 2, height_button, self.options.cut_copy.val, 'Create a copy of the current object rather than modifying it.')
				self.options.cut_draw_border = Blender.Draw.Toggle('Draw border', self.EVENT_UPDATE_GUI, xcut + width_button / 2, cy, width_button / 2, height_button, self.options.cut_draw_border.val, 'Create an object describing the border of the created mesh.')
			cy -= height_button
			self.options.do_patch_border = Blender.Draw.Toggle('Patch borders', self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.do_patch_border.val, 'Patch the borders by adding small discs.')
			if self.options.do_patch_border.val:
				largeSize = True
				self.options.patch_border_stop_method = Blender.Draw.Menu(self.options.PATCH_STOP_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.patch_border_stop_method.val, "Method used to stop the patching process.")
				if self.options.patch_border_stop_method.val == self.options.PATCH_STOP_THRESHOLD:
					cy -= height_button
					self.options.patch_border_maxlength = Blender.Draw.Number('Maximum length: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.patch_border_maxlength.val, 0, 1000, 'Maximum length of the borders to be patched')
				if self.options.patch_border_stop_method.val == self.options.PATCH_STOP_NUMBER:
					cy -= height_button
					self.options.patch_border_maxnb = Blender.Draw.Number('Maximum number: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.patch_border_maxnb.val, 0, 1000, 'Maximum number of borders to be patched')

			cy -= height_button
			self.options.do_patch = Blender.Draw.Toggle('Patch', self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.do_patch.val, 'Patch the smallest loops by cutting and adding small discs.')
			if self.options.do_patch.val:
				largeSize = True
				self.options.patch_stop_method = Blender.Draw.Menu(self.options.PATCH_STOP_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.patch_stop_method.val, "Method used to stop the patching process.")
				if self.options.patch_stop_method.val == self.options.PATCH_STOP_THRESHOLD:
					cy -= height_button
					self.options.patch_maxlength = Blender.Draw.Number('Maximum length: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.patch_maxlength.val, 0, 1000, 'Maximum length of the loops to be patched')
				if self.options.patch_stop_method.val == self.options.PATCH_STOP_NUMBER:
					cy -= height_button
					self.options.patch_maxnb = Blender.Draw.Number('Maximum number: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.patch_maxnb.val, 0, 1000, 'Maximum number of loops to be patched')


			cy -= height_button
			self.options.do_topologicalcut = Blender.Draw.Toggle('Topological cut', self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.do_topologicalcut.val, 'Cut using a topological method.')
			if self.options.do_topologicalcut.val:
				largeSize = True
				self.options.tcut_global_method = Blender.Draw.Menu(self.options.TCUT_GLOBAL_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_global_method.val, "Global cutting method (only topological, or with distance information).")
				if self.options.tcut_global_method.val == self.options.TCUT_TOPONAIVE or self.options.tcut_global_method.val == self.options.TCUT_TOPONAIVEDISTANCE:
					if self.advanced.val > self.I_ADVANCED:
						cy -= height_button
						self.options.tcut_naive_startingPoint = Blender.Draw.Number('Starting point', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_naive_startingPoint.val, 0, 40000, "Starting point of the growing process.")
						cy -= height_button
						self.options.tcut_naive_draw_startingPoint = Blender.Draw.Toggle('Draw starting point', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_naive_draw_startingPoint.val, "Build a small 3D object to localize the starting point.")
					if self.advanced.val > self.I_SIMPLE:
						cy -= height_button
						self.options.tcut_naive_break = Blender.Draw.Toggle('Break', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_naive_break.val, "Stop the growing process at a given threshold.")
						if self.options.tcut_naive_break.val:
							cy -= height_button
							self.options.tcut_naive_threshold = Blender.Draw.Number('Break threshold: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_naive_threshold.val, 0, 40000, 'Number of steps of the growing process.')
				if self.options.tcut_global_method.val == self.options.TCUT_TOPODISTANCE:
					cy -= height_button
					self.options.tcut_g0_method = Blender.Draw.Menu(self.options.G0_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_g0_method.val, "Only for genus 0 surfaces. Cutting method.")
					if self.options.tcut_g0_method.val == 3:
						cy -= height_button
						self.options.tcut_vpca_size = Blender.Draw.Number('Number of voxels: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_vpca_size.val, 0, 40, 'Number of voxels used for the volumic PCA method')
				if self.advanced.val > self.I_SIMPLE and (self.options.tcut_global_method.val == self.options.TCUT_TOPODISTANCE or self.options.tcut_global_method.val == self.options.TCUT_TOPONAIVEDISTANCE):
					cy -= height_button
					self.options.tcut_distance = Blender.Draw.Menu(self.options.D_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_distance.val, "Distance used to compute the cuttings.")
					if self.options.tcut_distance.val == self.options.D_CURVATURE:
						cy -= height_button
						self.options.tcut_distance_ratio = Blender.Draw.Number('Ratio: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_distance_ratio.val, 0.0, 1.0, 'Ratio used to take into account the curvature (linear function).')
					cy -= height_button
					self.options.tcut_distance_weight = Blender.Draw.Toggle('Use weights', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_distance_weight.val, 'Use weights defined by users to modify the distance.')

				if self.options.tcut_global_method.val == self.options.TCUT_TOPODISTANCE:
					if self.advanced.val > self.I_SIMPLE:
						cy -= height_button
						self.options.tcut_stop_method = Blender.Draw.Menu(self.options.TCUT_STOP_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_stop_method.val, "Method used to stop the cutting process.")
						if self.options.tcut_stop_method.val == self.options.TCUT_STOP_NUMBER:
							cy -= height_button
							self.options.tcut_maxnb = Blender.Draw.Number('Number of cuts: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_maxnb.val, 0, 40, 'Number of loops cut during the cutting process.')
					if self.advanced.val > self.I_ADVANCED:
						cy -= height_button
						self.options.tcut_cmethod = Blender.Draw.Menu(self.options.CMETHOD_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_cmethod.val, "Select the algorithm used by the topological cutting.")
						cy -= height_button
						self.options.tcut_omethod = Blender.Draw.Menu(self.options.OMETHOD_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_omethod.val, "Select the optimimizations used by the topological cutting.")
					if self.advanced.val > self.I_SIMPLE:
						cy -= height_button
						self.options.tcut_mmethod = Blender.Draw.Menu(self.options.MMETHOD_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_mmethod.val, "Merging method for the multipoints.")
					if self.advanced.val > self.I_ADVANCED:
						cy -= height_button
						self.options.tcut_umethod = Blender.Draw.Menu(self.options.UMETHOD_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.tcut_umethod.val, "Unsticking method.")
			cy -= height_button
			self.options.do_geometricalcut = Blender.Draw.Toggle('Geometrical cut', self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.do_geometricalcut.val, 'Cut using a geometrical method.')
			if self.options.do_geometricalcut.val:
				largeSize = True
				if self.advanced.val > self.I_ADVANCED:
					self.options.gcut_geometrical_method = Blender.Draw.Menu(self.options.GMETHOD_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_geometrical_method.val, "Geometrical cutting method.")
					cy -= height_button
				if self.options.gcut_geometrical_method.val == self.options.GMETHOD_SIMPLEEXTREMA:
					self.options.gcut_precision_extrema = Blender.Draw.Number('Precision: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_precision_extrema.val, 0, 15, 'Distance between two extrema to be concidered at the same location (distance formulated in percent of the maximum length of the object).')
					cy -= height_button
				if self.options.gcut_geometrical_method.val == self.options.GMETHOD_FROMBORDERS:
					if self.advanced.val > self.I_SIMPLE:
						self.options.gcut_precision = Blender.Draw.Number('Precision: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_precision.val, 0, 15, 'Maximum iteration of the unfolding method (epsilon = 10^(-n), n = precision)')
						cy -= height_button
						self.options.gcut_maxiter = Blender.Draw.Number('Maximum iteration: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_maxiter.val, 0, 100000, 'Maximum iteration of the unfolding method')
						cy -= height_button
					self.options.gcut_use_cp = Blender.Draw.Toggle('Circle packing', self.EVENT_CHANGE_UMETHOD, xcut + width_button, cy, width_button, height_button, self.options.gcut_use_cp.val, 'Use Circle packing rather than ABF method to compute the extrema.')
					cy -= height_button
					self.options.gcut_smooth_ratios = Blender.Draw.Toggle('Smooth ratios', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_smooth_ratios.val, 'Smooth the ratios computed using the infolding method.')
					cy -= height_button
					self.options.gcut_threshold_method = Blender.Draw.Menu(self.options.THRESHOLD_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_threshold_method.val, "Threshold method (on the ratio) used to select the extrema.")
					if self.options.gcut_threshold_method.val == self.options.THRESHOLD_MANUAL:
						cy -= height_button
						self.options.gcut_threshold_value = Blender.Draw.Number('Number of extrema: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_threshold_value.val, 0, 1000, 'Threshold value used to select the number of extrema')
					if self.advanced.val > self.I_SIMPLE:
						cy -= height_button
						self.options.gcut_distance = Blender.Draw.Menu(self.options.D_MENU, self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_distance.val, "Distance used to compute the cuttings.")
						if self.options.gcut_distance.val == self.options.D_CURVATURE:
							cy -= height_button
							self.options.gcut_distance_ratio = Blender.Draw.Number('Ratio: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_distance_ratio.val, 0.0, 1.0, 'Ratio used to take into account the curvature (linear function).')
						cy -= height_button
						self.options.gcut_distance_weight = Blender.Draw.Toggle('Use weights', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.gcut_distance_weight.val, 'Use weights defined by users to modify the distance.')


			cy -= height_button + margin
		else:
			cy -= margin
		if largeSize:
			self.drawBorder(xcut, ybegin, xcut + 2 * width_button, cy + height_button + margin)
		else:
			self.drawBorder(xcut, ybegin, xcut + width_button, cy + height_button + margin)

		if self.options.hide_scalarfunctions.val == 1:
			self.options.hide_scalarfunctions = Blender.Draw.Toggle("-", self.EVENT_UPDATE_GUI, xcut, cy, height_button, height_button, self.options.hide_scalarfunctions.val, 'Hide scalar functions interface.')
			Blender.Draw.Label("Scalar functions", xcut + margin + height_button, cy, width_button, height_button)
		else:
			self.options.hide_scalarfunctions = Blender.Draw.Toggle("Scalar functions...", self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.hide_scalarfunctions.val, 'Show scalar functions interface.')

		cy -= height_button + margin_button


		ybegin = cy + 2 * height_button + margin_button
		largeSize = False
		if self.options.hide_scalarfunctions.val == 1:
			cytop = cy
			if self.options.open_scalar_value_from_file:
				text = "File: "
				Blender.Draw.Label(text, xcut, cy, Blender.Draw.GetStringWidth(text), height_button)
				size_text = width_button - 5 * margin_button - Blender.Draw.GetStringWidth(text) - Blender.Draw.GetStringWidth('Select')
				self.fileScalarFunction = Blender.Draw.String('', self.EVENT_FILE_NAME_SV, xcut + margin_button + Blender.Draw.GetStringWidth(text), cy, size_text, height_button, self.options.realFileScalarFunction, 200, 'Name of the first file')
				Blender.Draw.PushButton('Select', self.EVENT_SELECT_FILE_SV, xcut + width_button - Blender.Draw.GetStringWidth('Select') - 3 * margin_button, cy, Blender.Draw.GetStringWidth('Select') + 3 * margin_button, height_button)
				cy -= height_button
				Blender.Draw.PushButton('Load', self.EVENT_LOAD_FILE_SV, xcut, cy, width_button / 2, height_button)
				Blender.Draw.PushButton('Cancel', self.EVENT_CANCEL_FILE_SV, xcut + width_button / 2, cy, width_button / 2, height_button)
			elif self.options.create_scalar_function_by_fast_marching:
				self.options.sourcePointFM = Blender.Draw.Number('Center: ', self.EVENT_UPDATE_GUI, xcut, cy, width_button, height_button, self.options.sourcePointFM.val, 0, 30000, 'Center for the fast marching propagation')
				cy -= height_button
				Blender.Draw.PushButton('Create', self.EVENT_CREATE_SV_FM, xcut, cy, width_button / 2, height_button)
				Blender.Draw.PushButton('Cancel', self.EVENT_CANCEL_FM_SV, xcut + width_button / 2, cy, width_button / 2, height_button)
			else:
				Blender.Draw.PushButton('Load from file...', self.EVENT_LOAD_SV_FROM_FILE, xcut, cy, width_button / 2, height_button)
				Blender.Draw.PushButton('Create by FM...', self.EVENT_CREATE_SV_FROM_FM, xcut + width_button / 2, cy, width_button / 2, height_button)
			cy -= height_button

			if self.scalarFunction != None and not self.options.open_scalar_value_from_file and not self.options.create_scalar_function_by_fast_marching:
				largeSize = True
				Blender.Draw.Label("Number of values: " + str(self.scalarFunction.getNbValues()), xcut + width_button + height_button, cytop + height_button, width_button, height_button)
				Blender.Draw.Label("Minimum: " + str(self.scalarFunction.getMinValue()), xcut + width_button + height_button, cytop, width_button, height_button)
				Blender.Draw.Label("Maximum: " + str(self.scalarFunction.getMaxValue()), xcut + width_button + height_button, cytop - height_button, width_button, height_button)
				self.drawBorder(xcut + width_button + height_button, cytop + 2 * height_button, xcut + 2 * width_button - 2, cytop - height_button)
				Blender.Draw.PushButton('Unload values', self.EVENT_CLOSE_SV, xcut, cy, width_button, height_button)
				cy -= (height_button + margin)
				Blender.Draw.PushButton('Draw levelset', self.EVENT_DRAW_LEVELSET, xcut, cy, width_button, height_button)
				self.options.levelset_value = Blender.Draw.Slider('Value: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.levelset_value.val, self.scalarFunction.getMinValue(), self.scalarFunction.getMaxValue(), 0, 'Value of the level set to draw')
				cy -= height_button
				Blender.Draw.PushButton('Draw levelsets', self.EVENT_DRAW_LEVELSETS, xcut, cy, width_button, height_button)
				self.options.levelsets_number = Blender.Draw.Number('Number: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.levelsets_number.val, 1, 1000, 'Maximum number of level sets to draw')
				cy -= height_button
				Blender.Draw.PushButton('Draw integral line', self.EVENT_DRAW_INTEGRAL_LINE, xcut, cy, width_button, height_button)
				self.options.vertex_il = Blender.Draw.Slider('Vertex: ', self.EVENT_UPDATE_GUI, xcut + width_button, cy, width_button, height_button, self.options.vertex_il.val, 0, self.scalarFunction.getNbValues(), 0, 'Vertex id')
				cy -= height_button
				Blender.Draw.PushButton('Draw extrema', self.EVENT_DRAW_EXTREMA, xcut, cy, width_button, height_button)
				cy -= height_button
				Blender.Draw.PushButton('Draw saddle points', self.EVENT_DRAW_SADDLE_POINTS, xcut, cy, width_button, height_button)
				Blender.Draw.PushButton('Draw integral lines from s. p.', self.EVENT_DRAW_INTEGRAL_LINES_FROM_SADDLE_POINTS, xcut + width_button, cy, width_button - 2 * height_button, height_button)
				self.options.twoSidesIL = Blender.Draw.Toggle('2', self.EVENT_UPDATE_GUI, xcut + 2 * width_button - 2 * height_button, cy, 2 * height_button, height_button, self.options.twoSidesIL.val, "two sides.")
				cy -= height_button
				Blender.Draw.PushButton('Draw pre-3-loops from s. p.', self.EVENT_DRAW_PRE_3LOOPS_FROM_SADDLE_POINTS, xcut + width_button, cy, width_button, height_button)
				cy -= height_button
				Blender.Draw.PushButton('Draw pre-n-loops from s. p.', self.EVENT_DRAW_PRE_NLOOPS_FROM_SADDLE_POINTS, xcut + width_button, cy, width_button, height_button)
				cy -= height_button
				Blender.Draw.PushButton('Draw n-loops from s. p.', self.EVENT_DRAW_NLOOPS_FROM_SADDLE_POINTS, xcut, cy, width_button - 2 * height_button, height_button)
				self.options.adjust_by_angle = Blender.Draw.Toggle('angle', self.EVENT_UPDATE_GUI, xcut + width_button - 2 * height_button, cy, 2 * height_button, height_button, self.options.adjust_by_angle.val, "Adjust the n-loops using angles.")
				cy -= height_button
				Blender.Draw.PushButton('Draw PL n-loops from s. p.', self.EVENT_DRAW_PLNLOOPS_FROM_SADDLE_POINTS, xcut, cy, width_button - 2 * height_button, height_button)
				self.options.adjust_by_angle_pl = Blender.Draw.Toggle('angle', self.EVENT_UPDATE_GUI, xcut + width_button - 2 * height_button, cy, 2 * height_button, height_button, self.options.adjust_by_angle_pl.val, "Adjust the piecewise-linear n-loops using angles.")
				cy -= height_button



			cy -= margin
			# at the end: readjust the box
			if largeSize:
				cy = min(cy, cytop - 3 * height_button)
		else:
			cy -= margin

		if largeSize:
			self.drawBorder(xcut, ybegin, xcut + 2 * width_button, cy + height_button + margin)
		else:
			self.drawBorder(xcut, ybegin, xcut + width_button, cy + height_button + margin)


		cy = min(cyleft, cy)

		# advanced buttonEvent
		self.advanced = Blender.Draw.Menu(self.I_MENU, self.EVENT_UPDATE_GUI,margin, cy, width_button, height_button, self.advanced.val, 'Interface complexity.')

		# exit and about buttons
		Blender.Draw.PushButton('About', self.EVENT_ABOUT, xcut, cy, width_button, height_button, 'About')
		Blender.Draw.PushButton('Exit', self.EVENT_EXIT, xcut + width_button, cy, width_button, height_button, 'Exit script')

		cy -= margin
		# draw message
		Blender.BGL.glClearColor(0.3, 0.3, 0.3, 1)
		Blender.BGL.glColor3f(0.3, 0.3, 0.3)
		Blender.BGL.glRasterPos2i(margin, cy)
		Blender.Draw.Text(self.message)
	def say(self, m):
		self.message = m
		Blender.Draw.Redraw(1)
		Blender.Window.Redraw(Blender.Window.Types.SCRIPT)

if full:
	try:
		sys.setrecursionlimit(10000)
		gui = GUI()
		gui.makeStandardGUI()
	except:
		traceback.print_exc(file=sys.stdout)

