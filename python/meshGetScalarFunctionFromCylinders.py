#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



"""Usage: meshGetScalarFunctionFromCylinders.py [options] INPUTS

Options:
    -f, --flat-discs=VALUE  Remove the discs of the original tiles (usually extrema) that are "flat",
                            in the sense of area/perimeter < VALUE
    -t, --twist             Choose the second twist side
    -o, --output=FILE       Save the generated scalar function using the file FILE
    -h, --help              Print this message and exit.

INPUTS is a mesh file, and a scalar function on the vertices"""


import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt

def usage():
    print(__doc__)
    sys.exit(0)


def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "ho:f:t",
                               ["help", "output=", "flat-discs=", "twist"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()

        sys.exit(2)

    outputFile = ""
    flatDiscs = -1.
    twist = False
    for o, a in opts:
        if o in ("-t", "--twist"):
            twist = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-f", "--flat-discs"):
            flatDiscs = float(a)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if outputFile == "":
        print("No output defined. Abort")
        sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Mesh file not defined")
        usage()
        sys.exit()

    try:
        functionFile = args[1]
    except IndexError:
        print("Error: Function file not defined")
        usage()
        sys.exit()

    print("Load mesh")
    m = taglut.Mesh(inputFile)
    print("Load scalar function")
    mmap = taglut.MeshMap(functionFile)
    mmap.setMesh(m)
    print("Correct scalar function (removing flat edges)")
    mmap.removeFlatEdges()

    print("Compute the corresponnding vector field")
    vfield = taglut.VectorField(mmap, m)

    print("Compute the n-loops")
    nlf = taglut.NLoopFactory()
    nl = nlf.getNLoopsAsPLPathFromSaddlePoints(m, mmap, vfield)

    if flatDiscs > 0:
        print("Remove flat discs...")
        isize = len(nl)
        nl2 = taglut.PLPath.removeFlatDiscs(nl, m, flatDiscs)
        isize2 = len(nl2)
        print(" Reducing from " + str(isize) + " paths to " + str(isize2) + " paths")
    else:
        nl2 = nl

    print("Cut mesh using n-loops...")
    plCut = taglut.MeshPLCut()
    m2 = plCut.cutMesh(m, nl2)


    print("Generate the scalar function")
    mmap = taglut.MeshMap(m2)
    plCut.updateForCylindricalTiling(mmap, m2, twist)

    print("Save file (" + outputFile + ")")
    mmap.save(outputFile)

if __name__ == "__main__":
    main()
