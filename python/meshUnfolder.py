#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Usage: meshUnfolder [options] INPUT

Basic options:
    -n, --name                       Object name
    -c, --connected-component=C      Connected component in object
    -h, --help                       Print this message and exit.
    --no-cut                         Does not cut before unfolding.

Output options:
    -d, --display                    Display the final mesh
    -2, --dynamic-distance           Display using dynamic display of the
                                     distance.
    --dynamic                        Display dynamic view (2d + 3d window).
    -s, --stats                      Display or save stats rather than mesh.

    -o, --output=FILE                Save the mesh using the file FILE
    --log-file=LF                    Log file (with informations on radii,
                                     angles, etc)
    --export-mapping=PREFIXE_FILE    Export the mapping informations (2D
                                     location, 2D length of edges, ratio of
                                     the length of the edges between 2D and 3D
                                     data.
    --overlaps                       Use an additive method to display overlaps.
    -r, --random                     Use random color on output image.
    -g, --grid                       Use black and white grid to color output
                                     image (used if != -1).
    -b, --border                     Draw border.
    -t, --triangle-border            Draw border for each triangle.
    --normal-map                     Draw the normal map (using RGB = xyz).
    --distortion                     Draw distortion.
    -x, --x=X                        3d window size (x)
    -y, --y=Y                        3d window size (y)

Unfolding options:
    -u, --unfolding-method=M         Unfolding method (available:
                                     \"circle-packing\", \"abf\", \"Floater\", \"Tutte\",
                                     \"QuadFloater\")
    -e, --epsilon=E                  Epsilon (used in unfolding algorithms)
    -m, --maxIter=M                  Maximum iteration (used in unfolding
                                     algorithms)
    -v, --variant=V                  Circle packing variant (0: default with
                                     fixed boundaries, 1: dynamic boundaries)
    -p, --post-treatment=P           post treatment to use after unfolding (-1
                                     for help)
    -j, --iteration-post-treatment=I Maximum iteration for post treatment

Length options:
    -l, --length=L                   Select length method used to cut surface
                                     (or \"list\" for list them)
    -a, --ratio-length=A             Ratio of euclidean length that can be
                                     modified by other length methods. 0 <=
                                     ratio <= 1

Multiscale options:
    --multiscale-depth=MD            Multiscale depth
    --multiscale-method=MM           Multiscale method (available: \"readjust\"
                                     and \"interpolate\")

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "snhx:y:do:u:l:a:e:m:v:p:j:c:rg:bt2", ["name", "help", "x=", "y=", "display", "output=",
                                                                                    "log-file=", "unfolding-method=", "length=",
                                                                                    "ratio-length=", "epsilon=", "maxIter=", "variant=",
                                                                                    "post-treatment=", "iteration-post-treatment=",
                                                                                    "multiscale-depth=", "multiscale-method=",
                                                                                    "connected-component", "random", "grid=",
                                                                                    "border", "triangle-border", "dynamic-distance",
                                                                                    "dynamic", "stats", "no-cut", "overlaps", "normal-map",
                                                                                    "export-mapping=", "distortion"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    display = False
    outputFile = ""
    cComponent = 0
    sizex = 600
    sizey = 600
    epsilon = 1e-8
    maxIter = 30000
    cComponent = -1
    length = ""
    ratio = -1
    postTreatment = 0
    postTreatmentIt = 20000
    variant = 0
    method = ""
    logFile = ""
    mscaleDepth = 0
    mscaleMethod = ""
    gridStep = -1
    border = False
    borderTriangles = False
    random = False
    distortion = False
    dynamic = False
    dynamicDistance = False
    stats = False
    noCut = False
    overlaps = False
    normalMap = False
    exportMapping = ""

    for o, a in opts:
        if o == "--export-mapping":
            exportMapping = a
        if o == "--normal-map":
            normalMap = True
        if o == "--no-cut":
            noCut = True
        if o == "--log-file":
            logFile = a
        if o in ("-c", "--connected-component"):
            cComponent = int(a)
        if o in ("-u", "--unfolding-method"):
            method = a
        if o in ("-l", "--length"):
            length = a
        if o in ("-a", "--ratio-length"):
            ratio = float(a)
        if o in ("-e", "--epsilon"):
            epsilon = float(a)
        if o in ("-m", "--maxIter"):
            maxIter = int(a)
        if o in ("-n", "--name"):
            name = a
        if o in ("-v", "--variant"):
            variant = int(a)
        if o in ("-p", "--post-treatment"):
            postTreatment = int(a)
        if o in ("-j", "--iteration-post-treatment"):
            postTreatmentIt = int(a)
        if o == "--multiscale-depth":
            mscaleDepth = a
        if o == "--multiscale-depth":
            mscaleDepth = int(a)
        if o in ("-r", "--random"):
            random = True
        if o == "--distortion":
            distortion = True
        if o == "--overlaps":
            overlaps = True
        if o in ("-g", "--grid"):
            gridStep = int(a)
        if o in ("-b", "--border"):
            border = True
        if o in ("-t", "--triangle-border"):
            borderTriangles = True
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-2", "--dynamic-distance"):
            dynamicDistance = True
        if o == "--dynamic":
            dynamic = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-s", "--stats"):
            stats = True
        if o in ("-h", "--help"):
            usage()
            sys.exit()



    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit(2)

    try:
        lFactory = taglut.LengthFactory();

        if length == "list":
            print(lFactory.toString())
            sys.exit()

        if postTreatment == -1:
            print(taglut.Mapping2D3D.helpAdjustMapping())
            sys.exit()

        lFactory.setLength(length)
        if not lFactory.existsLength():
            usage()
            print("Error: length unknown (try -l list).")
            sys.exit(2)

        print("Loading mesh...")
        m = taglut.Mesh(inputFile)

        idMethod = 0; # default: circle packing
        if method == "":
            print("Selecting Circle Packing as unfolding algorithm.")
        else:
            if method == "circle-packing":
                idMethod = 0;
            elif method in ("Floater", "floater"):
                idMethod = 2
            elif method in ("Tutte", "tutte"):
                idMethod = 3
            elif method in ("QuadFloater", "quadfloater"):
                idMethod = 4
            else:
                if method == "abf":
                    idMethod = 1;
                    if maxIter > 10000:
                        print("Maximum iteration value is higher than 10000. Assums that is the default value, set to 30.")
                        maxIter = 30
                else:
                    usage()
                    print("Error: Unknown unfolding method.")
                    sys.exit(2)

        if outputFile == "" and not display and not dynamic and not dynamicDistance and not stats and not exportMapping:
            usage()
            print("Error: Specify an output file or select a display mode.")
            sys.exit(2)

        if epsilon <= 0:
            usage()
            print("Error: epsilon not positive.")
            sys.exit(2)

        if sizex <= 0 or sizey <= 0:
            usage()
            print("Error: Output size not positive")
            sys.exit(2)

        mParts = m.getConnectedComponents()
        if len(mParts) == 1:
            cComponent = 0
        else:
            if len(mParts) <= cComponent or cComponent < 0:
                print("Warning: connected component #" + str(cComponent) + " unknown. Surface has", len(mParts), "connected components.")
                print(" Selecting connected component by size")
                cComponent = 0
                nb = mParts[0].getNbPoints()
                i = 0
                for mmm in mParts:
                    if (mmm.getNbPoints() > nb):
                        cComponent = i
                        nb = mmm.getNbPoints()
                    ++i

        m = taglut.Mesh(mParts[cComponent])

        if noCut:
            print("Skipping cutting step.")
        else:
            meshCutter = taglut.MeshCut(m, lFactory.getLength(m))
            print("Cutting surface...")
            meshCutter.cutMeshUsingMinimalCut()
            sizeCut = meshCutter.getSize()
            if sizeCut != 0:
                print(" Cut size: " + str(sizeCut) + ". OK")
            else:
                print(" OK")

        mapping = taglut.Mapping2D3D(m)

        if idMethod == 0:
            print("Circle packing: unfolding...")
            uMethod = taglut.CPMethod(mapping, variant == 0)
        elif idMethod == 1:
            print("ABF: unfolding...")
            uMethod = taglut.ABFMethod(mapping)
        elif idMethod == 2:
            uMethod = taglut.FloaterMethod(mapping)
        elif idMethod == 3:
            uMethod = taglut.TutteMethod(mapping)
        elif idMethod == 4:
            uMethod = taglut.QuadFloaterMethod(mapping)
        else:
            print("Unknown method.")
            sys.exit(2)

        readjust = mscaleMethod == "" or mscaleMethod == "readjust"
        mSUnfolding = taglut.MultiscaleUnfolding (readjust, epsilon, maxIter, True, logFile)
        nbIter = mSUnfolding.processUnfolding(uMethod, mscaleDepth)

        if nbIter < 0:
            print("Abording.")
            sys.exit(2)

        print("nbPoints:" + str(m.getNbPoints()))
        print("nbTriangles:" + str(m.getNbTriangles()))

        nbNeighbours = 0
        for i in range(m.getNbPoints()):
            nbNeighbours += m.point(i).getNbNeighbours()
        print("nbNeighbours: " + str(nbNeighbours))

        iManager = taglut.IndentManager();
        print(iManager.__class__.__name__)
        mapping.adjustMapping(postTreatment, postTreatmentIt, epsilon, iManager);

        if outputFile != "" and mapping.isSaveFormat(outputFile):
            print("Export unfolding mapping...")
            mapping.save(outputFile)
        try:
            if outputFile != "" and idMethod == 0 and uMethod.isSaveFormat(outputFile):
                print("Saving circle set...")
                uMethod.save(outputFile)
        except AttributeError:
            pass

        if outputFile != "" and m.isSaveFormat(outputFile) and not mapping.isSaveFormat(outputFile) and not uMethod.isSaveFormat(outputFile):
            print("Saving mesh...")
            m2 = taglut.Mesh(mapping)
            m2.save(outputFile)

        if exportMapping != "":
            print("Export mapping infos...")
            mapping.exportMapping(exportMapping)

        if (outputFile != "" and (taglut.FileTools.extension(outputFile) == "jpg" or
                                  taglut.FileTools.extension(outputFile) == "png")) or (display and (normalMap or overlaps or distortion)):
            if stats:
                result = mapping.toImageStats(sizex, sizey)
            else:
                result = taglut.UCharCImg(sizey, sizey, 1, 3)
                result.fill(0)

                mapping.toImageRandom(result)

                if normalMap:
                    mapping.toImageDrawNormals(result)

                if overlaps:
                    mapping.toImageOverlap(result)

                if gridStep != -1:
                    m.computeGridValues(grid, gridStep)
                    mapping.toImage(result, grid)

                if distortion:
                    mapping.toImageDrawDistortions(result)

                if border:
                    mapping.toImageDrawBorder(result, 0)

                if borderTriangles:
                    mapping.toImageDrawBorderTriangles(result, 0)


            if display:
                disp = taglut.CImgDisplay(result, "Unfolded surface")
                while not disp.is_closed:
                    disp.wait()

            # save result image
            if outputFile != "":
                print("Saving image (", outputFile, ")...")
                result.save(outputFile)

        if stats and outputFile == "":
            img = mapping.toImageStats(sizex, sizey)
            print(img.__class__)

            disp = taglut.CImgDisplay(img, "Statistics")
            while not disp.is_closed:
                disp.wait()

        if dynamic:
            taglut.Display3D.displayMappingDynamicMap(sizex, sizey, "Visualisation", mapping)

        if dynamicDistance:
            taglut.Display3D.displayMappingDynamicDistance(sizex, sizey, "3D Visualisation", mapping)

        if display and not normalMap and not dynamic and not dynamicDistance and not overlaps and not distortion:
            taglut.Display3D.displayMapping(sizex, sizey, "3D Visualisation", mapping, False, borderTriangles, border)

    except taglut.Exception as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
