#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshGetRegionOfInterest.py [options] INPUT

Input options:
    -n, --name=NAME        Object name (in the input file)
    -f, --function=FILE    A scalar function (one line for each vertex)

Region-of-interest options:
    -x, --x=VALUE          X coordinate of the center
    -y, --y=VALUE          Y coordinate of the center
    -z, --z=VALUE          Z coordinate of the center
    -r, --radius=RADIUS    Radius of the region
    -s, --surface          Use a surface approach rather than a volumic approach
    -c, --cube             Use a cube rather than a ball centerd in (X, Y, Z).
    -a, --all              Preserve all the vertices inside the region of
                           interest, not only one connected component (by
                           default, it select the biggest one).
    --nearest              Select the nearest connected component rather than
                           the biggest one.
    --clean                Clean the resulting mesh

Output options:
    -o, --output=FILENAME  Output file (available output format: VRML, OFF, OBJ)
    --output-function=FILE Output file for the scalar function.

INPUT is a mesh file (available input formats: VRML, PLY, ...)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:f:x:y:z:r:scao:h", ["name=", "function=",
                                                                    "x=", "y=", "z=", "r=", "surface", "cube", "all", "nearest", "clean",
                                                                    "output=", "output-function=",
                                                                    "help"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    name = ""
    function= ""

    x = -1
    y = -1
    z = -1
    radius = -1
    nbParams = 0
    surface = False
    cube = False
    preserveAll = False
    nearest = False
    clean = False

    output = ""
    outputFunction = ""
    for o, a in opts:
        # input
        if o in ("-n", "--name"):
            name = a
        if o in ("-f", "--function"):
            function = a
        # region-of-interest
        if o in ("-x", "--x"):
            x = float(a)
            nbParams += 1
        if o in ("-y", "--y"):
            y = float(a)
            nbParams += 1
        if o in ("-z", "--z"):
            z = float(a)
            nbParams += 1
        if o in ("-r", "--radius"):
            radius = float(a)
            nbParams += 1
        if o in ("-s", "--surface"):
            surface = True
        if o in ("-c", "--cube"):
            cube = True
        if o in ("-a", "--all"):
            preserveAll = True
        if o == "--nearest":
            nearest = True
        if o == "--clean":
            clean = True
        # output
        if o in ("-o", "--output"):
            output = a
        if o == "--output-function":
            outputFunction = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if output == "":
        usage()
        print("Error: output file not defined")
        sys.exit()

    if nbParams != 4:
        usage()
        print("x, y, z and radius parameters are mandatory")
        sys.exit(1)

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:

        # load mesh
        print("Loading mesh (" + inputFile + ")...")
        m = taglut.Mesh(inputFile, name)
        if function != "":
            print("Loading function (" + function + ")...")
            mMap = taglut.MeshMap(m, function)

        # select the region of interest
        print("Select the region of interest")
        p = taglut.Point3D(x, y, z)
        if cube:
            print("")
            p1 = taglut.Point3D(x - radius, y - radius, z - radius)
            p2 = taglut.Point3D(x + radius, y + radius, z + radius)
            mPart = taglut.MeshPart(m, p1, p2)
        else:
            mPart = taglut.MeshPart(m, p, radius, surface)

        # select the connected component
        if not preserveAll:
            print("Crop mesh")
            m2 = mPart.buildCropMesh()
            if function != "":
                mMap2 = taglut.MeshMap(m2, mPart, mMap)
            if m2.getNbPoints() == 0:
                print("Warning: empty mesh")
                sys.exit(1)
            print("Select the connected component")
            if nearest:
                mPart = taglut.MeshPart(m2, p)
            else:
                mPart = m2.getLargestCC()
            print("Crop mesh")
            mFinal = mPart.buildCropMesh()
            if function != "":
                mMapFinal = taglut.MeshMap(mFinal, mPart, mMap2)
        else:
            mFinal = mPart.buildCropMesh()
            if function != "":
                mMapFinal = taglut.MeshMap(mFinal, mPart, mMap)

        if clean:
            print("Clean mesh")
            mPart = taglut.MeshPart(mFinal)
            mPart.cleanMesh()
            mFinal = taglut.Mesh(mFinal.getLargestCC())

        print("Saving mesh (" + output + ")...")
        mFinal.save(output)
        if function != "" and outputFunction != "":
            print("Saving scalar function (" + outputFunction + ")...")
            mMapFinal.save(outputFunction)

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
