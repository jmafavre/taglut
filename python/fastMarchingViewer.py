#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: fastMarchingViewer [options] INPUT

Options:
    -n, --name         Object name
    -x, --x=X          3d window size (x)
    -y, --y=Y          3d window size (y)
    -p, --point=POINT  The starting point
    -t, --target=POINT The possible target point
    -r, --radius=R     Stop value
    -v, --verify       Verify that geodesic distance is higher than Euclidean distance
    -g, --opengl       Use opengl display rather than X11
    -b, --border       Show borders using red color
    --gouraud          Smooth the surface using gouraud mehtod
    -a, --autolight    Auto light of the surface

    -h, --help         Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:hx:y:gbap:r:t:v", ["name=", "help", "opengl",
                                                             "x=", "y=",
                                                             "gouraud", "autolight",
                                                             "point=", "radius=", "target=", "verify" ])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    opengl = False
    sizex = 300
    sizey = 300
    name = ""
    gouraud = False
    autolight = False
    point = 0
    radius = -1
    target = -1
    verify = False
    for o, a in opts:
        if o in ("-v", "--verify"):
            verify = True
        if o in ("-t", "--target"):
            target = int(a)
        if o == "--gouraud":
            gouraud = True
        if o in ("-a", "--autolight"):
            autolight = True
        if o in ("-n", "--name"):
            name = a
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-g", "--opengl"):
            opengl = True
        if o in ("-p", "--point"):
            point = int(a)
        if o in ("-r", "--radius"):
            radius = float(a)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if sizex <= 0 or sizey <= 0:
        usage()
        print("Error: Output size not positive")
        sys.exit(2)

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh")
        m = taglut.Mesh(inputFile, name)
        if point > m.getNbPoints() - 1:
            print("Point out of range (" + str(m.getNbPoints()) + ")")
            sys.exit(1)

        print("Computing distances")
        dTool = taglut.FastMarching()

        mMap = taglut.MeshMap(m)
        if target > 0:
            dTool.computeDistanceFromPoint(mMap, m, point, radius, [target])
        else:
            dTool.computeDistanceFromPoint(mMap, m, point, radius)

        if verify:
            for i in range(0, mMap.getNbValues() - 1):
                if mMap.getValue(i) == -1:
                    if mMap.getValue(i) > mesh.point(point).distance(mesh.point(i)):
                        print("Error, point " + str(i) + ": Euclidean[" + str(mesh.point(point).distance(mesh.point(i))) + "] vs geodesic[" + str(mMap.getValue(i)) + "]")
        if  target <= 0:
            v = 0
            for i in range(0, mMap.getNbValues() - 1):
                if mMap.getValue(i) == -1:
                    print(" V:")
                    m.tracePoint(i)
                    if m.isSaddlePoint(i):
                        print("   saddle point")
                    for nb in m.point(i).getNeighbours():
                        print("  v(" + str(nb) + "): " + str(mMap.getValue(nb)) + ", d: " + str(m.point(nb).distance(m.point(i))))
                        if m.isSaddlePoint(nb):
                            print("   saddle point")
                    v += 1
            if v != 0:
                print("Warning: number of non-computed vertices:" + str(v) + "/" + str(mMap.getNbValues()))

        mMap.save("/tmp/map.txt")
        taglut.Display3D.displayMeshMapTriangles(sizex, sizey, "Display", m, mMap, 0.0, False, opengl, gouraud, autolight)

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
