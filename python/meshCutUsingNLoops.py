#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



"""Usage: meshCutUsingNLoops.py [options] INPUTS

Options:
    --exact                 Do an exact estimation of the base points rather than fitting them on an
                            existing vertex
    -a, --angles            Adjust using angles
    -e, --epsilon=VALUE     Set the epsilon value that corresponds to a tolerancy for merge (default: 0)
    --skip-nloop            Skip the n-loop computation (assuming that it's already done)
    -c, --cylinders         Do not cut the cylinders after the n-loop computation
    --param-choice=VALUE    Method used for the parameterization of cylinders (available values:
                            0: conformal approach, 1: by length of paths, 2: mean square error on
                            normals, 3: by distance to the cylinder, 4 (default): conformal approach
                            removing absurd twists)
    --cylinder-param=VALUE  Method for cylinder parameterization (available values:
                            0: in 3D space, 1: in 2D space, 2: in 3D space using integral lines,
                            3 (default): in 3D space using 4 integral lines)
    -f, --flat-discs=VALUE  Remove the discs of the original tiles (usually extrema) that are "flat",
                            in the sense of area/perimeter < VALUE
    --split-cylinders=VALUE Split the cylinders into subcylinders using the given value as threshold for the
                            wanted ratio between perimeter and length
    --sc-method=VALUE       Method ussed for the cylindrical splitting (available values:
                            0: cylindrical parameterization, 1 (default): floater parameterization),
                            2: shortest path

    -d, --display           Display the final mesh
    -i, --infos             Display information about final mesh
    -o, --output=FILE       Save the mesh using the file FILE

    -p, --parameterize=FILE Save the mesh and the parameterization using a Floater approach tile by tile
    -4, --quadrangulation   Describe the extrema tiles with quadrangles
    -b, --b-splines=FILE    Save a B-spline description of the quadrangles (obj format)
    -m, --bs-mesh=FILE      Save a mesh description of the B-splines
    -x, --x=X               3d window size (x)
    -y, --y=Y               3d window size (y)
    -h, --help              Print this message and exit.

INPUTS is a mesh file, and a scalar function on the vertices"""


import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt

def usage():
    print(__doc__)
    sys.exit(0)


def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "hdio:x:y:acp:4b:m:f:e:",
                               ["help", "infos", "display", "output=",
                                "x=", "y=", "angles", "cylinders",
                                "parameterize=", "quadrangulation",
                                "b-splines=", "bs-mesh=",
                                "flat-discs=",
                                "cylinder-param=", "param-choice=",
                                "skip-nloop", "split-cylinders=",
                                "sc-method=", "epsilon=", "exact"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()

        sys.exit(2)

    epsilon = 0.
    infos = False
    display = False
    sizex = 300
    sizey = 300
    outputFile = ""
    outputFileParam = ""
    nLoops = False
    plnLoops = False
    angles = False
    cylinders = False
    quads = False
    bsplines = ""
    bsMesh = ""
    skipNloop = False
    flatDiscs = -1.
    paramChoice = taglut.MeshPLCut.CPCByDistanceAndConformity
    paramMethod = taglut.MeshPLCut.CPMFloater4IntegralLines
    splitCylinders = -1
    scMethod = "floater"
    exact = False
    for o, a in opts:
        if o == "--exact":
            exact = True
        if o in ("-e", "--epsilon"):
            epsilon = float(a)
            if epsilon < 0:
                print("epsilon value cannot be negative")
                sys.exit(1)
        if o == "--sc-method":
            if a == "0":
                scMethod = "cylinder"
            elif a == "1":
                scMethod = "floater"
            elif a == "2":
                scMethod = "shortest-path"
            else:
                print("Unknown choice for cylindrical splitting")
                sys.exit(1)
        if o == "--split-cylinders":
            splitCylinders = float(a)
        if o == "--skip-nloop":
            skipNloop = True
        if o == "--param-choice":
            if int(a) == 0:
                paramChoice = taglut.MeshPLCut.CPCByConformity
            elif int(a) == 1:
                paramChoice = taglut.MeshPLCut.CPCByLength
            elif int(a) == 2:
                paramChoice = taglut.MeshPLCut.CPCByNormals
            elif int(a) == 3:
                paramChoice = taglut.MeshPLCut.CPCByDistance
            elif int(a) == 4:
                paramChoice = taglut.MeshPLCut.CPCByDistanceAndConformity
            else:
                print("Unknown choice for parameterization")
                sys.exit(1)
        if o == "--cylinder-param":
            if int(a) == 0:
                paramMethod = taglut.MeshPLCut.CPMFloater3D
            elif int(a) == 1:
                paramMethod = taglut.MeshPLCut.CPMFloater2D
            elif int(a) == 2:
                paramMethod = taglut.MeshPLCut.CPMFloaterIntegralLines
            elif int(a) == 3:
                paramMethod = taglut.MeshPLCut.CPMFloater4IntegralLines
            else:
                print("Unknown parameterization")
                sys.exit(1)
        if o in ("-i", "--infos"):
            infos = True
        if o in ("-d", "--display"):
            display = True
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-p", "--parameterize"):
            outputFileParam = a
        if o in ("-c", "--cylinders"):
            cylinders = True
        if o in ("-a", "--angles"):
            angles = True
        if o in ("-4", "--quadrangulation"):
            quads = True
        if o in ("-b", "--b-splines"):
            bsplines = a
        if o in ("-m", "--bs-mesh"):
            bsMesh = a
        if o in ("-f", "--flat-discs"):
            flatDiscs = float(a)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if (not infos) and (not display) and outputFile == "" and outputFileParam == "" and \
            bsplines == "" and bsMesh == "":
        print("No output defined. Abort")
        sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Mesh file not defined")
        usage()
        sys.exit()

    if not skipNloop:
        try:
            functionFile = args[1]
        except IndexError:
            print("Error: Function file not defined")
            usage()
            sys.exit()

    print("Load mesh")
    m = taglut.Mesh(inputFile)
    plCut = taglut.MeshPLCut()
    if not skipNloop:
        print("Load scalar function")
        mmap = taglut.MeshMap(functionFile)
        mmap.setMesh(m)
        print("Correct scalar function (removing flat edges)")
        mmap.removeFlatEdges()
        print("Compute the corresponnding vector field")
        vfield = taglut.VectorField(mmap, m)

        print("Compute the n-loops")
        nlf  =taglut.NLoopFactory()
        nlf.setAdjustByAngles(angles)
        nlf.setEpsilonForMerge(epsilon)
        if exact:
            nl = nlf.getPreNLoopsFromSaddlePoints(m, mmap, vfield)
        else:
            nl = nlf.getNLoopsAsPLPathFromSaddlePoints(m, mmap, vfield)

        if flatDiscs > 0:
            if exact:
                print("Remove flat discs...")
                isize = len(nl)
                nl2 = taglut.PreNLoop.removeFlatDiscs(nl, m, mmap, flatDiscs)
                isize2 = len(nl2)
                print(" Reducing from " + str(isize) + " n-loops to " + str(isize2) + " n-loops")
            else:
                print("Remove flat discs...")
                isize = len(nl)
                nl2 = taglut.PLPath.removeFlatDiscs(nl, m, flatDiscs)
                isize2 = len(nl2)
                print(" Reducing from " + str(isize) + " paths to " + str(isize2) + " paths")
        else:
            nl2 = nl

        if (infos):
            # TODO: refactoring...
            print("Found " + str(len(nl2)) + " paths")
            for p in nl2:
                print(" - a path with " + str(p.size()) + " points")

        print("Cut mesh using n-loops...")
        if exact:
            m2 = plCut.cutMeshPNLUpdateScalarFunction(m, nl2, mmap)
        else:
            m2 = plCut.cutMeshUpdateScalarFunction(m, nl2, mmap)
        mmap.setMesh(m2)

        if quads and not skipNloop:
            print("Split extrema discs...")
            m3 = plCut.cutExtremaDiscsUpdateScalarFunction(m2, mmap)
            mmap.setMesh(m3)
        else:
            m3 = m2
    else:
        m2 = m

    if not cylinders:
        print("Subdivise cylinders into quadrangles...")

        if splitCylinders > 0 and scMethod == "cylinder":
            m4 = plCut.cutMeshInQuadranglesFromCylindricalTiling(m3, splitCylinders, -1., paramChoice, paramMethod)
        else:
            m4 = plCut.cutMeshInQuadranglesFromCylindricalTiling(m3, -1., -1., paramChoice, paramMethod)

        if splitCylinders > 0 and scMethod == "floater":
            print("Subdivise cylinders into sub cylinders...")
            m5 = plCut.cutCylindersByQuadParam(m4, splitCylinders)
            m4 = m5

        if splitCylinders > 0 and scMethod == "shortest-path":
            print("Subdivise cylinders into sub cylinders...")
            m5 = plCut.cutCylindersByShPathParam(m4, splitCylinders)
            m4 = m5
    else:
        m4 = m3

    if (infos):
        print(m4.getInfos())
    if (display):
        print("Display...")
        taglut.Display3D.displayMesh(sizex, sizey, "Display", m4, False, True)
    if (outputFile != ""):
        print("Save mesh (" + outputFile + ")...")
        m4.save(outputFile)
    if (outputFileParam != ""):
        print("Parameterization")
        mtiling = taglut.MTiling(m4)
        if quads:
            print("Adjust quadrangles")
            mtiling.adjustQuads()
        mapping = mtiling.parameterize()
        print("Save mapping (" + outputFileParam + ")...")
        mapping.save(outputFileParam)
    if bsplines != "" or bsMesh != "":
        if not quads and not skipNloop:
            print("Split extrema discs...")
            m3 = plCut.cutExtremaDiscs(m2, mmap.getLocalExtrema())
        if (not quads) or cylinders:
            print("Split cylinders...")
            m4 = plCut.cutMeshInQuadranglesFromCylindricalTiling(m3)
        mtiling = taglut.MTiling(m4)
    if bsplines != "":
        print("Compute and save B-splines (" + bsplines + ")...")
        mtiling.saveApproxSurf(bsplines)
    if bsMesh != "":
        print("Compute and save mesh (" + bsMesh + ")...")
        mesh = mtiling.remeshByBSplines(0.05)
        mesh.save(bsMesh)

if __name__ == "__main__":
    main()
