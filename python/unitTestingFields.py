#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal / IMATI-CNR
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import taglut
import sys

m = taglut.Mesh()
p1 = taglut.Point3D(0, 0, 0)
m.addPoint(p1)
p2 = taglut.Point3D(0, 1, 0)
m.addPoint(p2)
p3 = taglut.Point3D(0, 0, 1)
m.addPoint(p3)
t = taglut.Triangle(0, 1, 2)
m.addTriangleFull(t)

mMap = taglut.MeshMap(m)
mMap.setValue(0, 0.)
mMap.setValue(1, 1.)
mMap.setValue(2, 1.)

vf = taglut.VectorField(mMap, m)
if vf[0].getX() != 0.:
    print("Error (X coordinate)")
    sys.exit(-1)
if vf[0].getZ() == 0.:
    print("Error (Z coordinate)")
    sys.exit(-1)
if vf[0].getZ() != vf[0].getY():
    print("Error (Y vs Z coordinates)")
    sys.exit(-1)

print("Pass")

next = vf.getNextPoint(taglut.PointOnEdge(m.point(1), m.point(2), 0.5), m.triangle(0), m)
sys.exit(0)

