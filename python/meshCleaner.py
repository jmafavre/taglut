#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshCleaner [options] INPUT

Basic Options:
    -n, --name               Object name

    -h, --help               Print this message and exit.

Posttreatment options:
    --cc-by-size             Select the connected component by size (Default)
    --cc-by-id=ID            Select the connected component by size

Output options:
    -d, --display            Display the final mesh
    -o, --output=FILE        Save the mesh using the file FILE
    -x, --x=X                Window size (x)
    -y, --y=Y                Window size (y)
    -g, --opengl             Use opengl display rather than X11
    -e, --edges              Display edges rather than triangles

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "nhdo:x:y:ge",
                               ["name", "help", "display", "output=", "opengl",
                                "edges", "x=", "y=", "cc-by-size", "cc-by-id=",
                                "cc-no-selection"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        usage()
        print("Error: " + msg)
        sys.exit(2)

    ccBySize = False
    ccById = -1
    display = False
    outputFile = ""
    edges = False
    opengl = False
    sizex = 300
    sizey = 300

    for o, a in opts:
        if o == "--cc-by-size":
            ccBySize = True
        if o == "--cc-by-id":
            ccById = int(a)
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-g", "--opengl"):
            opengl = True
        if o in ("-b", "--border"):
            border = True
        if o in ("-e", "--edges"):
            edges = True
        if o in ("-h", "--help"):
            usage()
            sys.exit()


    try:
        inputFile = args[0]
    except IndexError:
        usage()
        print("Error: Input file not defined")
        sys.exit(1)

    if not display and outputFile == "":
        usage()
        print("Error: Output not defined (display mode or filename)")
        sys.exit(1)

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)

        print("Cleaning mesh removing bad points (with wrong local topology)")
        mPart = taglut.MeshPart(m)
        nbClean = mPart.cleanMesh()
        if nbClean != 0:
            print(" Removing " + str(nbClean) + " point(s)")
        else:
            print(" No point removed")

        if ccBySize:
            print("Selecting the largest connected component...")
            largestCC = m.getLargestCC()
            largestCC.cropMesh()
        elif ccById >= 0:
            prin("Selecting the connected component #" + str(ccById) + "...")
            mParts = m.getConnectedComponents()
            if (ccById >= len(mParts)):
                print("Error: the connected component #" + str(ccById) + " doesn't exist. There is only " + str(len(mParts)) + " connected components")
                sys.exit(1)
            mParts[ccById].cropMesh()


        if (display):
            taglut.Display3D.displayMesh(sizex, sizey, "3D Visualisation", m, edges, True, opengl);
        if (outputFile != ""):
            print("Saving file(" + outputFile + ")...")
            m.save(outputFile)

    except taglut.ExceptionFileNotFound as msg:
        print ("Error: " + msg.getMessage())
        sys.exit(1)


if __name__ == "__main__":
    main()
