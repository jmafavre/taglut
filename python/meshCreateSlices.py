#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



"""Usage: meshCreateSlices.py [options] INPUT

Options:
    -p, --patch             Patch before cutting
    --no-x, --no-y, --no-z  Do not generate the plane(s) in the given direction
    -x, --x-direction=X,Y,Z Set the direction of the first slice series by giving the normal (X, Y, Z) to the plane
    -y, --y-direction=X,Y,Z Set the direction of the second slice series by giving the normal (X, Y, Z) to the plane
    -z, --z-direction=X,Y,Z Set the direction of the third slice series by giving the normal (X, Y, Z) to the plane
    -u, --uniform-noise=E   Modify the direction of the normals using an uniform noise (E is the maximum perturbation in
                            each of the directions on a normalized vector)
    -n, --number-max=NB     Maximal number of slice for a direction
    -m, --number-min=NB     Minimal number of slice for a direction (if given, the maximal number is ignored)
    -a, --anisotropy=AN     Anisotropy (ratio defined by distance between
                            slices and distance between points on a slice
    -t, --trace	            Write in standard output a trace of the points
    -o, --output=FILE       Save the polygons using the file FILE
    -h, --help              Print this message and exit.

INPUTS is a mesh file"""


import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt

def usage():
    print(__doc__)
    sys.exit(0)


def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "o:hn:a:m:x:y:z:u:pt", ["help", "output=",
                                                                   "number-max=", "anisotropy=",
                                                                   "number-min=",
                                                                   "x-direction=", "y-direction=", "z-direction=",
                                                                   "unifom-noise=",
                                                                   "no-x", "no-y", "no-z", "patch", "trace"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()

        sys.exit(2)

    outputFile = ""
    nbSlicesMax = 10
    nbSlicesMin = 0
    anisotropy = -1
    dx = taglut.Coord3D(1., 0., 0.)
    dy = taglut.Coord3D(0., 1., 0.)
    dz = taglut.Coord3D(0., 0., 1.)
    nox = False
    noy = False
    noz = False
    noise = 0.
    patch = False
    trace = False

    for o, a in opts:
        if o in ("-p", "--patch"):
            patch = True
        if o in ("-t", "--trace"):
            trace = True
        if o == "--no-x":
            nox = True
        if o == "--no-y":
            noy = True
        if o == "--no-z":
            noz = True
        if o in ("-x", "--x-direction", "-y", "--y-direction", "-z", "--z-direction"):
            l = a.rsplit(",")
            if len(l) != 3:
                print("Error: bad direction description")
                sys.exit(1)
            try:
                ddx = float(l[0])
                ddy = float(l[1])
                ddz = float(l[2])
                if o in ("-x", "--x-direction"):
                    dx = taglut.Coord3D(ddx, ddy, ddz).getNormalize()
                elif o in ("-y", "--y-direction"):
                    dy = taglut.Coord3D(ddx, ddy, ddz).getNormalize()
                else: # o in ("-z", "--z-direction"):
                    dz = taglut.Coord3D(ddx, ddy, ddz).getNormalize()
            except TypeError:
                print("Error: bad direction description")
                sys.exit(1)
        if o in ("-u", "--unifom-noise"):
            noise = float(a)
            if noise != 0.:
                rgen = taglut.RandomGeneratorUniform(-noise, noise)
                dx.setCoords(dx.getX() + rgen.value(), dx.getY() + rgen.value(), dx.getZ() + rgen.value())
                dx.normalize()
                dy.setCoords(dy.getX() + rgen.value(), dy.getY() + rgen.value(), dy.getZ() + rgen.value())
                dy.normalize()
                dz.setCoords(dz.getX() + rgen.value(), dz.getY() + rgen.value(), dz.getZ() + rgen.value())
                dz.normalize()
        if o in ("-m", "--number-min"):
            nbSlicesMin = int(a)
        if o in ("-n", "--number-max"):
            nbSlicesMax = int(a)
        if o in ("-a", "--anisotropy"):
            anisotropy = int(a)
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if outputFile == "":
        print("No output defined. Abort")
        sys.exit(1)


    if nbSlicesMax <= 0:
        print("Bad number of slices (may be > 0). Abort")
        sys.exit(1)

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Mesh file not defined")
        usage()
        sys.exit(1)


    print("Load mesh")
    m = taglut.Mesh(inputFile)

    if patch:
        print("Preprocessing (patch step)...")
        mPatch = taglut.MeshPatches(m)
        mPatch.closeBoundaries()

    print("Estimate space between slices")
    sx = m.getSize(dx)
    sy = m.getSize(dy)
    sz = m.getSize(dz)

    if nbSlicesMin > 0:
        smin = min(sx, sy, sz)
        space = smin / nbSlicesMin
    else:
        smax = max(sx, sy, sz)
        space = smax / nbSlicesMax

    print("Compute slices")
    polygons = []
    if not nox:
        polygonsx = m.getIntersectionBySpace(dx, space)
        polygons += polygonsx
    if not noy:
        polygonsy = m.getIntersectionBySpace(dy, space)
        polygons += polygonsy
    if not noz:
        polygonsz = m.getIntersectionBySpace(dz, space)
        polygons += polygonsz

    if len(polygons) == 0:
        print("No computed polygons, abort")
        return 1

    if anisotropy > 0:
        print("Subdivise paths")
        polygons2 = taglut.PLPath.subdivise(polygons, space / anisotropy)

	if trace:
	  i = 0
	  for p in polygons:
	    print("polygon", i)
	    print(p.toString(True))
	    i = i + 1
        print("Save polygons")
        taglut.Path3D.savePolygons(polygons2, outputFile)
    else:
	if trace:
	  i = 0
	  for p in polygons:
	    print("polygon", i)
	    print(p.toString(True))
	    i = i + 1
        print("Save polygons")
        taglut.PLPath.savePolygons(polygons, outputFile)


if __name__ == "__main__":
    main()
