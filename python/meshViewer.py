#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshViewer [options] INPUT

Options:
    -n, --name         Object name
    -x, --x=X          3d window size (x)
    -y, --y=Y          3d window size (y)
    -g, --opengl       Use opengl display rather than X11
    -b, --border       Show borders using red color
    --gouraud          Smooth the surface using gouraud mehtod
    -a, --autolight    Auto light of the surface
    -s, --simplecut    Display simple cut
    -e, --edges        Display edges rather than triangles

    -t, --texture=T    A 3D file describing the texture
    -z, --zratio=R     Z ratio of the texture

    -f, --function=F   A file that describe a scalar function for each vertex
    -i, --ifunction=F  A file that describe an integer function for each vertex
    -p, --points=P     A file that contains a list of points

    --radius-tubular=R A radius value vor tubular approach

    -l, --log          Modify the scalar function using the log() function
    -h, --help         Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:hgex:y:sbt:z:af:lp:i:", ["name=", "help", "opengl",
                                                                    "edges", "x=", "y=", "simplecut",
                                                                    "border", "texture=", "zratio=",
                                                                    "gouraud", "autolight", "function=", "log",
                                                                     "radius-tubular=", "points=", "ifunction=" ])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    edges = False
    opengl = False
    simplecut = False
    simplecutplus = False
    sizex = 300
    sizey = 300
    border = False
    name = ""
    zRatio = 1
    textureFile = ""
    gouraud = False
    autolight = False
    functionFile = ""
    ifunctionFile = ""
    useLog = False
    radiusTubular = -1
    pointsFile = ""
    for o, a in opts:
        if o == "--radius-tubular":
            radiusTubular = float(a)
        if o == "--gouraud":
            gouraud = True
        if o in ("-a", "--autolight"):
            autolight = True
        if o in ("-z", "--zratio"):
            zRatio = float(a)
        if o in ("-t", "--texture"):
            textureFile = a
        if o in ("-f", "--function"):
            functionFile = a
        if o in ("-i", "--ifunction"):
            ifunctionFile = a
        if o in ("-p", "--points"):
            pointsFile = a
        if o in ("-n", "--name"):
            name = a
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-g", "--opengl"):
            opengl = True
        if o in ("-b", "--border"):
            border = True
        if o in ("-e", "--edges"):
            edges = True
        if o in ("-s", "--simplecut"):
            simplecut = True
        if o in ("-+", "--simplecut+"):
            simplecutplus = True
        if o in ("-l", "--log"):
            useLog = True
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if sizex <= 0 or sizey <= 0:
        usage()
        print("Error: Output size not positive")
        sys.exit(2)

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh")
        m = taglut.Mesh(inputFile, name)
        if simplecut:
            mParts = m.getConnectedComponents()
            resultM = taglut.MeshDeque()
            for mp in mParts:
                mmmm = taglut.Mesh(mp)
                mpm = taglut.MeshPart(mmmm)
                mpm.cropSimpleCut()
                resultM.push_back(mmmm)
                m = taglut.Mesh(resultM)
        if textureFile != "":
            print("Loading texture image")
            image = taglut.UCharCImg(textureFile)
            taglut.Display3D.displayMeshTexture(sizex, sizey, "Display", m, image, zRatio, opengl, gouraud, autolight)
        elif radiusTubular > 0:
            mtd = taglut.MeshTubularDetector(m)
            mMap = mtd.computeLocalFeatures(radiusTubular)

            taglut.Display3D.displayMeshMapUInt(sizex, sizey, "Display", m, mMap, .1, edges, opengl, gouraud, autolight)
        elif pointsFile != "":
            print("Loading points")
            f = open(pointsFile)
            l2 = f.readlines()
            f.close()
            l2 = [ int(i.replace('\n', '')) for i in l2 if str(int(i.replace('\n', ''))) == i.replace('\n', '') ]
            print(" " + str(len(l2)) + "values")
            mMap = taglut.MeshMap(m, 1)
            for v in l2:
                mMap.setValue(v, 0)
            taglut.Display3D.displayMeshMapTriangles(sizex, sizey, "Display", m, mMap, 0.0, edges, opengl, gouraud, autolight)

        elif functionFile == "":
            taglut.Display3D.displayMesh(sizex, sizey, "Display", m, edges, border, opengl, gouraud, autolight)
        else:
            if simplecut:
                print("Cannot display both scalar function and simple cut. Abort")
                sys.exit(1)
            print("Loading scalar function")
            mMap = taglut.MeshMap(m, functionFile)
            if useLog:
                mMap.applyLog()
            taglut.Display3D.displayMeshMapTriangles(sizex, sizey, "Display", m, mMap, 0.0, edges, opengl, gouraud, autolight)

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
