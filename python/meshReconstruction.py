#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



"""Usage: meshReconstruction.py [options] INPUT

Options:
    -e, --epsilon=EPSILON      Epsilon value to estimate similarities (distances between points)
    --epsilon-angles=EPSILON   Epsilon value to estimate similarities (angles)
    --epsilon-planes=EPSILON   Epsilon value to estimate similarities (distances between planes)
    -t, --wanted-topology=DESC A description of the wanted topology (XML of filename)
    -v, --verbose              Verbose mode
    -p, --output-planes=FILE   Save the polygons using the file FILE
    -i, --output-image=FILE    Save the interslices using an image
    -s, --output-scene=FILE    Save a description of the scene (genus and inclusions)
    -h, --help                 Print this message and exit.

INPUTS is an OFF file where polygons are stored, describing the intersection between planes and the 3D mesh"""


import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt

def usage():
    print(__doc__)
    sys.exit(0)


def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "p:i:hve:s:t:", ["help", "output-planes=", "output-images=", "verbose", "epsilon=",
                                                              "epsilon-planes=", "epsilon-angles=", "output-scene=",
                                                              "wanted-topology="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()

        sys.exit(2)

    outputPlanes = ""
    outputImage = ""
    outputScene = ""
    verbose = False
    epsilon = 1e-4
    epsilonAngle = 1e-2
    epsilonPlane = 1e-6
    wTopology = ""
    for o, a in opts:
        if o in ("-e", "--epsilon"):
            epsilon = float(a)
        if o == "--epsilon-angles":
            epsilonAngle = float(a)
        if o == "--epsilon-planes":
            epsilonPlane = float(a)
        if o in ("-v", "--verbose"):
            verbose = True
        if o in ("-p", "--output-planes"):
            outputPlanes = a
        if o in ("-i", "--output-image"):
            outputImage = a
        if o in ("-s", "--output-scene"):
            outputScene = a
        if o in ("-t", "--wanted-topology"):
            wTopology = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Polygons file not defined")
        usage()
        sys.exit(1)


    print("Load polygons")
    slices = taglut.MeshBySlices(inputFile, epsilon, epsilonAngle, epsilonPlane)

    if verbose:
        print(slices.toString())
        print(slices.toStringCC())
    if verbose or wTopology != "":
        print(slices.toStringCavities())

    if wTopology != "":
        t = slices.getMergeDescription(wTopology, taglut.MergeStepCostPolygonDistance(slices))
        print(t.toString())

    if outputPlanes != "":
        paths = slices.getPaths()
        print("Save the planes (" + outputPlanes + ")")
        taglut.Path3D.savePolygons(paths, outputPlanes)

    if outputImage != "":
        image = slices.getIntersliceImage()
        print("Save the interslice description (" + outputImage + ")")
        image.save(outputImage)

    if outputScene != "":
        sd = slices.getSceneDescription()
        print("Save the scene description (" + outputScene + ")")
        sd.save(outputScene)

if __name__ == "__main__":
    main()
