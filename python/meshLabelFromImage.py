#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



"""Usage: meshLabelFromImage [options] INPUTIMAGE INPUTMESH OUTPUT

Options:
    -d, --depth            Depth of the neigborhood
    -n, --number-of-points Number of points for discretization in the neigborhood
    -h, --help        Print this message and exit.


INPUTIMAGE is a 3D image file
INPUTMESH is a mesh file
OUTPUT is an mesh file"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:h", ["name=", "help"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    inputImageFile = ""
    inputMeshFile = ""
    outputFile = ""
    depth = 5
    nbPoints = 5

    for o, a in opts:
        if o in ("-d", "--depth"):
            depth = int(a)
        if o in ("-n", "--number-of-points"):
            nbPoints = int(a)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputImageFile = args[0]
    except IndexError:
        print("Error: Input image file not defined")
        usage()
        sys.exit()

    try:
        inputMeshFile = args[1]
    except IndexError:
        print("Error: Input mesh file not defined")
        usage()
        sys.exit()

    try:
        outputFile = args[2]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputMeshFile)

        print("Loading image...")
        image = taglut.UCharCImg(inputImageFile)

        # get the barycenter of the mesh
        b = m.getIsobarycenter()

        # TODO

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
