#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: scalarFunctionInfos [options] INPUT

Input options:
    -v, --verbose=N       Verbose modes (default: 0)
    -n, --name=NAME       Object name

INPUT is a mesh file (available input formats: VRML, PLY, ...) and a scalar function file"""

import taglut
import getopt
import sys
import operator

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:f:ho:v:", ["name=", "help", "verbose="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    name = ""
    scalarFunctionFile = ""
    inputFile = ""
    verbose = 0
    for o, a in opts:
        if o in ("-v", "--verbose"):
            verbose = int(a)
        if o in ("-n", "--name"):
            name = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
        scalarFunctionFile = args[1]
    except IndexError:
        print("Error: Input files not defined")
        usage()
        sys.exit()

    try:

        print("Loading file (" + inputFile + ")...")
        m = taglut.Mesh(inputFile, name)
        mMap = taglut.MeshMap(m, scalarFunctionFile)
        vf = taglut.VectorField(mMap, m)

        print("Mesh:")
        print(" - Number of points: " + str(m.getNbPoints()))
        print("Scalar function:")
        print(" - Values: [" + str(mMap.getMinValue()) + ", " + str(mMap.getMaxValue()) + "]")

        ep = mMap.getLocalExtrema()
        ep = [("extrema", extremum, mMap.isLocalMaximum(extremum), mMap.getValue(extremum)) for extremum in ep]
        print(" - Number of extrema: " + str(len(ep)))

        sp = mMap.getSaddlePoints()
        sp = [("saddle", saddlePoint[0], saddlePoint[1], mMap.getValue(saddlePoint[0])) for saddlePoint in sp]
        print(" - Number of saddle points: " + str(len(sp)))

        print(" - Values of critical points:")
        sp += ep
        sp = sorted(sp, key=operator.itemgetter(3))
        for point in sp:
            if point[0] == "extrema":
                if point[2]:
                    print("  - Maximum     : " + str(point[1]) + ": value=" + str(point[3]))
                else:
                    print("  - Minimum     : " + str(point[1]) + ": value=" + str(point[3]))
            else:
                print("  - Saddle point: " + str(point[1]) + ": value=" + str(point[3]) + ", arity: " + str(point[2]))
                if verbose > 0 and point[2] == 2:
                    ils = taglut.PLPath.getIntegralLinesFromSaddlePoint(m.point(point[1]), m, mMap, vf, -1., True)
                    for il in ils:
                        print("    - integral line: " + str(il.length()) + " with " + str(il.size()) + " points")

    except taglut.ExceptionFileNotFound as msg:
        print("Error: ", msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
