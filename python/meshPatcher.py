#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshPatcher.py [options] INPUT

Input options:
    -n, --name=NAME        Object name (in the input file)
    -f, --function=FILE    A scalar function (one line for each vertex, or a volumic image)

Preprocessing:
    --keep-all             Keep all the mesh (do not preserve only the main
                           connected component)

Patch options:
    -b, --one-boundary     Introduce a first step that patch all the boundaries
                           except the largest one. The scalar value of the new
                           points are estimated using a mean of there neighbours
    -l, --length=L         Select length method used to cut surface before
                           patching (or \"list\" for list them)
    --ratio-length=R       Ratio of euclidean length that can be modified by
                           other length methods. 0 <= ratio <= 1
    --optim-method=OM      Method used during the cutting process to optimize
                           the cutting speed. Optimizations of the base point
                           order, and using or not truncated Dijkstra
                           algorithm. Available options: \"no-trunc\", \"trunc-min\",
                           \"trunc-max\", \"trunc-min-max\", \"simple\",
                           \"by-strip\", \"by-strip-and-pred-cut\", \"approximation\".

Output options:
    -o, --output=FILENAME  Output file (available output format: VRML, OFF, OBJ)
    --save-cut=FILENAME    Save a file without the discs (only cuts)

INPUT is a mesh file (available input formats: VRML, PLY, ...)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:f:bl:o:h", ["name=", "function=",
                                                          "one-boundary", "length=", "ratio-length=", "optim-method=",
                                                          "output=", "save-cut=",
                                                          "help", "keep-all"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    name = ""
    function= ""

    oneBoundary = False
    lFactory = taglut.LengthFactory()
    ratio = -1
    length = ""

    output = ""
    saveCut = ""
    keepAll = False
    oMethod = taglut.MeshPathes.OECTruncMinMax


    for o, a in opts:
        # input
        if o in ("-n", "--name"):
            name = a
        if o in ("-f", "--function"):
            function = a
        # preprocessing
        if o == "--keep-all":
            keepAll = True
        # patch options
        if o in ("-b", "--one-boundary"):
            oneBoundary = True
        if o in ("-l", "--length"):
            length = a
            if length == "list":
                print(lFactory.toString())
                sys.exit(1)
        if o == "--ratio-length":
            ratio = float(a)
        if o == "--optim-method":
            if (a == "trunc-min-max"):
                oMethod = taglut.MeshPathes.OECTruncMinMax
            elif (a == "simple"):
                oMethod = taglut.MeshPathes.OECsimple
            elif (a == "by-strip"):
                oMethod = taglut.MeshPathes.OECbyStrip
            elif (a == "by-strip-and-pred-cut"):
                oMethod = taglut.MeshPathes.OECbyStripAndPredCut
            elif (a == "no-trunc"):
                oMethod = taglut.MeshPathes.OECnoTrunc
            elif (a == "trunc-min"):
                oMethod = taglut.MeshPathes.OECTruncMin
            elif (a == "trunc-max"):
                oMethod = taglut.MeshPathes.OECTruncMax
            elif (a == "approximation"):
                oMethod = taglut.MeshPathes.OECApproximation
            else:
               usage()
               print("Unknown optimization method (" + a + ")")
               sys.exit(1)
        # output
        if o in ("-o", "--output"):
            output = a
        if o == "--save-cut":
            saveCut = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if output == "":
        usage()
        print("Error: output file not defined")
        sys.exit()

    lFactory.setLength(length)
    if not lFactory.existsLength():
        usage()
        print("Error: length unknown (try -l list).")
        sys.exit(1)
    if ratio >= 0:
        lFactory.setRatio(ratio)

    if lFactory.needMeshMap() and function == "":
        usage()
        print("The length you selected need a scalar function, but not defined. Abort.")
        sys.exit(1)

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        if taglut.Mesh().isLoadFormat(inputFile):
            # load mesh
            print("Loading mesh (" + inputFile + ")...")
            m = taglut.Mesh(inputFile, name)
            if not keepAll:
                m = taglut.Mesh(m.getLargestCC())
        else:
            print("Unknown format" + inputFile)
            sys.exit(1)

        if function != "":
            print("Loading function (" + function + ")...")
            mMap = taglut.MeshMap(m, function)

        if oneBoundary:
            print("> Area of the mesh before removeing unwanted boundaries: " + str(m.getArea()))
            print("Removing unwanted boundaries")
            nbMax = m.getNbBoundaries() - 1
            mPatch = taglut.MeshPatches(m)
            mPatch.closeBoundaries(nbMax)
            if function != "":
                mMap.addMissingValuesUsingMean()

        print("> Area of the mesh before patching the handles: " + str(m.getArea()))

        print("Patching the surface (genus:" + str(m.getGenus()) + ", boundaries: " + str(m.getNbBoundaries()) + ")")
        if lFactory.needMeshMap():
            mPatch2 = taglut.MeshPatches(m, lFactory.getLength(mMap))
        else:
            if function != "":
                print("Warning: a function file was given, but the selected length method do not use it")
            mPatch2 = taglut.MeshPatches(m, lFactory.getLength(m))
        mPatch2.patchMeshInLoops(-1, oMethod)

        print("> Area of the mesh after patching the handles: " + str(m.getArea()))
        l = 0.0
        for path in mPatch2:
            l += m.getLength(path)
        print("> Euclidean length of the added paths: " + str(l))


        print("Saving mesh (" + output + ")...")
        m.save(output)

        if saveCut != "":
            print("Save cut mesh (" + saveCut + ")...")
            m.removeNonRealPoints()
            m.save(saveCut)

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
