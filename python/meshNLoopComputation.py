#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshNLoopComputation [options] INPUT

Options:
    -n, --name=NAME           Object name
    -d, --display             Display the final mesh
    -o, --output=FILE         Save the mesh using the file FILE
    -1, --first-basepoint=b   ID of the first basepoint
    -2, --second-basepoint=b  ID of the second basepoint
    -a, --arity=NB            Number of paths of the NLoop
    -s, --simulated-annealing Adjust the location using simulated annealing
    --arity-min=NBMIN         Minimal number of paths allowed in the NLoop
    --arity-max=NBMAX         Maximal number of paths allowed in the NLoop
    --temp-init=TEMP          Initial temperature
    --step-size=SIZE          Step size between two temperature modifications
    --temp-coeff=COEF         Coefficient of the geometric progression
    --accept-threshold=TRD    Value for acceptation
    --estimator=METHOD        Method to estimate the quality of an NLoop.
                              Available values: \"length\", \"mean\", \"max\".
    -x, --x=X                 Window size (x)
    -y, --y=Y                 Window size (y)
    -g, --opengl              Use opengl display rather than X11
    -h, --help                Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:do:1:2:ghx:y:a:s", ["name", "help", "opengl", "first-basepoint=", "second-basepoint=", "display",
                                                                    "output=", "x=", "y=", "arity=", "simulated-annealing",
                                                                    "temp-init=", "step-size=", "temp-coeff=", "accept-threshold=",
                                                                    "arity-min=", "arity-max=", "estimator="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    display = False
    outputFile = ""
    opengl = False
    sizex = 300
    sizey = 300
    first = -1
    second = -1
    arity = 3
    aritymin = -1
    aritymax = -1
    simulatedAnnealing = False
    estimator = "length"

    simTempInit = 20.
    simStepSize = 50
    simTempCoeff = .9
    simAcceptThreshold = .05
    for o, a in opts:
        if o == "--temp-init":
            simTempInit = float(a)
        if o == "--step-size":
            simStepSize = int(a)
        if o == "--temp-coeff":
            simTempCoeff = float(a)
        if o == "--accept-threshold":
            simAcceptThreshold = float(a)
        if o in ("-n", "--name"):
            name = a
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-g", "--opengl"):
            opengl = True
        if o in ("-1", "--first-basepoint"):
            first = int(a)
        if o in ("-2", "--second-basepoint"):
            second = int(a)
        if o in ("-a", "--arity"):
            arity = int(a)
        if o == "--arity-min":
            aritymin = int(a)
            if aritymin < 3:
                print("Error: the minimal arity value should be >= 3")
                sys.exit(-1)
            if aritymax >= 0 and aritymin > aritymax:
                print("Error: the minimal arity value should be <= the maximal arity value")
                sys.exit(-1)
        if o == "--arity-max":
            aritymax = int(a)
            if aritymax < 3:
                print("Error: the maximal arity value should be >= 3")
                sys.exit(-1)
            if aritymin >= 0 and aritymin > aritymax:
                print("Error: the maximal arity value should be >= the minimall arity value")
                sys.exit(-1)
        if o in ("-s", "--simulated-annealing"):
            simulatedAnnealing = True
        if o == "--estimator":
            if a == "mean" or a == "length" or a == "max":
                estimator = a
            else:
                print("Error: " + a + " is not a valid value for the estimator")
                sys.exit(-1)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if aritymin < 0 and aritymax > 0:
        print("Warning: minimal arity value not defined. Set it to 3")
        aritymin = 3


    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)


        if first >= m.getNbPoints() or second >= m.getNbPoints():
            print("Wrong id (max: " + str(m.getNbPoints()) + ")")
            sys.exit()


        if aritymin > 0 and aritymax < 0:
            print("Warning: maximal arity value not defined. Set it to 2 * g + b")
            aritymax = 2 * m.getGenus() + m.getNbBoundaries()
        if aritymin > 0 and aritymin == aritymax and aritymin != arity:
            arity = aritymin


        print("Processing...")
        mCut = taglut.MeshCut(m)

        print("Computing NLoop...")
        if simulatedAnnealing:
            if estimator == "length":
                nle = taglut.NLoopEvaluatorLength()
            elif estimator == "mean":
                nle = taglut.NLoopEvaluatorMean()
            else: # estimator == "max"
                nle = taglut.NLoopEvaluatorMax()

            simAnnealing = taglut.SimulatedAnnealingNLoop(m, nle, simTempInit, simStepSize, simTempCoeff, simAcceptThreshold)
            if first < 0:
                first = m.getNbPoints()
            if second < 0:
                second = m.getNbPoints()
            if aritymin == aritymax:
                nloop = simAnnealing.approximateShortestValidNLoopArityFixed(arity, first, second)
            else:
                nloop = simAnnealing.approximateShortestValidNLoop(aritymin, aritymax, first, second)
        else:
            if first == -1:
                print("Automatic selection of the first point is not defined with this method")
                sys.exit()
            if second == -1:
                print("Computing the second basepoint from " + str(first) + "...")
                loop = mCut.getShortestNonTrivialCycleFromPoint(first)

                second = m.getMiddlePoint(loop)
                print(" Selected: " + second)

            #TODO
#             m.tracePoint(first)
#             m.tracePoint(second)
#             wf = taglut.WaveFront(m)
#             print "avant la surface"
#             wf.growSurfaceFull(first)
#             print "apres la surface"
#             paths = wf.getBorderPaths()
#             print "recuperation des chemins"
#             for path in paths:
#                 mCut.addPath(path)
#             m2 = mCut.cutMesh()
#             taglut.Display3D.displayMesh(sizex, sizey, "Display", m2, False, True, opengl)
#             sys.exit()

            nloop = mCut.getShortestValidNLoop(first, second, arity)
        print("Number of paths:" + str(nloop.getNbPaths()) + ", longueur: " + str(m.getLength(nloop)))
        mCut.addNLoop(nloop)
        m2 = mCut.cutMesh()


        if (display):
            taglut.Display3D.displayMesh(sizex, sizey, "Display", m2, False, True, opengl)
        if (outputFile != ""):
            m2.save(outputFile)
    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
