#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2013 Jean-Marie Favreau <J-Marie.Favreau@udamail.fr>
#                    ISIT, UMR 6284 UdA – CNRS
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshVeinsPipeline [options] INPUT

Options:
    -x, --x=X                       X coordinate of the center of ROI
    -y, --y=Y                       Y coordinate of the center of ROI
    -z, --z=Z                       Z coordinate of the center of ROI
    -r, --radius=R                  Radius of the ROI
    -s, --simple-cut                If r is not defined, force to cut using simple cut
    --remove-poles=PERCENT          Remove the poles (following the main PCA axis), using
                                    a percentage of the length between the two poles
    --remove-pole1=PERCENT          Remove the 1st pole (following the main PCA axis), using
                                    a percentage of the length between the two poles
    --remove-pole2=PERCENT          Remove the 2nd pole (following the main PCA axis), using
                                    a percentage of the length between the two poles
    -u, --unfold-method=METHOD      Unfolding method: "ABF", "cylinder-projection" (default)
    -i, --output-image=FILE         Save the unfolded mesh as an image
    --sizex=X                       Size of the image (X)
    --sizex=Y                       Size of the image (Y)
    -m, --output-uv=FILE            Save the unfolded mesh as an UV file (format PLY,
                                    containing also the scalar values)
    -c, --control-image=FILE        Save an image that contains triangle distorsion,
                                    for control
    -h, --help                      Print this message and exit.

INPUT is a mesh file with associated scalar values (available formats: PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "x:y:z:r:i:m:hc:u:s", ["x=", "y=", "z=",
                                                               "radius=", "output-image=", "output-uv=", "help",
                                                               "sizex=", "sizey=", "control-image=",
                                                               "unfold-method=", "simple-cut",
                                                               "remove-poles=", "remove-pole1=",
                                                               "remove-pole2="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    sizex=512
    sizey=512
    outputImage = ""
    outputMapping = ""
    controlImage = ""
    unfoldMethod = 0
    x = 0
    y = 0
    z = 0
    r = 0
    rPoles = 0.
    rPole1 = 0.
    rPole2 = 0.
    coords = 0
    s = False
    roi = False
    for o, a in opts:
        if o in ("-x", "--x"):
            x = float(a)
            coords += 1
        if o in ("-y", "--y"):
            y = float(a)
            coords += 2
        if o in ("-z", "--z"):
            z = float(a)
            coords += 4
        if o == "--sizex":
            sizex = int(a)
        if o == "--sizey":
            sizey = int(a)
        if o in ("-r", "--radius"):
            r = float(a)
        if o in ("-u", "--unfold-method"):
            if (a == "ABF") or (a == "abf"):
                unfoldMethod = 1
            elif (a == "cylinder-projection"):
                unfoldMethod = 0
            else:
                print("Unknown unfold method")
                print("Abort.")
                sys.exit()
        if o in ("-s", "--simple-cut"):
            s = True
        if o in ("-i", "--output-image"):
            outputImage = a
        if o in ("-m", "--output-mapping"):
            outputMapping = a
        if o == "--remove-poles":
            rPoles = float(a)
        if o == "--remove-pole1":
            rPole1 = float(a)
        if o == "--remove-pole2":
            rPole2 = float(a)
        if o in ("-c", "--control-image"):
                controlImage = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    if coords != 7 or r == 0:
        print("Warning: no RIO defined. Use the full mesh")
    else:
        roi = True

    try:

        # load the mesh
        print("Load mesh and MeshMap")
        plyLoader = taglut.PLYLoader(inputFile)
        mesh = taglut.Mesh()
        mmap = taglut.MeshMap(mesh)
        plyLoader.loadMeshFromPLY(mesh)
        print(" Number of vertices: " + str(mesh.getNbPoints()))
        print(" Number of faces: " + str(mesh.getNbTriangles()))
        plyLoader.loadMeshMapFromPLY(mmap)
        print(" Number of scalar values: " + str(mmap.getNbValues()))

        barycenter = mesh.getIsobarycenter()
        axis = mesh.computePCAAxes()[0].getNormalize()
        center = taglut.Point3D(x, y, z)

        if rPoles != 0. or rPole1 != 0. or rPole2 != 0.:
            print("Remove poles")
            mpoles = taglut.MeshPart(mesh)
            if rPoles != 0.:
                mspoles = mpoles.buildRemovePoles(rPoles, axis, barycenter)
            else:
                mspoles = mpoles.buildRemovePoles(rPole1, rPole2, axis, barycenter)
            print(" Number of vertices: " + str(mesh.getNbPoints()))
            print(" Number of faces: " + str(mesh.getNbTriangles()))
            mmap = taglut.MeshMap(mspoles, mpoles, mmap)
            mesh = mspoles


        if r != 0:
            print("Crop the mesh")
            mpart = taglut.MeshPart(mesh, center, r, True)
            disc = mpart.buildCropMesh()
            discmap = taglut.MeshMap(disc, mpart, mmap)
        elif s:
            print("Cut the mesh (simple cut)")
            mpart = taglut.MeshPart(mesh, True)
            disc = mpart.buildCropMeshSimpleCut(mesh.getNearestPoint(center))
            discmap = taglut.MeshMap(disc, mpart, mmap)
        else:
            disc = mesh
            discmap = mmap

        print(" Number of vertices: " + str(disc.getNbPoints()))
        print(" Number of faces: " + str(disc.getNbTriangles()))

        print("Unfold mesh")
        mapping = taglut.Mapping2D3D(disc)
        if unfoldMethod == 1:
            uMethod = taglut.ABFMethod(mapping)
            mSUnfolding = taglut.MultiscaleUnfolding(False, 7e-8, 30, True)
            nbIter = mSUnfolding.processUnfolding(uMethod, 0)
        elif unfoldMethod == 0:
            uMethod = taglut.CylinderProjection(mapping, axis, barycenter)
            uMethod.processUnfolding(1e-7, 1)
            uMethod.compute2DLocation(taglut.IndentManager())
            nbIter = 1

        print("Save unfolded mesh (" + outputMapping +")")
        plySaver = taglut.PLYSaver()
        plySaver.savePLY(mapping, discmap, outputMapping)

        print("Save unfolded surface as image (" + outputImage +")")
        result = taglut.UCharCImg(sizey, sizey, 1, 1)
        result.fill(0)
        mapping.toImageFromMap(result, discmap)
        result.save(outputImage)
        if controlImage != "":
            print("Save control image (" + controlImage +")")
            result = taglut.UCharCImg(sizey, sizey, 1, 1)
            result.fill(0)
            mapping.toImageDrawDistortions(result)
            result.save(controlImage)


        if nbIter < 0:
            print("Abording.")
            sys.exit(2)

    except taglut.Exception as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
