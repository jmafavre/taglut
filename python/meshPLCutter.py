#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
#                    ISIT, Université d'Auvergne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshPLCutter [options] INPUT

Options:
    -n, --name                      Object name
    -1, --first-vertex=V1           First vertex of the path
    -2, --second-vertex=V2          Seconf vertex of the path
    -d, --display                   Display the final mesh
    -i, --infos                     Display information about final mesh
    -o, --output=FILE               Save the mesh using the file FILE
    -x, --x=X                       3d window size (x)
    -y, --y=Y                       3d window size (y)
    -h, --help                      Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "nhx:y:do:1:2:i", ["name", "help", "x=", "y=", "display", "output=", "first-vertex=", "second-vertex=", "infos"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    display = False
    outputFile = ""
    sizex = 600
    sizey = 600
    firstVertex = 0
    secondVertex = -1
    infos = False
    for o, a in opts:
        if o in ("-i", "--infos"):
            infos = True
        if o in ("-1", "--first-vertex"):
            firstVertex = a
        if o in ("-2", "--second-vertex"):
            secondVertex = a
        if o in ("-n", "--name"):
            name = a
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)

        print("Create path...")
        if firstVertex < 0 or firstVertex >= m.getNbPoints():
            print(" Bad value for the first point. Value selected: 0")
            firstVertex = 0
        print(" First point: " + str(firstVertex))
        if secondVertex < 0 or secondVertex >= m.getNbPoints():
            mManip = taglut.MeshManipulator(m)
            secondVertex = mManip.getOppositePoint(firstVertex)
            print(" Init second point using the opposite point:" + str(secondVertex))
        else:
            print(" Second point: " + str(secondVertex))
        gDist = taglut.FastMarching()
        path = gDist.getShortestPath(m, firstVertex, secondVertex)

        print("Cut mesh...")
        plCut = taglut.MeshPLCut()
        m2 = plCut.cutMesh(m, path)

        if (infos):
            print(m2.getInfos())
        if (display):
            print("Display...")
            taglut.Display3D.displayMesh(sizex, sizey, "Display", m2, False, True)
        if (outputFile != ""):
            print("Save mesh (" + outputFile + ")...")
            m2.save(outputFile)
    except taglut.Exception as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
