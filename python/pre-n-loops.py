#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



"""Usage: pre-n-loops.py [options] INPUTS

Options:
    -m, --merge        Merge the 3-loops
    -n, --n-loops      Compute the n-loops (not only the pre-n-loops)
    -p, --pl-n-loops   Compute the PL n-loops (not only the pre-n-loops)
    -h, --help         Print this message and exit.

INPUTS is a mesh file, and a scalar function on the vertices"""


import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt

def usage():
    print(__doc__)
    sys.exit(0)


def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "mhnp", ["merge", "n-loops", "pl-n-loops", "help"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: ", msg)
        usage()
        sys.exit(2)

    merge = False
    nLoops = False
    plnLoops = False
    for o, a in opts:
        if o in ("--merge", "-m"):
            merge = True
        if o in ("--n-loops", "-n"):
            nLoops = True
        if o in ("--pl-n-loops", "-p"):
            plnLoops = True
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Mesh file not defined")
        usage()
        sys.exit()

    try:
        functionFile = args[1]
    except IndexError:
        print("Error: Function file not defined")
        usage()
        sys.exit()

    m = taglut.Mesh(inputFile)
    mmap = taglut.MeshMap(functionFile)
    mmap.setMesh(m)
    mmap.removeFlatEdges()
    vfield = taglut.VectorField(mmap, m)

    nlf = taglut.NLoopFactory()
    if nLoops:
        print("n-loop generation merging 3-loops")
        nl = nlf.getNLoopsFromSaddlePoints(m, mmap, vfield)
        for p in nl:
            print("Found an n-loop. Arity:", p.getNbPaths())
            for pp in p.getPaths():
                print(" found a path with", len(pp), "points")
    elif plnLoops:
        print("pl-n-loop generation merging 3-loops")
        nl = nlf.getPLNLoopsFromSaddlePoints(m, mmap, vfield)
        for p in nl:
            print("Found an pl-n-loop. Arity:", p.getNbPaths())
            for pp in p.getPaths():
                print(" found a path with " + pp.size() + " points")
    elif merge:
        print("pre-n-loop generation merging 3-loops and building the associated paths")
        pnl = nlf.getPreNLoopsFromSaddlePoints(m, mmap, vfield)
        for p in pnl:
            print("Found an n-loop. Arity:" + str(p.arity()) + " well-ordered:" + str(p.isWellOrdered(mmap)))
            print(" " + p.toString())
            for pp in p.getLines(mmap):
                print(" found a path with" + str(len(pp)) + "points")
    else:
        print("3-loop generation")
        sp = mmap.getSaddlePoints()
        for p in sp:
            print("check for paths from" + str(p[0]))
            try:
                preNLoop = nlf.getPre3LoopFromSaddlePoint(m.point(p[0]), m, mmap, vfield)
                for path in preNLoop.getLines(mmap):
                    print(" found a path with" + str(len(path)) + "points")
            except:
                print("skip...")


if __name__ == "__main__":
    main()
