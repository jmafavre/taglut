#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
#                    Universite d'Auvergne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import taglut
import sys

def testGenus(filename, o, listOfGenus):
    if not len(listOfGenus) == o.getNbCC():
        print("Fail: " + filename + ": bad number of connected components")
        print(o.toStringCC())
        return False
    i = 0
    for g in listOfGenus:
        if not g == o.getGenus(i):
            print("Fail: " + filename + "(" + str(i) + "): bad genus")
            print(o.toStringCC())
            return False
        i += 1
    print(" Pass")
    return True

def testScene(filename, o, sdesc):
    if not o.getSceneDescription().similar(sdesc):
        print("Fail: " + filename + ": bad structure")
        print(o.toStringCC())
        return False
    print(" Pass")
    return True

def testTargetScene(filename, o, sdesc):
    if not o.isPossibleMergedScene(sdesc):
        print("Fail: " + filename + ": structure do not match with the target one")
        print(o.toStringCC())
        return False
    if not o.getMergeDescription(sdesc, taglut.MergeStepCostPolygonDistance(o)).similar(taglut.SceneDescriptor(sdesc)):
        print("Fail: " + filename + ": bad merging reconstruction")
        print(o.toStringCC())
        return False
    print(" Pass")
    return True

def testAll(filename, listOfGenus, sdesc, tsdesc):
    print("testAll(" + filename + ")")
    o = taglut.MeshBySlices(filename)
    if not testGenus(filename, o, listOfGenus):
        return False
    if not testScene(filename, o, sdesc):
        return False
    if not testTargetScene(filename, o, tsdesc):
        return False
    return True


if not testAll("../../../data/2spheres-slices.off", [0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"/><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/2spheres-slices-small.off", [0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"/><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/2spheres-slices-small2.off", [0, 0, 0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"/><object genus=\"0\"/><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"/><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/cube-slices.off", [0],
               "<scene><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/cube-slices-small.off", [0],
               "<scene><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/cube-slices-small2.off", [0, 0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"/><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/cavity-slices.off", [0, 0, 0, 0, 0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"><object genus=\"0\"/></object><object genus=\"0\"/><object genus=\"0\"/><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"><object genus=\"0\"/></object></scene>"):
    sys.exit(1)

if not testAll("../../../data/cavity-slices-small.off", [0, 0, 0, 0, 0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"><object genus=\"0\"/><object genus=\"0\"/><object genus=\"0\"/></object><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"><object genus=\"0\"/></object></scene>"):
    sys.exit(1)

if not testAll("../../../data/cavities-1-slices.off", [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               "<scene><object genus=\"0\"/><object genus=\"0\"><object genus=\"0\"/><object genus=\"0\"><object genus=\"0\"><object genus=\"0\"/></object><object genus=\"0\"/></object><object genus=\"0\"/><object genus=\"0\"/></object><object genus=\"0\"/><object genus=\"0\"/><object genus=\"0\"/></scene>",
               "<scene><object genus=\"0\"><object genus=\"0\"><object genus=\"0\"/></object><object genus=\"0\"/></object><object genus=\"0\"/><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/cavities-2-slices.off", [0, 0, 1],
               "<scene><object genus=\"1\"><object genus=\"0\"/></object><object genus=\"0\"/></scene>",
               "<scene><object genus=\"1\"><object genus=\"0\"/></object><object genus=\"0\"/></scene>"):
    sys.exit(1)

if not testAll("../../../data/donut-slices.off", [1],
               "<scene><object genus=\"1\"/></scene>",
               "<scene><object genus=\"1\"/></scene>"):
    sys.exit(1)


sys.exit(0)
