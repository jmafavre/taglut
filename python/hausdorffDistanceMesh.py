#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal / IMATI CNR
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: hausdorffDistanceMesh [options] MESH SET1 SET2

Options:
    -n, --name         Object name

MESH is a mesh file (available formats: VRML, PLY, ...)
SET1 and SET2 are two files that contains the IDs of points contained by the
mesh
"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:h", ["name=", "help" ])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    name = ""
    for o, a in opts:
        if o in ("-n", "--name"):
            name = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputMesh = args[0]
        inputSet1 = args[1]
        inputSet2 = args[2]
    except IndexError:
        print("Error: Input file(s) not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh")
        m = taglut.Mesh(inputMesh, name)

        print("Loading first set")
        f = open(inputSet1)
        l1 = f.readlines()
        f.close()
        l1 = [ int(i.replace('\n', '')) for i in l1 if str(int(i.replace('\n', ''))) == i.replace('\n', '') ]
        print(" " + str(len(l1)) + "values")

        print("Loading second set")
        f = open(inputSet2)
        l2 = f.readlines()
        f.close()
        l2 = [ int(i.replace('\n', '')) for i in l2 if str(int(i.replace('\n', ''))) == i.replace('\n', '') ]
        print(" " + str(len(l2)) + "values")
        mManip = taglut.MeshManipulator(m)

        print("Hausdorff distance: " + str(mManip.getHausdorffDistance(l1, l2)))


    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
