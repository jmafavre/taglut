#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
#                    ISIT, Université d'Auvergne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Usage: meshGetPathsBetweenBorderPoints.py [options] INPUT

Options:
    -n, --name                      Object name
    -s, --simple                    Simple version, without description of the new points
    -o, --output=FILE               Save the mesh using the file FILE
    -h, --help                      Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "no:hs", ["name", "help", "output=", "simple"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    outputFile = ""
    simple = False
    for o, a in opts:
        if o in ("-n", "--name"):
            name = a
        if o in ("-s", "--simple"):
            simple = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    if outputFile == "":
        print("Error: Output file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)

        bpoints = sorted(list(m.getBoundaryPoints()))

        dTool = taglut.FastMarching()
        mMap = taglut.MeshMap(m)

        filout = open(outputFile, 'w')

        filout.write("# This file contains a set of paths between two vertices on a given mesh (" + inputFile + ")\n")
        filout.write("# Each path is described as following:\n")
        if not simple:
            filout.write("#  * the first line contains 5 number\n")
        filout.write("#    * the ID of the first vertex\n")
        filout.write("#    * the ID of the last vertex\n")
        filout.write("#    * the length of the path\n")
        filout.write("#    * the number of points between the two vertices\n")
        if not simple:
            filout.write("#  * a line per new point between the two vertices, described as following:\n")
            filout.write("#    * 3 values (x, y, z) for the coordinates\n")
            filout.write("#    * 2 vertex IDs (may be equal in case of point on an existing vertex)\n")
            filout.write("#      corresponding to the edge where the point is located.\n")
        filout.write("# Note: the ID of the first vertex is 1.\n")


        for bpoint in bpoints:
            dTool.computeDistanceFromPoint(mMap, m, bpoint)
            vf = taglut.VectorField(mMap, m)

            for bpoint2 in bpoints:
                if bpoint2 > bpoint:
                    path = taglut.PLPath(taglut.PointOnEdge(m.point(bpoint2)), False, m, mMap, vf)
                    if path.size() >= 2:
                        filout.write(str(bpoint + 1) + " " + str(bpoint2 + 1) + " " + str(path.length()) + " " + str(path.size() - 2) + "\n")
                        if not simple:
                            plist = list(path.getPoints())
                            del plist[0]
                            del plist[-1]
                            for p in plist:
                                filout.write(str(p.getX()) + " " + str(p.getY()) + " " + str(p.getZ()) + " " + str(p.getFirstVertex() + 1) + " " + str(p.getSecondVertex() + 1) + "\n")

    except taglut.Exception as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
