#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: normalComparison [options] INPUT1 INPUT2

Options:
    -t, --threshold=T Threshold for angle distortion (in degrees)
    -m, --mesh=MESH   Mesh used to display the result
    -h, --help        Print this message and exit.

INPUT1 and INPUT2 are point clouds with normals (PLY)"""

import taglut
import getopt
import sys
import math

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "hm:t:", ["help", "mesh=", "threshold="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print "Error: ", msg
        usage()
        sys.exit(2)

    threshold = -1
    mesh = ""
    for o, a in opts:
        if o in ("-t", "--threshold"):
            threshold = float(a) / 180 * math.pi
            print("Threshold: " + threshold + " radians")
        if o in ("-m", "--mesh"):
            mesh = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile1 = args[0]
        inputFile2 = args[1]
    except IndexError:
        print "Error: Input file not defined"
        usage()
        sys.exit()

    if mesh == "":
        print "Error: the mesh is not defined"
        usage()
        sys.exit()

    try:
        print "Loading point clouds"
        cloud1 = taglut.PointCloud(inputFile1)
        cloud2 = taglut.PointCloud(inputFile2)
        m = taglut.Mesh(mesh)

        if cloud1.size() != cloud2.size():
            print "Error: the two point clouds are not containing the same number of points"
            sys.exit()
        if not cloud1.hasNormals():
            print "Error: the first point cloud do have not normals"
            sys.exit()
        if not cloud2.hasNormals():
            print "Error: the second point cloud do have not normals"
            sys.exit()

        if cloud1.size() != m.getNbPoints():
            print "Error: the given mesh has not the good number of points"
            sys.exit()

        mMap = taglut.MeshMap(m)
        for p in range(0, cloud1.size()):
            angle = taglut.Coord3D.angleVector(cloud1.getNormal(p), cloud2.getNormal(p))
            angle2 = taglut.Coord3D.angleVector(-cloud1.getNormal(p), cloud2.getNormal(p))
            if angle2 < angle:
                angle = angle2
            if threshold > 0:
                mMap.setValue(p, angle > threshold)
            else:
                mMap.setValue(p, angle)

        if threshold > 0:
            margin = 0.2
        else:
            margin = 0.0
        taglut.Display3D.displayMeshMapDouble(512, 512, "angles", m, mMap, margin)

    except taglut.ExceptionFileNotFound as msg:
        print "Error: ", msg.getMessage()
        sys.exit()

if __name__ == "__main__":
    main()
