#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshVoxelizer [options] INPUT

Basic Options:
    -n, --name        Object name

    -v, --max-voxel=M Maximum number of voxels along an axis
    -b, --binarize    Compute a binary image rather than the 3-value image (in, out and crossing)
    -m, --margin=NB   Add a margin of NB voxels

    -d, --display     Display the voxelized mesh
    -i, --infos       Write informations on the standard output (volumic PCA
                      axis, volumic isobarycenter)
    -o, --output=FILE Save bitmap file
    -h, --help        Print this message and exit.

Display Options:
    -s, --size=SIZE   Maximum size of the voxelized mesh for display.

INPUT is a mesh file (available formats: VRML, PLY)
FILE is a 3D bitmap file (available formats: dicom, cimg)
"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:dho:s:v:ibm:", ["name=", "help", "output=", "size=",
                                                              "max-voxel=", "infos", "binarize", "margin="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    name = ""
    display = False
    outputFile = ""
    voxelNb = 30
    size = 320
    infos = False
    binarize = False
    margin = 1
    for o, a in opts:
        if o in ("-s", "--size"):
            size = int(a)
        if o in ("-n", "--name"):
            name = a
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-v", "--max-voxel"):
            voxelNb = int(a)
        if o in ("-i", "--infos"):
            infos = True
        if o in ("-b", "--binarize"):
            binarize = True
        if o in ("-m", "--margin"):
            margin = int(a)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if not display and outputFile == "" and not infos:
        usage()
        print("Error: output method not selected (\"output\", \"infos\" or \"display\")")
        sys.exit(1)

    try:
        inputFile = args[0]
    except IndexError:
        usage()
        print("Error: Input file not defined")
        sys.exit(1)

    try:
        m = taglut.Mesh(inputFile, name)
        print("Closing boundaries...")
        mPatcher = taglut.MeshPatches(m)
        mPatcher.closeBoundaries()
        mVox = taglut.MeshVoxelizer(m, voxelNb)
        if outputFile != "":
            print("Saving file (" + outputFile + ")")
            if binarize:
                img = mVox.getBinaryVoxelization(-1, margin)
            else:
                img = mVox.getVoxelization(voxelNb, margin)
            img.save(outputFile)
        if infos:
            print("Volumic informations:")
            isoBarycenter = mVox.getVolumicIsoBarycenter()
            print(" Isobarycenter: (" + str(isoBarycenter.getX()) + ", " + str(isoBarycenter.getY()) + ", " + str(isoBarycenter.getZ()) + ")")
            volumicPCAAxes = mVox.getVolumicPCAAxes()
            print(" PCA Axes:")
            for axis in volumicPCAAxes:
                print("  Axis (" + str(axis.getX()) + ", " + str(axis.getY()) + ", " + str(axis.getZ()) + ")")
        if display:
            print("Display voxelization")
            mVox.display(size, margin, binarize)

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
