#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: cylinderCutter [options] INPUT

Basic options:
    -n, --name=NAME          Object name

Unfolding options:
    --epsilon=E              Epsilon (used in unfolding algorithms)
    -m, --maxIter=M          Maximum iteration (used in unfolding algorithms)

Path options:
    -i, --id=ID              ID of the point in the boundary region

Output options:
    -o, --output=FILE        Save the mesh using the file FILE
    -d, --display            Display the final mesh
    -x, --x=X                Window size (x)
    -y, --y=Y                Window size (y)
    -g, --opengl             Use opengl display rather than X11
    -e, --edges              Display edges rather than triangles

    -h, --help               Print this message and exit.

INPUT is a mesh file containing a cylinder (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:o:dx:y:gehm:i:",
                               ["name=",
                                "epsilon=", "maxIter=",
                                "id="
                                "output=", "display", "x=", "y=", "opengl", "edges", "help"])
    except getopt.GetoptError as msg:
        # print help information and exit:
        usage()
        print("Error: " + msg)
        sys.exit(2)


    epsilon = 1e-8
    maxIter = 30
    inputFile = ""
    objectName = ""
    outputFile = ""
    display = False
    sizex = 512
    sizey = 512
    openGL = False
    edges = False
    idPoint = -1
    for o, a in opts:
        if o in ("-n", "--name"):
            objectName = a
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-d", "--display"):
            display = True
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-g", "--opengl"):
            openGL = True
        if o in ("-e", "--edges"):
            edges = True
        if o == "--epsilon":
            epsilon = float(a)
        if o in ("-m", "--maxIter"):
            maxIter = int(a)
        if o in ("-i", "--i"):
            idPoint = int(a)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        usage()
        print("Error: Input file not defined")
        sys.exit(1)

    if outputFile == "" and not display:
        usage()
        print("Error: Output method not defined")
        sys.exit(1)


    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile, objectName)
        if m.getGenus() != 0:
            print("Error: this mesh is not a cylinder (genus != 0)")
            sys.exit(2)
        if m.getNbBoundaries() != 2:
            print("Error: this mesh is not a cylinder (nb boundaries != 2)")
            sys.exit(2)

        print("Unfolding cylinder...")
        abfs = taglut.ABFSolver(m)
        abfs.processUnfolding(epsilon, maxIter)

        print("Computing lengths...")
        l = abfs.getLengths()

        print("Selecting a boundary point")
        if idPoint < 0:
            p = m.point(m.getBoundaryPoint())
        else:
            bp = m.getBoundaryPoints()
            if idPoint >= len(bp):
                print("ID out of range")
                sys.exit(4)
            p = m.point(bp[idPoint])
        plist = taglut.IDDeque()
        plist.push_back(p.getId())


        print("Compute the shortest path between this point and the other boundary")
        m.setTriangleFlag(0)
        m.setPointFlag(0)
        m.setBoundaryPointFlag(1)
        mManip = taglut.MeshManipulator(m, taglut.LengthWeightedEdges(l))
        mManip.computeBoundaryCC(p.getId(), 1, 2, 0)
        bs = mManip.getBoundaries()
        if len(bs) != 2:
            print("Error: more than two boundaries.")
            sys.exit(3)
        if m.point(bs[0][0]).getFlag() == 2:
            path = mManip.computeShortestPath(plist, bs[0])
        else:
            path = mManip.computeShortestPath(plist, bs[1])

        print("Cut the mesh according to the path")
        mCut = taglut.MeshPathes(m)
        mCut.addPath(path)
        mend = mCut.cutMesh()

        if display:
            taglut.Display3D.displayMesh(sizex, sizey, "Display", mend, edges, True, openGL)
        else:
            print("Save file (" + outputFile + ")")
            mend.save(outputFile)

    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit(1)


if __name__ == "__main__":
    main()

