#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: mappingConverter [options] INPUT

Options:
    -n, --name=NAME       Object name
    -o, --output=FILENAME Output file (available output format: OBJ, MAP)

INPUT is a mesh file (available input formats: OBJ, MAP)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "n:ho:", ["name=", "help", "output="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    name = ""
    output = ""
    for o, a in opts:
        if o in ("-n", "--name"):
            name = a
        if o in ("-o", "--output+"):
            output = a
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if output == "":
        usage()
        print("Error: output file not defined")
        sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:

        m = taglut.Mesh()
        map = taglut.Mapping2D3D(m)

        if map.isSaveFormat(output):
            print("Loading file (" + inputFile + ")...")
            map.load(inputFile, name)

            print("Saving file(" + output + ")...")
            map.save(output)
        else:
            print("Unknown output format")
    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
