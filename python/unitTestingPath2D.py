#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
#                    Universite d'Auvergne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import taglut
import sys


path = taglut.Path2D()

path.push_back(taglut.Coord2D(0, 0))
path.push_back(taglut.Coord2D(0, 1))
path.push_back(taglut.Coord2D(1, 1))
path.push_back(taglut.Coord2D(1, 0))
path.push_back(taglut.Coord2D(0, 0))

if not path.isInside(taglut.Coord2D(0.5, 0.5)):
    print("Error (point should be inside)")
    sys.exit(-1)

if path.isInside(taglut.Coord2D(1.5, 1.5)):
    print("Error (point should be outside)")
    sys.exit(-1)

print("Pass")
sys.exit(0)
