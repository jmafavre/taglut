===== USAGE =====

After the Taglut building, all the classes are available on a module named taglut.
For a local usage, the environment variable should be set:
PYTHONPATH=$ROOT_UTOOL/lib/
where $ROOT_UTOOL is the root directory of taglut
