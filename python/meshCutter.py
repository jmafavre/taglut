#!/usr/bin/env python3.2mu
# -*- coding: utf-8 -*-
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshCutter [options] INPUT

Basic Options:
    -n, --name               Object name
    -m, --method=METHOD      Method used for cutting the given mesh. Available
                             methods: \"simple\", \"topologic\", \"PCA\", \"organic\",
                             \"quadrangulation\", \"cylindric\", \"quad-by-cylinders\",
                             \"organic-by-cylinders\".
    -l, --length=L           Select length method used to cut surface (or
                             \"list\" for list them)
    --ratio-length=R         Ratio of euclidean length that can be modified by
                             other length methods. 0 <= ratio <= 1
    --clean                  Remove points with bad topological property berfore
                             cutting, patching, selecting connected components
                             and/or closing boundaries
    -p, --patch              Patch before cutting
    -b, --close-boundaries   Close boundaries before cutting the mesh
    --maximum-cut=MAX        Maximum cutting iteration (used by \"topologic\" and
                             \"quadrangulation\" method)

    --cc-by-size             Select the connected component by size (Default)
    --cc-by-id=ID            Select the connected component by size
    --cc-no-selection        All the connected components are kept

    --unstick-method=METHOD  Method used fot unsticking pathes from boundaries.
                             Available methods: \"none\", \"fast\",
                             \"preserve-mesh\" (default).

    --log-mesh               Save intermediate meshes after each cutting or
                             patching step.
    --log-mesh-prefix=PREFIX Prefix of the log meshes.

Options for the \"patch\" step:
    --maximum-patch=MAX      Maximum patching steps
    --maximum-patch-length=M Maximum patching length
    --patch-length-ratio=L   Ratio used by the non euclidean length base on
                             local curvature. 0 <= L <= 1

Options for \"PCA\" and \"organic\" method:
    -a, --pca-axis=AXIS      Axis used to select points on the PCA method.
                             Available values: \"first\", \"second\", \"third\"
                             Default: first.
    --direction=DIR          Direction used to create cut between selected
                             points. Available values: \"front\", \"back\",
                             \"top\", \"bottom\", \"left\", \"right\", \"none\"
                             Default: none
    -v, --volumic=BOOLEAN    Volumic PCA method (using voxelization). Default: 1
    --voxel-number=MAXSIZE   Number of voxels in the largest direction for the
                             voxelization method. Default: 30

Options for \"organic\" and \"organic-by-cylinders\" method:
    --organic-preprocess=PP  Preprocessing for non trivial surfaces, with i.e.
                             borders. Available methods: \"patch\", \"cut\",
                             \"none\". If cutting is applied on a surface with
                             boundary, the PCA method is not applied next.
                             Default: \"patch\".
    -u, --unfolding-method=M Unfolding method used to compute clusters.
                             Available methods: abf, circle-packing.
    --epsilon=E              Epsilon value used by the unfolding method applyed
                             during the cutting process
    --max-iteration=MAX      Maximum iteration of the unfolding method applyed
                             during the cutting process
    --organic-method=OM      Method used by the organic cutting to find the
                             extrema of the cutting step. Available:
                             \"by-cluster\", \"from-border\", \"simple-extrema\",
                             \"none\".
                             Default: \"from-border\".
    --epsilon-extrema=EEX    Epsilon value for simple extrema method.
    --clustering-method=CM   Clustering method. Available values:
                             \"ratio-simple\", \"ratio-startall\", \"ratio-log\",
                             \"curvature\", \"curvature-or-ratio\",
                             \"curvature-or-ratio-log\", \"curvature-and-ratio\",
                             \"curvature-and-ratio-log\".
    -r, --cluster-ratio=R    Ratio used for cluster selection. 10000 is a good
                             idea of a common value, corresponds to the ratio
                             distance3D / distance2D, only clusters with higher
                             ratio are selected. Default: none. If R < 0, no
                             selection is done.
    --extrema-number=N       Number of extrema that will be selected. Default:
                             no selection by number.
    --cluster-maxcurv=MC     Maximum curvature value allowed during the growing
                             process of the clusters. 0 <= MC <= PI (0: flatten,
                             PI: maximum curve. Default: 0.2)
    --from-border-length=LM  Length method used to compute the distance to the
                             border. Available methods: \"euclidean\",
                             \"euclidean-2d\", \"ratio\", \"ratio-inverse\".
                             Default: \"ratio\".
    --from-border-logfile=F  Log the ratios on the given file F.
    --from-border-smooth     Adding a smoothing step for lengths before
                             computing the maxima using the \"from-border\"
                             method.
    --from-border-use-mean   Using mean of the ratio of the points as a
                             threshold to select the maxima.
    --from-border-use-median Using median of the ratio of the points as a
                             threshold to select the maxima.

Options for \"topologic\" and \"quadrangulation\" method:
    --cutting-method=CM      Method used during the topological step.
                             methods: \"Erickson\", \"Erickson+\" (default), \"old\".
    --merge-multipoints=MMP  Method used to merge multipoints. Available
                             methods: \"none\"  (default), \"simple\", \"one-point\".
    --optim-method=OM        Method used during the cutting process to optimize
                             the cutting speed. Optimizations of the base point
                             order, and using or not truncated Dijkstra
                             algorithm. Available options: \"no-trunc\", \"trunc-min\",
                             \"trunc-max\", \"trunc-min-max\", \"simple\",
                             \"by-strip\", \"by-strip-and-pred-cut\", \"approximation\".

Options for \"cylindric\", \"quad-by-cylinders\" and \"organic-by-cylinders\"
   methods:
    --temp-init=TI           Initial temperature.
    --step-size=SSIZE        Step size between two temperature modifications.
    --temp-coeff=TC          Coefficient of the geometric progression.
    --accept-threshold=AT    Value for acceptation.
    --temp-min=TM            Minimal temperature value.
    --sa-method=SM           Selection method for the next NLoop. Available
                             methods: \"simple\", \"by-middle\".
    --arity-min=NBMIN        Minimal number of paths allowed in the NLoop
    --arity-max=NBMAX        Maximal number of paths allowed in the NLoop
    --estimator=METHOD       Method to estimate the quality of an NLoop.
                             Available values: \"length\", \"mean\", \"max\".

Options for \"quad-by-cylinders\":
    --quad-method=METHOD     Method to split a cylinder into quads. Available
                             values: \"simple\", \"ABF\", \"Floater\".

Output options:
    -d, --display            Display the final mesh
    --display-unfolded       Display the unfolded mesh used during the organic
                             method
    --display-extrema        Display the extrema used during the organic method
                             (2D display)
    --display-extrema-3d     Display the extrema used during the organic method
                             (3D display)
    --display-pathes         Display the cut pathes
    --display-precuts        Display pre cuts meshes
    -i, --infos              Topological and geometrical properties of the mesh
                             after cutting are writen on the standard output.
    -o, --output=FILE        Save the mesh using the file FILE
    -x, --x=X                Window size (x)
    -y, --y=Y                Window size (y)
    -g, --opengl             Use opengl display rather than X11
    -e, --edges              Display edges rather than triangles

    -h, --help               Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "u:m:nhgex:y:l:do:r:a:pv:ci",
                               ["display-extrema", "method=", "name", "help", "opengl",
                                "edges", "x=", "y=", "display", "output=", "--max-iteration=",
                                "epsilon=", "cluster-ratio=", "merge-multipoints=",
                                "display-pathes", "length=", "ratio-length=", "pca-axis=", "direction=",
                                "display-precuts", "unfolding-method=", "display-unfolded", "extrema-number=",
                                "patch", "maximum-patch-length=", "maximum-patch=", "patch-length-ratio=",
                                "volumic=", "voxel-number=" "close-boundaries", "cc-by-size", "cc-by-id=",
                                "cc-no-selection", "maximum-cut=", "clean", "unstick-method=", "cutting-method=",
                                "info", "optim-method=", "log-mesh", "log-mesh-prefix=", "clustering-method=",
                                "cluster-maxcurv=", "display-extrema-3d", "organic-preprocess=",
                                "organic-method=", "from-border-smooth", "from-border-use-mean",
                                "from-border-use-median", "from-border-length=", "from-border-logfile=",
                                "temp-init=", "step-size=", "temp-coeff=", "accept-threshold=", "temp-min=",
                                "sa-method=", "epsilon-extrema=", "quad-method=",
                                "arity-min=", "arity-max=", "estimator=" ])
    except getopt.GetoptError as msg:
        # print help information and exit:
        usage()
        print("Error: " + msg)
        sys.exit(2)

    organicPreprocess = "patch"
    organicMethod = "from-border"
    patch = False
    closeBoundaries = False
    ccBySize = False
    ccById = -1
    ccNoSelection = False
    maxPatch = int(-1)
    maxPatchLength = 10000
    lpRatio = 0.3
    display = False
    displayExtrema = False
    displayExtrema3D = False
    displayPathes = False
    displayPrecuts = False
    displayUnfolded = False
    outputFile = ""
    edges = False
    opengl = False
    sizex = 300
    sizey = 300
    method = "topological"
    maxIter = 30000
    epsilon = 1e-7
    clusterRatio = -1
    extremaNumber = -1
    mergeMultipoints = taglut.MeshCut.MMPnone
    maxCut = -1
    length = ""
    lFactory = taglut.LengthFactory()
    ratio = -1
    pcaAxis = taglut.MeshCut.PCAFirst
    direction = taglut.DNone
    unfoldingMethod = "abf"
    volumicPCA = True
    voxelNumber = 30
    cleanMesh = False
    uMethod = taglut.MeshManipulator.UMPreserveMesh
    cMethod = taglut.MeshCut.CMEricksonPlus
    oMethod = taglut.MeshPathes.OECTruncMinMax
    cLMethod = taglut.ClusterGenerator.CMratiosimple
    qMethod = taglut.MeshCut.QMSimple
    info = False
    logMeshPrefix = ""
    maxCurv = 0.2
    fromBorderLength = "ratio"
    fromBorderSmooth = False
    fromBorderUseMean = False
    fromBorderUseMedian = False
    fromBorderLogFile = ""
    simTempInit = 20.
    simStepSize = 50
    simTempCoeff = 0.9
    simAcceptThreshold = 0.05
    simTempMin = 1e-3
    simAMethod = taglut.SimulatedAnnealingNLoop.SASimple
    epsilonExtrema = 0.05
    aritymin = 3
    aritymax = 3
    estimator = "length"


    for o, a in opts:
        if o == "--epsilon-extrema":
            epsilonExtrema = float(a)
        if o == "--temp-init":
            simTempInit = float(a)
        if o == "--step-size":
            simStepSize = int(a)
        if o == "--temp-coeff":
            simTempCoeff = float(a)
        if o == "--accept-threshold":
            simAcceptThreshold = float(a)
        if o == "--temp-min":
            simTempMin = float(a)
        if o == "--sa-method":
            if a == "simple":
                simAMethod = taglut.SimulatedAnnealingNLoop.SASimple
            elif a == "by-middle":
                simAMethod = taglut.SimulatedAnnealingNLoop.SAByMiddle
            else:
               usage()
               print("Unknown simulated annealing method (" + a + ")")
               sys.exit(1)
        if o == "--arity-min":
            aritymin = int(a)
            if aritymin < 3:
                print("Error: the minimal arity value should be >= 3")
                sys.exit(-1)
        if o == "--arity-max":
            aritymax = int(a)
            if aritymax < 3:
                print("Error: the maximal arity value should be >= 3")
                sys.exit(-1)
        if o == "--estimator":
            if a == "mean" or a == "length" or a == "max":
                estimator = a
            else:
                print("Error:" + a + " is not a valid value for the estimator")
                sys.exit(-1)
        if o in ("-i", "--info"):
            info = True
        if o == "--from-border-length":
            if a in ("ratio", "euclidean", "euclidean-2d", "ratio-inverse"):
                fromBorderLength = a
            else:
               usage()
               print("Unknown length method (" + a + ")")
               sys.exit(1)
        if o == "--from-border-logfile":
            fromBorderLogFile = a
        if o == "--from-border-smooth":
            fromBorderSmooth = True
        if o == "--from-border-use-median":
            fromBorderUseMedian = True
        if o == "--from-border-use-mean":
            fromBorderUseMean = True
        if o == "--from-border-smooth":
            fromBorderSmooth = True
        if o == "--organic-method":
            if a in ("by-cluster", "from-border", "simple-extrema", "none"):
                organicMethod = a
            else:
               usage()
               print("Unknown organic method (" + a + ")")
               sys.exit(1)
        if o == "--organic-preprocess":
            if a in ("patch", "cut", "none"):
                organicPreprocess = a
            else:
               usage()
               print("Unknown preprocess method (" + a + ")")
               sys.exit(1)
        if o == "--cluster-maxcurv":
            maxCurv = float(a)
        if o == "--log-mesh" and logMeshPrefix == "":
            logMeshPrefix = "/tmp/log"
        if o == "--log-mesh-prefix":
            logMeshPrefix = a
        if o == "--clustering-method":
            if a == "ratio-simple":
                cLMethod = taglut.ClusterGenerator.CMratiosimple
            elif a == "ratio-startall":
                cLMethod = taglut.ClusterGenerator.CMratiostartall
            elif a == "ratio-log":
                cLMethod = taglut.ClusterGenerator.CMratioLog
            elif a == "curvature":
                cLMethod = taglut.ClusterGenerator.CMcurvature
            elif a == "curvature-or-ratio":
                cLMethod = taglut.ClusterGenerator.CMcurvatureOrRatio
            elif a == "curvature-or-ratio-log":
                cLMethod = taglut.ClusterGenerator.CMcurvatureOrRatioLog
            elif a == "curvature-and-ratio":
                cLMethod = taglut.ClusterGenerator.CMcurvatureAndRatio
            elif a == "curvature-and-ratio-log":
                cLMethod = taglut.ClusterGenerator.CMcurvatureAndRatioLog
            else:
               usage()
               print("Unknown clustering method (" + a + ")")
               sys.exit(1)
        if o == "--optim-method":
            if (a == "trunc-min-max"):
                oMethod = taglut.MeshPathes.OECTruncMinMax
            elif (a == "simple"):
                oMethod = taglut.MeshPathes.OECsimple
            elif (a == "by-strip"):
                oMethod = taglut.MeshPathes.OECbyStrip
            elif (a == "by-strip-and-pred-cut"):
                oMethod = taglut.MeshPathes.OECbyStripAndPredCut
            elif (a == "no-trunc"):
                oMethod = taglut.MeshPathes.OECnoTrunc
            elif (a == "trunc-min"):
                oMethod = taglut.MeshPathes.OECTruncMin
            elif (a == "trunc-max"):
                oMethod = taglut.MeshPathes.OECTruncMax
            elif (a == "approximation"):
                oMethod = taglut.MeshPathes.OECApproximation
            else:
               usage()
               print("Unknown optimization method (" + a + ")")
               sys.exit(1)
        if o == "--quad-method":
            if (a == "simple"):
                qMethod = taglut.MeshCut.QMSimple
            elif a in ("ABF", "abf"):
                qMethod = taglut.MeshCut.QMABF
            elif a in ("Floater", "floater"):
                qMethod = taglut.MeshCut.QMFloater
            else:
               usage()
               print("Unknown quadrangulation method (" + a + ")")
               sys.exit(1)
        if o == "--cutting-method":
           if (a == "Erickson+"):
               cMethod = taglut.MeshCut.CMEricksonPlus
           elif (a == "Erickson"):
               cMethod = taglut.MeshCut.CMErickson
           elif (a == "old"):
               cMethod = taglut.MeshCut.CMOld
           else:
               usage()
               print("Unknown cutting method (" + a + ")")
               sys.exit(1)
        if o == "--unstick-method":
           if (a == "none"):
               uMethod = taglut.MeshManipulator.UMNone
           elif (a == "fast"):
               uMethod = taglut.MeshManipulator.UMFast
           elif (a == "preserve-mesh"):
               uMethod = taglut.MeshManipulator.UMPreserveMesh
           else:
               usage()
               print("Unknown unsticking path method (" + a + ")")
               sys.exit(1)
        if o == "--clean":
            cleanMesh = True
        if o in ("-b", "--close-boundaries"):
            closeBoundaries = True
        if o in ("-n", "--name"):
            name = a
        if o in ("-p", "--patch"):
            patch = True
        if o == "--maximum-patch":
            maxPatch = int(a)
        if o == "--maximum-patch-length":
            maxPatchLength = float(a)
        if o == "--patch-length-ratio":
            lpRatio = float(a)
            if lpRatio > 1 or lpRatio < 0:
                usage()
                print("Length ratio used for patching step should be 0 <= L <= 1")
                sys.exit(1)
        if o == "--cc-no-selection":
            ccNoSelection = True
        if o == "--cc-by-size":
            ccBySize = True
        if o == "--cc-by-id":
            ccById = int(a)
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-m", "--method"):
            method = a
        if o in ("-g", "--opengl"):
            opengl = True
        if o in ("-b", "--border"):
            border = True
        if o in ("-e", "--edges"):
            edges = True
        if o == "--display-extrema":
            displayExtrema = True
        if o == "--display-extrema-3d":
            displayExtrema3D = True
        if o == "--display-pathes":
            displayPathes = True
        if o == "--display-precuts":
            displayPrecuts = True
        if o == "--display-unfolded":
            displayUnfolded = True
        if o == "--epsilon":
            epsilon = float(a)
        if o == "--max-iteration":
            maxIter = int(a)
        if o == "--maximum-cut":
            maxCut = int(a)
        if o == "--ratio-length":
            ratio = float(a)
        if o in ("-v", "--volumic"):
            volumicPCA = a == "True" or a == "true" or a == "1"
        if o == "--voxel-number":
            voxelNumber = int(a)
        if o in ("-a", "--pca-axis"):
            if (a == "first"):
                pcaAxis = taglut.MeshCut.PCAFirst
            elif (a == "second"):
                pcaAxis = taglut.MeshCut.PCASecond
            elif (a == "third"):
                pcaAxis = taglut.MeshCut.PCAThird
            else:
                usage()
                print("Unknown value for PCA axis (" + a + ")")
                sys.exit(1)
        if o  == "--direction":
            if (a == "front"):
                direction = taglut.DFront
            elif (a == "back"):
                direction = taglut.DBack
            elif (a == "top"):
                direction = taglut.DTop
            elif (a == "bottom"):
                direction = taglut.DBottom
            elif (a == "left"):
                direction = taglut.DLeft
            elif (a == "right"):
                direction = taglut.DRight
            elif (a == "none"):
                direction = taglut.DNone
            else:
                usage()
                print("Unknown direction value for PCA cutting method (" + a + ")")
                sys.exit(1)
        if o in ("-l", "--length"):
            length = a
            if length == "list":
                print(lFactory.toString())
                sys.exit(1)
        if o == "--merge-multipoints":
            if a == "none":
                mergeMultipoints = taglut.MeshCut.MMPnone
            elif a == "simple":
                mergeMultipoints = taglut.MeshCut.MMPsimple
            elif a == "one-point":
                mergeMultipoints = taglut.MeshCut.MMPonePoint
            else:
                usage()
                print("Unknown merge multipoints method (" + a + ")")
                sys.exit(1)
        if o in ("-r", "--cluster-ratio"):
            clusterRatio = float(a)
        if o == "--extrema-number":
            extremaNumber = int(a)
        if o in ("-u", "--unfolding-method"):
            if a in ("abf", "circle-packing"):
                unfoldingMethod = a
            else:
                usage()
                print("Unknown unfolding method (" + a + ")")
                sys.exit(1)
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    if (method == "PCA" or method == "organic") and maxIter > 10000 and unfoldingMethod == "abf":
        print("Maximum iteration value is higher than 10000 or not defined. Assums that is the default value, set to 30.")
        maxIter = 30
        maxIter = 30

    if method == "organic" and organicMethod == "by-cluster" and clusterRatio == -1 and extremaNumber == -1:
        print("Warning: no method for cluster selection has been declared")


    if ccById < 0 and not ccBySize and not ccNoSelection:
        ccBySize = True

    lFactory.setLength(length)
    if not lFactory.existsLength():
        usage()
        print("Error: length unknown (try -l list).")
        sys.exit(1)
    if ratio >= 0:
        lFactory.setRatio(ratio)

    if aritymin > aritymax:
        print("Error: wrong values for the boundary values of the arity. Abort.")
        sys.exit(1)

    if method == "cylindric" or method == "quad-by-cylinders" or method == "organic-by-cylinders":
        if estimator == "length":
            nle = taglut.NLoopEvaluatorLength()
        elif estimator == "mean":
            nle = taglut.NLoopEvaluatorMean()
        else: # estimator == "max"
            nle = taglut.NLoopEvaluatorMax()


    try:
        inputFile = args[0]
    except IndexError:
        usage()
        print("Error: Input file not defined")
        sys.exit(1)

    if not displayPathes and not displayExtrema3D and not displayExtrema and not displayPrecuts and not display and outputFile == "":
        usage()
        print("Error: Output not defined (display mode or filename)")
        sys.exit(1)

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)

        if cleanMesh:
            print("Cleaning mesh removing bad points (with wrong local topology)")
            mPart = taglut.MeshPart(m)
            nbClean = mPart.cleanMesh()
            if nbClean != 0:
                print(" Removing " + str(nbClean) + " point(s)")
            else:
                print(" No point removed")

        if cleanMesh:
            print("Cleaning mesh removing bad points (with wrong local topology)")
            mPart = taglut.MeshPart(m)
            nbClean = mPart.cleanMesh()
            if nbClean != 0:
                print(" Removing " + str(nbClean) + " point(s)")
            else:
                print(" No point removed")

        if ccBySize:
            print("Selecting the largest connected component...")
            largestCC = m.getLargestCC()
            largestCC.cropMesh()
        elif ccById >= 0:
            print("Selecting the connected component #" + str(ccById) + "...")
            mParts = m.getConnectedComponents()
            if (ccById >= len(mParts)):
                print("Error: the connected component #" + str(ccById) + " doesn't exist. There is only " + str(len(mParts)) + " connected components")
                sys.exit(1)
            mParts[ccById].cropMesh()

        if patch:
            print("Preprocessing (patch step)...")
            pLength = taglut.LengthEdgeLinearCurvature(m, lpRatio)
            mPatch = taglut.MeshPatches(m, pLength, maxPatchLength)
            mPatch.patchMesh(maxPatch, oMethod, "", logMeshPrefix)
            m.save("/tmp/patch.save.wrl")
        if closeBoundaries or method == "PCA" or (method == "organic" and organicPreprocess == "patch"):
            print("Preprocessing: patching surface closing boundaries...")
            meshPatcher = taglut.MeshPatches(m)
            meshPatcher.closeBoundaries(-1, logMeshPrefix)
        if (method == "organic" or method == "organic-by-cylinders") and organicPreprocess == "cut":
            print("Preprocessing: cutting surface using minimal cut...")
            if m.getGenus() == 0 and m.getNbBoundaries() == 0:
                meshCut = taglut.MeshCut(m)
                print("Cutting using PCA...")
                meshCut.buildPCACut()
                meshCut.cutMesh()
            else:
                meshCut = taglut.MeshCut(m)
                meshCut.cutMeshUsingMinimalCut()

        print("Processing...")
        if method == "PCA" or method == "organic":
            if method == "organic" and organicMethod == "simple-extrema":
                print("Cutting using organic method...")
                extrema = m.findExtrema(epsilonExtrema)
                print("Number of extrema: " + str(len(extrema)))
                if displayExtrema3D:
                    taglut.Display3D.displayPointsOnMesh(sizex, sizey, "3D Visualisation (precuts)", m, extrema, edges, opengl)
                print("Cutting according to the founded extrema")
                meshCut = taglut.MeshCut(m, lFactory.getLength(m))
                meshCut.buildCutFromPoints(extrema)
            else:
                print("Cutting using organic method...")
                if m.getNbBoundaries() == 0:
                    meshMap = taglut.MeshMap(m)
                    meshMap.setValuesFromDirection(direction)
                    meshCut = taglut.MeshCut(m) #TODO, taglut.LengthMap(meshMap, lFactory.getLength(m)))
                    if volumicPCA:
                        print("Cutting using volumic PCA...")
                        meshCut.buildVolumicPCACut(voxelNumber, pcaAxis)
                    else:
                        print("Cutting using PCA...")
                        meshCut.buildPCACut(pcaAxis)
                if method == "organic":
                    if m.getGenus() != 0 or m.getNbBoundaries() > 1:
                        print("Warning: the topology of the surface does not allow an organic cutting. Abording.")
                        sys.exit(1)
                    print(" Unfolding step...")
                    if m.getNbBoundaries() == 0:
                        meshCut.cutMesh()
                    if displayPrecuts:
                        taglut.Display3D.displayMesh(sizex, sizey, "3D Visualisation (precuts)", m, edges, True, opengl)
                    mapping = taglut.Mapping2D3D(m)
                    if unfoldingMethod == "abf":
                        uM = taglut.ABFMethod(mapping)
                    else:
                        uM = taglut.CPMethod(mapping)
                    uM.processUnfolding(epsilon, maxIter)
                    uM.compute2DLocation()
                    if displayUnfolded:
                        taglut.Display3D.displayMapping(sizex, sizey, "Unfolded mesh", mapping, False, edges)
                    if organicMethod == "by-cluster":
                        mapping.computeClusters(clusterRatio, extremaNumber, cLMethod, maxCurv)
                        if displayExtrema:
                            img = mapping.clustersToImage(sizex, sizey)
                            disp = taglut.CImgDisplay(img, "Clusters")
                            while not disp.is_closed:
                                disp.wait()
                    elif organicMethod == "from-border":
                        if fromBorderLength == "euclidean":
                            le = taglut.LengthEdgeEuclidean(m)
                        if fromBorderLength == "euclidean-2d":
                            le = taglut.LengthEdgeEuclidean2D(mapping)
                        if fromBorderLength == "ratio":
                            le = taglut.LengthEdgeMappingRatio(mapping)
                        elif fromBorderLength == "ratio-inverse":
                            le = taglut.LengthEdgeMappingInverseRatio(mapping)
                        mapping.computeExtremaFromBorder(le, fromBorderSmooth, fromBorderUseMean, fromBorderUseMedian, extremaNumber, fromBorderLogFile)
                        print("Number of extrema: " + str(len(mapping.getExtrema())))
                        if displayExtrema:
                            img = mapping.extremaToImage(sizex, sizey)
                            disp = taglut.CImgDisplay(img, "Clusters")
                            while not disp.is_closed:
                                disp.wait()
                    if displayExtrema3D:
                        taglut.Display3D.displayPointsOnMesh(sizex, sizey, "3D Visualisation (precuts)", m, mapping.getExtrema(), edges, opengl)
                    print("Cutting according to the found extrema")
                    meshCut = taglut.MeshCut(m, lFactory.getLength(m))
                    meshCut.buildCutFromPoints(mapping.getExtrema())
        elif method == "simple":
            print("Cutting using a simple method...")
            meshCut = taglut.MeshPart(m)
            meshCut.cropSimpleCut()
        elif method == "topologic":
            print("Cutting using topologic method...")
            meshCut = taglut.MeshCut(m, lFactory.getLength(m))
            meshCut.buildMinimalCut(maxCut, cMethod, mergeMultipoints, uMethod, oMethod, "", logMeshPrefix)
        elif method == "quadrangulation":
            print("Cutting using quadrangulation method...")
            meshCut = taglut.MeshCut(m, lFactory.getLength(m))
            meshCut.buildRectangularCut(maxCut, cMethod, mergeMultipoints, uMethod, oMethod, "", logMeshPrefix)
        elif method == "cylindric":
            print("Cutting by cylinders using simulated annealing...")
            meshCut = taglut.MeshCut(m, lFactory.getLength(m))
            meshCut.buildCylinderCutUsingNLoops(nle, aritymin, aritymax, simTempInit, simStepSize, simTempCoeff, simAcceptThreshold, simTempMin, simAMethod, logMeshPrefix)
        elif method == "quad-by-cylinders":
            print("Cutting using quadrangulation method...")
            meshCut = taglut.MeshCut(m, lFactory.getLength(m))
            meshCut.buildRectangularCutUsingNLoops(nle, aritymin, aritymax, simTempInit, simStepSize, simTempCoeff, simAcceptThreshold, simTempMin, simAMethod, uMethod, qMethod, logMeshPrefix)
        elif method == "organic-by-cylinders":
            print("Cutting using organic method with cylinders...")
            if displayPathes:
                taglut.Display3D.displayMeshPathes(sizex, sizey, "3D Visualisation (pathes)", meshCut, edges);

            if organicMethod == "simple-extrema":
                extrema = m.findExtrema(epsilonExtrema)
            elif organicMethod == "from-border":
                mapping = taglut.Mapping2D3D(m)
                if unfoldingMethod == "abf":
                    uM = taglut.ABFMethod(mapping)
                else:
                    uM = taglut.CPMethod(mapping)
                uM.processUnfolding(epsilon, maxIter)
                uM.compute2DLocation()
                if fromBorderLength == "euclidean":
                    le = taglut.LengthEdgeEuclidean(m)
                if fromBorderLength == "euclidean-2d":
                    le = taglut.LengthEdgeEuclidean2D(mapping)
                if fromBorderLength == "ratio":
                    le = taglut.LengthEdgeMappingRatio(mapping)
                elif fromBorderLength == "ratio-inverse":
                    le = taglut.LengthEdgeMappingInverseRatio(mapping)
                mapping.computeExtremaFromBorder(le, fromBorderSmooth, fromBorderUseMean, fromBorderUseMedian, extremaNumber, fromBorderLogFile)
                extrema = mapping.getExtrema()
            elif organicMethod != "none":
                print("Non supported extrema detection")
                return 1
            if organicMethod != "none":
                print("Removing" + str(len(extrema)) + "extrema")
                if displayExtrema3D:
                    taglut.Display3D.displayPointsOnMesh(sizex, sizey, "3D Visualisation (precuts)", m, extrema, edges, opengl)
                m.removePoints(extrema)
            print("Cleaning mesh.")
            print(" Before:" + str(m.getNbPoints()) + " points")
            mPart = taglut.MeshPart(m)
            mPart.cleanMesh()
            print(" After:" + str(m.getNbPoints()) + " points")
            lengthFactory = taglut.LengthFactory()
            lengthFactory.setLength("no-boundary")
            meshCut = taglut.MeshCut(m, lengthFactory.getLength(m))
            meshCut.buildCylinderCutUsingNLoops(nle, aritymin, aritymax, simTempInit, simStepSize, simTempCoeff, simAcceptThreshold, simTempMin, simAMethod, logMeshPrefix)
        else:
            usage()
            print("Error: Unknown cutting method")
            sys.exit(1)

        if method != "simple":
            if displayPathes:
                taglut.Display3D.displayMeshPathes(sizex, sizey, "3D Visualisation (pathes)", meshCut, edges);
            meshCut.cutMesh()

        if (info):
            print(m.getInfos())
        if (outputFile != ""):
            print("Saving file(" + outputFile + ")...")
            if method != "simple" and meshCut.isExportFormat(outputFile):
                meshCut.exportToFile(outputFile)
            else:
                m.save(outputFile)
        if (display):
            taglut.Display3D.displayMesh(sizex, sizey, "3D Visualisation", m, edges, True, opengl);
    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit(1)


if __name__ == "__main__":
    main()
