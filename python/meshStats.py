#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshStats [options] INPUT

Options:
    -n, --name                      Object name
    -a, --adjust-mean-distance=BOOL Adjust the map's size according to the mean
                                    distance (0: false; 1: true)
    -d, --display                   Display the final mesh
    -o, --output=FILE               Save the mesh using the file FILE
    -x, --x=X                       3d window size (x)
    -y, --y=Y                       3d window size (y)
    -h, --help                      Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "a:nhx:y:do:", ["adjust-mean-distance=", "name", "help", "x=", "y=", "display", "output="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    display = False
    outputFile = ""
    sizex = 600
    sizey = 600
    adjustMeanDistance = True
    for o, a in opts:
        if o in ("-n", "--name"):
            name = a
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-a", "--adjust-mean-distance"):
            adjustMeanDistance = int(a) == 1
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)


        print("Processing...")
        mCut = taglut.MeshCut(m)
        mCut.cutMeshUsingMinimalCut()
        mapping = taglut.Mapping2D3D(m)

        mUnfolder = taglut.ABFMethod(mapping)
        if mUnfolder.processUnfolding(0.000000001, 30) == -1:
            print("Cannot unfold the given mesh")
            sys.exit()

        mUnfolder.compute2DLocation(adjustMeanDistance)
        img = mapping.toImageStats(sizex, sizey)

        if (display):
            display = taglut.CImgDisplay(img, "Statistiques")
            while not display.is_closed:
                display.wait()
        if (outputFile != ""):
            img.save(outputFile)
    except taglut.Exception as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
