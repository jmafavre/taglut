#!/usr/bin/env python3.2
#
# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


"""Usage: meshsMoother [options] INPUT

Options:
    -n, --name               Object name
    -l, --lambda             Lambda value used during smoothing
    -s, --step-smoothing=NB  NB is the number of smoothing steps
    -d, --display            Display the final mesh
    -o, --output=FILE        Save the mesh using the file FILE
    -x, --x=X                3d window size (x)
    -y, --y=Y                3d window size (y)
    -g, --opengl             Use opengl display rather than X11
    -b, --border             Show borders using red color
    -e, --edges              Display edges rather than triangles
    -h, --help               Print this message and exit.

INPUT is a mesh file (available formats: VRML, PLY)"""

import taglut
import getopt
import sys

try:
    my_getopt = getopt.gnu_getopt
except AttributeError:
    my_getopt = getopt.getopt


def usage():
    print(__doc__)
    sys.exit(0)

def main():
    try:
        opts, args = my_getopt(sys.argv[1:], "s:nhgex:y:bl:do:", ["step=", "name", "help", "opengl", "edges", "x=", "y=", "border", "lambda", "display", "output="])
    except getopt.GetoptError as msg:
        # print help information and exit:
        print("Error: " + msg)
        usage()
        sys.exit(2)

    display = False
    outputFile = ""
    lambdaValue = 0.5
    edges = False
    opengl = False
    sizex = 300
    sizey = 300
    border = False
    step = 1
    for o, a in opts:
        if o in ("-n", "--name"):
            name = a
        if o in ("-x", "--x"):
            sizex = int(a)
        if o in ("-y", "--y"):
            sizey = int(a)
        if o in ("-d", "--display"):
            display = True
        if o in ("-o", "--output"):
            outputFile = a
        if o in ("-l", "--lambda"):
            lambdaValue = float(a)
        if o in ("-g", "--opengl"):
            opengl = True
        if o in ("-b", "--border"):
            border = True
        if o in ("-e", "--edges"):
            edges = True
        if o in ("-s", "--step"):
            step = int(a)
            if (step <= 0):
                print("Step may be a non negative integer")
                usage()
                sys.exit()
        if o in ("-h", "--help"):
            usage()
            sys.exit()

    try:
        inputFile = args[0]
    except IndexError:
        print("Error: Input file not defined")
        usage()
        sys.exit()

    try:
        print("Loading mesh...")
        m = taglut.Mesh(inputFile)

        print("Processing...")
        for s in range(step):
            print(" Smoothing (" + str(lambdaValue) + ")")
            taglut.MeshSmoother.smoothLaplacian(m, lambdaValue)

        if (display):
            taglut.Display3D.displayMesh(sizex, sizey, "Display", m, edges, border, opengl);
        if (outputFile != ""):
            m.save(outputFile)
    except taglut.ExceptionFileNotFound as msg:
        print("Error: " + msg.getMessage())
        sys.exit()


if __name__ == "__main__":
    main()
