# TAGLUT : Topological And Geometrical Library - a Useful Toolkit


## License 
Taglut is distributed under the GPL v2 license.
See LICENSE.txt and src/include/opennl/doc/OpenNL_License.txt

## Author
The main author of Taglut is Jean-Marie Favreau (http://jmfavreau.info).
The developpement of Taglut started in 2006 for its PhD experimentations.

## How to compile the project 

This code is provided as this. The last compilation has been done in 2014.

Some tools are required:

* g++ compiler
* a recent version of the CImg library (1.3.2 will works or SVN current version)
* libpng-dev, libjpeg-dev libraries
* doxygen to generate documentation
* cmake for compilation

Others tools are optional:

* libx11-dev for graphical display
* libpopt-dev for binaries
* glut for OpenGL display
* libxml2 for xml script interpreter
* swig and python-dev for python bindings
* openmp for a parallelized version of Circle Packing

How to proceed:

* Run "ccmake ." and configure needed parameters
* Run "make" to compile libraries and binaries
* Possibly use "make doc" to build doxygen documentation
* Run "make install" to install libraries and binaries

For a local usage, set the LD_LIBRARY_PATH variable containing the lib/
directory.

## Note 

OpenNL is a library to easily construct and solve sparse linear systems. It was
part of blender (http://www.blender.org).
A large part of the source code of the ABF implementation (src/unfolding/abf/)
was also part of blender.
