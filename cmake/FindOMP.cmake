INCLUDE(CheckFunctionExists)

MESSAGE(STATUS "Check for compiler OpenMP support...")
SET(OPENMP_FLAG)
SET(OPENMP_FLAG_FOUND FALSE)
SET(
  OPENMP_FLAGS
  "-fopenmp" # gcc
  "-openmp" # icc
  "-mp" # SGI & PGI
  "-xopenmp" # Sun
  "-omp" # Tru64
  "-qsmp=omp" # AIX
  )
SET(OPENMP_FUNCTION omp_set_num_threads)

FOREACH(FLAG ${OPENMP_FLAGS})
  IF(NOT OPENMP_FLAG_FOUND)
    MESSAGE(STATUS "Check if \"${FLAG}\" OpenMP flag working...")

    SET(CMAKE_REQUIRED_FLAGS ${FLAG})
    CHECK_FUNCTION_EXISTS(${OPENMP_FUNCTION} OPENMP_FUNCTION_FOUND${FLAG})

    IF(OPENMP_FUNCTION_FOUND${FLAG})
      SET(OPENMP_FLAG ${FLAG})
      SET(OPENMP_FLAG_FOUND TRUE)
    ENDIF(OPENMP_FUNCTION_FOUND${FLAG})
  ENDIF(NOT OPENMP_FLAG_FOUND)
ENDFOREACH(FLAG ${OPENMP_FLAGS})

IF(NOT OPENMP_FLAG_FOUND)
  MESSAGE(STATUS "Check for compiler OpenMP support: no.")
ELSE(NOT OPENMP_FLAG_FOUND)
  MESSAGE(STATUS "Check for compiler OpenMP support: yes.")
ENDIF(NOT OPENMP_FLAG_FOUND)
