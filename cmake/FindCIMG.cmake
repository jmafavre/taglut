FIND_PATH(CIMG_INCLUDE_DIR CImg.h
  /usr/local/include
  /usr/include
  src/include
)


IF (CIMG_INCLUDE_DIR)
  SET(CIMG_FOUND "YES")
  MESSAGE(STATUS "Found CImg: ${CIMG_INCLUDE_DIR}")
ENDIF (CIMG_INCLUDE_DIR)
