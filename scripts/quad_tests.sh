#!/bin/bash

objects[0]=~/recherche/data/IMATI/silvia/bitori_laplace/bitoro_calt_decimeate_remesh.off
objects[1]=~/recherche/data/IMATI/silvia/bitori_laplace/bitoro_calt_pinched.obj
objects[2]=~/recherche/data/IMATI/silvia/bitori_laplace/bitoro_calt_distorted.obj
objects[3]=~/recherche/data/IMATI/silvia/bitoro_calt.wrl

functions[0]=~/recherche/data/IMATI/silvia/bitori_laplace/bitoro_calt_decimeate_remesh_l2.txt
functions[1]=~/recherche/data/IMATI/silvia/bitori_laplace/bitoro_calt_pinched_l2.txt
functions[2]=~/recherche/data/IMATI/silvia/bitori_laplace/bitoro_calt_distorted_l2.txt
functions[3]=~/recherche/data/IMATI/silvia/bitoro_calt.usr


function run_test() {
    python/meshCutUsingNLoops.py ${objects[$1]} ${functions[$1]}  -d  --exact -o /tmp/test-$1.obj -c
}

if [ $# == 0 ]; then
    for i in `seq 0 3`; do
	run_test $i
    done
else
    run_test $1
fi