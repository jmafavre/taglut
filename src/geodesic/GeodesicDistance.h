/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CNR-IMATI
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMap.h"
#include "PLPath.h"

#ifndef GEODESIC_DISTANCE
#define GEODESIC_DISTANCE

#include <vector>
// using an extension of the stl in order to use priority queues with random remove...
#include <ext/pb_ds/priority_queue.hpp>
#include "IDTypes.h"
#include "Exception.h"

namespace Taglut {
  class Mesh;


  /**
     @class GeodesicOrder
     Order the windows according to their position on the edge
  */
  class GeodesicOrder {
  public:
    /**
       Comparator of the first value
    */
    bool operator() (const std::pair<double, double> & b1,
		     const std::pair<double, double> & b2) const {
      return b1.first > b2.first;
    }

  };

  /**
     @class GeodesicDistance
     Abstract class that describe a method to compute a shortest path between points
  */
  class GeodesicDistance {

  protected:
    double default_value;

  private:
    virtual void computeDistanceInternal(MeshMap & result, Mesh & mesh, const std::vector<VertexID> & points,
					 int tFlag, bool useFlags, double stop,
					 const std::vector<VertexID> & targets, bool alltargets) = 0;

    static std::pair<PointOnEdge, double> findNextPointOnGeodesic(const std::pair<PointOnEdge, double> & pOn, const Mesh & mesh, const MeshMap & mmap);

    static double getExactDistance(const PointOnEdge & pOn, const Mesh & mesh, const MeshMap & mmap);


    /** a class that describe a triangle on the plane, with an origin */
    class TriangleOnPlane {
    private:
      Coord2D origin;
      std::map<VertexID, Coord2D> points;
      const Mesh & mesh;
      const MeshMap & mmap;


      /** estimate a location for a new point defined by the distance to two existing points */
      const Coord2D initLocation(VertexID v1, double d1, VertexID v2, double d2) const;

      /** get the third point (or throws an exception if there's more than 3 points on the object) */
      VertexID getOtherPoint(VertexID v1, VertexID v2) const;

      /** get the two other points (or throws an exception if there's more than 3 points on the object) */
      std::pair<VertexID, VertexID> getOtherPoints(VertexID v1) const;

      /** compute the intersection between edge (\p v1, \p v2) and the line (\p pon, origin). If no intersection
	  found, it returns \p pon. */
      PointOnEdge getIntersection(VertexID v1, VertexID v2, const Line2D & l, const PointOnEdge & pon) const;

    public:
      /** the triangle is placed on the plane, and the source is adjusted using the distances from \p mmap_ */
      TriangleOnPlane(const Triangle & t, const Mesh & mesh_, const MeshMap & mmap_);

      /** accessor */
      inline const Coord2D & getOrigin() const { return origin; }

      /** compute the exact location of the given point, located on an edge of the triangle */
      inline Coord2D get2DLocation(const PointOnEdge & pon) const {
	if (pon.isVertex())
	  return get2DCoord(pon.getClosestVertex());
	else {
	  const Coord2D & p1 = get2DCoord(pon.getFirstVertex());
	  const Coord2D & p2 = get2DCoord(pon.getSecondVertex());
	  return p1 + (p2 - p1) * pon.getLocation();
	}
      }

      /** get the 2D coord of a vertex */
      const Coord2D & get2DCoord(VertexID v) const {
	std::map<VertexID, Coord2D>::const_iterator r = points.find(v);
	if (r == points.end())
	  throw Exception("get2DCoord(1): cannot find required point");
	return (*r).second;
      }

      /** compute the exact distance between the source and the given point, located on an edge of the triangle */
      inline double getDistance(const PointOnEdge & pon) const {
	return get2DLocation(pon).distance2D(origin);
      }

      /** return the intersection between the line (given point on edge, origin) and the edges */
      std::pair<PointOnEdge, double> findOtherIntersection(const std::pair<PointOnEdge, double> & pon) const;

      /** return a string description of the triangle on plane */
      std::string toString() const;
    };

  public:
    /**
       default constructor
    */
    GeodesicDistance() : default_value(std::numeric_limits<double>::max()) {}

    /** destructor */
    virtual ~GeodesicDistance() {}

    inline void setDefaultValue(double value = std::numeric_limits<double>::max()) { default_value = value; }

    /** compute distances from a given set of points on a given mesh.
	A maximal distance is possibly given, and a set of targets */
    virtual inline void computeDistanceFromPoints(MeshMap & result, Mesh & mesh,
						  std::vector<VertexID> & points, double stop = -1.,
						  const std::vector<VertexID> & targets = std::vector<VertexID>(),
						  bool alltargets = false) {
      computeDistanceInternal(result, mesh, points, 0, false, stop, targets, alltargets);
    }

    /** compute distances from a given set of points on a given mesh.
	A maximal distance is possibly given, and a set of targets */
    virtual inline void computeDistanceFromPointsTargetPOE(MeshMap & result, Mesh & mesh,
							   std::vector<VertexID> & points, double stop = -1.,
							   const std::vector<PointOnEdge> & targets = std::vector<PointOnEdge>(),
							   bool alltargets = false) {
      std::vector<VertexID> newtargets;
      {
	std::set<VertexID> newtargetsset;
	for(std::vector<PointOnEdge>::const_iterator t = targets.begin(); t != targets.end(); ++t) {
	  if ((*t).isVertex()) {
	    newtargetsset.insert((*t).getClosestVertex());
	  }
	  else {
	    newtargetsset.insert((*t).getFirstVertex());
	    newtargetsset.insert((*t).getSecondVertex());
	  }
	}
	newtargets.insert(newtargets.begin(), newtargetsset.begin(), newtargetsset.end());
      }
      computeDistanceInternal(result, mesh, points, 0, false, stop, newtargets, alltargets);
    }

    /** compute distances from a given set of points on a given mesh.
	A maximal distance is possibly given, and a set of targets. The full 1-star is used as real target if alltargets = true */
    virtual inline void computeDistanceFromPointsTargetPOE1Star(MeshMap & result, Mesh & mesh,
							   std::vector<VertexID> & points, double stop = -1.,
							   const std::vector<PointOnEdge> & targets = std::vector<PointOnEdge>(),
							   bool alltargets = false) {
      std::vector<VertexID> realTargets;
      for(std::vector<PointOnEdge>::const_iterator t = targets.begin(); t != targets.end(); ++t) {
	if (alltargets && (*t).isVertex()) {
	  const Point3D & p = mesh.point((*t).getClosestVertex());
	  realTargets.push_back(p.getId());
	  for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin(); nb != p.getNeighbours().end(); ++nb)
	    realTargets.push_back(*nb);
	  realTargets.push_back((*t).getClosestVertex());
	}
	else {
	  realTargets.push_back((*t).getFirstVertex());
	  realTargets.push_back((*t).getSecondVertex());
	}
      }

      computeDistanceInternal(result, mesh, points, 0, false, stop, realTargets, alltargets);
    }

    /** compute distances from a given point on a given mesh.
	A maximal distance is possibly given, and a set of targets */
    virtual inline void computeDistanceFromPoint(MeshMap & result, Mesh & mesh, VertexID point, double stop = -1,
						 const std::vector<VertexID> & targets = std::vector<VertexID>(),
						 bool alltargets = false) {
      std::vector<VertexID> points;
      points.push_back(point);
      computeDistanceFromPoints(result, mesh, points, stop, targets, alltargets);
    }

    /** compute distances from a given point on a given mesh.
	A maximal distance is possibly given, and a set of targets */
    virtual inline void computeDistanceFromPointTargetPOE(MeshMap & result, Mesh & mesh, VertexID point, double stop = -1,
							  const std::vector<PointOnEdge> & targets = std::vector<PointOnEdge>(),
							  bool alltargets = false) {
      std::vector<VertexID> points;
      points.push_back(point);
      computeDistanceFromPointsTargetPOE(result, mesh, points, stop, targets, alltargets);
    }

    /** compute distances from a given point on a given mesh.
	A maximal distance is possibly given, and a set of targets */
    virtual inline void computeDistanceFromPointTargetPOE1Star(MeshMap & result, Mesh & mesh, VertexID point, double stop = -1,
							  const std::vector<PointOnEdge> & targets = std::vector<PointOnEdge>(),
							  bool alltargets = false) {
      std::vector<VertexID> points;
      points.push_back(point);
      computeDistanceFromPointsTargetPOE1Star(result, mesh, points, stop, targets, alltargets);
    }

    /** compute distances from a given set of points on a given mesh.
	A maximal distance is possibly given, and a set of targets.
	The manipulated region is described by the triangle flags (\p tFlag)
    */
    virtual inline void computeDistanceFromPointsInRegion(MeshMap & result, Mesh & mesh,
							  std::vector<VertexID> & points, int tFlag, double stop = -1.,
							  const std::vector<VertexID> & targets = std::vector<VertexID>(),
							  bool alltargets = false) {
      computeDistanceInternal(result, mesh, points, tFlag, true, stop, targets, alltargets);
    }

    /** compute distances from a given point on a given mesh.
	A maximal distance is possibly given, and a set of targets
	The manipulated region is described by the triangle flags (\p tFlag)
    */
    virtual inline void computeDistanceFromPointInRegion(MeshMap & result, Mesh & mesh, VertexID point, int tFlag, double stop = -1,
							 const std::vector<VertexID> & targets = std::vector<VertexID>(),
							 bool alltargets = false) {
      std::vector<VertexID> points;
      points.push_back(point);
      computeDistanceFromPointsInRegion(result, mesh, points, tFlag, stop, targets, alltargets);
    }

    /**
       given a mesh and two points, it computes the shortest geodesic path between p1 and p2.
    */
    PLPath getShortestPath(Mesh & mesh, VertexID p1, VertexID p2);

    /**
       Compute the shortest path using a previously computed MeshMap that corresponds to
       a geodesic distance.
     */
    static PLPath getPathToOrigin(const PointOnEdge & pOn, const Mesh & mesh, const MeshMap & mmap);
  };

  /**
     @class FastMarching
     Fast marching method to compute a shortest path between points
     Exact implementation of V. Surazhsky, T. Surazhsky, D. Kirsanov, S. Gortler, H. Hoppe, Fast exact and approximate geodesics on meshes (2005).
  */
  class FastMarching : public GeodesicDistance {
#ifndef SWIG
  public:

    /**
       @class WindowOnEdge
       A window on an edge used by the fast marching propagation
    */
    class WindowOnEdge {
    private:
      /** first vertex of the edge */
      VertexID v1;
      /** second vertex of the edge */
      VertexID v2;
      /** position of the first point on the edge (geodesic coordinate) */
      double p1;
      /** position of the second point on the edge (geodesic coordinate) */
      double p2;
      /** distance between the first point and the pseudosource */
      double d1;
      /** distance between the second point and the pseudosource */
      double d2;
      /** distance between the pseudosource and the source */
      double dSource;
      /** triangle where the "light" come from */
      TriangleID direction;
    public:
      /** default constructor */
      WindowOnEdge(VertexID v1, VertexID v2,
		   double p1, double p2,
		   double d1, double d2,
		   TriangleID direction,
		   double edgeLength,
		   double dSource = 0.);

      /** copy constructor */
      WindowOnEdge(const WindowOnEdge & woe);

      /** constructor that modify the boundaries of the window */
      WindowOnEdge(const WindowOnEdge & woe, double p1, double p2);

      /** constructor that modify the boundaries of the window.
	 To speed up the computation, the sources of the window is given in parameter (they where previously computed).
      */
      WindowOnEdge(const WindowOnEdge & woe, double p1, double p2, const Coord2D & s);

      /** accessor */
      inline VertexID getV1() const { return v1; }
      /** accessor */
      inline VertexID getV2() const { return v2; }

      /** return (p1, p2) */
      inline std::pair<double, double> getExtremities() const {
	return std::pair<double, double>(p1, p2);
      }

      /** modifier */
      inline void adjustP1(double p, const Coord2D & s) {
	p1 = p;
	d1 = getDistance(p, s);
      }
      /** modifier */
      inline void adjustP2(double p, const Coord2D & s) {
	p2 = p;
	d2 = getDistance(p, s);
      }

      /** modifier */
      inline void adjustP1(double p) {
	adjustP1(p, getSourceCoord2D());
      }
      /** modifier */
      inline void adjustP2(double p) {
	adjustP2(p, getSourceCoord2D());
      }

      /**
	 adjust the border location of the current window according
	 to the w's borders, using epsilon
      */
      inline void adjustEpsilon(const WindowOnEdge & w, double epsilon, const Coord2D & s) {
	if (((getSize() > epsilon) || (w.getP1() < p1)) && (fabs(p1 - w.getP1()) <= epsilon))
	  adjustP1(w.getP1(), s);
	if ((w.getP2() <= p2) && (fabs(p1 - w.getP2()) <= epsilon))
	  adjustP1(w.getP2(), s);
	if ((w.getP1() >= p1) && (fabs(w.getP1() - p2) <= epsilon))
	  adjustP2(w.getP1(), s);
	if (((getSize() > epsilon) || (w.getP2() > p2)) && (fabs(w.getP2() - p2) <= epsilon))
	  adjustP2(w.getP2(), s);
	if (p2 < p1)
	  p2 = p1;
      }

      /** accessor */
      inline double getP1() const { return p1; }
      /** accessor */
      inline double getP2() const { return p2; }
      /** accessor */
      inline double getSize() const { return p2 - p1; }

      /** accessor */
      inline double getD1() const { return d1; }
      /** accessor */
      inline double getD2() const { return d2; }
      /** accessor */
      inline double getGlobalD1() const { return d1 + dSource; }
      /** accessor */
      inline double getGlobalD2() const { return d2 + dSource; }

      /** accessor */
      inline double getDSource() const { return dSource; }
      /** accessor */
      inline TriangleID getDirection() const { return direction; }
      /** return the minimal distance between a point of this window and the source */
      inline double getMinDistance() const {
	if (d1 < d2)
	  return dSource + d1;
	else
	  return dSource + d2;
      }

      /**
	 return true if the two windows are neighbours
      */
      inline bool isNeighbour(const WindowOnEdge & w) const {
	return inSameEdge(w) && ((p1 == w.p2) || (p2 == w.p1));
      }

      /** return true if the two windows are in the same edge */
      inline bool inSameEdge(const WindowOnEdge & wOe) const {
	return (v1 == wOe.getV1()) && (v2 == wOe.getV2());
      }

      /** a comparator accessor using the minimal distance */
      inline bool operator< (const WindowOnEdge & wOe) const {
	return getMinDistance() > wOe.getMinDistance();
      }

      /** return true if the windows are equal */
      inline bool operator== (const WindowOnEdge & wOe) const {
	return ((v1 == wOe.v1) && (v2 == wOe.v2) &&
		(p1 == wOe.p1) && (p2 == wOe.p2) &&
		(d1 == wOe.d1) && (d2 == wOe.d2) &&
		(dSource == wOe.dSource) && (direction == wOe.direction));
      }


      /** affectation operator */
      inline WindowOnEdge & operator= (const WindowOnEdge & wOe) {
	v1 = wOe.v1;
	v2 = wOe.v2;
	p1 = wOe.p1;
	p2 = wOe.p2;
	d1 = wOe.d1;
	d2 = wOe.d2;
	dSource = wOe.dSource;
	direction = wOe.direction;
	return *this;
      }

      /** return true if the two windows have a non-empty intersection */
      inline bool intersects(const WindowOnEdge & wOe) const {
	if (inSameEdge(wOe))
	  return (((p1 <= wOe.p1) && (wOe.p1 < p2)) ||
		  ((p1 < wOe.p2) && (wOe.p2 <= p2)) ||
		  ((wOe.p1 <= p1) && (p1 < wOe.p2)) ||
		  ((wOe.p1 < p2) && (p2 <= wOe.p2)));
	else
	  return false;
      }

      /** return true if the window is empty */
      inline bool isEmpty() const {
	return p1 >= p2;
      }

      /** return true if the window is smaller than a given epsilon */
      inline bool isSmall(double e = 1e-7) const {
	return p2 - p1 < e;
      }

      /** return true if the current window has a minimal value in the whole intersection */
      bool fullMinimalOnInterval(const WindowOnEdge & w, const std::pair<double, double> & interval) const;

      /**
	 compute the location of the pseudo-source in the plane
      */
      inline Coord2D getSourceCoord2D() const {
	// distances
	const double d1_t = getD1();
	const double d2_t = getD2();

	// location of the two extrema
	const double b0x = getP1();
	const double b1x = getP2();
	assert(!isEmpty());
	assert(b0x < b1x);

	// location of the source
	const double sx = ((d2_t * d2_t - d1_t * d1_t) / (b0x - b1x) + b0x + b1x) / 2;
	const double sy = sqrt(fabs(d1_t * d1_t - (b0x - sx) * (b0x - sx)));

	return Coord2D(sx, sy);
      }

      /** return the overlapping segment of two windows */
      inline std::pair<double, double> getOverlapping(const WindowOnEdge & w) const {
	if (inSameEdge(w))
	  return std::pair<double, double>(w.getP1() < getP1() ? getP1() : w.getP1(),
					   w.getP2() < getP2() ? w.getP2() : getP2());
	else
	  return std::pair<double, double>(-1., -1.);
      }

      /** return the union of segments of two windows if it's a window */
      inline std::pair<double, double> getUnion(const WindowOnEdge & w) const {
	if (inSameEdge(w) && intersects(w))
	  return std::pair<double, double>(w.getP1() < getP1() ? w.getP1() : getP1(),
					   w.getP2() < getP2() ? getP2() : w.getP2());
	else
	  return std::pair<double, double>(-1., -1.);
      }

      /**
	 return the new boundaries (according to the distance to the sources) inside the given segment.
	 The result can 0, 1 or 2 values (-1 describe a "void value")
	 To speed up the computation, the sources of the two windows are given in parameter (they where previously computed).
	 s0 corresponds to the source of *this, and s1 to the source of w
      */
      std::pair<double, double> getNewBoundariesInsideSegment(const WindowOnEdge & w,
							      const std::pair<double, double> & segment, const Coord2D & s0, const Coord2D & s1) const;

      /**
	 return the new boundaries (according to the distance to the sources)
	 The result can 0, 1 or 2 values (-1 describe a "void value").
	 To speed up the computation, the sources of the two windows are given in parameter (they where previously computed)
	 s0 corresponds to the source of *this, and s1 to the source of w
       */
      std::pair<double, double> getNewBoundaries(const WindowOnEdge & w, const Coord2D & s0, const Coord2D & s1) const;

      /**
	 return the new boundaries (according to the distance to the sources) inside the given segment.
	 The result can 0, 1 or 2 values (-1 describe a "void value")
      */
      inline std::pair<double, double> getNewBoundariesInsideSegment(const WindowOnEdge & w,
								     const std::pair<double, double> & segment) const {
	return getNewBoundariesInsideSegment(w, segment, getSourceCoord2D(), w.getSourceCoord2D());
      }

      /**
	 return the new boundaries (according to the distance to the sources)
	 The result can 0, 1 or 2 values (-1 describe a "void value").
       */
      std::pair<double, double> getNewBoundaries(const WindowOnEdge & w) const {
	return getNewBoundaries(w, getSourceCoord2D(), w.getSourceCoord2D());
      }

      /**
	 return the distance from a point on the window and the source.
	 \p p is the geodesic coordinate along the edge
      */
      inline double getGlobalDistance(double p) const {
	return getGlobalDistance(p, getSourceCoord2D());
      }

      /**
	 return the distance from a point on the window and a given point, incrementing using distance between
	 the pseudosource and the real one.
	 \p p is the geodesic coordinate along the edge
      */
      inline double getGlobalDistance(double p, const Coord2D & s) const {
	return s.distance2D(Coord2D(p, 0.)) + dSource;
      }

      /**
	 return the distance from a point on the window and a given point.
	 \p p is the geodesic coordinate along the edge
      */
      inline double getDistance(double p, const Coord2D & s) const {
	return s.distance2D(Coord2D(p, 0.));
      }

      /**
	 return true if the current window is strictly inside \p w,
	 i.e. if it's inside, and the extremities are disconnected
      */
      inline bool insideStrict(const WindowOnEdge & w) const {
	if (inSameEdge(w))
	  return (w.getP1() < getP1()) && (getP2() < w.getP2());
	else
	  return false;
      }


      /**
	 Given a set of splitting points, and two windows, split the given window
	 according to these points, preserving only better sub-windows
       */
      std::vector<std::pair<double, double> > splitWindows(const std::vector<double> & points,
							   const WindowOnEdge & w2,
							   const Coord2D & s1, const Coord2D & s2);

      /**
	 Given a set of ordered intervals contained inside the current window, return a vector of
	 intervals that corresponds to the complement on the window.
	 Warning: this function remove all the elements of the interval queue
       */
      std::vector<std::pair<double, double> >
      getNegativeIntervals(std::priority_queue<std::pair<double, double>,
			   std::vector<std::pair<double, double> >, GeodesicOrder> & intervals);

      /** generate a string from the current window */
      std::string toString() const;

      /** return the minimal distance between boundaries of the current window and the given one */
      inline double getMinDistanceBetweenBoundaries(const WindowOnEdge & w) const {
	double d = fabs(p1 - w.p1);

	double d2_t = fabs(p1 - w.p2);
	if (d2_t < d) d = d2_t;
	d2_t = fabs(p2 - w.p2);
	if (d2_t < d) d = d2_t;
	d2_t = fabs(p2 - w.p2);
	if (d2_t < d) d = d2_t;

	return d;
      }

      /** return the closest boundary from v of the current window if inside epsilon, or v otherwise */
      inline double getClosestBoundaryEpsilon(double v, double epsilon) const {
	double r = p1;
	double d = fabs(v - p1);
	if (fabs(p2 - v) < d) {
	  d = fabs(p2 - v);
	  r = p2;
	}
	if (d > epsilon)
	  r = v;
	return r;
      }

      /**
	 return the crossings on the overlapping of the function distance to the sources, using the given pseudosources
      */
      std::vector<double> getCrossings(const WindowOnEdge & cw, const Coord2D & s, const Coord2D & scw, double epsilon);

    };


    typedef __gnu_pbds::priority_queue<WindowOnEdge, std::less<WindowOnEdge>, __gnu_pbds::binomial_heap_tag> WindowPriorityQueue;

  private:
    /**
       @class FMEdge
       An edge structure that contains the corresponding windows
    */
    class FMEdge {
    private:
      /** first vertex */
      VertexID v1;
      /** second vertex */
      VertexID v2;

      /** edge size */
      double length;
      /** list of open windows */
      std::vector<WindowPriorityQueue::point_iterator> windows;

      /** list of closed window (that corresponds to the closest distance
	  according to the sources */
      std::vector<WindowOnEdge> closedWindows;

      /** set the vertices of the current edge */
      inline void setVertices(VertexID v1_t, VertexID v2_t) {
	if (v1_t < v2_t) {
	  (*this).v1 = v1_t;
	  (*this).v2 = v2_t;
	}
	else {
	  (*this).v1 = v2_t;
	  (*this).v2 = v1_t;
	}
      }
      inline void setLength(double length_t) {
	(*this).length = length_t;
      }


    public:
      /** default constructor */
      FMEdge(VertexID v1_t = 0, VertexID v2_t = 0, double length_t = -1.) : v1(v1_t), v2(v2_t), length(length_t) {
      }

      /** constructor with a given window */
      FMEdge(VertexID v1_t, VertexID v2_t, double length_t, const WindowPriorityQueue::point_iterator & w);

      /** copy constructor */
      FMEdge(const FMEdge & e) : v1(e.v1), v2(e.v2), windows(e.windows) {
      }
      /** accessor */
      inline VertexID getV1() const { return v1; }

      /** accessor */
      inline VertexID getV2() const { return v2; }

      /** return the number of windows */
      inline unsigned int getNbWindows() const { return windows.size(); }

      /** return the number of windows */
      inline unsigned int getNbClosedWindows() const { return closedWindows.size(); }

      /**
	 readjust the existing windows according to the new one.
	 return a list of windows that have to be added
      */
      std::vector<WindowOnEdge> prepare(const WindowOnEdge & w, FastMarching & fm, const Mesh & mesh);

      /**
	 extend the extremity windows to join the unseen vertices
	 return the global distance associated to the extremity points, or -1 if there is no window on this edge
      */
      std::pair<double, double> extendExtremityWindows();

      /**
	 add the given window to the current edge without adjustement
      */
      inline FMEdge & addWindow(const WindowPriorityQueue::point_iterator & w) {
	windows.push_back(w);
	return *this;
      }


      /** remove a window from the current edge */
      inline void removeWindow(const WindowPriorityQueue::point_iterator & w) {
	for(std::vector<WindowPriorityQueue::point_iterator>::iterator i = windows.begin(); i != windows.end(); ++i)
	  if (*i == w) {
	    windows.erase(i);
	    return;
	  }
	throw Exception("removeWindow(1): the window is not contained on this edge.");
      }

      /** remove a window from the set of open windows of the current edge */
      inline WindowPriorityQueue::point_iterator closeWindow(const WindowOnEdge & w) {
	closedWindows.push_back(w);
	WindowPriorityQueue::point_iterator result;
	for(std::vector<WindowPriorityQueue::point_iterator>::iterator i = windows.begin(); i != windows.end(); ++i)
	  if (**i == w) {
	    result = *i;
	    windows.erase(i);
	    return result;
	  }
	throw Exception("closeWindow(1): the window is not contained on this edge.");
      }

      /**
	 return the smallest border of the closed windows
      */
      inline double getMinBorderClosedWindows() const {
	double result = std::numeric_limits<double>::max();
	for(std::vector<WindowOnEdge>::const_iterator w = closedWindows.begin(); w != closedWindows.end(); ++w)
	  if ((*w).getP1() < result)
	    result = (*w).getP1();
	return result;
      }


      /**
	 return the highest border of the closed windows
      */
      inline double getMaxBorderClosedWindows() const {
	double result = 0.;
	for(std::vector<WindowOnEdge>::const_iterator w = closedWindows.begin(); w != closedWindows.end(); ++w)
	  if ((*w).getP2() > result)
	    result = (*w).getP2();
	return result;
      }

      /** return the cumulative length of the closed windows of this edge
       */
      inline double getWindowLength() const {
	double result = 0.;
	for(std::vector<WindowOnEdge>::const_iterator w = closedWindows.begin(); w != closedWindows.end(); ++w)
	  if ((*w).getP2() > (*w).getP1())
	    result += (*w).getP2() - (*w).getP1();
	return result;
      }

    };

    /** a comparator for pair of points */
    class ltPair {
    public:
      /** binary operator */
      inline bool operator() (const std::pair<VertexID, VertexID> & p1,
			      const std::pair<VertexID, VertexID> & p2) const {
	if (p1.first < p1.second) {
	  if (p2.first < p2.second)
	    return (p1.first < p2.first) || ((p1.first == p2.first) && (p1.second < p2.second));
	  else
	    return (p1.first < p2.second) || ((p1.first == p2.second) && (p1.second < p2.first));
	}
	else {
	  if (p2.first < p2.second)
	    return (p1.second < p2.first) || ((p1.second == p2.first) && (p1.first < p2.second));
	  else
	    return (p1.second < p2.second) || ((p1.second == p2.second) && (p1.first < p2.first));
	}
      }
    };

    /** the set of edges */
    std::map<std::pair<VertexID, VertexID>, FMEdge, ltPair> edges;

    /** epsilon for the comparisons */
    double epsilon;

    /** the priority queue of windows */
    WindowPriorityQueue windows;

    /** remove a window from the structure */
    void removeWindow(const WindowPriorityQueue::point_iterator & w);

    /** remove windows from the structure */
    inline void removeWindows(const std::vector<WindowPriorityQueue::point_iterator> & ws) {
      for(std::vector<WindowPriorityQueue::point_iterator>::const_iterator w = ws.begin(); w != ws.end(); ++w)
	removeWindow(*w);
    }

    /** adding a window on the structure, and update the mesh map */
    inline void addWindow(const WindowOnEdge & w, MeshMap & mMap);

    /** remove the first window from the structure */
    void removeFirstWindow();

    /** update the mesh map using the distance value associated to the given vertex */
    inline void updateMeshMap(MeshMap & mMap, VertexID v, double d) const {
      if ((mMap[v] < 0) || (mMap[v] > d)) {
	  mMap[v] = d;
      }
    }

    /** update the mesh map using the distance values contained by the given edge */
    inline void updateMeshMap(MeshMap & mMap, const WindowOnEdge & w) const {
      if (w.getP1() <= 2 * epsilon) {
	updateMeshMap(mMap, w.getV1(), w.getGlobalD1());
      }
      if (w.getP2() >= mMap.getMesh().point(w.getV1()).distance(mMap.getMesh().point(w.getV2())) - 2 * epsilon) {
	updateMeshMap(mMap, w.getV2(), w.getGlobalD2());
      }
    }

    /**
       propagate the given window on the given triangle
    */
    void propagateWindow(MeshMap & mMap, const WindowOnEdge & w, const Triangle & t, bool useFlags, int tFlag);

    /**
       propagate the wavefront (one step) from the first window.
       if return true, a break condition has been detected
    */
    bool propagateFirstWindow(MeshMap & mMap, double stop,
			      const std::vector<VertexID> & targets,
			      std::vector<bool> & seentargets,
			      bool alltargets,
			      bool useFlags, int tFlag);

    /**
       return the edge that corresponds to the window,
       create it if it does not exists
    */
    inline FMEdge & getEdge(const WindowOnEdge & woe) {
      return edges[std::pair<VertexID, VertexID>(woe.getV1(), woe.getV2())];
    }

    /**
       return true if the given window is on a known edge
    */
    inline bool existsEdge(const WindowOnEdge & woe) const {
      return existsEdge(woe.getV1(), woe.getV2());
    }

    /**
       return true if the given edge is on a known edge
    */
    inline bool existsEdge(VertexID v1, VertexID v2) const {
      return edges.find(std::pair<VertexID, VertexID>(v1, v2)) != edges.end();
    }


    /**
       return true if the given window is on an open edge (i.e.with open windows)
    */
    inline bool existsOpenEdge(const WindowOnEdge & woe) const {
      return existsOpenEdge(woe.getV1(), woe.getV2());
    }

    /**
       return true if the given edge is on an open edge (i.e. with open windows)
    */
    inline bool existsOpenEdge(VertexID v1, VertexID v2) const {
      std::map<std::pair<VertexID, VertexID>, FMEdge, ltPair>::const_iterator e = edges.find(std::pair<VertexID, VertexID>(v1, v2));
      if (e == edges.end())
	return false;
      return ((*e).second.getNbWindows() != 0);
    }

    /**
       return true if the given vertex is part of a known edge
    */
    inline bool hasEdgeFromVertex(const Point3D & v) const {
      const VertexID vid = v.getId();
      for(std::deque<VertexID>::const_iterator nb = v.getNeighbours().begin(); nb != v.getNeighbours().end(); ++nb)
	if (existsEdge(vid, *nb))
	  return true;
      return false;
    }

    /**
       return true if the given vertex is part of an open edge
    */
    inline bool hasOpenEdgeFromVertex(const Point3D & v) const {
      const VertexID vid = v.getId();
      for(std::deque<VertexID>::const_iterator nb = v.getNeighbours().begin(); nb != v.getNeighbours().end(); ++nb)
	if (existsOpenEdge(vid, *nb))
	  return true;
      return false;
    }

    /**
       modify the window pointed by \p p with the new value \p w */
    inline void updateWindow(const WindowPriorityQueue::point_iterator & p, const WindowOnEdge & w) {
      windows.modify(p, w);
    }


    inline VertexID getNbWindowsInEdges() const {
      VertexID result = 0;
      for(std::map<std::pair<VertexID, VertexID>, FMEdge, ltPair>::const_iterator e = edges.begin(); e != edges.end(); ++e)
	result += (*e).second.getNbWindows();
      return result;
    }

#endif

    /** compute distances from a given set of points on a given mesh.
	A maximal distance is possibly given, and a set of targets */
    virtual void computeDistanceInternal(MeshMap & result, Mesh & mesh, const std::vector<VertexID> & points, int tFlag, bool useFlags, double stop = -1.,
					 const std::vector<VertexID> & targets = std::vector<VertexID>(),
					 bool alltargets = false);

  public:
    /**
       default constructor
    */
    FastMarching() : epsilon(1e-12) { }


    /** destructor */
      virtual ~FastMarching() { }


    /** accessor */
    inline double getEpsilon() const { return epsilon; }

    // define a friend function
    friend
    std::vector<WindowOnEdge> FastMarching::FMEdge::prepare(const WindowOnEdge & w, FastMarching & fm, const Mesh & mesh);


  };

}

#endif
