/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_ON_EDGE
#define POINT_ON_EDGE

#include <sstream>
#include "IDTypes.h"
#include "Coord3D.h"
#include "Point3D.h"
#include "Triangle.h"

namespace Taglut {


  /** @class PointOnEdge
      A point located on an edge
  **/
  class PointOnEdge : public Coord3D {
  private:
    /** ID of the first vertex of the edge */
    VertexID vertex1;
    /** ID of the second vertex of the edge */
    VertexID vertex2;
    /** location of the point on the edge, in [0, 1] */
    float location;

  public:
    /** default constructor. if location = 0, the new point = p1. If location = 1., the new point is p2. */
    PointOnEdge(const Point3D & p1, const Point3D & p2, float location_l = 0.) : Coord3D(p1 + location_l * (p2 - p1)),
									       vertex1(p1.getId()), vertex2(p2.getId()),
									       location(location_l) {
      // if the initial points are joined, the location on the "edge" is 0 (the new point corresponds exactly to the first point of the edge)
      if (p1.getId() == p2.getId())
	location = 0.;
      assert(location >= 0.);
      assert(location <= 1.);
    }

    /** constructor using a coord, assuming that this coord is part of the edge defined by p1 and p2 */
    PointOnEdge(const Point3D & p1, const Point3D & p2, const Coord3D & c) : Coord3D(c),
									     vertex1(p1.getId()), vertex2(p2.getId()) {
      const double d = p1.distance(p2);
      if (d == 0.)
	location = 0.;
      else
	location = p1.distance(c) / d;
    }

    /** constructor using a vertex of the mesh */
    PointOnEdge(const Point3D & p) : Coord3D(p),
				     vertex1(p.getId()), vertex2(p.getId()),
				     location(0.) {
    }

    /** empty constructor */
    PointOnEdge() : Coord3D(), vertex1(0), vertex2(0), location(0.) {

    }

    /** copy constructor */
    PointOnEdge(const PointOnEdge & poe) : Coord3D(poe), vertex1(poe.vertex1), vertex2(poe.vertex2), location(poe.location) {

    }

    /** destructor */
    ~PointOnEdge() {

    }

    /** modifier to set the corresponding vertex (1), and reajust the current point */
    inline void setFirstVertex(VertexID p) { vertex1 = p;  }
    /** modifier to set the corresponding vertex (2) */
    inline void setSecondVertex(VertexID p) { vertex2 = p; }
    /** modifier to set the location */
    inline void setLocation(double l) { location = l; }

    /** readjust the location */
    inline void adjustLocation(const Coord3D & p1, const Coord3D & p2, double l = -1.) {
      if (l >= 0.)
	location = l;
      (*(Coord3D *)this) = p1 + location * (p2 - p1);
    }

    /** adjust the location, using the given one */
    inline void adjustLocation(const Coord3D & p) {
      (*(Coord3D *)this) = p;
    }

    /** accessor to get the corresponding vertex (1) */
    inline VertexID getFirstVertex() const { return vertex1; }
    /** accessor to get the corresponding vertex (2) */
    inline VertexID getSecondVertex() const { return vertex2; }
    /** accessor to get the other vertex */
    inline VertexID getOtherVertex(VertexID v) const {
      if (v == vertex1)
	return vertex2;
      else {
	assert(v == vertex2);
	return vertex1;
      }
    }

    /** accessor to get the location */
    inline double getLocation() const { return location; }
    /** accessor to get the closest vertex */
    inline VertexID getClosestVertex() const { if (location < .5) return vertex1; else return vertex2; }

    /** return true if the edge where the current point is located contains v */
    inline bool hasVertex(VertexID v) const {
      return (vertex1 == v) || (vertex2 == v);
    }

    /** return vertex with the smallest id */
    inline VertexID getFirstVertexByID() const { return (vertex1 > vertex2) ? vertex2 : vertex1; }
    /** return vertex with the biggest id */
    inline VertexID getSecondVertexByID() const { return (vertex2 > vertex1) ? vertex2 : vertex1; }

    /** return position with vertices ordered by ID */
    inline double getLocationOrderedByID() const {
      return (vertex1 > vertex2) ? 1 - location : location;
    }

    /** assuming that the two points are not in the same edge, it returns the vertex
	shared by the two edges */
    inline VertexID getCommonVertex(const PointOnEdge & p) const {
      assert(!inSameEdge(p));
      if ((vertex1 == p.vertex1) || (vertex1 == p.vertex2))
	return vertex1;
      else {
	assert((vertex2 == p.vertex1) || (vertex2 == p.vertex2));
	return vertex2;
      }
    }


    /** assuming that the two points are in the same edge, it returns the vertex
	located in (*this) side */
    inline VertexID getVertexInOtherSide(const PointOnEdge & p) const {
      assert(inSameEdge(p));
      if (vertex1 == p.vertex1)
	if (location > p.location)
	  return vertex2;
	else
	  return vertex1;
      else {
	assert(vertex2 == p.vertex1);
	if (location > 1. - p.location)
	  return vertex2;
	else
	  return vertex1;
      }
    }

    /** comparator */
    inline bool operator==(const PointOnEdge & p1) const {
      const double epsilon = 1e-6;
      const double epsilon1 = 1 - epsilon;
      return ((vertex1 == p1.vertex1) && (vertex2 == p1.vertex2) && (p1.distance(*this) < epsilon)) ||
	((vertex1 == p1.vertex2) && (vertex2 == p1.vertex1) && (p1.distance(*this) < epsilon)) ||
	((location < epsilon) && (vertex1 == p1.vertex1) && (p1.location < epsilon)) ||
	((location < epsilon) && (vertex1 == p1.vertex2) && (p1.location > epsilon1)) ||
	((location > epsilon1) && (vertex2 == p1.vertex1) && (p1.location < epsilon)) ||
	((location > epsilon1) && (vertex2 == p1.vertex2) && (p1.location > epsilon1));
    }

    /** a strict weak ordering to use PointOnEdge on maps */
    inline bool operator<(const PointOnEdge & p) const {
      if ((*this) == p)
	return false;
      VertexID vt1 = getFirstVertexByID();
      VertexID vt2 = getSecondVertexByID();
      VertexID vp1 = p.getFirstVertexByID();
      VertexID vp2 = p.getSecondVertexByID();
      if (isVertex()) {
	vt1 = getClosestVertex();
	vt2 = getClosestVertex();
      }
      if (p.isVertex()) {
	vp1 = p.getClosestVertex();
	vp2 = p.getClosestVertex();
      }

      if (vt1 < vp1)
	return true;
      else if (vt1 > vp1)
	return false;
      else { //if (vt1 == vp1)
	if (vt2 < vp2)
	  return true;
	else if (vt2 > vp2)
	  return false;
	else { // if (vt2 == vp2)
	  double l = getLocationOrderedByID();
	  double lp = p.getLocationOrderedByID();
	  if (isVertex())
	    l = 0.;
	  if (p.isVertex())
	    lp = 0.;
	  return l < lp;
	}
      }
    }

#ifndef SWIG
    /**
       copy operator
    */
    inline PointOnEdge & operator=(const PointOnEdge & p) {
      Coord3D::operator=(p);
      vertex1 = p.vertex1;
      vertex2 = p.vertex2;
      location = p.location;
      return *this;
    }
#endif

    /**
       return true if the point is one of the two vertices that defined the edge
    */
    inline bool isVertex() const {
      return (location == 0.) || (location == 1.) || (vertex1 == vertex2);
    }

    /**
       return true if the point is defined by a single vertex
    */
    inline bool isNotOnEdge() const {
      return (vertex1 == vertex2);
    }

    /** return true if the current point is in the same edge of p. Specific situations (points on vertices) are
	taken into account, except the following configuration:
	(a-b) is an edge of the mesh, (*this) is located on a, \p p is located on b, but their
	description do not share any vertex.
     */
    inline bool inSameEdge(const PointOnEdge & p) const {
      if (p.isVertex())
	if ((p.getClosestVertex() == getFirstVertex()) || (p.getClosestVertex() == getSecondVertex()))
	  return true;
      if (isVertex())
	if ((getClosestVertex() == p.getFirstVertex()) || (getClosestVertex() == p.getSecondVertex()))
	  return true;
      if (!p.isVertex() && !isVertex())
	return ((p.getFirstVertex() == getFirstVertex()) && (p.getSecondVertex() == getSecondVertex())) ||
	  ((p.getFirstVertex() == getSecondVertex()) && (p.getSecondVertex() == getFirstVertex()));
      else
	return false;
    }

    /** return true if the current point is in the edge (efirst, esecond). Specific situations (points on vertices) are
	taken into account.
     */
    inline bool inEdge(VertexID efirst, VertexID esecond) const {
      assert(efirst != esecond);
      if (isVertex())
	return (getClosestVertex() == efirst) || (getClosestVertex() == esecond);
      else
	return ((efirst == getFirstVertex()) && (esecond == getSecondVertex())) ||
	  ((efirst == getSecondVertex()) && (esecond == getFirstVertex()));
    }

    /** return true if the current point is in the edge e. Specific situations (points on vertices) are
	taken into account.
     */
    inline bool inEdge(const std::pair<VertexID, VertexID> & e) const {
      return inEdge(e.first, e.second);
    }

    inline static std::pair<VertexID, VertexID> getEdge(const PointOnEdge & v1, const PointOnEdge & v2) {
      return v1.getEdge(v2);
    }

#ifndef SWIG

    /** assuming that the current point and the given points are in the same edge, return the corresponding edge.
     */
    inline std::pair<VertexID, VertexID> getEdge(const PointOnEdge & p) const {
      if (isVertex()) {
	if (p.isVertex())
	  return std::pair<VertexID, VertexID>(p.getClosestVertex(), getClosestVertex());
	else {
	  if (p.hasVertex(getClosestVertex()))
	    return std::pair<VertexID, VertexID>(p.getFirstVertex(), p.getSecondVertex());
	  else
	    throw Exception("getEdge(1): not in the same edge");
	}
      }
      else {
	if (p.isVertex()) {
	  if (hasVertex(p.getClosestVertex()))
	    return std::pair<VertexID, VertexID>(getFirstVertex(), getSecondVertex());
	  else
	    throw Exception("getEdge(1): not in the same edge");
	}
	else {
	  if (((p.getFirstVertex() == getFirstVertex()) && (p.getSecondVertex() == getSecondVertex())) ||
	      ((p.getFirstVertex() == getSecondVertex()) && (p.getSecondVertex() == getFirstVertex())))
	    return std::pair<VertexID, VertexID>(p.getFirstVertex(), p.getSecondVertex());
	  else
	    throw Exception("getEdge(1): not in the same edge");
	}
      }
    }
#endif

    /**
       return the location of the current point on the boundary of the given triangle (value between 0 and 3).
       throws an exception if the point does not take part of the triangle
    */
    inline double getLocationOnTriangle(const Triangle & t) const {
      if (isVertex()) {
	VertexID p = getClosestVertex();
	if (p == t.getP1())
	  return 0.;
	else if (p == t.getP2())
	  return 1.;
	else if (p == t.getP3())
	  return 2.;
	else
	  throw Exception("getLocationOnTriangle(1): point not in triangle");
      }
      else {
	for(unsigned int i = 0; i < 3; ++i) {
	  const std::pair<VertexID, VertexID> edge = t.getEdge(i);
	  if ((edge.first == vertex1) && (edge.second == vertex2))
	    return i + location;
	  else if ((edge.first == vertex2) && (edge.second == vertex1))
	    return i + (1. - location);
	}
      }
      throw Exception("getLocationOnTriangle(1): point not in triangle");
    }

    /**
       assuming that \p p is not defined by a single point, and assuming that p and the current point are
       in the same edge (\see inSameEdge()), return the location of the current point on the edge.
    */
    inline double getLocationOnEdge(const PointOnEdge & p) const {
      if (p.isNotOnEdge())
	throw Exception("getLocationOnEdge(1): the given point is defined by a single vertex");

      return getLocationOnEdge(std::pair<VertexID, VertexID>(p.getFirstVertex(), p.getSecondVertex()));
    }

    /**
       assuming that \p p is not a vertex point, and assuming that p and the current point are
       in the same edge (\see inSameEdge()), return the location of the current point on the edge.
    */
    inline double getLocationOnEdge(const std::pair<VertexID, VertexID> & e) const {
      if (isVertex()) {
	if (getClosestVertex() == e.first)
	  return 0.;
	else
	  if (getClosestVertex() == e.second)
	    return 1.;
	  else
	    throw Exception("getLocationOnEdge(1): point not on the given edge");
      }
      else {
	if ((getFirstVertex() == e.first) && (getSecondVertex() == e.second))
	  return location;
	else if ((getFirstVertex() == e.second) && (getSecondVertex() == e.first))
	  return 1. - location;
	else
	throw Exception("getLocationOnEdge(1): the point is not in the given edge");
      }
    }

    /**
       Return the value of a piecewise linear function computing the value from the values associated
       to the two edge points.
     */
    template <typename T>
    inline T getValueFromPLFunction(T firstValue, T secondValue) const {
      return firstValue + location * (secondValue - firstValue);
    }

    /**
       return a string that describes the current point
    */
    std::string toString() const {
      std::ostringstream s;
      s << "Point(" << getX() << ", " << getY() << ", " << getZ() << "; edge = (" << getFirstVertex() << ", " << getSecondVertex() << "); location: " << getLocation();
      return s.str();
    }

  };

  /**
     Stream operator
  */
  template <typename T, typename S, typename U>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const PointOnEdge & p) {
    f << p.toString();

    return f;
  }

}

#endif
