/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CNR-IMATI
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>
#include <sstream>

#include "VectorField.h"
#include "GeodesicDistance.h"
#include "PointOnEdge.h"
#include "Mesh.h"


using namespace Taglut;

std::string GeodesicDistance::TriangleOnPlane::toString() const {
  std::ostringstream result;

  result << "Origin: " << origin << std::endl;
  for(std::map<VertexID, Coord2D>::const_iterator p = points.begin(); p != points.end(); ++p)
    result << " - " << (*p).second << ", distance: " << mmap.getValue((*p).first) << ", " << mesh.point((*p).first) << std::endl;

  return result.str();
}

std::pair<VertexID, VertexID> GeodesicDistance::TriangleOnPlane::getOtherPoints(VertexID v1) const {
  std::pair<VertexID, VertexID> result(v1, v1);
  if (points.size() != 3)
    throw Exception("getOtherPoints(2): bad number of points");

  for(std::map<VertexID, Coord2D>::const_iterator c = points.begin(); c != points.end(); ++c)
    if ((*c).first != v1) {
      if (result.first == v1) {
	result.first = (*c).first;
      }
      else {
	if (result.second != v1)
	  throw Exception("getOtherPoint(2): too many points");
	result.second = (*c).first;
      }
    }

  if (result.second == v1)
    throw Exception("getOtherPoint(2): cannot find other points");

  return result;
}


VertexID GeodesicDistance::TriangleOnPlane::getOtherPoint(VertexID v1, VertexID v2) const {
  if (points.size() != 3)
    throw Exception("getOtherPoint(2): bad number of points");

  for(std::map<VertexID, Coord2D>::const_iterator c = points.begin(); c != points.end(); ++c)
    if (((*c).first != v1) && ((*c).first != v2))
      return (*c).first;

  throw Exception("getOtherPoint(2): cannot find another point");
}


PointOnEdge GeodesicDistance::TriangleOnPlane::getIntersection(VertexID v1, VertexID v2, const Line2D & l, const PointOnEdge & pon) const {
  PointOnEdge result(pon);
  const Coord2D & p1 = get2DCoord(v1);
  const Coord2D & p2 = get2DCoord(v2);
  Line2D v1v2(p1, p2 - p1);

  try {
    Coord2D intersection = v1v2.getIntersection(l);
    const double d1 = p1.distance2D(intersection);
    const double d2 = p2.distance2D(intersection);
    if (d1 < 1e-5) return mesh.point(v1);
    if (d2 < 1e-5) return mesh.point(v2);

    const double d12 = p1.distance2D(p2);
    assert(d1 >= 0.);
    assert(d2 >= 0.);
    assert(d12 >= 0.);

    if (fabs(d1 + d2 - d12) < 1e-5)
      return PointOnEdge(mesh.point(v1), mesh.point(v2), d1 / d12);
    else
      return pon;

  }
  catch (Exception e) {
    return pon;
  }

  return result;
}

std::pair<PointOnEdge, double> GeodesicDistance::TriangleOnPlane::findOtherIntersection(const std::pair<PointOnEdge, double> & pon) const {
  const Coord2D p(get2DLocation(pon.first));
  const double dpon2d = p.distance2D(origin);
  const Line2D p2s(p, origin - p);

  if (pon.first.isVertex()) {
    const std::pair<VertexID, VertexID> oPoints(getOtherPoints(pon.first.getClosestVertex()));
    PointOnEdge r(getIntersection(oPoints.first, oPoints.second, p2s, pon.first));

    if (r == pon.first) return pon; // point not found, return the original one

    double rd = getDistance(r);

    return std::pair<PointOnEdge, double>(r, rd);
  }
  else {
    const VertexID oPoint(getOtherPoint(pon.first.getFirstVertex(), pon.first.getSecondVertex()));

    PointOnEdge r1(getIntersection(oPoint, pon.first.getFirstVertex(), p2s, pon.first));
    PointOnEdge r2(getIntersection(oPoint, pon.first.getSecondVertex(), p2s, pon.first));
    double d1 = pon.second, d2 = pon.second;

    const bool e1 = (r1 == pon.first);
    const bool e2 = (r2 == pon.first);
    if (e1 && e2) {
      return pon; // point not found, return the original one
    }

    if (!e1) d1 = getDistance(r1);
    if (!e2) d2 = getDistance(r2);

    if (!e1 && (e2 || (d1 <= d2)) && (r1.isVertex() || !pon.first.inSameEdge(r1))) {
      return std::pair<PointOnEdge, double>(r1, d1);
    }

    if (!e2 && (e1 || (d2 <= d1)) && (r2.isVertex() || !pon.first.inSameEdge(r2))) {
      return std::pair<PointOnEdge, double>(r2, d2);
    }

    assert(false);
    throw Exception("findOtherIntersection(1): cannot find it");
  }
}

const Coord2D GeodesicDistance::TriangleOnPlane::initLocation(VertexID v1, double d1, VertexID v2, double d2) const {
  const double length = mesh.point(v1).distance(mesh.point(v2));
  const double px = ((d2 * d2 - d1 * d1) / (-length) + length) / 2;
  const double py = - sqrt(fabs(d1 * d1 - px * px));
  return Coord2D(px, py);
}

GeodesicDistance::TriangleOnPlane::TriangleOnPlane(const Triangle & t, const Mesh & mesh_, const MeshMap & mmap_) : mesh(mesh_), mmap(mmap_) {
  // position of the two first points
  points[t.getP1()] = Coord2D(0., 0.);
  points[t.getP2()] = Coord2D(mesh.point(t.getP1()).distance(mesh.point(t.getP2())), 0.);

  assert(mmap.getValue(t.getP1()) != std::numeric_limits<double>::max());
  assert(mmap.getValue(t.getP2()) != std::numeric_limits<double>::max());
  //assert(mmap[t.getP1()] + mmap[t.getP2()] > mesh.point(t.getP1()).distance(mesh.point(t.getP2())));

  // compute the location of the source
  origin = initLocation(t.getP1(), mmap[t.getP1()], t.getP2(), mmap[t.getP2()]);

  assert(fabs(points[t.getP1()].distance2D(origin) - mmap.getValue(t.getP1())) < 1e-2);
  assert(fabs(points[t.getP2()].distance2D(origin) - mmap.getValue(t.getP2())) < 1e-2);

  // compute a first possible location for the third point
  points[t.getP3()] = initLocation(t.getP1(), mesh.point(t.getP1()).distance(mesh.point(t.getP3())),
				   t.getP2(), mesh.point(t.getP2()).distance(mesh.point(t.getP3())));


  // if this location is not correct, adjust it by reversing the y value
  const double dd = fabs(points[t.getP3()].distance2D(origin) - mmap.getValue(t.getP3()));

  // and choose the best one
  if (dd > 1e-7) {
    points[t.getP3()].set2DY(-points[t.getP3()].get2DY());
    const double dd2 = fabs(points[t.getP3()].distance2D(origin) - mmap.getValue(t.getP3()));

    if (dd2 > dd) {
      points[t.getP3()].set2DY(-points[t.getP3()].get2DY());
    }
  }

}


FastMarching::WindowOnEdge::WindowOnEdge(const FastMarching::WindowOnEdge & woe,
					 double p1_t, double p2_t) : v1(woe.v1), v2(woe.v2),
								     p1(p1_t), p2(p2_t),
								     dSource(woe.dSource), direction(woe.direction) {
  const Coord2D s = woe.getSourceCoord2D();
  d1 = woe.getDistance(p1_t, s);
  d2 = woe.getDistance(p2_t, s);
  //assert(!isEmpty());
}

FastMarching::WindowOnEdge::WindowOnEdge(const FastMarching::WindowOnEdge & woe,
					 double p1_t, double p2_t,
					 const Coord2D & s) : v1(woe.v1), v2(woe.v2),
							      p1(p1_t), p2(p2_t),
							      dSource(woe.dSource), direction(woe.direction) {
  d1 = woe.getDistance(p1_t, s);
  d2 = woe.getDistance(p2_t, s);
  //assert(!isEmpty());

}

FastMarching::WindowOnEdge::WindowOnEdge(VertexID v1_t, VertexID v2_t,
					 double p1_t, double p2_t,
					 double d1_t, double d2_t,
					 TriangleID direction_t,
					 double edgeLength_t,
					 double dSource_t) {
  assert(p1_t >= 0.);
  assert(p1_t <= edgeLength_t);
  assert(p2_t >= 0.);
  assert(p2_t <= edgeLength_t);

  (*this).direction = direction_t;
  (*this).dSource = dSource_t;

  if (p1_t > p2_t) {
    (*this).p1 = p2_t;
    (*this).p2 = p1_t;
    (*this).d1 = d2_t;
    (*this).d2 = d1_t;
  }
  else {
    (*this).p1 = p1_t;
    (*this).p2 = p2_t;
    (*this).d1 = d1_t;
    (*this).d2 = d2_t;
  }

  if (v1_t > v2_t) {
    (*this).v1 = v1_t;
    (*this).v2 = v2_t;
  }
  else {
    (*this).v1 = v2_t;
    (*this).v2 = v1_t;
    const double predp1 = (*this).p1;
    (*this).p1 = edgeLength_t - (*this).p2;
    (*this).p2 = edgeLength_t - predp1;
    const double predd1 = (*this).d1;
    (*this).d1 = (*this).d2;
    (*this).d2 = predd1;
  }

  assert((*this).p1 >= 0.);
  assert((*this).p1 <= edgeLength_t);
  assert((*this).p2 >= 0.);
  assert((*this).p2 <= edgeLength_t);
  assert((*this).v1 > (*this).v2);
  //assert(!isEmpty());

}


FastMarching::WindowOnEdge::WindowOnEdge(const WindowOnEdge & woe) : v1(woe.v1), v2(woe.v2),
								     p1(woe.p1), p2(woe.p2),
								     d1(woe.d1), d2(woe.d2),
								     dSource(woe.dSource), direction(woe.direction) {
  //assert(!isEmpty());

}

std::string FastMarching::WindowOnEdge::toString() const {
  std::ostringstream s;

  s << "WindowOnEdge([" << v1 << " : " << p1 << ", " << p2 << " : " << v2 << " [" << (p2 - p1) << "]], d1: "
    << d1 << ", d2: " << d2 << ", dSource: " << dSource << ", direction: "<< direction << ")";

  return s.str();
}



std::pair<double, double> FastMarching::WindowOnEdge::getNewBoundariesInsideSegment(const WindowOnEdge & w,
										    const std::pair<double, double> & segment,
										    const Coord2D & s0, const Coord2D & s1) const {
  std::pair<double, double> r = getNewBoundaries(w, s0, s1);
  if ((r.first <= segment.first) || (r.first >= segment.second))
    r.first = -1.;
  if ((r.second <= segment.first) || (r.second >= segment.second))
    r.second = -1.;
  if ((r.first == -1.) && (r.second != -1.)) {
    r.first = r.second;
    r.second = -1.;
  }

  return r;
}

std::pair<double, double> FastMarching::WindowOnEdge::getNewBoundaries(const WindowOnEdge & w,
								       const Coord2D & s0, const Coord2D & s1) const {
  const double s1x2 = s1.get2DX() * s1.get2DX() + s1.get2DY() * s1.get2DY();

  const double alpha = s1.get2DX() - s0.get2DX();
  const double beta = w.getDSource() - getDSource();
  const double beta2 = beta * beta;
  const double gamma = ((s0.get2DX() * s0.get2DX() + s0.get2DY() * s0.get2DY()) - s1x2 - beta2);

  const double A = alpha * alpha  - beta2;
  const double B = gamma * alpha + 2 * s1.get2DX() * beta2;
  const double C = .25 * gamma * gamma - s1x2 * beta2;

  if (A == 0.) {
    if (B == 0.)
      return std::pair<double, double>(-1, -1.);
    else
      return std::pair<double, double>(-C / B, -1.);
  }
  assert(A != 0.);

  // compute the discriminant
  const double delta = B * B - 4 * A * C;
  if (delta < 0)
    return std::pair<double, double>(-1., -1.);
  else if (fabs(delta) < 1e-18)
    return std::pair<double, double>(-B / (2 * A), -1.);
  else { // two values
    const double sqrtdelta = sqrt(delta);
    const double s1_t = (-B - sqrtdelta) / (2 * A);
    const double s2_t = (-B + sqrtdelta) / (2 * A);
    if (s1_t < s2_t)
      return std::pair<double, double>(s1_t, s2_t);
    else
      return std::pair<double, double>(s2_t, s1_t);
  }
}


std::vector<std::pair<double, double> > FastMarching::WindowOnEdge::splitWindows(const std::vector<double> & points,
										 const WindowOnEdge & w2,
										 const Coord2D & s1, const Coord2D & s2) {
  std::vector<std::pair<double, double> > result;
  if (points.size() < 2)
    throw Exception("splitWindows(5): points have to be more than 2");
  assert(points.size() >= 2);
  // first, before points.front()

  if (w2.getP1() < points.front()) {
    assert(getP1() >= points.front());
    result.push_back(std::pair<double, double>(w2.getP1(), points.front()));
  }

  // then segments between points
  std::vector<double>::const_iterator p1_t = points.begin();
  for(std::vector<double>::const_iterator p2_t = points.begin() + 1; p2_t != points.end(); p1_t = p2_t, ++p2_t) {
    const double middle = .5 * (*p1_t + *p2_t);
    const double d1_t = getGlobalDistance(middle, s1);
    const double d2_t = w2.getGlobalDistance(middle, s2);
    if (d1_t >= d2_t)
      result.push_back(std::pair<double, double>(*p1_t, *p2_t));
  }

  // finally, points after points.back()
  if (w2.getP2() > points.back()) {
    assert(getP2() <= points.back());
    result.push_back(std::pair<double, double>(points.back(), w2.getP2()));
  }

  return result;
}




std::vector<double> FastMarching::WindowOnEdge::getCrossings(const WindowOnEdge & cw, const Coord2D & s, const Coord2D & scw, double epsilon) {
  const std::pair<double, double> overlapping = getOverlapping(cw);
  assert(overlapping.first >= 0.);
  assert(overlapping.second >= overlapping.first);

  const std::pair<double, double> positions = getNewBoundariesInsideSegment(cw, overlapping, s, scw);
  std::vector<double> points;

  points.push_back(overlapping.first);
  if (positions.first >= 0.) {
    assert(positions.first - points.back() >= 0.);
    if (positions.first - points.back() >= epsilon)
      points.push_back(positions.first);
  }
  if (positions.second >= 0.) {
    assert(positions.second - points.back() >= 0.);
    if (positions.second - points.back() >= epsilon)
      points.push_back(positions.second);
  }
  assert(overlapping.second - points.back() >= 0.);
  if (overlapping.second - points.back() >= epsilon)
    points.push_back(overlapping.second);
  else
    points.back() = overlapping.second;

  return points;
}


std::vector<std::pair<double, double> >
FastMarching::WindowOnEdge::getNegativeIntervals(std::priority_queue<std::pair<double, double>,
						 std::vector<std::pair<double, double> >, GeodesicOrder> & intervals) {
  std::vector<std::pair<double, double> > result;
  double begin = getP1();
  while(!intervals.empty()) {
    std::pair<double, double> i = intervals.top();
    intervals.pop();
    assert(i.first <= getP2());
    assert((result.empty()) || (i.first >= getP1()));
    assert((intervals.empty()) || (i.second <= getP2()));
    assert(i.second >= getP1());
    if (i.first > begin) {
      result.push_back(std::pair<double, double>(begin, i.first));
    }
    begin = i.second;
  }
  if (begin < getP2()) {
    result.push_back(std::pair<double, double>(begin, getP2()));
  }
  return result;
}


std::vector<FastMarching::WindowOnEdge> FastMarching::FMEdge::prepare(const WindowOnEdge & w, FastMarching & fm, const Mesh & mesh) {
  const double epsilon = fm.getEpsilon();
  // 0. update v1 and v2 if needed
  if (v1 == v2) {
    v1 = w.getV1();
    v2 = w.getV2();
    length = mesh.point(v1).distance(mesh.point(v2));
  }

  WindowOnEdge wAdjusted(w);
  Coord2D s = w.getSourceCoord2D();

  if (wAdjusted.getP1() < epsilon)
    wAdjusted.adjustP1(0., s);
  if (wAdjusted.getP2() > length - epsilon)
    wAdjusted.adjustP2(length, s);
  // 0. Adjust the boundaries of the window
  for(std::vector<WindowOnEdge>::const_iterator cw = closedWindows.begin(); cw != closedWindows.end(); ++cw)
    wAdjusted.adjustEpsilon(*cw, epsilon, s);
  for(std::vector<WindowPriorityQueue::point_iterator>::const_iterator w2 = windows.begin(); w2 != windows.end(); ++w2)
    wAdjusted.adjustEpsilon(**w2, epsilon, s);

  // 1. construct the overlapping with the closed windows
  std::priority_queue<std::pair<double, double>,
    std::vector<std::pair<double, double> >, GeodesicOrder> overlapping;

  std::vector<WindowOnEdge> cwAdd;
  std::vector<std::vector<WindowOnEdge>::iterator> cwRemove;

  for(std::vector<WindowOnEdge>::iterator cw = closedWindows.begin(); cw != closedWindows.end(); ++cw)
    if (wAdjusted.intersects(*cw)) {
      Coord2D scw = (*cw).getSourceCoord2D();
      std::vector<double> points = wAdjusted.getCrossings(*cw, s, scw, epsilon);
#ifndef NDEBUG
      for(std::vector<double>::const_iterator p = points.begin(); p != points.end(); ++p) {
	assert(*p >= wAdjusted.getP1());
	assert(*p <= wAdjusted.getP2());
      }
#endif

      std::vector<std::pair<double, double> > cwSplit = wAdjusted.splitWindows(points, *cw, s, scw);
      if (cwSplit.size() == 0) {
	cwRemove.push_back(cw);
      }
      else {
	assert(cwSplit.size() != 0);
	overlapping.push(cwSplit.front());
	if ((cwSplit.size() > 1) || (!((cwSplit.front().first == (*cw).getP1()) && (cwSplit.front().second == (*cw).getP2())))) {
	  // updating the current window
	  *cw = WindowOnEdge(*cw, cwSplit.front().first, cwSplit.front().second, scw);

	  // then add the possible other subwindows in order to add it at the end of the process
	  for(std::vector<std::pair<double, double> >::const_iterator newCW = cwSplit.begin() + 1; newCW != cwSplit.end(); ++newCW)
	    /*if ((*newCW).second - (*newCW).first > epsilon)*/ {
	      assert(!(*newCW).first < (*newCW).second);
	      overlapping.push(*newCW);
	      cwAdd.push_back(WindowOnEdge(*cw, (*newCW).first, (*newCW).second, scw));
	    }
	}
      }
    }

  for(std::vector<std::vector<WindowOnEdge>::iterator>::const_reverse_iterator cwr = cwRemove.rbegin(); cwr != cwRemove.rend(); ++cwr)
    closedWindows.erase(*cwr);

  for(std::vector<WindowOnEdge>::const_iterator cwa = cwAdd.begin(); cwa != cwAdd.end(); ++cwa)
    closedWindows.push_back(*cwa);

  std::vector<WindowPriorityQueue::point_iterator> eRemove;
  std::vector<WindowOnEdge> result;
  // 2. compute the overlapping with the open windows
  for(std::vector<WindowPriorityQueue::point_iterator>::const_iterator w2 = windows.begin(); w2 != windows.end(); ++w2)
    if (wAdjusted.intersects(**w2)) {
      Coord2D sw2 = (**w2).getSourceCoord2D();
      std::vector<double> points = wAdjusted.getCrossings(**w2, s, sw2, epsilon);

      std::vector<std::pair<double, double> > w2Split = wAdjusted.splitWindows(points, **w2, s, sw2);
      if (w2Split.size() == 0) {
	eRemove.push_back(*w2);
      }
      else {
	assert(w2Split.size() != 0);
	overlapping.push(w2Split.front());
	if ((w2Split.size() > 1) || (!((w2Split.front().first == (**w2).getP1()) && (w2Split.front().second == (**w2).getP2())))) {
	  // updating the current window
	  assert(w2Split.front().first < w2Split.front().second);
	  fm.updateWindow(*w2, WindowOnEdge(**w2, w2Split.front().first, w2Split.front().second, sw2));

	  // then add the possible other subwindows in order to add it at the end of the process
	  for(std::vector<std::pair<double, double> >::const_iterator newW2 = w2Split.begin() + 1; newW2 != w2Split.end(); ++newW2)
	    /*if ((*newW2).second - (*newW2).first > epsilon) */ {
	      assert(!(*newW2).first < (*newW2).second);
	      overlapping.push(*newW2);
	      result.push_back(WindowOnEdge(**w2, (*newW2).first, (*newW2).second, sw2));
	      assert((*newW2).first < (*newW2).second);
	    }
	}
      }
    }

  fm.removeWindows(eRemove);

  // 3. compute the negative on wAdjusted
  std::vector<std::pair<double, double> > negative = wAdjusted.getNegativeIntervals(overlapping);
  for(std::vector<std::pair<double, double> >::const_iterator n = negative.begin(); n != negative.end(); ++n)
    /*if ((*n).second - (*n).first > epsilon) */ {
    assert((*n).first < (*n).second);
    result.push_back(WindowOnEdge(wAdjusted, (*n).first, (*n).second, s));
  }

  return result;
}


void FastMarching::addWindow(const WindowOnEdge & w, MeshMap & mMap) {
  assert(mMap.getMesh().point(w.getV1()).hasNeighbour(w.getV2()));
  assert(w.getP1() >= 0);
  assert(w.getP2() >= 0);
  assert(w.getP2() >= w.getP1());

  if (w.isEmpty() || w.isSmall())
    return;
  // update the meshmap
  updateMeshMap(mMap, w);
  // Modify the corresponding edge
  FMEdge & edge = getEdge(w);
  std::vector<WindowOnEdge> resultingWindows = edge.prepare(w, *this, mMap.getMesh());

  for(std::vector<WindowOnEdge>::const_iterator rw = resultingWindows.begin(); rw != resultingWindows.end(); ++rw)
    if (!(*rw).isEmpty()) {
      //assert((*rw).getSize() >= epsilon);

      // update the priority queue
      WindowPriorityQueue::point_iterator wpq = windows.push(*rw);
      // add it to the corresponding edge
      edge.addWindow(wpq);
    }
}



void FastMarching::removeWindow(const WindowPriorityQueue::point_iterator & w) {
  // remove the window from the edges
  const std::pair<VertexID, VertexID> vertices((*w).getV1(), (*w).getV2());
  FMEdge & fme = edges[vertices];
  fme.removeWindow(w);

  // remove it from the windows
  windows.erase(w);

  assert(windows.size() == getNbWindowsInEdges());
}


void FastMarching::removeFirstWindow() {
  // remove the window from the edges
  const WindowOnEdge & w = windows.top();
  const std::pair<VertexID, VertexID> vertices(w.getV1(), w.getV2());
  FMEdge & fme = edges[vertices];
  fme.closeWindow(w);

  // remove it from the windows
  windows.pop();
}


void FastMarching::propagateWindow(MeshMap & mMap, const WindowOnEdge & w, const Triangle & t, bool useFlags, int tFlag) {
  const Mesh & mesh = mMap.getMesh();
  assert(mesh.point(w.getV1()).hasNeighbour(w.getV2()));
  // distances
  const double eLength = mesh.point(w.getV1()).distance(mesh.point(w.getV2()));
  const double d1 = w.getD1();
  const double d2 = w.getD2();

  // location of the two extrema
  const double b0x = w.getP1();
  const double b1x = w.getP2();
  assert(b0x < b1x);
  // if (d1 + d2 < b1x - b0x) // epsilon ?
  //   return;

  // location of the source
  const double sx = ((d2 * d2 - d1 * d1) / (b0x - b1x) + b0x + b1x) / 2;
  const double sy = sqrt(fabs(d1 * d1 - (b0x - sx) * (b0x - sx)));

  // location of the new point (distances first)
  const VertexID oPoint = t.getOtherPoint(w.getV1(), w.getV2());
  const Point3D & moPoint = mesh.point(oPoint);
  const double e1 = moPoint.distance(mesh.point(w.getV1()));
  const double e2 = moPoint.distance(mesh.point(w.getV2()));

  // location of the other point in R^2
  // TODO: find another way to compute the 2D location of the source in order to improve the distance computation...
  const double p2x = ((e2 * e2 - e1 * e1) / (-eLength) + eLength) / 2;
  const double p2y = - sqrt(fabs(e1 * e1 - p2x * p2x));

  const Coord2D s(sx, sy);
  const Coord2D b0(b0x, 0.);
  const Coord2D b1(b1x, 0.);
  const Coord2D p0(0., 0.);
  const Coord2D p1(eLength, 0.);
  const Coord2D p2(p2x, p2y);

  const Line2D l1(s, b0 - s);
  const Line2D l2(s, b1 - s);
  const Line2D ee1(p0, p2 - p0);
  const Line2D ee2(p1, p2 - p1);

  std::pair<VertexID, VertexID> int1, int2;
  Coord2D i1p;
  double i1 = -1.;
  double i1norm;

  if (d1 == 0.) {
    i1 = 0.;
    i1p = p0;
  }
  else if (d2 != 0.) {
    try {
      i1norm = ee2.getLocation(l1);
      i1p = ee2.getPointFromLocation(i1norm);
      i1 = i1norm * ee2.getVector().norm2D();
    }
    catch (Exception e) {
      // parallel
    }
    if ((i1 >= 0.) && (i1 <= e2)) {
      if (i1 <= epsilon) i1 = 0.;
      if (i1 >= e2 - epsilon) i1 = e2;
      int1.first = w.getV2();
    }
    else {
      i1 = -1.;
      try {
	i1norm = ee1.getLocation(l1);
	i1p = ee1.getPointFromLocation(i1norm);
	i1 = i1norm * ee1.getVector().norm2D();
	if (i1 <= epsilon) i1 = 0.;
	if (i1 >= e1 - epsilon) i1 = e1;
	assert((i1 >= 0.) && (i1 <= e1));
	int1.first = w.getV1();
      }
      catch (Exception e) {
	// parallel
      }
    }
  }

  Coord2D i2p;
  double i2 = -1.;
  double i2norm;

  if (d2 == 0.) {
    i2 = 0.;
    i2p = p1;
  }
  else if (d1 != 0.) {
    try {
      i2norm = ee1.getLocation(l2);
      i2p = ee1.getPointFromLocation(i2norm);
      i2 = i2norm * ee1.getVector().norm2D();
    }
    catch (Exception e) {
      // parallel
    }

    if ((i2 >= 0.) && (i2 <= e1)) {
      if (i2 <= epsilon) i2 = 0.;
      if (i2 >= e1 - epsilon) i2 = e1;
      int2.first = w.getV1();
    }
    else {
      i2 = -1.;
      try {
	i2norm = ee2.getLocation(l2);
	i2p = ee2.getPointFromLocation(i2norm);
	i2 = i2norm * ee2.getVector().norm2D();

	if (i2 <= epsilon) i2 = 0.;
	if (i2 >= e2 - epsilon) i2 = e2;
	assert((i2 >= 0.) && (i2 <= e2));
	int2.first = w.getV2();
      }
      catch (Exception e) {
	// parallel
      }
    }
  }

  int1.second = int2.second = oPoint;

  if (i2 == -1.) { // special case: not first propagation on saddle point...
    assert(i1 != -1.);
    i2 = i1;
    i2p = i1p;
    int2.first = int1.first;
  }
  else if (i1 == -1.) {
    i1 = i2;
    i1p = i2p;
    int1.first = int2.first;
  }

  assert(i1 >= 0.);
  assert(i1 <= mesh.point(int1.first).distance(mesh.point(int1.second)) + epsilon);
  assert(i2 >= 0.);
  assert(i2 <= mesh.point(int2.first).distance(mesh.point(int2.second)) + epsilon);

  const double dint1 = i1p.distance2D(b0) + d1;
  const double dint2 = i2p.distance2D(b1) + d2;

  // propagating the window
  if (int1.first == int2.first) {
    if (fabs(i2 - i1) > epsilon) { // epsilon ?
      const double de = mesh.point(int1.first).distance(moPoint);
      addWindow(WindowOnEdge(int1.first, oPoint, i1, i2,
			     dint1,
			     dint2,
			     t.getId(), de,
			     w.getDSource()), mMap);
    }

    const bool isBoundaryV1 = mesh.point(w.getV1()).getIsBoundary() || (useFlags && (mesh.hasTriangleDifferentFlag(w.getV1(), tFlag)));
    const bool isBoundaryV2 = mesh.point(w.getV2()).getIsBoundary() || (useFlags && (mesh.hasTriangleDifferentFlag(w.getV2(), tFlag)));
    // if an extremity is a saddle point
    if (((fabs(w.getP1()) == 0.)  && (isBoundaryV1 || (mesh.isSaddlePoint(w.getV1())))) &&
	(w.getV1() != int1.first)) {
      assert(w.getV1() != int1.first);
      assert(w.getV1() != int1.second);
      assert(mesh.point(w.getV1()).hasNeighbour(oPoint));
      addWindow(WindowOnEdge(w.getV1(), oPoint, 0., e1,
			     0., e1,
			     t.getId(), e1,
			     w.getGlobalD1()), mMap);
      double pos = i1 > i2 ? i1 : i2;
      double distance;
      if (i1 == i2) {
	pos = 0.;
	distance = eLength;
      }
      else
	distance = p0.distance2D(i1 > i2 ? i1p : i2p);
      assert(pos <= e2);
      assert(mesh.point(w.getV2()).hasNeighbour(oPoint));
      if (e2 - pos > epsilon) {
	addWindow(WindowOnEdge(w.getV2(), oPoint, pos, e2,
			       distance, e1,
			       t.getId(), e2,
			       w.getGlobalD1()), mMap);
      }
    }
    else if (((w.getP2() == eLength) && (isBoundaryV2 || (mesh.isSaddlePoint(w.getV2())))) &&
	(w.getV2() != int1.first)) {
      assert(w.getV2() != int1.first);
      assert(w.getV2() != int1.second);
      assert(mesh.point(w.getV2()).hasNeighbour(oPoint));
      addWindow(WindowOnEdge(w.getV2(), oPoint, 0., e2,
			     0., e2,
			     t.getId(), e2,
			     w.getGlobalD2()), mMap);
      double pos = i1 > i2 ? i1 : i2;
      double distance;
      if (i1 == i2) {
	pos = 0.;
	distance = eLength;
      }
      else
	distance = p1.distance2D(i1 > i2 ? i1p : i2p);
      assert(pos <= e1);
      assert(mesh.point(w.getV1()).hasNeighbour(oPoint));
      if (e1 - pos > epsilon) {
	addWindow(WindowOnEdge(w.getV1(), oPoint, pos, e1,
			       distance, e2,
			       t.getId(), e1,
			       w.getGlobalD2()), mMap);
      }
    }


  }
  else {
    const double dfo = mesh.point(int1.first).distance(moPoint);
    assert(mesh.point(int1.first).hasNeighbour(oPoint));
    addWindow(WindowOnEdge(int1.first, oPoint, i1, dfo,
			   dint1,
			   s.distance2D(p2),
			   t.getId(), dfo,
			   w.getDSource()), mMap);
    const double dfo2 = mesh.point(int2.first).distance(moPoint);
    assert(mesh.point(int2.first).hasNeighbour(oPoint));
    addWindow(WindowOnEdge(int2.first, oPoint, i2, dfo2,
			   dint2,
			   s.distance2D(p2),
			   t.getId(), dfo2,
			   w.getDSource()), mMap);

  }
}

bool FastMarching::propagateFirstWindow(MeshMap & mMap, double stop,
					const std::vector<VertexID> & targets,
					std::vector<bool> & seentargets,
					bool alltargets,
					bool useFlags, int tFlag) {
  // then get the first window
  const WindowOnEdge w = windows.top();
  removeFirstWindow();
  if (w.isEmpty())
    return false;

  const VertexID v1 = w.getV1();
  const VertexID v2 = w.getV2();

  // if the new window is after the stop position, then break
  if ((stop >= 0.) && (w.getMinDistance() >= stop))
    return true;

  // if a target point has been passed
  if ((targets.size() != 0) && (!existsOpenEdge(w))) {
    unsigned int idtarget = 0;
    for(std::vector<VertexID>::const_iterator t = targets.begin(); t != targets.end(); ++t, ++idtarget)
      if (!alltargets || (!seentargets[idtarget])) {
	if (((*t) == v1) || ((*t) == v2)) { // the current edge contains a target
	  if (!hasOpenEdgeFromVertex(mMap.getMesh().point(*t)) && mMap[*t] != default_value) {
	    // and no other edge in the wavefront contains this target
	    if (alltargets) {
	      assert(targets.size() == seentargets.size());
	      seentargets[idtarget] = true;
	      bool all = true;
	      for(std::vector<bool>::const_iterator s = seentargets.begin(); s != seentargets.end(); ++s)
		if (!(*s)) {
		  all = false;
		  break;
		}
	      // all the targets has been visited
	      if (all) {
		return true;
	      }
	    }
	    else {
	      return true;
	    }
	  }
	}
      }
  }


  try {
    const Triangle & t = mMap.getMesh().findOtherTriangle(v1, v2, w.getDirection());
    if ((!useFlags) || (t.getFlag() == tFlag))
      propagateWindow(mMap, w, t, useFlags, tFlag);
  }
  catch (Exception e) {
    // boundary
  }

  return false;
}


void FastMarching::computeDistanceInternal(MeshMap & result, Mesh & mesh, const std::vector<VertexID> & points, int tFlag, bool useFlags, double stop,
					   const std::vector<VertexID> & targets, bool alltargets) {
  // first, strip the targets structure
  // TODO: check for redundency with the set in the POE functions
  std::vector<VertexID> uniqueTargets(targets);
  std::sort(uniqueTargets.begin(), uniqueTargets.end());
  std::vector<VertexID>::iterator new_end_pos;
  new_end_pos = std::unique(uniqueTargets.begin(), uniqueTargets.end());
  uniqueTargets.erase(new_end_pos, uniqueTargets.end());

  std::vector<bool> seentargets;
  if (alltargets) {
    seentargets.insert(seentargets.begin(), uniqueTargets.size(), false);
    // remove all the target points that are part of the origin points
    std::vector<bool>::iterator s = seentargets.begin();
    for(std::vector<VertexID>::const_iterator pp = uniqueTargets.begin(); pp != uniqueTargets.end(); ++pp, ++s)
      for(std::vector<VertexID>::const_iterator ppp = points.begin(); ppp != points.end(); ++ppp)
	if (*ppp == *pp)
	  *s = true;
  }

  const TriangleID tout = mesh.getNbTriangles();
  if (result.getNbValues() != mesh.getNbPoints())
    throw Exception("computeDistanceFromPoints(4): wrong number of parameters");
  result.initValues(default_value);
  epsilon = mesh.getLengthSmallestEdge();
  if (epsilon == 0.)
    epsilon = 1e-6;
  else
    epsilon *= 1e-6;
  assert(epsilon != 0.);

  edges.clear();
  windows.clear();
  assert(windows.size() == 0);

  // init windows arround the points
  for(std::vector<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p) {
    const Point3D & point = mesh.point(*p);
    result[*p] = 0.;
    std::deque<VertexID>::const_iterator nbpred = point.getNeighbours().begin() + (point.getNeighbours().size() - 1);
    for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); nbpred = nb, ++nb) {
      TriangleID tid = tout;
      try {
	tid = mesh.findTriangle(*p, *nb, *nbpred).getId();
      }
      catch (Exception e) {
	// triangle not found
      }
      if (tid != tout)
	addWindow(WindowOnEdge(*nbpred, *nb,
			       0., mesh.point(*nbpred).distance(mesh.point(*nb)),
			       point.distance(mesh.point(*nbpred)), point.distance(mesh.point(*nb)),
			       tid,
			       mesh.point(*nbpred).distance(mesh.point(*nb))), result);
    }
  }

  // then apply propagation
  while((!windows.empty()) && !propagateFirstWindow(result, stop, uniqueTargets, seentargets, alltargets, useFlags, tFlag)) {
    // propagate using the first edges. if the return value is true, a break condition has been detected...
  }


#ifndef NDEBUG
  // DEBUG

  double WPE = 0.;
  for(std::map<std::pair<VertexID, VertexID>, FMEdge, ltPair>::iterator e = edges.begin(); e != edges.end(); ++e) {
    WPE += (*e).second.getNbClosedWindows();
  }
  std::cout << "WPE: " << WPE << std::endl;

  if (uniqueTargets.size() == 0) {
    for(std::map<std::pair<VertexID, VertexID>, FMEdge, ltPair>::const_iterator e = edges.begin(); e != edges.end(); ++e) {
      const double elength = mesh.point(((*e).second.getV1())).distance(mesh.point((*e).second.getV2()));
      const double wlength = (*e).second.getWindowLength();
      if (elength > wlength + 1e1 * epsilon) {
	std::cout << "Warning: edge (" << (*e).second.getV1() << " (" << result.getValue((*e).second.getV1()) << "), " <<
	  (*e).second.getV2() << " (" << result.getValue((*e).second.getV2()) << ")" <<
	  "): [" << (elength - wlength) << "]" << std::endl;
      }
    }
  }
#endif

  edges.clear();
}

std::pair<double, double> FastMarching::FMEdge::extendExtremityWindows() {
  if (!closedWindows.empty()) {

    std::vector<WindowOnEdge>::iterator firstWindow = closedWindows.begin();
    std::vector<WindowOnEdge>::iterator lastWindow = closedWindows.begin();

    // get the extremity windows
    for(std::vector<WindowOnEdge>::iterator w = closedWindows.begin(); w != closedWindows.end(); ++w) {
      if ((*firstWindow).getP1() > (*w).getP1())
	firstWindow = w;
      if ((*lastWindow).getP2() < (*w).getP2())
	lastWindow = w;

    }
    assert(length > 0.);
    (*firstWindow).adjustP1(0.);
    (*lastWindow).adjustP2(length);
    return std::pair<double, double>((*firstWindow).getGlobalD1(), (*firstWindow).getGlobalD2());
  }
  else
    return std::pair<double, double>(-1., -1.);
}

PLPath GeodesicDistance::getShortestPath(Mesh & mesh, VertexID p1, VertexID p2) {
  MeshMap mMap(mesh);
  // compute distance
  std::vector<VertexID> source;
  source.push_back(p1);
  std::vector<VertexID> target;
  target.push_back(p2);

  computeDistanceInternal(mMap, mesh, source, 0, false, -1., target, false);

  // then compute the corresponding integral line
  return getPathToOrigin(PointOnEdge(mesh.point(p2)), mesh, mMap);
}


std::pair<PointOnEdge, double> GeodesicDistance::findNextPointOnGeodesic(const std::pair<PointOnEdge, double> & pOn, const Mesh & mesh, const MeshMap & mmap) {

  std::pair<PointOnEdge, double> result = pOn;
  if (pOn.first.isVertex()) {
    const Point3D & p = mesh.point(pOn.first.getClosestVertex());

    for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end() ; ++t) {
      TriangleOnPlane top(mesh.triangle(*t), mesh, mmap);

       std::pair<PointOnEdge, double> result2 = top.findOtherIntersection(pOn);
       if (result2.second < result.second) {
	 result = result2;
       }
    }
    // try the points in the 1-star
    if (result.second >= pOn.second) {
      for(std::deque<VertexID>::const_iterator n = p.getNeighbours().begin(); n != p.getNeighbours().end() ; ++n) {
	const double d = mmap.getValue(*n);
	if ((d < result.second) || ((result.second == pOn.second) && !mesh.point(*n).getIsBoundary())) {
	  result = std::pair<PointOnEdge, double>(mesh.point(*n), d);
	}
      }
    }
  }
  else {
    double dOnTop;
    if (mmap.getValue(pOn.first.getFirstVertex()) == 0.) {
      return std::pair<PointOnEdge, double>(mesh.point(pOn.first.getFirstVertex()), 0.);
    }
    else if (mmap.getValue(pOn.first.getSecondVertex()) == 0) {
      return std::pair<PointOnEdge, double>(mesh.point(pOn.first.getSecondVertex()), 0.);
    }
    else {
      const Triangle & t = mesh.findTriangle(pOn.first.getFirstVertex(), pOn.first.getSecondVertex());
      TriangleOnPlane top(t, mesh, mmap);
      const double dOnTop1 = top.getDistance(pOn.first);
      dOnTop = dOnTop1;

      result = top.findOtherIntersection(pOn);
      try {
	const Triangle & t2 = mesh.findOtherTriangle(pOn.first.getFirstVertex(), pOn.first.getSecondVertex(), t);
	TriangleOnPlane top(t2, mesh, mmap);
	const double dOnTop2 = top.getDistance(pOn.first);

	std::pair<PointOnEdge, double> result2 = top.findOtherIntersection(pOn);

	// suggestion: recalculer la distance de pOn dans les triangles où sont calculés result et result2, cette distance peut changer...
	if ((result.second == dOnTop1) || (((dOnTop2 - result2.second) > (dOnTop1 - result.second)) && !(result2.second == dOnTop2))) {
	  bool isgood1 = !mesh.isBoundaryEdge(result.first) || (result.second < 1e-5);
	  bool isgood2 = !mesh.isBoundaryEdge(result2.first) || (result2.second < 1e-5);
	  if (isgood2 || !isgood1) {
	    result = result2;
	    dOnTop = dOnTop2;
	  }
	}
      }
      catch (Exception) {
      }

      if ((result.second >= dOnTop) && (!mesh.isBoundaryEdge(pOn.first))) {
	// trick to find a best point
	const double d1 = mmap.getValue(pOn.first.getFirstVertex());
	const double d2 = mmap.getValue(pOn.first.getSecondVertex());
	bool isgood1 = !mesh.point(pOn.first.getFirstVertex()).getIsBoundary() || (d1 == 0.);
	bool isgood2 = !mesh.point(pOn.first.getSecondVertex()).getIsBoundary() || (d2 == 0.);
	if ((d1 < pOn.second + 1e-5) && ((d1 <= d2) || !isgood2) && (isgood1))
	  return std::pair<PointOnEdge, double>(mesh.point(pOn.first.getFirstVertex()), d1);
	else if ((d2 < pOn.second + 1e-5) && ((d2 <= d1) || !isgood1) && (isgood2))
	  return std::pair<PointOnEdge, double>(mesh.point(pOn.first.getSecondVertex()), d2);
      }
    }
  }


  return result;
}

double GeodesicDistance::getExactDistance(const PointOnEdge & pOn, const Mesh & mesh, const MeshMap & mmap) {
  if (pOn.isVertex())
    return mmap.getValue(pOn.getClosestVertex());
  else {
    const Triangle & t = mesh.findTriangle(pOn.getFirstVertex(), pOn.getSecondVertex());
    TriangleOnPlane top(t, mesh, mmap);
    return top.getDistance(pOn);
  }
}



PLPath GeodesicDistance::getPathToOrigin(const PointOnEdge & pOn, const Mesh & mesh, const MeshMap & mmap) {
  assert(mmap.getValue(pOn.getFirstVertex()) >= 0);
  assert(mmap.getValue(pOn.getSecondVertex()) >= 0);
  PLPath result(mesh);
  result.push_back(pOn);

  assert(!pOn.isVertex() || pOn.distance(mesh.point(pOn.getClosestVertex())) == 0.);

  double d = getExactDistance(result.back(), mesh, mmap);

  while(mmap.getValue(result.back()) != 0.) {
    const std::pair<PointOnEdge, double> n = findNextPointOnGeodesic(std::pair<PointOnEdge, double>(result.back(), d),  mesh, mmap);
    if ((result.back() == n.first) || ((result.size() >= 2) && (result[result.size() - 2] == n.first))) {
      std::cout << "Length: " << result.size() << ", current distance to origin: " << getExactDistance(result.back(), mesh, mmap) << " (starting from " << getExactDistance(result.front(), mesh, mmap) << ")" << std::endl;
      for(PLPath::const_iterator p = result.begin(); p != result.end(); ++p) {
	std::cout << ">>>> " << (*p).toString() << ", distance: " << getExactDistance(*p, mesh, mmap) << std::endl;
	mesh.tracePoint((*p).getFirstVertex());
	mesh.tracePoint((*p).getSecondVertex());
	std::cout << "Distances: " << (*p).getFirstVertex() << ": " << mmap.getValue((*p).getFirstVertex()) << " and " << (*p).getSecondVertex() << ": " << mmap.getValue((*p).getSecondVertex()) << std::endl;
      }

      throw Exception("getPathToOrigin(3): cannot find a path");
    }
    if (n.second < 1e-5) {
      result.push_back(mesh.point(n.first.getClosestVertex()));
      assert(mmap.getValue(result.back()) == 0.);
      d = 0.;
    }
    else {
      result.push_back(n.first);
      d = n.second;
    }
  }
  return result;
}


