/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>

#include "MeshMap.h"
#include "VectorField.h"
#include "PLPath.h"
#include "Mesh.h"
#include "Exception.h"
#include "Path3D.h"
#include "Coord3D.h"

using namespace Taglut;


PLPath::PLPath(const PointOnEdge & point_t, bool up, const Mesh & mesh_t, const MeshMap & mMap, const VectorField & vf, double length_t) : mesh(&mesh_t) {
  addPointAtEnd(point_t);
  double currentLength = 0.;
  double limit = up ? mMap.getMaxValue() : mMap.getMinValue();
  while ((length_t < 0) || (currentLength < length_t)) {
    if (mMap.getValue(points.back()) == limit) {
      break;
    }

    PointOnEdge p;
    if (points.back().isVertex()) {
      try {
	p = vf.getNextPoint(mesh_t.point(points.back().getClosestVertex()), up, mesh_t, mMap);

      } catch (Exception e) {
	// end of a path
	break;
      }
    }
    else if(points.size() != 1) {
      try {
	const Triangle & t = mesh_t.findOtherTriangle(points.back().getFirstVertex(), points.back().getSecondVertex(),
						      mesh_t.findTriangle(points.back(), points[points.size() - 2]));
	p = vf.getNextPoint(points.back(), t, up, mesh_t, mMap);
      } catch (Exception e) {
	// end of a path
	break;
      }
    }
    else {
      bool found = true;
      TriangleID tt1 = (*mesh).getNbTriangles();
      PointOnEdge p1;
      try {
	const Triangle & t1 = mesh_t.findTriangle(points.back().getFirstVertex(), points.back().getSecondVertex());
	tt1 = t1.getId();
	p1 = vf.getNextPoint(points.back(), t1, up, mesh_t, mMap);
      } catch (Exception e) {
	found = false;
      }
      double value1 = std::numeric_limits<double>::max();
      if (found) {
	value1 = mMap.getValue(p1);
      }

      if (tt1 != (*mesh).getNbTriangles()) {
	found = true;

	try {
	  const Triangle & t = mesh_t.findOtherTriangle(points.back().getFirstVertex(), points.back().getSecondVertex(), tt1);
	  p = vf.getNextPoint(points.back(), t, up, mesh_t, mMap);
	} catch (Exception e) {
	  found = false;
	}

	double value = std::numeric_limits<double>::max();
	if (found) {
	  value = mMap.getValue(p);
	}

	if ((value == std::numeric_limits<double>::max()) && (value1 == std::numeric_limits<double>::max())) {
	  break; // end of a path
	}

	if ((value1 != std::numeric_limits<double>::max()) &&
	    ((value == std::numeric_limits<double>::max()) || (up && (value < value1)) || ((!up) && (value > value1))))
	  p = p1;
      }
      else {
	break; // no triangle found, end of path
      }

    }
    if ((up && (mMap.getValue(p) < mMap.getValue(points.back()) - 1e-7)) ||
	((!up) && (mMap.getValue(p) > mMap.getValue(points.back()) + 1e-7))) {
      break;
    }
    currentLength += p.distance(points.back());
    addPointAtEnd(p);
  }
}

PLPath & PLPath::addPointAtBeginning(const PointOnEdge & point) {
  assert(mesh != NULL);
  if (points.size() != 0) {
#ifndef NDEBUG
    try {
      if ((point.getLocation() != 0.) && (point.getLocation() != 1.) &&
	  (points.front().getLocation() != 0.) && (points.front().getLocation() != 1.)) {
	(*mesh).findTriangle(point, points.front());
      }
    }
    catch (Exception e) {
      throw Exception("addPointAtEnd(1): the new point is not connected to the path");
    }
#endif
    points.push_front(point);
  }
  else {
    points.push_front(point);
  }
  return *this;
}

PLPath & PLPath::addPointAtEnd(const PointOnEdge & point) {
  assert(mesh != NULL);
  if (points.size() != 0) {
#ifndef NDEBUG
    try {
      if ((point.getLocation() != 0.) && (point.getLocation() != 1.) &&
	  (points.back().getLocation() != 0.) && (points.back().getLocation() != 1.)) {
	(*mesh).findTriangle(point, points.back());
      }
    }
    catch (Exception e) {
      throw Exception("addPointAtEnd(1): the new point is not connected to the path");
    }
#endif
    points.push_back(point);
  }
  else {
    points.push_back(point);
  }
  return *this;
}

PLPath & PLPath::append(const PLPath & path) {
  assert(mesh != NULL);
#ifndef NDEBUG
  if ((points.size() != 0) && (path.size() != 0)) {
    try {
      if ((path.front().getLocation() != 0.) && (path.front().getLocation() != 1.) &&
	  (points.back().getLocation() != 0.) && (points.back().getLocation() != 1.)) {
	(*mesh).findTriangle(path.front(), points.back());
      }
    }
    catch (Exception e) {
      throw Exception("addPointAtEnd(1): the new point is not connected to the path");
    }
  }
#endif

  points.insert(points.end(), path.begin(), path.end());

  return *this;
}



double PLPath::length() const {
  if (points.size() <= 1)
    return 0.0;

  double result = 0.;
  std::deque<PointOnEdge>::const_iterator ppred = points.begin();
  for(std::deque<PointOnEdge>::const_iterator p = ++(points.begin()); p != points.end(); ++p, ++ppred)
    result += (*ppred).distance(*p);

  return result;
}

PLPath & PLPath::reversePath() {
  std::reverse(points.begin(), points.end());
  return *this;
}

PLPath PLPath::computeIntegralLine(const PointOnEdge & point, bool up,
				     const Mesh & mesh,
				     const MeshMap & mMap, const VectorField & vf,
				     double length) {
  return PLPath(point, up, mesh, mMap, vf, length);
}

std::vector<PLPath> PLPath::getIntegralLinesFromSaddlePoint(const Point3D & point,
							    const Mesh & mesh, const MeshMap & mMap, const VectorField & vf,
							    double length, bool twoSides) {
  std::vector<PLPath> result;
  assert(mMap.isSaddlePoint(point.getId()));

  double valuePoint = mMap[point.getId()];
  VertexID pUp[2] = {mesh.getNbPoints(), mesh.getNbPoints()};
  VertexID pDown[2] = {mesh.getNbPoints(), mesh.getNbPoints()};
  unsigned char idSide = 0;
  unsigned char idInP = 0;

  // first computes the next points on integral lines in both sides
  double diff = 0;
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin();
      nb != point.getNeighbours().end(); ++nb) {
    double v = mMap[*nb];
    if ((nb != point.getNeighbours().begin()) && (diff * (v - valuePoint) < 0)) {
      ++idSide;
      if (idSide == 5)
	throw Exception("getIntegralLinesFromSaddlePoint(5): this method is not able to manage with non regular saddle points.");
      if (idSide == 2)
	idInP = 1;
      if (idSide == 4)
	idInP = 0;
    }
    diff = v - valuePoint;
    if (diff > 0) {
      if ((pUp[idInP] == mesh.getNbPoints()) || (v > mMap[pUp[idInP]])) {
	pUp[idInP] = *nb;
      }
    }
    else if (diff < 0) {
      if ((pDown[idInP] == mesh.getNbPoints()) || (v < mMap[pDown[idInP]])) {
	pDown[idInP] = *nb;
      }
    }
    else {
      std::cout << "getIntegralLinesFromSaddlePoint(5): flat edge detected" << std::endl;
    }
  }
  if ((pUp[0] == mesh.getNbPoints()) || (pUp[1] == mesh.getNbPoints()) ||
      (pDown[0] == mesh.getNbPoints()) || (pDown[1] == mesh.getNbPoints()))
    throw Exception("getIntegralLinesFromSaddlePoint(5): a point is missing in the neighbourhood of the saddle point.");

  if (twoSides) {
    result.push_back(PLPath(PointOnEdge(mesh.point(pUp[0]), point), true, mesh, mMap, vf, length - point.distance(mesh.point(pUp[0]))));
    result.back().addPointAtBeginning(point);
    result.push_back(PLPath(PointOnEdge(mesh.point(pUp[1]), point), true, mesh, mMap, vf, length - point.distance(mesh.point(pUp[1]))));
    result.back().addPointAtBeginning(point);
    result.push_back(PLPath(PointOnEdge(mesh.point(pDown[0]), point), false, mesh, mMap, vf, length - point.distance(mesh.point(pDown[0]))));
    result.back().addPointAtBeginning(point);
    result.push_back(PLPath(PointOnEdge(mesh.point(pDown[1]), point), false, mesh, mMap, vf, length - point.distance(mesh.point(pDown[1]))));
    result.back().addPointAtBeginning(point);
  }
  else {
    // then compute the level sets from each found points
    bool up = true;
    if (mMap[pUp[1]] > mMap[pUp[0]]) {
      PLPath ls = mMap.computeLevelSetCC(PointOnEdge(mesh.point(pUp[0]), point, .5));
      assert(ls.size() > 2);
      if (!ls.intersects(point.getId(), pUp[1]))
        up = false;
    }
    else {
      PLPath ls = mMap.computeLevelSetCC(PointOnEdge(mesh.point(pUp[1]), point, .5));
      assert(ls.size() > 2);
      if (!ls.intersects(point.getId(), pUp[0]))
        up = false;
    }

    if (!up) {
      if (mMap[pDown[1]] > mMap[pDown[0]]) {
        PLPath ls = mMap.computeLevelSetCC(PointOnEdge(mesh.point(pDown[1]), point, .5));
	assert(ls.size() > 2);
        if (!ls.intersects(point.getId(), pDown[0]))
	  throw Exception("getIntegralLinesFromSaddlePoint(5): cannot find the valid direction.");
      }
      else {
        PLPath ls = mMap.computeLevelSetCC(PointOnEdge(mesh.point(pDown[0]), point, .5));
	assert(ls.size() > 2);
        if (!ls.intersects(point.getId(), pDown[1]))
	  throw Exception("getIntegralLinesFromSaddlePoint(5): cannot find the valid direction.");
      }
    }

    // then compute the integral lines
    const VertexID * pp = up ? pUp : pDown;
    double d1 = length - point.distance(mesh.point(pp[0]));
    if (d1 < 0.) d1 = 0.;
    double d2 = length - point.distance(mesh.point(pp[1]));
    if (d2 < 0.) d2 = 0.;
    if (length < 0.)
      d1 = d2 = -1.;
    result.push_back(PLPath(PointOnEdge(mesh.point(pp[0]), point), up, mesh, mMap, vf, d1));
    result.back().addPointAtBeginning(point);
    result.push_back(PLPath(PointOnEdge(mesh.point(pp[1]), point), up, mesh, mMap, vf, d2));
    result.back().addPointAtBeginning(point);
  }

  return result;

}

bool PLPath::intersects(VertexID v1, VertexID v2) const {

  for(std::deque<PointOnEdge>::const_iterator p = points.begin(); p != points.end(); ++p)
    if ((*p).isVertex()) {
      const VertexID c = (*p).getClosestVertex();
      if ((v1 == c) || (v2 == c))
	return true;
    }
    else {
      if (((v1 == (*p).getFirstVertex()) && (v2 == (*p).getSecondVertex())) ||
	  ((v2 == (*p).getFirstVertex()) && (v1 == (*p).getSecondVertex())))
	return true;
    }
  return false;
}

bool PLPath::isConnected(const PLPath & path) const {
  return ((size() != 0) && (path.size() != 0) &&
	  ((path.front() == front()) ||
	   (path.front() == back()) ||
	   (path.back() == front()) ||
	   (path.back() == back())));
}

unsigned char PLPath::getConnectionType(const PLPath & path) const {
  if ((size() == 0) || (path.size() == 0))
    return 0;
  if ((path.front() == back()) || (path.back() == front()))
    return 1;
  if ((path.front() == front()) || (path.back() == back()))
    return 2;
  return 0;
}

bool PLPath::intersects(const PLPath & path) const {
  PLPath::const_iterator predp = path.begin();
  for(PLPath::const_iterator p = path.begin() + 1; p != path.end(); ++p, ++predp)
    if (intersects(*predp, *p)) {
      return true;
    }
  return false;
}



bool PLPath::intersects(const PointOnEdge & v1, const PointOnEdge & v2) const {
  if (points.size() == 1) {
    const PointOnEdge p = points.front();
    if (p.isVertex()) {
      if ((v1 == p) || (v2 == p))
	return true;
      else
	return false;
    } else {
      if ((v1 == p) || (v2 == p))
	return true;
      else if (p.inSameEdge(v1) && p.inSameEdge(v2)) {
	const double pP = p.getLocation();
	const double pV1 = v1.getLocationOnEdge(p);
	const double pV2 = v2.getLocationOnEdge(p);
	return ((pV1 >= pP) && (pP >= pV2)) || ((pV1 <= pP) && (pP <= pV2));
      }
      else
	return false;
    }
  }
  else {
    std::deque<PointOnEdge>::const_iterator predp = points.begin();
    assert(mesh != NULL);
    for(std::deque<PointOnEdge>::const_iterator p = points.begin() + 1; p != points.end(); ++p, ++predp) {
      if ((*mesh).intersects(*predp, *p, v1, v2)) {
	return true;
      }
    }
    return false;
  }

}

std::vector<Coord3D> PLPath::getCoordsCutPath(double value, bool direction, const MeshMap & mMap) const {
  std::vector<Coord3D> result;
  if (points.size() == 0)
    return result;

  // check for the valid starting point
  if ((direction && (value > mMap.getValue(points.front()))) ||
      (!direction && (value < mMap.getValue(points.front())))) {
    // starting from the beginning
    std::deque<PointOnEdge>::const_iterator predp = points.begin();
    std::deque<PointOnEdge>::const_iterator p = points.begin();
    for(; ((p != points.end()) &&
	   ((direction && (value > mMap.getValue(*p))) ||
	    (!direction && (value < mMap.getValue(*p))))); predp = p, ++p)
      result.push_back(*p);
    if (p != points.end()) {
      const PointOnEdge & p1 = *predp;
      const PointOnEdge & p2 = *p;
      assert(!(p1 == p2));
      assert(((mMap.getValue(p2) >= value) && (value >= mMap.getValue(p1))) ||
	     ((mMap.getValue(p1) >= value) && (value >= mMap.getValue(p2))));
      double v = (mMap.getValue(p2) - value) / (mMap.getValue(p2) - mMap.getValue(p1));
      assert((v >= 0) && (v <= 1));
      result.push_back(p2 + (p1 - p2) * v);
    }
  }
  else if ((direction && (value > mMap.getValue(points.back()))) ||
	   (!direction && (value < mMap.getValue(points.back())))) {
    // starting from the end
    std::deque<PointOnEdge>::const_reverse_iterator predp = points.rbegin();
    std::deque<PointOnEdge>::const_reverse_iterator p = points.rbegin();
    for(; ((p != points.rend()) &&
	   ((direction && (value > mMap.getValue(*p))) ||
	    (!direction && (value < mMap.getValue(*p))))); predp = p, ++p)
      result.push_back(*p);
    if (p != points.rend()) {
      const PointOnEdge & p1 = *predp;
      const PointOnEdge & p2 = *p;
      assert(!(p1 == p2));
      assert(((mMap.getValue(p2) >= value) && (value >= mMap.getValue(p1))) ||
	     ((mMap.getValue(p1) >= value) && (value >= mMap.getValue(p2))));
      double v = (mMap.getValue(p2) - value) / (mMap.getValue(p2) - mMap.getValue(p1));
      assert((v >= 0) && (v <= 1));
      result.push_back(p2 + (p1 - p2) * v);
    }

    std::reverse(result.begin(), result.end());
  }
  else {
    throw Exception("getCoordsCutPath(3): both begin and end sides of the path are outside of the wanted area");
  }

  return result;
}

PLPath & PLPath::cutPath(double value, bool direction, const MeshMap & mMap) {
  if (points.size() == 0)
    return *this;

  std::deque<PointOnEdge> newPath;
  // check for the valid starting point
  if ((direction && (value > mMap.getValue(points.front()))) ||
      (!direction && (value < mMap.getValue(points.front())))) {
    // starting from the beginning
    for(std::deque<PointOnEdge>::const_iterator p = points.begin(); ((p != points.end()) &&
								     ((newPath.size() == 0) ||
								      (direction && (value > mMap.getValue(newPath.back()))) ||
								      (!direction && (value < mMap.getValue(newPath.back()))))); ++p)
      newPath.push_back(*p);
  }
  else if ((direction && (value > mMap.getValue(points.back()))) ||
	   (!direction && (value < mMap.getValue(points.back())))) {
    // starting from the end
    for(std::deque<PointOnEdge>::const_reverse_iterator p = points.rbegin(); ((p != points.rend()) &&
									      ((newPath.size() == 0) ||
									       (direction && (value > mMap.getValue(newPath.back()))) ||
									       (!direction && (value < mMap.getValue(newPath.back()))))); ++p)
      newPath.push_front(*p);
  }
  else {
    throw Exception("cutPath(3): both begin and end sides of the path are outside of the wanted area");
  }
  points = newPath;

  return *this;
}

double PLPath::getIntersectionValue(const PointOnEdge & v1, const PointOnEdge & v2) const {
  if (size() < 2)
    throw Exception("getIntersectionCoord(1): there is no intersection");
  assert(mesh != NULL);
  PLPath::const_iterator previous = begin();
  for(PLPath::const_iterator p = begin() + 1; p != end(); previous = p, ++p) {
    if ((*mesh).intersects(*previous, *p, v1, v2)) {
      Line3D pp(*previous, (*p) - (*previous));
      return Line3D(v1, v2 - v1).getIntersectionLocation(pp);
    }
  }

  throw Exception("getIntersectionValue(2): there is no intersection");
}

Coord3D PLPath::getIntersectionCoord(const PLPath & path) const{
  if (size() < 2)
    throw Exception("getIntersectionCoord(1): there is no intersection");
  PLPath::const_iterator previous = path.begin();
  for(PLPath::const_iterator p = path.begin() + 1; p != path.end(); previous = p, ++p)
    if (intersects(*previous, *p))
      return getIntersectionCoord(*previous, *p);
  throw Exception("getIntersectionCoord(1): there is no intersection");
}


double PLPath::getIntersectionValue(const PLPath & path) const {
  double result = 0.;
  if (size() < 2)
    throw Exception("getIntersectionValue(1): there is no intersection (current path too small)");
  if (path.size() < 2)
    throw Exception("getIntersectionValue(1): there is no intersection (path too small)");
  assert(mesh != NULL);

  PLPath::const_iterator previous = begin();
  for(PLPath::const_iterator p = begin() + 1; p != end(); previous = p, ++p)
    if (path.intersects(*previous, *p)) {
      double d = path.getIntersectionValue(*previous, *p);
      assert(d >= -1e-4);
      assert(d <= 1. + 1e-1);
      if (d <= 0.) d = 0.;
      if (d >= 1.) d = 1.;
      return result + d * (*previous).distance(*p);
    }
    else
      result += (*previous).distance(*p);

  throw Exception("getIntersectionValue(1): no intersection found");
}

std::pair<double, double> PLPath::getExtremaValuesBetweenPoints(double p1, double p2) const {
  assert(isLoop());
  const double l = length();

  if (p1 == p2)
    throw Exception("getExtremaValuesBetweenPoints(2): same location");
  if ((p1 > l) || (p1 < 0.))
    throw Exception("getExtremaValuesBetweenPoints(2): first value out of range");
  if ((p2 > l) || (p2 < 0.))
    throw Exception("getExtremaValuesBetweenPoints(2): second value out of range");

  // order the points
  double pp1, pp2;
  if (p1 < p2) {
    pp1 = p1;
    pp2 = p2;
  }
  else {
    pp1 = p2;
    pp2 = p1;
  }

  if ((pp1 == 0.) && (pp2 == l))
    throw Exception("getExtremaValuesBetweenPoints(2): same location (loop).");

  // compute the extrema
  const double r1 = (pp2 + pp1) / 2;

  double r2 = (pp2 + l + pp1) / 2;
  while(r2 > l)
    r2 -= l;

  return std::pair<double, double>(r1, r2);
}

Coord3D PLPath::getCoordFromValue(double input) const {
  if (size() == 0)
    throw Exception("getCoordFromValue(1): empty path");
  if ((input == 0.) || (size() == 1))
    return front();

  double current = 0.;
  PLPath::const_iterator previous = begin();
  for(PLPath::const_iterator p = begin() + 1; p != end(); previous = p, ++p) {
    const double next = current + (*previous).distance(*p);
    if (next < input)
      current = next;
    else {
      const double r = (input - current) / (next - current);
      assert(r >= 0.);
      assert(r <= 1.);
      Coord3D point((*previous) + ((*p) - (*previous)) * r);
      assert((r <= 1e-6) || (1. - r <= 1e-6) || Edge3D(*previous, *p).contains(point, 1e-1));
      return point;
    }
  }

  throw Exception("getCoordFromValue(1): geodesic coordinate out of range");
}

std::string PLPath::toString(bool jump) const {
  std::ostringstream s;
  if (size() == 0)
    return s.str();

  s << front().toString();
  for(PLPath::const_iterator p = begin() + 1; p != end(); ++p) {
    s << "->";
    if (jump)
        s << std::endl;
    s << (*p).toString();
  }

  if (isLoop())
    s << "->";

  return s.str();
}

double PLPath::distanceMod(double p1, double p2, double length) {
  if (p1 > p2) {
    const double d1 = p1 - p2;
    const double d2 = p2 + length - p1;
    if (d1 < d2)
      return d1;
    else
      return d2;
  }
  else {
    const double d1 = p2 - p1;
    const double d2 = p1 + length - p2;
    if (d1 < d2)
      return d1;
    else
      return d2;
  }

}

double PLPath::middleMod(double p1, double p2, double length) {
  if (p1 > p2)
    return middleMod(p2, p1, length);
  if (fabs(p1 - p2) < fabs(p1 + length - p2))
    return (p1 + p2) / 2;
  else {
    const double m = (p1 + p2 + length) / 2;
    return m > length ? m - length : m;
  }
}

double PLPath::meanMod(const std::vector<double> & values, double length, double outside) {
  if (values.size() == 0)
    throw Exception("meanMod(2): empty values");
  if (values.size() == 1)
    return values.front();

  double sum = 0.;
  for(std::vector<double>::const_iterator p = values.begin(); p != values.end(); ++p) {
      sum += *p;
      if (*p < outside)
	sum += length;
  }
  sum /= values.size();
  return sum > length ? sum - length : sum;
}


const PointOnEdge & PLPath::getPointOnEdge(VertexID v1, VertexID v2) const {
  if (size() == 0)
    throw Exception("getClosestPointOnEdge(1): empty path");

  for(PLPath::const_iterator p = begin() + 1; p != end(); ++p) {
    if ((*p).inEdge(v1, v2))
      return *p;
  }
  throw Exception("getPointOnEdge(2): edge without point");
}

const PointOnEdge & PLPath::getClosestPointOnEdge(double value) const {
  if (size() == 0)
    throw Exception("getClosestPointOnEdge(1): empty path");
  if ((value == 0.) || (size() == 1))
    return front();

  double v = 0.;
  PLPath::const_iterator pred = begin();
  for(PLPath::const_iterator p = begin() + 1; p != end(); pred = p, ++p) {
    v += (*pred).distance(*p);
    if (value <= v) {
      if (value < v - (*pred).distance(*p) / 2)
	return *pred;
      else
	return *p;
    }
  }
  throw Exception("getClosestPointOnEdge(1): value out of range");
}

std::pair<double, PointOnEdge> PLPath::getMiddleByAngle(std::vector<Coord3D> & points_l, bool local) const {
  if (size() == 0)
    throw Exception("getMiddleByAngle(1): empty path");
  if (points_l.size() < 2)
    throw Exception("getMiddleByAngle(1): it needs more than one point.");

  // optimal ratio
  const double target = 1. / points_l.size();

  double dist = -1;
  PLPath::const_iterator result;
  for(PLPath::const_iterator pOnPath = begin(); pOnPath != end(); ++pOnPath) {
    double min = std::numeric_limits<double>::max();
    double max = -1.;
    double sum = 0.;
    std::vector<Coord3D>::const_iterator pred = points_l.begin() + (points_l.size() - 1);
    for(std::vector<Coord3D>::const_iterator p = points_l.begin(); p != points_l.end(); pred = p, ++p) {
      const double a = (*pOnPath).angle(*pred, *p);
      assert(a > 0.);
      sum += a;
      if (a < min) min = a;
      if (a > max) max = a;
    }
    assert(min >= 0.);
    assert(max > 0.);
    min /= sum;
    max /= sum;

    double v = max - target;
    if (target - min > v) v = target - min;

    if ((dist == -1) || (v < dist)) {
      dist = v;
      result = pOnPath;
    }
    else
      if (local)
	break;
  }
  assert(dist != -1);
  return std::pair<double, PointOnEdge>(dist, *result);
}

PLPath PLPath::operator+ (const PLPath & path) const {
  assert(back() == path.front());
  PLPath result(*this);
  if (path.size() >= 2)
    for(std::deque<PointOnEdge>::const_iterator p = path.points.begin() + 1; p != path.points.end(); ++p)
      result.addPointAtEnd(*p);
  return result;
}

PLPath & PLPath::adjustOnVertices(const double epsilon) {
  // first adjust on vertices
  for(std::deque<PointOnEdge>::iterator p = points.begin(); p != points.end(); ++p)
    if (((*p).getLocation() < epsilon) || ((*p).getLocation() > 1. - epsilon)) {
      (*p).adjustLocation((*mesh).point((*p).getClosestVertex()));
      if ((*p).getLocation() < epsilon)
	(*p).setLocation(0.);
      else
	(*p).setLocation(1.);
    }
  return *this;
}

PLPath & PLPath::mergeSimilarPoints(double epsilon, bool force) {
  std::deque<PointOnEdge>::iterator ppred = points.begin();
  for(std::deque<PointOnEdge>::iterator p = points.begin(); p != points.end(); ) {
    if (ppred != p) {
      if ((force || (*p).inSameEdge(*ppred)) && (*p).distance(*ppred) < epsilon) {
          p = points.erase(p);
      }
      else {
          ppred = p;
          ++p;
      }
    }
    else {
           ppred = p;
           ++p;
    }
  }
  return *this;
}

PLPath & PLPath::adjustForAdjacency() {
  std::deque<PointOnEdge>::iterator ppred = points.begin();
  for(std::deque<PointOnEdge>::iterator p = points.begin(); p != points.end(); ++p) {
    if (ppred != p) {
      if ((*p).isVertex() && (*ppred).isVertex()) {
	(*p).setFirstVertex((*p).getClosestVertex());
	(*p).setLocation(0.);
	(*p).setSecondVertex((*ppred).getClosestVertex());
      }
    }
    ppred = p;
  }
  return *this;
}


Path3D PLPath::getSubdivise(double s) const {
  Path3D result(*this);
  return result.subdivise(s);
}


bool PLPath::savePolygons(const std::vector<PLPath> & polygons, const std::string & filename, bool onlyPoints) {
  return Path3D::savePolygons(Path3D::convert(polygons), filename, onlyPoints);
}


std::vector<Path3D> PLPath::subdivise(const std::vector<PLPath> & paths, double size) {
  std::vector<Path3D> result;
  for(std::vector<PLPath>::const_iterator p = paths.begin(); p != paths.end(); ++p)
    result.push_back((*p).getSubdivise(size));
  return result;
}

std::vector<PLPath> PLPath::removeFlatDiscs(const std::vector<PLPath> & paths, Mesh & mesh, double ratio) {
  std::vector<PLPath> result;
  std::vector<bool> preserve(paths.size(), true);

  // init flags and inclusions
  mesh.setPointFlag(0);
  mesh.setTriangleFlag(0);
  std::map<TriangleID, std::vector<unsigned int> > pathsOnTriangles;
  unsigned int idPath = 0;
  for(std::vector<PLPath>::const_iterator path = paths.begin(); path != paths.end(); ++path, ++idPath) {
    PLPath::const_iterator predp = (*path).begin();
    for(PLPath::const_iterator p = (*path).begin(); p != (*path).end(); predp = p, ++p) {
      if ((*p).isVertex())
	mesh.point((*p).getClosestVertex()).setFlag(1);
      if (predp != p) {
	if (((*predp).inSameEdge(*p)) || ((*p).isVertex() && (*predp).isVertex() && mesh.point((*p).getClosestVertex()).hasNeighbour((*predp).getClosestVertex()))) {
	  std::pair<VertexID, VertexID> e = (*predp).getEdge(*p);
	  Triangle & tt1 = mesh.findTriangle(e.first, e.second);
	  tt1.setFlag(1);
	  pathsOnTriangles[tt1.getId()].push_back(idPath);
	  Triangle & tt2 = mesh.findOtherTriangle(e.first, e.second, tt1);
	  tt2.setFlag(1);
	  pathsOnTriangles[tt2.getId()].push_back(idPath);
	}
	else {
	  TriangleID t = mesh.findTriangle(*p, *predp).getId();
	  mesh.triangle(t).setFlag(1);
	  pathsOnTriangles[t].push_back(idPath);
	}
      }
    }
  }

  // compute the CC
  unsigned int idCC = 2;
  for(Mesh::const_point_iterator pt = mesh.point_begin(); pt != mesh.point_end(); ++pt) {
    if ((*pt).getFlag() == 0) {
      double area = -1.;
      double perimeter = -1.;
      unsigned int nbBd = 0;
      std::vector<unsigned int> idBd = getRegionProperties(*pt, mesh, paths, pathsOnTriangles, idCC, area, perimeter, nbBd);
      assert(idBd.size() == nbBd);
      if ((nbBd == 1) && (area / perimeter < ratio)) {
	  for(std::vector<unsigned int>::const_iterator bd = idBd.begin(); bd != idBd.end(); ++bd) {
	    assert(*bd < preserve.size());
	    preserve[*bd] = false;
	  }
	}
      ++idCC;
    }
  }

  std::vector<bool>::const_iterator pres = preserve.begin();
  for(std::vector<PLPath>::const_iterator path = paths.begin(); path != paths.end(); ++path, ++pres)
    if (*pres)
      result.push_back(*path);

  // remove the single paths
  return removeSinglePaths(result);
}

std::vector<unsigned int> PLPath::getRegionProperties(const Point3D & point, Mesh & mesh,
						      const std::vector<PLPath> & paths,
						      const std::map<TriangleID, std::vector<unsigned int> > & pathTriangles,
						      unsigned int idCC,
						      double & area, double & perimeter, unsigned int & nbBd) {
  area = 0.;
  perimeter = 0.;
  // compute area, perimeter, and paths in the border
  std::vector<VertexID> open;
  open.push_back(point.getId());
  assert(point.getFlag() == 0);
  mesh.point(point.getId()).setFlag(-idCC);
  std::set<int> bdPart; // id of the path, with sign defined by the direction

  while(!open.empty()) {
    VertexID current = open.back();
    Point3D & cPoint = mesh.point(current);
    assert(cPoint.getFlag() == -(int)idCC);
    open.pop_back();
    for(std::deque<TriangleID>::const_iterator t = cPoint.getTriangles().begin(); t != cPoint.getTriangles().end(); ++t) {
      Triangle & cTriangle = mesh.triangle(*t);
      VertexID next[2];
      next[0] = cTriangle.getOtherPoint(current);
      next[1] = cTriangle.getOtherPoint(current, next[0]);
      if (cTriangle.getFlag() == 0) {
	cTriangle.setFlag(-idCC);
	area += mesh.getArea(*t);
	for(unsigned char i = 0; i < 2; ++i) {
	  if (mesh.point(next[i]).getFlag() == 0) {
	    mesh.point(next[i]).setFlag(-idCC);
	    open.push_back(next[i]);
	  }
	}
      }
      else if ((cTriangle.getFlag() > 0) && ((unsigned int)cTriangle.getFlag() != idCC)) { // a boundary triangle not seen before in this CC
	std::vector<VertexID> openOthers;
	openOthers.push_back(next[0]);
	openOthers.push_back(next[1]);

	cTriangle.setFlag(idCC);
	std::map<TriangleID, std::vector<unsigned int> >::const_iterator idlistpt = pathTriangles.find(cTriangle.getId());
	assert(idlistpt != pathTriangles.end());
	const std::vector<unsigned int> & idlist = (*idlistpt).second;
	unsigned int bestid = 0;
	double bestarea = std::numeric_limits<double>::max();
	bool bestside = false;
	for(std::vector<unsigned int>::const_iterator id = idlist.begin();
	    id != idlist.end(); ++id) {
	  std::pair<double, bool> side = paths[*id].getAreaInSide(*t, current, openOthers);
	  if (side.first < bestarea) {
	    bestarea = side.first;
	    bestside = side.second;
	    bestid = *id;
	  }
	}
	area += bestarea;
	bdPart.insert(bestside ? (int)bestid + 1 : -((int)bestid + 1));

	assert(openOthers.size() < 2);
	for(std::vector<VertexID>::const_iterator n = openOthers.begin(); n != openOthers.end(); ++n) {
	  if (mesh.point(*n).getFlag() == 0) {
	    mesh.point(*n).setFlag(-idCC);
	    open.push_back(*n);
	  }
	}

      }
      else {
	assert((cTriangle.getFlag() > (int)0) || (cTriangle.getFlag() == -(int)idCC));
      }
    }
  }
  assert(area != 0.);

  // compute the number of borders
  std::vector<std::vector<int> > borders = computeCC(paths, bdPart);
  nbBd = borders.size();


  // get paths part of the boundary
  std::vector<unsigned int> result;
  for(std::set<int>::const_iterator b = bdPart.begin(); b != bdPart.end(); ++b) {
    result.push_back(fabs(*b) - 1); // note: paths may be inserted two time if they are present on two sides of the tile...
    perimeter += paths[fabs(*b) - 1].length();
  }
  assert(perimeter != 0.);
  return result;
}


std::vector<PLPath> PLPath::removeSinglePaths(const std::vector<PLPath> & paths) {
  std::vector<PLPath> result;
  std::vector<bool> single(paths.size(), true);

  std::vector<bool>::iterator s = single.begin();
  for(std::vector<PLPath>::const_iterator path = paths.begin(); path != paths.end(); ++path, ++s)
    if ((*path).isLoop()) {
      *s = false;
    }
    else {
      std::vector<bool>::iterator s2 = s + 1;
      for(std::vector<PLPath>::const_iterator path2 = path + 1; path2 != paths.end(); ++path2, ++s2)
	if ((*path2).isConnected(*path)) {
	  *s = false;
	  *s2 = false;
	}
    }

  std::vector<PLPath>::const_iterator path = paths.begin();
  for(std::vector<bool>::const_iterator s = single.begin(); s != single.end(); ++s, ++path)
    if (!(*s))
      result.push_back(*path);

  return result;
}


std::pair<double, bool> PLPath::getAreaInSide(TriangleID triangle,
					      VertexID corner,
					      std::vector<VertexID> & openOthers) const {
  std::pair<double, bool> result;
  const Triangle & t = (*mesh).triangle(triangle);
  assert(t.hasPoint(corner));


  // find the two points that cut the triangle
  const_iterator predp = begin();
  const_iterator p = begin() + 1;

  bool part = false;
  if ((*predp).isVertex() && (*predp).getClosestVertex() == corner)
    part = true;
  bool predisintriangle = (((*predp).isVertex() && t.hasPoint((*predp).getClosestVertex())) ||
			   ((!(*predp).isVertex()) && t.hasPoint((*predp).getFirstVertex()) && t.hasPoint((*predp).getSecondVertex())));
  bool foundedge = false;
  for(; p != points.end(); ++p, ++predp) {
    const bool pisintriangle = (((*p).isVertex() && t.hasPoint((*p).getClosestVertex())) ||
				((!(*p).isVertex()) && t.hasPoint((*p).getFirstVertex()) && t.hasPoint((*p).getSecondVertex())));
    if (predisintriangle && pisintriangle) {
      assert(predp < p);
      if (!(*predp).inSameEdge(*p)) {
	foundedge = true;
	break;
      }
      if ((*p).isVertex() && (*p).getClosestVertex() == corner)
	part = true;
    }
    predisintriangle = pisintriangle;
  }


  if (foundedge) {
    // the path go in the middle of the triangle
    VertexID c1 = t.getOtherPoint(corner);
    VertexID c2 = t.getOtherPoint(corner, c1);

    if (!(*predp).isVertex() && (*predp).hasVertex(c1) && (*predp).hasVertex(c2)) {
      assert(!(*p).isVertex());
      const VertexID p2 = (*p).getOtherVertex(corner);
      const VertexID p1 = t.getOtherPoint(corner, p2);
      const Triangle3D tr3D((*mesh).point(p2), *p, *predp);
      result.first = (*mesh).getArea(triangle) - tr3D.area();
      result.second = t.sameOrder(p1, p2);
      openOthers.erase(std::remove(openOthers.begin(), openOthers.end(), p2), openOthers.end());
      assert(openOthers.size() < 2);
    }
    else if (!(*p).isVertex() && (*p).hasVertex(c1) && (*p).hasVertex(c2)) {
      assert(!(*predp).isVertex());
      const VertexID p1 = (*predp).getOtherVertex(corner);
      const VertexID p2 = t.getOtherPoint(corner, p1);
      const Triangle3D tr3D((*mesh).point(p1), *p, *predp);
      result.first = (*mesh).getArea(triangle) - tr3D.area();
      result.second = t.sameOrder(p1, p2);
      openOthers.erase(std::remove(openOthers.begin(), openOthers.end(), p1), openOthers.end());
      assert(openOthers.size() < 2);
    }
    else {
      // in the good side
      const Triangle3D tr3D((*mesh).point(corner), *p, *predp);
      const VertexID p1 = (*predp).isVertex() ? (*predp).getClosestVertex() : (*predp).getOtherVertex(corner);
      const VertexID p2 = (*p).isVertex() ? (*p).getClosestVertex() : (*p).getOtherVertex(corner);
      result.first = tr3D.area();
      result.second = t.sameOrder(p1, p2);
      openOthers.clear();
      assert(openOthers.size() < 2);
    }
  }
  else { // the path is along an edge
    if ((*predp).isVertex())
      if ((*p).isVertex()) { // if all the edge is contained in the path
	if (find(openOthers.begin(), openOthers.end(), (*predp).getClosestVertex()) != openOthers.end())
	  openOthers.erase(std::remove(openOthers.begin(), openOthers.end(), (*predp).getClosestVertex()), openOthers.end());
	if (find(openOthers.begin(), openOthers.end(), (*p).getClosestVertex()) != openOthers.end())
	  openOthers.erase(std::remove(openOthers.begin(), openOthers.end(), (*p).getClosestVertex()), openOthers.end());
      }
      else {
	if(find(openOthers.begin(), openOthers.end(), (*predp).getClosestVertex()) != openOthers.end())
	  openOthers.erase(std::remove(openOthers.begin(), openOthers.end(), (*predp).getClosestVertex()), openOthers.end());
	else
	  openOthers.clear();
      }
    else {
      if (find(openOthers.begin(), openOthers.end(), (*p).getClosestVertex()) != openOthers.end())
	openOthers.erase(std::remove(openOthers.begin(), openOthers.end(), (*p).getClosestVertex()), openOthers.end());
      else
	openOthers.clear();
    }
    assert(openOthers.size() < 2);
    predp = points.begin();
    p = points.begin() + (points.size() - 1);
    assert((*predp).inSameEdge(*p));
    if (part) {
      // note: is it possible?
      result.first = 0.;
    }
    else {
      result.first = (*mesh).getArea(triangle);
    }

    VertexID v1, v2;
    if ((*predp).isVertex()) {
      if ((*p).isVertex()) {
	v1 = (*predp).getClosestVertex();
	v2 = (*p).getClosestVertex();
      }
      else {
	v1 = (*predp).getClosestVertex();
	v2 = (*p).getOtherVertex(v1);
      }
    }
    else if ((*p).isVertex()) {
      v2 = (*p).getClosestVertex();
      v1 = (*predp).getOtherVertex(v2);
    }
    else {
      v1 = (*predp).getVertexInOtherSide(*p);
      v2 = (*p).getVertexInOtherSide(*predp);
    }
    assert(v1 != v2);
    result.second = t.sameOrder(v1, v2);
  }

  return result;
}

std::vector<std::vector<int> >
PLPath::computeCC(const std::vector<PLPath> & paths,
		  const std::set<int> & bdpart) {

  std::vector<std::vector<int> > cc;

  for(std::set<int>::const_iterator bd = bdpart.begin(); bd != bdpart.end(); ++bd) {
    const unsigned int id = fabs(*bd) - 1;
    const PLPath & pbd = paths[id];
    // first find the list of connected cc
    std::vector<std::vector<std::vector<int> >::iterator> connected;
    for(std::vector<std::vector<int> >::iterator mcc = cc.begin(); mcc != cc.end(); ++mcc) {
      for(std::vector<int>::const_iterator p = (*mcc).begin(); p != (*mcc).end(); ++p) {
	const unsigned char c = pbd.getConnectionType(paths[fabs(*p) - 1]);
	const bool sameDir = (*bd) * (*p) > 0;
	if (((c == 1) && sameDir) ||
	    ((c == 2) && !sameDir)) {
	  connected.push_back(mcc);
	  break;
	}
      }
    }
    // then create a new CC
    std::vector<int> newCC;
    newCC.push_back(*bd);
    for(std::vector<std::vector<std::vector<int> >::iterator>::const_iterator old = connected.begin();
	old != connected.end(); ++old)
      newCC.insert(newCC.end(), (**old).begin(), (**old).end());
    // remove the old ones
    std::reverse(connected.begin(), connected.end());
    for(std::vector<std::vector<std::vector<int> >::iterator>::const_iterator old = connected.begin();
	old != connected.end(); ++old)
      cc.erase(*old);
    // finally, add the new CC to the structure
    cc.push_back(newCC);
  }

  return cc;
}


PLPath & PLPath::removeBranchParts() {

  bool remove = true;
  while (remove) {
    remove = false;
    if (size() <= 2)
      break;
    PLPath::iterator p[3] = {begin(), begin() + 1, begin() + 2};
    for(; p[2] != end(); ++(p[0]), ++(p[1]), ++(p[2])) {
      if ((*(p[0])).inSameEdge(*(p[2]))) {
	if ((*(p[0]) == (*(p[2])))) {
	  points.erase(p[0], p[2]);
	}
	else {
	  points.erase(p[1]);
	}
	remove = true;
	break;
      }
    }
  }

  return *this;
}


PLPath & PLPath::removeDoublePoints() {
  if (points.size() <= 1)
    return *this;
  bool similar = false;

  std::deque<PointOnEdge>::iterator first;
  std::deque<PointOnEdge>::iterator predp = points.begin();
  for(std::deque<PointOnEdge>::iterator p = points.begin() + 1; p != points.end();) {
    if (*p == *predp) {
      if (!similar)
	first = predp;
      similar = true;
      predp = p;
      ++p;
    }
    else {
      if (similar) {
	predp = points.erase(first, predp);
	if (predp == points.end())
	  p = points.end();
	else
	  p = predp + 1;
	similar = false;
      }
      else {
	predp = p;
	++p;
      }
    }
  }

  return *this;
}


PLPath & PLPath::fitToVertices(double epsilon) {
  assert(mesh != NULL);

  if ((epsilon <= 0.) || (epsilon >= .5))
    return *this;

  for(std::deque<PointOnEdge>::iterator p = points.begin(); p != points.end(); ++p) {
    if ((*p).getLocation() <= epsilon) {
      (*p).adjustLocation((*mesh).point((*p).getFirstVertex()), (*mesh).point((*p).getSecondVertex()), 0.);
    }
    else if ((*p).getLocation() >= 1. - epsilon) {
      (*p).adjustLocation((*mesh).point((*p).getFirstVertex()), (*mesh).point((*p).getSecondVertex()), 1.);
    }
  }

  return removeDoublePoints();
}


std::vector<PLPath> PLPath::fitToVertices(const std::vector<PLPath> & paths, double epsilon) {
  std::vector<PLPath> result;
  for(std::vector<PLPath>::const_iterator p = paths.begin(); p != paths.end(); ++p) {
    result.push_back(*p);
    result.back().fitToVertices(epsilon);
  }
  return result;
}
