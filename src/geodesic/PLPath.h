/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMap.h"

#ifndef PL_PATH
#define PL_PATH

#include <deque>
#include <vector>
#include <algorithm>

#include "PointOnEdge.h"

namespace Taglut {
  class Mesh;
  class VectorField;
  class Path3D;

  /** @class PLPath
      A class that describe piecewise linear paths on surfaces,
      using PointOnEdge
  */
  class PLPath {
  private:

    /** the set of points (on edges) that defined the path */
    std::deque<PointOnEdge> points;

    /** The mesh were the path is defined */
    const Mesh * mesh;

    /** get properties of the region containing the given point. The flag of the mesh are
	precomputed with 0 if not seen, 1 if it's a non-seen boundary objet, falg > 1 if
	it corresponds to a boundary triangle seen before (set by the idCC value),
	and flag < 1 if it corresponds to an inside triangle seen before (set as -idCC value)
	Id of paths intersecting a given triangle
	are given by the map \p pathTriangles.
	As a result, it returns a list of integer that corresponds to the id of the paths in
	the border of the region, the area of the region, the perimeter, and the number of connected components
	of boundaries.
      */
    static std::vector<unsigned int> getRegionProperties(const Point3D & point, Mesh & mesh,
							 const std::vector<PLPath> & paths,
							 const std::map<TriangleID, std::vector<unsigned int> > & pathTriangles,
							 unsigned int idCC,
							 double & area, double & perimeter, unsigned int & nbBd);

  public:
    /**
       PointOnEdge iterator
    */
    typedef std::deque<PointOnEdge>::iterator iterator;

    /**
       PointOnEdge iterator (const)
    */
    typedef std::deque<PointOnEdge>::const_iterator const_iterator;

    /**
       Default constructor
    */
    PLPath(const Mesh & mesh_l) : mesh(&mesh_l) {
    }

    /**
       Empty constructor
    */
    PLPath() {
      mesh = NULL;
    }

    /**
       Copy constructor
    */
    PLPath(const PLPath & path) : points(path.points), mesh(path.mesh) {

    }

    /**
       Copy constructor
    */
    PLPath(const PLPath & path, const Mesh & mesh_l) : points(path.points), mesh(&mesh_l) {

    }

    /**
       constructor from an integral line
     */
    PLPath(const PointOnEdge & point, bool up, const Mesh & mesh, const MeshMap & mMap, const VectorField & vf, double length = -1.);

    /**
       destructor
    */
    ~PLPath() {
    }

#ifndef SWIG
    /**
       Copy operator
    */
    inline PLPath & operator=(const PLPath & p) {
      mesh = p.mesh;
      points = p.points;
      return *this;
    }
#endif

    /** accessor */
    inline const Mesh & getMesh() const {
      if (mesh == NULL)
	throw Exception("getMesh(): no mesh defined");
      return *mesh;
    }

    /** clear the current path */
    inline PLPath & clear() {
      points.clear();
      return *this;
    }

    /**
       Add a point at the beginning of the path
    */
    PLPath & addPointAtBeginning(const PointOnEdge & point);

    /**
       Add a point at the end of the path
    */
    PLPath & addPointAtEnd(const PointOnEdge & point);

    /**
       an alias for the push_back operator
    */
    inline PLPath & push_back(const PointOnEdge & point) {
      return addPointAtEnd(point);
    }

    /**
       Remove a point at the beginning of the path
    */
    inline PLPath & removePointAtBeginning() {
      if (points.size() != 0)
	points.pop_front();
      return *this;
    }

    /**
       Remove a point at the end of the path
    */
    inline PLPath & removePointAtEnd() {
      if (points.size() != 0)
	points.pop_back();
      return *this;
    }

    /** return the last element of the list */
    inline const PointOnEdge & back() const {
      return points.back();
    }
    /** return the last element of the list */
    inline PointOnEdge & back() {
      return points.back();
    }

    /** return the first element of the list */
    inline const PointOnEdge & front() const {
      return points.front();
    }
    /** return the first element of the list */
    inline PointOnEdge & front() {
      return points.front();
    }

    /**
       iterator on the points
    */
    inline iterator begin() { return points.begin();  }

    /**
       iterator on the points
    */
    inline const_iterator begin() const { return points.begin();  }

    /**
       iterator on the points
    */
    inline iterator end() { return points.end();  }

    /**
       iterator on the points
    */
    inline const_iterator end() const { return points.end();  }

    /**
       return points
    */
    inline const std::deque<Taglut::PointOnEdge> & getPoints() const { return points; }

#ifndef SWIG
    /** random accessor */
    inline PointOnEdge & operator[](unsigned int id) {
      return points[id];
    }
    /** random accessor */
    inline const PointOnEdge & operator[](unsigned int id) const {
      return points[id];
    }

    /** return the concatenation of the current path and the given one */
    PLPath operator+ (const PLPath & path) const;
#endif

    /** return the size of the path (number of points) */
    inline unsigned int size() const {
      return points.size();
    }

    /** insert a point in the middle of the path */
    inline iterator insert(const iterator & it, const PointOnEdge & p) {
      return points.insert(it, p);
    }

    /**
       Return true if the given path is a loop, i.e. if the first point
       is the last one.
    */
    inline bool isLoop() const {
      return points.front() == points.back();
    }

    /**
       return the Euclidean length of the current
       path.
    */
    double length() const;


    /**
       Assuming that (v1, v2) is an edge, return true if
       the current path intersects the given edge.
     */
    bool intersects(VertexID v1, VertexID v2) const;

    /**
       Assuming that (v1, v2) is an edge of a PLPath (ie go throw a triangle), return true if
       the current path intersects the given edge.
     */
    bool intersects(const PointOnEdge & v1, const PointOnEdge & v2) const;

    /**
       return true if the current path intersects the given path.
     */
    bool intersects(const PLPath & path) const;

    /**
       return true if the current path is connected to the given one (at start or end point)
     */
    bool isConnected(const PLPath & path) const;

    /**
       return 0 if the current path is not connected to the given one, return 1 if they are connected
       in a coherent order, and 2 if the connection is inverted
     */
    unsigned char getConnectionType(const PLPath & path) const;

    /**
       if exists, return the value \in [0, 1] that corresponds to the location between v1 and v2 of
       the crossing between the current path and the edge (v1, v2)
       If not exists, throws an exception
     */
    double getIntersectionValue(const PointOnEdge & v1, const PointOnEdge & v2) const;

    /**
       if exists, return the value \in [0, length] that corresponds to the location between along the curent path
       of the crossing between the this path and the given one
       If not exists, throws an exception
     */
    double getIntersectionValue(const PLPath & p) const;

    /**
       if exists, return the coordinates of
       the crossing between the current path and the edge (v1, v2)
       If not exists, throws an exception
     */
    inline Coord3D getIntersectionCoord(const PointOnEdge & v1, const PointOnEdge & v2) const {
      return v1 + getIntersectionValue(v1, v2) * (v2 - v1);
    }

    /**
       if exists, return the coordinates of
       the crossing between the current path and the given path.
       If not exists, throws an exception
     */
    Coord3D getIntersectionCoord(const PLPath & path) const;

    /**
       invert the path (begin<->end)
    */
    PLPath & reversePath();

    /**
       add the given path at the end of the current one
    */
    PLPath & append(const PLPath & path);

    /**
       Compute the integral line from a point, in the direction given by the boolean, and
       stop when the path is longer than the given length (if length <= 0, the path is maximal)
     */
    static PLPath computeIntegralLine(const PointOnEdge & point, bool up, const Mesh & mesh, const MeshMap & mMap, const VectorField & vf, double length = -1);

    /**
       Compute the integral line from a point.
       stop when the path is longer than the given length (if length <= 0, the path is maximal)
     */
    static inline PLPath computeIntegralLine(const PointOnEdge & point, const Mesh & mesh, const MeshMap & mMap, const VectorField & vf, double length = -1) {
      PLPath p1 = computeIntegralLine(point, true, mesh, mMap, vf, length);
      PLPath p2 = computeIntegralLine(point, false, mesh, mMap, vf, length);
      p1.removePointAtBeginning();
      p2.reversePath();
      return p2.append(p1);
    }


    /** compute the integral lines associated to a given saddle point,
	in the direction corresponding to a single level set. The path is computed
	using the given MeshMap and VectorField on the given Mesh, and the length
	of the paths is longer than the given length. If length <= 0, the path is maximal.

    */
    static std::vector<PLPath> getIntegralLinesFromSaddlePoint(const Point3D & point,
							       const Mesh & mesh, const MeshMap & mMap, const VectorField & vf,
							       double length = -1., bool twoSides = false);

    /**
       Cut the current path preserving only the points with a value higher than the given value (for direction = true)
       or with values smaller than the given value (for direction = false), using the given MeshMap to compute the values
     */
    PLPath & cutPath(double value, bool direction, const MeshMap & mMap);

    /**
       return the coordinates associated to the current path
    */
    inline std::vector<Coord3D> getCoordsPath() const {
      std::vector<Coord3D> result;
      for(std::deque<PointOnEdge>::const_iterator p = points.begin(); p != points.end(); ++p)
	result.push_back(*p);
      return result;
    }

    /**
       return the coordinates associated to a set of paths
    */
    static inline std::vector<std::vector<Coord3D> > getCoordsPaths(const std::vector<PLPath> & paths) {
      std::vector<std::vector<Coord3D> > result;
      for(std::vector<PLPath>::const_iterator p = paths.begin(); p != paths.end(); ++p)
	result.push_back((*p).getCoordsPath());
      return result;
    }

    /**
       Return a set of points from the current path preserving only the points with a value higher than the given value (for direction = true)
       or with values smaller than the given value (for direction = false), using the given MeshMap to compute the values.
       Return only the coordinates. The crossing point is at the exact position, and there is no extra edges (like in \p cutPath)
     */
    std::vector<Coord3D> getCoordsCutPath(double value, bool direction, const MeshMap & mMap) const;


    /**
       assuming that p1 and p2 are crossing the current path, return the two points on the current path that are local maxima
       according to the distance from the crossing points.
       The returned points are defined by there geodesic coordinates.
    */
    inline std::pair<Coord3D, Coord3D> getExtremaBetweenPoints(const PLPath & p1, const PLPath & p2) const {
      return getExtremaBetweenPoints(getIntersectionValue(p1), getIntersectionValue(p2));
    }

    /**
       return the two points on the current path that are local maxima
       according to the distance from the givan points.
       The input and output points are defined by there geodesic coordinates.
    */
    std::pair<double, double> getExtremaValuesBetweenPoints(double p1, double p2) const;

    /**
       return the two points on the current path that are local maxima
       according to the distance from the givan points.
       The input points are defined by there geodesic coordinates.
    */
    inline std::pair<Coord3D, Coord3D> getExtremaBetweenPoints(double p1, double p2) const {
      std::pair<double, double> r = getExtremaValuesBetweenPoints(p1, p2);
      return std::pair<Coord3D, Coord3D>(getCoordFromValue(r.first), getCoordFromValue(r.second));
    }

    /**
       given a geodesic coordinate, return the corresponding coordinates in 3D
    */
    Coord3D getCoordFromValue(double p) const;

    /**
       generate a string that describe the current path
    */
    std::string toString(bool jump = false) const;


    /* static functions using modulo to compute distances along a level set (i.e. a loop) */
    /**
       compute a distance modulo a length
    */
    static double distanceMod(double p1, double p2, double length);

    /**
       compute the middle modulo a length
    */
    static double middleMod(double p1, double p2, double length);

    /**
       compute the mean modulo a length. The values are ordered according to the
       order along the loop. The given outside value is outside of the segment (modulo...)
    */
    static double meanMod(const std::vector<double> & values, double length, double outside);

    /** return the distance of two given points on a path that is a loop
     */
    inline double distance(double p1, double p2) const {
      assert(isLoop());
      return distanceMod(p1, p2, length());
    }

    /**
       Given a value (thar corresponds to a position onto the path), return the closest point on edge.
     */
    const PointOnEdge & getClosestPointOnEdge(double v) const;

    /**
       Given two vertices, it returns a point of the path in the corresponding edge, or throws an exception
    */
    const PointOnEdge & getPointOnEdge(VertexID v1, VertexID v2) const;

    /**
       given a set of ordered points, return the point contained in the given path
       that is "middle by angle".
    */
    std::pair<double, PointOnEdge> getMiddleByAngle(std::vector<Coord3D> & points, bool local = false) const;

    /**
       adjust the points contained in the path in order to solve the adjacency limitations
       described in PointOnEdge::inSameEdge()
     */
    PLPath & adjustForAdjacency();

    /**
       merge points that are similar (in the same edge, but with a very small distance)
     */
    PLPath & mergeSimilarPoints(double epsilon = 1e-5, bool force = false);

    /**
       adjust the points contained in the path if they are concidered as vertices, but not exactly vertices
     */
    PLPath & adjustOnVertices(const double epsilon = 1e-6);

    /** remove one of two consecutive points if they are in the same location */
    PLPath & removeDoublePoints();

    /** given a vector of cyclic PLPath, it save it in a mesh file */
    static bool savePolygons(const std::vector<PLPath> & polygons, const std::string & filename, bool onlyPoints = false);

    /**
       return a path described by coords, and corresponding to a subdivision
       of the current path, with less than size distance between two succesive
       points
     */
    Path3D getSubdivise(double size) const;

    /** return the area corresponding to the subpart of the given triangle
	in the corner's side. The returned boolean value indicates the location of
	the corner according to the ordering of the triangle (direct or not) and
	the path.
	openOthers is the list of the other points in the triangle (except corner)
	that can be reach without crossing a path. This list is updated using the
	current path. */
    std::pair<double, bool> getAreaInSide(TriangleID triangle,
					  VertexID corner,
					  std::vector<VertexID> & openOthers) const;

    /** subdivision of a set of paths */
    static std::vector<Path3D> subdivise(const std::vector<PLPath> & paths, double size);


    /** given a set of paths in a mesh, the corresponding mesh and a ratio,
	it return a new list of paths by removing all the flat discs:
	* for each region in the complement of the paths, check the topology
	* if it's a disc, we compute the corresponding area, and the perimeter
	* if area / perimeter is less than the ratio, it corresponds to a flat disc
	* we remove all the paths that corresponds to these discs
	* finally, we remove all the single paths (with no other path connected)
	\note the paths are connected one to one on their extremities
	*/
    static std::vector<PLPath> removeFlatDiscs(const std::vector<PLPath> & paths, Mesh & mesh, double ratio);

    /** remove the single paths (ie not connected to another path and not a loop) */
    static std::vector<PLPath> removeSinglePaths(const std::vector<PLPath> & paths);

    /** given a set of paths in a mesh, and a set of concerned paths (with their orientation, encoded by the sign),
	it returns the connected components (corresponding to the border of a subpart of the surface),
	with the orientation.
	\note the paths are connected one to one on their extremities */
    static std::vector<std::vector<int> > computeCC(const std::vector<PLPath> & paths,
							     const std::set<int> & bdpart);

    /** remove the subparts of the current path that corresponds to branching parts */
    PLPath & removeBranchParts();

    /** Given an epsilon between 0 and .5, it adjusts the path by moving the created points on the closest vertex
	in the edge, if the location of the new point is closest than the given epsilon (percentage of length) */
    PLPath & fitToVertices(double epsilon);

    /**
       a method that apply for each path of the given set of paths the fitToVertices method.
     */
    static std::vector<PLPath> fitToVertices(const std::vector<PLPath> & paths, double epsilon);
  };

}

#endif
