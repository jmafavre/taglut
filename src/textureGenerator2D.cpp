/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "RandomVertexSelector.h"
#include "RandomGenerator.h"
#include "MeshCPCNaive.h"

#include "Messages.h"
using namespace Taglut;

static int      size = 256;
static char*    outputfilename  = NULL;
static int      help = 0;
static double   lmin = 0.1;
static double   lmax = 1.;
static int      wMethod = 2;
static double   density = 1;
static double   w0 = 0.9;
static double   temp = 0.7;
static double   sigma = 1.;
static double   mu = 0.;
static int      display = 0;
static int      multiplierNumber = 0;

struct poptOption options[] = {
  { "size", 's', POPT_ARG_INT, &size, 0, "Size of the plane (number of vertices in x direction).", NULL},
  { "output", 'o', POPT_ARG_STRING, &outputfilename, 0, "Output image", NULL},
  { "display", 0, POPT_ARG_NONE, &display, 0, "Display the resulting image", NULL},
  { "l", 'l', POPT_ARG_DOUBLE, &lmin, 0, "Minimum scale value of the texture. Default: 0.1.", NULL},
  { "L", 'L', POPT_ARG_DOUBLE, &lmax, 0, "Maximum scale value of the texture. Default: 1.", NULL},
  { "density", 'd', POPT_ARG_DOUBLE, &density, 0, "Density (multiplicative coefficient). Default: 1.", NULL},
  { "w-method", 'w', POPT_ARG_INT, &wMethod, 0, "Distribution used to generate the multipliers' value (1: log normal, 2: log exponential, 3: log Poisson). Default: 2.", NULL},
  { "sigma", 0, POPT_ARG_DOUBLE, &sigma, 0, "sigma value (parameter of the log normal distribution). Default: 1.", NULL},
  { "mu", 0, POPT_ARG_DOUBLE, &mu, 0, "mu value (parameter of the log normal distribution). Default: 0.", NULL},
  { "temp", 0, POPT_ARG_DOUBLE, &temp, 0, "temp value (parameter of the log exponential distribution). Default: 0.7", NULL},
  { "w0", 0, POPT_ARG_DOUBLE, &w0, 0, "w0 value (parameter of the log Poisson distribution). Default: 0.9 (w0 < 1)", NULL},
  { "multiplier-number", 'n', POPT_ARG_INT, &multiplierNumber, 0, "Choose the number of multipliers (default: use the therorical value according to the surface).", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("textureGenerator2D", argc, argv, options, 0);


  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Texture generator", "Creating a procedural texture.");
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if ((wMethod <= 0) || (wMethod > 3)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the distribution of multipliers is not valid." << endl;
    return 1;
  }

  if (lmin > lmax) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the extrema of texture are not coherent (l > L)." << endl;
    return 1;
  }

  if (size < 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the size of the square should be positive." << endl;
    return 1;
  }


  if ((outputfilename == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select the display mode." << endl;
    return 1;
  }


  Mesh mesh = Mesh::createSquare(size);
  mesh.save("/tmp/square.wrl");

  // initialization of the parameters
  RandomGenerator1_z3 rDistrib(lmin, lmax);
  RandomVertexSelectorUniformSurface vSelector(mesh);
  RandomGenerator * wDistrib = NULL;
  long unsigned int nbMul;
  if (multiplierNumber != 0)
    nbMul = multiplierNumber;
  else
    nbMul = (long unsigned int)(density * mesh.getArea() * 2 / M_PI * (1 / (lmin * lmin) - 1));
  std::cout << "+---------------------------------------------------------+" << std::endl;
  std::cout << "Number of multipliers: " << nbMul << std::endl;
  std::cout << "Density: " << density << std::endl;
  long double coef;

  switch(wMethod) {
  case 1:
    std::cout << "Distribution: log normal (sigma: " << sigma << ", mu: " << mu << ")" << std::endl;
    wDistrib = new RandomGeneratorLogNormal(sigma, mu);
    coef = 1. - exp(mu + sigma / 2.);
    break;
  case 2:
    std::cout << "Distribution: log exponential (temp: " << temp << ")" << std::endl;
    wDistrib = new RandomGeneratorLogExponential(temp);
    coef = 0.0;
    break;
  case 3:
  default:
    std::cout << "Distribution: log Poisson (w0: " << w0 << ")" << std::endl;
    wDistrib = new RandomGeneratorLogPoisson(w0);
    coef = 1 - w0;
    break;
  }
  coef = pow((long double) lmin, density * coef);
  std::cout << "+---------------------------------------------------------+" << std::endl;

  // build the texture
  std::cout << "Generating texture, please wait..." << std::endl;
  MeshCPCNaive cpcNaive(mesh, rDistrib, vSelector, *wDistrib, nbMul, coef);

  // create the image corresponding to the mesh
  std::cout << "Build resulting image..." << std::endl;
  CImg<double> image(size, size);
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p) {
    image((unsigned int)round((*p).getX() * size), (unsigned int)round((*p).getY() * size)) = cpcNaive.getTexture()[(*p).getId()];
  }


  if (display != 0) {
    std::cout << "Display..." << std::endl;
    CImg<double> visu(image);
    CImgDisplay disp(visu, "Visualisation");
    while (!disp._is_closed) {
      disp.wait();
      if (disp.key() == cimg::keyESC) {
	break;
      }
      if (disp._is_resized) {
	unsigned int s = min(disp._window_width, disp._window_height);
	visu.assign(image).resize(s, s);
	disp._is_resized = false;
      }
      disp.resize(visu).display(visu);
    }
  }

  if (outputfilename != NULL) {
    std::cout << "Saving image..." << std::endl;
    image.normalize(0, 255).save(outputfilename);

  }

  delete wDistrib;



  return 0;
}
