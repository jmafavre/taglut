/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "Point3D.h"
#include "PointCloud.h"
#include "PointCloudShapeEstimator.h"

#include <unistd.h>
#include "Messages.h"
#include "CImgUse.h"
#include "Display3D.h"
#include "DisplayGL.h"

using namespace Taglut;

static char*  filename_input        = NULL;
static char*  filename_output       = NULL;
static int    help                  = 0;
static int    display               = 0;
static int    sphere_resolution     = 2;
static double surface_ratio         = .2;
static int    use_real_size         = 0;
static double maximum_size          = std::numeric_limits<double>::max();
static int    no_draw_normal        = 0;
static int    draw_only_bd_edges    = 0;
static int    draw_edges            = 0;
static double normal_size           = 5;
static double maxAngle              = 3 * M_PI / 8;
static int    normal_method         = 1;
static double pruneRatio            = 0.0;
static int    nb_points_on_edges    = 0;
static int    draw_normal_with_axes = 0;


struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input file", NULL},
  { "prune-ratio", 'p', POPT_ARG_DOUBLE, &pruneRatio, 0, "Prune ratio, between 0 and 1. Default: 0.", NULL},
  { "sphere-resolution", 'r', POPT_ARG_INT, &sphere_resolution, 0, "Sphere resolution (number of subdivision steps from the icosahedron). Default: 2.", NULL},
  { "surface-ratio", 0, POPT_ARG_DOUBLE, &surface_ratio, 0, "Surface ratio of the sphere used to compute the normal direction, when a global ratio method is used. Default: 0.2.", NULL},
  { "max-angle", 0, POPT_ARG_DOUBLE, &maxAngle, 0, "Maximum angle value for radius computation. Value between 0 and pi/2. Default: 3pi/8.", NULL},
  { "normal-method", 0, POPT_ARG_INT, &normal_method, 0, "Normal computation method. Available values: 0 (using main connected component), 1 (using a PCA approach). Default: 1.", NULL},
  { "nb-points-on-edges", 0, POPT_ARG_INT, &nb_points_on_edges, 0, "Number of points in the circle edges. Default: 0.", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output PLY file that will contains the normal estimations for every point", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display object after curvature computation", NULL},
  { "draw-normal-with-axes", 'd', POPT_ARG_NONE, &draw_normal_with_axes, 0, "Draw normal using axes.", NULL},
  { "use-real-size", 0, POPT_ARG_NONE, &use_real_size, 0, "Use the real size of the balls for the display mode.", NULL},
  { "maximum-size", 0, POPT_ARG_DOUBLE, &maximum_size, 0, "Use the given value as maximum size of the balls for the display mode.", NULL},
  { "no-draw-normal", 'n', POPT_ARG_NONE, &no_draw_normal, 0, "Do not draw the computed normals.", NULL},
  { "draw-edges", 'e', POPT_ARG_NONE, &draw_edges, 0, "Draw the edges corresponding to neighbourhoods.", NULL},
  { "draw-only-bd-edges", 0, POPT_ARG_NONE, &draw_only_bd_edges, 0, "Draw only the edges corresponding to neighbourhoods on the boundary.", NULL},
  { "normal-size", 0, POPT_ARG_DOUBLE, &normal_size, 0, "Size of the normals according to the associated spheres. Default: 5.", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("Global curvature of point cloud", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (sphere_resolution < 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: sphere resolution should be a positive integer." << endl;
    return 1;
  }


  if ((surface_ratio <= 0.) || (surface_ratio >= 1.)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the surface ratio have to be betwen 0.0 and 1.0." << endl;
    return 1;
  }

  if ((maxAngle <= 0.) || (maxAngle > M_PI / 2)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the maximum angle value have to be betwen 0.0 and pi / 2." << endl;
    return 1;
  }

  if (sphere_resolution > 6) {
    cout << "Warning: sphere resolution > 6 will produce a big sphere" << endl;
  }

  if ((normal_method < 0) || (normal_method >= 2)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: bad normal computation method." << endl;
    return 1;
  }
  enum PointCloudShapeEstimator::NormalMethod nMethod = PointCloudShapeEstimator::NMMainCC;
  if (normal_method == 1)
    nMethod = PointCloudShapeEstimator::NMPCA;


  if (help != 0) {
    HelpMessage hMsg("pointCloudShapeEstimator", "Computing for the given point cloud various shape measures.");
    hMsg << "Input: point cloud to use. Format: \"x1 y1 z1 ... xn yn zn\". Separators can be space, tabulation, new line. Lines starting with \"#\" are ignored. 3D mesh format can also be used (PLY, VRML, etc.).";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }


  try {
    cout << "Loading point cloud..." << std::endl;
    PointCloud cloud(filename_input);

    if (cloud.size() == 0) {
      std::cout << "The cloud point is empty. Abort." << std::endl;
      return 1;
    }
    cout << " " << cloud.size() << " points has been loaded" << std::endl;

    if (pruneRatio > 0) {
      cout << "Using a random prune method (ratio: " << pruneRatio << ")" << endl;
      cout << " " << (cloud.size() - cloud.prune(pruneRatio).size()) << " points has been removed" << endl;
    }


    cout << "Normal estimator initialization..." << std::endl;
    PointCloudShapeEstimator pcEstimator(cloud, sphere_resolution, true, nMethod);
    pcEstimator.setNbPointsOnShpereEdges((unsigned int) nb_points_on_edges);
    pcEstimator.radiusByEstimation(surface_ratio, maxAngle);
    pcEstimator.setAngleErrorOnBoundary(3*M_PI / 8);

    /* process mesh */
    cout << "Processing normal estimation..." << endl;
    pcEstimator.computeBoundaryFlags();
    if (draw_only_bd_edges == 1)
      pcEstimator.computeLocalBoundaryDistortion();

    if (filename_output != NULL) {
      std::cout << "Saving normals (" << filename_output << ")" << std::endl;
      cloud.save(filename_output);
    }

    if (display != 0) {
      std::cout << "Visualization" << std::endl;
      const std::string title = "3D visualization";
      DisplayPointCLoudGL d(512, 512, title, pcEstimator, true, !draw_normal_with_axes, DisplayPointCLoudGL::SCBoundary, use_real_size == 1, draw_edges == 1, draw_only_bd_edges == 1, draw_normal_with_axes && (no_draw_normal == 0), normal_size, maximum_size);
      d.disp();
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
