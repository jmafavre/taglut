/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "PointSelector.h"
#include "PointModifier.h"
#include "Display3D.h"
#include "Length.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    display = 0;
static int    cComponent = 0;
static int    help = 0;
static int    drawEdges = 0;
static char*  sMethod = NULL;
static char*  sOptions = NULL;
static char*  nMethod = NULL;
static int    openGL = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "selection-method", 's', POPT_ARG_STRING, &sMethod, 0, "Selection method (all, random, id)", NULL},
  { "selection-options", 'p', POPT_ARG_STRING, &sOptions, 0, "Options used by the selection method.", NULL},
  { "noise", 'b', POPT_ARG_STRING, &nMethod, 0, "Noise method (move-uniform(max), move-uniform(x,y,z), move-gaussian(dev), move-gaussian(x,y,z), move-barycenter(n), move-barycenter2(n).", NULL},
  { "connected-component", 'c', POPT_ARG_INT, &cComponent, 0, "Connected component in object", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "edges", 'e', POPT_ARG_NONE, &drawEdges, 0, "Draw edges", NULL},
  { "opengl", 'g', POPT_ARG_NONE, &openGL, 0, "Use opengl for display", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  LengthFactory lFactory;

  poptContext context = poptGetContext("meshNoiser", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh noiser", "Noise input mesh using random methods.");
    hMsg << "Input: mesh to noise (available format: VRML)";
    hMsg << "Output: display noised mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
  }

  PointSelector pSelector(sMethod == NULL ? "all" : sMethod, sOptions == NULL ? "" : sOptions);
  PointModifier pModifier("modify-location", nMethod == NULL ? "move-uniform(2%)" : nMethod);

  if (!pSelector.isValidSelector()) {
    std::cout << "Selection method is not valid." << std::endl;
    return -1;
  }

  if (!pModifier.isValidModifier()) {
    std::cout << "Modification method is not valid." << std::endl;
    return -1;
  }

  cout << "Building mesh..." << endl;
  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename_input, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }





  cout << "Noising mesh..." << endl;
  try {
    pSelector.setMesh(mesh);
    pModifier.setMesh(mesh);
    for(Mesh::point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      if (pSelector.isSelected(*p))
	pModifier.modify(*p);


    if (display != 0)
      Display3D::displayMesh(256, 256, "3D Visualisation", mesh, drawEdges != 0, true, openGL != 0);

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      mesh.save(filename_output, objectName);
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
