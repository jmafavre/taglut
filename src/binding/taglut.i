/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Taglut - SWIG interface
%module taglut
%include "std_string.i"
%include "std_vector.i"
%include "std_deque.i"
%include "std_pair.i"
%{
#include "src/core/IDTypes.h"
#include "src/core/Node.h"
#include "src/core/Vertex.h"
#include "src/core/Triangle.h"
#include "src/maths/Coord2D.h"
#include "src/maths/Coord3D.h"
#include "src/maths/Matrix.h"
#include "src/core/Point3D.h"
#include "src/core/Point2D.h"
#include "src/core/PLY.h"
#include "src/geodesic/PLPath.h"
#include "src/geodesic/PointOnEdge.h"
#include "src/parameterization/utils/Point2D3D.h"
#include "src/utils/Exception.h"
#include "src/utils/files/FileExceptions.h"
#include "src/utils/files/FileManipulator.h"
#include "src/topology/MeshTubularDetector.h"
#include "src/core/Mesh.h"
#include "src/core/PointCloud.h"
#include "src/topology/MeshPart.h"
#include "src/display/Display3D.h"
#include "src/algos/MeshSmoother.h"
#include "src/parameterization/Mapping2D3D.h"
#include "src/topology/MeshManipulator.h"
#include "src/topology/MeshPathes.h"
#include "src/topology/MeshCut.h"
#include "src/topology/MeshUncut.h"
#include "src/algos/WeightedEdges.h"
#include "src/core/Length.h"
#include "src/algos/MeshSegmentation.h"
#include "src/parameterization/UnfoldingMethod.h"
#include "src/parameterization/MultiscaleUnfolding.h"
#include "src/parameterization/abf/ABFMethod.h"
#include "src/parameterization/circlepacking/CPMethod.h"
#include "src/parameterization/cylinder/CylinderProjection.h"
#include "src/utils/MeshVoxelizer.h"
#include "src/utils/IndentManager.h"
#include "src/utils/files/FileTools.h"
#include "src/utils/MeshMap.h"
#include "src/geodesic/GeodesicDistance.h"
#include "src/topology/MeshPatches.h"
#include "src/algos/Graph.h"
#include "src/topology/TGraph.h"
#include "src/algos/ClusterGenerator.h"
#include "src/algos/WaveFront.h"
#include "src/algos/EdgeManipulator.h"
#include "src/algos/BorderSelector.h"
#include "src/display/DDisplay.h"
#include "src/apps/medical/Electrodes.h"
#include "src/core/NLoop.h"
#include "src/algos/SimulatedAnnealingNLoop.h"
#include "src/utils/VectorField.h"
#include "src/core/PreNLoop.h"
#include "src/core/NLoopFactory.h"
#include "src/parameterization/barycentric/FloaterMethod.h"
#include "src/topology/MeshPLCut.h"
#include "src/tiling/MTiling.h"
#include "src/statistics/RandomGenerator.h"
#include "src/core/Path3D.h"
#include "src/core/Path2D.h"
#include "src/reconstruction/MergeStep.h"
#include "src/reconstruction/SceneDescriptor.h"
#include "src/reconstruction/MergeDescriptor.h"
#include "src/reconstruction/MeshBySlices.h"
#include "CImgUse.h"
using namespace Taglut;
%}

%exception {
        try {
        $function
        }
	catch (CImgException e) {
		PyErr_SetString(PyExc_Exception, e._message);
		return NULL;
	}
        catch (Taglut::ExceptionFile e) {
                PyErr_SetString(PyExc_IOError, e.getMessage().c_str());
                return NULL;
        }
        catch (Taglut::Exception e) {
                PyErr_SetString(PyExc_Exception, e.getMessage().c_str());
                return NULL;
        }
}



namespace cimg_library {
  struct UCharCImg {
  public:
    UCharCImg();
    UCharCImg(const char *const filename);
    UCharCImg(const unsigned int dx, const unsigned int dy=1, const unsigned int dz=1, const unsigned int dv=1);
    ~UCharCImg();
    const UCharCImg& save(const char *const filename, const int number=-1) const;
    UCharCImg& fill(const unsigned char & val);
    unsigned char atXYZC(const int x, const int y, const int z, const int c) const;
  };

  struct CImgDisplay {
  public:
    CImgDisplay(const UCharCImg& img, const char *title=0,
                const unsigned int normalization_type=3,
                const bool fullscreen_flag=false, const bool closed_flag=false);
    ~CImgDisplay();
    CImgDisplay & show();
    CImgDisplay & wait();
    CImgDisplay & wait(const unsigned int milliseconds);
    bool _is_closed;
  };

  template<typename T> struct CImgList {
    CImgList();
    ~CImgList();
  };

  struct CImgException;

  %template(UCharCImgList) CImgList<unsigned char>;
  %template(DoubleCImgList) CImgList<double>;
}

%include "src/core/IDTypes.h"
%include "src/core/Node.h"
%include "src/core/Vertex.h"
%include "src/core/Triangle.h"
%include "src/maths/Coord2D.h"
%include "src/maths/Coord3D.h"
%include "src/core/Point3D.h"
%include "src/core/Point2D.h"
%include "src/parameterization/utils/Point2D3D.h"
namespace Taglut {
  %template(Coord2D) Coord2DT<double>;
  %template(Coord3D) Coord3DT<double>;
  %template(Point3D) Point3DT<double>;
  %template(Point2D) Point2DT<double>;
  %template(Point2D3D) Point2D3DT<double, double>;
  %template(Plane3D) Plane3DT<double>;
  %template(Line3D) Line3DT<double>;
}

// Instantiate templates
namespace std {
  %template(MeshDeque) deque<Taglut::Mesh>;
  %template(MeshPartDeque) deque<Taglut::MeshPart>;
  %template(Mapping2D3DDeque) deque<Taglut::Mapping2D3D>;
  %template(Point2DDeque) deque<Taglut::Point2D>;
  %template(Point3DDeque) deque<Taglut::Point3D>;
  %template(Coord3DDeque) deque<Taglut::Coord3D>;
  %template(TriangleDeque) deque<Taglut::Triangle>;
  %template(StringDeque) deque<string>;
  %template(UIntDeque) deque<unsigned int>;
  %template(UIntDequeDeque) deque<deque<unsigned int> >;
  %template(IDDeque) deque<Taglut::VertexID>;
  %template(IDDequeDeque) deque<deque<Taglut::VertexID> >;
  %template(IDVectorVector) vector<vector<Taglut::VertexID> >;
  %template(NodeDeque) deque<Taglut::Node>;
  %template(Point3DVector) vector<Taglut::Point3D>;
  %template(Coord2DVector) vector<Taglut::Coord2D>;
  %template(Coord3DVector) vector<Taglut::Coord3D>;
  %template(Coord3DVectorVector) vector<vector<Taglut::Coord3D> >;
  %template(VertexIDVector) vector<Taglut::VertexID>;
  %template(TriangleVector) vector<Taglut::Triangle>;
  %template(PointOnEdgeDeque) deque<Taglut::PointOnEdge>;
  %template(IDAndValue) pair<Taglut::VertexID, unsigned int>;
  %template(IDAndValueVector) vector<pair<Taglut::VertexID, unsigned int> >;
  %template(PreNLoopVector) vector<Taglut::PreNLoop>;
  %template(NLoopVector) vector<Taglut::NLoop>;
  %template(PLNLoopVector) vector<Taglut::PLNLoop>;
}


%include "src/core/NLoop.h"
%include "src/maths/Matrix.h"
%include "src/utils/Exception.h"
%include "src/utils/files/FileExceptions.h"
%include "src/utils/files/FileManipulator.h"
%include "src/core/Mesh.h"
%include "src/core/PointCloud.h"
%include "src/utils/MeshMap.h"
%include "src/core/PLY.h"

namespace Taglut {
  %template(MeshMap) MeshMapT<double>;
 }
%include "src/topology/MeshManipulator.h"
%include "src/topology/MeshTubularDetector.h"
%include "src/topology/MeshPart.h"
%include "src/algos/MeshSmoother.h"
%include "src/parameterization/Mapping2D3D.h"
%include "src/topology/MeshPathes.h"
%include "src/topology/MeshCut.h"
%include "src/topology/MeshUncut.h"
%include "src/parameterization/UnfoldingMethod.h"
%include "src/algos/WeightedEdges.h"
%include "src/parameterization/abf/ABFMethod.h"
%include "src/parameterization/circlepacking/CPMethod.h"
%include "src/parameterization/MultiscaleUnfolding.h"
%include "src/parameterization/cylinder/CylinderProjection.h"
%include "src/utils/MeshVoxelizer.h"
%include "src/utils/IndentManager.h"
%include "src/utils/files/FileTools.h"

namespace std {
  %template(PLPathVector) vector<Taglut::PLPath>;
  %template(PLPathVectorAndOrientation) pair<vector<Taglut::PLPath>, bool>;
  %template(PLPathVectorVector) vector<vector<Taglut::PLPath> >;
}

%include "src/geodesic/PointOnEdge.h"
%include "src/geodesic/PLPath.h"
%include "src/reconstruction/MergeStep.h"
%include "src/reconstruction/SceneDescriptor.h"
%include "src/reconstruction/MergeDescriptor.h"
%include "src/reconstruction/MeshBySlices.h"

%include "src/core/Path3D.h"
%include "src/core/Path2D.h"

namespace std {
  %template(Path3DVector) vector<Taglut::Path3D>;
  %template(Path3DVectorVector) vector<vector<Taglut::Path3D> >;
}

%include "src/geodesic/GeodesicDistance.h"
%include "src/display/Display3D.h"
%include "src/core/Length.h"
%include "src/topology/MeshPatches.h"
%include "src/algos/Graph.h"
%include "src/topology/TGraph.h"
%include "src/algos/ClusterGenerator.h"
%include "src/algos/WaveFront.h"
%include "src/algos/EdgeManipulator.h"
%include "src/algos/BorderSelector.h"
%include "src/display/DDisplay.h"
%include "src/apps/medical/Electrodes.h"
%include "src/algos/MeshSegmentation.h"
%include "src/algos/SimulatedAnnealingNLoop.h"
%include "src/utils/VectorField.h"
%include "src/core/PreNLoop.h"
%include "src/core/NLoopFactory.h"
%include "src/parameterization/barycentric/FloaterMethod.h"
%include "src/topology/MeshPLCut.h"
%include "src/tiling/MTiling.h"
%include "src/statistics/RandomGenerator.h"
%include "src/utils/CImgUse.h"




