==== Structure des sources  ====
Les fichiers sources des exécutables sont dans le répertoire courant. Ce 
répertoire contient également l'ensemble des répertoires spécifiques à chaque
partie.
Le répertoire utils contient ainsi l'ensemble des fichiers de classes proposant
des outils partiques de manipulation d'image, de fichiers, de maillages, etc.
Le répertoire unfolding contient les différentes méthodes de dépliage (circle 
packing, etc) disponibles.
Le répertoire unfolding/utils contient les outils nécessaires aux méthodes de 
dépliage (circleset, etc)

