/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CSIRO
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImg.h"
#include "PointCloud.h"
#include "Exception.h"

#include "Messages.h"
using namespace Taglut;

static char* filename1   = NULL;
static char* objectName1 = NULL;
static char* filename2   = NULL;
static char* objectName2 = NULL;
static char* outputFile  = NULL;
static int   help        = 0;
static int   hausdorff   = 0;
static int   similarity  = 0;
static float epsilon     = 0.;

struct poptOption options[] = {
  { "input1", 'i', POPT_ARG_STRING, &filename1, 0, "First input file (mesh)", NULL},
  { "name1", 0, POPT_ARG_STRING, &objectName1, 0, "Object name (first file)", NULL},
  { "input2", 'j', POPT_ARG_STRING, &filename2, 0, "Second input file (mesh)", NULL},
  { "name1", 0, POPT_ARG_STRING, &objectName2, 0, "Object name (second file)", NULL},
  { "output", 'o', POPT_ARG_STRING, &outputFile, 0, "Output file (rather than standard output)", NULL},
  { "hausdorff", 'h', POPT_ARG_NONE, &hausdorff, 0, "Compute Hausdorff distance", NULL},
  { "similarity", 's', POPT_ARG_NONE, &similarity, 0, "Percentage of points from the first mesh that are part of the second one", NULL},
  { "similarity-epsilon", 'e', POPT_ARG_FLOAT, &epsilon, 0, "Epsilon value for the similarity test (distance error).", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("compareMeshes", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Comparison of two meshes", "Several values can be generated (distances or pseudo-distances) in order to compare the two meshes.");
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if ((filename1 == NULL) || (filename2 == NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify two input files." << endl;
    return 1;
  }

  if ((hausdorff == 0) && (similarity == 0)) {
    hausdorff = 1;
    similarity = 1;
  }

  try {

    std::cout << "Loading first file";
    std::cout.flush();
    PointCloud pCloud1(filename1, objectName1 == NULL ? "" : objectName1);
    std::cout << ". Done." << std::endl;
    std::cout << "Loading second file";
    std::cout.flush();
    PointCloud pCloud2(filename2, objectName2 == NULL ? "" : objectName2);
    std::cout << ". Done." << std::endl;


    if (outputFile != NULL) {
      std::ofstream outfile(outputFile, std::ios::out);
      if (!outfile.is_open()) {
	std::cerr << "Cannot open file. Abort." << std::endl;
	return 1;
      }

      if (hausdorff)
	outfile << "Hausdorff distance: " << pCloud1.getHausdorffDistance(pCloud2) << std::endl;

      if (similarity)
	outfile << "Similarity: " << pCloud1.getSimilarity(pCloud2, epsilon) << std::endl;

      outfile.close();
    }
    else {

      if (hausdorff) {
	cout << "Hausdorff distance: ";
	cout.flush();
	cout << pCloud1.getHausdorffDistance(pCloud2) << std::endl;
      }
      if (similarity) {
	cout << "Similarity: ";
	cout.flush();
	cout << pCloud1.getSimilarity(pCloud2, epsilon) << std::endl;
      }
    }


  }
  catch (Exception e) {
    cerr << e << std::endl;
    return 1;
  }


  return 0;

}
