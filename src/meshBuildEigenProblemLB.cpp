/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "MeshList.h"
#include "MeshPart.h"

#include "Messages.h"

using namespace Taglut;

static char* filename  = NULL;
static char* objectName = NULL;
static char* lcot = NULL;
static char* b = NULL;
static int   help = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "L_cot", 'l', POPT_ARG_STRING, &lcot, 0, "Output file name for matrix L_cot", NULL},
  { "B", 'b', POPT_ARG_STRING, &b, 0, "Output file name for matrix B", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("meshInfos", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("meshBuildEigenProblemLB", "Build the matrices that described the eigen problem of the Laplace-Beltrami operator.");
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }
  if ((lcot == NULL) || (b == NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify output files." << endl;
    return 1;
  }

  try {
    /* load mesh */
    Mesh mesh(filename, objectName);

    if (mesh.getNbBoundaryPoints() != 0) {
      std::cout << "This method can only be applied in non-boundary meshes." << std::endl;
      std::cout << "Abort." << std::endl;
      return 1;
    }

    std::ofstream lcotfile(lcot);
    if (!lcotfile.is_open())
      throw ExceptionFileNotFound();

    std::ofstream bfile(b);
    if (!bfile.is_open())
      throw ExceptionFileNotFound();

    for(Mesh::const_point_iterator pi = mesh.point_begin(); pi != mesh.point_end(); ++pi) {
      for(Mesh::const_point_iterator pj = mesh.point_begin(); pj != mesh.point_end(); ++pj) {
	if ((*pi).getId() == (*pj).getId()) {
	  double vl = 0.;
	  double vb = 0.;

	  for(std::deque<VertexID>::const_iterator nb = (*pi).getNeighbours().begin(); nb != (*pi).getNeighbours().end(); ++nb) {
	    const Point3D & nbv = mesh.point(*nb);

	    const Triangle & t1 = mesh.findTriangle((*pi).getId(), *nb);
	    const Point3D & p1 = mesh.point(t1.getOtherPoint((*pi).getId(), *nb));
	    const Triangle & t2 = mesh.findOtherTriangle((*pi).getId(), *nb, t1);
	    const Point3D & p2 = mesh.point(t2.getOtherPoint((*pi).getId(), *nb));

	    vl += p1.cotanAngle(*pi, nbv) + p2.cotanAngle(*pi, nbv);
	  }

	  for(std::deque<TriangleID>::const_iterator t = (*pi).getTriangles().begin(); t != (*pi).getTriangles().end(); ++t)
	    vb += mesh.getArea(*t);

	  lcotfile << (-vl / 2) << " ";
	  bfile << vb / 6 << " ";
	}
	else if ((*pi).hasNeighbour((*pj).getId())) {
	  const Triangle & t1 = mesh.findTriangle((*pi).getId(), (*pj).getId());
	  const Point3D & p1 = mesh.point(t1.getOtherPoint((*pi).getId(), (*pj).getId()));
	  const Triangle & t2 = mesh.findOtherTriangle((*pi).getId(), (*pj).getId(), t1);
	  const Point3D & p2 = mesh.point(t2.getOtherPoint((*pi).getId(), (*pj).getId()));

	  lcotfile << ((p1.cotanAngle(*pi, *pj) + p2.cotanAngle(*pi, *pj)) / 2) << " ";
	  bfile << ((mesh.getArea(t1.getId()) + mesh.getArea(t2.getId())) / 12) << " ";
	}
	else {
	  lcotfile << "0 ";
	  bfile << "0 ";
	}
      }
      lcotfile << std::endl;
      bfile << std::endl;
    }

    lcotfile.close();
    bfile.close();

  }
  catch(Exception e) {
    std::cerr << e << std::endl;
    return 1;
  }

  return 0;
}
