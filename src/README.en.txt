==== Sources structure ====
Binary source file  are in current directory. This directory contains also
all specific directories.
"utils" directory contains thus all files containing tools to manipulate 
images, files, meshes, etc.
"unfolding" directory contains unfolding methods (circle packing, etc).
"unfolding/utils" contains tools needed by unfolding methods (circleset, etc)
