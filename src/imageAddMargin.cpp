/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <popt.h>
#include "CImgUse.h"

#include "Messages.h"
#include "Exception.h"

using namespace std;
using namespace Taglut;

static char*  filename  = NULL;
static char*  filename2  = NULL;
static int    help = 0;
static int    marginSize = 1;
static double marginValue = 0.;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input image", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename2, 0, "Output image", NULL},
  { "margin-size", 'm', POPT_ARG_INT, &marginSize, 0, "Margin size (in voxels. Default: 1)", NULL},
  { "margin-value", 0, POPT_ARG_DOUBLE, &marginValue, 0, "Margin value (default: 0)", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};




int main (int argc, const char **argv) {
  poptContext context = poptGetContext("imageBinarize", argc, argv, options, 0);

  /* check values */

  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("imageAddMargin", "Given an image, add a margin of the given size and value.");
    hMsg << "Input: 3d image (available format: dicom, analyze)";
    hMsg << "Output: 3d image (available format: dicom, analyze)";
    cout << hMsg << endl;
    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (marginSize <= 0) {
    cout << "The margin size should be positive." << endl;
    return 1;
  }


  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if (filename2 == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }


  /* load image */
  CImg<> img(filename);



  /* add margin */
  CImg<> img2(img.width() + 2 * marginSize, img.height() + 2 * marginSize, img.depth() + 2 * marginSize);
  img2.fill(marginValue);
  img2.draw_image(marginSize, marginSize, marginSize, img);
  img = img2;

  /* save image */
  try {
    img.save(filename2);
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  poptFreeContext(context);
  return 0;
}


