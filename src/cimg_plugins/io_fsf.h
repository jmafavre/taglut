/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/*-----------------------------------------------------------------------------

  File        : io_fsf.h

  Description : FSF input/output PLUG-IN

  Copyright  : Jean-Marie Favreau - http://www.isima.fr/favreau/

  This software is governed by the CeCILL  license under French law and
  abiding by the rules of distribution of free software.  You can  use,
  modify and/ or redistribute the software under the terms of the CeCILL
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info".

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability.

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or
  data to be ensured and,  more generally, to use and operate it in the
  same conditions as regards security.

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL license and that you accept its terms.


  Usage : You would probably have this kind of declaration:

  #define cimg_plugin <io_fsf.h>
  #include <CImg.h>

  ------------------------------------------------------------------------------*/


#ifndef IO_FSF
#define IO_FSF

private:
static void build_real_filename(const char *const filename1, const char *const filename2, char * result, unsigned int resultSize = 256) {
#if defined(_WIN32) || defined(__WIN32__)
  const char separator = '\\';
#else
  const char separator = '/';
#endif

  // find last path separator in filename1
  int i1 = strlen(filename1);
  while((i1 >= 0) && (filename1[i1] != separator))
    --i1;

  // find last path separator in filename2
  int i2 = strlen(filename2);
  while((i2 >= 0) && (filename2[i2] != separator))
    --i2;

  if (i2 == -1) {
    int i = 0;
    for(; i <= i1; ++i)
      result[i] = filename1[i];

    for(unsigned int f2i = 0; f2i <= strlen(filename2); ++f2i) {
      if ((unsigned int) i >= resultSize)
	break;
      result[i] = filename2[f2i];
      ++i;
    }

  }
  else
    strcpy(result, filename2);
}

public:

//! Load an image from a file in fsf format.
/**
   \param filename = name of the image file to load.
   \return A CImg<T> instance containing the pixel data defined in the image file.
**/
CImg& load_fsf(const char *const filename) {
  std::FILE *const nfile = cimg::fopen(filename,"rb");
  char line_l[256] = {0};
  char filenameImage[256] = {0};
  char filenameImageReal[256] = {0};
  unsigned int dataSize;
  std::fscanf(nfile, "FSFWIN%u", &dataSize);

  unsigned int dx = 0, dy = 1, dz = 1;
  float skip = 1;

  std::fscanf(nfile, "%u", &dx);
  std::fscanf(nfile, "%u", &dy);
  std::fscanf(nfile, "%f", &skip); // skip data
  std::fscanf(nfile, "%u", &dz);
  std::fscanf(nfile, "%f", &skip); // skip data
  std::fgets(line_l, 256, nfile); // skip data
  std::fgets(line_l, 256, nfile);
  std::fscanf(nfile, "%255[^\r\n]", filenameImage);
  std::fclose(nfile);

  build_real_filename(filename, filenameImage, filenameImageReal);

  if (dataSize == 1) {
    CImg<unsigned char> imgTmp;
    *this = imgTmp.get_load_raw(filenameImageReal, dx, dy, dz, 1, false, true);
    return *this;
  }
  else if (dataSize == 2) {
    CImg<unsigned int> imgTmp;
    *this = imgTmp.get_load_raw(filenameImageReal, dx, dy, dz, 1, false, true);
    return *this;
  }
  else if (dataSize == 4) {
    CImg<float> imgTmp;
    *this = imgTmp.get_load_raw(filenameImageReal, dx, dy, dz, 1, false, true);
    return *this;
  }
  else
    throw CImgIOException("CImg<%s>::load_fsf() : File '%s' is not a valid .FSF file.\n"
			    "Specified file format is (%u).",
			    pixel_type(), filename ? filename : "(FILE*)", dataSize);
}


// The code below allows to add the support for the specified extension.
//---------------------------------------------------------------------
#ifndef cimg_load_plugin
#define cimg_load_plugin(filename) \
  if (!cimg::strncasecmp(cimg::split_filename(filename), "fsf", 3)) return load_fsf(filename);
#endif



#endif
