/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "Display3D.h"

#include "Messages.h"
using namespace Taglut;

static char* filename  = NULL;
static int   sizex = 300;
static int   sizey = 300;
static char* objectName = NULL;
static int   simplecut = 0;
static int   simplecutplus = 0;
static int   edges = 0;
static int   help = 0;
static int   opengl = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "x", 'x', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizex, 0, "3d window size (x)", NULL},
  { "x", 'y', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizey, 0, "3d window size (y)", NULL},
  { "opengl", 'g', POPT_ARG_NONE, &opengl, 0, "display using opengl", NULL},
  { "simplecut", 's', POPT_ARG_NONE, &simplecut, 0, "display simple cut", NULL},
  { "simplecut+", '+', POPT_ARG_NONE, &simplecutplus, 0, "display simple cut (with boundary coloration)", NULL},
  { "edges", 'e', POPT_ARG_NONE, &edges, 0, "display edges", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("meshViewer", argc, argv, options, 0);


  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh Viewer", "3D interactive display of mesh.");
    hMsg << "Input: mesh to display (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((sizex <= 0) || (sizey <= 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: 3d window size must be positive." << endl;
    return 1;
  }


  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }


  try {

    /* then display */
    if (simplecut + simplecutplus != 0) {
      std::deque<MeshPart> cc = mesh.getConnectedComponents();
      CImgList<unsigned char> colors(mesh.getNbTriangles(), CImg<unsigned char>::vector(30, 30, 30));

      for(std::deque<MeshPart>::iterator it = cc.begin(); it != cc.end(); ++it) {
	if (simplecutplus != 0) {
	  //(*it).cropSimpleCut();
	  (*it).stripTriangleFlags(1);
	  (*it).switchTriangleFlags(1, 2);
	} else
	  (*it).initSimpleCutFlags();
	for(Mesh::triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t)
	  switch ((*t).getFlag()) {
	  case 0: // ouside
	    break;
	  case 1: // triangles inside MeshPart
	    colors[(*t).getId()] = CImg<unsigned char>::vector(255, 0, 0);
	    break;
	  case 2: // triangles inside simple cut
	    colors[(*t).getId()] = CImg<unsigned char>::vector(255, 180, 0);
	    break;
	  default:
	    colors[(*t).getId()] = CImg<unsigned char>::vector(255 * (*t).getFlag() / ((*it).getBettiNumber() + 1), 0, 0);
	  }

      }

      Display3D::displayCImg(sizex, sizey, "3D Visualisation", mesh.getCImgPoints(), mesh.getCImgPrimitives(), colors, edges != 0, opengl);
    }
    else
      Display3D::displayCImg(sizex, sizey, "3D Visualisation", mesh.getCImgPoints(), mesh.getCImgPrimitives(), edges != 0, opengl);
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
