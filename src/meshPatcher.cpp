/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "MeshPatches.h"
#include "Display3D.h"
#include "Length.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    display = 0;
static int    cComponent = 0;
static int    help = 0;
static int    drawEdges = 0;
static char*  length = NULL;
static double ratio = -1;
static double maxCut = -1;
static int    optimMethod = 3;
static int    maxCutNb = -1;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "connected-component", 'c', POPT_ARG_INT, &cComponent, 0, "Connected component in object", NULL},
  { "optim-method", 0, POPT_ARG_INT, &optimMethod, 0, "Optimization method (0: no trunc, 1: trunc minimum, 2: trunc maximum, 3: trunc minimum and maximum, 4: simple, 5: by strip, 6: by strip and pred cut; 1000: approximation)", NULL},
  { "maximum-cut", 'x', POPT_ARG_INT, &maxCutNb, 0, "Maximum cutting iteration.", NULL},
  { "edges", 'e', POPT_ARG_NONE, &drawEdges, 0, "Draw edges", NULL},
  { "max", 'm', POPT_ARG_DOUBLE, &maxCut, 0, "Maximum size of path to patch", NULL},
  { "length", 'l', POPT_ARG_STRING, &length, 0, "Select length method used to cut surface (or \"list\" for list them)", NULL},
  { "ratio-length", 'a', POPT_ARG_DOUBLE, &ratio, 0, "Ratio of euclidean length that can be modified by other length methods. 0 <= ratio <= 1", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  LengthFactory lFactory;
  MeshPathes::OptimEC oec = MeshPathes::OECTruncMinMax;

  poptContext context = poptGetContext("meshPatcher", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh patcher", "Patch a selected component of the input mesh with minimal method and maximal path size.");
    hMsg << "Input: mesh to patch (available format: VRML)";
    hMsg << "Output: display patch mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if ((length != NULL) && (strcmp(length, "list") == 0)) {
    cout << lFactory << endl;
    return 0;
  }

  lFactory.setLength(length);
  if (!lFactory.existsLength()) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: length unknown (try -l list)." << endl;
    return 1;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
  }

  if (optimMethod == 0)
    oec = MeshPathes::OECnoTrunc;
  else if (optimMethod == 1)
    oec = MeshPathes::OECTruncMin;
  else if (optimMethod == 2)
    oec = MeshPathes::OECTruncMax;
  else if (optimMethod == 3)
    oec = MeshPathes::OECTruncMinMax;
  else if (optimMethod == 4)
    oec = MeshPathes::OECsimple;
  else if (optimMethod == 5)
    oec = MeshPathes::OECbyStrip;
  else if (optimMethod == 6)
    oec = MeshPathes::OECbyStripAndPredCut;
  else if (optimMethod == 1000)
    oec = MeshPathes::OECApproximation;
  else
    std::cout << "Unknown optimization method, using a non-optimized algorithm." << std::endl;


  cout << "Building mesh..." << endl;
  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename_input, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }





  cout << "Checking for topological properties...";
  std::deque<MeshPart> cc = mesh.getConnectedComponents();
  if (((int)cc.size() <= cComponent) || (cComponent < 0)) {
    cerr << "Error: connected component #" << cComponent << " unknown. Surface has " << cc.size() << " connected components."<< endl;
    return 1;
  }
  cout << " OK" << endl;

  cout << "Selecting connected component..." << endl;
  Mesh meshCropped(cc[cComponent]);

  cout << "Computing patches..." << endl;
  if (ratio >= 0)
    lFactory.setRatio(ratio);
  MeshPatches mPatches(meshCropped, lFactory.getLength(meshCropped), maxCut);

  try {
    cout << " Number of points before patch: " << meshCropped.getNbPoints() << endl;
    mPatches.patchMesh(maxCutNb, oec);
    cout << " Number of points after patch: " << meshCropped.getNbPoints() << endl;

    if (display != 0)
      Display3D::displayMesh(256, 256, "3D Visualisation (crop disc)", meshCropped, drawEdges != 0, true);

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      meshCropped.save(filename_output, objectName);
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
