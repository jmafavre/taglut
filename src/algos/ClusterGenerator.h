/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CLUSTER_GENERATOR
#define CLUSTER_GENERATOR

#include <assert.h>
#include <deque>
#include <list>
#include <map>

#include "IDTypes.h"
#include "MeshManipulator.h"

namespace Taglut {
  class Vertex;

  /**
     @class ClusterDescriptor

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-05-31
     @brief Descriptor for cluster in mesh
  */
  class ClusterDescriptor : public std::vector<VertexID> {
  private:
    VertexID nbPoints;
    const Mesh * mesh;
    double sum;
    double squareSum;
    unsigned int id;
    VertexID nbPMin;
    VertexID germ;

  public:
    /** empty constructor */
    ClusterDescriptor();

    /** set mesh */
    void setMesh(const Mesh * mesh);

    /** set germ id */
    void setGerm(VertexID germ);

    /** set min number */
    void setMinPoints(VertexID nbPMin);

    /** add a point to the cluster */
    void addPoint(VertexID id, double value);

    /** merge current cluster with another */
    void merge(const ClusterDescriptor & c);

    /** return mean value */
    double getMean() const;
    /** return variance value */
    double getVariance() const;

    /** return number of points inside */
    VertexID getNbPoints() const;

    /** get germ id */
    VertexID getGerm() const;

    /** get id */
    unsigned int getId() const;

    /** set id */
    void setId(unsigned int id);

    /** return true if cluster has more than min points */
    bool hasEnougthPoints() const;

    /** return a mesure of the curvature described by the edge (pidoutside, pidinside) and the cluster */
    double getAngleJunction(VertexID pidoutside, VertexID pidinside) const;

    /** return a mesure of the curvature of the junction between the cluser and \p pid */
    double getAngleJunction(VertexID pid) const;

    /** return true if the current cluster contains the given point. */
    bool hasPoint(VertexID idP) const;

  };


  /**
     @class ClusterMembership

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-06-05
     @brief Tool for cluster membership detection. Abstract class.
  */
  class ClusterMembership {

  public:

    /** virtual destructor */
    virtual ~ClusterMembership();

    /** clone tool*/
    virtual ClusterMembership * clone() const = 0;

    /** return true if newValue is inside the given cluster */
    virtual bool isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd);
  };


  /**
     @class ClusterMembershipRatioSimple

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-06-05
     @brief Tool for cluster membership detection. Simple implementation
  */
  class ClusterMembershipRatioSimple : public ClusterMembership {

  public:
    /** destructor */
    ~ClusterMembershipRatioSimple();

    /** clone tool */
    ClusterMembership * clone() const;


    /** return true if newValue is inside the given cluster */
    bool isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd);
  };

  /**
     @class ClusterMembershipRatioAllNb

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-06-05
     @brief Tool for cluster membership detection. All neighbours of germ are added.
  */
  class ClusterMembershipRatioAllNb : public ClusterMembership {

  public:
    /** destructor */
    ~ClusterMembershipRatioAllNb();

    /** clone tool */
    ClusterMembership * clone() const;


    /** return true if newValue is inside the given cluster */
    bool isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd);
  };


  /**
     @class ClusterMembershipCurvature

     @author Jean-Marie Favreau (LIMOS/Univ. Blaise Pascal)
     @date 2008-09-22
     @brief Tool for cluster membership detection. The curvature is the only information
     taken into account.
  */
  class ClusterMembershipCurvature : public ClusterMembership {
    /** maximum curvature allowed during the growing of the cluster */
    double maxCurv;
  public:
    /** constructor */
    ClusterMembershipCurvature(double maxCurv = 0.2);

    /** destructor */
    ~ClusterMembershipCurvature();

    /** clone tool */
    ClusterMembership * clone() const;


    /** return true if newValue is inside the given cluster */
    bool isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd);
  };


  /**
     @class ClusterMembershipCurvatureOrRatio

     @author Jean-Marie Favreau (LIMOS/Univ. Blaise Pascal)
     @date 2008-09-22
     @brief Tool for cluster membership detection. The curvature or the ratio are allowing merge.
  */
  class ClusterMembershipCurvatureOrRatio : public ClusterMembership {
    /** maximum curvature allowed during the growing of the cluster */
    double maxCurv;
  public:
    /** constructor */
    ClusterMembershipCurvatureOrRatio(double maxCurv = 0.2);

    /** destructor */
    ~ClusterMembershipCurvatureOrRatio();

    /** clone tool */
    ClusterMembership * clone() const;


    /** return true if newValue is inside the given cluster */
    bool isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd);
  };

  /**
     @class ClusterMembershipCurvatureAndRatio

     @author Jean-Marie Favreau (LIMOS/Univ. Blaise Pascal)
     @date 2008-09-22
     @brief Tool for cluster membership detection. The ratio and the curvature are needed to allow the merge.
  */
  class ClusterMembershipCurvatureAndRatio : public ClusterMembership {
    /** maximum curvature allowed during the growing of the cluster */
    double maxCurv;
  public:
    /** constructor */
    ClusterMembershipCurvatureAndRatio(double maxCurv = 0.2);

    /** destructor */
    ~ClusterMembershipCurvatureAndRatio();

    /** clone tool */
    ClusterMembership * clone() const;


    /** return true if newValue is inside the given cluster */
    bool isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd);
  };



  /**
     @class ClusterGenerator

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-05-31
     @brief Generator of cluster in mesh
  */
  class ClusterGenerator : public MeshManipulator {
  private:
    double * ratios;
    bool done;
    std::map<unsigned int, ClusterDescriptor> cDesc;
    ClusterMembership * cMembership;
    std::vector<VertexID> centers;

    void removeClusters(const std::list<unsigned int> & clusters);

  public:
    /**
       Available clustering methods.
    */
    enum ClusteringMethod { CMratiosimple, CMratiostartall, CMratioLog, CMcurvature,
			    CMcurvatureOrRatioLog, CMcurvatureOrRatio,
			    CMcurvatureAndRatioLog, CMcurvatureAndRatio };

    /** default constructor */
    ClusterGenerator();

    /** copy constructor */
    ClusterGenerator(const ClusterGenerator & cg);

    /** real constructor */
    ClusterGenerator(Mesh * mesh, double * ratios);

    /** constructor by copy with a new mesh */
    ClusterGenerator(Mesh * mesh, double * ratios, const ClusterMembership & cm);

    /** destructor */
    ~ClusterGenerator();

#ifndef SWIG
    /** copy operator */
    ClusterGenerator & operator=(const ClusterGenerator & cg);
#endif

    /** set data. equivalent to copy */
    void setData(Mesh * mesh, double * ratios);

    /** build clusters given germs */
    void clustering(const std::vector<VertexID> & centers, const enum ClusteringMethod & cMethod = CMratiostartall, double maxCurv = 0.2);

    /** removing smaller clusters */
    void removeSmallerClusters();

    /** remove clusters with small ratios */
    void removeSmallRatioClustersByRatio(double ratio = 10);

    /** remove clusters with hight ratios */
    void removeHightRatioClustersByRatio(double ratio = 10);

    /** remove clusters keeping \p nb clusters with higher ratio */
    void removeSmallRatioClusters(unsigned int nb);


    /** get germs of clusters computed before */
    std::vector<VertexID> getGerms() const;

    /** get clusters */
    std::vector<std::vector<VertexID> > getClusters() const;

    /** get number of clusters */
    unsigned int getNbClusters() const;


    /** get number of clusters */
    unsigned int getVertexFlag(unsigned int idV);

    /** Modify the flags of points on the mesh according to the cluster segmentation (0: not in a cluster, i: in the i-st cluster). */
    void setFlagsOnMesh();

    /**
       clear data
    */
    void clear();

    /**
       get infos about clusters (string format for output)
    */
    std::string getInfos() const;
  };
}

#endif
