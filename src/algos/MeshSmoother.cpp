/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshSmoother.h"
#include "Mesh.h"

using namespace Taglut;

void MeshSmoother::smoothLaplacian(Mesh & mesh, double lambda) {
  Mesh meshCopy(mesh);

  Mesh::point_iterator pNew = mesh.point_begin();
  for(Mesh::const_point_iterator p = meshCopy.point_begin(); p != meshCopy.point_end(); ++p, ++pNew)
    if (!(*p).getIsBoundary()) {
      Coord3D U0p(0.0, 0.0, 0.0);
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	U0p += meshCopy.point(*nb);
      U0p /= (*p).getNbNeighbours();
      U0p -= *p;

      *pNew += lambda * U0p;
    }

}
