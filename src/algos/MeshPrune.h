/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_PRUNE
#define MESH_PRUNE

#include <list>

#include "MeshManipulator.h"
#include "Triangle.h"

namespace Taglut {
  /**
     @class MeshPrune
     @author Jean-Marie Favreau
     Mesh manipulator used to prune mesh
  */
  class MeshPrune : public MeshManipulator {
  private:

    static const int NOT_SEEN;
    static const int IN_PROGRESS;

    static const int UNDER_FINALMESH;
    static const int IN_FINALMESH;
    static const int IN_FINALMESH_B;
    static const int IN_MIDDLE;
    static const double EPSILON;

    /** depth used by prune */
    unsigned int depth;

    /** list of triangles used to compute the new mesh */
    std::deque<MultiTriangle> resultTriangles;


    /** list of points used to compute the new mesh */
    std::deque<Point3D> resultPoints;

    /** number of seen triangles */
    TriangleID nbSeenTriangles;

    /** matching between original id and new id */
    VertexID * newId;

    /** number of points in the original mesh */
    VertexID nbOriginalPoints;

    /** length to the mesh allready seen */
    double * localLength;

    /** true if the ids of points in triangles are corresponding to the original mesh */
    bool originalIds;

    /**
       Set of open triangles
    */
    std::deque<MultiTriangle> openMultiTriangleSet;

    /**
       Return true if the given edge is part of an open MultiTriangle
    */
    bool isInOpenTriangle(const MultiEdge & mEdge) const;

    /**
       Add the given multitriangle to the structure
    */
    MultiTriangle & addMultiTriangle(const MultiTriangle & mt);

    /**
       prune mesh with local depth
    */
    void pruneMesh(unsigned int localDepth);

    /**
       Build new mesh
    */
    void buildMesh();

    /**
       Struct used for edge sorting
    */
    struct multiTriangleSort;

    /**
       Compute neighbours (2nd and 1st levels) of a given point
       @param point point from where computing the neighbour list
       @return list of points
    */
    std::deque<VertexID> computeOpenNeighbours(Point3D & point);

    /**
       Compute optimal pathes from each extremity of a given edge to the other point
    */
    std::list<std::pair<MultiEdge, MultiEdge> > computeMatchingTriangles(const MultiEdge & mEdge, VertexID otherPoint);

    /**
       Build initial edge
    */
    MultiEdge buildInitialEdge();

    /**
       return true if the given triangle is a star and if each small edge is part of a seen triangle (or a boundary edge)
    */
    bool isGoodStar(const MultiTriangle & mt) const;

    /**
       Given an edge, build list of available triangles from this edge
    */
    std::list<MultiTriangle> computeOpenTriangles(const MultiEdge & mEdge);


    /**
       build the first multitriangle triangle
    */
    MultiTriangle buildFirstTriangle();

    /**
       Given a multitrangle, compute the initial small triangle, then fill the multitriangle using flag.
    */
    inline bool fillTriangleUsingFlags(MultiTriangle & triangle, TriangleID idFTriangle) {
      return fillTriangleUsingFlags(triangle, findFirstTriangleInside(triangle), idFTriangle);
    }

    /**
       Given a multitrangle, compute the initial small triangle, then fill the multitriangle using flag.
    */
    inline bool fillTriangleUsingFlags(MultiTriangle & triangle) {
      return fillTriangleUsingFlags(triangle, findFirstTriangleInside(triangle), (*mesh).getNbTriangles());
    }

    /**
       Given a multitrangle and an initial small triangle, fill the multitriangle using flag.
    */
    inline bool fillTriangleUsingFlags(MultiTriangle & triangle, const Triangle & firstTriangle) {
      return fillTriangleUsingFlags(triangle, firstTriangle, (*mesh).getNbTriangles());
    }

    /**
       Given a multitrangle and an initial small triangle, fill the multitriangle using flag. A forbidden small triangle
       is given by \p idFTriangle
    */
    bool fillTriangleUsingFlags(MultiTriangle & triangle, const Triangle & firstTriangle, TriangleID idFTriangle);


    /**
       Given an edge, build a new triangle using edges and flags. Update flags.
       If idFTriangle less than number of triangles in the mesh, resulting MultiTriangle cannot contain
       this triangle.
    */
    inline MultiTriangle buildTriangleUsingFlags(const MultiEdge & mEdge) {
      return buildTriangleUsingFlags(mEdge, (*mesh).getNbTriangles());
    }

    /**
       Given an edge, and a forbidden triangle, build a new triangle using edges and flags. Update flags.
       If idFTriangle less than number of triangles in the mesh, resulting MultiTriangle cannot contain
       this triangle.
    */
    MultiTriangle buildTriangleUsingFlags(const MultiEdge & mEdge, TriangleID idFTriangle);

    /**
       Given 3 edges and a forbidden, check if it's a good triangle using edges and flags. Update flags
       If idFTriangle less than number of triangles in the mesh, the MultiTriangle should contain
       this triangle.
    */
    bool checkTriangleUsingFlags(MultiTriangle & triangle, TriangleID idFTriangle);

    /**
       Return true if the given mutliple edge is open
    */
    bool isOpenEdge(const MultiEdge & e) const;

    /**
       Return number of open edges
    */
    VertexID computeNbOpenEdge() const;

    /**
       return true if the given multiedge is part of the mesh's boundary
    */
    bool isBoundaryEdge(const MultiEdge & e) const;

    /**
       Return true if the given point is a boundary one
    */
    bool isBoundaryVertex(const Point3D & point) const;

    /**
       Return true if the given small edge is allowed in a new triangle
    */
    bool isAllowedEdgeInNewTriangle(VertexID idP1, VertexID idP2) const;

    /**
       Return true if the given multiedge edge is allowed in a new triangle
    */
    bool isAllowedMultiEdgeInNewTriangle(const MultiEdge & mEdge) const;

    /**
       Find a triangle inside the large triangle defined by edges, using triangle order
    */
    Triangle & findFirstTriangleInside(const MultiTriangle & mTriangle);

    /**
       Find a triangle inside the triangle defined by the two given edges.
       Assums that mEdge1.getLastId() == mEdge2.getFirstId()
    */
    Triangle & findFirstTriangleInside(const MultiEdge & mEdge1, const MultiEdge & mEdge2);

    /**
       Find a triangle inside the polygon defined by list of edges.
       Assums that two consecutive edges are not virtual
    */
    Triangle & findFirstTriangleInside(const std::deque<MultiEdge> & mEdges);

    /**
       Update flags of the corner points of the given triangle (using IN_FINALMESH_B and IN_FINALMESH)
    */
    void addMultiTriangleAndPoints(const MultiTriangle & triangle);

    /**
       given a triangle, compute and add forbidden triangles (if exists)
    */
    void addForbiddenTriangle(MultiTriangle & triangle) const;

    /**
       given a star triangle, compute and add forbidden triangles (if exists)
    */
    void addForbiddenTriangleStar(MultiTriangle & triangle) const;

    /**
       get multiedge from p1 to p2 if exists.
    */
    MultiEdge getMEdge(VertexID p1, VertexID p2);

    /**
       Return the list of final edges that contains the given edge
    */
    /*std::deque<MultiEdge> getListOfFinalEdges(VertexID idP1, VertexID idP2) const;*/

    /**
       return true if the given edge is part of a final edge
    */
    bool isPartOfFinalEdge(VertexID idP1, VertexID idP2) const;

    /**
       return true if the given edge is part of a final edge
    */
    bool isPartOfFinalEdge(const MultiEdge & mEdge) const;

    /**
       return true if the given edge is part of a final and open edge
    */
    bool isPartOfFinalAndOpenEdge(VertexID idP1, VertexID idP2) const;

    /**
       Return true if this triangle will be part of new triangles
    */
    bool hasAvailableNewTriangle(const MultiEdge & mEdge);

    /**
       Return a list of multiedges from the given mutliedge that is a quadrilateral closure on border.
       If mEdge is not part of a quadrilateral closure on border, an empty list is return.
       mEdge becomes result[0] or result[1], and result[3] is the virtual multiedge on the boundary
    */
    std::deque<MultiEdge> getQuadrilateralBorderClosure(const MultiEdge & mEdge);

    /**
       return true if the given triangle is unknown
    */
    bool isUnknownTriangle(const MultiTriangle & t) const;

    /**
       Return true if the given mutliedge is part of a quadrilateral closure
    */
    bool isQuadrilateralClosure(const MultiEdge & mEdge) const;

    /**
       Return true if the given mutliedge is part of a quadrilateral closure on border
    */
    bool isQuadrilateralBorderClosure(const MultiEdge & mEdge);

    /**
       Return true if the given mutliedge is part of a border closure
    */
    bool isBorderClosure(const MultiEdge & mEdge);

    /**
       Assums that e is part of a quadrilateral closure. Two virtual
       multitriangles are added, and flags points are updated to close the
       quadrilateral.
    */
    void addVirtualTrianglesUpdatePointFlags(const MultiEdge & e);

    /**
       Assums that e is part of a border closure. A virtual
       multitriangle is added, and flags points are updated to close the
       quadrilateral.
    */
    void addVirtualTriangleOnBorderUpdatePointFlags(const MultiEdge & mEdge);

    /**
       Assums that e is part of a border closure. Two virtual
       multitriangles is added, and flags points are updated to close the
       quadrilateral.
    */
    void addVirtualTrianglesOnBorderUpdatePointFlags(const MultiEdge & mEdge);

    /**
       Given a point and a multiedge from this point, find the other
       multiedge from this point
    */
    const MultiEdge & findNeighbourBoundaryMEdge(VertexID point, const MultiEdge & mEdge) const;


    /**
       return true if there is triangle inside quadrilater
    */
    bool hasTriangleInside(const MultiEdge & mEdge1,
			   const MultiEdge & mEdge2,
			   const MultiEdge & mEdge3,
			   const MultiEdge & mEdge4) const;


    /**
       Set the id of the points in the result triangles according to the original mesh
    */
    void setOriginalIds();

    /**
       add a small triangle in the nearest MultiTriangle, using isobarycenter
    */
    void addTriangleInMultiTriangle(const Triangle & t,
				    MultiTriangle & mt1,
				    MultiTriangle & mt2);

  public:
    /**
       Default constructor
    */
    MeshPrune(Mesh & mesh, unsigned int depth = 1);

    /**
       Destructor
    */
    ~MeshPrune();


    /**
       Prune mesh using initially given depth
    */
    void pruneMesh();

    /**
       Get multitriangles with points' id corresponding to the original mesh
    */
    std::deque<MultiTriangle> & getMultiTriangles();

    /**
       Return the new id of a given point in the original mesh
    */
    inline VertexID getNewId(VertexID id) const {
      assert(id < nbOriginalPoints);
      return newId[id];
    }

    /**
       Return number of points in the original mesh
    */
    inline VertexID getNbOrignalPoints() const {
      return nbOriginalPoints;
    }
  };
}

#endif
