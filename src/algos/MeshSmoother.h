/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_SMOOTHER
#define MESH_SMOOTHER

#include "Mesh.h"

namespace Taglut {
  class MeshSmoother {
  public:

    /**
       Given a mesh and a lambda term in [0, 1], compute a
       smooth algorithm on the mesh. \p lambda is a parameter
       that describe the influence of the smoothing process
    */
    static void smoothLaplacian(Mesh & mesh, double lambda);

  };
}

#endif
