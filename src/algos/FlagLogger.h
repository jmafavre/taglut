/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef FLAG_LOGGER_H
#define FLAG_LOGGER_H

#include <vector>

namespace Taglut {
  class Mesh;


  /**
     @class FlagLogger
     @author Jean-Marie Favreau (CNRS / Univ. Blaise Pascal)
     @brief Flag logger
  */
  class FlagLogger {
  private:
    /**
       Triangles' flags
    */
    std::vector<int> tflags;
    /**
       Points' flags
    */
    std::vector<int> pflags;

  public:

    /**
       Default constructor
    */
    FlagLogger();

    /**
       Log flags of the given mesh
    */
    void logFlags(const Mesh & mesh);

    /**
       Restore the logged flags on the given mesh
    */
    void restoreFlags(Mesh & mesh) const;
  };
}

#endif
