/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>
#include "WaveFront.h"
#include "Triangle.h"
#include "Mesh.h"
#include "MeshManipulator.h"

using namespace Taglut;

WaveFront::WaveFront(Mesh & mesh_l) : mesh(mesh_l) {

}

WaveFront::WaveFront(const WaveFront & wf) : mesh(wf.mesh), front(wf.front) {
}

WaveFront::~WaveFront() {
}


std::vector<VertexID>::const_iterator WaveFront::nextInFrontConst(const std::vector<VertexID>::const_iterator & it, int n) const {
  std::vector<VertexID>::const_iterator result = it;
  if ((it == front.end()) || (n == 0))
    return result;

  if (n > 0)
    for(unsigned int i = 0; i < (unsigned int) n; ++i) {
      ++result;
      if (result == front.end())
	result = front.begin();
    }
  else {
    unsigned int nn = -n;
    for(unsigned int i = 0; i < nn; ++i) {
      if (result != front.begin())
	result -= 1;
      else
	result = front.begin() + (front.size() - 1);
    }
  }
  return result;
}

std::vector<VertexID>::iterator WaveFront::nextInFront(const std::vector<VertexID>::iterator & it, int n) {
  std::vector<VertexID>::iterator result = it;
  if ((it == front.end()) || (n == 0))
    return result;

  if (n > 0)
    for(unsigned int i = 0; i < (unsigned int) n; ++i) {
      ++result;
      if (result == front.end())
	result = front.begin();
    }
  else {
    unsigned int nn = -n;
    for(unsigned int i = 0; i < nn; ++i) {
      if (result != front.begin())
	result -= 1;
      else
	result = front.begin() + (front.size() - 1);
    }
  }
  return result;
}

// TODO: optimization using the flag of p1 and p2 to stop the walk when the front does not contains another instance of a search point.
bool WaveFront::isFrontEdge(const VertexID & p1, const VertexID & p2) const {
  if ((mesh.getNbPoints() <= p1) || (mesh.getNbPoints() <= p2) ||
      (!mesh.point(p1).hasNeighbour(p2)) ||
      (mesh.point(p1).getFlag() <= 0) || (mesh.point(p2).getFlag() <= 0))
    return false;

  for(std::vector<VertexID>::const_iterator p = front.begin(); p != front.end(); ++p) {
    if ((*p == p1) || (*p == p2)) {
      std::vector<VertexID>::const_iterator pp = nextInFrontConst(p);
      if ((((*pp == p1) || (*pp == p2)) && (*p != *pp)))
	return true;
      else if (p == front.begin()) {
	std::vector<VertexID>::const_iterator pp2 = predInFrontConst(p);
	if ((((*pp2 == p1) || (*pp2 == p2)) && (*p != *pp2)))
	  return true;
      }
    }

  }

  return false;
}

bool WaveFront::hasTriangleInside(const VertexID & p1, const VertexID & p2) const {
  const Triangle & t1 = mesh.findTriangle(p1, p2);
  if (t1.getFlag() != 0)
    return true;
  try {
    const Triangle & t2 = mesh.findOtherTriangle(p1, p2, t1);
    if (t2.getFlag() != 0)
      return true;

  }
  catch (Exception e) {}

  return false;
}


/*
  flags for triangles:
  - 0: outside
  - 1: inside
  - 2: in the open list
 */
const std::vector<VertexID> & WaveFront::growSurfaceTriangleFull(VertexID point, TriangleID nbTriangles) {
  std::deque<std::pair<VertexID, VertexID> > open;
  Triangle & t1 = mesh.triangle(mesh.point(point).getTriangles().front());
  mesh.setTriangleFlag(0);
  mesh.setPointFlag(0);

  // add the first triangle
  front.push_back(point);
  mesh.point(point).setFlag(1);
  front.push_back(t1.getNextVertexId(point));
  mesh.point(front.back()).setFlag(1);
  front.push_back(t1.getNextVertexId(front.back()));
  mesh.point(front.back()).setFlag(1);
  t1.setFlag(1);

  for(unsigned int i = 0; i < 3; ++i) {
    try {
      Triangle & t2 = mesh.findOtherTriangle(t1.getVertexId(i), t1.getVertexId((i + 1) % 3), t1);
      if (t2.getFlag() == 0) {
	open.push_back(std::pair<VertexID, VertexID>(t1.getVertexId(i), t1.getVertexId((i + 1) % 3)));
	t2.setFlag(2);
      }
    }
    catch (Exception e) { }
  }

  TriangleID nbAdded = 0;
  while(!open.empty()) {
    std::pair<VertexID, VertexID> e = open.front();
    open.pop_front();
    TriangleID t;
    Triangle & t1_2 = mesh.findTriangle(e.first, e.second);
    if (t1_2.getFlag() == 2)
      t = t1_2.getId();
    else {
      Triangle & t2 = mesh.findOtherTriangle(e.first, e.second, t1_2);
      assert(t2.getFlag() == 2);
      t = t2.getId();
    }
    assert(mesh.triangle(t).getFlag() == 2);
    addTriangleOnFront(e.first, e.second, t);
    ++nbAdded;
    if ((nbTriangles !=0) && (nbAdded == nbTriangles))
      return front;
    for(unsigned int i = 0; i < 3; ++i) {
      try {
	Triangle & tt = mesh.findOtherTriangle(mesh.triangle(t).getVertexId(i), mesh.triangle(t).getVertexId((i + 1) % 3), mesh.triangle(t));
	if (tt.getFlag() == 0) {
	  open.push_back(std::pair<VertexID, VertexID>(mesh.triangle(t).getVertexId(i), mesh.triangle(t).getVertexId((i + 1) % 3)));
	  tt.setFlag(2);
	}
      }
      catch (Exception e_l) { }
    }
  }
  return front;
}

void WaveFront::addTriangleOnFront(VertexID p1, VertexID p2, TriangleID triangle) {
  front.reserve(front.size() + 1);
  std::vector<VertexID>::iterator location = getNextEdge(front.begin(), p1, p2);
  VertexID point = mesh.triangle(triangle).getOtherPoint(p1, p2);
  std::vector<VertexID>::iterator next = location + 1;

  // first insert the point
  assert((next >= front.begin()) && (next <= front.end()));
  front.insert(next, point);
  setFlagInsert(point);
  mesh.triangle(triangle).setFlag(1);

  // check for points to be removed
  std::vector<VertexID>::iterator first = front.end();
  std::vector<VertexID>::iterator last = front.end();
  if (*predInFront(location) == point) {
    first = predInFront(location);
    last = nextInFront(location);
  }
  if (*nextInFront(location, 3) == point) {
    if (first == front.end())
      first = nextInFront(location);
    last = nextInFront(location, 3);
  }

  // remove points on front
  if (first != front.end()) {
    do {
      first = predInFront(first);
      last = nextInFront(last);
    }
    while(*first == *last);

    first = nextInFront(first, 2);
    if (first < last) {
      assert((front.begin() <= first) && (first < front.end()));
      front.erase(first, last);
    }
    else {
      assert((front.begin() <= first) && (first < front.end()));
      front.erase(first, front.end());
      assert((front.begin() <= last) && (last < front.end()));
      front.erase(front.begin(), last);
    }

  }


}

const std::vector<VertexID> & WaveFront::growSurfaceFull(VertexID point) {
  MeshManipulator mManip(mesh);
  // init flags
  mesh.setPointFlag(0);
  mesh.setTriangleFlag(0);

  // compute length from the starting point
  mManip.computeDijkstra(point, 0);

  // build an ordered list of points
  lengthPathBP lsort;
  lsort.length = mManip.length;
  lsort.mesh = &mesh;
  std::vector<VertexID> sList;
  for(VertexID i = 0; i < mesh.getNbPoints(); ++i)
    sList.push_back(i);
  sort(sList.begin(), sList.end(), lsort);

  return growSurfaceFull(sList);
}

const std::vector<VertexID> & WaveFront::growSurfaceFull(const std::vector<VertexID> & points) {
  if (points.size() < 4)
    return front;

  std::vector<VertexID>::const_iterator p = points.begin();
  std::vector<VertexID>::const_iterator ppred = points.begin();
  VertexID third = mesh.getNbPoints();

  if (front.size() == 0) { // first call: initialization
    mesh.setTriangleFlag(0);
    mesh.setPointFlag(0);


    front.push_back(*p);
    mesh.point(*p).setFlag(1);
    ppred = p;
    ++p;
    front.push_back(*p);
    mesh.point(*p).setFlag(1);

    // adding the third point
    Triangle & t1 = mesh.findTriangle(points.front(), *p);
    third = t1.getOtherPoint(points.front(), *p);
    if (t1.sameOrder(*p, third)) {
      assert((front.begin() <= nextInFront(front.begin())) && (nextInFront(front.begin()) < front.end()));
      front.insert(nextInFront(front.begin()), third);
    }
    else
      front.push_back(third);

    mesh.point(third).setFlag(1);
    t1.setFlag(1);

    ppred = p;
    ++p;
  }
  else { // the growing surface has been started at a previous call, should find the first non seen point
    while(mesh.point(*p).getFlag() != 0) {
      ppred = p;
      ++p;
      if (p == points.end())
	return front;
    }
  }

  while(p != points.end()) {
    if (*p != third) {
      assert(mesh.point(*p).getFlag() == 0);
      addPointOnFront(*p);
    }
    ppred = p;
    ++p;
  }

  return front;
}


/*
  During the growing process, the flags are used as follow:
  Triangles:
  -  0: not in the growing surface
  -  1: in the growing surface
  Vertices:
  -  0: not in the growing surface
  - -1: in the growing surface, but not on the wave front
  -  n: in the wave front, contained on n part(s) of the boundary
 */
VertexID WaveFront::growSurfaceUntilJunction(const std::vector<VertexID> & points, unsigned int nbEdges) {
  VertexID result = mesh.getNbPoints();

  if (points.size() < 4)
    return result;

  std::vector<VertexID>::const_iterator p = points.begin();
  std::vector<VertexID>::const_iterator ppred = points.begin();
  VertexID third = mesh.getNbPoints();

  if (front.size() == 0) { // first call: initialization
    mesh.setTriangleFlag(0);
    mesh.setPointFlag(0);


    front.push_back(*p);
    mesh.point(*p).setFlag(1);
    ppred = p;
    ++p;
    front.push_back(*p);
    mesh.point(*p).setFlag(1);

    // adding the third point
    Triangle & t1 = mesh.findTriangle(points.front(), *p);
    third = t1.getOtherPoint(points.front(), *p);
    if (t1.sameOrder(*p, third)) {
      assert((front.begin() <= nextInFront(front.begin())) && (nextInFront(front.begin()) < front.end()));
      front.insert(nextInFront(front.begin()), third);
    }
    else
      front.push_back(third);

    mesh.point(third).setFlag(1);
    t1.setFlag(1);

    ppred = p;
    ++p;
  }
  else { // the growing surface has been started at a previous call, should find the first non seen point
    while(mesh.point(*p).getFlag() != 0) {
      ppred = p;
      ++p;
      if (p == points.end())
	return result;
    }
  }

  while(p != points.end()) {
    if (*p != third) {
      assert(mesh.point(*p).getFlag() == 0);

      addPointOnFront(*p);

      if ((unsigned int) mesh.point(*p).getFlag() >= nbEdges) {
	return *p;
      }
      // TODO fill the circle like parts
    }
    ppred = p;
    ++p;
  }

  // a junction point with the good arity was not found
  return result;
}

void WaveFront::addPointOnFront(VertexID point) {
  front.reserve(front.size() + 2 * mesh.point(point).getNbNeighbours());

  // first get the triangles that will be added
  std::vector<TriangleID> tr = getCorrespondingTriangles(point);

  // then set their flags
  for(std::vector<TriangleID>::const_iterator t = tr.begin(); t != tr.end(); ++t) {
    VertexID p1 = mesh.triangle(*t).getOtherPoint(point);
    VertexID p2 = mesh.triangle(*t).getOtherPoint(point, p1);
    if (isFrontEdge(p1, p2))
      mesh.triangle(*t).setFlag(1);
  }

  // then add edge on the front
  for(std::deque<TriangleID>::const_iterator t = mesh.point(point).getTriangles().begin(); t != mesh.point(point).getTriangles().end(); ++t)
    if (mesh.triangle(*t).getFlag() == 1) {
      addPointOnFrontUsingTriangle(point, *t);
    }
    else {
      // look at the neighbour point in the front
      for(unsigned int i = 0; i < 3; ++i)
	if ((mesh.triangle(*t).getVertexId(i) != point) && (mesh.point(mesh.triangle(*t).getVertexId(i)).getFlag() != 0) && !hasTriangleInside(mesh.triangle(*t).getVertexId(i), point)) {
	  // and add the current point to the front using this point
	  addPointOnFrontUsingPoint(point, mesh.triangle(*t).getVertexId(i), *t);
	  break;
	}
    }
}

 bool WaveFront::isRemovableEdge(VertexID p1, VertexID p2) const {
  if (!mesh.point(p1).hasNeighbour(p2))
    throw Exception("isRemovableEdge(2): Points are not neighbours");
  const Triangle & t1 = mesh.findTriangle(p1, p2);
  if (t1.getFlag() == 0)
    return false;

  try {
    const Triangle & t2 = mesh.findOtherTriangle(p1, p2, t1);
    if (t2.getFlag() == 0)
      return false;
  }
  catch (Exception e) {
    return false;
  }
  return true;
}

void WaveFront::addPointOnEdge(VertexID point, const std::vector<VertexID>::iterator & location) {
  std::vector<std::vector<VertexID>::iterator> rem;

  std::vector<VertexID>::iterator next = location + 1;


  // first insert the point
  assert((next >= front.begin()) && (next <= front.end()));
  front.insert(next, point);
  setFlagInsert(point);

  // check for point to be removed
  std::vector<VertexID>::iterator nextP = nextInFront(location);
  assert(*nextP == point);
  std::vector<VertexID>::iterator nextP3 = nextInFront(nextP, 2);
  const std::vector<VertexID>::iterator predP = predInFront(location);
  std::vector<VertexID>::iterator nextPP = nextInFront(nextP);
  bool onlyFirstLast = true;


  if (*nextP3 == point) {
    if (isRemovableEdge(point, *(nextInFront(nextP)))) {
      std::vector<VertexID>::iterator nextP4 = nextInFront(nextP3);
      std::vector<VertexID>::iterator l = location;
      rem.push_back(nextP);
      rem.push_back(nextPP);
      setFlagRemove(*nextP);
      setFlagRemove(*nextPP);
      while (*l == *nextP4) {
	onlyFirstLast = false;
	rem.push_back(nextP3);
	rem.push_back(nextP4);
	setFlagRemove(*nextP3);
	setFlagRemove(*nextP4);
	nextP3 = l;
	l = predInFront(l);
	nextP4 = nextInFront(nextP4);
      }
    }
  }
  if (*predP == point) {
    if (isRemovableEdge(point, *location)) {
      std::vector<VertexID>::iterator predPP = predInFront(predP);
      setFlagRemove(*predP);
      setFlagRemove(*location);
      rem.push_back(predP);
      rem.push_back(location);
      while (*nextPP == *predPP) {
	onlyFirstLast = false;
	rem.push_back(nextP);
	rem.push_back(nextPP);
	setFlagRemove(*nextP);
	setFlagRemove(*nextPP);
	nextP = predPP;
	predPP = predInFront(predPP);
	nextPP = nextInFront(nextPP);
      }
    }
  }

  if ((rem.size() == 4) && (onlyFirstLast)) {
    std::vector<VertexID>::iterator predPPU = predInFront(predP);
    std::vector<VertexID>::iterator nextPPU = nextInFront(nextP3);
    std::vector<VertexID>::iterator middU = nextP3;
    while (*predPPU == *nextPPU) {
      rem.push_back(middU);
      rem.push_back(nextPPU);
      setFlagRemove(*middU);
      setFlagRemove(*nextPPU);
      middU = predPPU;
      predPPU = predInFront(predPPU);
      nextPPU = nextInFront(nextPPU);
    }
  }

  if (rem.size() != 0) {
    sort(rem.begin(), rem.end());
    //unique(rem.begin(), rem.end());
    for(std::vector<std::vector<VertexID>::iterator>::reverse_iterator i = rem.rbegin(); i != rem.rend(); ++i) {
      assert((front.begin() <= *i) && (*i < front.end()));
      front.erase(*i);
    }
#ifndef NDEBUG
    for(std::vector<VertexID>::const_iterator i = front.begin(); i != front.end(); ++i)
      assert(hasValidFlag(*i));
#endif
  }

}


const std::vector<VertexID>::iterator WaveFront::getNextEdge(const std::vector<VertexID>::iterator & loc, VertexID p1, VertexID p2) {
  bool first = true;
  for(std::vector<VertexID>::iterator p = loc; first || (p != loc); p = nextInFront(p)) {
    first = false;
    if (((*p) == p1) || ((*p) == p2)) {
      std::vector<VertexID>::iterator pp = nextInFront(p);
      if (((*pp) == p1) || ((*pp) == p2)) {
	return p;
      }
    }
  }

  throw Exception("getNextEdge(3): The given edge does not take part of the wavefront.");
}


bool WaveFront::isGoodSidePoint(const std::vector<VertexID>::const_iterator & loc, VertexID point, TriangleID triangle) const {
  if (!mesh.point(point).hasNeighbour(*loc))
    throw Exception("isGoodSidePoint(2): The given points are not neighbours");

  Triangle & t1 = mesh.triangle(triangle);
  VertexID nextP = t1.getOtherPoint(*loc, point);
  TriangleID tt = triangle;

  try {
    while (!isFrontEdge(nextP, *loc)) {
      const Triangle & ttp = mesh.findOtherTriangle(*loc, nextP, tt);
      tt = ttp.getId();
      nextP = ttp.getOtherPoint(*loc, nextP);
    }
  } catch (Exception e) {
    return false;
  }


  if (mesh.triangle(tt).hasPoint(*nextInFrontConst(loc)))
    return isGoodSideTriangle(loc, tt);
  else if (mesh.triangle(tt).hasPoint(*predInFrontConst(loc)))
    return isGoodSideTriangle(predInFrontConst(loc), tt);
  else
    return false;
}


bool WaveFront::isGoodSideTriangle(const std::vector<VertexID>::const_iterator & loc, TriangleID triangle) const {
  if (triangle >= mesh.getNbTriangles())
    throw Exception("isGoodSideTriangle(2): Wrong triangle.");

  return mesh.triangle(triangle).sameOrder(*loc, *(nextInFrontConst(loc)));
}

void WaveFront::addPointOnFrontUsingTriangle(VertexID point, TriangleID triangle) {
  const VertexID p1 = mesh.triangle(triangle).getOtherPoint(point);
  const VertexID p2 = mesh.triangle(triangle).getOtherPoint(point, p1);
  assert(isFrontEdge(p1, p2));

  try {
    const Triangle & oT = mesh.findOtherTriangle(p1, p2, triangle);
    if (oT.getFlag() == 0) { // two edges are available for adding point. We should select the good one.
      std::vector<VertexID>::iterator p = getNextEdge(front.begin(), p1, p2);
      if (isGoodSideTriangle(p, triangle))
	addPointOnEdge(point, p);
      else
	addPointOnEdge(point, getNextEdge(nextInFront(p), p1, p2));
      return;
    }

  }
  catch(Exception e) { }
  assert(isFrontEdge(p1, p2));

  // find the first (and unique) correponding edge and add point between p1 and p2, removing the overmuch points
  addPointOnEdge(point, getNextEdge(front.begin(), p1, p2));
}



void WaveFront::addPointOnFrontUsingPoint(VertexID point, VertexID pointOnFront, TriangleID triangle) {
  unsigned int nbSeen = 0;

  std::vector<VertexID>::iterator pred = front.end();
  for(std::vector<VertexID>::iterator p = front.begin(); p != front.end(); ++p) {
    if (*p == pointOnFront) {
      if (p == front.begin()) // if the correspond point is the first one, we should select the good pred one
	pred = predInFront(front.begin());
      if ((*pred != point) && (*(nextInFront(p)) != point)) {

	// then if the corresponding edge is in the good side of the front, the insertion is done
	if (isGoodSidePoint(p, point, triangle)) {

	  ++nbSeen;
	  // otherwise, add the point splitting pointOnFront
	  if (p > pred) {
	    assert((p >= front.begin()) && (p < front.end()));
	    front.insert(p, point);
	    assert((pred >= front.begin()) && (pred < front.end()));
	    assert((nextInFront(pred) >= front.begin()) && (nextInFront(pred) < front.end()));
	    front.insert(nextInFront(pred), pointOnFront);
	    setFlagInsert(point);
	    setFlagInsert(pointOnFront);
	  }
	  else {
	    assert(pred + 1 == front.end());
	    front.push_back(pointOnFront);
	    front.push_back(point);
	    setFlagInsert(point);
	    setFlagInsert(pointOnFront);
	  }

	  return;
	}
      }
    }
    pred = p;
  }
}



std::vector<TriangleID> WaveFront::getCorrespondingTriangles(VertexID point) const {
  if (point >= mesh.getNbPoints())
    throw Exception("getCorrespondingTriangles(1): wrong point id.");
  std::vector<TriangleID> result;
  for(std::deque<TriangleID>::const_iterator t = mesh.point(point).getTriangles().begin(); t != mesh.point(point).getTriangles().end(); ++t) {
    if (*t >= mesh.getNbTriangles())
      throw Exception("getCorrespondingTriangles(1): wrong triangle id.");
    bool newT = true;

    for(unsigned int i = 0; i < 3; ++i)
      if ((point != mesh.triangle(*t).getVertexId(i)) &&
	  (mesh.point(mesh.triangle(*t).getVertexId(i)).getFlag() == 0)) {
	newT = false;
	break;
      }

    if (newT)
      result.push_back(*t);
  }
  return result;
}

std::ostream & Taglut::operator << (std::ostream & f, const WaveFront & wf) {
  f << "Front:";
  for(std::vector<VertexID>::const_iterator p = wf.front.begin(); p != wf.front.end(); ++p)
    f << " " << *p << "(" << wf.mesh.point(*p).getFlag() << ")";
  return f;
}


std::vector<std::vector<VertexID> > WaveFront::growSurfaceUntilJunctionGetNLoopStart(const std::vector<VertexID> & points, unsigned int nbEdges) {
  return getNLoopStart(growSurfaceUntilJunction(points, nbEdges), nbEdges);
}

std::vector<std::vector<VertexID> > WaveFront::getNLoopStart(VertexID junctionPoint, unsigned int nbPaths) {
  std::vector<std::vector<VertexID> > result;
  unsigned int nb = 0;

  for(std::vector<VertexID>::const_iterator p = front.begin(); p != front.end(); ++p)
    if (*p == junctionPoint) {
      result.push_back(getPathStart(p));
      ++nb;
      if (nb == nbPaths)
	break;
    }

  assert(nb == nbPaths);
  return result;
}

std::vector<VertexID> WaveFront::getPathStart(const std::vector<VertexID>::const_iterator & p) {
  std::vector<VertexID> result;
  std::vector<VertexID>::const_iterator p1 = p;
  std::vector<VertexID>::const_iterator p2 = predInFrontConst(p);

  while(true) {
    if ((p1 == p2) || (p2 == nextInFrontConst(p1)))
      throw Exception("getPathStart(1): wrong front structure");

    try {
      const Triangle & t1 = getTriangleInside(p1);
      if (hasPointInsideNotFront(t1)) {
	result.push_back(getPointInsideNotFront(t1));
	while (result.back() != *p) {
	  result.push_back(*p1);
	  p1 = predInFrontConst(p1);
	}
	break;
      }
    }
    catch (Exception e) { }

    try {
      const Triangle & t2 = getTriangleInside(p2);
      if (hasPointInsideNotFront(t2)) {
	result.push_back(getPointInsideNotFront(t2));
	p2 = nextInFrontConst(p2);
	while (result.back() != *p) {
	  result.push_back(*p2);
	  p2 = nextInFrontConst(p2);
	}
	break;
      }
    }
    catch (Exception e) { }

    p1 = nextInFrontConst(p1);
    p2 = predInFrontConst(p2);
  }

  reverse(result.begin(), result.end());

  return result;
}

const Triangle & WaveFront::getTriangleInside(const std::vector<VertexID>::const_iterator & p) const {
  const Triangle & t1 = mesh.findTriangle(*p, *(nextInFrontConst(p)));
  if (!isGoodSideTriangle(p, t1.getId()))
    return t1;
  else
    try {
      const Triangle & t2 = mesh.findOtherTriangle(*p, *(nextInFrontConst(p)), t1);
      if (!isGoodSideTriangle(p, t2.getId()))
	return t2;
    }
    catch (Exception e) {
    }

  throw Exception("getTriangleOutside(1): triangle not found");
}

bool WaveFront::hasPointInsideNotFront(const Triangle & t) const {
  for(unsigned int i = 0; i < 3; ++i)
    if (mesh.point(t.getVertexId(i)).getFlag() == -1)
      return true;

  return false;
}

VertexID WaveFront::getPointInsideNotFront(const Triangle & t) const {
  for(unsigned int i = 0; i < 3; ++i) {
    if (mesh.point(t.getVertexId(i)).getFlag() == -1) {
      return t.getVertexId(i);
    }
  }
  throw Exception("getPointInsideNotFront(1): no corresponding point.");
}

bool WaveFront::hasValidFlag(VertexID point) const {
  unsigned int i = 0;
  for(std::vector<VertexID>::const_iterator pf = front.begin(); pf != front.end(); ++pf)
    if (*pf == point)
      ++i;
  bool result = ((i == 0) && (mesh.point(point).getFlag() <= 0)) || ((unsigned int) mesh.point(point).getFlag() == i);
  if (!result) {
    std::cout << "Error: " << point << " has bad flag value (" << mesh.point(point).getFlag() << ", " << i << ")" << std::endl;
    std::cout << *this << std::endl;
  }
  return result;
}

void WaveFront::setFlagInsert(VertexID point) {
  mesh.point(point).incFlag();
}

void WaveFront::setFlagRemove(VertexID point) {
  mesh.point(point).incFlag(-1);
  if (mesh.point(point).getFlag() == 0)
    mesh.point(point).setFlag(-1);
}

std::vector<std::vector<VertexID> > WaveFront::getBorderPaths() {
  std::vector<std::vector<VertexID> > paths;

  // init flag values on the front
  for(std::vector<VertexID>::const_iterator pf = front.begin(); pf != front.end(); ++pf)
    mesh.point(*pf).setFlag(0);

  // first set flags according to the arity of the points on the wavefront.
  for(std::vector<VertexID>::const_iterator pf = front.begin(); pf != front.end(); ++pf) {
    mesh.point(*pf).incFlag();
    if (mesh.point(*pf).getIsBoundary()) {
      mesh.point(*pf).incFlag();
      if (!mesh.point(*nextInFrontConst(pf)).getIsBoundary() || !mesh.point(*predInFrontConst(pf)).getIsBoundary())
	mesh.point(*pf).incFlag();
    }
  }

  // then find the first point with arity > 2
  std::vector<VertexID>::const_iterator curStart = front.begin();
  std::vector<VertexID> firstPath;
  firstPath.push_back(*curStart);
  while(mesh.point(*curStart).getFlag() == 2) {
    ++curStart;
    firstPath.push_back(*curStart);
    if (curStart == front.end()) { // if there is no multipoint on the front
      paths.push_back(firstPath);
      return paths;
    }
  }

  std::vector<VertexID> path;
  bool add = true;
  bool isBoundaryPath = true;
  path.push_back(*curStart);
  for(std::vector<VertexID>::const_iterator current = curStart + 1; current != front.end(); ++current) {
    path.push_back(*current);
    if (mesh.point(*current).getFlag() > 2) {
      // end of a path
      if (add && (!isBoundaryPath) && ((path.size() > 3) || (path.front() != path.back())))
	paths.push_back(path);
      path.clear();
      path.push_back(*current);
      add = true;
      isBoundaryPath = true;
    }
    else {
      if (mesh.point(*current).getFlag() <= 1) // exclude from this construction the tree paths (that are still present...)
	add = false;
      if ((!mesh.point(*current).getIsBoundary()) || (isBoundaryPath && (path.size() > 2) && (!mesh.isBoundaryEdge(path.back(), path[path.size() - 2]))))
	isBoundaryPath = false;
      mesh.point(*current).setFlag(0);
    }
  }


  return paths;
}

void WaveFront::removeTreeParts() {
  bool ok = false;
  while(!ok) {
    ok = true;
    std::vector<VertexID>::iterator pred = predInFront(front.begin());
    std::vector<VertexID>::iterator next = nextInFront(front.begin());
    for(std::vector<VertexID>::iterator m = front.begin(); m != front.end(); ++m) {
      if (*pred == *next) {
	ok = false;

	setFlagRemove(*pred);
	setFlagRemove(*m);
	if (m < pred) {
	  front.erase(pred);
	  front.erase(m);
	}
	else {
	  front.erase(m);
	  front.erase(pred);
	}
	break;
      }

      pred = m;
      next = nextInFront(next);
    }


  }

}
