/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CNR-IMATI
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef WEIGHTED_EDGES
#define WEIGHTED_EDGES

#include <map>
#include "Mesh.h"
#include "IDTypes.h"
#include "Node.h"



namespace Taglut {

  class WeightedEdges {
  private:
    /** weights associated to the edges */
    std::map<VertexID, double> weights;

    /** the manipulated mesh */
    const Mesh & mesh;

    /** return the ID of the non-oriented edge */
    inline VertexID getEdgeId(VertexID v1, VertexID v2) const {
      if (v1 > v2)
	return v2 * mesh.getNbPoints() + v1;
      else
	return v1 * mesh.getNbPoints() + v2;
    }

  public:
    /** default constructor */
    WeightedEdges(const Mesh & mesh);

    /** copy constructor */
    WeightedEdges(const WeightedEdges & we);

    /** copy constructor using a new mesh */
    WeightedEdges(const WeightedEdges & we, const Mesh & mesh);

    /** destructor */
    ~WeightedEdges();

    /** accessor */
    inline const Mesh & getMesh() const { return mesh; }

    /** accessor */
    const double & operator()(VertexID v1, VertexID v2) const;
    /** accessor */
    inline const double & operator()(const Node & n1, const Node & n2) const {
      return (*this)(n1.getId(), n2.getId());
    }

    /** modifier */
    double & operator()(VertexID v1, VertexID v2);
    /** modifier */
    inline double & operator()(const Node & n1, const Node & n2) {
      return (*this)(n1.getId(), n2.getId());
    }

    /** return true if the given edge is weigthed */
    inline bool isDefined(VertexID v1, VertexID v2) const {
      return weights.find(getEdgeId(v1, v2)) != weights.end();
    }

    /** accessor */
    inline VertexID getNbLengths() const {
      return weights.size();
    }

  };

};

#endif
