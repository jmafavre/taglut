/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshPrune.h"

#include <list>

using namespace Taglut;

const int MeshPrune::NOT_SEEN = 0;
const int MeshPrune::IN_PROGRESS = 42;

const int MeshPrune::UNDER_FINALMESH = 1;
const int MeshPrune::IN_FINALMESH = 3;
const int MeshPrune::IN_FINALMESH_B = 2;
const int MeshPrune::IN_MIDDLE = 4;
const double MeshPrune::EPSILON = 0.0000001;

/**
   @struct MeshPrune::multiTriangleSort
   @brief Functor used to sort triangles
*/
struct MeshPrune::multiTriangleSort {
  /**
     Mesh that contains triangles
  */
  Mesh * mesh;


  /**
     return true if the given edge is part of a triangle with flag = 1
     of if it is part of a boundary
  */
  bool isGoodEdge(VertexID id1, VertexID id2) const {
    const Triangle & t1 = (*mesh).findTriangle(id1, id2);

    if (t1.getFlag() != 0)
      return true;
    else {
      try {
	const Triangle & t2 = (*mesh).findOtherTriangle(id1, id2, t1);
	return (t2.getFlag() != 0);
      }
      catch (Exception) {
	return true;
      }
    }
  }

  /**
     binary comparaison (is triangle1 better than triangle2?)
   */
  bool operator()(const MultiTriangle & triangle1, const MultiTriangle & triangle2) const {
    const MultiEdge & edges1first = triangle1.getEdge(1);
    const MultiEdge & edges1second = triangle1.getEdge(2);
    const MultiEdge & edges2first = triangle2.getEdge(1);
    const MultiEdge & edges2second = triangle2.getEdge(2);

    if (triangle1.isStar() && triangle2.isStar()) {
      bool isGoodStar1 = (isGoodEdge(edges1first.getFirstId(), edges1first.getMiddleId()) &&
			  isGoodEdge(edges1first.getMiddleId(), edges1first.getLastId()) &&
			  isGoodEdge(edges1second.getFirstId(), edges1second.getMiddleId()));
      bool isGoodStar2 = (isGoodEdge(edges2first.getFirstId(), edges2first.getMiddleId()) &&
			  isGoodEdge(edges2first.getMiddleId(), edges2first.getLastId()) &&
			  isGoodEdge(edges2second.getFirstId(), edges2second.getMiddleId()));
      if (isGoodStar1 && !isGoodStar2)
	return true;
      if (isGoodStar2 && !isGoodStar1)
	return false;
    }

    // squashed triangles are bad
    bool squashedT1 = triangle1.isSquashed();
    bool squashedT2 = triangle2.isSquashed();
    if ((squashedT1 && !squashedT2) || triangle1.isStar())
      return false;
    else if ((squashedT2 && !squashedT1) || triangle2.isStar())
      return true;

    // a border point is better than all others
    if (((*mesh).point(edges1first.getLastId()).getFlag() == IN_FINALMESH_B) &&
	((*mesh).point(edges2first.getLastId()).getFlag() != IN_FINALMESH_B))
      return true;
    if (((*mesh).point(edges2first.getLastId()).getFlag() == IN_FINALMESH_B) &&
	((*mesh).point(edges1first.getLastId()).getFlag() != IN_FINALMESH_B))
      return false;

    // check local curvature (on vertices)
    double lcs1 = (*mesh).computeMeanLocalCurvature(edges1first.getLastId());
    double lcs2 = (*mesh).computeMeanLocalCurvature(edges2first.getLastId());
    if (lcs2 < lcs1 - EPSILON)
      return true;
    else if (lcs2 > lcs1 + EPSILON)
      return false;

    // check local curvature (on edges)
    double lc1 = (*mesh).computeLocalCurvature(edges1first) + (*mesh).computeLocalCurvature(edges1second);
    double lc2 = (*mesh).computeLocalCurvature(edges2first) + (*mesh).computeLocalCurvature(edges2second);

    if (lc2 < lc1 - EPSILON)
      return true;
    else if (lc2 > lc1 + EPSILON)
      return false;

    // next, use length
    double v1 = (*mesh).getLength(edges1first) + (*mesh).getLength(edges1second);
    double v2 = (*mesh).getLength(edges2first) + (*mesh).getLength(edges2second);
    if (v1 < v2 - EPSILON)
      return false;
    else if (v1 > v2 + EPSILON)
      return true;

    // then, return id
    return edges1first.getLastId() < edges2first.getLastId();
  }
};


MeshPrune::MeshPrune(Mesh & mesh_l, unsigned int depth_l) : MeshManipulator(mesh_l), depth(depth_l) {
  newId = NULL;
  localLength = NULL;
  originalIds = true;
  nbOriginalPoints = 0;
  nbSeenTriangles = 0;
}

MeshPrune::~MeshPrune() {
  if (newId != NULL)
    delete [] newId;
  if (localLength != NULL)
    delete [] localLength;

}

bool MeshPrune::isGoodStar(const MultiTriangle & mt) const {
  if (!mt.isStar())
    return false;
  multiTriangleSort mSort;
  mSort.mesh = mesh;

  return (mSort.isGoodEdge(mt.getP1(), mt.getEdge(0).getMiddleId()) &&
	  mSort.isGoodEdge(mt.getP2(), mt.getEdge(0).getMiddleId()) &&
	  mSort.isGoodEdge(mt.getP3(), mt.getEdge(0).getMiddleId()));
}

void MeshPrune::pruneMesh(unsigned int localDepth) {
  if (localDepth == 0)
    return;
  else {
    TriangleID nbTBefore = (*mesh).getNbTriangles();
    buildMesh();

    if (resultTriangles.size() == 0) {
      std::cout << "Warning: resulting triangle list is empty." << std::endl;
    }

    TriangleID nbSmall = 0;
    for(std::deque<MultiTriangle>::const_iterator t = resultTriangles.begin(); t != resultTriangles.end(); ++t)
      nbSmall += (*t).getTriangles().size();

    if (nbSmall != nbTBefore)
      std::cout << "Warning: during prune process, only " << nbSmall << " small triangles has been seen (should be " << nbTBefore << ")" << std::endl;

    if (localDepth > 1)
      pruneMesh(localDepth - 1);
  }
}


void MeshPrune::pruneMesh() {
  pruneMesh(depth);
}


MultiEdge MeshPrune::buildInitialEdge() {
  double curvature = -1;
  MultiEdge mEdge;

  // compute edge with maximum curvature
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin();
	nb != (*p).getNeighbours().end(); ++nb) {
      double localCurvature = (*mesh).computeLocalCurvature((*p).getId(), *nb);
      if (curvature < localCurvature) {
	curvature = localCurvature;
	mEdge.setFirstId((*p).getId());
	mEdge.setLastId(*nb);
      }
    }

  // then choose prolongation from the point with the better alignement
  double angle = 42;
  VertexID nextPoint = (*mesh).getNbPoints();
  bool inFirst = true;
  for(std::deque<VertexID>::const_iterator nb = (*mesh).point(mEdge.getFirstId()).getNeighbours().begin();
      nb != (*mesh).point(mEdge.getFirstId()).getNeighbours().end(); ++nb)
    if (*nb != mEdge.getLastId()) {
      double lAngle = M_PI - (*mesh).computeAngle(mEdge.getFirstId(), mEdge.getLastId(), *nb);
      if (lAngle < angle) {
	nextPoint = *nb;
	angle = lAngle;
      }
    }
  assert(nextPoint != (*mesh).getNbPoints());

  for(std::deque<VertexID>::const_iterator nb = (*mesh).point(mEdge.getLastId()).getNeighbours().begin();
      nb != (*mesh).point(mEdge.getLastId()).getNeighbours().end(); ++nb)
    if (*nb != mEdge.getFirstId()) {
      double lAngle = M_PI - (*mesh).computeAngle(mEdge.getLastId(), mEdge.getFirstId(), *nb);
      if (lAngle < angle) {
	nextPoint = *nb;
	angle = lAngle;
	inFirst = false;
      }
    }

  // add it to the edge
  if (inFirst)
    mEdge.addAsFirst(nextPoint);
  else
    mEdge.addAsLast(nextPoint);

  return mEdge;
}

bool MeshPrune::isUnknownTriangle(const MultiTriangle & t) const {
  VertexID idP = (*mesh).getNbPoints();

  // first find a point in t that has been seen before
  for(unsigned int i = 0; i < 3; ++i) // no iterator available
    if (((*mesh).point(t.getVertexId(i)).getFlag() == IN_FINALMESH_B) ||
	((*mesh).point(t.getVertexId(i)).getFlag() == IN_FINALMESH)) {
      idP = t.getVertexId(i);
      break;
    }
  if (idP == (*mesh).getNbPoints())
    return true;

  // check each triangle connected to this point
  for(std::deque<TriangleID>::const_iterator it = resultPoints[newId[idP]].getTriangles().begin();
      it != resultPoints[newId[idP]].getTriangles().end(); ++it) {
    const MultiTriangle & mt = resultTriangles[*it];
    if (mt.hasPoint(t.getP1()) && mt.hasPoint(t.getP2()) && mt.hasPoint(t.getP3())) {
      return false;
    }
  }

  return true;
}


const MultiEdge & MeshPrune::findNeighbourBoundaryMEdge(VertexID point, const MultiEdge & mEdge) const {
  VertexID otherPoint = mEdge.getOtherId(point);
  for(std::deque<TriangleID>::const_iterator t = resultPoints[newId[point]].getTriangles().begin(); t != resultPoints[newId[point]].getTriangles().end(); ++t){
      assert(*t < resultTriangles.size());
      const MultiTriangle & mt = resultTriangles[*t];
      for(unsigned int i = 0; i < 3; ++i) { // there is no iterator available
	const MultiEdge & e = mt.getEdge(i);
	if (e.hasPoint(point) && (!e.hasPoint(otherPoint)) && isOpenEdge(e)) // for this triangle, check if its an open edge
	  return e;
      }
    }

  return mEdge;
}


bool MeshPrune::hasAvailableNewTriangle(const MultiEdge & mEdge) {
  std::list<MultiTriangle> mtList = computeOpenTriangles(mEdge);

  for(std::list<MultiTriangle>::const_iterator mti = mtList.begin(); mti != mtList.end(); ++mti)
    if ((*mesh).point((*mti).getOtherPoint(mEdge.getFirstId(), mEdge.getLastId())).getFlag() == NOT_SEEN)
      return true;


  return false;
}

bool MeshPrune::isBorderClosure(const MultiEdge & mEdge) {
  if ((*mesh).point(mEdge.getFirstId()).getIsBoundary() == (*mesh).point(mEdge.getLastId()).getIsBoundary())
    return false;
  else if ((*mesh).point(mEdge.getLastId()).getIsBoundary())
    return isBorderClosure(mEdge.getInverted());

  assert((*mesh).point(mEdge.getFirstId()).getIsBoundary());
  MultiEdge m2 = findNeighbourBoundaryMEdge(mEdge.getLastId(), mEdge);

  if (isInOpenTriangle(m2))
    return false;

  VertexID otherPoint = m2.getOtherId(mEdge.getLastId());

  if (!(*mesh).point(otherPoint).getIsBoundary())
    return false;

  return !isInOpenTriangle(m2);
}

bool MeshPrune::isQuadrilateralClosure(const MultiEdge & mEdge) const {
  // find the two neighbour edges of mEdge
  const MultiEdge & mEdge1 = findNeighbourBoundaryMEdge(mEdge.getFirstId(), mEdge);
  const MultiEdge & mEdge2 = findNeighbourBoundaryMEdge(mEdge.getLastId(), mEdge);

  if ((mEdge1 == mEdge) || (mEdge2 == mEdge))
    return false;

  if (isInOpenTriangle(mEdge1) ||
      isInOpenTriangle(mEdge2))
    return false;

  // find the two points (next and pred)
  const Point3D & p1 = resultPoints[newId[mEdge1.getOtherId(mEdge.getFirstId())]];
  const Point3D & p2 = resultPoints[newId[mEdge2.getOtherId(mEdge.getLastId())]];

  // if those points are not neighbours, return false
  if (!p1.hasNeighbour(mEdge2.getOtherId(mEdge.getLastId())))
    return false;
  else { // else, find multi triangle that connect p1 and p2
    for(std::deque<TriangleID>::const_iterator t = p1.getTriangles().begin(); t != p1.getTriangles().end(); ++t) {
      assert(*t < resultTriangles.size());
      const MultiTriangle & mt = resultTriangles[*t];
      if (mt.hasPoint(p2)) // for this triangle, check if its an open edge
	return isOpenEdge(mt.getEdge(p1.getId(), p2.getId())) && !isInOpenTriangle(mt.getEdge(p1.getId(), p2.getId()));
    }

    // other case: not possible
    throw Exception("isQuadrilateralClosure(1): triangle has not be found. Not possible.");
  }
}

std::deque<MultiEdge> MeshPrune::getQuadrilateralBorderClosure(const MultiEdge & mEdge) {
  std::deque<MultiEdge> result;
  MultiEdge first, middle, last, vEdge;
  bool invert = false;

  if ((*mesh).point(mEdge.getFirstId()).getIsBoundary())
    if ((*mesh).point(mEdge.getLastId()).getIsBoundary())
      return std::deque<MultiEdge>();
    else {
      first = mEdge;
      middle = findNeighbourBoundaryMEdge(first.getLastId(), first).getOrderedFirst(first.getLastId());
      if ((*mesh).point(middle.getFirstId()).getIsBoundary() ||
	  (*mesh).point(middle.getLastId()).getIsBoundary())
	return result;
      last = findNeighbourBoundaryMEdge(middle.getLastId(), middle).getOrderedFirst(middle.getLastId());
      if ((*mesh).point(last.getFirstId()).getIsBoundary() ||
	  !(*mesh).point(last.getLastId()).getIsBoundary())
	return result;
      vEdge = MultiEdge(last.getLastId(), first.getFirstId());
    }
  else
    if ((*mesh).point(mEdge.getLastId()).getIsBoundary()) {
      first = mEdge.getOrderedFirst(mEdge.getLastId());
      if (!(first == mEdge))
	invert = true;
      middle = findNeighbourBoundaryMEdge(first.getLastId(), first).getOrderedFirst(first.getLastId());
      if ((*mesh).point(middle.getFirstId()).getIsBoundary() ||
	  (*mesh).point(middle.getLastId()).getIsBoundary())
	return result;
      last = findNeighbourBoundaryMEdge(middle.getLastId(), middle).getOrderedFirst(middle.getLastId());
      if ((*mesh).point(last.getFirstId()).getIsBoundary() ||
	  !(*mesh).point(last.getLastId()).getIsBoundary())
	return result;
      vEdge = MultiEdge(last.getLastId(), first.getFirstId());
    }
    else {
      middle = mEdge;
      first = findNeighbourBoundaryMEdge(middle.getLastId(), middle).getOrderedLast(middle.getFirstId());
      if (!(*mesh).point(first.getFirstId()).getIsBoundary() ||
	  (*mesh).point(first.getLastId()).getIsBoundary())
	return result;
      last = findNeighbourBoundaryMEdge(middle.getLastId(), middle).getOrderedFirst(middle.getLastId());
      if ((*mesh).point(last.getFirstId()).getIsBoundary() ||
	  !(*mesh).point(last.getLastId()).getIsBoundary())
	return result;
      vEdge = MultiEdge(last.getLastId(), first.getFirstId());
    }

  if (isInOpenTriangle(first) ||
      isInOpenTriangle(middle) ||
      isInOpenTriangle(last))
    return result;

  if ((first.getLastId() != middle.getFirstId()) ||
      (middle.getLastId() != last.getFirstId()) ||
      (last.getLastId() != vEdge.getFirstId()) ||
      (vEdge.getLastId() != first.getFirstId()))
    return result;

  result.push_back(first);
  result.push_back(middle);
  result.push_back(last);
  result.push_back(vEdge);

  if (invert) {
    std::deque<MultiEdge> newResult;
    for(std::deque<MultiEdge>::const_iterator me = result.begin(); me != result.end(); ++me)
      newResult.push_front((*me).getInverted());
    result = newResult;
  }

  assert((*mesh).point(first.getFirstId()).getIsBoundary());
  assert(!(*mesh).point(middle.getFirstId()).getIsBoundary());
  assert(!(*mesh).point(last.getFirstId()).getIsBoundary());
  assert((*mesh).point(vEdge.getFirstId()).getIsBoundary());

  return result;
}

bool MeshPrune::isInOpenTriangle(const MultiEdge & mEdge) const {
  for(std::deque<MultiTriangle>::const_iterator t = openMultiTriangleSet.begin(); t != openMultiTriangleSet.end(); ++t)
    if ((*t).hasEdge(mEdge))
      return true;

  return false;
}

bool MeshPrune::isQuadrilateralBorderClosure(const MultiEdge & mEdge) {
  return getQuadrilateralBorderClosure(mEdge).size() == 4;
}


std::list<MultiTriangle> MeshPrune::computeOpenTriangles(const MultiEdge & mEdge) {
  std::deque<VertexID> nbFirst = computeOpenNeighbours((*mesh).point(mEdge.getFirstId()));
  std::deque<VertexID> nbSecond = computeOpenNeighbours((*mesh).point(mEdge.getLastId()));
  std::deque<VertexID> doubleNb;

#ifndef NDEBUG
  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    assert((*i).getFlag() < IN_PROGRESS);
#endif
  int * tmpId;
  tmpId = new int [nbFirst.size()];

  // first computing intersection between neighbours
  VertexID i = 0;
  for(std::deque<VertexID>::const_iterator e = nbFirst.begin(); e != nbFirst.end(); ++e) {
    tmpId[i] = (*mesh).point(*e).getFlag();
    assert(tmpId[i] < IN_PROGRESS);
    assert(tmpId[i] != IN_FINALMESH);
    (*mesh).point(*e).setFlag(IN_PROGRESS + i);
    ++i;
  }

  i = 0;
  for(std::deque<VertexID>::const_iterator e = nbSecond.begin(); e != nbSecond.end(); ++e) {
    int lFlag = (*mesh).point(*e).getFlag();
    assert(lFlag != IN_FINALMESH);
    if (lFlag >= IN_PROGRESS)
      doubleNb.push_front(*e);
    ++i;
  }

  i = 0;
  for(std::deque<VertexID>::const_iterator e = nbFirst.begin(); e != nbFirst.end(); ++e) {
    (*mesh).point(*e).setFlag(tmpId[i++]);
    assert((*mesh).point(*e).getFlag() != IN_MIDDLE);
  }
  delete[] tmpId;

  if (doubleNb.size() == 0) {
    return std::list<MultiTriangle>();
  }

  // then build list of edges
  std::list<MultiTriangle> mTriangles;

  // insert
  for(std::deque<VertexID>::const_iterator t = doubleNb.begin(); t != doubleNb.end(); ++t) {
    std::list<std::pair<MultiEdge, MultiEdge> > eList = computeMatchingTriangles(mEdge, *t);
    for(std::list<std::pair<MultiEdge, MultiEdge> >::const_iterator it = eList.begin(); it != eList.end(); ++it) {
      mTriangles.push_back(MultiTriangle(mEdge, (*it).first, (*it).second));
    }
  }

  return mTriangles;
}

MultiTriangle MeshPrune::buildTriangleUsingFlags(const MultiEdge & mEdge, TriangleID idFTriangle) {
  std::list<MultiTriangle> mTriangles = computeOpenTriangles(mEdge);

  if (mTriangles.size() == 0) {
    return MultiTriangle();
  }

  multiTriangleSort mSort;
  mSort.mesh = mesh;

  // and order it
  mTriangles.sort(mSort);



  for(std::list<MultiTriangle>::iterator me = mTriangles.begin(); me != mTriangles.end(); ++me) {
    assert((*me).getEdge(1).getLastId() == (*me).getEdge(2).getLastId());
    // check triangle using this points
    if (isUnknownTriangle(*me)) {
      if (checkTriangleUsingFlags(*me, idFTriangle)) {
	return *me;
      }
    }
  }


  return MultiTriangle();
}

Triangle & MeshPrune::findFirstTriangleInside(const MultiTriangle & mTriangle) {
  return findFirstTriangleInside(mTriangle.getEdge(0), mTriangle.getEdge(2));
}


bool MeshPrune::fillTriangleUsingFlags(MultiTriangle & triangle, const Triangle & firstTriangle, TriangleID idFTriangle) {
  bool stop = false;

  std::deque<TriangleID> openSet, listOfSeenTriangles;

  if ((firstTriangle.getFlag() == 1) || (firstTriangle.getId() == idFTriangle)) {
    throw Exception("fillTriangleUsingFlags(3): wrong triangle");
  }

  openSet.push_front(firstTriangle.getId());
  assert(firstTriangle.getFlag() == 0);
  (*mesh).triangle(firstTriangle.getId()).setFlag(2);
  listOfSeenTriangles.push_front(firstTriangle.getId());


  while((openSet.size() != 0) && (!stop)) {
    TriangleID currentTriangle = openSet.front();
    assert((*mesh).triangle(currentTriangle).getFlag() == 2);
    openSet.pop_front();

    for(unsigned int i = 0; i < 3; ++i) { // no iterator available
      // get edge #i
      std::pair<VertexID, VertexID> edge = (*mesh).triangle(currentTriangle).getEdge(i);
      // if this edge is not an edge of the multitriangle
      if (!triangle.hasMinimalEdge(edge.first, edge.second)) {
	try {
	  // find other triangle using this edge
	  Triangle & otherTriangle = (*mesh).findOtherTriangle(edge.first, edge.second, currentTriangle);
	  // if other triangle found
	  if ((otherTriangle.getFlag() == 1) || (otherTriangle.getId() == idFTriangle)) {
	    stop = true;
	    break;
	  }
	  else if (otherTriangle.getFlag() == 0) { // if has not been seen before
	    // adding triangle to the open set
	    openSet.push_front(otherTriangle.getId());
	    otherTriangle.setFlag(2);
	    listOfSeenTriangles.push_front(otherTriangle.getId());
	  }

	}
	catch(Exception e) { }
      }
    }
  }

  // if a triangle with flag = 1 has been found, or forbidden triangle has been found
  if (stop) {
    // reset all seen triangles
    for(std::deque<TriangleID>::const_iterator t = listOfSeenTriangles.begin(); t != listOfSeenTriangles.end(); ++t)
      (*mesh).triangle(*t).setFlag(0);

    // error
    return false;
  }


  if (listOfSeenTriangles.size() != 0) {
    // first compute forbidden triangles
    addForbiddenTriangle(triangle);

    // then update list of triangles
    for(std::deque<TriangleID>::const_iterator t = listOfSeenTriangles.begin(); t != listOfSeenTriangles.end(); ++t) {
      (*mesh).triangle(*t).setFlag(1);
    }

    // then set list of triangles of the Multitriangle
    assert(listOfSeenTriangles.size() != 0);
    triangle.setTriangles(listOfSeenTriangles);

    return true;
  }
  else
    return false;
}


bool MeshPrune::checkTriangleUsingFlags(MultiTriangle & triangle, TriangleID idFTriangle) {
  assert(triangle.getEdge(2).getLastId() == triangle.getEdge(1).getLastId());
  // first check for tripode configuration
  if (triangle.isStar()) {
    addForbiddenTriangleStar(triangle);
    return true;
  }

  // then check if middle points are not edge points
  for(unsigned int i = 0; i < 3; ++i) // no iterator available
    if (triangle.getEdge(i).hasMiddlePoint())
      if (triangle.hasPoint(triangle.getEdge(i).getMiddleId())) {
	return false;
      }

  // set flags for all triangles
  std::deque<TriangleID> listOfSeenTriangles;
  try {
    if (fillTriangleUsingFlags(triangle, idFTriangle)) {
      if ((triangle.getTriangles().size() > nbSeenTriangles) && (computeNbOpenEdge() < 3)) {
	for(std::deque<TriangleID>::const_iterator t = triangle.getTriangles().begin();
	    t != triangle.getTriangles().end(); ++t)
	  (*mesh).triangle(*t).setFlag(0);

	return false;
      }
      else
	return true;
    }
    else {
      return false;
    }
  }
  catch(Exception e) {
    return false;
  }

}

void MeshPrune::addForbiddenTriangleStar(MultiTriangle & triangle) const {
  assert(triangle.isStar());

  for(unsigned int i = 0; i < 3; ++i)
    if (!isPartOfFinalEdge(triangle.getEdge(i))) {
      unsigned int pred_l, next;
      pred_l = 0;
      if (i == 0) pred_l = 1;
      next = 2;
      if (i == 2) next = 1;

      if (!isPartOfFinalEdge(triangle.getEdge(pred_l))) {
	Triangle & t1 = (*mesh).findTriangle(triangle.getEdge(i).getFirstId(), triangle.getEdge(i).getMiddleId());
	if (((i == 1) && t1.sameOrder(triangle.getEdge(i).getFirstId(), triangle.getEdge(i).getMiddleId())) ||
	    ((i != 1) && !t1.sameOrder(triangle.getEdge(i).getFirstId(), triangle.getEdge(i).getMiddleId()))) {
	  triangle.addForbiddenTriangle(i, t1.getId());
	}
	else {
	  try {
	    Triangle & t2 = (*mesh).findOtherTriangle(triangle.getEdge(i).getMiddleId(), triangle.getEdge(i).getLastId(), t1);
	    triangle.addForbiddenTriangle(i, t2.getId());
	  }
	  catch(Exception e) { }
	}
      }

      if (!isPartOfFinalEdge(triangle.getEdge(next))) {
	Triangle & t1 = (*mesh).findTriangle(triangle.getEdge(i).getMiddleId(), triangle.getEdge(i).getLastId());
	if (((i == 1) && t1.sameOrder(triangle.getEdge(i).getMiddleId(), triangle.getEdge(i).getLastId())) ||
	    ((i != 1) && !t1.sameOrder(triangle.getEdge(i).getMiddleId(), triangle.getEdge(i).getLastId()))) {
	  triangle.addForbiddenTriangle(i, t1.getId());
	}
	else {
	  try {
	    Triangle & t2 = (*mesh).findOtherTriangle(triangle.getEdge(i).getMiddleId(), triangle.getEdge(i).getLastId(), t1);
	    triangle.addForbiddenTriangle(i, t2.getId());
	  }
	  catch(Exception e) { }
	}
      }
  }
}


void MeshPrune::addForbiddenTriangle(MultiTriangle & triangle) const {
  for(unsigned int i = 0; i < 3; ++i) {
    assert(i < 3);

    // if given edge has not middle point, there is no forbidden point
    if (!triangle.getEdge(i).hasMiddlePoint())
      continue;

    VertexID id1 = (*mesh).point(triangle.getEdge(i).getFirstId()).getId();
    Point3D & pM = (*mesh).point(triangle.getEdge(i).getMiddleId());
    VertexID id2 = (*mesh).point(triangle.getEdge(i).getLastId()).getId();
    TriangleID idT = (*mesh).findTriangle(id1, pM.getId()).getId();

    TriangleID idT2 = (*mesh).getNbTriangles();
    if ((*mesh).triangle(idT).getFlag() != 2) {
      try {
	idT = (*mesh).findOtherTriangle(id1, pM.getId(), idT).getId();
      }
      catch (Exception e) {
	continue;
      }
    }

    // if the found triangle contains the third point, there is no forbidden triangle
    if ((*mesh).triangle(idT).hasPoint(id2))
      continue;

    // else, if triangle is inside
    if ((*mesh).triangle(idT).getFlag() == 2) {
      // we are computing the other small triangle in the same side of the edge #i
      try {
	idT2 = (*mesh).findTriangle(pM.getPointSameSide(id1, id2, (*mesh).triangle(idT).getOtherPoint(id1, pM.getId())), id2, pM.getId()).getId();
      }
      catch (Exception e) {
	continue;
      }
    }
    else {
      // if triangle is outside, we should watch for triangles near to the (pM, id2) edge
      idT = (*mesh).findTriangle(id2, pM.getId()).getId();

      if ((*mesh).triangle(idT).getFlag() != 2) {
	try {
	  idT = (*mesh).findOtherTriangle(id2, pM.getId(), idT).getId();
	}
	catch (Exception e) {
	  continue;
	}
      }

      assert((*mesh).triangle(idT).getFlag() == 2);

      idT2 = (*mesh).findTriangle(pM.getPointSameSide(id2, id1, (*mesh).triangle(idT).getOtherPoint(id2, pM.getId())), id1, pM.getId()).getId();
    }
    assert(idT2 != (*mesh).getNbTriangles());

    if ((*mesh).triangle(idT2).getFlag() != 2)
      triangle.addForbiddenTriangle(i, idT2);
  }
}


VertexID MeshPrune::computeNbOpenEdge() const {
  VertexID result = 0;
  for(std::deque<MultiTriangle>::const_iterator t = openMultiTriangleSet.begin();
      t != openMultiTriangleSet.end(); ++t)
    for(unsigned int i = 0; i < 3; ++i)
      if (isOpenEdge((*t).getEdge(i)))
	++result;

  return result;
}

bool MeshPrune::isOpenEdge(const MultiEdge & e) const {
  if (((newId[e.getFirstId()] == (*mesh).getNbPoints()) || (newId[e.getLastId()] == (*mesh).getNbPoints())))
    return false;
  return (!isBoundaryEdge(e)) && resultPoints[newId[e.getFirstId()]].isPartOfBoundaryEdge(e.getLastId(), resultTriangles);
}

bool MeshPrune::isBoundaryEdge(const MultiEdge & e) const {
  if (e.hasMiddlePoint())
    return (*mesh).isBoundaryEdge(e.getFirstId(), e.getMiddleId()) && (*mesh).isBoundaryEdge(e.getMiddleId(), e.getLastId());
  else
    return (*mesh).isBoundaryEdge(e.getFirstId(), e.getLastId());
}

bool MeshPrune::isBoundaryVertex(const Point3D & point) const {
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb)
    if (point.isPartOfBoundaryEdge(*nb, resultTriangles))
      return true;
  return false;
}


MultiTriangle MeshPrune::buildFirstTriangle() {
  // build first edge and set flags
  MultiEdge mEdge = buildInitialEdge();
  if (mEdge.hasMiddlePoint())
    (*mesh).point(mEdge.getMiddleId()).setFlag(UNDER_FINALMESH);

  std::list<MultiTriangle> mTriangles = computeOpenTriangles(mEdge);

  if (mTriangles.size() == 0) {
    return MultiTriangle();
  }

  multiTriangleSort mSort;
  mSort.mesh = mesh;

  // and order it
  mTriangles.sort(mSort);

  // then get the first one
  MultiTriangle & result = mTriangles.front();

  try {
    Triangle & t1 = findFirstTriangleInside(result);
    bool r = fillTriangleUsingFlags(result, t1);

    TriangleID size = result.getTriangles().size();
    if ((!r) || (size > (*mesh).getNbTriangles() - size)) {
      // invert triangle
      std::deque<TriangleID> tr = result.getTriangles();
      for(std::deque<TriangleID>::const_iterator str = tr.begin(); str != tr.end(); ++str)
	(*mesh).triangle(*str).setFlag(0);
      result.clearTriangles();
      result.invertOrder();

      Triangle & t2 = findFirstTriangleInside(result);
      assert(t1.getId() != t2.getId());
      fillTriangleUsingFlags(result, t2);
    }
  }
  catch(Exception e) {
    result.invertOrder();

    if (!fillTriangleUsingFlags(result))
      throw Exception("buildFirstTriangle(1): error during filling triangle");
  }

  return result;
}

void MeshPrune::buildMesh() {
  (*mesh).reorient();
  openMultiTriangleSet.clear();
  nbSeenTriangles = 0;
  MultiTriangle t;

  if (newId != NULL)
    delete [] newId;
  newId = new VertexID[(*mesh).getNbPoints()];
  nbOriginalPoints = (*mesh).getNbPoints();

  for(VertexID * id = newId; id < newId + (*mesh).getNbPoints(); ++id)
    *id = (*mesh).getNbPoints();


  (*mesh).setPointFlag(0); // all points are "not seen"
  (*mesh).setTriangleFlag(0); // all triangles are "not seen"

  resultTriangles.clear();
  resultPoints.clear();

  // build first triangle (setting flags)
  MultiTriangle initialTriangle = buildFirstTriangle();

  addMultiTriangleAndPoints(initialTriangle);

  // add it to the open set
  openMultiTriangleSet.push_front(initialTriangle);

  VertexID idNb = 0;
  while (openMultiTriangleSet.size() != 0) {
    MultiTriangle mt = openMultiTriangleSet.front();

    openMultiTriangleSet.pop_front();
    for(unsigned int i = 0; i < 3; ++i) { // no iterator available
      MultiEdge e = mt.getEdge(i);
      if (i != 1)
	e = e.getInverted();

      if (isOpenEdge(e)) {

	assert((*mesh).point(e.getFirstId()).getFlag() != IN_FINALMESH);
	assert((*mesh).point(e.getLastId()).getFlag() != IN_FINALMESH);

	if (mt.hasForbiddenTriangle(i))
	  t = buildTriangleUsingFlags(e, mt.getForbiddenTriangle(i)); // build first triangle (setting flags)
	else
	  t = buildTriangleUsingFlags(e);

	if (!t.isEmpty()) {
	  addMultiTriangleAndPoints(t);
	  openMultiTriangleSet.push_back(t);
	}
	else if (isQuadrilateralClosure(e)) {
	  addVirtualTrianglesUpdatePointFlags(e);
	}
	else if (isBorderClosure(e)) {
	  addVirtualTriangleOnBorderUpdatePointFlags(e);
	}
	else if (isQuadrilateralBorderClosure(e)) {
	  addVirtualTrianglesOnBorderUpdatePointFlags(e);
	}

	assert(!e.hasMiddlePoint() || ((*mesh).point(e.getMiddleId()).getFlag() != IN_MIDDLE));
      }

    }

    ++idNb;
  }


  // update result ids (in points and triangles)
  VertexID i = 0;
  for(std::deque<Point3D>::iterator it = resultPoints.begin(); it != resultPoints.end(); ++it) {
    (*it).setId(i++);
    for(std::deque<VertexID>::iterator it2 = (*it).getNeighbours().begin(); it2 != (*it).getNeighbours().end(); ++it2)
      *it2 = newId[*it2];
  }

  for(std::deque<MultiTriangle>::iterator t_l = resultTriangles.begin(); t_l != resultTriangles.end(); ++t_l) {
    assert(newId[(*t_l).getP1()] != (*mesh).getNbPoints());
    assert(newId[(*t_l).getP2()] != (*mesh).getNbPoints());
    assert(newId[(*t_l).getP3()] != (*mesh).getNbPoints());
    (*t_l).setP1(newId[(*t_l).getP1()]);
    (*t_l).setP2(newId[(*t_l).getP2()]);
    (*t_l).setP3(newId[(*t_l).getP3()]);
  }
  originalIds = false;

  // then apply to mesh
  (*mesh).clear();
  (*mesh).addPoints(resultPoints);
  (*mesh).addTriangles(resultTriangles, false);

}

Triangle & MeshPrune::findFirstTriangleInside(const std::deque<MultiEdge> & mEdges) {
  if (mEdges.size() < 2)
    throw Exception("findFirstTriangleInside(1): too small list");

  std::deque<MultiEdge>::const_iterator first = mEdges.begin();
  std::deque<MultiEdge>::const_iterator second = ++(mEdges.begin());

  while((first != mEdges.end()) && !isPartOfFinalEdge(*first) && !isPartOfFinalEdge(*second)) {
    first = second;
    ++second;
    if (second == mEdges.end())
      second = mEdges.begin();
  }
  if (first == mEdges.end())
    throw Exception("findFirstTriangleInside(1): wrong order");
  assert((*first).getLastId() == (*second).getFirstId());

  return findFirstTriangleInside(*first, *second);
}

Triangle & MeshPrune::findFirstTriangleInside(const MultiEdge & mEdge1, const MultiEdge & mEdge2) {
  assert(mEdge1.getLastId() == mEdge2.getFirstId());
  // compute first points

  VertexID p1, p2;
  if (!mEdge1.hasMiddlePoint()) {
    p1 = mEdge1.getFirstId();
    p2 = mEdge1.getLastId();
  }
  else if (!mEdge2.hasMiddlePoint()) {
    p1 = mEdge2.getFirstId();
    p2 = mEdge2.getLastId();
  }
  else if (mEdge1.getMiddleId() == mEdge2.getMiddleId()) {
    if (!(*mesh).isBoundaryEdge(mEdge1.getFirstId(), mEdge1.getMiddleId())) {
      p1 = mEdge1.getFirstId();
      p2 = mEdge1.getMiddleId();
    }
    else {
      assert(!(*mesh).isBoundaryEdge(mEdge2.getLastId(), mEdge2.getMiddleId()));
      p1 = mEdge2.getMiddleId();
      p2 = mEdge2.getLastId();
    }
  }
  else {
    assert(!(*mesh).isBoundaryEdge(mEdge1.getMiddleId(), mEdge1.getMiddleId()));
    p1 = mEdge1.getMiddleId();
    p2 = mEdge1.getLastId();
  }

  Triangle & t = (*mesh).findTriangle(p1, p2);
  if (t.sameOrder(p1, p2))
    return t;
  else
    return (*mesh).findOtherTriangle(p1, p2, t);
}



void MeshPrune::addVirtualTriangleOnBorderUpdatePointFlags(const MultiEdge & mEdge) {
  assert(isBorderClosure(mEdge));
  // find the other edge
  MultiEdge m2;
  VertexID middlePoint;
  if ((*mesh).point(mEdge.getFirstId()).getIsBoundary())
   middlePoint = mEdge.getLastId();
  else
   middlePoint = mEdge.getFirstId();
  m2 = findNeighbourBoundaryMEdge(middlePoint, mEdge);

  // build the third
  MultiEdge m3(mEdge.getOtherId(middlePoint), m2.getOtherId(middlePoint));

  // then build the triangle
  MultiTriangle mt;
  if (mEdge.getFirstId() == middlePoint)
    mt = MultiTriangle(mEdge, m2.getOrderedFirst(middlePoint), m3.getOrderedFirst(mEdge.getLastId()));
  else
    mt = MultiTriangle(mEdge, m3.getOrderedFirst(mEdge.getFirstId()), m2.getOrderedFirst(middlePoint));

  // and add it to the result list
  MultiTriangle & mtR = addMultiTriangle(mt);

  // then fill it
  if (mEdge.getFirstId() == middlePoint) {
    Triangle & triangle = findFirstTriangleInside(m2.getOrderedLast(middlePoint), mEdge);
    if (!fillTriangleUsingFlags(mtR, triangle))
      throw Exception("addVirtualTriangleOnBorderUpdatePointFlags(1): error during filling triangle");
  }
  else {
    Triangle & triangle = findFirstTriangleInside(mEdge, m2.getOrderedFirst(middlePoint));
    if (!fillTriangleUsingFlags(mtR, triangle))
      throw Exception("addVirtualTriangleOnBorderUpdatePointFlags(1): error during filling triangle");
  }

  // update points flag in middle
  for(std::deque<TriangleID>::const_iterator it = mtR.getTriangles().begin(); it != mtR.getTriangles().end(); ++it)
    for(unsigned int i = 0; i < 3; ++i)
      if ((*mesh).point((*mesh).triangle(*it).getVertexId(i)).getFlag() == 0)
	(*mesh).point((*mesh).triangle(*it).getVertexId(i)).setFlag(IN_MIDDLE);
}


void MeshPrune::addVirtualTrianglesOnBorderUpdatePointFlags(const MultiEdge & mEdge) {
  VertexID pfirst = mEdge.getFirstId();
  VertexID plast = mEdge.getLastId();
  MultiEdge mEdge1, mEdge2, mEdge3;
  std::deque<MultiEdge> mEdges = getQuadrilateralBorderClosure(mEdge);
  assert(mEdges.size() == 4);

  if (mEdges[0] == mEdge) {
    mEdge1 = mEdges[3];
    mEdge2 = mEdges[1];
    mEdge3 = mEdges[2];
  }
  else if (mEdges[0] == mEdge.getInverted()) {
    mEdge1 = mEdges[1];
    mEdge2 = mEdges[3];
    mEdge3 = mEdges[2];
  }
  else if (mEdges[1] == mEdge) {
    mEdge1 = mEdges[0];
    mEdge2 = mEdges[2];
    mEdge3 = mEdges[3];
  }


  // build multitriangles
  MultiTriangle mt1(mEdge, mEdge1.getOrderedFirst(pfirst), MultiEdge(mEdge.getLastId(), mEdge1.getOtherId(pfirst)));
  MultiTriangle mt2(MultiEdge(mEdge1.getOtherId(pfirst), plast), mEdge3.getOrderedFirst(mEdge1.getOtherId(pfirst)), mEdge2.getOrderedFirst(plast));

  Triangle & t = findFirstTriangleInside(mEdges);
  std::deque<TriangleID> openSet;
  assert(t.getFlag() == 0);
  openSet.push_back(t.getId());

  t.setFlag(1);

  while(openSet.size() != 0) {
    TriangleID currentTriangle = openSet.front();
    openSet.pop_front();
    addTriangleInMultiTriangle((*mesh).triangle(currentTriangle), mt1, mt2);

    for(unsigned int i = 0; i < 3; ++i) { // no iterator available
      // get edge #i
      std::pair<VertexID, VertexID> edge = (*mesh).triangle(currentTriangle).getEdge(i);
      // if this edge is not an edge of the multitriangle
      if (!mEdges[0].hasMinimalEdge(edge.first, edge.second) &&
	  !mEdges[1].hasMinimalEdge(edge.first, edge.second) &&
	  !mEdges[2].hasMinimalEdge(edge.first, edge.second)) {
	try {
	  // find other triangle using this edge
	  Triangle & otherTriangle = (*mesh).findOtherTriangle(edge.first, edge.second, currentTriangle);
	  // if other triangle found
	  if (otherTriangle.getFlag() == 0) { // if has not been seen before
	    // adding triangle to the open set
	    openSet.push_front(otherTriangle.getId());
	    otherTriangle.setFlag(1);
	  }

	}
	catch(Exception e) { }
      }
    }
  }



  // set point flags and build new multitriangles
  addMultiTriangle(mt1);
  addMultiTriangle(mt2);

  // reorder and set border flag
  for(unsigned int idP = 0; idP < 3; ++idP) { // no iterator available
    Point3D & point = resultPoints[newId[(mt1.getVertexId(idP))]];
    if (!isBoundaryVertex(point))
      (*mesh).point(mt1.getVertexId(idP)).setFlag(IN_FINALMESH);
  }

  // set flags for points
  if (mEdge3.hasMiddlePoint()) {
    Point3D & point = (*mesh).point(mEdge3.getMiddleId());
    point.setFlag(UNDER_FINALMESH);
  }


  Point3D & point = resultPoints[newId[(mt2.getVertexId(2))]];
  if (!isBoundaryVertex(point))
    (*mesh).point(mt2.getVertexId(2)).setFlag(IN_FINALMESH);

  for(std::deque<TriangleID>::const_iterator it = mt1.getTriangles().begin(); it != mt1.getTriangles().end(); ++it)
    for(unsigned int i = 0; i < 3; ++i)
      if ((*mesh).point((*mesh).triangle(*it).getVertexId(i)).getFlag() == 0)
	(*mesh).point((*mesh).triangle(*it).getVertexId(i)).setFlag(IN_MIDDLE);
  for(std::deque<TriangleID>::const_iterator it = mt2.getTriangles().begin(); it != mt2.getTriangles().end(); ++it)
    for(unsigned int i = 0; i < 3; ++i)
      if ((*mesh).point((*mesh).triangle(*it).getVertexId(i)).getFlag() == 0)
	(*mesh).point((*mesh).triangle(*it).getVertexId(i)).setFlag(IN_MIDDLE);

}

void MeshPrune::addVirtualTrianglesUpdatePointFlags(const MultiEdge & mEdge) {
  assert(isQuadrilateralClosure(mEdge));
  assert(!isPartOfFinalEdge(mEdge) || isOpenEdge(mEdge));

  // getting the two original points id
  VertexID pfirst = mEdge.getFirstId();
  VertexID plast = mEdge.getLastId();

  const MultiEdge & mEdge1 = findNeighbourBoundaryMEdge(pfirst, mEdge);
  const MultiEdge & mEdge2 = findNeighbourBoundaryMEdge(plast, mEdge);
  MultiEdge mEdge3;
  assert(!isPartOfFinalEdge(mEdge1) || isOpenEdge(mEdge1));
  assert(!isPartOfFinalEdge(mEdge2) || isOpenEdge(mEdge2));


  assert(mEdge1.hasPointNotMiddle(pfirst));
  assert(mEdge2.hasPointNotMiddle(plast));


  // find the two points (next and pred)
  Point3D & p1 = resultPoints[newId[mEdge1.getOtherId(mEdge.getFirstId())]];
  Point3D & p2 = resultPoints[newId[mEdge2.getOtherId(mEdge.getLastId())]];

  // then find the last edge
  bool find = false;
  for(std::deque<TriangleID>::const_iterator t = p1.getTriangles().begin(); t != p1.getTriangles().end(); ++t) {
    assert(*t < resultTriangles.size());
    MultiTriangle & mt = resultTriangles[*t];
    if (mt.hasPoint(p2)) {
      mEdge3 = mt.getEdge(p1.getId(), p2.getId());
      assert(!isPartOfFinalEdge(mEdge3) || isOpenEdge(mEdge3));
      find = true;
      break;
    }
  }
  if (!find)
    throw Exception("addVirtualTrianglesUpdatePointFlags(1): cannot find the fourth edge.");


  // build multitriangles
  MultiTriangle mt1(mEdge, mEdge1.getOrderedFirst(pfirst), MultiEdge(mEdge.getLastId(), mEdge1.getOtherId(pfirst)));
  MultiTriangle mt2(MultiEdge(mEdge1.getOtherId(pfirst), plast), mEdge3.getOrderedFirst(mEdge1.getOtherId(pfirst)), mEdge2.getOrderedFirst(plast));

  // set triangle flags
  if (hasTriangleInside(mEdge, mEdge1, mEdge2, mEdge3)) {
    Triangle & t = findFirstTriangleInside(mEdge1.getOrderedLast(pfirst), mEdge);
    assert(t.getFlag() == 0);
    std::deque<TriangleID> openSet;
    openSet.push_back(t.getId());
    t.setFlag(1);

    while(openSet.size() != 0) {
      TriangleID currentTriangle = openSet.front();
      openSet.pop_front();
      addTriangleInMultiTriangle((*mesh).triangle(currentTriangle), mt1, mt2);

      for(unsigned int i = 0; i < 3; ++i) { // no iterator available
	// get edge #i
	std::pair<VertexID, VertexID> edge = (*mesh).triangle(currentTriangle).getEdge(i);
	// if this edge is not an edge of the multitriangle
	if (!mEdge.hasMinimalEdge(edge.first, edge.second) &&
	    !mEdge1.hasMinimalEdge(edge.first, edge.second) &&
	    !mEdge2.hasMinimalEdge(edge.first, edge.second) &&
	    !mEdge3.hasMinimalEdge(edge.first, edge.second)) {
	  try {
	    // find other triangle using this edge
	    Triangle & otherTriangle = (*mesh).findOtherTriangle(edge.first, edge.second, currentTriangle);
	    // if other triangle found
	    if (otherTriangle.getFlag() == 0) { // if has not been seen before
	      // adding triangle to the open set
	      openSet.push_front(otherTriangle.getId());
	      otherTriangle.setFlag(1);
	    }

	  }
	  catch(Exception e) { }
	}
      }
    }
  }


  // set point flags and build new multitriangles
  addMultiTriangle(mt1);
  addMultiTriangle(mt2);

  // reorder and set border flag
  for(unsigned int idP = 0; idP < 3; ++idP) { // no iterator available
    Point3D & point = resultPoints[newId[(mt1.getVertexId(idP))]];
    if (!isBoundaryVertex(point))
      (*mesh).point(mt1.getVertexId(idP)).setFlag(IN_FINALMESH);
  }

  // set flags for points
  if (mEdge3.hasMiddlePoint()) {
    Point3D & point = (*mesh).point(mEdge3.getMiddleId());
    point.setFlag(UNDER_FINALMESH);
  }


  Point3D & point = resultPoints[newId[(mt2.getVertexId(2))]];
  if (!isBoundaryVertex(point))
    (*mesh).point(mt2.getVertexId(2)).setFlag(IN_FINALMESH);

  for(std::deque<TriangleID>::const_iterator it = mt1.getTriangles().begin(); it != mt1.getTriangles().end(); ++it)
    for(unsigned int i = 0; i < 3; ++i)
      if ((*mesh).point((*mesh).triangle(*it).getVertexId(i)).getFlag() == 0)
	(*mesh).point((*mesh).triangle(*it).getVertexId(i)).setFlag(IN_MIDDLE);
  for(std::deque<TriangleID>::const_iterator it = mt2.getTriangles().begin(); it != mt2.getTriangles().end(); ++it)
    for(unsigned int i = 0; i < 3; ++i)
      if ((*mesh).point((*mesh).triangle(*it).getVertexId(i)).getFlag() == 0)
	(*mesh).point((*mesh).triangle(*it).getVertexId(i)).setFlag(IN_MIDDLE);


}

void MeshPrune::addTriangleInMultiTriangle(const Triangle & t,
					   MultiTriangle & mt1,
					   MultiTriangle & mt2) {
  Coord3D isBt = (*mesh).getIsobarycenter(t);
  Coord3D isBmt1 = (*mesh).getIsobarycenter(mt1);
  Coord3D isBmt2 = (*mesh).getIsobarycenter(mt2);

  if (isBt.distance(isBmt1) < isBt.distance(isBmt2))
    mt1.addTriangle(t.getId());
  else
    mt2.addTriangle(t.getId());
}


bool MeshPrune::hasTriangleInside(const MultiEdge & mEdge1,
			       const MultiEdge & mEdge2,
			       const MultiEdge & mEdge3,
			       const MultiEdge & mEdge4) const {
  // first check for simple edges
  if (!mEdge1.hasMiddlePoint() || !mEdge2.hasMiddlePoint() ||
      !mEdge3.hasMiddlePoint() || !mEdge4.hasMiddlePoint())
    return true;
  assert(mEdge1.hasMiddlePoint() && mEdge2.hasMiddlePoint() &&
	 mEdge3.hasMiddlePoint() && mEdge4.hasMiddlePoint());
  // else, there is no simple edge, check for star configuration
  if ((mEdge1.getMiddleId() == mEdge2.getMiddleId()) &&
      (mEdge2.getMiddleId() == mEdge3.getMiddleId()) &&
      (mEdge3.getMiddleId() == mEdge4.getMiddleId()) &&
      (mEdge4.getMiddleId() == mEdge1.getMiddleId()))
    return false;
  else
    return true;
}

MultiTriangle & MeshPrune::addMultiTriangle(const MultiTriangle & mt) {
  assert(!isPartOfFinalEdge(mt.getEdge(0)) || isOpenEdge(mt.getEdge(0)));
  assert(!isPartOfFinalEdge(mt.getEdge(1)) || isOpenEdge(mt.getEdge(1)));
  assert(!isPartOfFinalEdge(mt.getEdge(2)) || isOpenEdge(mt.getEdge(2)));

  resultTriangles.push_back(mt);
  MultiTriangle & mtR = resultTriangles.back();
  mtR.setId(resultTriangles.size() - 1);

  for(unsigned int i = 0; i < 3; ++i)
    resultPoints[newId[mt.getVertexId(i)]].addTriangle(mtR);

  nbSeenTriangles += mtR.getTriangles().size();

  return mtR;
}

void MeshPrune::addMultiTriangleAndPoints(const MultiTriangle & triangle) {
  // adding the triangle to the list
  resultTriangles.push_back(triangle);
  resultTriangles.back().setId(resultTriangles.size() - 1);

  nbSeenTriangles += resultTriangles.back().getTriangles().size();

  for(unsigned int idE = 0; idE < 3; ++idE) { // no iterator available
    const MultiEdge & e = triangle.getEdge(idE);
    if (e.hasMiddlePoint())
      (*mesh).point(e.getMiddleId()).setFlag(UNDER_FINALMESH);
  }

  // add points to the result list
  for(unsigned int idP = 0; idP < 3; ++idP) { // no iterator available
    Point3D & point = (*mesh).point(triangle.getVertexId(idP));

    int flag = point.getFlag();
    if (flag == 0) {
      newId[point.getId()] = resultPoints.size();
      resultPoints.push_back(point);
      Point3D & newPoint = resultPoints.back();
      newPoint.clearNeighboursAndTriangles();
      newPoint.addTriangle(resultTriangles.back());
    }
#ifndef NDEBUG
    else {
      assert((flag == IN_FINALMESH) || (flag == IN_FINALMESH_B));
    }
#endif
  }

  // add triangles
  for(unsigned int idP = 0; idP < 3; ++idP) { // no iterator available
    Point3D & point = (*mesh).point(triangle.getVertexId(idP));

    int flag = point.getFlag();

    assert(flag != IN_FINALMESH);
    if (flag == 0)
      point.setFlag(IN_FINALMESH_B);
    else {
      assert(flag == IN_FINALMESH_B);
      assert(newId[point.getId()] != (*mesh).getNbPoints());
      assert(newId[point.getId()] < resultPoints.size());
      assert(resultTriangles.size() != 0);
      Point3D & newPoint = resultPoints[newId[point.getId()]];
      newPoint.addTriangle(resultTriangles.back());

      assert(newPoint.getNbNeighbours() != 0);
    }
  }

  // update flags
  for(unsigned int idP = 0; idP < 3; ++idP) { // no iterator available
    Point3D & point = resultPoints[newId[(triangle.getVertexId(idP))]];
    if (!isBoundaryVertex(point))
      (*mesh).point(triangle.getVertexId(idP)).setFlag(IN_FINALMESH);
  }

  // update points flag in middle
  for(std::deque<TriangleID>::const_iterator it = triangle.getTriangles().begin(); it != triangle.getTriangles().end(); ++it)
    for(unsigned int i = 0; i < 3; ++i)
      if ((*mesh).point((*mesh).triangle(*it).getVertexId(i)).getFlag() == 0)
	(*mesh).point((*mesh).triangle(*it).getVertexId(i)).setFlag(IN_MIDDLE);
}

MultiEdge MeshPrune::getMEdge(VertexID p1, VertexID p2) {
  if ((newId[p1] == (*mesh).getNbPoints()) ||
      (newId[p2] == (*mesh).getNbPoints()))
    throw Exception();
  else {
    assert(newId[p1] < resultPoints.size());
    Point3D & p = resultPoints[newId[p1]];
    if (!p.hasNeighbour(p2))
      throw Exception();
    else {
      for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t) {
	assert(*t < resultTriangles.size());
	MultiTriangle & mt = resultTriangles[*t];
	if (mt.hasPoint(p2))
	  return mt.getEdge(p1, p2).getOrdered(p1, p2);
      }
      assert(false);
      throw Exception();
    }
  }
}


std::deque<VertexID> MeshPrune::computeOpenNeighbours(Point3D & point) {
  assert((resultPoints.size() == 0) || (point.getFlag() == IN_FINALMESH_B));
  std::deque<VertexID> result;

  point.incFlag(IN_PROGRESS);


  // compute list of available points
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin();
      nb != point.getNeighbours().end(); ++nb) {
    int nbFlag = (*mesh).point(*nb).getFlag();
    (*mesh).point(*nb).incFlag(IN_PROGRESS);
    if ((nbFlag < IN_PROGRESS) && (nbFlag != IN_FINALMESH) && (nbFlag != UNDER_FINALMESH) && (nbFlag != IN_MIDDLE))
      result.push_front(*nb);

    for(std::deque<VertexID>::const_iterator nb2 = (*mesh).point(*nb).getNeighbours().begin();
	nb2 != (*mesh).point(*nb).getNeighbours().end(); ++nb2) {
      int nb2Flag = (*mesh).point(*nb2).getFlag();
      if (nb2Flag < IN_PROGRESS) {
	(*mesh).point(*nb2).incFlag(IN_PROGRESS);
	if ((nb2Flag != UNDER_FINALMESH) && (nb2Flag != IN_FINALMESH) && (nb2Flag != IN_MIDDLE))
	  result.push_front(*nb2);
      }
    }
  }

  // reset flags values
  point.incFlag(- IN_PROGRESS);

  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin();
      nb != point.getNeighbours().end(); ++nb) {
    if ((*mesh).point(*nb).getFlag() >= IN_PROGRESS)
      (*mesh).point(*nb).incFlag(- IN_PROGRESS);

    for(std::deque<VertexID>::const_iterator nb2 = (*mesh).point(*nb).getNeighbours().begin();
	nb2 != (*mesh).point(*nb).getNeighbours().end(); ++nb2)
      if ((*mesh).point(*nb2).getFlag() >= IN_PROGRESS)
	(*mesh).point(*nb2).incFlag(- IN_PROGRESS);
  }

#ifndef NDEBUG
  for(std::deque<VertexID>::const_iterator r = result.begin(); r != result.end(); ++r)
    assert((*mesh).point(*r).getFlag() != UNDER_FINALMESH);
#endif
  return result;
}


bool MeshPrune::isAllowedMultiEdgeInNewTriangle(const MultiEdge & mEdge) const {
  if (isOpenEdge(mEdge))
    return true;
  else if (isPartOfFinalEdge(mEdge))
    return false;
  else if (mEdge.hasMiddlePoint()) {
    if (!(*mesh).point(mEdge.getFirstId()).getIsBoundary() &&
	(*mesh).point(mEdge.getMiddleId()).getIsBoundary() &&
	!(*mesh).point(mEdge.getLastId()).getIsBoundary())
      return false;
    return (isAllowedEdgeInNewTriangle(mEdge.getFirstId(), mEdge.getMiddleId()) && isAllowedEdgeInNewTriangle(mEdge.getLastId(), mEdge.getMiddleId())) ||
      ((*mesh).isBoundaryEdge(mEdge.getFirstId(), mEdge.getMiddleId()) && (*mesh).isBoundaryEdge(mEdge.getLastId(), mEdge.getMiddleId()));
  }
  else
    return isAllowedEdgeInNewTriangle(mEdge.getFirstId(), mEdge.getLastId()) || (*mesh).isBoundaryEdge(mEdge.getFirstId(), mEdge.getLastId());
}

bool MeshPrune::isAllowedEdgeInNewTriangle(VertexID idP1, VertexID idP2) const {
  if (!(*mesh).point(idP1).hasNeighbour(idP2))
    return false;
  int flag1 = (*mesh).point(idP1).getFlag();
  int flag2 = (*mesh).point(idP2).getFlag();

  if ((flag1 == IN_MIDDLE) || (flag2 == IN_MIDDLE))
    return false;
  if ((flag1 == NOT_SEEN) || (flag2 == NOT_SEEN))
    return true;
  else if ((flag1 == IN_FINALMESH) || (flag2 == IN_FINALMESH))
    return false;
  else if ((flag1 == UNDER_FINALMESH) && (flag2 == UNDER_FINALMESH))
    return false;

  assert(((flag1 == UNDER_FINALMESH) || (flag1 == IN_FINALMESH_B)) && ((flag2 == UNDER_FINALMESH) || (flag2 == IN_FINALMESH_B)));

  try {
    Triangle & t1 = (*mesh).findTriangle(idP1, idP2);

    if (t1.getFlag() == 0)
      return true;

    try {
      Triangle & t2 = (*mesh).findOtherTriangle(idP1, idP2, t1);
      if (t2.getFlag() == 0)
	return true;
      else
	return isPartOfFinalAndOpenEdge(idP1, idP2);
    }
    catch (Exception e) {
      // only one triangle has been found: we are in a boundary
      if ((flag1 == NOT_SEEN) || (flag2 == NOT_SEEN))
	return false;
      return isPartOfFinalAndOpenEdge(idP1, idP2); // TO FIX
    }
  }
  catch (Exception e) {
    return isPartOfFinalAndOpenEdge(idP1, idP2);
  }
}

std::list<std::pair<MultiEdge, MultiEdge> > MeshPrune::computeMatchingTriangles(const MultiEdge & mEdge, VertexID otherPoint) {
  std::list<std::pair<MultiEdge, MultiEdge> > result;
  std::deque<MultiEdge> firstEdges;
  std::deque<MultiEdge> secondEdges;

  // first compute neighbours for the first point of mEdge
  try {
    const MultiEdge & me = getMEdge(mEdge.getFirstId(), otherPoint);
    if (isOpenEdge(me))
      firstEdges.push_front(me);
  }
  catch (Exception e) {
    if ((*mesh).point(mEdge.getFirstId()).hasNeighbour(otherPoint)) {
      MultiEdge me(mEdge.getFirstId(), otherPoint);
      if (isAllowedMultiEdgeInNewTriangle(me))
	firstEdges.push_front(me);
    }
    else
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(mEdge.getFirstId()).getNeighbours().begin();
	  nb != (*mesh).point(mEdge.getFirstId()).getNeighbours().end(); ++nb) {
	const Point3D & pt = (*mesh).point(*nb);
	if (pt.hasNeighbour(otherPoint))
	  if ((pt.getFlag() != IN_FINALMESH) && (pt.getFlag() != IN_FINALMESH_B)) {
	    MultiEdge me(mEdge.getFirstId(), otherPoint, pt.getId());
	    if (isAllowedMultiEdgeInNewTriangle(me))
	      firstEdges.push_front(me);
	  }
      }
  }

  // then compute neighbours for the second point of mEdge
  try {
    const MultiEdge & me = getMEdge(mEdge.getLastId(), otherPoint);
    if (isOpenEdge(me))
      secondEdges.push_front(me);
  }
  catch (Exception e) {
    if ((*mesh).point(mEdge.getLastId()).hasNeighbour(otherPoint)) {
      MultiEdge me(mEdge.getLastId(), otherPoint);
      if (isAllowedMultiEdgeInNewTriangle(me))
	secondEdges.push_front(me);
    }
    else
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(mEdge.getLastId()).getNeighbours().begin();
	  nb != (*mesh).point(mEdge.getLastId()).getNeighbours().end(); ++nb) {
	const Point3D & pt = (*mesh).point(*nb);
	if (pt.hasNeighbour(otherPoint))
	  if ((pt.getFlag() != IN_FINALMESH) && (pt.getFlag() != IN_FINALMESH_B)) {
	    MultiEdge me(mEdge.getLastId(), otherPoint, pt.getId());
	    if (isAllowedMultiEdgeInNewTriangle(me))
	      secondEdges.push_front(me);
	  }
      }
  }


  // then build list of triangles
  for(std::deque<MultiEdge>::const_iterator it = firstEdges.begin(); it != firstEdges.end(); ++it)
    for(std::deque<MultiEdge>::const_iterator it2 = secondEdges.begin(); it2 != secondEdges.end(); ++it2)
      result.push_back(std::pair<MultiEdge, MultiEdge>(*it, *it2));

  return result;
}


bool MeshPrune::isPartOfFinalEdge(const MultiEdge & mEdge) const {
  if((newId[mEdge.getFirstId()] == (*mesh).getNbPoints()) || (newId[mEdge.getLastId()] == (*mesh).getNbPoints()))
    return false;

  const Point3D & point = resultPoints[newId[mEdge.getFirstId()]];

  for(std::deque<TriangleID>::const_iterator i = point.getTriangles().begin();
      i != point.getTriangles().end(); ++i)
    if (resultTriangles[*i].hasEdge(mEdge))
      return true;

  return false;
}


/*std::deque<MultiEdge> MeshPrune::getListOfFinalEdges(VertexID idP1, VertexID idP2) const {
  std::deque<MultiEdge> result;

  if((newId[idP1] == mesh.getNbPoints()) && (newId[idP2] == mesh.getNbPoints()))
    return result;
  if(newId[idP1] == mesh.getNbPoints())
    return isPartOfFinalEdge(idP2, idP1);
  const Point3D & point = resultPoints[newId[idP1]];

  for(std::deque<TriangleID>::const_iterator i = point.getTriangles().begin();
      i != point.getTriangles().end(); ++i)
    if (resultTriangles[*i].hasMinimalEdge(idP1, idP2)) {
      std::deque<MultiEdge>
      return result.push_back();
    }

  return result;
  }*/


bool MeshPrune::isPartOfFinalEdge(VertexID idP1, VertexID idP2) const {
  if((newId[idP1] == (*mesh).getNbPoints()) && (newId[idP2] == (*mesh).getNbPoints()))
    return false;
  if(newId[idP1] == (*mesh).getNbPoints())
    return isPartOfFinalEdge(idP2, idP1);
  const Point3D & point = resultPoints[newId[idP1]];

  for(std::deque<TriangleID>::const_iterator i = point.getTriangles().begin();
      i != point.getTriangles().end(); ++i)
    if (resultTriangles[*i].hasMinimalEdge(idP1, idP2))
      return true;

  return false;
}


bool MeshPrune::isPartOfFinalAndOpenEdge(VertexID idP1, VertexID idP2) const {
  if((newId[idP1] == (*mesh).getNbPoints()) && (newId[idP2] == (*mesh).getNbPoints()))
    return false;
  if(newId[idP1] == (*mesh).getNbPoints())
    return isPartOfFinalAndOpenEdge(idP2, idP1);
  const Point3D & point = resultPoints[newId[idP1]];

  std::list<VertexID> mEdges;

  for(std::deque<TriangleID>::const_iterator i = point.getTriangles().begin();
      i != point.getTriangles().end(); ++i)
    if (resultTriangles[*i].hasMinimalEdge(idP1, idP2))
      if (isOpenEdge(resultTriangles[*i].getEdgeContaining(idP1, idP2)))
	return true;

  return false;
}


std::deque<MultiTriangle> & MeshPrune::getMultiTriangles() {
  if (!originalIds)
    setOriginalIds();

  return resultTriangles;
}

void MeshPrune::setOriginalIds() {
  if(!originalIds) {
    // compute oldId index
    VertexID * oldId = new VertexID[resultPoints.size()];
    VertexID * idId = oldId;
    for(VertexID i = 0; i < resultPoints.size(); ++i, ++idId)
      *idId = nbOriginalPoints;

    for(VertexID i = 0; i < nbOriginalPoints; ++i)
      if (newId[i] != nbOriginalPoints) {
	assert(newId[i] < resultPoints.size());
	oldId[newId[i]] = i;
      }


    for(std::deque<MultiTriangle>::iterator t = resultTriangles.begin(); t != resultTriangles.end(); ++t) {
      assert(oldId[(*t).getP1()] != nbOriginalPoints);
      assert(oldId[(*t).getP2()] != nbOriginalPoints);
      assert(oldId[(*t).getP3()] != nbOriginalPoints);
      (*t).setP1(oldId[(*t).getP1()]);
      (*t).setP2(oldId[(*t).getP2()]);
      (*t).setP3(oldId[(*t).getP3()]);
    }

    delete [] oldId;
    originalIds = true;
  }
}
