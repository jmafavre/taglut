/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef MESH_SEGMENTATION
#define MESH_SEGMENTATION

#include "MeshMap.h"

namespace Taglut {
  class Mesh;
  class MeshManipulator;

  /**
     @class MeshSegmentation
     Mesh segmentation
  */
  class MeshSegmentation : public MeshManipulator {
  private:
    MeshMap mMap;

  public:
    /** Default constructor */
    MeshSegmentation(Mesh & mesh_t) : MeshManipulator(mesh_t), mMap(mesh_t) { }

    /** copy constructor */
    MeshSegmentation(MeshSegmentation & pcut) : MeshManipulator(pcut), mMap(*(pcut.mesh)) { }


    /** segmentation using the rapprochement method
	@param borders List of boundary points
	@param flagInside Value of the flags carred by the vertices inside the area to be segmented (should be positive). Outside flags should be != -2
	@param withVoronoi If true, the Voronoi segmentation is applied after the rapprochement method to segment the non segmented points
	@param withCC If false, do not complete the segmentation using the closest CC
	@return The resulting MeshMap (-2: segmentation unknown)
    */
    MeshMap & segmentationByRapprochement(const std::vector<std::vector<VertexID> > & borders, int flagInside = 0, bool withVoronoi = true, bool withCC = true);

    /** segmentation using the Voronoï method
	@param sites List of site points
	@param flagInside Value of the flags carred by the vertices inside the area to be segmented
	@param offset Starting value for the output flags
	@return The resulting MeshMap
    */
    MeshMap & segmentationByVoronoi(const std::vector<std::vector<VertexID> > & sites, int flagInside = -2, int offset = 0);

  };

}

#endif
