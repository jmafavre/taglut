/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SIMULATED_ANNEALING_NLOOP
#define SIMULATED_ANNEALING_NLOOP

#include "MeshPathes.h"
#include "RandomGenerator.h"
#include "RandomVertexSelector.h"


namespace Taglut {
  class Mesh;
  class NLoop;
  class NLoopEvaluator;
  class LengthEdge;

  /**
     @class SimulatedAnnealingNLoop
     Simulated Annealing for NLoop computation
  */
  class SimulatedAnnealingNLoop : public MeshPathes {
  public:
    /**
       available methods to select next neighbour */
    enum SAMethod {
      SASimple, SAByMiddle
    };

  protected:
    /** random generator to select next step */
    RandomGeneratorUniform randgen;
    /** random generator to select first point with random method */
    RandomVertexSelectorUniform randvect;

    /** initial temperature */
    double temp0;
    /** number of steps between two temperature modification */
    unsigned int step_size;
    /** coefficient of the geometric progression */
    double temp_coeff;
    /** accepted threshold */
    double accept_threshold;
    /** minimum value for temperature */
    double temp_min;
    /** selection method */
    enum SAMethod method;
    /** return the global shortest rather than the last shortest NLoop */
    bool returnShortest;

    /** an object that describe the way to evaluate and compare n-loops */
    const NLoopEvaluator & nLEvaluator;

    /**
       Given an NLoop, computes a near NLoop that will be the next used by the simulated annealing approach.
       The neighbours are defined by selecting the neighbours of the base points.
     */
    std::pair<VertexID, VertexID> getNextNLoopBasePointsSimple(const NLoop & nloop) const;

    /**
       Given an NLoop, computes a near NLoop that will be the next used by the simulated annealing approach.
       The neighbours are defined by using the middle of each path to compute the new base points.
     */
    std::pair<VertexID, VertexID> getNextNLoopBasePointsByMiddle(const NLoop & nloop);

    /**
       Compute the arity of the next NLoop
    */
    unsigned int getNextArity(unsigned int nbPaths, unsigned int nbPathsMin, unsigned int nbPathsMax);

   public:
    /**
       Default constructor
    */
    SimulatedAnnealingNLoop(Mesh & mesh, const NLoopEvaluator & nLEvaluator, double temp0 = 5., unsigned int step_size = 100, double temp_coeff = 0.9, double accept_threshold = .01, double temp_min = 1e-3, enum SAMethod method = SAByMiddle, bool returnShortest = true);

    /**
       Default constructor with length operator
    */
    SimulatedAnnealingNLoop(Mesh & mesh, const NLoopEvaluator & nLEvaluator, const LengthEdge & lMethod, double temp0 = 5., unsigned int step_size = 100, double temp_coeff = 0.9, double accept_threshold = .01, double temp_min = 1e-3, enum SAMethod method = SAByMiddle, bool returnShortest = true);

    /**
       Copy constructor
    */
    SimulatedAnnealingNLoop(const SimulatedAnnealingNLoop & saNLoop);

    /**
       Copy constructor with other mesh
    */
    SimulatedAnnealingNLoop(const SimulatedAnnealingNLoop & saNLoop, Mesh & mesh);

    /**
       Destructor
    */
    virtual inline ~SimulatedAnnealingNLoop() { }


    /**
       Set parameters of the simulated annealing method
    */
    inline void setParameters(double temp0_t = 5., unsigned int step_size_t = 20, double temp_coeff_t = 0.9, double accept_threshold_t = .1) {
      (*this).temp0 = temp0_t;
      (*this).step_size = step_size_t;
      (*this).temp_coeff = temp_coeff_t;
      (*this).accept_threshold = accept_threshold_t;
    }


    /**
       Set parameters of the simulated annealing method
    */
    inline void setSelectionMethod(enum SAMethod method_t = SAByMiddle) {
      (*this).method = method_t;
    }

    /**
       Given a number of paths and two starting basepoints, compute an approximation
       of the shortest valid NLoop with this given arity (simulated annealing).
       VertexID == -1 for non seleted points
     */
    NLoop approximateShortestValidNLoopArityFixed(unsigned int nbPaths,
						  VertexID bb1 = std::numeric_limits<VertexID>::max(),
						  VertexID bb2 = std::numeric_limits<VertexID>::max());


    /**
       Given a range for the number of paths and two starting basepoints,
       compute an approximation of the shortest valid NLoop with this given
       arity (simulated annealing).
       VertexID == -1 for non seleted points
     */
    NLoop approximateShortestValidNLoop(unsigned int nbPathsMin, unsigned int nbPathsMax,
					VertexID bb1 = std::numeric_limits<VertexID>::max(),
					VertexID bb2 = std::numeric_limits<VertexID>::max());


  };
}

#endif
