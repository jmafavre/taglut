/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VERTICES_MANIPULATOR
#define VERTICES_MANIPULATOR

#include "IDTypes.h"

namespace Taglut {
  class Vertex;

  /**
     @class VerticesManipulator

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-05-31
     @brief Descriptor for cluster in mesh
  */

  class VerticesManipulator {

  protected:
    /** list of vertices manipulated */
    Vertex * vertices;
    /** number pof vertices in the list */
    unsigned int nbVertices;

    /**
       Compute the connected component of idV
       @param idV Id of the first vertex
       @param iFlag Input flag value
       @param fFlag Final flag value
    */
    void computeCC(VertexID idV, int iFlag, int fFlag);

  public:

    /**
       Default constructor
    */
    VerticesManipulator();

    /**
       Copy constructor
    */
    VerticesManipulator(const VerticesManipulator & vm);

#ifndef SWIG
    /**
       Copy operator
    */
    VerticesManipulator & operator=(const VerticesManipulator & vm);
#endif

    /**
       real constructor
    */
    VerticesManipulator(Vertex * vertices, VertexID nbVertices);

    /**
       set data
    */
    void setData(Vertex * vertices, VertexID nbVertices);

    /**
       Clear data
    */
    void clear();

  };
}

#endif
