/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GRAPH
#define GRAPH

#include <list>

#include "Node.h"

namespace Taglut {
  /**
     @class Graph

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2007-05-31
     @brief A graph as a list of \c Node
  */
  class Graph : public std::deque<Node> {
  private:


  public:

    /** Default constructor */
    Graph();

    /** Copy constructor */
    Graph(const Graph & g);


    /** add a node to the current graph */
    Node & addNode(int flag = 0);

    /** add an edge to the current graph */
    void addEdge(const NodeID & n1, const NodeID & n2);

    /** add an edge to the current graph */
    void addEdge(Node & n1, Node & n2);

    /** get the list of the edges */
    std::deque<std::pair<NodeID, NodeID> > getEdges() const;

  };
}

#endif
