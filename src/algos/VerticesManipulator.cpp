/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <assert.h>
#include "VerticesManipulator.h"
#include "Vertex.h"

using namespace Taglut;

VerticesManipulator::VerticesManipulator() {
  vertices = NULL;
  nbVertices = 0;
}

VerticesManipulator::VerticesManipulator(const VerticesManipulator & vm) {
  (*this) = vm;
}

VerticesManipulator & VerticesManipulator::operator=(const VerticesManipulator & vm) {
  vertices = vm.vertices;
  nbVertices = vm.nbVertices;

  return *this;
}


VerticesManipulator::VerticesManipulator(Vertex * vertices_l, VertexID nbVertices_l) : vertices(vertices_l), nbVertices(nbVertices_l) {
  assert((vertices_l != NULL) && (nbVertices_l > 0));
}

void VerticesManipulator::computeCC(VertexID idV, int iFlag, int fFlag) {
  if (idV >= nbVertices)
    return;
  else if (iFlag == vertices[idV].getFlag()) {
    vertices[idV].setFlag(fFlag);
    for(std::deque<VertexID>::const_iterator inb = vertices[idV].getNeighbours().begin();
	inb != vertices[idV].getNeighbours().end(); ++inb)
      computeCC(*inb, iFlag, fFlag);
  }
}

void VerticesManipulator::setData(Vertex * vertices_l, VertexID nbVertices_l) {
  clear();
  (*this).vertices = vertices_l;
  (*this).nbVertices = nbVertices_l;
}


void VerticesManipulator::clear() {
  nbVertices = 0;
  if (vertices != NULL) {
    delete[] vertices;
    vertices = NULL;
  }
}
