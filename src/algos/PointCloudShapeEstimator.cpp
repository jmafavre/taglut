/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <sys/times.h>
#include <algorithm>

#include "PointCloudShapeEstimator.h"
#include "Exception.h"
#include "FileExceptions.h"
#include "Mesh.h"
#include "MeshManipulator.h"
#include "Display3D.h"
#include "DisplayGL.h"
#include "MeshMap.h"
#include "Matrix.h"
#include "PointCloudOctree.h"

using namespace Taglut;

/** a comparator using the center of a point cloud */
struct CenterComparator {
  /** the manipulated point cloud */
  const PointCloud * pCloud;
  /* the center */
  const Point3D * center;
  /* compare the distance from the center */
  bool operator()(const VertexID & p1, const VertexID & p2) const {
    return (*center).distance((*pCloud)[p1]) < (*center).distance((*pCloud)[p2]);
  }
};

/** a comparator using distances */
struct DistanceComparator {
  /** the manipulated distances */
  const std::vector<double> * distance;
  /** the order accessor */
  bool operator()(const VertexID & p1, const VertexID & p2) const {
    return (*distance)[p1] < (*distance)[p2];
  }
};

PointCloudShapeEstimator::PointCloudShapeEstimator(PointCloud & pCloud_t, double sphere_resolution_t,
						   bool useOctree_t, const enum NormalMethod & nMethod_t,
						   unsigned int maxNbInOctree_t, unsigned int octreeDepth_t) : pCloud(pCloud_t),
													       octree(pCloud_t, maxNbInOctree_t, octreeDepth_t),
													       sphere_resolution(sphere_resolution_t),
													       rMethod(RMEstimation),
													       nMethod(nMethod_t), useOctree(useOctree_t) {

  setAngleErrorOnBoundary();
  setNbPointsOnShpereEdges();
  if (rMethod == RMEstimation)
    radiusByEstimation();
  else if (rMethod == RMNbNeighbours)
    radiusByNbNeighbours();
  else // if (rMethod == RMValue)
    radiusByValue();
}

void PointCloudShapeEstimator::setNbPointsOnShpereEdges(unsigned int nbPointsOnSphereEdges_t) {
  clearNormalData();
  (*this).nbPointsOnSphereEdges = nbPointsOnSphereEdges_t;
}


void PointCloudShapeEstimator::setAngleErrorOnBoundary(double angle_error_t) {
  clearBoundaryData();
  (*this).angle_error = angle_error_t;
}

void PointCloudShapeEstimator::radiusByEstimation(double ratio_t, double maxAngle_t) {
  clearData();
  rMethod = RMEstimation;
  (*this).ratio = ratio_t;
  (*this).maxAngle = maxAngle_t;
}

void PointCloudShapeEstimator::radiusByValue(double value_t) {
  clearData();
  rMethod = RMValue;
  (*this).value_sphere = value_t;
}

void PointCloudShapeEstimator::radiusByNbNeighbours(unsigned int number) {
  clearData();
  rMethod = RMNbNeighbours;
  (*this).number_neighbours = number;
}

void PointCloudShapeEstimator::clearData() {
  radii.clear();
  neighbours.clear();
  clearNormalData();
  clearBoundaryData();
}

void PointCloudShapeEstimator::clearBoundaryData() {
  bNeighgours.clear();
  boundaryFlags.clear();
  localBoundaryDistortions.clear();
}

void PointCloudShapeEstimator::clearNormalData() {
  pCloud.getNormals().clear();
  normalsBis.clear();
  localDistortions.clear();
  clearBoundaryData();
}

double PointCloudShapeEstimator::computeInsideOutsideDistance(const Point3D & center, const Coord3D & vector, double maxAngle_t) {
  double result = std::numeric_limits<double>::max();
  const double maxDist = cos(maxAngle_t);
  const double norm = 1.0; //vector.norm();
  assert(fabs(vector.norm() -  1.) <= 0.0000001);
  assert(maxDist >= 0.0);
  const VertexID cId = center.getId();
  const PointCloud::const_iterator pcEnd = pCloud.end();
  for(PointCloud::const_iterator otherCenter = pCloud.begin(); otherCenter != pcEnd; ++otherCenter) {
    const double d1 = center.distance(*otherCenter);
    if (((*otherCenter).getId() != cId) && (d1 < 2 * result)) {
      const Coord3D n1 = *otherCenter - center;
      double sproduct = (n1.getX() * vector.getX() + n1.getY() * vector.getY() + n1.getZ() * vector.getZ()) / (n1.norm() * norm);
      if (sproduct > 1.)
	sproduct = 1.;
      if (sproduct > maxDist) { // if alpha >= M_PI / 2, no point on the vector can be both into the two spheres
	const double d = d1 / sproduct / 2;
	if (d < result) {
	  result = d;
	}
      }
    }
  }
  return result;
}

const Coord3D PointCloudShapeEstimator::computeFirstEdgePoint(const Coord3D & A, const Coord3D & B, const Coord3D & C, double radius) {
  const double epsilon = radius * 0.0000001;
  if (fabs(A.getX() - B.getX()) < epsilon)
    return Coord3D(C.getX() + radius, C.getY(), C.getZ());

  double trans = 0.0;
  if ((fabs(C.getY()) < epsilon) && (fabs(C.getZ()) < epsilon)) {
    trans = 1.0;
  }
  const Coord3D AB = B - A;
  const Coord3D result((C.getX() * AB.getX() + C.getY() * AB.getY() + (C.getZ() - trans) * AB.getZ()) / AB.getX(), 0.0, trans);
  return C + (result - C) * (radius / C.distance(result));
}


std::vector<Coord3D> PointCloudShapeEstimator::computeEdgePointsOnSphere(const Point3D & center, double radius, unsigned int nbPointsOnSphereEdges_t) {
#ifndef NDEBUG
  const double epsilon = 0.00000001;
#endif
  assert(nbPointsOnSphereEdges_t != 0);
  std::vector<Coord3D> result;
  const double angle_step = 2 * M_PI / nbPointsOnSphereEdges_t;

  for(std::map<VertexID, unsigned int>::const_iterator nb = neighbours[center.getId()].begin(); nb != neighbours[center.getId()].end(); ++nb) {
    const double ABdistanceO2 = center.distance(pCloud[(*nb).first]) / 2;
    if (ABdistanceO2 < radius) {
      const double circle_radius = sqrt(radius * radius - ABdistanceO2 * ABdistanceO2);
      const Coord3D vect = (center - pCloud[(*nb).first]).normalize();
      const Coord3D middle = (center + pCloud[(*nb).first]) / 2;
      Coord3D ePoint = computeFirstEdgePoint(center, pCloud[(*nb).first], middle, circle_radius);
      assert(fabs(ePoint.distance(middle) - circle_radius) < epsilon);
      assert(fabs(Coord3D::angleVector(center - middle, ePoint - middle) - M_PI / 2) < epsilon);

      Matrix3x3 M = Matrix3x3::getRotationMatrix(vect, angle_step);
      for (unsigned int i = 0; i < nbPointsOnSphereEdges_t; ++i) {
	assert(fabs(ePoint.distance(center) - radius) < epsilon);
	bool outside = true;
	for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p)
	  if (((*p).getId() != center.getId()) && ((*p).getId() != (*nb).first))
	    if ((*p).distance(ePoint) <= radius) {
	      outside = false;
	      break;
	    }

	if (outside) {
	  result.push_back((ePoint - center) / radius);
	}

	ePoint = middle + M * (ePoint - middle);
      }
    }
  }

  return result;
}

const std::vector<Coord3D> & PointCloudShapeEstimator::computeNormalsUsingSurface(const Mesh & mesh) {
  if (mesh.getNbPoints() != pCloud.size())
    throw Exception("computeNormalsUsingSurface(1): Bad number of points in the mesh.");
  pCloud.initNormals();
  neighbours.clear();
  neighbours.reserve(pCloud.size());

  Mesh::const_point_iterator p = mesh.point_begin();
  for(VertexID i = 0; i < mesh.getNbPoints(); ++i, ++p) {
    pCloud.getNormal(i) = mesh.computePointNormal(i);
    pCloud.getNormal(i).normalize();
    neighbours.push_back(std::map<VertexID, unsigned int>());
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
      neighbours.back()[*nb] = 1;
  }

  return pCloud.getNormals();
}


const std::vector<Coord3D> & PointCloudShapeEstimator::computeNormalsOffsetMethod() {
  pCloud.initNormals();
  if (((nMethod == PointCloudShapeEstimator::NMMainCCandPCA) || (nMethod == PointCloudShapeEstimator::NMPCAandMainCC)) && (normalsBis.size() != pCloud.size())) {
    normalsBis.clear();
    normalsBis.insert(normalsBis.begin(), pCloud.size(), Coord3D(0.0, 0.0, 0.0));
  }

  // clear the class structures
  neighbours.clear();
  neighbours.reserve(pCloud.size());
  radii.clear();
  radii.reserve(pCloud.size());
  boundaryFlags.clear();

  struct timeval tstart_real, tend_real;
  gettimeofday(&tstart_real, NULL);

  // create the ball and initialize the number of points on its surface.
  Mesh sphere = Mesh::createSphere(1., sphere_resolution);
  sphere.setTriangleFlag(0);
  gettimeofday(&tend_real, NULL);
  std::cout << " Creating sphere: " << (((double)(tend_real.tv_usec - tstart_real.tv_usec)) / 1000000L + tend_real.tv_sec - tstart_real.tv_sec) << " s" << std::endl;

  /*
    Initialization for RMEstimation method
  */
  // created outside of the loop for speedup reasons
  std::vector<double> ioThreshold(rMethod == RMEstimation ? sphere.getNbPoints() : 0);


  /*
    Initialization for RMValue method
  */
  double fixed_radius = value_sphere;
  if (rMethod == RMValue) {
    fixed_radius *= pCloud.getSmallestDistance();
    assert(fixed_radius != 0.0);
  }

  // init a list of the sphere points that will be sorted according to there io threshold
  std::vector<VertexID> sPoints(sphere.getNbPoints());
  {
    unsigned int i = 0;
    for(std::vector<VertexID>::iterator p = sPoints.begin(); p != sPoints.end(); ++p, ++i)
      *p = i;
  }

  if ((rMethod == RMEstimation) && ((unsigned int)(sphere.getNbPoints() * ratio) == 0))
    throw Exception("Warning: surface ratio is smaller. Cannot compute normals.");
  gettimeofday(&tstart_real, NULL);

  for(PointCloud::const_iterator center = pCloud.begin(); center != pCloud.end(); ++center) {
    if (rMethod == RMEstimation) {
      radii.push_back(estimateRadius(sphere, *center, sPoints, ioThreshold));
      // compute neighbour points
      if (useOctree)
	computeNeighboursUsingFlags(*center, radii.back(), sphere);
      else
	computeNeighboursUsingFlagsWithoutOctree(*center, radii.back(), sphere);
    }
    else {
      if (rMethod == RMValue)
	radii.push_back(fixed_radius);
      else { // if (rMethod == RMNbNeighbours)
	radii.push_back(estimateRadiusByNeighbours(*center));
      }

      // compute neighbour points
      if (useOctree)
	computeNeighbours(*center, radii.back(), sphere);
      else
	computeNeighboursWithoutOctree(*center, radii.back(), sphere);
    }

    std::vector<Coord3D> edgePoints;
    // if needed, compute points on the sphere edge
    if (nbPointsOnSphereEdges != 0)
      edgePoints = computeEdgePointsOnSphere(*center, radii.back(), nbPointsOnSphereEdges);

    // normal computation
    if (nMethod == NMPCA)
      computeNormalUsingPCA((*center).getId(), pCloud.getNormals(), sphere, edgePoints);
    else if (nMethod == NMMainCC)
      computeNormalUsingMainCC((*center).getId(), pCloud.getNormals(), sphere);
    else if (nMethod == NMMainCCandPCA) {
      computeNormalUsingPCA((*center).getId(), normalsBis, sphere, edgePoints);
      computeNormalUsingMainCC((*center).getId(), pCloud.getNormals(), sphere);
    }
    else /*if (nMethod == NMPCAandMainCC)*/ {
      computeNormalUsingPCA((*center).getId(), pCloud.getNormals(), sphere, edgePoints);
      computeNormalUsingMainCC((*center).getId(), normalsBis, sphere);
    }
  }

  gettimeofday(&tend_real, NULL);
  std::cout << " Computing normals and neighbours: " << (((double)(tend_real.tv_usec - tstart_real.tv_usec)) / 1000000L + tend_real.tv_sec - tstart_real.tv_sec) << " s" << std::endl;

  return pCloud.getNormals();
}

double PointCloudShapeEstimator::estimateRadiusByNeighbours(const Point3D & center) {
  return octree.distanceNthNeighbour(center, number_neighbours) / 2;
}

double PointCloudShapeEstimator::estimateRadius(Mesh & sphere, const Point3D & center, std::vector<VertexID> & sPoints, std::vector<double> & ioThreshold) {
  DistanceComparator dC;
  dC.distance = &ioThreshold;

  // for each point of the sphere, compute an inside/outside distance threshold
  std::vector<double>::iterator iod = ioThreshold.begin();
  for(Mesh::point_iterator pSphere = sphere.point_begin(); pSphere != sphere.point_end(); ++pSphere, ++iod) {
    if (useOctree)
      (*iod) = octree.getRadiusFromDirection(center, *pSphere, maxAngle);
    else
      (*iod) = computeInsideOutsideDistance(center, *pSphere, maxAngle);
  }

  // then ordering the sphere points according to this distance value
  std::sort(sPoints.begin(), sPoints.end(), dC);

  // radius computation
  const VertexID nbInfty = std::count(ioThreshold.begin(), ioThreshold.end(), std::numeric_limits<double>::max());
  const double radius = ioThreshold[sPoints[(sphere.getNbPoints() - nbInfty) * (1 - ratio)]];

  bool outside = false;
  unsigned int nbSPointsOutside = 0;
  for(std::vector<VertexID>::const_iterator sP = sPoints.begin(); sP != sPoints.end(); ++sP) {
    if ((!outside) && (ioThreshold[*sP] > radius)) {
      outside = true;
    }
    sphere.point(*sP).setFlag(outside);
    if (outside)
      ++nbSPointsOutside;
  }
  assert(nbSPointsOutside != 0);

  return radius;
}

void PointCloudShapeEstimator::computeNormalUsingMainCC(VertexID id, std::vector<Coord3D> & lNormals, Mesh & sphere) {
  // first compute the connected components of the outside parts of the sphere
  MeshManipulator mManipSphere(sphere);
  unsigned int nbCC = 0;
  int idCC = 0;
  VertexID nbInCC = 0;
  for(Mesh::point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p)
    if ((*p).getFlag() == 1) {
      const VertexID nbInCCa = mManipSphere.computeCCUsingPoints((*p).getId(), 1, nbCC + 2, 0);
      if (nbInCCa > nbInCC) {
	nbInCC = nbInCCa;
	idCC = nbCC + 2;
      }
      ++nbCC;
    }
  if (nbCC == 0)
    throw Exception("Warning: no neighbours found. Cannot compute a normal.");

  // then compute the normal vector associated to each connected
  Coord3D normalVector;

  for(Mesh::point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p)
    if ((*p).getFlag() == idCC)
      normalVector += *p;

  normalVector /= nbInCC;
  normalVector.normalize();
  lNormals[id] = normalVector;

}

void PointCloudShapeEstimator::computeNormalUsingPCA(VertexID id, std::vector<Coord3D> & lNormals, const Mesh & sphere, const std::vector<Coord3D> & otherPoints) {
  CImg<double> matrix(3, 3);
  matrix(0, 2) = -1;
  for(unsigned int i = 0; i < 3; ++i)
    for(unsigned int j = 0; j <= i; ++j) {
      double value = 0.0;
      for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p)
	if ((*p).getFlag() != 0)
	  value += ((*p)(i) * (*p)(j));
      for(std::vector<Coord3D>::const_iterator p = otherPoints.begin(); p != otherPoints.end(); ++p)
	  value += ((*p)(i) * (*p)(j));
      matrix(i, j) = value;
      if (i != j) {
	matrix(j, i) = value;
      }
    }

  CImg<double> val(3), vec(3,3);
  matrix.symmetric_eigen(val, vec);
  Coord3D normalVector;
  normalVector[0] = vec(0, 0);
  normalVector[1] = vec(0, 1);
  normalVector[2] = vec(0, 2);
  normalVector.normalize();
  lNormals[id] = normalVector;
}


void PointCloudShapeEstimator::computeNeighboursWithoutOctree(const Point3D & center, double radius, Mesh & sphere) {
  neighbours.push_back(std::map<VertexID, unsigned int>());
  const VertexID cId = center.getId();
  std::vector<VertexID> nbs;
  // first compute flags
  for(Mesh::point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p) {
    const Coord3D p2 = (*p) * radius + center;
    (*p).setFlag(1);
    double minD = std::numeric_limits<double>::max();
    nbs.push_back(pCloud.size());
    for (PointCloud::const_iterator nb = pCloud.begin(); nb != pCloud.end(); ++nb)
      if ((*nb).getId() != cId) {
	const double d2 = (*nb).distance(p2);
	if ((d2 <= minD) && (d2 <= radius)) {
	  (*p).setFlag(0);
	  nbs.back() = (*nb).getId();
	  minD = d2;
	}
      }
  }

  std::vector<VertexID>::const_iterator curNbs = nbs.begin();
  for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p, ++curNbs)
    if ((*p).getFlag() == 0) {
      assert(*curNbs != pCloud.size());
      if (*curNbs >= pCloud.size())
	throw Exception("computeNeighbours(3): wrong neighbour value");
      bool bd = false;
      for(std::deque<VertexID>::const_iterator pnb = (*p).getNeighbours().begin(); pnb != (*p).getNeighbours().end(); ++pnb)
	if (sphere.point(*pnb).getFlag() != 0) {
	  bd = true;
	  break;
	}
      // if *p is a boundary point, we are looking for the nearest point of the surface
      if (bd) {
	neighbours.back()[*curNbs] += 1;
      }
    }
}


void PointCloudShapeEstimator::computeNeighbours(const Point3D & center, double radius, Mesh & sphere) {
  neighbours.push_back(std::map<VertexID, unsigned int>());

  std::vector<VertexID> nbsSphere = octree.findPoints(center, center.getId(), 2.001 * radius);

  std::vector<VertexID> nbs;
  // first compute flags
  for(Mesh::point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p) {
    const Coord3D p2 = (*p) * radius + center;
    (*p).setFlag(1);
    double minD = std::numeric_limits<double>::max();
    nbs.push_back(pCloud.size());
    for (std::vector<VertexID>::const_iterator nb = nbsSphere.begin(); nb != nbsSphere.end(); ++nb) {
      const double d2 = pCloud[*nb].distance(p2);
      if ((d2 <= minD) && (d2 <= radius)) {
	(*p).setFlag(0);
	nbs.back() = *nb;
	minD = d2;
      }
    }
  }

  std::vector<VertexID>::const_iterator curNbs = nbs.begin();
  for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p, ++curNbs)
    if ((*p).getFlag() == 0) {
      assert(*curNbs != pCloud.size());
      if (*curNbs >= pCloud.size())
	throw Exception("computeNeighbours(3): wrong neighbour value");
      bool bd = false;
      for(std::deque<VertexID>::const_iterator pnb = (*p).getNeighbours().begin(); pnb != (*p).getNeighbours().end(); ++pnb)
	if (sphere.point(*pnb).getFlag() != 0) {
	  bd = true;
	  break;
	}
      // if *p is a boundary point, we are looking for the nearest point of the surface
      if (bd) {
	neighbours.back()[*curNbs] += 1;
      }
    }
}

void PointCloudShapeEstimator::computeNeighboursUsingFlagsWithoutOctree(const Point3D & center, double radius,
									const Mesh & sphere) {

  const VertexID cId = center.getId();
  neighbours.push_back(std::map<VertexID, unsigned int>());
  for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p)
    if ((*p).getFlag() == 0) {
      bool bd = false;
      for(std::deque<VertexID>::const_iterator pnb = (*p).getNeighbours().begin(); pnb != (*p).getNeighbours().end(); ++pnb)
	if (sphere.point(*pnb).getFlag() != 0) {
	  bd = true;
	  break;
	}
      // if *p is a boundary point, we are looking for the nearest point of the surface
      if (bd) {
	const Coord3D p2 = (*p) * radius + center;
	double minD = std::numeric_limits<double>::max();
	VertexID nbID = pCloud.size();
	for (PointCloud::const_iterator nb = pCloud.begin(); nb != pCloud.end(); ++nb)
	  if ((*nb).getId() != cId) {
	    const double d2 = (*nb).distance(p2);
	    if (d2 <= minD) {
	      nbID = (*nb).getId();
	      minD = d2;
	    }
	  }
	if (nbID >= pCloud.size()) {
	  throw Exception("computeNeighboursUsingFlags(3): wrong neighbour value");
	}
	neighbours.back()[nbID] += 1;
      }
    }
}

void PointCloudShapeEstimator::computeNeighboursUsingFlags(const Point3D & center, double radius,
							   const Mesh & sphere) {

  std::vector<VertexID> nbsSphere = octree.findPoints(center, center.getId(), 2.001 * radius);
  neighbours.push_back(std::map<VertexID, unsigned int>());
  for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p)
    if ((*p).getFlag() == 0) {
      bool bd = false;
      for(std::deque<VertexID>::const_iterator pnb = (*p).getNeighbours().begin(); pnb != (*p).getNeighbours().end(); ++pnb)
	if (sphere.point(*pnb).getFlag() != 0) {
	  bd = true;
	  break;
	}
      // if *p is a boundary point, we are looking for the nearest point of the surface
      if (bd) {
	const Coord3D p2 = (*p) * radius + center;
	double minD = std::numeric_limits<double>::max();
	VertexID nbID = pCloud.size();
	for (std::vector<VertexID>::const_iterator nb = nbsSphere.begin(); nb != nbsSphere.end(); ++nb) {
	  const double d2 = pCloud[*nb].distance(p2);
	  if (d2 <= minD) {
	    nbID = *nb;
	    minD = d2;
	  }
	}
	if (nbID >= pCloud.size()) {
	  throw Exception("computeNeighboursUsingFlags(3): wrong neighbour value");
	}
	neighbours.back()[nbID] += 1;
      }
    }
}

const std::vector<double> & PointCloudShapeEstimator::computeLocalDistortion() {
  if ((pCloud.getNormals().size() != pCloud.size()) || (neighbours.size() != pCloud.size()))
    computeNormalsOffsetMethod();
  localDistortions.clear();
  localDistortions.reserve(pCloud.size());

  std::vector<double> distanceNb;

  std::vector<std::map<VertexID, unsigned int> >::const_iterator cNeighbours = neighbours.begin();
  for(PointCloud::const_iterator center = pCloud.begin(); center != pCloud.end(); ++center, ++cNeighbours) {
    double localDistortion = 0.0;
    for(std::map<VertexID, unsigned int>::const_iterator nb = (*cNeighbours).begin(); nb != (*cNeighbours).end(); ++nb) {
      double angle = Coord3D::angleVector(pCloud.getNormal((*center).getId()), pCloud.getNormal((*nb).first));
      const double angle2 = Coord3D::angleVector(-pCloud.getNormal((*center).getId()), pCloud.getNormal((*nb).first));
      const double d = (*center).distance(pCloud[(*nb).first]);
      assert(d != 0.0);
      if (angle2 < angle)
	angle = angle2;
      localDistortion += angle / d;
      distanceNb.push_back(d);
    }
    assert((*cNeighbours).size() != 0);
    localDistortion /= (*cNeighbours).size();
    localDistortions.push_back(localDistortion);
  }

  std::sort(distanceNb.begin(), distanceNb.end());
  const double r = distanceNb[distanceNb.size() / 2];
  for(std::vector<double>::iterator ld = localDistortions.begin(); ld != localDistortions.end(); ++ld)
    (*ld) *= r;

  return localDistortions;
}



double PointCloudShapeEstimator::getMedianLocalDistortion() const {
  if ((localDistortions.size() != pCloud.size()) || (radii.size() != pCloud.size()))
    throw Exception("getMedianLocalDistortion(): local distortions has not been computed.");

  std::vector<double> olDesc(localDistortions);
  std::sort(olDesc.begin(), olDesc.end());

  return olDesc[olDesc.size() / 2];
}


double PointCloudShapeEstimator::computeShapeEstimator() {
  if ((localDistortions.size() != pCloud.size()) || (radii.size() != pCloud.size()))
    computeLocalDistortion();

  return getMeanLocalDistortion();
}

double PointCloudShapeEstimator::getMeanLocalDistortion() const {
  if ((localDistortions.size() != pCloud.size()) || (radii.size() != pCloud.size()))
    throw Exception("getMeanLocalDistortion(): local distortions has not been computed.");
  long double result = 0.0;

  for(std::vector<double>::const_iterator d = localDistortions.begin(); d != localDistortions.end(); ++d) {
    result += *d;
  }
  result /= localDistortions.size();

  return result;
}

double PointCloudShapeEstimator::getMeanLocalDistortionFlags() {
  if ((localDistortions.size() != pCloud.size()) || (radii.size() != pCloud.size()))
    computeLocalDistortion();
  long double result = 0.0;

  VertexID nbPoints = 0;
  PointCloud::const_iterator p = pCloud.begin();
  for(std::vector<double>::const_iterator d = localDistortions.begin(); d != localDistortions.end(); ++d, ++p)
    if ((*p).getFlag() != 0) {
      result += *d;
      ++nbPoints;
    }
  if (nbPoints > 0)
    result /= nbPoints;

  return result;
}


double PointCloudShapeEstimator::getMeanLocalDistortionThreshold(double alpha) {
  if ((localDistortions.size() != pCloud.size()) || (radii.size() != pCloud.size()))
    computeLocalDistortion();
  long double result = 0.0;

  VertexID nbPoints = 0;
  std::vector<double>::const_iterator r = radii.begin();
  const double threshold = alpha * pCloud.getSmallestDistance();
  for(std::vector<double>::const_iterator d = localDistortions.begin(); d != localDistortions.end(); ++d, ++r)
    if (*r <= threshold) {
      result += *d;
      ++nbPoints;
    }
  if (nbPoints > 0)
    result /= nbPoints;

  return result;
}


const std::vector<double> & PointCloudShapeEstimator::computeLocalBoundaryDistortion() {
  if (boundaryFlags.size() == 0)
    computeBoundaryFlags();
  bNeighgours.clear();
  bNeighgours.insert(bNeighgours.begin(), pCloud.size(), std::pair<VertexID, VertexID>(pCloud.size(), pCloud.size()));
  if (bNeighgours.size() != pCloud.size())
    throw Exception("computeBoundaryFlags(): cannot initialize boundary neighbours structure. Abort.");

  // first computing the boundary connections (may not be symetric)
  std::vector<std::map<VertexID, unsigned int> >::const_iterator nbs = neighbours.begin();
  PointCloud::const_iterator p = pCloud.begin();
  for(std::vector<bool>::const_iterator bf = boundaryFlags.begin(); bf != boundaryFlags.end(); ++bf, ++nbs, ++p)
    if (*bf) {
      VertexID firstNbB = pCloud.size();
      VertexID secondNbB = pCloud.size();
      unsigned int dFirstNbB = 0;
      unsigned int dSecondNbB = 0;
      for (std::map<VertexID, unsigned int>::const_iterator nb = (*nbs).begin(); nb != (*nbs).end(); ++nb)
	if (boundaryFlags[(*nb).first] && (neighbours[(*nb).first].find((*p).getId()) != neighbours[(*nb).first].end())) {
	  if ((*nb).second > dFirstNbB) {
	    secondNbB = firstNbB;
	    dSecondNbB = dFirstNbB;
	    firstNbB = (*nb).first;
	    dFirstNbB = (*nb).second;
	  }
	  else if ((*nb).second > dSecondNbB) {
	    secondNbB = (*nb).first;
	    dSecondNbB = (*nb).second;
	  }
	}

      if (dFirstNbB != 0) 
	bNeighgours[(*p).getId()].first = firstNbB;
      if (dSecondNbB != 0)
	  bNeighgours[(*p).getId()].second = secondNbB;
      }


  // then compute the local distortion for every boundary point
  localBoundaryDistortions.clear();
  localBoundaryDistortions.reserve(pCloud.size());
  p = pCloud.begin();
  std::vector<bool>::const_iterator bf = boundaryFlags.begin();
  for(std::vector<std::pair<VertexID, VertexID> >::const_iterator bnbs = bNeighgours.begin(); bnbs != bNeighgours.end(); ++bnbs, ++bf, ++p)
    if (*bf) {
      if ((*bnbs).second != pCloud.size()) {
	double angle = Coord3D::angleVector(pCloud[(*bnbs).first] - *p, pCloud[(*bnbs).second] - *p);
	assert((0 <= angle) && (angle <= M_PI)); 
	if (angle >= M_PI / 2)
	  angle -= M_PI / 2;
	localBoundaryDistortions.push_back(M_PI / 4 - fabs(angle - M_PI / 4));
      }
      else
	localBoundaryDistortions.push_back(-3.);
    }
    else
      localBoundaryDistortions.push_back(-1.);


  return localBoundaryDistortions;
 }

double PointCloudShapeEstimator::computeBoundaryShapeEstimator() {
   double result = 0.0;

  if (localBoundaryDistortions.size() != pCloud.size())
    computeLocalBoundaryDistortion();

  VertexID nbUsed = 0;
  for(std::vector<double>::const_iterator lbd = localBoundaryDistortions.begin(); lbd != localBoundaryDistortions.end(); ++lbd)
    if (*lbd >= 0) {
      result += *lbd;
      ++nbUsed;
    }

  if (nbUsed != 0)
    result /= nbUsed;
  return result;
}

const std::vector<bool> & PointCloudShapeEstimator::computeBoundaryFlags() {
  if (nMethod == PointCloudShapeEstimator::NMMainCC)
    nMethod = PointCloudShapeEstimator::NMMainCCandPCA;
  if (nMethod == PointCloudShapeEstimator::NMPCA)
    nMethod = PointCloudShapeEstimator::NMPCAandMainCC;

  if ((localDistortions.size() != pCloud.size()) || (radii.size() != pCloud.size()))
    computeLocalDistortion();

  if (nMethod == PointCloudShapeEstimator::NMMainCCandPCA)
    computeBoundaryFlags(angle_error, pCloud.getNormals(), normalsBis);
  else //  (nMethod == PointCloudShapeEstimator::NMPCAandMainCC)
    computeBoundaryFlags(angle_error, normalsBis, pCloud.getNormals());

  return boundaryFlags;
}

void PointCloudShapeEstimator::computeBoundaryFlags(double angle_error_t, const std::vector<Coord3D> & normals, const std::vector<Coord3D> & pseudoNormals) {
  assert(normals.size() == pseudoNormals.size());
  assert(normals.size() == pCloud.size());
  assert((localDistortions.size() == pCloud.size()) || (radii.size() == pCloud.size()));
  boundaryFlags.clear();
  boundaryFlags.reserve(pCloud.size());

  std::vector<Coord3D>::const_iterator pseudoNormal = pseudoNormals.begin();
  for(std::vector<Coord3D>::const_iterator normal = normals.begin(); normal != normals.end(); ++normal, ++pseudoNormal) {
    double angle = Coord3D::angleVector(*normal, *pseudoNormal);
    boundaryFlags.push_back((angle >= angle_error_t) && (angle <= M_PI - angle_error_t));
  }
}



