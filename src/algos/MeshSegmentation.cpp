#include "Mesh.h"
#include "MeshMap.h"
#include "MeshManipulator.h"
#include "MeshSegmentation.h"

using namespace Taglut;


MeshMap & MeshSegmentation::segmentationByRapprochement(const std::vector<std::vector<VertexID> > & borders, int flagInside, bool withVoronoi, bool withCC) {
  std::vector<std::vector<VertexID> > sites;
  if (withVoronoi || withCC)
    sites = borders;

  // compute for each border the distance map using Dijkstra algorithm
  std::vector<std::pair<double *, VertexID *> > dijkstraValues;
  if (length != NULL) delete[] length;
  if (pred != NULL) delete[] pred;
  for(std::vector<std::vector<VertexID> >::const_iterator path = borders.begin(); path != borders.end(); ++path) {
    length = NULL;
    pred = NULL;
    computeDijkstra((*path).begin(), (*path).end(), flagInside);
    dijkstraValues.push_back(std::pair<double *, VertexID *>(length, pred));
  }
  length = NULL;
  pred = NULL;


  // then compute the classification by rapprochement of each vertex of the mesh

  unsigned int i = flagInside + 1;
  for(std::vector<std::vector<VertexID> >::const_iterator path = borders.begin(); path != borders.end(); ++path, ++i) {
    for(std::vector<VertexID>::const_iterator p = (*path).begin(); p != (*path).end(); ++p) {
      (*mesh).point(*p).setFlag(i);
      mMap[*p] = i;
    }
  }


  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == flagInside) {
      VertexID pid = (*p).getId();
      int value = 0;
      int idB = flagInside + 1;
      bool found = false;
      for(std::vector<std::pair<double *, VertexID *> >::const_iterator b = dijkstraValues.begin(); b != dijkstraValues.end(); ++b, ++idB) {
	VertexID pred_t = (*b).second[pid];
	bool only = true;
	for(std::vector<std::pair<double *, VertexID *> >::const_iterator b2 = dijkstraValues.begin(); b2 != dijkstraValues.end(); ++b2)
	  if ((b != b2) && ((*b2).first[pid] > (*b2).first[pred_t])) {
	    only = false;
	    break;
	  }
	if (only) {
	  found = true;
	  if (value != 0) // more than one class for *p: no classification found
	    value = -2;
	  else
	    value = idB;
	}
      }
      if (!found)
	value = -2;
      mMap[pid] = value;
      (*p).setFlag(value);
      if ((withVoronoi || withCC) && (value != -2))
	sites[value - flagInside - 1].push_back(pid);
    }


  // free dijkstra results
  for(std::vector<std::pair<double *, VertexID *> >::iterator dv = dijkstraValues.begin(); dv != dijkstraValues.end(); ++dv) {
    delete[] (*dv).first;
    delete[] (*dv).second;
  }


  if (withCC) {
    int idB = flagInside + 1;
    for(std::vector<std::vector<VertexID> >::iterator site = sites.begin(); site != sites.end(); ++site, ++idB) {
      std::vector<VertexID> newSite;
      computeCCUsingPoints((*site).front(), idB, 0, 0);
      for(std::vector<VertexID>::const_iterator s = (*site).begin(); s != (*site).end(); ++s)
	if ((*mesh).point(*s).getFlag() == 0) {
	  newSite.push_back(*s);
	  mMap[*s] = idB;
	  (*mesh).point(*s).setFlag(idB);
	}
	else {
	  mMap[*s] = -2;
	  (*mesh).point(*s).setFlag(-2);
	}
      *site = newSite;
    }
  }

  if (withVoronoi)
    segmentationByVoronoi(sites, -2);

  return mMap;
}


MeshMap & MeshSegmentation::segmentationByVoronoi(const std::vector<std::vector<VertexID> > & sites, int flagInside, int offset) {
  // build a list of the inside points, and set flags of the sites and the inside points
  std::vector<VertexID> inside;

  unsigned int nb1 = 0;
  for(std::vector<std::vector<VertexID> >::const_iterator path = sites.begin(); path != sites.end(); ++path)
    for(std::vector<VertexID>::const_iterator p = (*path).begin(); p != (*path).end(); ++p, ++nb1)
      (*mesh).point(*p).setFlag(flagInside);

  unsigned int nb2 = 0;
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == flagInside) {
      inside.push_back((*p).getId());
      ++nb2;
    }


  // compute for each border the distance map using Dijkstra algorithm
  std::vector<std::pair<double *, VertexID *> > dijkstraValues;
  if (length != NULL) delete[] length;
  if (pred != NULL) delete[] pred;
  for(std::vector<std::vector<VertexID> >::const_iterator site = sites.begin(); site != sites.end(); ++site) {
    length = NULL;
    pred = NULL;
    computeDijkstra((*site).begin(), (*site).end(), flagInside);
    dijkstraValues.push_back(std::pair<double *, VertexID *>(length, pred));
  }
  length = NULL;
  pred = NULL;


  // Voronoi computation
  for(std::vector<VertexID>::const_iterator p = inside.begin(); p != inside.end(); ++p) {
    int flagValue = offset + 1;
    int idB = 1;
    double distance = dijkstraValues.front().first[*p];
    for(std::vector<std::pair<double *, VertexID *> >::const_iterator b = dijkstraValues.begin(); b != dijkstraValues.end(); ++b, ++idB) {
      if ((*b).first[*p] < distance) {
	flagValue = offset + idB;
	distance = (*b).first[*p];
      }
    }
    mMap[*p] = flagValue;
  }

  // free dijkstra results
  for(std::vector<std::pair<double *, VertexID *> >::iterator dv = dijkstraValues.begin(); dv != dijkstraValues.end(); ++dv) {
    delete[] (*dv).first;
    delete[] (*dv).second;
  }

  return mMap;
}
