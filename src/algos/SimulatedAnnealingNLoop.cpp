/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include <list>
#include <set>
#include <algorithm>
#include "SimulatedAnnealingNLoop.h"
#include "MeshPathes.h"
#include "MeshPart.h"
#include "Length.h"
#include "Point3D.h"
#include "Exception.h"
#include "FileTools.h"
#include "NLoop.h"
#include "WaveFront.h"
#include "FlagLogger.h"
#include "RandomGenerator.h"
#include "RandomVertexSelector.h"

#ifdef USE_LIBXML
#include <libxml/xmlreader.h>
#endif

using namespace Taglut;

SimulatedAnnealingNLoop::SimulatedAnnealingNLoop(Mesh & mesh_t,
						 const NLoopEvaluator & nLEvaluator_t,
						 double temp0_t, unsigned int step_size_t,
						 double temp_coeff_t, double accept_threshold_t, double temp_min_t,
						 enum SAMethod method_t, bool returnShortest_t) : MeshPathes(mesh_t), randvect(mesh_t),
												  temp0(temp0_t), step_size(step_size_t),
												  temp_coeff(temp_coeff_t),
												  accept_threshold(accept_threshold_t),
												  temp_min(temp_min_t),
												  method(method_t), returnShortest(returnShortest_t),
												  nLEvaluator(nLEvaluator_t) { }

SimulatedAnnealingNLoop::SimulatedAnnealingNLoop(Mesh & mesh_t,
						 const NLoopEvaluator & nLEvaluator_t,
						 const LengthEdge & lMethod_t,
						 double temp0_t, unsigned int step_size_t,
						 double temp_coeff_t, double accept_threshold_t, double temp_min_t,
						 enum SAMethod method_t, bool returnShortest_t) : MeshPathes(mesh_t, lMethod_t), randvect(mesh_t),
												  temp0(temp0_t), step_size(step_size_t),
												  temp_coeff(temp_coeff_t),
												  accept_threshold(accept_threshold_t),
												  temp_min(temp_min_t),
												  method(method_t), returnShortest(returnShortest_t),
												  nLEvaluator(nLEvaluator_t) { }

SimulatedAnnealingNLoop::SimulatedAnnealingNLoop(const SimulatedAnnealingNLoop & saNLoop) : MeshPathes(saNLoop), randvect(*(saNLoop.mesh)),
											    nLEvaluator(saNLoop.nLEvaluator) { }

SimulatedAnnealingNLoop::SimulatedAnnealingNLoop(const SimulatedAnnealingNLoop & saNLoop, Mesh & mesh_t) : MeshPathes(saNLoop, mesh_t), randvect(mesh_t),
													 nLEvaluator(saNLoop.nLEvaluator) { }



unsigned int SimulatedAnnealingNLoop::getNextArity(unsigned int nbPaths, unsigned int nbPathsMin, unsigned int nbPathsMax) {
  if (nbPathsMin >= nbPathsMax)
    return nbPaths;

  if (randgen() > .5) {
    if (nbPathsMin == nbPaths)
      return nbPaths + 1;
    if (nbPathsMax == nbPaths)
      return nbPaths - 1;
    else if (randgen() > .5)
      return nbPaths + 1;
    else
      return nbPaths - 1;
  }
  else return nbPaths;
}


NLoop SimulatedAnnealingNLoop::approximateShortestValidNLoopArityFixed(unsigned int nbPaths, VertexID bb1, VertexID bb2) {
  return approximateShortestValidNLoop(nbPaths, nbPaths, bb1, bb2);
}

NLoop SimulatedAnnealingNLoop::approximateShortestValidNLoop(unsigned int nbPathsMin, unsigned int nbPathsMax, VertexID bb1, VertexID bb2) {
  if (nbPathsMin > nbPathsMax)
    throw Exception("approximateShortestValidNLoop(4): wrong range number for the arity of the n-loop.");
  // select the first point using a random method if not defined.
  double minLength = std::numeric_limits<double>::max();
  VertexID b1 = (bb1 >= (*mesh).getNbPoints())  ? randvect() : bb1;
  std::deque<VertexID> path;
  VertexID b2;
  unsigned int nbPaths = nbPathsMin;

  if (bb2 >= (*mesh).getNbPoints()) {
    // compute the shortest non trivial loop
    path = getShortestNonTrivialCycleFromPoint(b1);
    // select the middle point of this loop
    b2 = (*mesh).getMiddlePoint(path);
    if ((*mesh).point(b2).getIsBoundary()) {
      do {
	unsigned int idNb = (unsigned int)floor((*mesh).point(b2).getNbNeighbours() * randgen());
	assert(idNb < (*mesh).point(b2).getNbNeighbours());
	b2 = (*mesh).point(b2).getNeighbour(idNb);
      } while((b1 == b2) || ((*mesh).point(b2).getIsBoundary()));
    }
  }
  else
    b2 = bb2;

  // then build the first NLoop
  NLoop result = getShortestValidNLoop(b1, b2, nbPaths);
  NLoop shortest = result;

  // simulated annealing
  bool nonstop = true;
  double temp = temp0;
  double real_size;
  do {
    unsigned int nb_moves = 0;
    real_size = step_size;
    for(unsigned int i = 0; i < step_size; ++i) {
      std::pair<VertexID, VertexID> newBasePoints;

      if (method == SimulatedAnnealingNLoop::SASimple)
	newBasePoints = getNextNLoopBasePointsSimple(result);
      else { // if (method == SimulatedAnnealingNLoop::SAByMiddle)
	newBasePoints = getNextNLoopBasePointsByMiddle(result);
      }

      if ((newBasePoints.second != result.getBasePoint()) || (newBasePoints.first != result.getLastPoint())) {
	nbPaths = getNextArity(nbPaths, nbPathsMin, nbPathsMax);
	NLoop newNLoop = getShortestValidNLoop(newBasePoints.first, newBasePoints.second, nbPaths);

	if (newNLoop.getNbPaths() == nbPaths) {
	  double e1 = nLEvaluator(*mesh, newNLoop);
	  double e2 = nLEvaluator(*mesh, result);
	  double delta = e1 - e2;
	  if ((result.getNbPaths() != nbPaths) || (((delta < 0.0) || (randgen() < exp(-delta/temp))) && ((delta != 0.0) || ((result.getBasePoint() != newNLoop.getBasePoint()) && (result.getLastPoint() != newNLoop.getLastPoint()))))) {
	    result = newNLoop;
	    ++nb_moves;
	  }
	  if (e2 < minLength) {
	    minLength = e2;
	    shortest = result;
	  }
	}
	else
	  --real_size;
      }
      else
	--real_size;
    }
    double accept = (double)nb_moves / real_size;
    std::cout << " Temperature: " << temp << ", accept: " << (accept*100) << "% for " << real_size << " valid runs. Current length: " << nLEvaluator(*mesh, result) << " (arity: " << result.getNbPaths() << ")"  << ", minimum: " << minLength << " (arity: " << shortest.getNbPaths() << ")" << std::endl;
    if ((accept < (double)accept_threshold) || (temp < temp_min))
      nonstop = false;
    temp *= temp_coeff;
  } while (nonstop);


  if (returnShortest)
    return shortest;
  else
    return result;
}


std::pair<VertexID, VertexID> SimulatedAnnealingNLoop::getNextNLoopBasePointsByMiddle(const NLoop & nloop) {
  std::pair<VertexID, VertexID> result;
  std::vector<VertexID> middleOfPaths;
  double min = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();
  double sum = 0.0;
  const VertexID b1 = nloop.getLastPoint();
  const VertexID b2 = nloop.getBasePoint();

  if (nloop.getNbPaths() == 0)
    throw Exception("getNextNLoopBasePoints(1): empty NLoop.");

  // first look for the length of each path of the NLoop
  unsigned int id = 0;
  unsigned int maxid = 0;
  unsigned int minid = 0;
  for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p, ++id) {
    const double length_t = (*mesh).getLength(*p);
    sum += length_t;
    if (min > length_t) {
      min = length_t;
      minid = id;
    }
    if (max < length_t) {
      max = length_t;
      maxid = id;
    }
  }

  std::vector<VertexID> path = nloop.getPath(maxid);
  if ((1.5 * sum < nloop.getNbPaths() * max) && (path[1] != path[path.size() - 2])) {
    result.first = path[1];
    result.second = path[path.size() - 2];
  }
  else if (sum > 4 * nloop.getNbPaths() * min) {
    std::vector<VertexID> pathmin = nloop.getPath(minid);
    assert(pathmin.size() >= 2);
    result.first = (*mesh).point(pathmin.front()).getNeighbourOtherSide(pathmin[1]);
    result.second = (*mesh).point(pathmin.back()).getNeighbourOtherSide(pathmin[pathmin.size() - 2]);
  }
  else {
    // first compute the middle of each path
    for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p) {
      middleOfPaths.push_back((*mesh).getMiddlePoint(*p));
    }

    // then compute a distance map to the middle points
    std::vector<double> sumDistance((*mesh).getNbPoints(), 0.0);
    std::vector<double>::iterator sumd = sumDistance.begin();

    (*mesh).setPointFlag(0);
    for(std::vector<VertexID>::const_iterator mp = middleOfPaths.begin(); mp != middleOfPaths.end(); ++mp) {
      computeDijkstra(*mp, 0);
      double * l = length;
      sumd = sumDistance.begin();
      for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i, ++sumd, ++l)
	(*sumd) += *l;
    }

    // then select a list of potential base points (local minima of the distance map)
    std::vector<VertexID> potentialBP;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p) {
      double l = sumDistance[(*p).getId()];
      bool localMin = true;
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	if (sumDistance[*nb] < l) {
	  localMin = false;
	  break;
	}
      if ((*p).getNbNeighbours() == 0)
	localMin = false;
      if (localMin)
	potentialBP.push_back((*p).getId());
    }

    if ((potentialBP.size() > 1) &&
	((potentialBP.size() != 2) || (!(((potentialBP.front() == b1) && (potentialBP.back() == b2))) || ((potentialBP.front() == b2) && (potentialBP.back() == b1))))) {

      lengthPathBP lsort;
      lsort.mesh = mesh;
      std::vector<VertexID> potentialBP2(potentialBP);
      // then select two base points using a random method
      computeDijkstra(b1, 0);
      lsort.length = length;
      std::sort(potentialBP.begin(), potentialBP.end(), lsort);
      computeDijkstra(b2, 0);
      lsort.length = length;
      std::sort(potentialBP2.begin(), potentialBP2.end(), lsort);

      result.first = potentialBP.front();
      result.second = potentialBP.back();

    }
    else {
      result.first = path.front();
      result.second = path.back();
    }
  }

  if (randgen() < 0.5) {
    do {
      unsigned int idNb = (unsigned int)floor((*mesh).point(result.second).getNbNeighbours() * randgen());
      assert(idNb < (*mesh).point(result.second).getNbNeighbours());
      result.second = (*mesh).point(result.second).getNeighbour(idNb);
    } while((result.first == result.second) || ((*mesh).point(result.second).getIsBoundary()));
  }
  else {
    do {
      unsigned int idNb = (unsigned int)floor((*mesh).point(result.first).getNbNeighbours() * randgen());
      assert(idNb < (*mesh).point(result.first).getNbNeighbours());
      result.first = (*mesh).point(result.first).getNeighbour(idNb);
    } while((result.first == result.second) || ((*mesh).point(result.first).getIsBoundary()));
  }

  return result;
}

std::pair<VertexID, VertexID> SimulatedAnnealingNLoop::getNextNLoopBasePointsSimple(const NLoop & nloop) const {
  std::pair<VertexID, VertexID> result;
  double min = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();
  double sum = 0.0;

  if (nloop.getNbPaths() == 0) {
    throw Exception("getNextNLoopBasePoints(1): empty NLoop.");
  }

  unsigned int id = 0;
  unsigned int maxid = 0;
  unsigned int minid = 0;
  for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p, ++id) {
    double length_t = (*mesh).getLength(*p);
    sum += length_t;
    if (min > length_t) {
      min = length_t;
      minid = id;
    }
    if (max < length_t) {
      max = length_t;
      maxid = id;
    }
  }

  std::vector<VertexID> path = nloop.getPath(maxid);
  if ((1.5 * sum < nloop.getNbPaths() * max) && (path[1] != path[path.size() - 2])) {
    result.first = path[1];
    result.second = path[path.size() - 2];
  }
  else if ((sum > 4 * nloop.getNbPaths() * min)) {
    std::vector<VertexID> pathmin = nloop.getPath(minid);
    assert(pathmin.size() >= 2);
    result.first = (*mesh).point(pathmin.front()).getNeighbourOtherSide(pathmin[1]);
    result.second = (*mesh).point(pathmin.back()).getNeighbourOtherSide(pathmin[pathmin.size() - 2]);
  }
  else {
    result.first = path.front();
    result.second = path.back();
  }

  if (randgen() < 0.5) {
    do {
      unsigned int idNb = (unsigned int)floor((*mesh).point(result.second).getNbNeighbours() * randgen());
      assert(idNb < (*mesh).point(result.second).getNbNeighbours());
      result.second = (*mesh).point(result.second).getNeighbour(idNb);
    } while((result.first == result.second) || ((*mesh).point(result.second).getIsBoundary()));
  }
  else {
    do {
      unsigned int idNb = (unsigned int)floor((*mesh).point(result.first).getNbNeighbours() * randgen());
      assert(idNb < (*mesh).point(result.first).getNbNeighbours());
      result.first = (*mesh).point(result.first).getNeighbour(idNb);
    } while((result.first == result.second) || ((*mesh).point(result.first).getIsBoundary()));
  }

  return result;
}

