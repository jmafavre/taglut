/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BORDER_SELECTOR_H
#define BORDER_SELECTOR_H

#include <map>
#include "EdgeManipulator.h"
#include "Mesh.h"

namespace Taglut {

  /**
     @class BorderSelector
     @author Jean-Marie Favreau (Univ. Blaise Pascal - LIMOS UMR CNRS ????)
     @brief Functor used by Mesh::forEachEdge(1). Build a mesh containing the border
     of the manipulated mesh.
  */
  class BorderSelector : public Taglut::EdgeManipulator {
  private:
    /** constructed mesh */
    Mesh & mesh;
    std::map<VertexID, VertexID> translation;
    bool vtriangles;
  public:
    /**
       default constructor
       @param mesh Mesh where the border will be constructed
       @param triangles Add virtual triangles to the structure
    */
    BorderSelector(Mesh & mesh, bool triangles = true);

    /** destructor */
    ~BorderSelector();


    /**
       functor (edge manipulation)
    */
    void operator() (VertexID id1, VertexID id2, const Mesh & omesh);


  };
}

#endif
