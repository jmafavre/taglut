/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef WAVEFRONT_H
#define WAVEFRONT_H

#include <vector>
#include <iostream>
#include "IDTypes.h"

namespace Taglut {
  class Mesh;
  class Triangle;

  /**
     @class WaveFront
     Description of a wavefront homeomorphic to a disc. Tool to detect NLoop
  */
  class WaveFront {
  private:

    /** Mesh where the wavefront is computed */
    Mesh & mesh;
    /** Front of the wave */
    std::vector<VertexID> front;


    /**
       Return true if the edge (p1, p2) is contains on a triangle inside the growing surface (i.e. with flag != 0)
    */
    bool hasTriangleInside(const VertexID & p1, const VertexID & p2) const;

    /**
       return the n-st next point in front
    */
    std::vector<VertexID>::iterator nextInFront(const std::vector<VertexID>::iterator & it, int n = 1);

    /**
       return the n-st next point in front
    */
    std::vector<VertexID>::const_iterator nextInFrontConst(const std::vector<VertexID>::const_iterator & it, int n = 1) const;


    /**
       return the n-st pred point in front
    */
    inline std::vector<VertexID>::iterator predInFront(const std::vector<VertexID>::iterator & it, int n = 1) {
      return nextInFront(it, -n);
    }

    /**
       return the n-st pred point in front
    */
    inline std::vector<VertexID>::const_iterator predInFrontConst(const std::vector<VertexID>::const_iterator & it, int n = 1) const {
      return nextInFrontConst(it, -n);
    }

    /**
       Return true if the two given points are defining an edge contains in the front.
    */
    bool isFrontEdge(const VertexID & p1, const VertexID & p2) const;

    /**
       Return true if the given edge of the wavefront is a removable edge, i.e.
       if the triangles containing it are inside the growing surface, and it is
       not a boundary edge.
    */
    bool isRemovableEdge(VertexID p1, VertexID p2) const;

    /**
       Get list of triangles added when point is added to the growing surface.
    */
    std::vector<TriangleID> getCorrespondingTriangles(VertexID  point) const;

    /**
       Add a point in the growing surface
    */
    void addPointOnFront(VertexID point);

    /**
       Add a triangle in the growing surface
    */
    void addTriangleOnFront(VertexID p1, VertexID p2, TriangleID triangle);

    /**
       Add \p point on the front (adding \p triangle) to the growing surface).
    */
    void addPointOnFrontUsingTriangle(VertexID point, TriangleID triangle);

    /**
       Add \p point on the front connecting it to \p pointOnFront
       @param point The point to add
       @param pointOnFront The point on the front where \p point will be added
       @param triangle Side of the edge (\p point, \p pointOnFront) used during insertion
    */
    void addPointOnFrontUsingPoint(VertexID point, VertexID pointOnFront, TriangleID triangle);


    /**
       Add \p point on the front in the edge defined by (\p location, \p location + 1).
    */
    void addPointOnEdge(VertexID point, const std::vector<VertexID>::iterator & location);

    /**
       Given an iterator on the front, look for the next edge (p1, p2) on the wave front and return an
       iterator on the first of these points
    */
    const std::vector<VertexID>::iterator getNextEdge(const std::vector<VertexID>::iterator & loc, VertexID p1, VertexID p2);


    /**
       Return true if the edge (loc, loc + 1) of the wavefront is in the good side according to the triangle \p triangle
       (\p triangle should contains the edge (\p loc, \p loc + 1))
    */
    bool isGoodSideTriangle(const std::vector<VertexID>::const_iterator & loc, TriangleID triangle) const;

    /**
       Return true if the edge (loc, loc + 1) of the wavefront is in the good side according to the point \p point (using the side defined by \p triangle)
    */
    bool isGoodSidePoint(const std::vector<VertexID>::const_iterator & loc, VertexID point, TriangleID triangle) const;

    /**
       Grow the wavefront until the detection of a multipoint juntion of arity >= \p nbEges
       @param points Ordered list of the points added during the growing process
       @param nbEdges Arity of the search junction
       @return ID of the junction point
    */
    VertexID growSurfaceUntilJunction(const std::vector<VertexID> & points, unsigned int nbEdges);

    /**
       Assuming that the growing surface has been down, return a set of paths corresponding to
       the start of the NLoop.
       @param junctionPoint The junction point
       @param nbPaths Arity of the search junction
       @return Start of paths contains in the NLoop
    */
    std::vector<std::vector<VertexID> > getNLoopStart(VertexID junctionPoint, unsigned int nbPaths);

    /**
       Assuming that the growing surface has been down, return a start of path starting from p.
       @param p Iterator on the front
       @return Start of path starting from p
    */
    std::vector<VertexID> getPathStart(const std::vector<VertexID>::const_iterator & p);

    /**
       Return the adjacent triangle of (p, nextInFront(p)) inside the growing surface
    */
    const Triangle & getTriangleInside(const std::vector<VertexID>::const_iterator & p) const;

    /**
       Return true if the given triangle has a point inside the growing surface, but not in front
    */
    bool hasPointInsideNotFront(const Triangle & t) const;

    /**
       If the given triangle has a point inside the growing surface (but not in front), return this point
    */
    VertexID getPointInsideNotFront(const Triangle & t) const;

    /**
       Return true if the given point has a valid flag (according to its position in the front of the wave)
    */
    bool hasValidFlag(VertexID point) const;

    /**
       Modify the flag of the given point at the insertion of the point on the wavefront
    */
    void setFlagInsert(VertexID point);

    /**
       Modify the flag of the given point at the removal of the point on the wavefront
    */
    void setFlagRemove(VertexID point);

  public:
    /**
       Default constructor
    */
    WaveFront(Mesh & mesh);

    /**
       Copy constructor
    */
    WaveFront(const WaveFront & wf);

    /**
       Destructor
    */
    ~WaveFront();

    /**
       Grow the wavefront until the detection of a multipoint juntion of arity >= \p nbEges
       @param points Ordered list of the points added during the growing process
       @param nbEdges Arity of the search junction
       @return Start of paths contains in the NLoop
    */
    std::vector<std::vector<VertexID> > growSurfaceUntilJunctionGetNLoopStart(const std::vector<VertexID> & points, unsigned int nbEdges);

    /**
       Fully grows the wavefront
       @param points Ordered list of the points added during the growing process
       @return Wavefront points
    */
    const std::vector<VertexID> & growSurfaceFull(const std::vector<VertexID> & points);

    /**
       Fully grows the wavefront from the given point
       @param point Starting point of the growing process
       @return Wavefront points
    */
    const std::vector<VertexID> & growSurfaceFull(VertexID point);

    /**
       Fully grows the wavefront, using triangles' adjacency
       @param point The starting point
       @param nbTriangles Number of triangles to be added (break of the growing surface after this triangle). 0 means no threshold.
       @return Wavefront points
    */
    const std::vector<VertexID> & growSurfaceTriangleFull(VertexID point, TriangleID nbTriangles = 0);

    /**
       Return paths that takes part into the border. The first and last points of each path is a point that take part
       into three or more paths. There is no duplicated path (even if the path is more than one time on the border.
     */
    std::vector<std::vector<VertexID> > getBorderPaths();

#ifndef SWIG
    /**
       A string version of the wavefront
    */
    friend std::ostream & operator << (std::ostream & f, const WaveFront & wf);
#endif

    /**
       Removing the forgotten tree parts of the wavefront
     */
    void removeTreeParts();
  };
}

#endif
