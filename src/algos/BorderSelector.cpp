#include "BorderSelector.h"

using namespace Taglut;

BorderSelector::BorderSelector(Mesh & mesh_t, bool triangles) : mesh(mesh_t), vtriangles(triangles) {
  mesh.clear();
}

BorderSelector::~BorderSelector() {

}


void BorderSelector::operator() (VertexID id1, VertexID id2, const Mesh & omesh) {
  if (omesh.isBoundaryEdge(id1, id2)) {
    std::map<VertexID, VertexID>::iterator it1 = translation.find(id1);
    std::map<VertexID, VertexID>::iterator it2 = translation.find(id2);

    if (it1 == translation.end()) {
      translation[id1] = mesh.getNbPoints();
      mesh.addPoint(omesh.point(id1));
      it1 = translation.find(id1);
      mesh.point((*it1).second).clearNeighboursAndTriangles();
    }

    if (it2 == translation.end()) {
      translation[id2] = mesh.getNbPoints();
      mesh.addPoint(omesh.point(id2));
      it2 = translation.find(id2);
      mesh.point((*it2).second).clearNeighboursAndTriangles();
    }

    if (vtriangles) {
      Triangle t((*it1).second, (*it1).second, (*it2).second);
      mesh.addTriangle(t);
    }
    else {
      mesh.point((*it1).second).addNeighbour((*it2).second);
      mesh.point((*it2).second).addNeighbour((*it1).second);
    }
  }

}


