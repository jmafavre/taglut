/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Graph.h"
#include "Exception.h"

using namespace Taglut;

Graph::Graph() {

}

Graph::Graph(const Graph & g) : std::deque<Node>(g) {

}


Node & Graph::addNode(int flag) {
  push_back(Node(size(), flag));

  return back();
}


void Graph::addEdge(const NodeID & n1, const NodeID & n2) {
  if ((n1 >= size()) || (n2 >= size()))
    throw Exception("Unknown node");

  (*this)[n1].addNeighbour(n2);
  if (n1 != n2)
    (*this)[n2].addNeighbour(n1);
}

void Graph::addEdge(Node & n1, Node & n2) {
  addEdge(n1.getId(), n2.getId());
}


std::deque<std::pair<NodeID, NodeID> > Graph::getEdges() const {
  std::deque<std::pair<NodeID, NodeID> > result;

  for(Graph::const_iterator n = begin(); n != end(); ++n) {
    NodeID nid = (*n).getId();
    for(std::deque<NodeID>::const_iterator b = (*n).getNeighbours().begin(); b != (*n).getNeighbours().end(); ++b)
      if (nid <= (*this)[*b].getId())
	result.push_back(std::pair<NodeID, NodeID>(nid, (*this)[*b].getId()));
  }

  return result;
}
