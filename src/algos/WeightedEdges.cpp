/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CNR-IMATI
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "WeightedEdges.h"
#include "Mesh.h"
#include "Exception.h"

using namespace Taglut;

WeightedEdges::WeightedEdges(const Mesh & mesh_t) : mesh(mesh_t) {

}

WeightedEdges::WeightedEdges(const WeightedEdges & we) : weights(we.weights), mesh(we.mesh) {

}

WeightedEdges::~WeightedEdges() {
}

WeightedEdges::WeightedEdges(const WeightedEdges & we, const Mesh & mesh_l) : weights(we.weights), mesh(mesh_l) {
  std::cout << "w: " << weights.size() << std::endl;
  if (mesh_l.getNbPoints() != we.mesh.getNbPoints())
    throw Exception("WeightedEdges(2): wrong number of points in the new mesh");
}


const double & WeightedEdges::operator()(VertexID v1, VertexID v2) const {
  std::map<VertexID, double>::const_iterator i = weights.find(getEdgeId(v1, v2));
  if (i == weights.end())
    throw Exception("operator()(2): unknown element");
  else {
    return (*i).second;
  }
}

double & WeightedEdges::operator()(VertexID v1, VertexID v2) {
  return weights[getEdgeId(v1, v2)];
}
