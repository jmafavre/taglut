#include "Mesh.h"
#include "FlagLogger.h"

using namespace Taglut;

FlagLogger::FlagLogger() { }

void FlagLogger::logFlags(const Mesh & mesh) {
  tflags.clear();
  pflags.clear();

  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    pflags.push_back((*p).getFlag());
  for(Mesh::const_triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t)
    tflags.push_back((*t).getFlag());
}

void FlagLogger::restoreFlags(Mesh & mesh) const {
  if (pflags.size() != mesh.getNbPoints())
    throw Exception("restoreFlags(1): wrong number of points");
  if (tflags.size() != mesh.getNbTriangles())
    throw Exception("restoreFlags(1): wrong number of triangles");

  std::vector<int>::const_iterator fp = pflags.begin();
  for(Mesh::point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p, ++fp)
    (*p).setFlag(*fp);
  std::vector<int>::const_iterator ft = tflags.begin();
  for(Mesh::triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t, ++ft)
    (*t).setFlag(*ft);

}
