/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <sstream>
#include <list>
#include <map>

#include "ClusterGenerator.h"
#include "Vertex.h"

using namespace Taglut;

/**
   cluster comparator based on ratio values
*/
namespace Taglut {
  struct clusterComparator {
  private:
    std::map<unsigned int, ClusterDescriptor> * cDesc;

  public:
    /**
       set the cluster descriptors list
    */
    void setCDesc(std::map<unsigned int, ClusterDescriptor> & cDesc_t) {
      (*this).cDesc = &cDesc_t;
    }

    /**
       comparison operator
    */
    bool operator()(unsigned int a, unsigned int b) {
      return (*cDesc)[a].getMean() < (*cDesc)[b].getMean();
    }

  };
}

/*
  ClusterDescriptor methods
*/

ClusterDescriptor::ClusterDescriptor() {
  nbPMin = 0;
  germ = 0;
  sum = 0;
  squareSum = 0;
  id = 0;
  mesh = NULL;
}

void ClusterDescriptor::setMesh(const Mesh * mesh_t) {
  (*this).mesh = mesh_t;
}


void ClusterDescriptor::setGerm(VertexID germ_t) {
  (*this).germ = germ_t;
}

VertexID ClusterDescriptor::getGerm() const {
  return germ;
}

void ClusterDescriptor::setMinPoints(VertexID nbPMin_t) {
  (*this).nbPMin = nbPMin_t;
}

unsigned int ClusterDescriptor::getId() const {
  return id;
}


void ClusterDescriptor::setId(unsigned int id_t) {
  (*this).id = id_t;
}

void ClusterDescriptor::addPoint(VertexID id_t, double value) {
  push_back(id_t);
  sum += value;
  squareSum += value * value;
}


void ClusterDescriptor::merge(const ClusterDescriptor & c) {
  for(std::vector<VertexID>::const_iterator i = c.begin(); i != c.end(); ++i)
    push_back(*i);
  sum += c.sum;
  squareSum += c.squareSum;
}

double ClusterDescriptor::getMean() const {
  if (size() == 0)
    return 0;
  else
    return sum / size();
}

double ClusterDescriptor::getVariance() const {
  if (size() <= 1)
    return std::numeric_limits<double>::max();
  else {
    double mean = getMean();
    return squareSum / size() - mean * mean;
  }
}

VertexID ClusterDescriptor::getNbPoints() const {
  return size();
}

bool ClusterDescriptor::hasPoint(VertexID idP) const {
  for(ClusterDescriptor::const_iterator p = begin(); p != end(); ++p)
    if (*p == idP)
      return true;
  return false;
}

bool ClusterDescriptor::hasEnougthPoints() const {
  return size() >= nbPMin;
}

double ClusterDescriptor::getAngleJunction(VertexID pidoutside, VertexID pidinside) const {
  double result = 0.0;
  unsigned int nbSum = 0;
  const Point3D & pin = (*mesh).point(pidinside);
  const Point3D & pout = (*mesh).point(pidoutside);
  for(std::deque<TriangleID>::const_iterator t = pin.getTriangles().begin(); t != pin.getTriangles().end(); ++t) {
    const Triangle & tr = (*mesh).triangle(*t);
    if (((unsigned int)(*mesh).point(tr.getP1()).getFlag() == id) &&
	((unsigned int)(*mesh).point(tr.getP2()).getFlag() == id) &&
	((unsigned int)(*mesh).point(tr.getP3()).getFlag() == id)) {

      result += Coord3D::angleVector((*mesh).computeTriangleNormal(*t), pout - pin) - M_PI / 2;
      ++nbSum;
    }
  }

  if (nbSum == 0) { // no triangle inside
    for(std::deque<VertexID>::const_iterator nb = pin.getNeighbours().begin(); nb != pin.getNeighbours().end(); ++nb) {
      const Point3D & nbp = (*mesh).point(*nb);
      if ((unsigned int)nbp.getFlag() == id) {
	result += pin.angle(pout, nbp) - M_PI;
	++nbSum;
      }
    }
  }

  if (nbSum == 0.0)
    return M_PI;

  return result / nbSum;
}

double ClusterDescriptor::getAngleJunction(VertexID pid) const {
  double result = std::numeric_limits<double>::max();
  for(std::deque<VertexID>::const_iterator c = (*mesh).point(pid).getNeighbours().begin(); c != (*mesh).point(pid).getNeighbours().end(); ++c)
    if ((unsigned int)(*mesh).point(*c).getFlag() == id) {
      double value = getAngleJunction(pid, *c);
      if (value < result)
	result = value;
    }
  return result;
}

/*
  ClusterMembership methods
*/

ClusterMembership::~ClusterMembership() {}

bool ClusterMembership::isMember(VertexID, double, const ClusterDescriptor & cd) {
  return (!cd.hasEnougthPoints());
}


/*
  ClusterMembershipratioSimple methods
*/
ClusterMembershipRatioSimple::~ClusterMembershipRatioSimple() {}

ClusterMembership * ClusterMembershipRatioSimple::clone() const {
  return new ClusterMembershipRatioSimple();
}

bool ClusterMembershipRatioSimple::isMember(VertexID, double newValue, const ClusterDescriptor & cd) {
  if (cd.getNbPoints() < 2)
    return true;
  else {
    double mean = cd.getMean();
    double variance = cd.getVariance();
    return ((newValue >= mean - variance) && (newValue <= mean + variance));
  }
}

/*
  ClusterMembershipRatioAllNb methods
*/
ClusterMembershipRatioAllNb::~ClusterMembershipRatioAllNb() {}

ClusterMembership * ClusterMembershipRatioAllNb::clone() const {
  return new ClusterMembershipRatioAllNb();
}

bool ClusterMembershipRatioAllNb::isMember(VertexID, double newValue, const ClusterDescriptor & cd) {
  if (!cd.hasEnougthPoints())
    return true;
  else {
    double mean = cd.getMean();
    double variance = cd.getVariance();
    return ((newValue >= mean - variance) && (newValue <= mean + variance));
  }
}



/*
  ClusterMembershipCurvature methods
*/
ClusterMembershipCurvature::ClusterMembershipCurvature(double maxCurv_t) : maxCurv(maxCurv_t) {
}

ClusterMembershipCurvature::~ClusterMembershipCurvature() {}

ClusterMembership * ClusterMembershipCurvature::clone() const {
  return new ClusterMembershipCurvature(maxCurv);
}

bool ClusterMembershipCurvature::isMember(VertexID pointID, double, const ClusterDescriptor & cd) {
  if (!cd.hasEnougthPoints()) // adding all the point arround the first one.
    return true;
  else
    return fabs(cd.getAngleJunction(pointID)) < maxCurv;
}

/*
  ClusterMembershipCurvatureOrRatio methods
*/
ClusterMembershipCurvatureOrRatio::ClusterMembershipCurvatureOrRatio(double maxCurv_t) : maxCurv(maxCurv_t) {
}

ClusterMembershipCurvatureOrRatio::~ClusterMembershipCurvatureOrRatio() {}

ClusterMembership * ClusterMembershipCurvatureOrRatio::clone() const {
  return new ClusterMembershipCurvatureOrRatio(maxCurv);
}

bool ClusterMembershipCurvatureOrRatio::isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd) {
  if (!cd.hasEnougthPoints()) // adding all the point arround the first one.
    return true;
  else if (fabs(cd.getAngleJunction(pointID)) < maxCurv)
    return true;
  else {
    double mean = cd.getMean();
    double variance = cd.getVariance();
    return ((newValue >= mean - variance) && (newValue <= mean + variance));
  }
}


/*
  ClusterMembershipCurvatureAndRatio methods
*/
ClusterMembershipCurvatureAndRatio::ClusterMembershipCurvatureAndRatio(double maxCurv_t) : maxCurv(maxCurv_t) {
}

ClusterMembershipCurvatureAndRatio::~ClusterMembershipCurvatureAndRatio() {}

ClusterMembership * ClusterMembershipCurvatureAndRatio::clone() const {
  return new ClusterMembershipCurvatureAndRatio(maxCurv);
}

bool ClusterMembershipCurvatureAndRatio::isMember(VertexID pointID, double newValue, const ClusterDescriptor & cd) {
  if (!cd.hasEnougthPoints()) // adding all the point arround the first one.
    return true;
  else {
    double mean = cd.getMean();
    double variance = cd.getVariance();
    bool result = ((newValue >= mean - variance) && (newValue <= mean + variance));
    return result && (fabs(cd.getAngleJunction(pointID)) < maxCurv);
  }

}

/*
  ClusterGenerator methods
*/

ClusterGenerator::ClusterGenerator(const ClusterGenerator & cg) : MeshManipulator(*(cg.mesh)) {
  cMembership = NULL;
  ratios = NULL;
  *this = cg;
}

ClusterGenerator::ClusterGenerator(Mesh * mesh_t, double * ratios_t) : MeshManipulator(*mesh_t), ratios(ratios_t) {
  clear();
  assert(ratios != NULL);
  done = false;
  for(std::vector<Point3D>::iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    (*i).setFlag(0);
  cMembership = new ClusterMembershipRatioSimple();
}

ClusterGenerator::ClusterGenerator() {
  ratios = NULL;
  cMembership = NULL;
  mesh = NULL;
}


ClusterGenerator::ClusterGenerator(Mesh * mesh_t, double * ratios_t,
				   const ClusterMembership & cm) : MeshManipulator(*mesh_t), ratios(ratios_t) {
  assert(ratios_t != NULL);
  done = false;
  for(std::vector<Point3D>::iterator i = (*mesh_t).point_begin(); i != (*mesh_t).point_end(); ++i)
    (*i).setFlag(0);
  cMembership = cm.clone();
}


ClusterGenerator::~ClusterGenerator() {
  clear();
}

ClusterGenerator & ClusterGenerator::operator=(const ClusterGenerator & cg) {
  clear();

  mesh = cg.mesh;
  ratios = cg.ratios;
  done = cg.done;
  cDesc = cg.cDesc;
  if (cg.cMembership != NULL)
    cMembership = (*(cg.cMembership)).clone();
  else
    cMembership = NULL;

  return *this;
}

void ClusterGenerator::setData(Mesh * mesh_t, double * ratios_t) {
  (*this).mesh = mesh_t;
  (*this).ratios = ratios_t;
  done = false;
  cDesc.clear();
}

void ClusterGenerator::removeClusters(const std::list<unsigned int> & clusters) {
  for(std::list<unsigned int>::const_iterator r = clusters.begin(); r != clusters.end(); ++r) {
    Vertex & germ = (*mesh).point(cDesc[*r].getGerm());
    computeCCUsingPointsSimple(germ.getId(), germ.getFlag(), 0);
    cDesc.erase(*r);
  }
}

void ClusterGenerator::removeSmallRatioClusters(unsigned int nb) {
  clusterComparator cComp;
  cComp.setCDesc(cDesc);
  std::list<unsigned int> idClusters;
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd)
    idClusters.push_front((*cd).first);
  idClusters.sort(cComp);

  for(unsigned int i = 0; (i < nb) && (idClusters.size() != 0); ++i)
    idClusters.pop_back();
  removeClusters(idClusters);
}


void ClusterGenerator::removeSmallRatioClustersByRatio(double ratio) {
  std::list<unsigned int> removed;

  /** first compute clusters that will be removed */
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd)
    if ((*cd).second.getMean() < ratio)
      removed.push_front((*cd).first);

  /** then remove it */
  removeClusters(removed);

}

void ClusterGenerator::removeHightRatioClustersByRatio(double ratio) {
  std::list<unsigned int> removed;

  /** first compute clusters that will be removed */
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd)
    if ((*cd).second.getMean() > ratio)
      removed.push_front((*cd).first);

  /** then remove it */
  removeClusters(removed);

}

void ClusterGenerator::removeSmallerClusters() {
  std::list<unsigned int> removed;

  /** first compute clusters that will be removed */
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd)
    if ((*cd).second.getNbPoints() <= (*mesh).point((*cd).second.getGerm()).getNbNeighbours() + 1)
      removed.push_front((*cd).first);

  /** then remove it */
  removeClusters(removed);

}

void ClusterGenerator::clustering(const std::vector<VertexID> & centers_t, const enum ClusteringMethod & cMethod, double maxCurv) {
  assert(mesh != NULL);

  if (cMembership != NULL)
    delete cMembership;

  if ((cMethod == CMratioLog) || (cMethod == CMcurvatureOrRatioLog) || (cMethod == CMcurvatureAndRatioLog)) {
    double * r = ratios;
    for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i, ++r)
      (*r) = log(*r);
  }

  switch (cMethod) {
  case CMcurvatureAndRatioLog:
  case CMcurvatureAndRatio:
    cMembership = new ClusterMembershipCurvatureAndRatio(maxCurv);
    break;
  case CMcurvatureOrRatioLog:
  case CMcurvatureOrRatio:
    cMembership = new ClusterMembershipCurvatureOrRatio(maxCurv);
    break;
  case CMcurvature:
    cMembership = new ClusterMembershipCurvature(maxCurv);
    break;
  case CMratioLog:
  case CMratiostartall:
    cMembership = new ClusterMembershipRatioAllNb();
    break;
  default:
  case CMratiosimple:
    cMembership = new ClusterMembershipRatioSimple();
  }


  (*this).centers = centers_t;
  unsigned int nbloops = 0;
  std::deque<VertexID> open;

  // init flags and build clusterdescriptors for centers
  (*mesh).setPointFlag(0);

  unsigned int idC = 1;
  for(std::vector<VertexID>::const_iterator c = centers_t.begin(); c != centers_t.end(); ++c) {
    cDesc[idC].setMinPoints((*mesh).point(*c).getNbNeighbours() + 1);
    cDesc[idC].setMesh(mesh);
    cDesc[idC].setGerm(*c);
    cDesc[idC].addPoint(*c, ratios[*c]);
    cDesc[idC].setId(idC);
    (*mesh).point(*c).setFlag(idC);
    open.push_front(*c);
    ++idC;
  }

  while(open.size() != 0) {
    VertexID cptAdded = 0;
    VertexID cptOutside = 0;
    bool merged = false;
    ++nbloops;

    VertexID point = open.front();
    open.pop_front();
    Vertex & v = (*mesh).point(point);
    int vflag = v.getFlag();
    for(std::deque<VertexID>::const_iterator vnb = v.getNeighbours().begin();
	vnb != v.getNeighbours().end(); ++vnb) {
      int vnbflag = (*mesh).point(*vnb).getFlag();
      double vnbratio = ratios[*vnb];
      if (vnbflag == 0) {
	if(cDesc[vflag].getId() != (unsigned int)vflag)
	  throw Exception("Wrong cluster descriptor id.");
	if ((*cMembership).isMember(*vnb, vnbratio, cDesc[vflag])) {
	  cDesc[vflag].addPoint(*vnb, vnbratio);
	  (*mesh).point(*vnb).setFlag(vflag);
	  ++cptAdded;
	  open.push_back(*vnb);
	  nbloops = 0;
	}
	else
	  ++cptOutside;
      }
      else if (vnbflag != vflag) { // link with another cluster
	double cratio = cDesc[vnbflag].getMean();
	if ((*cMembership).isMember(*vnb, cratio, cDesc[vflag])) {
	  cDesc[vflag].merge(cDesc[vnbflag]);
	  computeCCUsingPointsSimple(*vnb, vnbflag, vflag);
	  cDesc.erase(vnbflag);
	  merged = true;
	}
      }
    }
    if (merged || (cptOutside != 0))
      open.push_back(point);
    else
      nbloops = 0;

    // if all points has been checked without changement
    if (nbloops > open.size())
      break;
  }


  // adjust cluster ids
  unsigned int i = 0;
  for(std::map<unsigned int, ClusterDescriptor>::iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd)
    (*cd).second.setId(i++);

  done = true;
}


std::vector<VertexID> ClusterGenerator::getGerms() const {
  std::vector<VertexID> result;
  if (!done)
    return result;

  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin();
      cd != cDesc.end(); ++cd)
    result.push_back((*cd).second.getGerm());


  return result;
}

std::vector<std::vector<VertexID> > ClusterGenerator::getClusters() const {
  std::vector<std::vector<VertexID> > result;

  // build the list of clusters
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin();
      cd != cDesc.end(); ++cd) {
    result.push_back((*cd).second);
  }

  return result;
}


void ClusterGenerator::clear() {
  if (cMembership != NULL) {
    delete(cMembership);
    cMembership = NULL;
  }
  cDesc.clear();

}

unsigned int ClusterGenerator::getNbClusters() const {
  return cDesc.size();
}

unsigned int ClusterGenerator::getVertexFlag(unsigned int idV) {
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd)
    if ((*cd).second.hasPoint(idV))
      return (*cd).second.getId();
  return 0;
}


std::string ClusterGenerator::getInfos() const {
  std::ostringstream str;

  str << " Number of clusters: " << cDesc.size() << std::endl;
  unsigned int i = 0;
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd) {
    str << "  Cluster #" << i << " (" << (*cd).first << "): " << (*cd).second.getNbPoints() << " points. Mean ratio: "
	<< (*cd).second.getMean() << ", variance ratio: " << (*cd).second.getVariance() << std::endl;
    ++i;
  }

  return str.str();
}

void ClusterGenerator::setFlagsOnMesh() {
  (*mesh).setPointFlag(0);

  int flag = 1;
  for(std::map<unsigned int, ClusterDescriptor>::const_iterator cd = cDesc.begin(); cd != cDesc.end(); ++cd, ++flag)
    for(ClusterDescriptor::const_iterator i = (*cd).second.begin(); i != (*cd).second.end(); ++i)
      (*mesh).point(*i).setFlag(flag);


}
