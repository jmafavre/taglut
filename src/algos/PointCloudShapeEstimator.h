/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_CLOUD_NORMAL_ESTIMATOR
#define POINT_CLOUD_NORMAL_ESTIMATOR

#include <assert.h>
#include <vector>
#include <map>
#include <algorithm>
#include <limits>

#include "PointCloudOctree.h"
#include "PointCloud.h"
#include "Point3D.h"


namespace Taglut {

  /**
     @class PointCloudShapeEstimator
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @brief Normal estimator of a point cloud
  */
  class PointCloudShapeEstimator {
  public:

    /**
       available methods to compute normal axes
    */
    enum NormalMethod { NMMainCC, NMPCA, NMMainCCandPCA, NMPCAandMainCC };

    /**
       available methods to compute sphere radius
    */
    enum RadiusMethod { RMEstimation, RMNbNeighbours, RMValue };

  private:
    /** Manipulated point cloud */
    PointCloud & pCloud;
    /** Octree to improve the point cloud manipulation */
    PointCloudOctree octree;

    /** resolution of the sphere used for the offset computation */
    double sphere_resolution;
    /** radius method */
    enum RadiusMethod rMethod;
    /** normal method */
    enum NormalMethod nMethod;
    /** Use an octree to speedup the normal computation */
    bool useOctree;

    /*
       options of the RMEstimation method
    */
    /** radius method */
    double ratio;
    /** maximum angle value allowed between a neighbour and a normal on a point on the sphere (if higher, distance = infty) */
    double maxAngle;

    /*
       options of the RMValue method
    */
    /** radius is computed by multiply the smallest distance between points on the cloud by value_sphere */
    double value_sphere;

    /*
       options of the RMNbNeighbours method
    */
    /** radius is computed with \p number_neighbours intersection with neighbour spheres  */
    double number_neighbours;

    /*
      options of the normal computation
    */
    /** Number of points on the circles used for sphere edges (improvement of the normal estimator, not very efficient)  */
    unsigned int nbPointsOnSphereEdges;

    /*
      options of the boundary computation
    */
    /** angle between normal and pseudo-normal used to estimate the boundary property */
    double angle_error;

    /** Computed normals with the second method */
    std::vector<Coord3D> normalsBis;
    /** Computed neighbours */
    std::vector<std::map<VertexID, unsigned int> > neighbours;
    /** Computed radii */
    std::vector<double> radii;
    /** Computed local distortions */
    std::vector<double> localDistortions;
    /** Computed local boundary distortions */
    std::vector<double> localBoundaryDistortions;
    /** Computed boundary flags */
    std::vector<bool> boundaryFlags;
    /** Computed boundary neighbours */
    std::vector<std::pair<VertexID, VertexID> > bNeighgours;

    std::vector<Coord3D> computeEdgePointsOnSphere(const Point3D & center, double radius, unsigned int nbPointsOnSphereEdges);


    /**
       Compute a point on the circle of radius \p radius, on the plane orthogonal to AB and of center C.
     */
    const Coord3D computeFirstEdgePoint(const Coord3D & A, const Coord3D & B, const Coord3D & C, double radius);


    void computeNeighboursWithoutOctree(const Point3D & center, double radius, Mesh & sphere);

    void computeNeighboursUsingFlagsWithoutOctree(const Point3D & center, double radius, const Mesh & sphere);

    void computeNeighbours(const Point3D & center, double radius, Mesh & sphere);

    void computeNeighboursUsingFlags(const Point3D & center, double radius, const Mesh & sphere);

    void computeNormalUsingPCA(VertexID id, std::vector<Coord3D> & lNormals, const Mesh & sphere, const std::vector<Coord3D> & otherPoints);

    void computeNormalUsingMainCC(VertexID id, std::vector<Coord3D> & lNormals, Mesh & sphere);

    double computeInsideOutsideDistance(const Point3D & center, const Coord3D & vector, double maxAngle);

    void computeBoundaryFlags(double angle_error, const std::vector<Coord3D> & normals, const std::vector<Coord3D> & pseudoNormals);

    void clearData();

    void clearBoundaryData();

    void clearNormalData();

    double estimateRadiusByNeighbours(const Point3D & center);

    double estimateRadius(Mesh & sphere, const Point3D & center, std::vector<VertexID> & sPoints, std::vector<double> & ioThreshold);

  public:

    /**
       Constructor with radius method (using default values)
    */
    PointCloudShapeEstimator(PointCloud & pCloud, double sphere_resolution = 2,
			     bool useOctree = true,
			     const enum NormalMethod & nMethod = NMPCA,
			     unsigned int maxNbInOctree = 18, unsigned int octreeDepth = 2);


    /**
	Set the number of points on the circles used for sphere edges (improvement of the normal estimator, not very efficient)
	All data structures estimated using this value are cleared.
    */
    void setNbPointsOnShpereEdges(unsigned int nbPointsOnSphereEdges = 0);

    /**
	Set angle between normal and pseudo-normal used to estimate the boundary property 
	All data structures estimated using this value are cleared.
    */
    void setAngleErrorOnBoundary(double angle_error = M_PI / 6);

    /**
       Select the estimation method for radius computation.
       Data structures are cleared.
    */
    void radiusByEstimation(double ratio = .5, double maxAngle = M_PI / 2);

    /**
       Select the fixed method for radius computation.
       Data structures are cleared.
    */
    void radiusByValue(double value = 1.0);

    /**
       Select the neighbours method for radius computation.
       Data structures are cleared.
    */
    void radiusByNbNeighbours(unsigned int number = 18);


    /**
       Compute normals of the points using the given corresponding mesh
    */
    const std::vector<Coord3D> & computeNormalsUsingSurface(const Mesh & mesh);

    /**
       Compute normals of the points using a discrete offset method
    */
    const std::vector<Coord3D> & computeNormalsOffsetMethod();

    /**
       Compute a local distortion estimation using the normals of the neighbours.
       Normals are computed if they are missing.
    */
    const std::vector<double> & computeLocalDistortion();

    /**
       Compute a shape estimator using the normals of the neighbours of every point.
       Normals and local distortions are computed if they are missing.
    */
    double computeShapeEstimator();


    /**
       Compute a local boundary distortion estimation using the normals of the neighbours.
       Normals are computed if they are missing.
    */
    const std::vector<double> & computeLocalBoundaryDistortion();

    /**
       Compute an estimation of the boundary flags using the normals of the neighbours.
       Normals are computed if they are missing.
    */
    const std::vector<bool> & computeBoundaryFlags();




    /**
       Compute an estimation of the boundary shape using the local boundary distortion.
       Normals and boundary flags are computed if they are missing.
    */
    double computeBoundaryShapeEstimator();

    /**
       Accessor
    */
    inline const PointCloud & getPointCloud() const { return pCloud; }
    /**
       Accessor
    */
    inline const std::vector<Coord3D> & getNormals() const { return pCloud.getNormals(); }
    /**
       Accessor
    */
    inline const std::vector<std::map<VertexID, unsigned int> > & getNeighbours() const { return neighbours; }
    /**
       Accessor
    */
    inline const std::vector<double> & getRadii() const { return radii; }
    /**
       Accessor
    */
    inline const std::vector<double> & getLocalDistortions() const { return localDistortions; }
    /**
       Accessor
    */
    inline const std::vector<double> & getLocalBoundaryDistortions() const { return localBoundaryDistortions; }
    /**
       Accessor
    */
    inline const std::vector<bool> & getBoundaryFlags() const { return boundaryFlags; }
    /**
       Accessor
    */
    inline const std::vector<std::pair<VertexID, VertexID> > & getBoundaryNeighbours() const { return bNeighgours; }

    /**
       Accessor
    */
    inline double getMinLocalDistortion() const { return *std::min_element(localDistortions.begin(), localDistortions.end()); }

    /**
       Accessor
    */
    inline double getMaxLocalDistortion() const { return *std::max_element(localDistortions.begin(), localDistortions.end()); }

    /**
       Accessor
    */
    double getMeanLocalDistortion() const;

    /**
       Accessor
    */
    double getMedianLocalDistortion() const;

    /**
       Accessor. Compute the mean of the local disortions only for points flag values != 0.
    */
    double getMeanLocalDistortionFlags();

    /**
       Accessor. Compute the mean of the local disortions only for points with radius smaller than alpha * mindist, where mindist is the minimal distance between points on the cloud.
    */
    double getMeanLocalDistortionThreshold(double alpha);

    /**
       Accessor
    */
    inline double getMinLocalBoundaryDistortion() const {
      double result = std::numeric_limits<double>::max();
      for(std::vector<double>::const_iterator d = localBoundaryDistortions.begin(); d != localBoundaryDistortions.end(); ++d)
	if ((*d >= 0.0) && (*d < result))
	  result = *d;
      return result;
    }

    /**
       Accessor
    */
    inline double getMaxLocalBoundaryDistortion() const { return localBoundaryDistortions.size() == 0 ? 0.0 : *std::max_element(localBoundaryDistortions.begin(), localBoundaryDistortions.end()); }

  };

}

#endif
