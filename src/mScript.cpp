/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include <libxml/xmlreader.h>

#include "CImgUse.h"
#include "Mesh.h"
#include "ScriptInterpreter.h"

#include "Exception.h"
#include "FileTools.h"
#include "Length.h"
#include "Messages.h"

using namespace std;
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  objectName = NULL;
static char*  filename_output  = NULL;
static char*  filename_script  = NULL;
static char*  filename_xmlSchema  = NULL;
static int    help = 0;
static int    helpxml = 0;
static int    noValidation = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or dicom file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "script", 's', POPT_ARG_STRING, &filename_script, 0, "Script file (xml)", NULL},
  { "xml-schema", 'x', POPT_ARG_STRING, &filename_xmlSchema, 0, "XML Schema for script validation (facultative)", NULL},
  { "no-validation", 'v', POPT_ARG_NONE, &noValidation, 0, "Skip XML validation", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output file (image, cst)", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  { "help-xml", 'h', POPT_ARG_NONE, &helpxml, 0, "Show a short xml format introduction", NULL},
  POPT_TABLEEND
};

using namespace std;



/************* Main function *****************/

int main (int argc, const char **argv) {

#ifndef LIBXML_READER_ENABLED
  cerr << "Error: need xml support (libxml reader)." << endl;
  return -1;
#else

  poptContext context = poptGetContext("mScript", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh manipulator script", "Using xml script to manipulate mesh (cut, unfold, display, etc).");
    hMsg << "Input: xml script, mesh to manipulate (available format: VRML)";
    hMsg << "Output: display result data or save it (available format: VRML, cset, image)";
    cout << hMsg << endl;
    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (helpxml != 0) {
    cout << " -- ( XML format ) --" << endl;
    cout << " " << endl;
    cout << ScriptInterpreter::helpScript();
    return 0;
  }

  if (filename_script == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify a script file (xml)." << endl;
    return 1;
  }

  try {


    // create mesh
    Mesh mesh;

    // load file if needed
    if (filename_input != NULL) {
      // then load input mesh
      cout << "Loading mesh using parameter (" << filename_input << ")..." << endl;

      if (mesh.isLoadFormat(filename_input)) { // mesh file
	try {
	  cout << "Building mesh..." << endl;
	  mesh.load(filename_input, objectName);
	} catch (Exception e) {
	  cerr << e << endl;
	  return -1;
	}
      }
      else if (FileTools::extension(filename_input) == "dcm") { // dicom file
	CImg<> points;
	CImgList<VertexID> primitives;
	CImg<> img(filename_input);
	points = img.get_isosurface3d(primitives, 0.5);
	cout << "Building mesh..." << endl;
 	mesh.buildMeshTriangulate(points, primitives);
      }
      else
	cerr << " Warning (filename from parameters): unknown format." << endl;
    }

    // init and run script
    string xmlSchema;
    if (filename_xmlSchema == NULL)
      xmlSchema = FileTools::concat(FileTools::path(argv[0]), ScriptInterpreter::SchemaFileName);
    else
      xmlSchema = string(filename_xmlSchema);
    ScriptInterpreter sInterpreter(filename_script, noValidation == 0, mesh, xmlSchema);
    sInterpreter.runScript();

    // saving file if needed
    if (filename_output != NULL) {
      if (mesh.isSaveFormat(filename_output)) {
	Mesh rMesh = sInterpreter.getMesh();
	cout << "Saving mesh..." << endl;
	rMesh.save(filename_output);
      }
      else {
	cerr << "Error: unknown format (" << filename_output << ")" << endl;
	return 1;
      }
    }

  }

  catch (Exception e) {
    cerr << e << endl;
    return 2;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }
#endif

  return 0;
}
