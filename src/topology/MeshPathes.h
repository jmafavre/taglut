/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESHPATHES
#define MESHPATHES

#include "MeshManipulator.h"

#ifndef SWIG
#include <map>
#endif

namespace Taglut {
  class Mesh;
  class NLoop;

  /**
     @class MeshPathes
     Mesh manipulator using pathes
  */
  class MeshPathes : public std::deque<std::deque<VertexID> >, public MeshManipulator {
  public:

    /**
       Available methods for non separating cycle detection
    */
    enum OptimEC { OECnoTrunc, OECTruncMin, OECTruncMax, OECTruncMinMax, OECsimple, OECbyStrip, OECbyStripAndPredCut, OECApproximation };

    /**
       base point corresponding to local minima and used during the search of the shortest non separating loop.
    */
    std::vector<VertexID> minLocalBasePoints;

  protected:

    /**
       @class CycleDescriptor
       Cycle descriptor
    */
#ifndef SWIG
    class CycleDescriptor;
#endif

    /**
       Log file
    */
    std::string logFile;

    /**
       Resulting Mesh used by current MeshPathes
    */
    Mesh cMesh;

    /**
       Number of Boundaries
    */
    unsigned int nbBoundaries;
    /**
       Size of the fundamental group
    */
    unsigned int fGroupSize;

    /**
       after cut, projection[newPointId] = oldPointId
    */
    std::map<VertexID, VertexID> projection;

    /**
       log intermediate meshes
    */
    void logCMesh(const std::string & logMesh);

    /**
       Using logFile to save pathes
    */
    void logPathes(const std::deque<std::deque<VertexID> > & pathes) const;

    /**
       For each point inside path, set flag value using parameter
       @param value Flag value
    */
    void setPathFlags(int value);


    /**
       For each path, set flag value of each point using path id + startValue.
       @param startValue Start flag value
    */
    void setPathFlagsIncremental(int startValue);


    /**
       Splitting pathes (start and end of a path can only be at start or end of another path)
    */
    void splitPathesBeginEnd();

    /**
       Add path to current meshcut and update cMesh with simpliest method: cutting mesh
       Get real resulting pathes
    */
    std::pair<std::deque<VertexID>, std::deque<VertexID> > addPathGetExactPathes(const std::deque<VertexID> & path);

    /**
       Add a front to current meshcut and update cMesh with simpliest method: cutting mesh
    */
   void addFront(const std::vector<VertexID> & front);

    /**
       First split the given path according to the boundary (a path has no boundary edge),
       then add pathes to current meshcut and update cMesh with simpliest method: cutting mesh.
    */
    void addPathSplitBefore(const std::deque<VertexID> & path);

    /**
       Add path to current meshcut and update cMesh with simpliest method: cutting mesh.
       Cutting on the boundary are allowed
       Return list of extremity points (on each side of the path)
    */
    std::deque<VertexID> addPathAllowSplitBoundary(const std::deque<VertexID> & path);

    /**
       Cut cMesh at point idCenter, with neighbours points in path defined by (idP1, idP1bis) cut before and idP2.
       idPCenterNew id id of the new point.
       @return idP1 or idP1bis
    */
    VertexID updateCutMesh(VertexID idPCenter, VertexID idP1, VertexID idP2, VertexID idPCenterNew, VertexID idP1bis);


    /**
       Cut cMesh at point idCenter, using neighbour triangles (idPCenter is extremity of path)
    */
    void updateCutMesh(VertexID idPCenter);

    /**
       Cut cMesh at point (idPOutside, idPInside), starting with triangle idT, from point idBP to point idTarget.
       @param idPOutside Cut point (id outside)
       @param idPInside Cut point (id inside)
       @param idT First triangle id
       @param idBP First point id
       @param idTarget Last point id
       @param idTargetBis Last point id (other side)
       @param firstSide true if current side is first side
       @param firstTriangle True if it is first triangle in cut
       @return idTarget or idTargetBis (found point)
    */
    VertexID cutBoundarySide(VertexID idPOutside, VertexID idPInside,
			     TriangleID idT, VertexID idBP, VertexID idTarget, VertexID idTargetBis, bool firstSide, bool firstTriangle = true);


    /**
       Get shortest non separating cycle, with pseudo-volumic approach
       // TODO (see cpp)
    */
    std::deque<VertexID> getShortestNonSeparatingCyclePseudoVolumic(double minLength,
									    const UCharCImg & insideMask,
									    const UCharCImg & region,
									    bool inside);

    /**
       Get shortest non separating cycle
    */
    std::deque<VertexID> getShortestNonSeparatingCycle(double minLength = 0.0, enum OptimEC optimMethod = OECTruncMinMax, bool merge = true);

    /**
       Get shortest non separating cycle using points as base point list
    */
    std::deque<VertexID> getShortestNonSeparatingCycleFromPoints(std::deque<VertexID> & points, double minLength = 0.0, enum OptimEC optimMethod = OECTruncMinMax, bool merge = true);

    /**
       Get shortest non separating cycle descriptor using points as base point list
    */
    CycleDescriptor getShortestNonSeparatingCycleDescFromPoints(const std::deque<VertexID> & points, double minLength = 0.0, bool merge = true);

    /**
       Add i to the path given in parameter that should have i as extremity
    */
    void addExtremity(VertexID i, std::pair<std::deque<VertexID>, std::deque<VertexID> > & result);

    /**
       Load meshpath from file
    */
    static std::deque<std::deque<VertexID> > loadFromFile(const std::string & filename);

    /**
       Return a projection of the given path on the original mesh
    */
    std::deque<VertexID> getProjection(const std::deque<VertexID> & path) const;

    /**
       Return true if the given n-loop has the good topological properties (the complementary of the n-loop is constituted of surfaces
       with a non zero genus, or with more than one boundary).
    */
    bool hasGoodTopologicalProperties(const NLoop & result);

    /**
       Return the shortest n-loop with base-point defined by \p basePoint
    */
    NLoop getShortestNLoop(VertexID basePoint, unsigned int nbPaths = 3);


    /**
       Return true if the given path (extracted from a front loop) is a single path (no other part of the front is on the same part of the mesh)
    */
    bool isSinglePath(const std::deque<VertexID> & path, const std::vector<VertexID> & front) const;

    /**
       Return true if the given path (extracted from a front loop) is a valid cutting path
    */
    bool isValidCuttingPathFromFront(const std::deque<VertexID> & path, const std::vector<VertexID> & front) const;

    /**
       Return true if the iterator on the front (and corresponding to the temporary end of path) is a valid end of path
    */
    bool isEndOfPathOnFront(const std::vector<VertexID>::const_iterator & p,
			    const std::vector<VertexID>::const_iterator & nextp,
			    const std::vector<VertexID>::const_iterator & nextp2,
			    const std::deque<VertexID> & path);

    /**
       return true if the given path (extracted from a front loop) is valid according to the given data, i.e.:
       * if the path is outside of the given region, return true
       * if the path is inside of the given region, it computes an estimation of the topological property of the
         loop: will the loop add matter or not. The \p inside parameter describe if we want or not to add matter.
    */
    bool isValidPathPseudoVolumic(const std::deque<VertexID> & path, const UCharCImg & insideMask,
				  const UCharCImg & region,
				  bool inside);

  public:
    /**
       Default constructor
    */
    MeshPathes(Mesh & mesh);

    /**
       Default constructor with length operator
    */
    MeshPathes(Mesh & mesh, const LengthEdge & lMethod);

    /**
       Copy constructor
    */
    MeshPathes(const MeshPathes & meshPathes);

    /**
       Copy constructor with other mesh
    */
    MeshPathes(const MeshPathes & meshPathes, Mesh & mesh);

    /**
       Destructor
    */
    virtual inline ~MeshPathes() { }


    /**
       set mesh (restriction: current mesh and the given mesh should have
       same number of points and triangles
    */
    void setMesh(const Mesh & mesh);

    /**
       Get number of path
    */
    unsigned int getNbPaths() const;

    /**
       Get number of edges inside path idPath
    */
    VertexID getPathLength(unsigned int idPath) const;


    /**
       Get number of edges inside pathes
    */
    VertexID getSize() const;

    /**
       Get number of points after cut mesh
    */
    VertexID getCNbPoints() const;

    /**
       Add a point to the lastest path
    */
    void addPoint(VertexID idPoint);

    /**
       Add a point to the path idPath
    */
    void addPoint(unsigned int idPath, VertexID idPoint);

    /**
       return true if edge (p1, p2) is inside a path
    */
    bool inPathEdge(VertexID p1, VertexID p2) const;

    /**
       return true if edge (p1, p2) is inside last path
    */
    bool inLastPathEdge(VertexID p1, VertexID p2) const;

    /**
       Set log file for current tool
    */
    void setLogFile(std::string logFile);

    /**
       Get shortest path from a given point to the boundary
    */
    std::deque<VertexID> getShortestPathToBoundary(VertexID point);

    /**
       Cut the mesh according to MeshPathes.
       @return Result cut mesh
    */
    Mesh & cutMesh();

    /**
       @return Result cut mesh
    */
    inline const Mesh & getCutMesh() const { return cMesh; }

    /**
       Build colors for cimg 3d output
    */
    std::deque<unsigned int> buildBorderColor(CImgList<unsigned char> & colors, CImgList<VertexID> & primitives);


    /**
       return a clone of the current object
    */
    virtual MeshPathes * clone() const;

    /**
       return a clone of the current object (using the given mesh rather than original one
    */
    virtual MeshPathes * clone(Mesh & mesh) const;


    /**
       Return true if the given file is a known format for export.
    */
    virtual bool isExportFormat(const std::string &) const { return false; }

    /**
       Export to file if the format is known.
    */
    virtual void exportToFile(const std::string &) { }

    /**
       Return id on the original mesh (using projection). If projection does not exists, return idP
    */
    VertexID getProjection(VertexID idP) const;

    /**
       Given a path, correcting the begining and the end of the path, if they has been broken
       during a previous cut (looking for same location points)
    */
    void correctingPathFull(std::deque<VertexID> & path);

    /**
       Given a path (loop), correcting the first and last point looking for points at same location, that are neighbours of
       the next point on the path (used to cut pathes that has been computed before a cutting)
    */
    void correctingPath(std::deque<VertexID> & path);

    /**
       Given a path (loop), correcting it removing the first and last edges if they are duplicated
    */
    void correctingLoop(std::deque<VertexID> & path) const;


    /**
       Given two basepoints, and a number of paths, compute the shortest valid NLoop with this given arity.
       This problem is not easy. Only a partial solution has been implemented here. It returns the shortest NLoop of its
       homotopy class, but not necessary the shortest...
     */
    NLoop getShortestValidNLoopNaive(VertexID b1, VertexID b2, unsigned int nbPaths = 3);

    /**
       Given two basepoints, and a number of paths, compute the shortest valid NLoop with this given arity.
       This problem is not easy. Only a partial solution has been implemented here. It returns the shortest NLoop of its
       homotopy class, but not necessary the shortest...
     */
    NLoop getShortestValidNLoop(VertexID b1, VertexID b2, unsigned int nbPaths = 3);


    /**
       Add path to current meshcut and update cMesh with simpliest method: cutting mesh
    */
    void addPath(const std::deque<VertexID> & path);

    /**
       Add an NLoop to the current meshcut and update cMesh with simpliest method: cutting mesh
    */
    void addNLoop(const NLoop & nloop);

  };

#ifndef SWIG
  class MeshPathes::CycleDescriptor {
  private:
    /** is a valid cycle? */
    bool isValid;

    /** base-point */
    VertexID basePoint;

    /** target point */
    VertexID targetPoint;

    /** first pred point */
    VertexID pred1;

    /** second pred point */
    VertexID pred2;

    /** path  */
    std::deque<VertexID> path;

  public:
    /**
       Construction of a non valid cycle
    */
    inline CycleDescriptor() : isValid(false) { }

    /**
       Construction of a valid cycle
    */
    inline CycleDescriptor(VertexID basePoint_l, VertexID targetPoint_l,
			   VertexID pred1_l, VertexID pred2_l,
			   const std::deque<VertexID> & path_l) : isValid(true),
								basePoint(basePoint_l), targetPoint(targetPoint_l),
								pred1(pred1_l), pred2(pred2_l), path(path_l) { }

    /**
       Copy operator
    */
    inline CycleDescriptor & operator=(const CycleDescriptor & cDesc) {
      isValid = cDesc.isValid;
      basePoint = cDesc.basePoint;
      targetPoint = cDesc.targetPoint;
      pred1 = cDesc.pred1;
      pred2 = cDesc.pred2;
      path = cDesc.path;

      return *this;
    }

    /** is valid */
    bool getIsValid() const { return isValid; }

    /** basePoint accessor */
    VertexID getBasePoint() const { return basePoint; }

    /** targetPoint accessor */
    VertexID getTargetPoint() const { return targetPoint; }

    /** pred1 accessor */
    VertexID getPred1() const { return pred1; }

    /** pred2 accessor */
    VertexID getPred2() const { return pred2; }

    /** return the path corresponding to the current cycle */
    const std::deque<VertexID> & getPath() const { return path; }

  };
#endif

}

#endif
