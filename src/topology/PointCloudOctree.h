/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_CLOUD_OCTREE
#define POINT_CLOUD_OCTREE

#include <assert.h>
#include <vector>

#include "Point3D.h"
#include "IDTypes.h"
namespace Taglut {
  class PointCloud;

  /**
     @class PointCloudOctree
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @brief Octree to manipulate point cloud data
  */
  class PointCloudOctree : public Box3D {
  private:

    /**
       @class Plane3DFunction
       @author Jean-Marie Favreau (LIMOS/ENS Cachan)
       @brief A functional description of a plane to compute direct side property
    */
    class Plane3DFunction : public Plane3D {
    private:
      enum Direction { DX, DY, DZ };
      /** Direction of the function
	  if direction = DX, f(y, z) = ay + bz + c
	  if direction = DY, f(x, z) = ax + bz + c
	  if direction = DZ, f(x, y) = ax + by + c
      */
      enum Direction direction;
      /** Coefficient */
      double a;
      /** Coefficient */
      double b;
      /** Coefficient */
      double c;
      /**
	 Side of the vector according to the plane
      */
      bool vside;
    public:
      /**
	 Default constructor using a point and a vector
      */
      Plane3DFunction(const Coord3D & point, const Coord3D & vector);



      /**
	 Return true if the given point is in the good side of the plane, according to the vector
	 used for the description of the plane.
      */
      inline bool inGoodSide(const Coord3D & p) const {
	const double f = (*this)(p);
	return (f == 0.0) || (((*this)(p) > 0.0) == vside);
      }

      /**
	 Return true if a part of the given box is in the good side of the plane, according to the vector
	 used for the description of the plane.
      */
      inline bool partInGoodSide(const Box3D & b_l) const {
	return (inGoodSide(Coord3D(b_l.getMinX(), b_l.getMinY(), b_l.getMinZ())) ||
		inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMinY(), b_l.getMinZ())) ||
		inGoodSide(Coord3D(b_l.getMinX(), b_l.getMinY(), b_l.getMaxZ())) ||
		inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMinY(), b_l.getMaxZ())) ||
		inGoodSide(Coord3D(b_l.getMinX(), b_l.getMaxY(), b_l.getMinZ())) ||
		inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMaxY(), b_l.getMinZ())) ||
		inGoodSide(Coord3D(b_l.getMinX(), b_l.getMaxY(), b_l.getMaxZ())) ||
		inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMaxY(), b_l.getMaxZ())));
      }

      /**
	 Return true if the given box is fully in the bad side of the plane, according to the vector
	 used for the description of the plane.
      */
      inline bool inBadSide(const Box3D & b_l) const {
	return (!inGoodSide(Coord3D(b_l.getMinX(), b_l.getMinY(), b_l.getMinZ())) &&
		!inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMinY(), b_l.getMinZ())) &&
		!inGoodSide(Coord3D(b_l.getMinX(), b_l.getMinY(), b_l.getMaxZ())) &&
		!inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMinY(), b_l.getMaxZ())) &&
		!inGoodSide(Coord3D(b_l.getMinX(), b_l.getMaxY(), b_l.getMinZ())) &&
		!inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMaxY(), b_l.getMinZ())) &&
		!inGoodSide(Coord3D(b_l.getMinX(), b_l.getMaxY(), b_l.getMaxZ())) &&
		!inGoodSide(Coord3D(b_l.getMaxX(), b_l.getMaxY(), b_l.getMaxZ())));
      }


      /**
	 Return the altitude of the given point according to the plane
      */
      inline double operator() (const Coord3D & p) const {
	if (direction == DX)
	  return a * p.getY() + b * p.getZ() + c - p.getX();
	else if (direction == DY)
	  return a * p.getX() + b * p.getZ() + c - p.getY();
	else // if (direction == DZ)
	  return a * p.getX() + b * p.getY() + c - p.getZ();
      }

      /**
	 Stream operator
      */
      template <typename T, typename S>
	friend std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const PointCloudOctree::Plane3DFunction & plane);

    };

    /**
       Maximum number of points allowed in the box before splitting
    */
    unsigned int maximum_points;

    /**
       Maximum splitting depth allowed from this box
    */
    unsigned int maximum_depth;

    /**
       The manipulated point cloud
    */
    const PointCloud & pCloud;

    /**
       List of points contained in the current octree box
    */
    std::vector<VertexID> points;
    /**
       List of the child boxes
    */
    PointCloudOctree ** children;

    /**
       Number of points contained in the current box, or in its children if not a leaf box.
     */
    VertexID nbPoints;

    /**
       During the constructing process, each point is added using this method
    */
    void addPoint(const Point3D & point);

    /**
       Split the current leaf box and move the points on the children structures
    */
    void split();

    /**
       Private constructor that only build the box without adding points inside
       @param pCloud The manipulated point cloud
       @param maximum_points Maximum number of points in a leaf box
       @param maximum_depth Maximum supdivision depth
       @param minx Minimum x value
       @param minx Maximum x value
       @param miny Minimum y value
       @param miny Maximum y value
       @param minz Minimum z value
       @param minz Maximum z value
    */
    PointCloudOctree(const PointCloud & pCloud, unsigned int maximum_points, unsigned int maximum_depth,
		     double minx, double maxx, double miny, double maxy, double minz, double maxz);


    /**
       Internal version after initialization of the public method
    */
    double updateRadiusFromDirection(const Point3D & center, const Coord3D & vector, const Plane3DFunction & plane, double maxAngle, double previousResult) const;

    /**
       Return distance between the given center and its nbNb-st neighbour (ordered by distance)
       Internal version after initialization of the public method
     */
    double distanceNthNeighbour(const Point3D & center, unsigned int nbNb, std::vector<std::pair<VertexID, double> > & known_points, double distance = std::numeric_limits<double>::max()) const;

    /**
       Internal version after initialization of the public method
    */
    std::pair<VertexID, double> findClosestPoint(const Coord3D & point, VertexID forbidden, double distance) const;

  public:
    /**
       Default constructor.
       @param pCloud The manipulated point cloud
       @param maximum_points Maximum number of points in a leaf box
       @param maximum_depth Maximum supdivision depth
    */
    PointCloudOctree(const PointCloud & pCloud, unsigned int maximum_points = 18, unsigned int maximum_depth = 5, bool useSmallestDistance = true);

    /**
       Destructor
    */
    ~PointCloudOctree();

    /**
       Accessor. Return true if the current box is a leaf of the tree
    */
    inline bool isLeaf() const { return children == NULL; }

    /**
       Stream operator
    */
    template <typename T, typename S>
      friend std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const PointCloudOctree & p);

    /**
       Return the size of the sphere associated to the given direction from the center point. See PointCloudShapeEstimator for more details
     */
    inline double getRadiusFromDirection(const Point3D & center, const Coord3D & vector, double maxAngle) const {
      return updateRadiusFromDirection(center, vector, Plane3DFunction(center, vector), maxAngle, std::numeric_limits<double>::max());
    }

    /**
       Return distance between the given center and its nbNb-st neighbour (ordered by distance)
    */
    inline double distanceNthNeighbour(const Point3D & center, unsigned int nbNb) const {
      assert(nbNb > 0);
      std::vector<std::pair<VertexID, double> > known_points;
      return distanceNthNeighbour(center, nbNb, known_points);
    }

    /**
       Return the id of the closest point from \p point, with id != |p forbidden.
    */
    inline std::pair<VertexID, double> findClosestPoint(const Coord3D & point, VertexID forbidden = std::numeric_limits<VertexID>::max()) const {
      return findClosestPoint(point, forbidden, std::numeric_limits<double>::max());
    }

    /**
       Return a list of the points from the cloud that are not the forbidden point, and upon which distance to the given \p point is smaller than the given \p distance
     */
    std::vector<VertexID> findPoints(const Coord3D & point, VertexID forbidden, double distance) const;

  };

  /**
     Stream operator
  */
  template <typename T, typename S>
    std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const PointCloudOctree & octree) {
    const unsigned int tr = 10 - octree.maximum_depth;
    for(unsigned int i = 0; i < tr; ++i)
      f << " ";
    f << "Octree[" << (Box3D) octree << ", " << octree.nbPoints << " points";
    if (octree.isLeaf())
      f << " [" << octree.points.size() << "]";
    else {
      for (unsigned int j = 0; j < 8; ++j) {
	f << std::endl;
	for(unsigned int i = 0; i < tr; ++i)
	  f << " ";
	f << *(octree.children[j]);
      }
    }
    f << "]";
    return f;
  }

  /**
     Stream operator
  */
  template <typename T, typename S>
    std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const PointCloudOctree::Plane3DFunction & plane) {
    f << "Function defined by " << plane.getPoint() << ", ->" << plane.getVector() << ". Direction: ";
    if (plane.direction == PointCloudOctree::Plane3DFunction::DX)
      f << "DX";
    else if (plane.direction == PointCloudOctree::Plane3DFunction::DY)
      f << "DY";
    else
      f << "DZ";
    f << ". a: " << plane.a << ", b: " << plane.b << ", c: " << plane.c;
    return f;
  }


}

#endif
