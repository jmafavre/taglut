/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include <list>
#include <set>
#include <algorithm>
#include "MeshPathes.h"
#include "MeshPart.h"
#include "Length.h"
#include "Point3D.h"
#include "Exception.h"
#include "FileTools.h"
#include "NLoop.h"
#include "WaveFront.h"
#include "FlagLogger.h"

// used during debug
#include "MeshMap.h"
#include "Display3D.h"

#ifdef USE_LIBXML
#include <libxml/xmlreader.h>
#endif

namespace Taglut {
  /**
     @class CompFirst
     A comparator for paths descriptors
  */
  template <class T>
  class CompFirst {
  public:
    /**
       Comparator of the first element of the pair
    */
    bool operator() (const std::pair<double, T> & b1,
		     const std::pair<double, T> & b2) const {
      return b1.first < b2.first;
    }

  };
}


using namespace Taglut;

MeshPathes::MeshPathes(Mesh & mesh_l):MeshManipulator(mesh_l), cMesh(mesh_l) { }

MeshPathes::MeshPathes(Mesh & mesh_l, const LengthEdge & lMethod) : MeshManipulator(mesh_l, lMethod), cMesh(mesh_l) { }

MeshPathes::MeshPathes(const MeshPathes & meshPathes) : std::deque<std::deque<VertexID> >(meshPathes), MeshManipulator(meshPathes),
							cMesh(meshPathes.cMesh),
							projection(meshPathes.projection) { }

MeshPathes::MeshPathes(const MeshPathes & meshPathes, Mesh & mesh_l) : std::deque<std::deque<VertexID> >(meshPathes),
								       MeshManipulator(meshPathes, mesh_l),
								       cMesh(meshPathes.cMesh),
								       projection(meshPathes.projection) { }



void MeshPathes::setMesh(const Mesh & mesh_l) {
  if (mesh_l.getNbPoints() != (*((*this).mesh)).getNbPoints())
    throw Exception("setMesh(1): Mesh should have same number of points");
  if (mesh_l.getNbTriangles() != (*((*this).mesh)).getNbTriangles())
    throw Exception("setMesh(1): Mesh should have same number of triangles");
  *((*this).mesh) = mesh_l;
}

MeshPathes * MeshPathes::clone() const {
  return new MeshPathes(*this);
}

MeshPathes * MeshPathes::clone(Mesh & mesh_l) const {
  return new MeshPathes(*this, mesh_l);
}

VertexID MeshPathes::getProjection(VertexID idP) const {
  if (idP < (*mesh).getNbPoints())
    return idP;
  else {
    std::map<VertexID, VertexID>::const_iterator r = projection.find(idP);
    if (r != projection.end())
      return (*r).second;
    else
      return idP;
  }
}

std::deque<VertexID> MeshPathes::getProjection(const std::deque<VertexID> & path) const {
  std::deque<VertexID> ppath;
  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p)
    ppath.push_back(getProjection(*p));

  return ppath;
}

unsigned int MeshPathes::getNbPaths() const {
  return (*this).size();
}

VertexID MeshPathes::getCNbPoints() const {
  return cMesh.getNbPoints();
}

void MeshPathes::addPoint(VertexID idPoint) {
  (*this)[(*this).size() - 1].push_front(idPoint);
}

void MeshPathes::setLogFile(std::string logFile_l) {
  (*this).logFile = logFile_l;
}


void MeshPathes::setPathFlags(int value) {
  for(std::deque<std::deque<VertexID> >::const_iterator i = (*this).begin();
      i != (*this).end(); ++i)
    for(std::deque<VertexID>::const_iterator j = (*i).begin();
	j != (*i).end(); ++j)
      (*mesh).point(*j).setFlag(value);
}


void MeshPathes::setPathFlagsIncremental(int value) {
  for(std::deque<std::deque<VertexID> >::const_iterator i = (*this).begin();
      i != (*this).end(); ++i) {
    for(std::deque<VertexID>::const_iterator j = (*i).begin();
	j != (*i).end(); ++j)
      (*mesh).point(*j).setFlag(value);
    ++value;
  }
}



std::pair<std::deque<VertexID>, std::deque<VertexID> > MeshPathes::addPathGetExactPathes(const std::deque<VertexID> & path) {
  // add path to current mesh

  (*this).push_back(path);
  bool isNewCut = ((!cMesh.point(path.front()).getIsBoundary()) && (!cMesh.point(path.back()).getIsBoundary()) &&
		   (path.front() != path.back()));


  std::pair<std::deque<VertexID>, std::deque<VertexID> > result;

  /* first cut each path */
  if (path.size() > 2) {
    std::deque<VertexID> addedPoints(path);
    addedPoints.pop_front();
    addedPoints.pop_back();
    VertexID idAdded = cMesh.addPoints(addedPoints);
    for(VertexID p = 1; p < path.size() - 1; ++p) { // for each point inside path
      VertexID target;
      assert(idAdded < cMesh.getNbPoints());

      if (p == 1) {
	target = updateCutMesh(path[p], path[p - 1], path[p + 1], idAdded, path[p - 1]);
      }
      else {
	target = updateCutMesh(path[p], path[p - 1], path[p + 1], idAdded, idAdded - 1);
      }
      // TODO: unused target?

      result.first.push_back(idAdded);
      result.second.push_back(path[p]);

      ++idAdded;
    }
  }
  VertexID nbP = cMesh.getNbPoints();

  /* then cut path extremity */
  // not seen before
  cMesh.setPointFlag(0);
  cMesh.point(path.front()).incFlag();
  cMesh.point(path.back()).incFlag();

  // for each extremity not seen before, selecting first border on when path size = 2
  VertexID first, second;
  if ((path.size() == 2) && !cMesh.point(path.front()).getIsBoundary()) {
    first = path.back();
    second = path.front();
  }
  else {
    first = path.front();
    second = path.back();
  }
  updateCutMesh(first);
  if (cMesh.point(second).getFlag() != 0)
    updateCutMesh(second);


  cMesh.initIsBoundaryFlag(false);

  if (path.size() > 2) {
    std::deque<VertexID>::iterator lasti = result.first.begin();
    std::deque<VertexID>::iterator j = result.second.begin() + 1;
    for(std::deque<VertexID>::iterator i = result.first.begin() + 1; i != result.first.end(); ++i) {
      if (!(cMesh.isBoundaryEdge(*i, *lasti))) {
	VertexID tmp = *j;
	*j = *i;
	*i = tmp;
	assert(cMesh.isBoundaryEdge(*i, *lasti));
      }
      ++j;
      ++lasti;
    }
  }

  for(VertexID i = nbP; i < cMesh.getNbPoints(); ++i)
    addExtremity(i, result);

  addExtremity(path.front(), result);
  if (path.front() != path.back()) {
    if (isNewCut) {
      addExtremity(path.front(), result);
      addExtremity(path.back(), result);
      addExtremity(path.back(), result);
    }
    else
      addExtremity(path.back(), result);
  }

  if (path.front() == path.back()) {
    if (result.first.size() != path.size())
      result.first.push_back(result.first.front());
    if (result.second.size() != path.size())
      result.second.push_back(result.second.front());
  }

  return result;
}


std::deque<VertexID> MeshPathes::addPathAllowSplitBoundary(const std::deque<VertexID> & path) {
  MeshManipulator mManip(cMesh);
  std::deque<VertexID> result;

  std::deque<std::deque<VertexID> > pathes = mManip.splitPath(path);

  if (pathes.size() != 1)
    throw Exception("addPathAllowSplitBoundary(1): not implemented with cutting on boundary in middle of path");

  /*for(std::deque<std::deque<VertexID> >::const_iterator p = pathes.begin(); p != pathes.end(); ++p)
    if ((*p).size() > 1)*/
  std::pair<std::deque<VertexID>, std::deque<VertexID> > bPathes = addPathGetExactPathes(pathes[0]);

  assert(pathes[0].size() == bPathes.first.size());
  assert(pathes[0].size() == bPathes.second.size());

  if (pathes[0].front() != path.front()) {
    VertexID newId;

    // first find first point of path on pathes[0]
    unsigned int i = 0;
    while(pathes[0].front() != path[i]) ++i;
    assert(i != 0);
    assert(i < path.size());

    // then find corresponding point
    VertexID lastNewId  = bPathes.first.front();
    if (cMesh.point(path[i - 1]).hasNeighbour(lastNewId))
      lastNewId = bPathes.second.front();

    do {
#ifndef NDEBUG
      unsigned int current = i;
#endif
      --i;
      // build a new point
      Point3D newPoint = cMesh.point(path[i]);
      // remove neighbour properties
      newPoint.clearNeighboursAndTriangles();
      newPoint.setIsBoundary(true);

      // add the point to cMesh
      newId = cMesh.addPoint(newPoint);

      projection[newId] = getProjection(path[i]);

      assert(cMesh.point(path[i]).getIsBoundary());
      assert(cMesh.point(path[current]).getIsBoundary());
      assert(cMesh.point(lastNewId).getIsBoundary());

      // and connect them
      cMesh.point(newId).addNeighbour(lastNewId);
      cMesh.point(lastNewId).addNeighbour(newId);
      lastNewId = newId;

    } while (i != 0);
    result.push_back(path.front());
    result.push_back(newId);
  }
  else {
    result.push_back(bPathes.first.front());
    result.push_back(bPathes.second.front());
  }

  if (pathes[0].back() != path.back()) {
    VertexID newId;

    // first find last point of path on pathes[0]
    unsigned int i = path.size() - 1;
    while(pathes[0].back() != path[i]) --i;
    assert(i != path.size() - 1);
    assert(i < path.size());

    // then find corresponding point
    VertexID lastNewId  = bPathes.first.back();
    if (cMesh.point(path[i + 1]).hasNeighbour(lastNewId))
      lastNewId = bPathes.second.back();

    do {
#ifndef NDEBUG
      unsigned int current = i;
#endif
      ++i;
      // build a new point
      Point3D newPoint = cMesh.point(path[i]);
      // remove neighbour properties
      newPoint.clearNeighboursAndTriangles();
      newPoint.setIsBoundary(true);

      // add the point to cMesh
      newId = cMesh.addPoint(newPoint);

      projection[newId] = getProjection(path[i]);

      assert(cMesh.point(path[i]).getIsBoundary());
      assert(cMesh.point(path[current]).getIsBoundary());
      assert(cMesh.point(lastNewId).getIsBoundary());

      // and connect them
      cMesh.point(newId).addNeighbour(lastNewId);
      cMesh.point(lastNewId).addNeighbour(newId);

      lastNewId = newId;

    } while (i != path.size() - 1);
    result.push_back(path.back());
    result.push_back(newId);
  }
  else {
    result.push_back(bPathes.first.back());
    result.push_back(bPathes.second.back());
  }

  return result;
}

void MeshPathes::addPathSplitBefore(const std::deque<VertexID> & path) {
  MeshManipulator mManip(cMesh);

  std::deque<std::deque<VertexID> > pathes = mManip.splitPath(path);

  for(std::deque<std::deque<VertexID> >::iterator p = pathes.begin(); p != pathes.end(); ++p)
    if ((*p).size() > 1) {
      correctingPath(*p);
      addPath(*p);
    }

}

void MeshPathes::addNLoop(const NLoop & nloop) {
  std::vector<std::vector<VertexID> > paths = nloop.getPaths();
  unsigned int maxSize = 0;
  std::vector<std::vector<VertexID> >::iterator pos;

  for(std::vector<std::vector<VertexID> >::iterator p = paths.begin(); p != paths.end(); ++p)
    if (maxSize < (*p).size()) {
      maxSize = (*p).size();
      pos = p;
    }
  if (maxSize <= 2)
    throw Exception("addNLoop(1): one of the paths should have a number of paths > 2.");

  if (paths.front().size() <= 2) {
    std::vector<VertexID> h = *pos;
    paths.erase(pos);
    paths.insert(paths.begin(), h);
  }

  bool first = true;
  // TODO: unused first?
  for(std::vector<std::vector<VertexID> >::const_iterator path = paths.begin(); path != paths.end(); ++path) {
    std::deque<VertexID> path2((*path).size());
    std::copy((*path).begin(), (*path).end(), path2.begin());
    correctingPathFull(path2);
    addPathSplitBefore(path2);
    first = false;
  }
}


void MeshPathes::addPath(const std::deque<VertexID> & path) {
  // add path to current mesh

  if (path.size() < 2)
    throw Exception("Cannot add an empty path.");

  (*this).push_back(path);
  /* first cut each path */
  if (path.size() > 2) {
    std::deque<VertexID> addedPoints(path);
    addedPoints.pop_front();
    addedPoints.pop_back();
    VertexID idAdded = cMesh.addPoints(addedPoints);

    for(unsigned int p = 1; p < path.size() - 1; ++p) { // for each point inside path
      VertexID target;
      assert(idAdded < cMesh.getNbPoints());

      if (p == 1) {
	target = updateCutMesh(path[p], path[p - 1], path[p + 1], idAdded, path[p - 1]);
      }
      else {
	target = updateCutMesh(path[p], path[p - 1], path[p + 1], idAdded, idAdded - 1);
      }
      // TODO: unused target?
      ++idAdded;
    }
  }
  /* then cut path extremity */
  // not seen before
  cMesh.setPointFlag(0);
  cMesh.point(path.front()).incFlag();
  cMesh.point(path.back()).incFlag();

  // for each extremity not seen before, selecting first border on when path size = 2
  VertexID first, second;
  if ((path.size() == 2) && !cMesh.point(path.front()).getIsBoundary()) {
    first = path.back();
    second = path.front();
  }
  else {
    first = path.front();
    second = path.back();
  }

  updateCutMesh(first);

  if (cMesh.point(second).getFlag() != 0)
    updateCutMesh(second);


  cMesh.initIsBoundaryFlag(false);
}

void MeshPathes::addExtremity(VertexID i, std::pair<std::deque<VertexID>, std::deque<VertexID> > & result) {
  if (result.first.size() + result.second.size() == 0)
    result.first.push_front(i);
  else if ((result.first.size() != 0) && cMesh.isBoundaryEdge(i, result.first.front()))
    result.first.push_front(i);
  else if ((result.first.size() != 0) && cMesh.isBoundaryEdge(i, result.first.back()))
    result.first.push_back(i);
  else if ((result.second.size() != 0) && cMesh.isBoundaryEdge(i, result.second.front()))
    result.second.push_front(i);
  else if ((result.second.size() != 0) && cMesh.isBoundaryEdge(i, result.second.back()))
    result.second.push_back(i);
  else if (result.first.size() == 0)
    result.first.push_back(i);
  else {
    assert(result.second.size() == 0);
    result.second.push_back(i);
  }
}

void MeshPathes::updateCutMesh(VertexID idPCenter) {
  VertexID nbAdded = 0;
  VertexID nbPtsBefore = cMesh.getNbPoints();


  Point3D pCenter = cMesh.point(idPCenter);
  int flag = cMesh.point(idPCenter).getFlag();
  assert(flag != 0);

  std::set<VertexID> bEdgeList;
  for(std::deque<VertexID>::const_iterator i = cMesh.point(idPCenter).getNeighbours().begin(); i != cMesh.point(idPCenter).getNeighbours().end(); ++i)
    if (cMesh.isBoundaryEdge(idPCenter, *i))
      bEdgeList.insert(*i);

  // first find number of points needed
  nbAdded = flag - 1;
  if (cMesh.point(idPCenter).getIsBoundary())
    ++nbAdded;


  cMesh.point(idPCenter).setFlag(0);

  // then add points.
  if (nbAdded != 0) {
    std::deque<VertexID> addedPoints(nbAdded, idPCenter);
    VertexID idAdded = cMesh.addPoints(addedPoints);

    std::deque<VertexID> pointList;
    pointList.push_back(idPCenter);

    // build list of points
    for(VertexID i = idAdded; i < cMesh.getNbPoints(); ++i) {
      pointList.push_back(i);

      projection[i] = getProjection(idPCenter);
    }

    // current point is the first one
    VertexID idLCurrentPoint = 0;
    VertexID idN = cMesh.point(idPCenter).getNeighbour(0);
    TriangleID idT = cMesh.findTriangle(idPCenter, idN).getId();
    TriangleID idFirstT = idT;
    // set points in each triangle
    TriangleID nbT = cMesh.point(idPCenter).getTriangles().size();
    TriangleID nbTSeen = 0;
    do {

      // replace point in triangle
      cMesh.triangle(idT).replacePoint(idPCenter, pointList[idLCurrentPoint]);

      // if all triangles has been seen: break
      ++nbTSeen;
      if (nbTSeen == nbT)
	break;

      // find next point
      VertexID idLastN = idN;
      idN = cMesh.triangle(idT).getOtherPoint(idN, pointList[idLCurrentPoint]);
      assert(cMesh.point(idPCenter).hasNeighbour(idN));

      if (inLastPathEdge(idPCenter, idN) && (cMesh.point(idN).getFlag() != 0)) { // if idN is another cut point (but not seen before)
	++idLCurrentPoint; // change region
	if (idLCurrentPoint == pointList.size()) idLCurrentPoint = 0;
      }
      // next triangle
      try {
	idT = cMesh.findOtherTriangle(idPCenter, idN, idT).getId();
      }
      catch (Exception e) { // if next triangle not found
	++idLCurrentPoint; // change region
	if (idLCurrentPoint == pointList.size()) idLCurrentPoint = 0;

	// and find first triangle in new region
	idN = pCenter.findNextNeighbour(idLastN, idN);
	idT = cMesh.findOtherTriangle(idPCenter, idN, idT).getId();

      }
    } while(idT != idFirstT);

    // then set triangles in points and neighbours in points
    std::deque<TriangleID> pTriangles = cMesh.point(idPCenter).getTriangles();
    std::set<VertexID> nbChanged;
    std::set<VertexID> nbChangedTwo;

    for(std::deque<TriangleID>::const_iterator tr = pTriangles.begin(); tr != pTriangles.end(); ++tr) {
      // first find point inside triangle
      VertexID idL = 0;
      while(!(cMesh.triangle(*tr).hasPoint(pointList[idL]))) {
	++idL;
	assert(idL < pointList.size());
      }

      // then remove this triangle from others region
      for(std::deque<VertexID>::const_iterator i = pointList.begin(); i != pointList.end(); ++i)
	if (*i != pointList[idL])
	  cMesh.point(*i).removeTriangle(*tr);

      // then set good neighbour for neighbours of idPCenter
      for(unsigned int e = 0; e < 3; ++e) {
	VertexID idPt = cMesh.triangle(*tr).getVertexId(e);
	if (idPt != pointList[idL]) {
	  if ((cMesh.point(idPt).getFlag() == 0) || (bEdgeList.find(idPt) != bEdgeList.end())) {
	    assert(idPt < nbPtsBefore);
	    if (nbChanged.find(idPt) == nbChanged.end()) { // not seen before
	      cMesh.point(idPt).replaceNeighbour(idPCenter, pointList[idL], true);
	      nbChanged.insert(idPt);
	    }
	  }
	  else
	    if (nbChanged.find(idPt) == nbChanged.end()) { // not seen before
	      cMesh.point(idPt).replaceNeighbour(idPCenter, pointList[idL], true);
	      nbChanged.insert(idPt);
	    }
	    else if (nbChangedTwo.find(idPt) == nbChangedTwo.end()) {
	      // find other neighbour (created from idPCenter and added to idPt)
	      VertexID otherIdPCenter = 0;
	      for(std::deque<VertexID>::const_iterator oId = pointList.begin(); oId != pointList.end(); ++oId) {
		if (cMesh.point(idPt).hasNeighbour(*oId)) {
		  otherIdPCenter = *oId;
		  break;
		}
		assert(*oId != pointList.back());
	      }

	      // get other point in triangle
	      VertexID otherP = cMesh.triangle(*tr).getOtherPoint(idPCenter, idPt);
	      if (otherP >= nbPtsBefore)
		otherP = cMesh.triangle(*tr).getOtherPoint(otherP, idPt);

	      // then insert neighbour inside idPt neighbour list
	      if ((pointList[idL] != otherIdPCenter)/* && (pointList[idL] != otherP)*/) {
		assert(cMesh.point(idPt).hasNeighbour(otherP));
		assert(cMesh.point(idPt).hasNeighbour(otherIdPCenter));
		cMesh.point(idPt).addNeighbourBetweenPoints(pointList[idL], otherIdPCenter, otherP);
		nbChangedTwo.insert(idPt);
	      }
	    }
	}
      }
    }
    std::deque<VertexID> pNeighbours = cMesh.point(idPCenter).getNeighbours();
    // then remove neighbours if needed for each idPCenter clone
    for(std::deque<VertexID>::const_iterator i = pointList.begin(); i != pointList.end(); ++i)
      for(std::deque<VertexID>::const_iterator nb = pNeighbours.begin(); nb != pNeighbours.end(); ++nb)
	if (!cMesh.point(*nb).hasNeighbour(*i))
	  cMesh.point(*i).removeNeighbour(*nb, true);

  }

  // point processed
  cMesh.point(idPCenter).setFlag(0);

}

VertexID MeshPathes::updateCutMesh(VertexID idPCenter, VertexID idP1, VertexID idP2, VertexID idPCenterNew, VertexID idP1bis) {
  //assert(cMesh.point(idPCenter).hasNeighbour(idP2));
  assert(!cMesh.point(idPCenter).getIsBoundary());

  cMesh.point(idPCenterNew) = cMesh.point(idPCenter); // update idPCenterNew point using idPCenter (neighbours, triangles)
  cMesh.point(idPCenterNew).setId(idPCenterNew);

  projection[idPCenterNew] = getProjection(idPCenter);

  TriangleID t1 = cMesh.findTriangle(idPCenter, idP2).getId();
  cutBoundarySide(idPCenter, idPCenterNew, t1, idP2, idP1, idP1bis, true);
  TriangleID t2 = cMesh.findOtherTriangle(idPCenter, idP2, t1).getId();
  VertexID result = cutBoundarySide(idPCenterNew, idPCenter, t2, idP2, idP1, idP1bis, false);


  assert((result == idP1) || (result == idP1bis));
  return result;
}


VertexID MeshPathes::cutBoundarySide(VertexID idPOutside, VertexID idPInside,
			       TriangleID idT, VertexID idBP, VertexID idTarget, VertexID idTargetBis, bool firstSide, bool firstTriangle) {
  VertexID idNext;
  if (cMesh.triangle(idT).hasPoint(idPInside))
     idNext = cMesh.triangle(idT).getOtherPoint(idPInside, idBP);
  else
     idNext = cMesh.triangle(idT).getOtherPoint(idPOutside, idBP);

  cMesh.triangle(idT).replacePoint(idPOutside, idPInside);
  cMesh.point(idPOutside).removeTriangle(idT);
  if ((idNext == idTarget) || (idNext == idTargetBis)) { // if end side
    if (firstTriangle) {
      if (firstSide) {
	if (!cMesh.point(idBP).hasNeighbour(idPInside))
	  cMesh.point(idBP).addNeighbourBetweenPoints(idPInside, idPOutside, idNext);
	else
	  cMesh.point(idBP).addNeighbourBetweenPoints(idPOutside, idPInside, idNext);
      }
    } else {
      cMesh.point(idBP).replaceNeighbour(idPOutside, idPInside);
      cMesh.point(idPOutside).removeNeighbour(idBP);
    }


    if (!(idTarget == idTargetBis)) { // if it's not first point in cut
      cMesh.point(idNext).replaceNeighbour(idPOutside, idPInside);
      cMesh.point(idPOutside).removeNeighbour(idNext);
    }
    else {
      if (firstSide) {
	if (!cMesh.point(idNext).hasNeighbour(idPInside)) {
	  cMesh.point(idNext).addNeighbourBetweenPoints(idPInside, idPOutside, idBP);
	}
	else
	  cMesh.point(idNext).addNeighbourBetweenPoints(idPOutside, idPInside, idBP);
      }
    }
    return idNext;
  }
  else {
    if (firstTriangle) {
      if (firstSide) {
	if (!cMesh.point(idBP).hasNeighbour(idPInside))
	  cMesh.point(idBP).addNeighbourBetweenPoints(idPInside, idPOutside, idNext);
	else
	  cMesh.point(idBP).addNeighbourBetweenPoints(idPOutside, idPInside, idNext);
      }
    } else {
      cMesh.point(idBP).replaceNeighbour(idPOutside, idPInside);
      cMesh.point(idPOutside).removeNeighbour(idBP);
    }

    TriangleID idTNext;
    try {
      idTNext = cMesh.findOtherTriangle(idPOutside, idNext, idT).getId();
    }
    catch(Exception e) {
      idTNext = cMesh.findOtherTriangle(idPInside, idNext, idT).getId();
    }
    return cutBoundarySide(idPOutside, idPInside, idTNext, idNext, idTarget, idTargetBis, firstSide, false);
  }

}

VertexID MeshPathes::getPathLength(unsigned int idPath) const {
  if ((*this)[idPath].size() != 0)
    return (*this)[idPath].size() - 1;
  else
    return 0;
}

VertexID MeshPathes::getSize() const {
  VertexID result = 0;
  for(std::deque<std::deque<VertexID> >::const_iterator i = (*this).begin();
      i != (*this).end(); ++i)
    if ((*i).size() != 0)
      result += (*i).size() - 1;
  return result;
}

void MeshPathes::addPoint(unsigned int idPath, VertexID idPoint) {
  if (idPath >= (*this).size())
    throw Exception(std::string("Unknown path"));
  (*this)[idPath].push_front(idPoint);
}


bool MeshPathes::inLastPathEdge(VertexID p1, VertexID p2) const {

  if (cMesh.isBoundaryEdge(p1, p2))
    return true;
  else {
    std::deque<VertexID> path = (*this).back();
    for(std::deque<VertexID>::const_iterator j = path.begin();
	j != path.end(); ++j)
      if (*j == p1) {
	std::deque<VertexID>::const_iterator j2 = j + 1;
	if ((j2 != path.end()) && (*j2 == p2))
	  return true;
      }
      else if (*j == p2) {
	std::deque<VertexID>::const_iterator j2 = j + 1;
	if ((j2 != path.end()) && (*j2 == p1))
	  return true;
      }
  }

  return false;

}


bool MeshPathes::inPathEdge(VertexID p1, VertexID p2) const {
  if (cMesh.isBoundaryEdge(p1, p2))
    return true;
  else {
    for(std::deque<std::deque<VertexID> >::const_iterator i = (*this).begin();
	i != (*this).end(); ++i)
      for(std::deque<VertexID>::const_iterator j = (*i).begin();
	  j != (*i).end(); ++j)
	if (*j == p1) {
	  std::deque<VertexID>::const_iterator j2 = j + 1;
	  if ((j2 != (*i).end()) && (*j2 == p2))
	    return true;
	}
	else if (*j == p2) {
	  std::deque<VertexID>::const_iterator j2 = j + 1;
	  if ((j2 != (*i).end()) && (*j2 == p1))
	    return true;
	}

  }

  return false;
}


bool MeshPathes::isValidPathPseudoVolumic(const std::deque<VertexID> & path,
					  const UCharCImg & insideMask,
					  const UCharCImg & region,
					  bool inside) {
  if (path.size() == 0)
    return false;


  Coord3D barycenter;
  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p)
    barycenter += cMesh.point(*p);
  barycenter /= path.size();

  std::vector<Coord3DT<unsigned int> > newCoords;
  bool inregion = false;
  unsigned int nbInside = 0;

  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
    Coord3D newCoord = cMesh.point(*p) + (barycenter - cMesh.point(*p)).normalize();
    if (cMesh.point(*p).distance(newCoord) > cMesh.point(*p).distance(barycenter)) {
      newCoord = cMesh.point(*p) + (barycenter - cMesh.point(*p)).normalize() * .5;
    }
    newCoords.push_back(Coord3DT<unsigned int>(floor(newCoord.getX()),
					       floor(newCoord.getY()),
					       floor(newCoord.getZ())));
    if (region(newCoords.back().getX(),
	       newCoords.back().getY(),
	       newCoords.back().getZ()) != 0)
      inregion = true;
    if (insideMask(newCoords.back().getX(),
	     newCoords.back().getY(),
	     newCoords.back().getZ()) != 0)
      ++nbInside;
  }

  return (!inregion) || ((nbInside > path.size() / 2) && inside) || ((nbInside < path.size() / 2) && !inside);
}


std::deque<VertexID> MeshPathes::getShortestNonSeparatingCyclePseudoVolumic(double minLength,
									    const UCharCImg & insideMask,
									    const UCharCImg & region,
									    bool inside) {

  MeshPart mPart(cMesh, cMesh.point(0));
  mPart.setLength(*lengthEdgeMethod);

  // compute a simple cut to select base points
  std::deque<VertexID> points = mPart.getSlimSimpleCutBorders();

  if (points.size() == 0)
    throw Exception(std::string("getShortestNonSeparatingCycle: shortest path cannot be computed: bad topology."));

  // compute strips on the simple cut boundary
  std::deque<std::deque<VertexID> > strips = mPart.getSimpleCutStrips();

  // if there is more than one strip
  if (strips.size() > 1) {
    std::deque<VertexID> result;
    // for each strip, compute better base point
    mPart.initInsideSimpleCutFlags();

    unsigned int i = 0;
    double lastLength = -1.;
    for(std::deque<std::deque<VertexID> >::const_iterator strip = strips.begin(); strip != strips.end(); ++strip, ++i) {
      if (points.size() > 1000) {
	std::cout << "\r   Cycle estimation: " << i << " / " << strips.size();
	std::cout.flush();
      }
      if ((*strip).size() > 1) {
	std::pair<VertexID, double> bS = mPart.estimateBetterBasePoint(*strip, lastLength);

	if (bS.second != std::numeric_limits<double>::max()) {
	  std::deque<VertexID> newResult;
	  // compute path from the last to the first point
	  newResult.push_back(bS.first);
	  while(mPart.getLength(newResult.back()) != 0.0) {
	    newResult.push_back(mPart.getPredPoint(newResult.back()));
	  }

	  double addedLength = 0.0;
	  // the path between the two points have to be computed
	  mPart.computeTDijkstraTarget(newResult.front(), newResult.back(), 1, 0);
	  addedLength = mPart.getLength(newResult.back());
	  while(mPart.getLength(newResult.back()) != 0.0) {
	    newResult.push_back(mPart.getPredPoint(newResult.back()));
	  }

	  if ((lastLength < 0) || (lastLength > bS.second + addedLength)) {
	    if (cMesh.isNonSeparatingCycle(newResult)) {
	      if ((minLength < bS.second + addedLength) && isValidPathPseudoVolumic(newResult, insideMask, region, inside)) {
		  lastLength = bS.second + addedLength;
		  result = newResult;
		}
	    }
	    mPart.initInsideSimpleCutFlags();
	  }
	}
      }
    }

    if (result.size() < 2)
      throw Exception(std::string("getShortestNonSeparatingCycle(3): shortest path cannot be computed. Wrong estimation."));

    return result;
  }
  else {
    throw Exception(std::string("getShortestNonSeparatingCycle(3): shortest path cannot be computed. Base points no structured as strips"));
  }

}

std::deque<VertexID> MeshPathes::getShortestNonSeparatingCycle(double minLength, enum OptimEC optimMethod, bool merge) {
  MeshPart mPart(cMesh, cMesh.point(0));
  mPart.setLength(*lengthEdgeMethod);

  // compute a simple cut to select base points
  std::deque<VertexID> points = mPart.getSlimSimpleCutBorders();

  if (points.size() == 0)
    throw Exception(std::string("getShortestNonSeparatingCycle: shortest path cannot be computed: bad topology."));

  if (optimMethod == OECApproximation) { // approximation rather than optimization
    // compute strips on the simple cut boundary
    std::deque<std::deque<VertexID> > strips = mPart.getSimpleCutStrips();

    // if there is more than one strip
    if (strips.size() > 1) {
      std::deque<VertexID> result;
      // for each strip, compute better base point
      mPart.initInsideSimpleCutFlags();

      unsigned int i = 0;
      double lastLength = -1.;
      for(std::deque<std::deque<VertexID> >::const_iterator strip = strips.begin(); strip != strips.end(); ++strip, ++i) {
	if (points.size() > 1000) {
	  std::cout << "\r   Cycle estimation: " << i << " / " << strips.size();
	  std::cout.flush();
	}
	if ((*strip).size() > 1) {
	  std::pair<VertexID, double> bS = mPart.estimateBetterBasePoint(*strip, lastLength);

	  if (bS.second != std::numeric_limits<double>::max()) {
	    std::deque<VertexID> newResult;
	    // compute path from the last to the first point
	    newResult.push_back(bS.first);
	    while(mPart.getLength(newResult.back()) != 0.0) {
	      newResult.push_back(mPart.getPredPoint(newResult.back()));
	    }

	    double addedLength = 0.0;
	    // the path between the two points have to be computed
	    mPart.computeTDijkstraTarget(newResult.front(), newResult.back(), 1, 0);
	    addedLength = mPart.getLength(newResult.back());
	    while(mPart.getLength(newResult.back()) != 0.0) {
	      newResult.push_back(mPart.getPredPoint(newResult.back()));
	    }

	    if ((lastLength < 0) || (lastLength > bS.second + addedLength)) {
	      if (cMesh.isNonSeparatingCycle(newResult)) {
		lastLength = bS.second + addedLength;
		result = newResult;
	      }
	      mPart.initInsideSimpleCutFlags();
	    }
	  }
	}
      }

      if (result.size() < 2)
	throw Exception(std::string("getShortestNonSeparatingCycle(3): shortest path cannot be computed. Wrong estimation."));

      return result;
    }
    else {
      throw Exception(std::string("getShortestNonSeparatingCycle(3): shortest path cannot be computed. Base points no structured as strips"));
    }
  }
  else {
    // if optimization
    if (optimMethod == OECsimple) {
      double * cycleLength;
      cycleLength = new double[cMesh.getNbPoints()];
      std::list<VertexID> sList;

      for(VertexID i = 0; i < cMesh.getNbPoints(); ++i)
	cycleLength[i] = -1;

      // for each point, estimate cycle length (naïve method)
      mPart.initInsideSimpleCutFlags();
      for(std::deque<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p) {
	if (points.size() > 1000) {
	  std::cout << "\r   Init. optimization: " << sList.size() << " / " << points.size();
	  std::cout.flush();
	}
	cycleLength[*p] = mPart.estimateCycleLength(*p);
	sList.push_back(*p);
      }

      // then order points using the estimated cycle length
      lengthPathBP mlpath;
      mlpath.length = cycleLength;
      mlpath.mesh = &cMesh;
      sList.sort(mlpath);
      points.clear();
      for(std::list<VertexID>::const_iterator p = sList.begin(); p != sList.end(); ++p)
	points.push_back(*p);
      delete[] cycleLength;
    }
    else if ((optimMethod == OECbyStrip) || ((optimMethod == OECbyStripAndPredCut) && (minLocalBasePoints.size() == 0))) {
      // compute strips on the simple cut boundary
      std::deque<std::deque<VertexID> > strips = mPart.getSimpleCutStrips();

      // if there is more than one strip
      if (strips.size() > 1) {
	double * cycleLength;
	cycleLength = new double[cMesh.getNbPoints()];
	std::list<VertexID> sList;

	for(std::deque<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p)
	  sList.push_back(*p);

	for(VertexID i = 0; i < cMesh.getNbPoints(); ++i)
	  cycleLength[i] = -1;

	// for each strip, compute better base point
	mPart.initInsideSimpleCutFlags();

	unsigned int i = 0;
	double lastLength = -1.;
	for(std::deque<std::deque<VertexID> >::const_iterator strip = strips.begin(); strip != strips.end(); ++strip, ++i) {
	  if (points.size() > 1000) {
	    std::cout << "\r   Init. optimization: " << i << " / " << strips.size();
	    std::cout.flush();
	  }
	  if ((*strip).size() > 1) {
	    std::pair<VertexID, double> bS = mPart.estimateBetterBasePoint(*strip, lastLength);
	    if (bS.second != std::numeric_limits<double>::max()) {
	      cycleLength[bS.first] = bS.second;
	      lastLength = bS.second;
	    }
	  }
	}

	// then rebuild list adding this points before others and sorted
	// by the estimated length
	lengthPathBP mlpath;
	mlpath.length = cycleLength;
	mlpath.mesh = &cMesh;
	sList.sort(mlpath);

	points.clear();
	for(std::list<VertexID>::const_iterator p = sList.begin(); p != sList.end(); ++p)
	  points.push_back(*p);

	delete[] cycleLength;
      }
    }

    return getShortestNonSeparatingCycleFromPoints(points, minLength, optimMethod, merge);
  }
}




std::deque<VertexID> MeshPathes::getShortestNonSeparatingCycleFromPoints(std::deque<VertexID> & points, double minLength, enum OptimEC optimMethod, bool merge) {
  clock_t tstart = clock();
  std::deque<VertexID> path;
  double length_l = std::numeric_limits<double>::max();
  MeshManipulator mManip(cMesh, *lengthEdgeMethod);

  LengthEdge * lEdgeCMesh;
  MeshMap * mMap = NULL;
  if ((*lengthEdgeMethod).needMeshMap()) {
    if (mMap != NULL)
      delete mMap;
    mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
    lEdgeCMesh = (*lengthEdgeMethod).cloneMeshMap(*mMap);
  }
  else
    lEdgeCMesh = (*lengthEdgeMethod).clone(cMesh);

  bool dec = false;
  VertexID last;
  VertexID NbMinLoc = 0;
  double newLength;


  if (minLocalBasePoints.size() != 0) {
    std::cout << " \r Using old multipoints (" << minLocalBasePoints.size() << " points)";
    std::cout.flush();
    std::reverse(minLocalBasePoints.begin(), minLocalBasePoints.end());
    points.insert(points.begin(), minLocalBasePoints.begin(), minLocalBasePoints.end());
  }
  minLocalBasePoints.clear();
  VertexID nbAll = points.size();

  // for each base point, compute shortest non trivial cycle

  unsigned int id = 0;
  for(std::deque<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p) {
    if ((nbAll > 500) || (optimMethod == OECnoTrunc) || ((clock() - tstart) / ((double) CLOCKS_PER_SEC) >= 5)) {
      std::cout << "\r   " << id << " / " << nbAll << ", length: " << length_l;
      if (minLength != 0)
	std::cout << ", min length: " << minLength;
      if (optimMethod == OECbyStripAndPredCut)
	std::cout << " (" << NbMinLoc<< " loc. min.)";
      std::cout << "     ";
      std::cout.flush();
    }
    // find shortest path
    std::deque<VertexID> newPath = mManip.getShortestNonSeparatingCycleFromPoint(*p, ((optimMethod != OECnoTrunc) && (optimMethod != OECTruncMin)) ? length_l : std::numeric_limits<double>::max(), minLength, merge);

    // removing duplicated edges at begin and end of loop
    correctingLoop(newPath);

    // if path shortest than length has been found
    if (newPath.size() != 0) {
      // if path is shorter than others seen before, it became shortest path
      if (merge) {
	LengthEdgeMergeBoundary lEdgeCMeshMerge(*lEdgeCMesh);
	newLength = cMesh.getLength(newPath, lEdgeCMeshMerge);
      }
      else
	newLength = cMesh.getLength(newPath, *lEdgeCMesh);
      if ((length_l >= newLength) && (newLength != 0)) {
	dec = true;
	last = *p;
      }

      if ((path.size() == 0) || ((length_l > newLength) && (newLength != 0))) {
	path = newPath;
	length_l = newLength;
      }
    }
    else if (dec && (optimMethod == OECbyStripAndPredCut)) {
      dec = false;
      minLocalBasePoints.push_back(last);
      ++NbMinLoc;
    }
    ++id;
  }

  std::cout << "\r";

  if (mMap != NULL)
    delete mMap;
  delete lEdgeCMesh;
  return path;
}

MeshPathes::CycleDescriptor MeshPathes::getShortestNonSeparatingCycleDescFromPoints(const std::deque<VertexID> & points, double minLength, bool merge) {
  double length_l = std::numeric_limits<double>::max();
  MeshManipulator mManip(cMesh, *lengthEdgeMethod);

  LengthEdge * lEdgeCMesh;
  MeshMap * mMap = NULL;
  if ((*lengthEdgeMethod).needMeshMap()) {
    if (mMap != NULL)
      delete mMap;
    mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
    lEdgeCMesh = (*lengthEdgeMethod).cloneMeshMap(*mMap);
  }
  else
    lEdgeCMesh = (*lengthEdgeMethod).clone(cMesh);

  VertexID sPoint = cMesh.getNbPoints();
  VertexID sPred1 = cMesh.getNbPoints();
  VertexID sPred2 = cMesh.getNbPoints();
  VertexID sNextPoint = cMesh.getNbPoints();
  std::deque<VertexID> path;
  double newLength;

  // for each base point, compute shortest non trivial cycle

  for(std::deque<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p) {
    // find shortest path
    std::pair<VertexID, std::pair<VertexID, VertexID> > target = mManip.getTargetShortestNonSeparatingCycleFromPoint(*p, length_l, minLength, merge);
    VertexID nextPoint = target.first;
    if (nextPoint != cMesh.getNbPoints()) {
      std::deque<VertexID> newPath;
      VertexID pred1 = target.second.first;
      VertexID pred2 = target.second.second;
      assert(pred1 != pred2);
      assert(pred1 < cMesh.getNbPoints());
      assert(pred2 < cMesh.getNbPoints());


      if (merge && cMesh.isBoundaryEdge(pred1, pred2))
	continue;

      // build shortest cycle and return it
      newPath = mManip.buildCycle(*p, nextPoint, pred1, pred2);

      // if path shortest than length has been found
      if (newPath.size() > 1) {
	// if path is shorter than others seen before, it became shortest path
	if (merge) {
	  LengthEdgeMergeBoundary lEdgeCMeshMerge(*lEdgeCMesh);
	  newLength = cMesh.getLength(newPath, lEdgeCMeshMerge);
	} else
	  newLength = cMesh.getLength(newPath, *lEdgeCMesh);
	if ((sPoint == cMesh.getNbPoints()) || (length_l > newLength)) {
	  sPoint = *p;
	  sPred1 = pred1;
	  sPred2 = pred2;
	  sNextPoint = nextPoint;
	  length_l = newLength;
	  path = newPath;
	  assert(path.front() < cMesh.getNbPoints());
	  assert(path.back() < cMesh.getNbPoints());
	  assert((path.front() == path.back()) || cMesh.point(path.front()).getIsBoundary() || cMesh.point(path.back()).getIsBoundary());
	}
      }
    }
  }

  if (mMap != NULL)
    delete mMap;
  delete lEdgeCMesh;

  if (sPoint == cMesh.getNbPoints())
    return CycleDescriptor();
  else
    return CycleDescriptor(sPoint, sNextPoint, sPred1, sPred2, path);
}




void MeshPathes::logPathes(const std::deque<std::deque<VertexID> > & pathes) const {
  if (logFile == "")
    return;

  std::ofstream outfile(logFile.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw Exception(std::string("Cannot open pathes log file."));

  outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
  outfile << "<pathes>" << std::endl;
  for(std::deque<std::deque<VertexID> >::const_iterator path = pathes.begin(); path != pathes.end(); ++path) {
    outfile << "\t<path>" << std::endl;
    for(std::deque<VertexID>::const_iterator i = (*path).begin(); i != (*path).end(); ++i)
      outfile << "\t\t<point id=\"" << *i << "\" />" << std::endl;

    outfile << "\t</path>" << std::endl;
  }
  outfile << "</pathes>" << std::endl;

  outfile.close();

}

std::deque<std::deque<VertexID> > MeshPathes::loadFromFile(const std::string & filename) {
#ifdef LIBXML_READER_ENABLED
  xmlTextReaderPtr reader;
  std::deque<std::deque<VertexID> > result;
  int ret;
  int id = 0;

  reader = xmlReaderForFile(filename.c_str(), NULL, 0);
  if (reader != NULL) {
    ret = xmlTextReaderRead(reader);
    while (ret == 1)  {
      if (std::string("path") == (const char *) xmlTextReaderConstName(reader)) {
	ret = xmlTextReaderRead(reader);
	result.push_back(std::deque<VertexID>());
	while ((ret == 1) && (std::string("path") != (const char *) xmlTextReaderConstName(reader)))  {
	  if ((std::string("point") == (const char *) xmlTextReaderConstName(reader))) {
	    id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));
	    result.back().push_back(id);
	  }
	  ret = xmlTextReaderRead(reader);
	}
      }
      ret = xmlTextReaderRead(reader);
    }
  }
  else {
    throw Exception(std::string("Cannot load pathes file."));
  }

  return result;
#else
  throw Exception("XML parser not available. " + filename + " not opened.");
#endif
}

std::deque<VertexID> MeshPathes::getShortestPathToBoundary(VertexID point) {
  assert(point < (*mesh).getNbPoints());
  std::deque<VertexID> result;
  VertexID otherPoint = point;

  // if point is a boundary point, return empty path
  if ((*mesh).point(point).getIsBoundary())
    return result;

  // first compute dijskstra from the given point to the boundary
  computeDijkstra(point);

  // then find nearest boundary point
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getIsBoundary())
      if(((point == otherPoint) || (length[otherPoint] > length[(*p).getId()])) && (length[(*p).getId()] >= 0))
	otherPoint = (*p).getId();

  // then build path
  if (otherPoint != point) {
    VertexID p = pred[otherPoint];
    result.push_front(otherPoint);

    while(result.front() != point) {
      result.push_front(p);
      p = pred[p];
    }
  }

  return result;
}


Mesh & MeshPathes::cutMesh() {
  (*mesh) = cMesh;

  return (*mesh);
}

std::deque<unsigned int> MeshPathes::buildBorderColor(CImgList<unsigned char> & colors, CImgList<VertexID> & primitives) {

  std::deque<unsigned int> caption;
  srand((unsigned)time(0));
  unsigned int c = (*mesh).buildBorderColor(colors, primitives);

  if (c != 0)
    caption.push_front(c);

  if ((*this).size() != 0) {
    unsigned int colorPath = 0;
    unsigned int colorPathStep = 256 * 256 * 256 / (*this).size();

    for(MeshPathes::const_iterator path = (*this).begin(); path != (*this).end(); ++path) {
      unsigned int addR = 1 + (int) ((colorPathStep / 2) * (rand() / (RAND_MAX + 1.0)));
      const unsigned char lineColor[3] = {(colorPath + addR) % 256, ((colorPath + addR) / 256) % 256, (colorPath + addR) / 256 / 256};
      caption.push_back(colorPath + addR);

      VertexID last = (*path).front();
      for(std::deque<VertexID>::const_iterator point = ++((*path).begin()); point != (*path).end(); ++point) {
	VertexID pointO = *point;
	if (*point >= (*mesh).getNbPoints())
	  pointO = projection[*point];
	for(std::deque<TriangleID>::const_iterator triangle = (*mesh).point(pointO).getTriangles().begin();
	    triangle != (*mesh).point(pointO).getTriangles().end(); ++triangle)
	  if ((*mesh).triangle(*triangle).hasPoint(last)) {
	    assert(last != pointO);
	    CImg<unsigned char> & color = colors[*triangle];
	    CImg<VertexID> & primitive = primitives[*triangle];
	    VertexID x1, x2, y1, y2;

	    if (((*mesh).triangle(*triangle).getP1() != last) && ((*mesh).triangle(*triangle).getP1() != pointO)) {
	      x1 = primitive[5]; y1 = primitive[6];
	      x2 = primitive[7]; y2 = primitive[8];
	    }
	    else if (((*mesh).triangle(*triangle).getP2() != last) && ((*mesh).triangle(*triangle).getP2() != pointO)) {
	      x1 = primitive[3]; y1 = primitive[4];
	      x2 = primitive[7]; y2 = primitive[8];
	    }
	    else {
	      x1 = primitive[3]; y1 = primitive[4];
	      x2 = primitive[5]; y2 = primitive[6];
	    }
	    color.draw_line(x1, y1, x2, y2, lineColor);
	    if ((y1 == 0) && (x2 == 0) && (x1 != 0)) {
	      color.draw_line(x1 - 1, y1, x2, y2 - 1, lineColor);
	      color.draw_line(x1 - 2, y1, x2, y2 - 2, lineColor);
	    }
	  }
	last = pointO;
      }
      colorPath += colorPathStep;
    }
  }

  return caption;
}


void MeshPathes::correctingPathFull(std::deque<VertexID> & path) {
  MeshManipulator cMeshManip(cMesh);


  if (path.size() > 2) {
    // looking for bad junctions at the begining of the path
    unsigned int sp = 0;
    while((sp != path.size() - 1) && (!cMesh.point(path[sp]).hasNeighbour(path[sp + 1])))
      ++sp;
    if (sp == path.size() - 1) {
      throw Exception("correctingPathFull(1): path cannot be corrected. No valid section.");
    }

    if (sp != 0) {
      // correcting the begining of the path
      for(unsigned int i = sp; i != 0; --i) {
	std::deque<VertexID> samePoints = cMeshManip.getPointsSameLocation(path[i - 1]);
	for(std::deque<VertexID>::const_iterator s = samePoints.begin(); s != samePoints.end(); ++s)
	  if (cMesh.point(path[i]).hasNeighbour(*s)) {
	    path[i - 1] = *s;
	    break;
	  }
      }
    }


    // looking for bad junctions at the begining of the path
    sp = path.size() - 1;
    while((sp != 1) && (!cMesh.point(path[sp]).hasNeighbour(path[sp - 1])))
      --sp;

    if (sp != path.size() - 1) {
      for(unsigned int i = sp; i != path.size() - 1; ++i) {
	std::deque<VertexID> samePoints = cMeshManip.getPointsSameLocation(path[i + 1]);
	for(std::deque<VertexID>::const_iterator s = samePoints.begin(); s != samePoints.end(); ++s)
	  if (cMesh.point(path[i]).hasNeighbour(*s)) {
	    path[i + 1] = *s;
	    break;
	  }
      }
    }
  }
  else if ((path.size() == 2) && (!cMesh.point(path.front()).hasNeighbour(path.back()))) {
      std::deque<VertexID> samePointsFront = cMeshManip.getPointsSameLocation(path.front());
      std::deque<VertexID> samePointsBack = cMeshManip.getPointsSameLocation(path.back());
      for(std::deque<VertexID>::const_iterator s1 = samePointsFront.begin(); s1 != samePointsFront.end(); ++s1)
	for(std::deque<VertexID>::const_iterator s2 = samePointsBack.begin(); s2 != samePointsBack.end(); ++s2)
	  if (cMesh.point(*s1).hasNeighbour(*s2)) {
	    path.front() = *s1;
	    path.back() = *s2;
	    return;
	  }
    }
}


void MeshPathes::correctingPath(std::deque<VertexID> & path) {
  MeshManipulator cMeshManip(cMesh);


  if (path.size() > 2) {
    if (!cMesh.point(path[0]).hasNeighbour(path[1])) {
      std::deque<VertexID> samePoints = cMeshManip.getPointsSameLocation(path.front());
      for(std::deque<VertexID>::const_iterator s = samePoints.begin(); s != samePoints.end(); ++s)
	if (cMesh.point(path[1]).hasNeighbour(*s)) {
	  path[0] = *s;
	  break;
	}
    }

    VertexID & last = path.back();
    VertexID pred_l = path[path.size() - 2];

    if (!cMesh.point(last).hasNeighbour(pred_l)) {
      std::deque<VertexID> samePoints = cMeshManip.getPointsSameLocation(last);
      for(std::deque<VertexID>::const_iterator s = samePoints.begin(); s != samePoints.end(); ++s)
	if (cMesh.point(pred_l).hasNeighbour(*s)) {
	  last = *s;
	  break;
	}
    }
  }
  else if ((path.size() == 2) && (!cMesh.point(path.front()).hasNeighbour(path.back()))) {
      std::deque<VertexID> samePointsFront = cMeshManip.getPointsSameLocation(path.front());
      std::deque<VertexID> samePointsBack = cMeshManip.getPointsSameLocation(path.back());
      for(std::deque<VertexID>::const_iterator s1 = samePointsFront.begin(); s1 != samePointsFront.end(); ++s1)
	for(std::deque<VertexID>::const_iterator s2 = samePointsBack.begin(); s2 != samePointsBack.end(); ++s2)
	  if (cMesh.point(*s1).hasNeighbour(*s2)) {
	    path.front() = *s1;
	    path.back() = *s2;
	    return;
	  }
    }
}

void MeshPathes::correctingLoop(std::deque<VertexID> & path) const {
  bool modif = true;

  while (modif) {
    modif = false;
    if (path.size() > 2) {
      if ((path.front() == path.back()) &&
	  (path[1] == path[path.size() - 2])) {
	path.pop_front();
	path.pop_back();
	modif = true;
      }
    }
  }

}

void MeshPathes::logCMesh(const std::string & logMesh) {
  int eC = cMesh.getEulerCaracteristic();
  unsigned int nbB = cMesh.getNbBoundaries();
  std::ostringstream str;
  str << logMesh << "Mesh-e" << eC << "-b" << nbB;
  std::string logFile_l = FileTools::getAvailableFileName(str.str(), "wrl");
  std::cout << "Logging mesh (" << logFile_l << ")" << std::endl;
  cMesh.save(logFile_l);

}

NLoop MeshPathes::getShortestNLoop(VertexID basePoint, unsigned int nbPaths) {
  lengthPathBP lpath;
  WaveFront wf(*mesh);
  NLoop result;

  computeDijkstra(basePoint);

  lpath.length = length;
  lpath.mesh = mesh;

  // sort points using dijkstra length
  std::vector<VertexID> sList;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    if (length[i] != -1) {
      sList.push_back(i);
    }
  sort(sList.begin(), sList.end(), lpath);

  std::vector<std::vector<VertexID> > nLoopStart;

  do {
    result.clear();
    nLoopStart = wf.growSurfaceUntilJunctionGetNLoopStart(sList, nbPaths);

    for(std::vector<std::vector<VertexID> >::iterator path = nLoopStart.begin(); path != nLoopStart.end(); ++path) {
      assert((*path).size() != 0);
      while((*path).back() != basePoint) {
	assert((*mesh).point(pred[(*path).back()]).hasNeighbour((*path).back()));
	(*path).push_back(pred[(*path).back()]);
      }

      result.addPath(*path);
    }

  } while (!hasGoodTopologicalProperties(result));


  return result;
}

bool MeshPathes::hasGoodTopologicalProperties(const NLoop & result) {
  FlagLogger fl;
  const std::vector<std::vector<VertexID> > & paths = result.getPaths();

  fl.logFlags(*mesh);

  // init triangles and point flags
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  for(std::vector<std::vector<VertexID> >::const_iterator path = paths.begin(); path != paths.end(); ++path)
    for(std::vector<VertexID>::const_iterator p = (*path).begin(); p != (*path).end(); ++p)
      (*mesh).point(*p).setFlag(1);

  for(Mesh::triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
    if ((*t).getFlag() == 0) {
      MeshPart mp(*mesh, (*t).getId(), result);
      // mp contains the complementary of the given NLoop, but it may also be swen on the paths
      // we have to compute the euler caracteristic taking into account to the paths going through mp
      int e = mp.getEulerCaracteristic(paths);
      if (e > 0) {
	fl.restoreFlags(*mesh);
	return false;
      }
    }

  fl.restoreFlags(*mesh);
  return true;
}

void MeshPathes::addFront(const std::vector<VertexID> & front_l) {
  if (front_l.size() < 3) {
    std::cout << "Warning: adding empty front" << std::endl;
    return;
  }

  // set flags (number of instances in the front)
  (*mesh).setPointFlag(0);
  for(std::vector<VertexID>::const_iterator p = front_l.begin(); p != front_l.end(); ++p)
    (*mesh).point(*p).incFlag();

  bool first = true;
  std::deque<VertexID> path;
  std::deque<VertexID> pathFirst;
  for(std::vector<VertexID>::const_iterator p = front_l.begin(); p != front_l.end(); ++p) {
    path.push_back(*p);
    std::vector<VertexID>::const_iterator nextp = p + 1;
    if (nextp == front_l.end())
      nextp = front_l.begin();
    std::vector<VertexID>::const_iterator nextp2 = nextp + 1;
    if (nextp2 == front_l.end())
      nextp2 = front_l.begin();
    if (isEndOfPathOnFront(p, nextp, nextp2, path)) {
      if (first) {
	pathFirst = path;
	first = false;
      }
      else {
	if (isValidCuttingPathFromFront(path, front_l)) {
	  correctingPath(path);
	  addPathSplitBefore(path);
	}
      }

      path.clear();
      path.push_back(*p);
    }
  }

  if (first)
    path.push_back(front_l.front());
  path.insert(path.end(), pathFirst.begin(), pathFirst.end());
  if (isValidCuttingPathFromFront(path, front_l)) {
    correctingPath(path);
    addPathSplitBefore(path);
  }

}

bool MeshPathes::isEndOfPathOnFront(const std::vector<VertexID>::const_iterator & p, const std::vector<VertexID>::const_iterator & nextp,
				    const std::vector<VertexID>::const_iterator & nextp2, const std::deque<VertexID> & path) {
  return (((*mesh).point(*p).getFlag() + (*mesh).point(*p).getIsBoundary() > 2) || // a junction point
	  ((path.size() >= 2) && ((*mesh).point(*p).getFlag() > (*mesh).point(*nextp).getFlag()) && (*p != *nextp2)) ||
	  ((path.size() >= 2) && (*(p - 1) == *nextp)) ||
	  ((path.size() >= 2) && !(*mesh).point(*(p - 1)).getIsBoundary() && (*mesh).point(*p).getIsBoundary()) ||
	  ((path.size() >= 2) && (*mesh).point(*p).getIsBoundary() && !(*mesh).point(*nextp).getIsBoundary()) ||
	  ((path.size() > 2) && ((*mesh).point(*(p - 1)).getFlag() + (*mesh).point(*p).getIsBoundary() != (*mesh).point(*p).getFlag() + (*mesh).point(*p).getIsBoundary())));

}

bool MeshPathes::isValidCuttingPathFromFront(const std::deque<VertexID> & path, const std::vector<VertexID> & front_l) const {
  return ((isSinglePath(path, front_l) ||
	   path.front() <= path.back() ||
	   ((path.front() == path.back()) && (path.size() >= 4) && (path[1] <= path[path.size() - 2])))
	  && !((*mesh).isBoundaryEdge(path.front(), path[1])));
}

bool MeshPathes::isSinglePath(const std::deque<VertexID> & path, const std::vector<VertexID> & front_l) const {
  if (path.size() < 2)
    return true;
  // check for the number of edges (path.front(), path[1]) in front
  VertexID first = path.front();
  VertexID second = path[1];
  unsigned int nb = 0;
  std::vector<VertexID>::const_iterator pred_l = front_l.begin() + (front_l.size() - 1);
  for(std::vector<VertexID>::const_iterator i = front_l.begin(); i != front_l.end(); pred_l = i, ++i)
    if (((*i == first) && (*pred_l == second)) ||
	((*pred_l == first) && (*i == second)))
      ++nb;
  return nb == 1;
}


NLoop MeshPathes::getShortestValidNLoop(VertexID b1, VertexID b2, unsigned int nbPaths) {

  if ((*mesh).point(b1).getIsBoundary()) {
    for(std::deque<VertexID>::const_iterator nb = (*mesh).point(b1).getNeighbours().begin(); nb != (*mesh).point(b1).getNeighbours().end(); ++nb)
      if (!(*mesh).point(*nb).getIsBoundary()) {
	b1 = *nb;
	break;
      }
    if ((*mesh).point(b1).getIsBoundary()) {
      return NLoop();
      //throw Exception("getShortestValidNLoop(3): cannot find a neighbour of the 1st basepoint inside the surface.");
    }
  }

  if ((*mesh).point(b2).getIsBoundary()) {
    for(std::deque<VertexID>::const_iterator nb = (*mesh).point(b2).getNeighbours().begin(); nb != (*mesh).point(b2).getNeighbours().end(); ++nb)
      if (!(*mesh).point(*nb).getIsBoundary()) {
	b2 = *nb;
	break;
      }
    if ((*mesh).point(b2).getIsBoundary()) {
      return NLoop();
      //throw Exception("getShortestValidNLoop(3): cannot find a neighbour of the 2nd basepoint inside the surface.");
    }
  }


  // compute paths contains on the cut locus of b1
  std::vector<std::vector<VertexID> > paths = getCutLocusPaths(b1);

  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    (*p).setFlag((*p).getIsBoundary() ? 0 : 1);

  // only compute distance on the complementary of the cut locus
  computeDijkstra(b1, 1);

  double * lengthb1 = length;
  VertexID * predb1 = pred;
  length = NULL;
  pred = NULL;

  for(std::vector<std::vector<VertexID> >::const_iterator path = paths.begin(); path != paths.end(); ++path) {
    for(std::vector<VertexID>::const_iterator p = (*path).begin(); p != (*path).end(); ++p)
      (*mesh).point(*p).setFlag(0);
  }

  if ((*mesh).point(b2).getFlag() == 0) {
    for(std::deque<VertexID>::const_iterator nb = (*mesh).point(b2).getNeighbours().begin(); nb != (*mesh).point(b2).getNeighbours().end(); ++nb)
      if ((*mesh).point(*nb).getFlag() == 1) {
	b2 = *nb;
	break;
      }
    if ((*mesh).point(b2).getFlag() == 0) {
      throw Exception("getShortestValidNLoop(3): cannot find a neighbour of the 2nd basepoint outside of the cut locus");
    }
  }


  NLoop result;

  // only compute distance on the complementary of the cut locus
  computeDijkstra(b2, 1);

  // the following return is not part of the algorithm, but a consequence of limitations due to the implementation choices.
  // in fact, rather than working on the dual graph, Taglut is working with the primary graph. A consequence is that the complentary
  // of the cut locus may be pinched. A consequence is that the Dijkstra algorithm from b2 may not build a path from b2 to b1...
  if (length[b1] < 0.0) {
    return result;
  }

  // TODO: debug mode
  // MeshMap mmaplocal(*mesh);
  // mmaplocal.setValuesFromLengths(*this);
  // std::cout << "Number of paths in cut locus: " << paths.size() << std::endl;
  // Display3D::displayMeshMap(512, 512, "debug", *mesh, mmaplocal);


  // then compute for each path of the cut locus the crossing location corresponding
  // to the shortest path between b1 and b2
  std::vector<std::pair<double, VertexID[3]> > pathDescs;
  for(std::vector<std::vector<VertexID> >::const_iterator path = paths.begin(); path != paths.end(); ++path)
    if ((*path).size() >= 3) {
      std::pair<double, VertexID[3]> crossing;
      crossing.first = std::numeric_limits<double>::max();
      std::vector<VertexID>::const_iterator ppred = (*path).begin();
      std::vector<VertexID>::const_iterator pnext = ppred + 2;
      for(std::vector<VertexID>::const_iterator p = ppred + 1; pnext != (*path).end(); ++p, ++ppred, ++pnext) {
	const Point3D & pt = (*mesh).point(*p);

	// on each side of the cut locus, find the starting of a shortest path to b1 and to b2
	double lb1[2];
	VertexID predPb1[2] = {(*mesh).getNbPoints(), (*mesh).getNbPoints()};
	double lb2[2];
	VertexID predPb2[2] = {(*mesh).getNbPoints(), (*mesh).getNbPoints()};

	unsigned char side = 0;
	for(std::deque<VertexID>::const_iterator nb = pt.getNeighbours().begin(); nb != pt.getNeighbours().end(); ++nb) {
	  // computing the side
	  if ((*nb == *ppred) || (*nb == *pnext))
	    side = (side + 1) % 2;
	  else if ((*mesh).point(*nb).getFlag() == 1) {
	    double lEdgeNb = (*mesh).point(*nb).distance(pt);
	    if (lengthb1[*nb] >= 0.0) // before: "!= 0"
	      if ((predPb1[side] == (*mesh).getNbPoints()) || (lb1[side] > lEdgeNb + lengthb1[*nb])) {
		lb1[side] = lEdgeNb + lengthb1[*nb];
		predPb1[side] = *nb;
	      }
	    if (length[*nb] >= 0.0)
	      if ((predPb2[side] == (*mesh).getNbPoints()) || (lb2[side] > lEdgeNb + length[*nb])) {
		lb2[side] = lEdgeNb + length[*nb];
		predPb2[side] = *nb;
	      }
	  }
	}

	for(unsigned char i = 0; i < 2; ++i)
	  if ((predPb1[i] != (*mesh).getNbPoints()) && (predPb2[(i + 1) % 2] != (*mesh).getNbPoints())) {
	    double l = lb1[i] + lb2[(i + 1) % 2];
	    if ((l < crossing.first) && (predb1[predPb1[i]] != *p) && (pred[predPb2[(i + 1) % 2]] != *p)) { // warning: don't keep crossings with edges along the cut locus
	      crossing.first = l;
	      crossing.second[0] = *p;
	      crossing.second[1] = predPb1[i];
	      crossing.second[2] = predPb2[(i + 1) % 2];
	    }
	}
      }
      if (crossing.first != std::numeric_limits<double>::max()) {
	pathDescs.push_back(crossing);
      }
    }

  // create the direct link between the two points
  std::pair<double, VertexID[3]> b1b2;
  b1b2.first = lengthb1[b2];
  b1b2.second[0] = b1b2.second[1] = b1b2.second[2] = b1;
  pathDescs.push_back(b1b2);

  // then order the path descriptors according to the length of the corresponding paths
  std::sort(pathDescs.begin(), pathDescs.end(), CompFirst<VertexID[3]>());

  // then build the first nbpaths corresponding paths and add it to the resulting NLoop
  unsigned int nbAdded = 0;
  for(std::vector<std::pair<double, VertexID[3]> >::const_iterator pdesc = pathDescs.begin(); pdesc != pathDescs.end(); ++pdesc, ++nbAdded) {
    if (nbAdded == nbPaths)
      break;

    std::vector<VertexID> pathb1;
    std::vector<VertexID> pathb2;
    // build the first part of the path

    pathb2.push_back((*pdesc).second[0]);

    if (pathb2.front() != (*pdesc).second[2])
      pathb2.push_back((*pdesc).second[2]);
    while(pathb2.back() != b2) {
#ifndef NDEBUG
      if (length[pathb2.back()] == -1)
	throw Exception("Error during NLoop reconstruction (0)");
      if (pred[pathb2.back()] == pathb2.back())
	throw Exception("Error during NLoop reconstruction (1)");
#endif
      pathb2.push_back(pred[pathb2.back()]);
      assert(pathb2.back() < (*mesh).getNbPoints());
    }


    if (pathb2.front() != b1) {
      // build the second part of the path
      pathb1.push_back((*pdesc).second[1]);
      while(pathb1.back() != b1) {
#ifndef NDEBUG
	if (lengthb1[pathb1.back()] == -1)
	  throw Exception("Error during NLoop reconstruction (2)");
	if (predb1[pathb1.back()] == pathb1.back())
	  throw Exception("Error during NLoop reconstruction (3)");
#endif
	pathb1.push_back(predb1[pathb1.back()]);
	assert(pathb1.back() < (*mesh).getNbPoints());
      }
    }

    // merge the two paths and add it to the NLoop
    reverse(pathb1.begin(), pathb1.end());
    pathb1.insert(pathb1.end(), pathb2.begin(), pathb2.end());
    result.addPath(pathb1);
  }

  delete [] lengthb1;
  delete [] predb1;

#ifndef NDEBUG
  unsigned int i = 0;
  for(std::vector<std::vector<VertexID> >::const_iterator p = result.getPaths().begin(); p != result.getPaths().end(); ++p, ++i)
    for(std::vector<VertexID>::const_iterator pt = (*p).begin(); pt != (*p).end(); ++pt) {
      if((*mesh).point(*pt).getIsBoundary()) {
	throw Exception("getShortestValidNLoop(3): point on boundary");
      }
    }
#endif
  return result;
}

NLoop MeshPathes::getShortestValidNLoopNaive(VertexID b1, VertexID b2, unsigned int nbPaths) {
  NLoop result;
  std::deque<VertexID> basePoints;
  basePoints.push_back(b1);
  basePoints.push_back(b2);

  // compute lengths from the two basepoints
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  computeDijkstra(basePoints, 0);

  // set flags for each of the surfaces on the complementary of the cut locus
  (*mesh).point(b1).setFlag(1);
  (*mesh).point(b2).setFlag(2);

  // build an open set with the neighbours of b1 and b2
  std::vector<VertexID> open;
  for(std::deque<VertexID>::const_iterator n = (*mesh).point(b1).getNeighbours().begin(); n != (*mesh).point(b1).getNeighbours().end(); ++n)
    if ((*mesh).point(*n).getFlag() == 0) {
      open.push_back(*n);
      (*mesh).point(*n).setFlag(-1);
    }
  for(std::deque<VertexID>::const_iterator n = (*mesh).point(b2).getNeighbours().begin(); n != (*mesh).point(b2).getNeighbours().end(); ++n)
    if ((*mesh).point(*n).getFlag() == 0) {
      open.push_back(*n);
      (*mesh).point(*n).setFlag(-1);
    }

  // compute flags for each point of the mesh
  while(open.size() != 0) {
    VertexID p = open.back();
    Vertex & v = (*mesh).point(p);
    open.pop_back();
    if (v.getFlag() <= 0) {
      if ((*mesh).point(pred[p]).getFlag() > 0) {
	v.setFlag((*mesh).point(pred[p]).getFlag());
	for(std::deque<VertexID>::const_iterator n = v.getNeighbours().begin(); n != v.getNeighbours().end(); ++n)
	  if ((*mesh).point(*n).getFlag() == 0) {
	    open.push_back(*n);
	    (*mesh).point(*n).setFlag(-1);
	  }
      }
      else
	v.setFlag(0);
    }
  }

  // then build a set of points that are in (1) and had neighbours in (2)
  // i.e. a list of border points
  std::vector<VertexID> borderPoints;
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 1) {
      for(std::deque<VertexID>::const_iterator n = (*p).getNeighbours().begin(); n != (*p).getNeighbours().end(); ++n)
	if ((*mesh).point(*n).getFlag() == 2) {
	  borderPoints.push_back((*p).getId());
	  (*p).setFlag(3);
	  break;
	}
    }


  // then build the connected components of the border points
  unsigned int nextBdFlag = 4;
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 3)
      computeCCUsingPointsSimple((*p).getId(), 3, nextBdFlag++);
  const unsigned int nbBd = nextBdFlag - 4;



  // check for the number of boundaries
  if (nbBd < nbPaths) {
    std::cout << "Number of boundaries: " << nbBd << std::endl;
    throw Exception("getShortestValidNLoopNaive(3): this non-complete method cannot compute a valid NLoop.");
  }

  // for each connected component, get the vertices corresponding to the shorter loop
  VertexID * bPoints = new VertexID[nbBd];
  double * bPointsLength = new double[nbBd];
  VertexID * bPointsOtherSide = new VertexID[nbBd];
  for(unsigned int i = 0; i < nbBd; ++i) {
    bPoints[i] = 0;
    bPointsOtherSide[i] = 0;
    bPointsLength[i] = std::numeric_limits<double>::max();
  }


  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() > 3) {
      const unsigned int idBd = (*p).getFlag() - 4;
      assert(idBd < nbBd);
      double l = length[(*p).getId()];
      double l2 = std::numeric_limits<double>::max();
      VertexID otherSide = (*mesh).getNbPoints();
      // find the neighbour in the other side of the boundary with the smaller length
      for(std::deque<VertexID>::const_iterator n = (*p).getNeighbours().begin(); n != (*p).getNeighbours().end(); ++n)
	if (((*mesh).point(*n).getFlag() == 2) && (l2 > length[*n] + (*mesh).point(*n).distance(*p))) {
	  l2 = length[*n] + (*mesh).point(*n).distance(*p);
	  otherSide = *n;
	}
      if (bPointsLength[idBd] > l + l2) {
	bPoints[idBd] = (*p).getId();
	assert(otherSide != (*mesh).getNbPoints());
	bPointsOtherSide[idBd] = otherSide;
	bPointsLength[idBd] = l + l2;
      }
    }


  // sort the paths according to the length
  std::vector<double> bPointsLengthSort;
  for(unsigned int i = 0; i < nbBd; ++i)
    bPointsLengthSort.push_back(bPointsLength[i]);
  std::sort(bPointsLengthSort.begin(), bPointsLengthSort.end());

  // if non wanted paths has the same length as the last selected path, modify their length
  if ((nbBd > nbPaths) && (bPointsLengthSort[nbPaths - 1] == bPointsLengthSort[nbPaths])) {
    double lastValue = bPointsLengthSort[nbBd - 1];
    unsigned int nbLastValue = 0;
    for(unsigned int i = 0; i < nbPaths; ++i)
      if (bPointsLengthSort[i] == lastValue)
	++nbLastValue;
    for(unsigned int i = 0; i < nbBd; ++i)
      if (bPointsLength[i] == lastValue) {
	if (nbLastValue > 0)
	  --nbLastValue;
	else
	  bPointsLength[i] = std::numeric_limits<double>::max();
      }
  }

  // then build the nbPaths corresponding paths, selecting the shortest one
  for(unsigned int i = 0; i < nbPaths; ++i) {
    std::vector<VertexID> b1side;
    VertexID current = bPoints[i];
    while(current != b1) {
      b1side.push_back(current);
      current = pred[current];
    }
    b1side.push_back(current);

    std::vector<VertexID> b2side;
    current = bPointsOtherSide[i];
    while(current != b2) {
      b2side.push_back(current);
      current = pred[current];
    }
    b2side.push_back(current);
    std::reverse(b1side.begin(), b1side.end());
    b1side.insert(b1side.end(), b2side.begin(), b2side.end());

    result.addPath(b1side);
  }


  delete[] bPoints;
  delete[] bPointsLength;
  delete[] bPointsOtherSide;

  return result;
}
