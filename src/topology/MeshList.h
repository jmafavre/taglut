/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_LIST
#define MESH_LIST

#include <deque>
#include <string>
#include "IDTypes.h"
#include "FileManipulator.h"

namespace Taglut {
  class Mesh;
  class MeshPart;
  class Mapping2D3D;
  class Mapping2D3DList;


  /**
     @class MeshList

     @author Jean-Marie Favreau
     @brief List of Mesh
  */
  class MeshList : public std::deque<Mesh>, public FileManipulator {

  private:

    bool findMeshBlockVRML(std::ifstream & infile) const;

    void addFormats();

  public:

    /**
       Constructor
    */
    MeshList();

    /**
       Constructor using a file
    */
    MeshList(const std::string & filename, const std::string & objectName = "");

    /**
       Constructor using a file
    */
    MeshList(const char * filename, const char * objectName);

#ifndef SWIG
    /**
       Affectation operator (using Mapping2D3DList)
    */
    MeshList & operator=(Mapping2D3DList & csets);

    /**
       Affectation operator (using Mesh)
    */
    MeshList & operator=(const Mesh & mesh);

    /**
       Affectation operator (using MeshPart)
    */
    MeshList & operator=(const MeshPart & meshPart);
#endif

    /**
       Return number of points
    */
    VertexID getNbPoints() const;

    /**
       Return number of connected components
    */
    unsigned long int getNbCC();

    /**
       Return number of triangles
    */
    TriangleID getNbTriangles() const;

    /**
       Compute number of flag values
    */
    unsigned long int getNbPointFlagValues() const;

    /**
       return true if all meshes are empty
    */
    bool isEmpty() const;

    /**
       return a list of connected components
    */
    std::deque<MeshPart> getConnectedComponents();

    /**
       compute largest connected component
    */
    MeshPart getLargestCC();

    /**
       Split all meshes in separated meshes using connected components
       @return true if has been splitten
    */
    bool splitCC();

    /**
       Return a mesh merging all meshes
    */
    Mesh getMesh() const;

    /**
       Merge all meshes in an unique mesh
    */
    void merge();

    /**
       Load MeshList from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Save MeshList from file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;


    /**
       Save the mesh list in the given file
    */
    void saveVRML(const std::string & filename) const;

    /**
       Load the mesh list from a file
    */
    void loadVRML(const std::string & fileName, const std::string & objectName = "");
  };
}

#endif
