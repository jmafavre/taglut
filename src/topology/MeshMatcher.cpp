/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMatcher.h"
#include "Mesh.h"

using namespace Taglut;

MeshMatcher::MeshMatcher(const Mesh & mesh1, const Mesh & mesh2) {
  double epsilon = 0.00001;
  if (mesh1.getNbPoints() == mesh2.getNbPoints()) {
    Mesh::const_point_iterator p2 = mesh2.point_begin();
    for(Mesh::const_point_iterator p1 = mesh1.point_begin(); p1 != mesh1.point_end(); ++p1, ++p2)
      (*this).push_back(std::pair<unsigned int, unsigned int>((*p1).getId(), (*p2).getId()));
  }
  else if (mesh1.getNbPoints() < mesh2.getNbPoints()) {
    Mesh::const_point_iterator p2 = mesh2.point_begin();
    for(Mesh::const_point_iterator p1 = mesh1.point_begin(); (p1 != mesh1.point_end()) && (p2 != mesh2.point_end()); ++p1, ++p2)
      if ((*p1).distance(*p2) < epsilon)
	(*this).push_back(std::pair<unsigned int, unsigned int>((*p1).getId(), (*p2).getId()));
      else
	++p2;
  }
  else { // (mesh1.getNbPoints() < mesh2.getNbPoints())
    Mesh::const_point_iterator p2 = mesh2.point_begin();
    for(Mesh::const_point_iterator p1 = mesh1.point_begin(); (p1 != mesh1.point_end()) && (p2 != mesh2.point_end()); ++p1, ++p2) {
      if ((*p1).distance(*p2) < epsilon)
	(*this).push_back(std::pair<unsigned int, unsigned int>((*p1).getId(), (*p2).getId()));
      else
	++p1;
    }
  }
}
