/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "PointModifier.h"
#include "Messages.h"
#include "StringManipulation.h"

using namespace Taglut;

PointModifier::PointModifier(const std::string & method_l, const std::string & options_l) : validityChecked(false), method(method_l), options(options_l) {
  srand(time(NULL));
  nbPoints = 0;
  validityChecked = false;
  mesh = NULL;
}

void PointModifier::setMesh(const Mesh & mesh_l) {
  if ((!validityChecked) && !isValidModifier())
    throw Exception("setMesh(1): selection method is not valid.");

  (*this).mesh = &mesh_l;
  nbPoints = mesh_l.getNbPoints();

  // compute min and max values
  if ((isPercentage || directionalIsPercentage[0] || directionalIsPercentage[1] || directionalIsPercentage[2]) &&
      ((rMethod == RMgaussian) || (rMethod == RMuniform) || (rMethod == RMtranslation))) {
    double minXValue = std::numeric_limits<double>::max();
    double maxXValue = -std::numeric_limits<double>::max();
    double minYValue = std::numeric_limits<double>::max();
    double maxYValue = -std::numeric_limits<double>::max();
    double minZValue = std::numeric_limits<double>::max();
    double maxZValue = -std::numeric_limits<double>::max();

    for(Mesh::const_point_iterator p = mesh_l.point_begin(); p != mesh_l.point_end(); ++p) {
      if ((*p).getX() > maxXValue) maxXValue = (*p).getX();
      if ((*p).getX() < minXValue) minXValue = (*p).getX();
      if ((*p).getY() > maxYValue) maxYValue = (*p).getY();
      if ((*p).getY() < minYValue) minYValue = (*p).getY();
      if ((*p).getZ() > maxYValue) maxZValue = (*p).getZ();
      if ((*p).getZ() < minYValue) minZValue = (*p).getZ();
    }

    double maxMeshX = maxXValue - minXValue;
    double maxMeshY = maxYValue - minYValue;
    double maxMeshZ = maxZValue - minZValue;

    double maxMesh = maxMeshX;
    if (maxMeshY > maxMesh)
      maxMesh = maxMeshY;
    if (maxMeshZ > maxMesh)
      maxMesh = maxMeshZ;

    if (directionalIsPercentage[0])
      directionalValue[0] = maxMeshX * directionalValueP[0];
    if (directionalIsPercentage[1])
      directionalValue[1] = maxMeshY * directionalValueP[1];
    if (directionalIsPercentage[2])
      directionalValue[2] = maxMeshZ * directionalValueP[2];

    if (isPercentage)
      value = maxMesh * valueP;
  }

}

std::string PointModifier::helpModificationMethods() {
  Message msg;

  msg << "Points are modified (method=\"modify-location\") using the \"method-options\" attributes: ";
  msg << "- \"move-uniform(max)\": selected points are moved arround their initial location using an uniform distribution (maximum translation value is given by the \"max\" parameter, a percentage of the whole mesh or an absolute value).";
  msg << "- \"move-uniform(x, y, z)\": selected points are moved arround their initial location using an uniform distribution (maximum translation value is given by the \"x\", \"y\" and \"z\" parameters, a percentage of the mesh or an absolute value).";
  msg << "- \"move-gaussian(dev)\": selected points are moved arround their initial location using a gaussian distribution (standard deviation value is given by the \"dev\" parameter, a percentage of the whole mesh or an absolute value).";
  msg << "- \"move-gaussian(x, y, z)\": selected points are moved arround their initial location using a gaussian distribution (standard deviation value is given by the \"x\", \"y\" and \"z\" parameters, percentage of the mesh or an absolute value).";
  msg << "- \"translation(x, y, z)\": seleted points are translated according to the \"x\", \"y\" and \"z\" values. Values are percentage of the whole mesh or absolute values.";
  msg << "- \"move-barycenter(n)\": seleted points are moved using a random barycenter of their neighbours, and a random translation according to its normal. Value for translation is percentage of the highest distance from the point to its neighbours or absolute value.";
  msg << "- \"move-barycenter2(n)\": seleted points are moved using a random barycenter of their neighbours (with a main neighbour with higher weigth), and a random translation according to its normal. Value for translation is percentage of the highest distance from the point to its neighbours or absolute value.";

  return msg.toString();
}

bool PointModifier::setIsUniform(unsigned int nb) {
  if (nb == 1)
    isUniform = true;
  else if (nb == 3)
    isUniform = false;
  else
    return false;

  return true;
}

void PointModifier::setValues(const std::deque<std::string> & optionsList) {
  assert(((isUniform && (optionsList.size() == 1)) || (!isUniform && (optionsList.size() == 3))));

  if (isUniform) {
    std::string maxString = optionsList.front();

    if (StringManipulation::isUnsignedPercent(maxString)) {
      isPercentage = true;
      valueP = StringManipulation::getPercent(maxString);
    }
    else {
      isPercentage = false;
      value = StringManipulation::getFloat(maxString);
    }
  }
  else {
    isPercentage = false;
    for(unsigned int i = 0; i < 3; ++i) {
      std::string valuei = optionsList[i];

      if (StringManipulation::isUnsignedPercent(valuei)) {
	directionalIsPercentage[i] = true;
	directionalValueP[i] = StringManipulation::getPercent(valuei);
      }
      else {
	directionalIsPercentage[i] = false;
	directionalValue[i] = StringManipulation::getFloat(valuei);
      }
    }
  }
}

bool PointModifier::isValidModifier() {
  validityChecked = true;

  // the method should be one of the defined methods
  if (method != "modify-location")
    return false;

  if (method == "modify-location") {
    if (StringManipulation::isFunction(options)) {
      std::string fName = StringManipulation::getFunctionName(options);
      std::deque<std::string> optionsList = StringManipulation::getParameters(options);

      if ((fName == "move-uniform") || (fName == "move-gaussian")) {
	if (!setIsUniform(optionsList.size()))
	  return false;

	setValues(optionsList);

	if (fName == "move-uniform")
	  rMethod = RMuniform;
	else if (fName == "move-gaussian")
	  rMethod = RMgaussian;
      }
      else if (fName == "translation") {
	if (!setIsUniform(optionsList.size()))
	  return false;

	setValues(optionsList);

	rMethod = RMtranslation;
      }
      else if ((fName == "move-barycenter") || (fName == "move-barycenter2")) {
	if (optionsList.size() != 1)
	  return false;
	else
	  isUniform = true;

	setValues(optionsList);

	if (fName == "move-barycenter")
	  rMethod = RMbarycenter;
	else
	  rMethod = RMbarycenter2;
      }
      else
	return false;
    }
    else
      return false;
  }
  else
    return false;

  return true;
}

void PointModifier::modify(Point3D & point) {
  if (!validityChecked)
    throw Exception("modify(1): validity of the selection method has not been checked, and mesh has not been set.");

  if (nbPoints == 0)
    throw Exception("modify(1): mesh has not been set.");
  if (point.getId() > nbPoints)
    throw Exception("modify(1): mesh is smaller than the given point.");

  if ((rMethod == RMgaussian) || (rMethod == RMuniform) || (rMethod == RMtranslation)) {
    Coord3D move;

    move.setX(movingValue(0));
    move.setY(movingValue(1));
    move.setZ(movingValue(2));

    point += move;
  }
  else if ((rMethod == RMbarycenter) || (rMethod == RMbarycenter2)) {
    if (isPercentage) {
      double length = 0.0;
      for(std::deque<VertexID>::const_iterator p = (*mesh).point(point.getId()).getNeighbours().begin();
	  p != (*mesh).point(point.getId()).getNeighbours().end(); ++p) {
	double localLength = point.distance((*mesh).point(*p));
	if (localLength > length)
	  length = localLength;
      }
      value = valueP * length;
    }

    if (rMethod == RMbarycenter)
      *((Coord3D *)&point) = randomBarycenter(point.getNeighbours()) + (*mesh).computePointNormal(point.getId()).normalize() * cimg::crand() * value;
    else
      *((Coord3D *)&point) = randomBarycenter2(point.getNeighbours()) + (*mesh).computePointNormal(point.getId()).normalize() * cimg::crand() * value;
  }
}

Coord3D PointModifier::randomBarycenter(const std::deque<VertexID> & points) const {
  std::deque<double> weights;

  for(VertexID i = 0; i < points.size(); ++i)
    weights.push_back(cimg::rand());

  return (*mesh).getBarycenter(points, weights);
}

Coord3D PointModifier::randomBarycenter2(const std::deque<VertexID> & points) const {
  std::deque<double> weights;

  for(VertexID i = 0; i < points.size(); ++i)
    weights.push_back(cimg::rand());

  VertexID selected = (VertexID)floor(cimg::rand() * points.size());
  assert(selected < points.size());
  weights[selected] = cimg::rand() * points.size();

  return (*mesh).getBarycenter(points, weights);
}

double PointModifier::movingValue(unsigned int axis) const {
  switch(rMethod) {
  case RMgaussian:
    if (isUniform)
      return cimg::grand() * value;
    else
      return cimg::grand() * directionalValue[axis];
  case RMuniform:
    if (isUniform)
      return cimg::crand() * value;
    else
      return cimg::crand() * directionalValue[axis];
  case RMtranslation:
  default:
    if (isUniform)
      return value;
    else
      return directionalValue[axis];
  }
}
