/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "SpaceSpliter.h"
#include "Mesh.h"

using namespace Taglut;

void SpaceSpliter::addTriangle(const Triangle3D & t, TriangleID id) {
  assert((triangleList != NULL) && (id < (*triangleList).size()));
  if (children == NULL) {
    if (triangles != NULL)
      (*triangles).push_back(id);
  }
  else {
    for(unsigned int i = 0; i < 2; ++i)
      for(unsigned int j = 0; j < 2; ++j)
	for(unsigned int k = 0; k < (flat ? 1 : 2); ++k)
	  if ((*(children[i + j * 2 + k * 4])).intersect(t))
	    (*(children[i + j * 2 + k * 4])).addTriangle(t, id);
  }
}

void SpaceSpliter::addPoint(VertexID i) {
  assert(i < pointList.size());

  if (children == NULL)
    (*points).push_back(i);
  else {
    const Point3D & point = pointList[i];
    unsigned int idX = (point(0) < mX) ? 0 : 1;
    unsigned int idY = (point(1) < mY) ? 0 : 1;
    unsigned int idZ = (flat || (point(2) < mZ)) ? 0 : 1;
    (*(children[idX + idY * 2 + idZ * 4])).addPoint(i);
  }
}

void SpaceSpliter::initPointsAndTriangles() {
  isInitialised(true);
  for(VertexID i = 0; i < pointList.size(); ++i)
    addPoint(i);

  if (triangleList != NULL)
    for(TriangleID i = 0; i < (*triangleList).size(); ++i) {
      addTriangle(Triangle3D(pointList[(*triangleList)[i].getP1()],
			     pointList[(*triangleList)[i].getP2()],
			     pointList[(*triangleList)[i].getP3()]), i);
    }
}


void SpaceSpliter::isInitialised(bool value) {
  initialised = value;
  if (children != NULL) {
    if (flat)
      for(unsigned int i = 0; i < 4; ++i)
	(*(children[i])).isInitialised(value);
    else
      for(unsigned int i = 0; i < 8; ++i)
	(*(children[i])).isInitialised(value);
  }
}



void SpaceSpliter::createStructure(unsigned int depth) {
  mX = minX + (maxX - minX) / 2;
  mY = minY + (maxY - minY) / 2;
  mZ = minZ + (maxZ - minZ) / 2;

  if (depth <= 0) {
    children = NULL;
    points = new std::vector<VertexID>;

    if (triangleList != NULL)
      triangles = new std::vector<TriangleID>;
  }
  else {
    points = NULL;
    if (flat)
      children = new SpaceSpliter *[4];
    else
      children = new SpaceSpliter *[8];
    if (children == NULL)
      throw Exception(std::string("Error during memory allocation."));

    if (flat)
      for(unsigned int i = 0; i < 2; ++i)
	for(unsigned int j = 0; j < 2; ++j)
	  children[i + j * 2] = new SpaceSpliter(depth - 1, pointList, triangleList,
						 minX + i * (maxX - minX) / 2,
						 minY + j * (maxY - minY) / 2,
						 minZ,
						 minX + (i + 1) * (maxX - minX) / 2,
						 minY + (j + 1) * (maxY - minY) / 2,
						 maxZ);
    else
      for(unsigned int i = 0; i < 2; ++i)
	for(unsigned int j = 0; j < 2; ++j)
	  for(unsigned int k = 0; k < 2; ++k)
	    children[i + j * 2 + k * 4] = new SpaceSpliter(depth - 1, pointList, triangleList,
							   minX + i * (maxX - minX) / 2,
							   minY + j * (maxY - minY) / 2,
							   minZ + k * (maxZ - minZ) / 2,
							   minX + (i + 1) * (maxX - minX) / 2,
							   minY + (j + 1) * (maxY - minY) / 2,
							   minZ + (k + 1) * (maxZ - minZ) / 2);
  }
}

void SpaceSpliter::computeMinMax() {
  minX = minY = minZ = std::numeric_limits<double>::max();
  maxX = maxY = maxZ = -std::numeric_limits<double>::max();
  for(std::vector<Point3D>::const_iterator i = pointList.begin(); i != pointList.end(); ++i) {
    if ((*i)(0) < minX) minX = (*i)(0);
    if ((*i)(1) < minY) minY = (*i)(1);
    if ((*i)(2) < minZ) minZ = (*i)(2);
    if ((*i)(0) > maxX) maxX = (*i)(0);
    if ((*i)(1) > maxY) maxY = (*i)(1);
    if ((*i)(2) > maxZ) maxZ = (*i)(2);
  }
}

SpaceSpliter::SpaceSpliter(const Mesh & mesh, bool flat_t) : pointList(mesh.getPoints()), triangleList(&(mesh.getTriangles())), initialised(false), flat(flat_t) {
  computeMinMax();
  createStructure((unsigned int)(pow(mesh.getNbPoints(), 1./3) / 5));
}


SpaceSpliter::SpaceSpliter(const std::vector<Point3D> & points_t, bool flat_t) : pointList(points_t), triangleList(NULL), initialised(false), flat(flat_t) {
  computeMinMax();
  createStructure((unsigned int)(pow(points_t.size(), 1./3) / 5));
}


SpaceSpliter::SpaceSpliter(unsigned int depth, const std::vector<Point3D> & points_t,
			   const std::vector<Triangle> * triangles_t,
			   double minX_t, double minY_t, double minZ_t,
			   double maxX_t, double maxY_t, double maxZ_t, bool flat_t) :
  Box3D(minX_t, maxX_t, minY_t, maxY_t, minZ_t, maxZ_t),
  pointList(points_t), triangleList(triangles_t), initialised(false), flat(flat_t) {
  createStructure(depth);
}


SpaceSpliter::~SpaceSpliter() {
  if (points != NULL)
    delete(points);
  if (children != NULL) {
    if (flat)
      for(unsigned int i = 0; i < 4; ++i)
	delete(children[i]);
    else
      for(unsigned int i = 0; i < 8; ++i)
	delete(children[i]);
    delete [] children;
  }
}

void SpaceSpliter::getSameLocationPoints(VertexID id, std::deque<VertexID> & result) {
  if (!initialised)
    initPointsAndTriangles();

  const Point3D & point = pointList[id];
  if (children == NULL) {
    for(std::vector<VertexID>::const_iterator i = (*points).begin(); i != (*points).end(); ++i)
      if (point == pointList[*i])
	if (*i != id)
	  result.push_back(*i);
  }
  else {
    unsigned int idX = (point(0) < mX) ? 0 : 1;
    unsigned int idY = (point(1) < mY) ? 0 : 1;
    unsigned int idZ = (flat || (point(2) < mZ)) ? 0 : 1;
    (*(children[idX + idY * 2 + idZ * 4])).getSameLocationPoints(id, result);
  }
}

VertexID SpaceSpliter::findFirstX() {
  if (!initialised)
    initPointsAndTriangles();


  if (children == NULL) {
    VertexID result = pointList.size();

    for(std::vector<VertexID>::const_iterator i = (*points).begin(); i != (*points).end(); ++i)
      if ((result == pointList.size()) || (pointList[*i](0) < pointList[result](0)))
	result = *i;

    return result;
  }
  else {
    VertexID result = 0;

    for(unsigned int j = 0; j < 2; ++j)
      for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
	VertexID local = (*children[j * 2 + k * 4]).findFirstX();
	if ((result >= pointList.size()) || ((local < pointList.size()) && (pointList[local](0) < pointList[result](0))))
	  result = local;
      }

    return result;
  }
}

VertexID SpaceSpliter::findFirstSameX(VertexID id) {
  VertexID result = pointList.size();

  if (!initialised)
    initPointsAndTriangles();

  if (children == NULL) {
    if ((*points).size() != 0) // if no points on current box, return error
      for(std::vector<VertexID>::const_iterator i = (*points).begin(); i != (*points).end(); ++i)
	if (pointList[*i](0) == pointList[id](0))
	  if ((result == pointList.size()) || (pointList[*i](1) < pointList[result](1)))
	    result = *i;
  }
  else {
    unsigned int idX = (pointList[id](0) < mX) ? 0 : 1;

    // find in the most y-left part
    for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
      VertexID local = (*(children[idX + k * 4])).findFirstSameX(id);
      if (local < pointList.size())
	if ((result == pointList.size()) || (pointList[local](1) < pointList[result](1))) {
	  result = local;
	}
      }

    // if not found, look for the y-right part
    if (result == pointList.size())
      for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
	VertexID local = (*(children[idX + 2 + k * 4])).findFirstSameX(id);
	if (local < pointList.size())
	  if ((result == pointList.size()) || (pointList[local](1) < pointList[result](1))) {
	    result = local;
	  }
      }
  }

  return result;
}


VertexID SpaceSpliter::findNextSameX(VertexID id) {
  VertexID result = pointList.size();

  if (!initialised)
    initPointsAndTriangles();

  if (children == NULL) {
    if ((*points).size() != 0) // if no points on current box, return error
      for(std::vector<VertexID>::const_iterator i = (*points).begin(); i != (*points).end(); ++i)
	if ((pointList[*i](0) == pointList[id](0)) && (pointList[*i](1) > pointList[id](1))) // point at the y-right of id
	    if ((result == pointList.size()) || (pointList[*i](1) < pointList[result](1)))
	      result = *i;
  }
  else {
    assert(id < pointList.size());
    unsigned int idX = (pointList[id](0) < mX) ? 0 : 1;
    unsigned int idY = (pointList[id](1) < mY) ? 0 : 2;

    // find in the box nearest than id point
    for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
      VertexID local = (*(children[idX + idY + k * 4])).findNextSameX(id);
      if (local < pointList.size())
	if ((result == pointList.size()) || (pointList[local](1) < pointList[result](1))) {
	  result = local;
	}
      }

    // if not found, look for the next part
    if ((result == pointList.size()) && (idY == 0)) {
      for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
	VertexID local = (*(children[idX + 2 + k * 4])).findNextSameX(id);
	if (local < pointList.size())
	  if ((result == pointList.size()) || (pointList[local](1) < pointList[result](1))) {
	    result = local;
	  }
      }
    }

  }

  return result;
}



VertexID SpaceSpliter::findNextSameY(VertexID id) {
  VertexID result = pointList.size();

  if (!initialised)
    initPointsAndTriangles();


  if (children == NULL) {
    if ((*points).size() != 0) // if no points on current box, return error
      for(std::vector<VertexID>::const_iterator i = (*points).begin(); i != (*points).end(); ++i)
	if ((pointList[*i](1) == pointList[id](1)) && (pointList[*i](0) > pointList[id](0))) // point at the x-right of id
	  if ((result == pointList.size()) || (pointList[*i](0) < pointList[result](0)))
	    result = *i;
  }
  else {
    unsigned int idX = (pointList[id](0) < mX) ? 0 : 1;
    unsigned int idY = (pointList[id](1) < mY) ? 0 : 2;

    // find in the box nearest than id point
    for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
      VertexID local = (*(children[idX + idY + k * 4])).findNextSameY(id);
      if (local < pointList.size())
	if ((result == pointList.size()) || (pointList[local](0) < pointList[result](0))) {
	  result = local;
	}
      }

    // if not found, look for the next part
    if ((result == pointList.size()) && (idX == 0)) {
      for(unsigned int k = 0; k < (flat ? 1 : 2); ++k) {
	VertexID local = (*(children[1 + idY + k * 4])).findNextSameY(id);
	if (local < pointList.size())
	  if ((result == pointList.size()) || (pointList[local](0) < pointList[result](0))) {
	    result = local;
	  }
      }
    }
  }
  return result;

}

std::vector<TriangleID> SpaceSpliter::findTrianglesInRay(const Coord2D & point) {
  if (triangleList == NULL)
    return std::vector<TriangleID>();

  if (!initialised)
    initPointsAndTriangles();

  if (children == NULL) {
    std::vector<TriangleID> result;
    const Coord3D back(0, 0, 1.0);
    Line3D line(Coord3D(point.get2DX(), point.get2DY(), 0.0), back);
    for(std::vector<TriangleID>::const_iterator idT = (*triangles).begin(); idT != (*triangles).end(); ++idT) {
      Triangle3D t(pointList[(*triangleList)[*idT].getP1()],
		   pointList[(*triangleList)[*idT].getP2()],
		   pointList[(*triangleList)[*idT].getP3()]);
      if (t.intersect(line))
	result.push_back(*idT);
    }

    return result;
  }
  else {
    unsigned int idX = (point.get2DX() < mX) ? 0 : 1;
    unsigned int idY = (point.get2DX() < mY) ? 0 : 1;
    if (flat)
      return (*(children[idX + idY * 2])).findTrianglesInRay(point);
    else {
      std::vector<TriangleID> result = (*(children[idX + idY * 2])).findTrianglesInRay(point);
      std::vector<TriangleID> result2 = (*(children[idX + idY * 2 + 4])).findTrianglesInRay(point);
      for(std::vector<TriangleID>::const_iterator i = result2.begin(); i != result2.end(); ++i)
	result.push_back(*i);
      return result;
    }
  }

}
