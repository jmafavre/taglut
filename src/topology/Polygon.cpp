/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Polygon.h"

using namespace Taglut;

Polygon::Polygon(Mapping2D3D & mapping_t, const std::deque<VertexID> & path1,
	const std::deque<VertexID> & path2,
	const std::deque<VertexID> & path3,
	const std::deque<VertexID> & path4, bool verbose) : mapping(mapping_t), isConvex(false) {
  if (verbose) {
    std::cout << "Polygon creation" << std::endl;

    std::cout << "path1:";
    for(std::deque<VertexID>::const_iterator i = path1.begin(); i != path1.end(); ++i)
      std::cout << " " << *i;
    std::cout << std::endl;
    std::cout << "path2:";
    for(std::deque<VertexID>::const_iterator i = path2.begin(); i != path2.end(); ++i)
      std::cout << " " << *i;
    std::cout << std::endl;
    std::cout << "path3:";
    for(std::deque<VertexID>::const_iterator i = path3.begin(); i != path3.end(); ++i)
      std::cout << " " << *i;
    std::cout << std::endl;
    std::cout << "path4:";
    for(std::deque<VertexID>::const_iterator i = path4.begin(); i != path4.end(); ++i)
      std::cout << " " << *i;
    std::cout << std::endl;
  }

  assert(path1.back() == path2.front());
  assert(path2.back() == path3.front());
  assert(path3.back() == path4.front());
  assert(path4.back() == path1.front());

  for(std::deque<VertexID>::const_iterator i = path1.begin() + 1; i != path1.end(); ++i)
    push_back(*i);
  for(std::deque<VertexID>::const_iterator i = path2.begin() + 1; i != path2.end(); ++i)
    push_back(*i);
  for(std::deque<VertexID>::const_iterator i = path3.begin() + 1; i != path3.end(); ++i)
    push_back(*i);
  for(std::deque<VertexID>::const_iterator i = path4.begin() + 1; i != path4.end(); ++i)
    push_back(*i);
}


std::deque<VertexID> Polygon::computeConvexHullHalfPart(const std::deque<VertexID> & inList,
							    Point2D & pmin, Point2D & pmax, bool bottom) const {
  std::deque<VertexID> result;
  if (inList.size() > 1) {
    std::deque<VertexID> listPminP;
    std::deque<VertexID> listPPmax;
    VertexID pId = inList.front();
    double dMax = 0.0;

    // compute line equation
    double a = (pmax.get2DY() - pmin.get2DY()) / (.01+ pmax.get2DX() - pmin.get2DX());
    double b = pmin.get2DY() - a * pmin.get2DX();

    // find point farest to the line
    for(std::deque<VertexID>::const_iterator i = inList.begin(); i != inList.end(); ++i) {
      double localDistance = fabs(mapping[*i].get2DY() - a * mapping[*i].get2DX() - b) / sqrt(a * a + b * b);
      if (localDistance > dMax) {
	dMax = localDistance;
	pId = *i;
      }
    }
    Point2D & p = mapping[pId];

    // then compute (pmin, p) equation
    a = (p.get2DY() - pmin.get2DY()) / (.01+ p.get2DX() - pmin.get2DX());
    b = pmin.get2DY() - a * pmin.get2DX();

    // and build list of point in the good side of the (pmin, p) line
    for(Polygon::const_iterator i = inList.begin(); i != inList.end(); ++i)
      if ((*i != p.getId()) && (((!bottom) && (mapping[*i].get2DY() > b + a * mapping[*i].get2DX())) ||
				 ((bottom) && (mapping[*i].get2DY() < b + a * mapping[*i].get2DX()))))
	listPminP.push_back(*i);

    // then compute (p, pmax) equation
    a = (pmax.get2DY() - p.get2DY()) / (.01+ pmax.get2DX() - p.get2DX());
    b = pmax.get2DY() - a * pmax.get2DX();

    // and build list of point in the good side of the (pmin, p) line
    for(Polygon::const_iterator i = inList.begin(); i != inList.end(); ++i)
      if ((*i != p.getId()) && (((!bottom) && (mapping[*i].get2DY() > b + a * mapping[*i].get2DX())) ||
				((bottom) && (mapping[*i].get2DY() < b + a * mapping[*i].get2DX()))))
	listPPmax.push_back(*i);


    // then compute convex hull recursively
    assert(listPminP.size() < inList.size());
    assert(listPPmax.size() < inList.size());
    std::deque<VertexID> resultPminP = computeConvexHullHalfPart(listPminP, pmin, p, bottom);
    std::deque<VertexID> resultPPmax = computeConvexHullHalfPart(listPPmax, p, pmax, bottom);

    // and build result list
    for(std::deque<VertexID>::const_iterator i = resultPminP.begin(); i != resultPminP.end(); ++i)
      result.push_back(*i);
    result.push_back(p.getId());
    for(std::deque<VertexID>::const_iterator i = resultPPmax.begin(); i != resultPPmax.end(); ++i)
      result.push_back(*i);

  }
  else if (inList.size() == 1) {
    result.push_back(inList.back());
  }

  return result;
}



Polygon Polygon::computeConvexHull() const {
  if (size() <= 3) // if polygon is a triangle, return himself
    return *this;
  else {
    std::deque<VertexID> resultList;

    // find points with min and max x location
    VertexID pminId = front();
    VertexID pmaxId = front();
    double pminx = mapping[pminId].get2DX();
    double pmaxx = mapping[pmaxId].get2DX();
    VertexID nextPmin = 0;
    bool firstTop = false;
    for(Polygon::const_iterator i = begin(); i != end(); ++i) {
      double x = mapping[*i].get2DX();
      if (pminx > x) {
	pminx = x;
	pminId = *i;
	Polygon::const_iterator in = i;
	++in;
	if (in == end())
	  nextPmin = front();
	else
	  nextPmin = *in;
      }
      if (pmaxx < x) {
	pmaxx = x;
	pmaxId = *i;
      }
    }
    Point2D & pmin = mapping[pminId];
    Point2D & pmax = mapping[pmaxId];

    // compute line equation
    double a = (pmax.get2DY() - pmin.get2DY()) / (.01 + pmaxx - pminx);
    double b = pmin.get2DY() - a * pminx;


    // then build list of points up and bottom the line
    std::deque<VertexID> topList;
    std::deque<VertexID> bottomList;
    for(Polygon::const_iterator i = begin(); i != end(); ++i)
      if ((*i != pmax.getId()) && (*i != pmin.getId())) {
	if (mapping[*i].get2DY() > b + a * mapping[*i].get2DX()) {
	  topList.push_back(*i);
	  if (*i == nextPmin)
	    firstTop = true;
	}
	else {
	  bottomList.push_back(*i);
	  if (*i == nextPmin)
	    firstTop = false;
	}
      }

    std::deque<VertexID> topResultList = computeConvexHullHalfPart(topList, pmin, pmax, false);
    std::deque<VertexID> bottomResultList = computeConvexHullHalfPart(bottomList, pmin, pmax, true);

    if (topList.size() * bottomList.size() == 0) {
      // first find the point after pmin  in Polygon
      Polygon::const_iterator i = begin();
      while(*i != pmin.getId()) ++i;
      ++i;
      if (i == end()) i = begin();
      // then determine order
      firstTop = (((topList.size() == 0) && (*i == pmax.getId())) ||
		  ((bottomList.size() == 0) && (*i != pmax.getId())));
    }

    // find order of original path

    // and build result list
    resultList.push_back(pmin.getId());

    if (firstTop)
      for(std::deque<VertexID>::const_iterator i = topResultList.begin(); i != topResultList.end(); ++i)
	resultList.push_back(*i);
    else
      for(std::deque<VertexID>::const_iterator i = bottomResultList.begin(); i != bottomResultList.end(); ++i)
	resultList.push_back(*i);


    resultList.push_back(pmax.getId());

    if (firstTop)
      for(std::deque<VertexID>::reverse_iterator i = bottomResultList.rbegin(); i != bottomResultList.rend(); ++i)
	resultList.push_back(*i);
    else
      for(std::deque<VertexID>::reverse_iterator i = topResultList.rbegin(); i != topResultList.rend(); ++i)
	resultList.push_back(*i);


    return Polygon(mapping, resultList, true);

  }
}

TriangleID Polygon::findFirstTriangleInside() const {
  assert(size() >= 3);

  VertexID other = back();
  // for each edge of the polygon
  for(Polygon::const_iterator current = begin(); current != end(); ++current) {
    assert(mapping.point3D(other).hasNeighbour(*current));
    // check each adjacent triangle
    try {
      Triangle & t1 = mapping.getMesh().findTriangle(other, *current);

      if (isInside(mapping.compute2DBarycenter(t1)))
	return t1.getId();
      else {
	Triangle & t2 = mapping.getMesh().findOtherTriangle(other, *current, t1);

	if (isInside(mapping.compute2DBarycenter(t2)))
	  return t2.getId();
      }
    }
    catch (Exception e) { }

    other = *current;
  }

  return mapping.getMesh().getNbTriangles();
}

bool Polygon::isInside(const Coord2D & point) const {
  bool inside = false;
  VertexID j = back();
  double x = point.get2DX();
  double y = point.get2DY();

  for(Polygon::const_iterator i = begin(); i != end(); ++i) {
    const Point2D & pi = mapping[*i];
    const Point2D & pj = mapping[j];

    if ((((pi.get2DY() <= y) && (y < pj.get2DY())) || ((pj.get2DY() <= y) && (y < pi.get2DY()))) &&
	(x < (pj.get2DX() - pi.get2DX()) * (y - pi.get2DY()) / (pj.get2DY() - pi.get2DY()) + pi.get2DX()))
      inside = !inside;
    j = *i;
  }

  return inside;
}

double Polygon::computeArea() {
  // if polygon is not convex, compute area using the original triangles
  if (!isConvex)
    return computeAreaUsingTriangles();
  double result = 0;

  Coord2D b = mapping.compute2DBarycenter(*this);


  VertexID lastPoint = back();

  for(Polygon::const_iterator i = begin(); i != end(); ++i) {
    result += Coord2D::compute2DTriangleArea(b, mapping[*i], mapping[lastPoint]);
    lastPoint = *i;
  }

  return result;
}

bool Polygon::isBorderEdge(VertexID p1, VertexID p2) const {
  VertexID lastPoint = back();
  for(Polygon::const_iterator i = begin(); i != end(); ++i) {
    if (((lastPoint == p1) && (*i == p2)) ||
	((lastPoint == p2) && (*i == p1)))
      return true;
    lastPoint = *i;
  }

  return false;
}

double Polygon::computeAreaUsingTriangles() {
  Mesh & mesh = mapping.getMesh();
  std::deque<TriangleID> openSet;

  double result = 0.0;

  mesh.setTriangleFlag(0);


  TriangleID t = findFirstTriangleInside();
  if (t >= mesh.getNbTriangles()) {
    return 0.0;
  }

  openSet.push_front(t);
  while(openSet.size() != 0) {
    Triangle & t_t = mesh.triangle(openSet.front());
    openSet.pop_front();
    t_t.setFlag(1);
    result += Point2D::compute2DTriangleArea(mapping[t_t.getP1()], mapping[t_t.getP2()], mapping[t_t.getP3()]);

    for(unsigned int i = 0; i < 3; ++i) { // no iterator available
      std::pair<VertexID, VertexID> edge = t_t.getEdge(i);

      if (!isBorderEdge(edge.first, edge.second)) {
	Triangle & otherTriangle = mesh.findOtherTriangle(edge.first, edge.second, t_t);
	if (otherTriangle.getFlag() != 1) {
	    openSet.push_front(otherTriangle.getId());
	    otherTriangle.setFlag(1);
	}
      }
    }
  }

  return result;
}


bool Polygon::isNotCrushed(double ratio, bool verbose) {
  double a1 = computeArea();
  double a2 = computeConvexHull().computeArea();
  if (verbose)
    std::cout << " Ratio between polygon and its hull: " << (a1 / a2) << std::endl;
  assert(a1 * 1.001 <= a2);
  return a1 / a2  >= ratio;
}

