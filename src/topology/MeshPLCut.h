/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESHPLCUT
#define MESHPLCUT

#include "PLPath.h"
#include "PreNLoop.h"
#include "IDTypes.h"
#include "MeshMap.h"

#include <vector>
#include <list>

namespace Taglut {

  class Mesh;
  class MTiling;

  /**
     @class MeshCut
     Mesh manipulator using path cuts
  */
  class MeshPLCut {
  private:

    std::vector<MeshMap *> scalarfunctions;
    std::vector<std::vector<double> > newSFValues;

    class TriangleWithCut : public Triangle {
    private:
      /** description of the cuts */
      std::vector<std::pair<PointOnEdge, PointOnEdge> > cuts;
      /** description of the new ids for corners */
      std::list<VertexID> corners[3];

    public:
      TriangleWithCut() : Triangle() {
      }

      /** constructor using triangle */
      TriangleWithCut(const Triangle & t) : Triangle(t) {
      }
      /** copy constructor */
      TriangleWithCut(const TriangleWithCut & t) : Triangle(t), cuts(t.cuts) {
      }

      /** add a new point on the structure */
      inline void addCut(const PointOnEdge & p1, const PointOnEdge & p2) {
	assert(hasPoint(p1.getFirstVertex()) || hasPoint(p1.getSecondVertex()));
	assert(hasPoint(p2.getFirstVertex()) || hasPoint(p2.getSecondVertex()));
	cuts.push_back(std::pair<PointOnEdge, PointOnEdge>(p1, p2));
      }

      /** return the number of cuts */
      inline unsigned int nbCuts() const {
	return cuts.size();
      }

      /** return the cuts */
      inline const std::vector<std::pair<PointOnEdge, PointOnEdge> > & getCuts() const {
	return cuts;
      }

      /** triangle initialization*/
      TriangleWithCut & operator=(const Triangle & t) {
	setP1(t.getP1());
	setP2(t.getP2());
	setP3(t.getP3());
	setId(t.getId());
	return *this;
      }

      /** return the number of cut that are going through the given \p corner, and along the edge
       (corner, cornerbis) */
      unsigned int getNbCutInEdgeThroughCorner(VertexID corner, VertexID cornerbis) const {
	unsigned int result = 0;
	for(std::vector<std::pair<PointOnEdge, PointOnEdge> >::const_iterator cut = cuts.begin(); cut != cuts.end(); ++cut)
	  if ((*cut).first.isVertex() && ((*cut).first.getClosestVertex() == corner)) {
	    if ((*cut).second.isVertex()) {
	      if (cornerbis == (*cut).second.getClosestVertex())
		result += 1;
	    }
	    else {
	      if ((*cut).second.hasVertex(corner) && (*cut).second.hasVertex(cornerbis))
		result += 1;
	    }
	  }
	  else if ((*cut).second.isVertex() && ((*cut).second.getClosestVertex() == corner)) {
	    if ((*cut).first.isVertex()) {
	      if (cornerbis == (*cut).first.getClosestVertex())
		result += 1;
	    }
	    else {
	      if ((*cut).first.hasVertex(corner) && (*cut).first.hasVertex(cornerbis))
		result += 1;
	    }
	  }
	return result;
      }

      /** return the number of cut that are going through the given corner, except the one located on the edge
       (corner, cornerbis) */
      unsigned int getNbCutThroughCorner(VertexID corner) const {
	unsigned int result = 0;
	for(std::vector<std::pair<PointOnEdge, PointOnEdge> >::const_iterator cut = cuts.begin(); cut != cuts.end(); ++cut)
	  if ((*cut).first.isVertex() && ((*cut).first.getClosestVertex() == corner))
	    result += 1;
	  else if ((*cut).second.isVertex() && ((*cut).second.getClosestVertex() == corner))
	    result += 1;
	return result;
      }

      /**
	 set the new ids associated to the given corner, add all the points, from the begin to the end (included),
	 using a cyclic description (realEnd and realBegin are the real end and begin of the structure)
      */
      template <class T>
      void setNewIdsOnCorner(VertexID corner, const T & begin, const T & end, const T & realEnd, const T & realBegin) {
	unsigned int pos = getVertexPositionById(corner);
	assert(pos != 3);
	for(T it = begin; it != end;) {
	  corners[pos].push_back(*it);
	  ++it;
	  if (it == realEnd)
	    it = realBegin;
	}
	corners[pos].push_back(*end);
      }

      /**
	 return the number of new points defined for each corner */
      unsigned int getNbNewIdsOnCorner(unsigned int i) const {
	if (i > 2)
	  return 0;
	else
	  return corners[i].size();
      }

      VertexID getIdOnCorner(unsigned int i) const {
	const unsigned int nb = getNbNewIdsOnCorner(i);
	if (nb == 0)
	  return getVertexId(i);
	else if (nb == 1)
	  return corners[i].front();
	else {
	  throw Exception("Wrong number of new points on a corner");
	}
      }

      /** get a new ID in the given corner, starting from the begining of the list,
	  and pop-it from the structure. If the list is empty, the function return the
	  initial ID. */
      VertexID popFirstIDOnCorner(unsigned int i) {
	if (i > 2)
	  throw Exception("popFirstIDOnCorner(1): ID out of bound");

	if (corners[i].size() == 0) {
	  return getVertexId(i);
	}
	else {
	  VertexID result;
	  result = corners[i].front();
	  corners[i].pop_front();
	  return result;
	}
      }

      /** get a new ID in the given corner, starting from the end of the list,
	  and pop-it from the structure. If the list is empty, the function throws
	  an exception */
      VertexID popIDOnCorner(unsigned int i) {
	if (i > 2)
	  throw Exception("popIDOnCorner(1): ID out of bound");

	if (corners[i].size() == 0) {
	  return getVertexId(i);
	}
	  // throw Exception("popIDOnCorner(1): empty list");
	else {
	  VertexID result;
	  result = corners[i].back();
	  corners[i].pop_back();
	  return result;
	}
      }

    };

    /** for each created point, this class describe the copies that will
	be added, and the corresponding ids */
    class CreatedPoint {
    private:
      unsigned int nbCopies;
      std::vector<VertexID> newIds;
    public:
      /** default constructor */
      CreatedPoint(unsigned int nb = 0) : nbCopies(nb) {
      }
      /** add a new copy */
      inline void addNewCopy() {
	++nbCopies;
      }

      /** add a new ID */
      inline void addNewID(VertexID p) {
	newIds.push_back(p);
      }
      /** accessor */
      inline unsigned int getNbCopies() const {
	return nbCopies;
      }

      /** accessor */
      const std::vector<VertexID> & getNewIds() const {
	return newIds;
      }
    };

    /** An edge splitter is a structure to describe the points that will be
	inserted on an edge (or in a corner of a triangle) */
    class EdgeSplitter {
    private:
      VertexID v1;
      VertexID v2;
      VertexID id1;
      VertexID id2;
    public:
      /** constructor */
      EdgeSplitter(VertexID vv1, VertexID vv2,
		   VertexID vid1, VertexID vid2) : v1(vv1), v2(vv2), id1(vid1), id2(vid2) {
      }

      /** copy constructor */
      EdgeSplitter(const EdgeSplitter & e) : v1(e.v1), v2(e.v2), id1(e.id1), id2(e.id2) {
      }

      /** accessor */
      inline VertexID getV1() const {
	return v1;
      }

      /** accessor */
      inline VertexID getV2() const {
	return v2;
      }

      /** given a vertex, it returns the other one */
      inline VertexID getOtherVertex(VertexID v) const {
	return v == v1 ? v2 : v1;
      }

      /** accessor */
      inline VertexID getId1() const {
	return id1;
      }

      /** accessor */
      inline VertexID getId2() const {
	return id2;
      }

      /** return the id in the side of the given vertex */
      inline VertexID getIdFromSide(VertexID v) const {
	if (v1 == v)
	  return id1;
	else {
	  assert(v == v2);
	  return id2;
	}
      }

      /** return the id in the other side of the given vertex */
      inline VertexID getIdFromOtherSide(VertexID v) const {
	if (v1 == v)
	  return id2;
	else {
	  assert(v == v2);
	  return id1;
	}
      }

      /** reorder the middle points using the convention: a-b-c-d with bc in the same order as ad */
      inline EdgeSplitter & reorder() {
	if (((v1 < v2) && (id1 > id2)) ||
	    ((v1 > v2) && (id1 < id2))) {
	  VertexID idtemp = id1;
	  id1 = id2;
	  id2 = idtemp;
	}
	return *this;
      }

      /** modify the current structure replacing the vertices contained
       in \p v1 by the one contained in \p v2. If \p full=true, it also replace the ids in the middle of the edgesplitter */
      EdgeSplitter & replacePoints(const VTriangle & t1, const VTriangle & t2, bool full = true);

      /** modify the crrent structure replacing \p iid1 by \p iid2, it also replace the ids in the middle of the edgesplitter */
      EdgeSplitter & replacePoints(VertexID iid1, VertexID iid2, bool full = true);

      /** assuming that the current object and \p es have a common vertex id (extremity), it
	  modify the other side of the current object using the vertex from \p es */
      EdgeSplitter & adjustSamePoints(const EdgeSplitter & es);

      /** \see EdgeSplitter & replacePoints(const VTriangle & t1, const VTriangle & t2) */
      inline EdgeSplitter getReplacePoints(const VTriangle & t1, const VTriangle & t2, bool full = true) const {
	return EdgeSplitter(*this).replacePoints(t1, t2, full);
      }

      /** return true if the current object has the given vertex as extremity */
      inline bool hasVertex(VertexID v) const {
	return (v1 == v) || (v2 == v);
      }

      /** an egality comparator that allows a complete reverse of the structure */
      inline bool operator==(const EdgeSplitter & es) const {
	return (((v1 == es.v1) && (v2 == es.v2) && (id1 == es.id1) && (id2 == es.id2)) ||
		((v2 == es.v1) && (v1 == es.v2) && (id2 == es.id1) && (id1 == es.id2)));
      }

    };


    /** \class MultiTriangle
	\brief A structure to describe triangle splitten by point on edge
	Several possible configurations:
	- the multitriangle is a leaf
	- the multitriangle is an alias (renaming vertices)
	- the multitriangle is splitten by an edge, but still sewed
	- the multitriangle is splitten by an edge, the new triangles are separated

	\internal
	Internal rules: if the multitriangle is a split, several rules are satisfied:
	- when introducing a cut a-b-c-d in an existing edge a-d (in the original mesh),
 	  the ordering of new points b-c is set using the a-d ordering (a>d <=> b>c).
	- when introducing a cut a-b-c-d in an existing corner a-c-d (in the original mesh),
	  the ordering of a-b-c-d is defined by the precomputed ordering arround c
	- the ordering of subtriangles is defined by the id of vertices in the triangle:
	  the first subtriangle is the one that corresponds to the largest vertex id (not
	  the shared vertex, but the two other one).
       */
    class MultiTriangle : public Triangle {
    private:
      MultiTriangle * mt1;
      MultiTriangle * mt2;
      PointOnEdge * splitter;
      EdgeSplitter * eSplitter;

      /** return true if the given PointOnEdge is contained in the first triangle */
      inline bool containedInFirst(const PointOnEdge & point, const EdgeSplitter & es) const {
	return containedInChildren(point, es, true);
      }

      /** return true if the given PointOnEdge is contained in the second triangle */
      inline bool containedInSecond(const PointOnEdge & point, const EdgeSplitter & es) const {
	return containedInChildren(point, es, false);
      }

      /** return true if the given PointOnEdge is contained in the choosen triangle (\p first) */
      bool containedInChildren(const PointOnEdge & point, const EdgeSplitter & es, bool first) const;

      void addPointOnEdge(const PointOnEdge & point, VertexID v1, VertexID v2, VertexID middle);

      void addPointOnEdgeFullSplit(const PointOnEdge & point, const EdgeSplitter & ids, const EdgeSplitter & idCorner);

      /** assuming that the current triangle is a split that has been computed using cut.first, ids.first,
       it applies a full split using cut.second, ids.second and adjust the structure... */
      void addPointOnEdgeFullSplitChildren(const std::pair<PointOnEdge, PointOnEdge> & cut,
					   const std::pair<EdgeSplitter, EdgeSplitter> & ids);

    public:
      /** constructor */
      MultiTriangle(const Triangle & t) : Triangle(t), mt1(NULL), mt2(NULL), splitter(NULL), eSplitter(NULL) {
      }

      /** constructor, with an alias */
      MultiTriangle(const Triangle & t, VertexID v1, VertexID v2, VertexID v3) : Triangle(t), mt1(NULL), mt2(NULL), splitter(NULL), eSplitter(NULL) {
	mt1 = new MultiTriangle(v1, v2, v3);
      }

      /** constructor */
      MultiTriangle(VertexID v1, VertexID v2, VertexID v3) : Triangle(v1, v2, v3), mt1(NULL), mt2(NULL), splitter(NULL), eSplitter(NULL) {
      }
      /** copy constructor */
      MultiTriangle(const MultiTriangle & t) : Triangle(t) {
	if (t.mt1 == NULL)
	  mt1 = NULL;
	else
	  mt1 = new MultiTriangle(*(t.mt1));
	if (t.mt2 == NULL)
	  mt2 = NULL;
	else
	  mt2 = new MultiTriangle(*(t.mt2));
	if (t.splitter == NULL)
	  splitter = NULL;
	else
	  splitter = new PointOnEdge(*(t.splitter));
	if (t.eSplitter == NULL)
	  eSplitter = NULL;
	else
	  eSplitter = new EdgeSplitter(*(t.eSplitter));
      }

      /** destructor */
      ~MultiTriangle() {
	if (mt1 != NULL)
	  delete mt1;
	if (mt2 != NULL)
	  delete mt2;
	if (splitter != NULL)
	  delete splitter;
	if (eSplitter != NULL)
	  delete eSplitter;
      }

      /** return true if the current triangle is spitten */
      inline bool isSplit() const {
	return splitter != NULL;
      }

      /** return true if the current triangle is a split, and if its two children are empty */
      inline bool isFinalSplit() const {
	if (splitter == NULL)
	  return false;
	assert(mt1 != NULL);
	assert(mt2 != NULL);
	return (*mt1).isFinal() && (*mt2).isFinal();
      }

      /** return true if the current triangle is a final one, i.e. not a splitted one,
	  and not an aliased */
      inline bool isFinal() const {
	return mt1 == NULL;
      }

      /** return true if the current triangle is an alias */
      inline bool isAlias() const {
	return (mt1 != NULL) && (mt2 == NULL);
      }

      /** return true if the current triangle is spitten, with disconnection (i.e. with an edge separating into two parts */
      inline bool isFullSplit() const {
	return isSplit() && ((*mt1).getNbSimilarPoints(*mt2) == 0);
      }

      /** assuming that the current object is an alias, it returns the alias of a given vertex */
      inline VertexID getAlias(VertexID v) const {
	assert(isAlias());
	if (getP1() == v)
	  return (*mt1).getP1();
	if (getP2() == v)
	  return (*mt1).getP2();
	if (getP3() == v)
	  return (*mt1).getP3();
	throw Exception("getAlias(1): cannot found the given point");
      }

      /** split the current multitriangle with the given new edge */
      void addCut(const std::pair<PointOnEdge, PointOnEdge> & cut,
		  const std::pair<EdgeSplitter, EdgeSplitter> & ids,
		  const Triangle & t,
		  const Mesh & mesh);

      /** add the small triangles in the given mesh */
      void addInMesh(Mesh & mesh);
    };

    std::map<TriangleID, TriangleWithCut> trianglesNotClean;

    std::map<PointOnEdge, CreatedPoint> newPoints;

    void addNonCleanTriangle(const Triangle & t, const PointOnEdge & pOe1, const PointOnEdge & pOe2);

    void addNewTriangles(Mesh & mesh, TriangleWithCut & triangle, const Mesh & oMesh);

    void addNewPoint(Mesh & mesh, const PointOnEdge & pOe, const Mesh & oMesh);

    std::pair<EdgeSplitter, EdgeSplitter> getSplitters(const std::pair<PointOnEdge, PointOnEdge> & cut,
						       const Triangle & t, const Mesh & mesh,
						       const MultiTriangle & mTriangle);

    EdgeSplitter getSplitter(const PointOnEdge & cut, const Triangle & t, const Mesh & mesh,
			     const MultiTriangle & mTriangle);

    static void sortBySide(Mesh & cMesh, MeshPart & cc, const std::vector<VertexID> & mpInsideCC, VertexID sideA[2], VertexID sideB[2]);


  public:


    enum CylinderParamChoice { CPCByLength, CPCByConformity, CPCByNormals, CPCByDistance, CPCByDistanceAndConformity };

    enum CylinderParamMethod { CPMFloater2D, CPMFloater3D, CPMFloaterIntegralLines, CPMFloater4IntegralLines };

  private:
    void addScalarFunction(MeshMap & mmap);

    void removeScalarFunction();

    void addNewSFValue(const PointOnEdge & pOe);

    void clearNewSFValues();

    void addNewScalarValues();

    void setNewMeshSV(Mesh & mesh);

    std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > getPathExtremaCutCylinders(Mesh & mesh, double maxratio,
													  const MTiling & mtiling,
													  const std::vector<std::pair<unsigned int, unsigned int> > & cylinders);

    std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > getPathExtremaCutCylinders(Mesh & mesh, double maxratio,
													  const MTiling & mtiling,
													  const std::vector<std::pair<unsigned int, unsigned int> > & cylinders,
													  const MeshMap & mmap);

    std::vector<PLPath> getPathsCutCylinders(const std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > & newLines,
					     const MeshMap & mmap);

    Mesh cutCylinders(Mesh & mesh, const MeshMap & mmap, double maxratio,
		      std::vector<std::pair<VertexID, VertexID> > & cylDesc);

    std::pair<VertexID, VertexID> getPointsOnQuadranglesFromCylinder(Mesh & cylinder,
								     const IDTranslator & translator);

    unsigned int getNbSectionsUpdateBorders(Mesh & mesh,
					    double maxratio,
					    const MTiling & mtiling,
					    const std::pair<unsigned int, unsigned int> & cylinder,
					    std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > & borders);

    static std::vector<PointOnEdge> getOnePerSide(const std::vector<PointOnEdge> & list,
						  Mesh & mesh, const MeshMap & mMap);

    static bool isUpSide(VertexID v, Mesh & mesh, const MeshMap & mMap);

    static PLPath & updatePLPathAfterCut(const Mesh & mesh, PLPath & path);


    static PointOnEdge findNextPointInDirection(const Mesh & mesh, const PointOnEdge & p1, const PointOnEdge & p2);

    static PointOnEdge findNextPointInDirection(const Mesh & mesh, const PointOnEdge & p1, const PointOnEdge & p2,
						const Triangle & triangle);

  public:

    /** default constructor */
    MeshPLCut() {
    }
    /**
       Given a mesh and a set of non-intersecting paths on the mesh (except first and last point),
       this function computes a new mesh, by cutting the original one along the paths.
    */
    Mesh cutMeshPaths(const Mesh & mesh, const std::vector<PLPath> & paths);

    /**
       Given a mesh and a non-intersecting path on the mesh (except first and last point),
       this function computes a new mesh, by cutting the original one along the path.
    */
    Mesh cutMesh(const Mesh & mesh, const PLPath & path);

    /**
       given a mesh and a set of point-on-edge on the mesh,
       this function computes a new mesh, adding the points on edge.
     */
    Mesh addPoints(const Mesh & mesh, const std::vector<PointOnEdge> & points);


    /**
       Given a mesh and a set of non-intersecting paths on the mesh (except first and last point),
       this function compute a new mesh, by cutting the original one along the paths. The paths
       cannot be loops. Update the given scalar function by adding values for the new points, using
       linear interpolation.  Warning: the reference to a mesh in the meshmap is not updated.
    */
    inline Mesh cutMeshUpdateScalarFunction(const Mesh & mesh, const std::vector<PLPath> & paths, MeshMap & sf) {
      addScalarFunction(sf);
      Mesh result = cutMeshPaths(mesh, paths);
      removeScalarFunction();
      return result;
    }

    /** an alias of \p cutMesh() with a vector of paths */
    inline Mesh cutMesh(const Mesh & mesh, const PLPath & path, MeshMap & sf) {
      std::vector<PLPath> paths;
      paths.push_back(path);
      return cutMeshUpdateScalarFunction(mesh, paths, sf);
    }

    /**
       Given a mesh and a set of non-intersecting pre-n-loops on the mesh,
       this function compute a new mesh, by cutting the original one along the paths.
       Update the given scalar function by adding values for the new points, using
       linear interpolation.  Warning: the reference to a mesh in the meshmap is not updated.
     */
    Mesh cutMeshPNLUpdateScalarFunction(const Mesh & mesh, const std::vector<PreNLoop> & pnl, MeshMap & mMap);


    /**
       For each of the cylindrical connected components of a given mesh with 2 multipoints on each boundary,
       it build a cut in quadrangles.
       \param flat Use (or not) a flat parameterization of the cylinder (as a CD)
       \param maxratio Maximum ratio between perimeter and length of the cylinder (that drive the cutting into subcylinders)
       if maxratio <= 0, no subcylinder is created
       \param epsilonSnap If value in [0, .5], the created points are snaped to the original vertices
     */
    Mesh cutMeshInQuadranglesFromCylindricalTiling(Mesh & mesh,
						   double maxratio = -1.,
						   double epsilonSnap = -1.,
						   const enum CylinderParamChoice & cpc = CPCByConformity,
						   const enum CylinderParamMethod & cpm = CPMFloater3D);

    /**
       For each of the cylindrical connected components of a given mesh with 2 multipoints on each boundary,
       it build a cut in quadrangles.
       Update the given scalar function by adding values for the new points, using
       linear interpolation. Warning: the reference to a mesh in the meshmap is not updated.
       \param flat Use (or not) a flat parameterization of the cylinder (as a CD)
       \param maxratio Maximum ratio between perimeter and length of the cylinder (that drive the cutting into subcylinders).
       if maxratio <= 0, no subcylinder is created
       \param epsilonSnap If value in [0, .5], the created points are snaped to the original vertices
     */
    inline Mesh cutMeshInQuadranglesFromCylindricalTilingUpdateScalarFunction(Mesh & mesh,
									      MeshMap & sf,
									      double maxratio = -1.,
									      double epsilonSnap = -1.,
									      const enum CylinderParamChoice & cpc = CPCByConformity,
									      const enum CylinderParamMethod & cpm = CPMFloater3D) {
      addScalarFunction(sf);
      Mesh result = cutMeshInQuadranglesFromCylindricalTiling(mesh, maxratio, epsilonSnap, cpc, cpm);
      removeScalarFunction();
      return result;
    }

    /**
       this method update a meshmap with an internal computation of the cutMeshInQuadranglesFromCylindricalTiling
       method, when using the CPMFloaterIntegralLines method
     */
    VertexID updateForCylindricalTiling(MeshMap & mmap, Mesh & mesh, bool side);

    /**
       For each of the connected component homoeomorphic to a disc and corresponding to a given extrema,
       split it into two connected components (that will be quads)
     */
    Mesh cutExtremaDiscs(Mesh & mesh, const std::vector<VertexID> & extrema);

    /**
       For each of the connected component homoeomorphic to a disc and corresponding to the extrema of the corresponding meshmap,
       split it into two connected components (that will be quads).
       Update the given scalar function by adding values for the new points, using
       linear interpolation.
       Warning: the reference to a mesh in the meshmap is not updated.
     */
    inline Mesh cutExtremaDiscsUpdateScalarFunction(Mesh & mesh, MeshMap & sf) {
      addScalarFunction(sf);
      Mesh result = cutExtremaDiscs(mesh, sf.getLocalExtrema(true));
      removeScalarFunction();
      return result;
    }

    /** subdivise the existing cylinders of the structure (that may be described by two quads)
	using \p maxratio as the maximal ratio between perimeter and length, and using
	a quad parameterization of each tile to compute the paths */
    Mesh cutCylindersByQuadParam(Mesh & mesh, double maxratio);

    /** subdivise the existing cylinders of the structure (that may be described by two quads)
	using \p maxratio as the maximal ratio between perimeter and length, and using
	a quad parameterization of each tile to compute the paths */
    inline Mesh cutCylindersByQuadParamUpdateScalarFunction(Mesh & mesh, double maxratio, MeshMap & sf) {
      addScalarFunction(sf);
      Mesh result = cutCylindersByQuadParam(mesh, maxratio);
      removeScalarFunction();
      return result;
    }

    /** subdivise the existing cylinders of the structure (that may be described by two quads)
	using \p maxratio as the maximal ratio between perimeter and length, and using
	a shortest path approach of each tile to compute the paths */
    Mesh cutCylindersByShPathParam(Mesh & mesh, double maxratio);

    /** subdivise the existing cylinders of the structure (that may be described by two quads)
	using \p maxratio as the maximal ratio between perimeter and length, and using
	a shortest path approach of each tile to compute the paths */
    inline Mesh cutCylindersByShPathParamUpdateScalarFunction(Mesh & mesh, double maxratio, MeshMap & sf) {
      addScalarFunction(sf);
      Mesh result = cutCylindersByShPathParam(mesh, maxratio);
      removeScalarFunction();
      return result;
    }

  };

  /**
     Stream operator
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const MeshPLCut::EdgeSplitter & es) {
    f << "EdgeSplitter( (" << es.getV1() << ") - " << es.getId1() << " - " << es.getId2() << " - (" << es.getV2() << ") )";
    return f;
  }


}


#endif
