/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMap.h"

#ifndef MESHMANIPULATOR
#define MESHMANIPULATOR

#include "Mesh.h"
#include "Length.h"
#include <set>
#include <queue>

namespace Taglut {

  /**
     Structure used to compare points using length computed by a dijkstra algorithm
  */
  struct lengthPathBP {
    /**
       Length array computed by a dijkstra algorithm
    */
    double * length;

    /**
       Mesh used for neighbours information
    */
    Mesh * mesh;

    /**
       binary comparaison
    */
    bool operator()(const VertexID p1, const VertexID p2) const {
      if (length != NULL)
	if (length[p1] == -1)
	  return false;
	else if (length[p2] == -1)
	  return true;
	else if (length[p1] == length[p2]) {
	  double minLNbP1 = std::numeric_limits<double>::max();
	  double minLNbP2 = std::numeric_limits<double>::max();
	  for(std::deque<VertexID>::const_iterator n = (*mesh).point(p1).getNeighbours().begin(); n != (*mesh).point(p1).getNeighbours().end(); ++n)
	    if (length[*n] < minLNbP1)
	      minLNbP1 = length[*n];
	  for(std::deque<VertexID>::const_iterator n = (*mesh).point(p2).getNeighbours().begin(); n != (*mesh).point(p2).getNeighbours().end(); ++n)
	    if (length[*n] < minLNbP2)
	      minLNbP2 = length[*n];
	  return minLNbP1 < minLNbP2;
	}
	else
	  return length[p1] < length[p2];
      else
	throw Exception(std::string("NULL value for length"));
    }
  };

  /**
     @class CCRuleTriangle
     Rule used to compute connected components
  */
  class CCRuleTriangle {
  public:
    /**
       Destructor
    */
    virtual ~CCRuleTriangle() {}

    /**
       Return true if a triangle is inside a connected component defined by flagValue
    */
    virtual bool isInside(const Mesh & mesh, int id1, int flagValue, int tFlagValue) const = 0;
  };

  /**
     @class CompDijkstra
     A comparator Dijkstra algorithm
  */
  class CompDijkstra {
  public:
    /**
       Comparator of the second element of the pair
    */
    bool operator() (const std::pair<VertexID, double> & b1,
		     const std::pair<VertexID, double> & b2) const {
      return b1.second > b2.second;
    }

  };

  /**
     @class CCRuleStripAndCrossRoads
     Rule used to compute connected components (with strips and crossroads)
  */
  class CCRuleStripAndCrossRoads : public CCRuleTriangle {
  public:
    CCRuleStripAndCrossRoads() {}

    /**
       Return true if a triangle is inside a connected component defined by flagValue
    */
    bool isInside(const Mesh & mesh, int id1, int flagValue, int tFlagValue) const {
      int f = mesh.triangle(id1).getFlag();
      return ((f != tFlagValue) && (f >= 6) && (f % 2 == 0)) || (f == flagValue);
    }
  };


  /**
     @class MeshManipulator
     A tool to manipulate mesh
  */
  class MeshManipulator {
  protected:
    /**
       (*this) supset
    */
    Mesh * mesh;

    /**
       Length values used by dijkstra algorithm
    */
    double * length;

    /**
       Predecessor values used by dijkstra algorithm
    */
    VertexID * pred;

    /**
       Length method used by dijkstra algorithm to compute shortest pathes
    */
    LengthEdge * lengthEdgeMethod;

    /**
       MeshMap used by the LengthEdge
    */
    MeshMap * mMapInternal;

    /**
       Size of the length and pred arrays
    */
    VertexID nbPoints;

    /**
       List of points with the same location
    */
    std::deque<VertexID> * sameLocation;

    /**
       Number of points in sameLocation deque
    */
    VertexID nbSameLocation;


    /**
       Init length and pred arrays
    */
    void initLengthAndPredList();

    /**
       Init length and pred arrays according to the number of points on the new mesh.
       Assums that the new number of points is highter than the old one.
    */
    bool updateLengthAndPredList();

    /**
       Return true if edge (idP1, idP2) is inside meshPart
       using triangle flags. Requirement: an edge e is inside meshpart if
       triangle containing e has flag set to tInside (i.e. if this triangle is
       inside mesh part)
    */
    bool isInsideEdgeTFlag(VertexID idP1, VertexID idP2, int tInside) const;

    /**
       return true if edge (idP1, idP2) is boundary of meshPart using triangle flages.
    */
    bool isBoundaryEdge(VertexID idP1, VertexID idP2, int tInside) const;

    /**
       return true if the two edges are in the same boundary CC assuming that two neighbour edges are
       connected if they are taken part in a triangle with flag = 1 (inside the allready seen part)
    */
    bool inSameBoundaryCC(VertexID id1, VertexID id2) const;

    /**
       Given a list of boundary points, find the first point according their neighbours,
       and set is pred[] value to break the loop along the boundary
    */
    void setFirstBoundaryPoint(const std::deque<VertexID> & bList);

    /**
       Compute connected component from triangle idP. All triangles
       in CC should have flag value equal iFlag. At end of computing, these
       triangle has flag value equal to fFlag.
       @return number of triangles inside CC
    */
    TriangleID computeCCUsingTriangles(VertexID idP, int iFlag, int fFlag);

    /**
       Compute connected component from triangle idP. All triangles
       in CC should have good flag (using CCRuleTriangle to define "good" status).
       At end of computing, these triangle has flag value equal to fFlag.
       @return number of triangles inside CC
    */
    TriangleID computeCCUsingTriangles(VertexID idP, const CCRuleTriangle & rule, int iFlag, int fFlag);


    /**
       Return a list containing idP's neighbours
    */
    std::deque<TriangleID> getTNeighbours(VertexID idP);


    /**
       For each point, compute the list of the points with the same location
    */
    void computeSameLocationList();


    /**
       Given a point id, compute euclidean length
    */
    void computeEuclideanLength(VertexID idCenter);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
    */
    void addPointSetFlag(MeshPart & meshPart, VertexID pt);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
    */
    VertexID computeNeighbourDistrib(VertexID nextPoint);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
       estimate the length of the path
    */
    double getEstimateLength(VertexID nextPoint, VertexID pred1, VertexID pred2) const;


    /**
       Tool for getShortestNonSeparatingCycleFromPoint
       Return a number of connected components of boundary points that are connected to
       an allready seen interior point. The flags are modified for those boundary points using a flag > 1
       corresponding to an id of connected component.
    */
    VertexID computeNeighbourDistrib(const std::deque<VertexID> & bList);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
    */
    std::pair<VertexID, VertexID> computePred(VertexID nextPoint);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
    */
    std::pair<VertexID, VertexID> computePred(const std::deque<VertexID> & pList);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
    */
    std::pair<VertexID, VertexID> computePredBoundary(VertexID nextPoint);

    /**
       Tool for getShortestNonSeparatingCycleFromPoint
       Search a boundary point in the neighbourhood of \p ptBd, with flag not equal to the given one.
       The starting and ending point of the search is \p firstPoint
    */
    VertexID findNextBPointFlag(VertexID ptBd, VertexID firstPoint, int flag = 1) const;


    /**
       Tool for getShortestNonSeparatingCycleFromPoint
    */
    std::pair<std::deque<TriangleID>, unsigned int> getJunctionsSetFlags(int flagValue, const std::deque<VertexID> & bList);

    /**
       Given a point, compute the smallest length of all their neighbours
    */
    double smallestLengthOfNeighbours(VertexID point) const;

    /**
       Given a list of points, compute the smallest length of all their neighbours
    */
    double smallestLengthOfNeighbours(const std::deque<VertexID> & points) const;

    /**
       Unstick a point from boundary
    */
    std::deque<VertexID> unstickPointFromBoundary(VertexID idPoint, VertexID idPointPred, VertexID idPointNext);

    /**
       Unstick a path from boundary, trying to preserve the mesh
    */
    std::deque<VertexID> unstickPathFromBoundaryPreserveMesh(const std::deque<VertexID> & path);

    /**
       Unstick a path from boundary, with a fast method
    */
    std::deque<VertexID> unstickPathFromBoundaryFast(const std::deque<VertexID> & path);

    /**
       Unstick a path from boundary, without using a global Dijkstra (modified fast method)
    */
    std::deque<VertexID> unstickPathFromBoundaryNoDijkstra(const std::deque<VertexID> & path);

  public:

    /**
       Methods available to merge points
    */
    enum UnstickMethod { UMNone, UMPreserveMesh, UMFast, UMNoDijkstra };

    /**
       Default constructor
    */
    MeshManipulator();

    /**
       Main constructor
    */
    MeshManipulator(Mesh & mesh);

    /**
       Default constructor with length operator
    */
    MeshManipulator(Mesh & mesh, const LengthEdge & lMethod);

    /**
       Copy constructor
    */
    MeshManipulator(const MeshManipulator & mManip);

    /**
       Copy constructor with other mesh
    */
    MeshManipulator(const MeshManipulator & mManip, Mesh & mesh);

    /**
       Destructor
    */
    ~MeshManipulator();

    /**
       Mesh accessor
    */
    const Mesh & getMesh() const;

#ifndef SWIG
    /**
       Copy operator
    */
    MeshManipulator & operator=(const MeshManipulator & mManip);
#endif

    /**
       Mesh accessor
    */
    Mesh & getMesh();

    /**
       Set length method (using clone method to manipulate real mesh)
    */
    void setLength(const LengthEdge & lMethod);

    /**
       get length associated to a given point
    */
    double getLength(VertexID idP) const;

    /**
       Return the median value of the length values
    */
    double getMedianLength() const;

    /**
       Return the mean value of the length values
    */
    double getMeanLength() const;

    /**
       Smooth the length values using the mean value of the neighbourhood of each point
    */
    void smoothLength();

    /**
       Return true if the given point is a local extrema according to the length function
    */
    bool isExtremaPoint(VertexID idP) const;

    /**
       get pred point associated to a given point
    */
    VertexID getPredPoint(VertexID idP) const;

    /**
       Return a list of points ordered by the surfacic distance to firstPoint
    */
    std::vector<VertexID> getPointsOrderedByLength(VertexID firstPoint);

    /**
       Return a list of points ordered by the surfacic distance to firstPoint, with length smaller than \p maxLength
    */
    std::vector<VertexID> getPointsOrderedByLength(VertexID firstPoint, double maxLength);

    /**
       Given a set of boundaries, compute shortest path between nearest pathes.
    */
    std::deque<VertexID> computeShortestPath(const std::deque<std::deque<VertexID> > & path);

    /** merge the wavefronts \p idPath1 and \p idPath2
	i.e. merge the dijkstra corresponding data, the corresponding sorted point's list, update the idInPath, and
	set dijkstra length = 0 for each point of the given \p path
    */
    void mergeWaveFronts(std::deque<std::pair<double *, VertexID *> > & dijkstraValues, std::deque<std::deque<VertexID> > & sortedPoints,
			 std::deque<VertexID> & idInPath,
			 int idPath1, int idPath2, const std::deque<VertexID> & path);

    /**
       Iterative method used by computeShortestPath(2) to build the tree.
       Structures are updated during the construction of the tree
       @param dijkstraValues Dijkstra values (length and pred) for each point
       @param sortedPoints List of id ordered according to each Dijkstra result
       @param idInPath Id of the next point on the wavefronts
       @param nbPathes Number of pathes that has to be computed
       @param resultTree The resulting list of pathes corresponding to the tree
       @param nbValidBorders number of valid borders
    */
    void computeShortestTree(std::deque<std::pair<double *, VertexID *> > & dijkstraValues, std::deque<std::deque<VertexID> > & sortedPoints,
			     std::deque<VertexID> & idInPath, unsigned int nbPathes, unsigned int nbValidBorders,
			     std::deque<std::deque<VertexID> > & resultTree);

    /**
       Given a set of pathes, compute shortest tree between pathes (maximum number of pathes defined by \p nbPathes).
    */
    std::deque<std::deque<VertexID> > computeShortestTreeFromPathes(const std::deque<std::deque<VertexID> > & pathes, unsigned int nbPathes = 0);

    /**
       Given a set of points, compute shortest tree between pathes (maximum number of pathes defined by \p nbPathes).
    */
    std::deque<std::deque<VertexID> > computeShortestTree(const std::deque<VertexID> & points, unsigned int nbPathes = 0);

    /**
       Given a set of boundaries, compute shortest path between nearest pathes (without using boundary points, except first and last).
    */
    std::deque<VertexID> computeShortestPathNoBoundary(VertexID idP1, VertexID idP2);


    /**
       Compute barycenter
    */
    Point3D computeBarycenter();

    /**
       Compute connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       point has flag value equal to fFlag.
       For connection, edge e is inside if a triangle containing e is
       inside (flag value equal to fInside)
       @return number of points inside CC  */
    VertexID computeCCUsingPoints(VertexID id, int iFlag, int fFlag, int tInside);

    /**
       Compute connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       point has flag value equal to fFlag.
       @return number of points inside CC  */
    VertexID computeCCUsingPointsSimple(VertexID id, int iFlag, int fFlag);

    /**
       Compute connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       point has flag value equal to fFlag. Using \p flags rather than the flags contains on the points
       @return number of points inside CC  */
    VertexID computeCCUsingPointsSimple(std::vector<int> & flags, VertexID id, int iFlag, int fFlag) const;

    /**
       Set flags of each point according to there connected component id (0 to nbCC - 1).
       @return number of connected components
    */
    VertexID computeFlagsUsingCC();

    /**
       Set flags of each point according to there connected component id (0 to nbCC - 1).
       @param flags The flags' list
       @return number of connected components
    */
    VertexID computeFlagsUsingCC(std::vector<int> & flags) const;

    /**
       Compute boundary connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       point has flag value equal to fFlag.
       For connection, edge e is inside if a triangle containing e is
       inside (flag value equal to fInside)
       @return number of points inside CC  */
    VertexID computeBoundaryCC(VertexID id, int iFlag, int fFlag, int tInside);

    /**
       Compute boundary connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       point has flag value equal to fFlag.
       For connection, edge e is inside if a triangle containing e is
       inside (flag value equal to fInside)
       @return boundary point list  */
    std::deque<VertexID> computeBoundaryCCList(VertexID id, int iFlag, int fFlag, int tInside);


    /**
       Compute part of a mesh boundary using tInside flag to define subset.
       @return number of points inside CC  */
    VertexID computeBoundaryCCSubset(VertexID id, int iFlag, int fFlag, int tInside);


    /**
       Compute boundary connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       point has flag value equal to fFlag.
       @return boundary point list  */
    std::deque<VertexID> computeBoundaryCCList(VertexID id, int iFlag, int fFlag);


    /**
       given two points, compute shortest path from first to second.
       Path is empty if idP1 and idP2 are not inside same connected component.
    */
    std::deque<VertexID> computeShortestPath(VertexID idP1, VertexID idP2);


    /**
       given two set of points, compute shortest path from first to second.
       Path is empty if idP1 and idP2 are not inside same connected component.
    */
    std::deque<VertexID> computeShortestPath(const std::deque<VertexID> & idPs1,
					     const std::deque<VertexID> & idPs2);

    /**
       given two points, compute shortest path from first to second, using iFlag as "inside" flag.
       Path is empty if idP1 and idP2 are not inside same connected component.
    */
    std::deque<VertexID> computeShortestPathInCC(VertexID idP1, VertexID idP2, int iFlag);


    /**
       given two points, compute shortest path from first to second.
       Length is not computed for the whole mesh, but only for points after the given bound
       Path is empty if idP1 and idP2 are not inside same connected component.
    */
    std::deque<VertexID> computeShortestPathUpdateLength(VertexID idP1, VertexID idP2, double bound);

    /**
       for each of the points given in parameter, compute the shortest path
       between \p idP and this point
     */
    std::vector<std::vector<VertexID> > computeShortestPaths(VertexID idP, const std::vector<VertexID> & points);

    /**
       Get shortest non trivial cycle starting from the given point
       @param basePoint the original base point of the cycle
       @return the constructed path
    */
    std::deque<VertexID> getShortestNonTrivialCycleFromPoint(VertexID basePoint);


    /**
       Compute shortest non separating cycle.
       @param basePoint the original base point of the cycle
       @param maxLength the maximum length value of the search path
       @param minLength the minimum length value of the search path
       @param merge merge the boundary
       @return target point and the two pred points
    */
    std::deque<VertexID> getShortestNonSeparatingCycleFromPoint(VertexID basePoint, double maxLength = -1.0, double minLength = 0.0, bool merge = true);

    /**
       Compute target of the shortest non separating cycle
       @param basePoint the original base point of the cycle
       @param maxLength the maximum length value of the search path
       @param minLength the minimum length value of the search path
       @param merge merge the boundary
       @return target point and the two pred points
    */
    std::pair<VertexID, std::pair<VertexID, VertexID> > getTargetShortestNonSeparatingCycleFromPoint(VertexID basePoint, double maxLength, double minLength = 0.0, bool merge = true);


    /**
       Given a point id, compute distance from this point to all others
    */
    void computeDistances(VertexID idCenter, std::deque<double> & distances);

    /**
       Given a point id, compute distance from this point to all others. Points with same location
       are considered as same points
    */
    void computeDistancesStickCuttings(VertexID idCenter, std::deque<double> & distances);

    /**
       Find nearest point of center
    */
    VertexID findNearestPoint(const Point3D & center) const;

    /**
       Given a point list, return point nearest to the boundary
    */
    VertexID getNearestToBoundary(std::deque<VertexID> points);


    /**
       find other point with same location as p. If not found, throws exception
    */
    VertexID findPointSameLocation(VertexID p) const;

    /**
       For each point, compute list of the points with the same location as p.
       Resulted list contains p.
    */
    std::deque<VertexID> getPointsSameLocation(VertexID p) const;


    /**
       Compute the geodesic distance between p1 and p2 using Dijkstra algorithm
    */
    double getGeodesicDistance(VertexID p1, VertexID p2);

    /**
       Compute the geodesic distance between two sets of points using Dijkstra algorithm
    */
    double getGeodesicDistance(const std::vector<VertexID> & ps1, const std::vector<VertexID> & ps2);

    /**
       Compute teh Hausdorff distance on the surface between two set of points, using Dijkstra algorithm as
       distance between points
    */
    double getHausdorffDistance(const std::vector<VertexID> & ps1, const std::vector<VertexID> & ps2);

    /******************** D I J K S T R A   T O O L S *********************/

    /**
       Given a point id, compute length and ancestors using dijkstra algorithm.
       All mesh is used.
    */
    void computeDijkstra(VertexID center);

    /**
       Given a point id, first compute connected component of center point (using
       fInside flag at input and fInsideCC at output), then compute
       length and ancestors using dijkstra algorithm.
       @param center first point
       @param fInside Flag value for points inside
       @param fInsideCC Flag value for points inside selection and in same connected component
       that center.
    */
    void computeDijkstra(VertexID center, int fInside, int fInsideCC);

    /**
       Given a point id, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag make an unique connected component.
       @param center first point
       @param fInside Flag value for points inside
    */
    void computeDijkstra(VertexID center, int fInside);

    /**
       Compute Dijkstra from a list of given points described by an iterator accesssor
    */
    template <class RandomAccessIterator>
    inline void computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside, int fInsideCC);

    /**
       Compute Dijkstra from a list of given points described by an iterator accesssor
    */
    template <class RandomAccessIterator>
    inline void computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside);

    /**
       Compute Dijkstra from a list of given points described by an iterator accesssor
    */
    template <class RandomAccessIterator>
    inline void computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside, const LengthEdge & lEdge, double bound = -1);

    /**
       Compute Dijkstra from a list of given points described by an iterator accesssor
    */
    template <class RandomAccessIterator>
    inline void computeDijkstraTarget(RandomAccessIterator first, RandomAccessIterator last, int fInside, const LengthEdge & lEdge, VertexID idTarget);

    /**
       Compute Dijkstra from a list of given points described by an iterator accesssor
    */
    template <class RandomAccessIterator>
    inline void computeDijkstraTarget(RandomAccessIterator first, RandomAccessIterator last, int fInside, const std::deque<VertexID> & targets);


    /**
       Given a set of point, first compute connected components of these point (using
       fInside flag at input and fInsideCC at output), then compute
       length and ancestors using dijkstra algorithm.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param fInsideCC Flag value for points inside selection and in same connected component
       that center.
    */
    inline void computeDijkstra(const std::deque<VertexID> & sPoints, int fInside, int fInsideCC) {
      computeDijkstra(sPoints.begin(), sPoints.end(), fInside, fInsideCC);
    }


    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
    */
    inline void computeDijkstra(const std::deque<VertexID> & sPoints, int fInside) {
      computeDijkstra(sPoints.begin(), sPoints.end(), fInside);
    }


    /**
       Given a point id, first compute connected component of center point (using
       fInside flag at input and fInsideCC at output), then compute
       length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param center base point
       @param lEdge rule to compute edge length
    */
    void computeDijkstra(VertexID center, const LengthEdge & lEdge);


    /**
       Given a point id, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param center base point
       @param fInside Flag value for points inside
       @param idTarget Target id (not computed for whole mesh if target != mesh.getNbPoints)
    */
    void computeDijkstraTarget(VertexID center, int fInside, VertexID idTarget);

    /**
       Given a point id, update length and ancestors for points after "bound", using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param center base point
       @param fInside Flag value for points inside
       @param idTarget Target id (not computed for whole mesh if target != mesh.getNbPoints)
       @param bound Bound of the length
    */
    void computeDijkstraTargetUpdateLength(VertexID center, int fInside, VertexID idTarget, double bound);


    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param lEdge rule to compute edge length
       @param bound Bound value (not computed for whole mesh if bound > 0)
    */
    inline void computeDijkstra(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge, double bound = -1) {
      computeDijkstra(sPoints.begin(), sPoints.end(), fInside, lEdge, bound);
    }

    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param lEdge rule to compute edge length
       @param idTarget Target id (not computed for whole mesh if target != mesh.getNbPoints)
    */
    inline void computeDijkstraTarget(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge, VertexID idTarget) {
      return computeDijkstraTarget(sPoints.begin(), sPoints.end(), fInside, lEdge, idTarget);
    }

    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param targets Target ids
    */
    inline void computeDijkstraTarget(const std::deque<VertexID> & sPoints, int fInside, const std::deque<VertexID> & targets) {
      return computeDijkstraTarget(sPoints.begin(), sPoints.end(), fInside, targets);
    }

    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param lEdge rule to compute edge length
       @param bound Bound value (not computed for whole mesh if bound > 0)
       @param targets Target ids
    */
    inline void computeDijkstra(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge, double bound, const std::deque<VertexID> & targets) {
      return computeDijkstra(sPoints.begin(), sPoints.end(), fInside, lEdge, bound, targets);
    }

    /**
       Compute Dijkstra from a list of given points described by an iterator accesssor
    */
    template <class RandomAccessIterator>
    void computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside, const LengthEdge & lEdge, double bound, const std::deque<VertexID> & targets);


    /**
       Given a set of point, update length and ancestors for points after "initBound", using dijkstra algorithm.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param lEdge rule to compute edge length
       @param initBound Bound value (only points after this bound are updated)
       @param targetBound Bound value (not computed for whole mesh if bound > 0)
       @param idTarget Target id (not computed for whole mesh if target != mesh.getNbPoints)
    */
    void computeDijkstraUpdateLength(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge, double initBound, double targetBound, VertexID idTarget);


    /**
       compute length and ancestors using dijkstra algorithm.
       @param center first point
       @param fInside Flag value for points inside
       @param fTInside Flag value for triangles inside
       that center.
    */
    void computeTDijkstra(VertexID center, int fInside, int fTInside);

    /**
       compute length and ancestors using dijkstra algorithm.
       Stop when join the target point
       @param center first point
       @param target target point
       @param fInside Flag value for points inside
       @param fTInside Flag value for triangles inside
       that center.
    */
    void computeTDijkstraTarget(VertexID center, VertexID target, int fInside, int fTInside);

    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param fTInside Flag value for triangles inside
       @param lEdge rule to compute edge length
    */
    void computeTDijkstra(const std::deque<VertexID> & sPoints, int fInside, int fTInside, const LengthEdge & lEdge);

    /**
       Given a set of point, compute length and ancestors using dijkstra
       algorithm. Stop when join one of the tPoints
       @param sPoints start points
       @param tPoints target point
       @param fInside Flag value for points inside
       @param fTInside Flag value for triangles inside
       @param lEdge rule to compute edge length
       @param bound Bound value (not computed for whole mesh if bound > 0)
    */
    void computeTDijkstraTarget(const std::deque<VertexID> & sPoints,
				const std::deque<VertexID> & tPoints,
				int fInside, int fTInside, const LengthEdge & lEdge, double bound = -1);

    /**
       Given a point id, compute length and ancestors using dijkstra algorithm.
       Each boundary is seen as a single point (i.e. edges in boundaries has length = 0).
       All mesh is used.
    */
    void computeDijkstraMergeBoundaries(VertexID center);

    /**
       Compute dijsktra method with bound value (not for whole mesh)
    */
    void computeDijkstraBound(VertexID center, const LengthEdge & lEdge, double bound);

    /**
       Compute dijsktra method with bound value (not for whole mesh)
    */
    void computeDijkstraBound(VertexID center, double bound);

    /**
       Given a point id, compute length and ancestors using dijkstra algorithm. Points
       with the same location are considered as the same point.
       All mesh is used.
    */
    void computeDijkstraStickCuttings(VertexID center);


    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm. Points
       with the same location are considered as the same point.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
    */
    void computeDijkstraStickCuttings(const std::deque<VertexID> & sPoints, int fInside);

    /**
       Given a set of point, compute length and ancestors using dijkstra algorithm. Points
       with the same location are considered as the same point.
       Requirement: points with fInside flag are connected.
       @param sPoints start points
       @param fInside Flag value for points inside
       @param lEdge rule to compute edge length
       @param bound Bound value (not computed for whole mesh if bound > 0)
    */
    void computeDijkstraStickCuttings(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge, double bound);

    /**
       Given a path, split it into small pathes without boundary edges inside
    */
    std::deque<std::deque<VertexID> > splitPath(const std::deque<VertexID> & path);

    /**
       If path is sticked to the boundary, unstick it adding points
       Flags of points are used. 0 means available point, 1 means forbidden point.
       @param path The path to unstick
       @param method Unsticking method
       @return The path added after unstick process
    */
    std::deque<VertexID> unstickPathFromBoundary(const std::deque<VertexID> & path, enum UnstickMethod method = UMPreserveMesh);

    /**
       Given a base point, a target point and two predecessors for the target point,
       build the corresponding cycle.
    */
    std::deque<VertexID> buildCycle(VertexID basePoint, VertexID nextPoint, VertexID pred1, VertexID pred2) const;


    /**
       Compute boundaries of the manipulated mesh
     */
    std::vector<std::vector<VertexID> > getBoundaries();


    /**
       Return a list of the point in the same boundary as the given point. List is not ordered
     */
    std::vector<VertexID> getBoundaryFromBPoint(VertexID bPoint);

    /**
       Compute paths of the cut locus of point
    */
    std::vector<std::vector<VertexID> > getCutLocusPaths(VertexID point);

    /**
       given a point, return the point on the surface which corresponding geodesic distance
       is the highest.
     */
    VertexID getOppositePoint(VertexID p);

    /**
       return a triangle in the same CC as t (using flag as value for triangles), in the boundary of the
       CC.
       \param idP the initial triangle
       \param flag the inside flag
       \param flagt a temporary value for flags
     */
    const Triangle & findTriangleOnBorderSameCC(TriangleID idP, int flag, int flagt);

    /**
       compute a connected component of triangles using edges as adjacency link.
       return the number of corresponding triangles
    */
    TriangleID computeTCC(TriangleID idP, int iFlag, int fFlag);


    friend class WaveFront;
  };

  template <class RandomAccessIterator>
  void MeshManipulator::computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside, const LengthEdge & lEdge, double bound, const std::deque<VertexID> & targets) {

    initLengthAndPredList();
    std::priority_queue<std::pair<VertexID, double>, std::vector<std::pair<VertexID, double> >, CompDijkstra> open;

    // init center length and open set
    for(RandomAccessIterator i = first; i != last; ++i) {
      length[*i] = 0.;
      open.push(std::pair<VertexID, double>(*i, 0.));
    }

    while(open.size() != 0) {
      VertexID idNearest = open.top().first;
      double v = open.top().second;
      open.pop();

      if (length[idNearest] == v) {
	if ((bound != -1.0) && (length[idNearest] != 0.) && (length[pred[idNearest]] > bound))
	  break;
	if (targets.size() != 0)
	  for(std::deque<VertexID>::const_iterator idTarget = targets.begin();
	      idTarget != targets.end(); ++idTarget)
	    if (*idTarget == idNearest)
	      break;

	// then remove it from open set
	if (length[idNearest] < 0)
	  throw Exception("computeDijkstra: negative length.");

	// for each neighbour inside CC
	std::deque<VertexID> &ns = (*mesh).point(idNearest).getNeighbours();
	for(std::deque<VertexID>::const_iterator n = ns.begin(); n != ns.end(); ++n)
	  if ((*mesh).point(*n).getFlag() == fInside) {
	    double l = length[idNearest] + lEdge(idNearest, *n);
	    if ((length[*n] == -1.0) || (length[*n] > l)) {
	      length[*n] = l;
	      pred[*n] = idNearest;
	      open.push(std::pair<VertexID, double>(*n, l));
	    }
	  }
      }
    }

  }

  template <class RandomAccessIterator>
  inline void MeshManipulator::computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside, int fInsideCC) {

    // compute connected component
    (*mesh).setTriangleFlag(0);
    for(std::deque<VertexID>::const_iterator i = first; i != last; ++i)
      computeCCUsingPoints(*i, fInside, fInsideCC, 0);

    computeDijkstra(first, last, fInsideCC);

  }

  template <class RandomAccessIterator>
  inline void MeshManipulator::computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside) {
    computeDijkstra(first, last, fInside, *lengthEdgeMethod);
  }

  template <class RandomAccessIterator>
  inline void MeshManipulator::computeDijkstra(RandomAccessIterator first, RandomAccessIterator last, int fInside, const LengthEdge & lEdge, double bound) {
    computeDijkstra(first, last, fInside, lEdge, bound, std::deque<VertexID>());
  }

  template <class RandomAccessIterator>
  inline void MeshManipulator::computeDijkstraTarget(RandomAccessIterator first, RandomAccessIterator last, int fInside, const LengthEdge & lEdge, VertexID idTarget) {
    std::deque<VertexID> tgs;
    tgs.push_front(idTarget);

    computeDijkstra(first, last, fInside, lEdge, -1.0, tgs);
  }

  template <class RandomAccessIterator>
  inline void MeshManipulator::computeDijkstraTarget(RandomAccessIterator first, RandomAccessIterator last, int fInside, const std::deque<VertexID> & targets) {
    computeDijkstra(first, last, fInside, *lengthEdgeMethod, -1.0, targets);
  }

}



#endif
