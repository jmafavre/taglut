/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_COMPARATOR
#define MESH_COMPARATOR

#include "Mesh.h"
#include "MeshMatcher.h"

namespace Taglut {

  /**
     @class MeshComparator

     @author Jean-Marie Favreau
     @brief Comparison between meshes (square distances & co)
  */
  class MeshComparator {
  private:
    /**
       Original mesh
    */
    const Mesh & mesh1;

    /**
       Reference mesh
    */
    const Mesh & mesh2;

    /**
       Matching tool for points' correspondance
    */
    MeshMatcher mMatcher;


  public:
    /**
       Default constructor
    */
    MeshComparator(const Mesh & mesh1, const Mesh & mesh2);

    /**
       get the sum of the square distances between the two compared meshes (3d locations)
    */
    double sumSquareDistance3D() const;

    /**
       get the mean of the square distances between the two compared meshes (3d locations)
    */
    double meanSquareDistance3D() const;

    /**
       get the max of the square distances between the two compared meshes (3d locations)
    */
    double maxSquareDistance3D() const;

    /**
       apply the method described by the string given in parametter
    */
    double useMethod(const std::string & method) const;

    /**
       return true if the given method is part of the available methods
    */
    static bool isKnownMethod(const std::string & method);

  };

}

#endif
