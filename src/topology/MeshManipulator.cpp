/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <list>
#include <iterator>
#include <algorithm>

#include "MeshMap.h"
#include "MeshManipulator.h"
#include "MeshPart.h"
#include "SpaceSpliter.h"
#include "WaveFront.h"

// used during debug
#include "MeshMap.h"
#include "Display3D.h"
#include "WeightedEdges.h"


using namespace Taglut;

MeshManipulator::MeshManipulator(Mesh & mesh_t) : mesh(&mesh_t) {
  length = NULL;
  mMapInternal = NULL;
  pred = NULL;
  sameLocation = NULL;
  nbPoints = 0;
  lengthEdgeMethod = new LengthEdgeEuclidean(mesh_t);
}

MeshManipulator::MeshManipulator() {
  mesh = NULL;
  length = NULL;
  mMapInternal = NULL;
  pred = NULL;
  sameLocation = NULL;
  nbPoints = 0;
  lengthEdgeMethod = NULL;
}

MeshManipulator::MeshManipulator(Mesh & mesh_t, const LengthEdge & lMethod) : mesh(&mesh_t) {
  length = NULL;
  mMapInternal = NULL;
  pred = NULL;
  sameLocation = NULL;
  nbPoints = 0;

  if (lMethod.needMeshMap()) {
    mMapInternal = new MeshMap(*(lMethod.getMeshMap()), mesh_t);
    lengthEdgeMethod = lMethod.cloneMeshMap(*mMapInternal);
  }
  else
    lengthEdgeMethod = lMethod.clone(mesh_t);
}

MeshManipulator::MeshManipulator(const MeshManipulator & mManip) : mesh(mManip.mesh) {
  length = NULL;
  mMapInternal = NULL;
  pred = NULL;
  nbPoints = 0;
  sameLocation = NULL;

  if ((*mManip.lengthEdgeMethod).needMeshMap()) {
    mMapInternal = new MeshMap(*((*mManip.lengthEdgeMethod).getMeshMap()), *mesh);
    lengthEdgeMethod = (*mManip.lengthEdgeMethod).cloneMeshMap(*mMapInternal);
  }
  else
    lengthEdgeMethod = (*mManip.lengthEdgeMethod).clone(*mesh);
}

MeshManipulator::MeshManipulator(const MeshManipulator & mManip, Mesh & mesh_t) : mesh(&mesh_t) {
  assert(mesh_t.getNbPoints() == mManip.getMesh().getNbPoints());
  assert(mesh_t.getNbTriangles() == mManip.getMesh().getNbTriangles());
  sameLocation = NULL;
  length = NULL;
  mMapInternal = NULL;
  pred = NULL;
  nbPoints = 0;

  if ((*mManip.lengthEdgeMethod).needMeshMap()) {
    mMapInternal = new MeshMap(*((*mManip.lengthEdgeMethod).getMeshMap()), mesh_t);
    lengthEdgeMethod = (*mManip.lengthEdgeMethod).cloneMeshMap(*mMapInternal);
  }
  else
    lengthEdgeMethod = (*mManip.lengthEdgeMethod).clone(mesh_t);
}

MeshManipulator::~MeshManipulator() {
  if (length != NULL)
    delete[] length;
  if (pred != NULL)
    delete[] pred;
  if (sameLocation != NULL)
    delete[] sameLocation;
  if (lengthEdgeMethod != NULL)
    delete lengthEdgeMethod;
  if (mMapInternal != NULL)
    delete mMapInternal;
}

MeshManipulator & MeshManipulator::operator=(const MeshManipulator & mManip) {
  if (&mManip == this)
    return *this;
  mesh = mManip.mesh;
  length = NULL;
  pred = NULL;
  nbPoints = 0;
  sameLocation = NULL;
  lengthEdgeMethod = (*mManip.lengthEdgeMethod).clone();

  return *this;
}

const Mesh & MeshManipulator::getMesh() const {
  return *mesh;
}

Mesh & MeshManipulator::getMesh() {
  return (*mesh);
}

void MeshManipulator::setLength(const LengthEdge & lMethod) {
  if (lengthEdgeMethod != NULL)
    delete lengthEdgeMethod;

  if (lMethod.needMeshMap()) {
    if (mMapInternal != NULL)
      delete mMapInternal;
    mMapInternal = new MeshMap(*(lMethod.getMeshMap()), *mesh);
    lengthEdgeMethod = lMethod.cloneMeshMap(*mMapInternal);
  }
  else {
    if (mMapInternal != NULL) {
      delete mMapInternal;
      mMapInternal = NULL;
    }
    lengthEdgeMethod = lMethod.clone(*mesh);
  }
}

VertexID MeshManipulator::getPredPoint(VertexID idP) const {
  if ((pred == NULL) ||(idP > (*mesh).getNbPoints()))
    throw Exception(std::string("Point id out of bound"));
  return pred[idP];
}

double MeshManipulator::getLength(VertexID idP) const {
  if ((length == NULL) ||(idP > (*mesh).getNbPoints()))
    throw Exception(std::string("Point id out of bound"));
  return length[idP];
}


bool MeshManipulator::isExtremaPoint(VertexID idP) const {
  if ((length == NULL) ||(idP > (*mesh).getNbPoints()))
    throw Exception(std::string("Point id out of bound"));

  const Point3D & p = (*mesh).point(idP);
  if (p.getNbNeighbours() == 0)
    return true;

  bool max;
  bool flat = true;
  for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin(); nb != p.getNeighbours().end(); ++nb)
    if (length[idP] != length[p.getNeighbours().front()]) {
      max = length[idP] > length[p.getNeighbours().front()];
      flat = false;
      break;
    }
  if (flat) // a flat part
    return false;

  for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin(); nb != p.getNeighbours().end(); ++nb)
    if ((max && (length[idP] < length[*nb])) || (!max && (length[idP] > length[*nb])))
      return false;

  return true;
}

double MeshManipulator::getMedianLength() const {
  if (length == NULL)
    throw Exception(std::string("Length has not be computed."));

  std::vector<double> lLength;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i)
    lLength.push_back(length[i]);
  std::sort(lLength.begin(), lLength.end());

  return lLength[lLength.size() / 2];
}

double MeshManipulator::getMeanLength() const {
  if (length == NULL)
    throw Exception(std::string("Length has not be computed."));
  double result = 0;
  VertexID nb = (*mesh).getNbPoints();

  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i)
    result += length[i] / nb;

  return result;
}

void MeshManipulator::smoothLength() {
  if (length == NULL)
    throw Exception(std::string("Length has not be computed."));

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i) {
    double & l = length[(*i).getId()];
    for(std::deque<VertexID>::const_iterator nb = (*i).getNeighbours().begin(); nb != (*i).getNeighbours().end(); ++nb)
      l += length[*nb];
    l /= ((*i).getNbNeighbours() + 1);
  }
}


void MeshManipulator::setFirstBoundaryPoint(const std::deque<VertexID> & bList) {
  double l = -1;
  VertexID bPoint = (*mesh).getNbPoints();
  VertexID bPointPred = (*mesh).getNbPoints();

  for(std::deque<VertexID>::const_iterator p = bList.begin(); p != bList.end(); ++p)
    for( std::deque<VertexID>::const_iterator nb = (*mesh).point(*p).getNeighbours().begin();
	 nb != (*mesh).point(*p).getNeighbours().end(); ++nb)
      if (!(*mesh).isBoundaryEdge(*p, *nb)) {
	double newLength = length[*nb] + (*lengthEdgeMethod)(*p, *nb);
	if ((l == -1) || (l > newLength)) {
	  l = newLength;
	  bPoint = *p;
	  bPointPred = *nb;
	}
      }

  assert(bPoint != (*mesh).getNbPoints());
  pred[bPoint] = bPointPred;

}

double MeshManipulator::getEstimateLength(VertexID nextPoint, VertexID pred1, VertexID pred2) const {
  double result = 0.0;

  if ((pred1 == (*mesh).getNbPoints()) || (pred2 == (*mesh).getNbPoints()))
    return result;

  if ((*mesh).point(nextPoint).hasNeighbour(pred1))
    result = length[pred1] + (*lengthEdgeMethod)(nextPoint, pred1);
  else {
    result = length[pred1];
    double lengthToBoundary = 0.0;
    for(std::deque<VertexID>::const_iterator i = (*mesh).point(pred1).getNeighbours().begin(); i != (*mesh).point(pred1).getNeighbours().end(); ++i)
      if (lengthToBoundary < (*lengthEdgeMethod)(pred1, *i))
	lengthToBoundary = (*lengthEdgeMethod)(pred1, *i);
    result += lengthToBoundary;
  }

  if ((*mesh).point(nextPoint).hasNeighbour(pred2))
    result += length[pred2] + (*lengthEdgeMethod)(nextPoint, pred2);
  else {
    result += length[pred2];
    double lengthToBoundary = 0.0;
    for(std::deque<VertexID>::const_iterator i = (*mesh).point(pred2).getNeighbours().begin(); i != (*mesh).point(pred2).getNeighbours().end(); ++i)
      if (lengthToBoundary < (*lengthEdgeMethod)(pred2, *i))
	lengthToBoundary = (*lengthEdgeMethod)(pred2, *i);
    result += lengthToBoundary;
  }

  return result;
}


std::pair<VertexID, std::pair<VertexID, VertexID> > MeshManipulator::getTargetShortestNonSeparatingCycleFromPoint(VertexID basePoint, double maxLength, double minLength, bool merge) {

  assert(!(*mesh).point(basePoint).getIsBoundary());
  VertexID pred1, pred2;
  bool found = false; // TODO: unused found?
  bool linkPoint = false;
  VertexID nextPoint;
  VertexID idNextPoint = 0;
  lengthPathBP lpath;

  int * sFlags = new int[(*mesh).getNbPoints()];

  MeshPart meshPart(*mesh);
  LengthEdgeMergeBoundary lb(*lengthEdgeMethod);

  // max radius is defined by maxLength
  double maxRadius = maxLength / 2;


  // first compute dijkstra with edges length in boundaries = 0;
  if (maxLength > 0)
    if (merge)
      computeDijkstraBound(basePoint, LengthEdgeMergeBoundary(*lengthEdgeMethod), maxRadius);
    else
      computeDijkstraBound(basePoint, *lengthEdgeMethod, maxRadius);
  else
    if (merge)
      computeDijkstra(basePoint, LengthEdgeMergeBoundary(*lengthEdgeMethod));
    else
      computeDijkstra(basePoint, *lengthEdgeMethod);


  lpath.length = length;
  lpath.mesh = mesh;

  // sort points using dijkstra length
  std::list<VertexID> sList;
  std::list<VertexID>::const_iterator itL;
  VertexID sListSize = 0;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    if (length[i] != -1) {
      sList.push_back(i);
      ++sListSize;
    }
  sList.sort(lpath);


  // flag initialisation
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  // find shortest cycle
  itL = sList.begin();
  assert(*itL == basePoint);

  while(true) {
    std::deque<VertexID> bList;

    if (idNextPoint >= sListSize) {
      delete [] sFlags;
      return std::pair<VertexID, std::pair<VertexID, VertexID> >((*mesh).getNbPoints(), std::pair<VertexID, VertexID>());
    }

    assert(itL != sList.end());
    nextPoint = *itL;
    if ((*mesh).point(nextPoint).getFlag() == 0) {
      // add next point
      // if nextPoint is a boundary, add all other boundary points
      if ((*mesh).point(nextPoint).getIsBoundary() && merge) {

	VertexID ptBd = nextPoint;
	VertexID ptBdPred = nextPoint;
	do {
#ifndef NDEBUG
	  VertexID last = ptBd;
#endif
	  bList.push_front(ptBd);

	  if (ptBd != nextPoint) {
	    pred[ptBd] = ptBdPred;
	    length[ptBd] = length[ptBdPred];
	    assert((*mesh).point(ptBd).hasNeighbour(ptBdPred));
	  }
	  ptBdPred = ptBd;

	  // add it to the meshpart
	  addPointSetFlag(meshPart, ptBd);

	  // find next boundary point
	  ptBd = findNextBPointFlag(ptBd, nextPoint);
	  assert(lb(last, ptBd) == 0);
	  assert(lb(ptBd, last) == 0);

	} while(nextPoint != ptBd); // for each point of the boundary

	pred[nextPoint] = ptBdPred; // the pred list is now defining a loop arround the boundary
	// Then set the pred to the first boundary point (according to its neighbours)
	setFirstBoundaryPoint(bList);

	linkPoint = computeNeighbourDistrib(bList) > 1;
      }
      else { // else: it is not a boundary point
	addPointSetFlag(meshPart, nextPoint);


	// watch neighbours
	linkPoint = (computeNeighbourDistrib(nextPoint) > 1);

	assert(((*mesh).point((*mesh).point(nextPoint).getNeighbours().front()).getFlag() == (*mesh).point((*mesh).point(nextPoint).getNeighbours().back()).getFlag()) ||
	       ((*mesh).point((*mesh).point(nextPoint).getNeighbours().front()).getFlag() == 0) || ((*mesh).point((*mesh).point(nextPoint).getNeighbours().back()).getFlag() == 0));

      }

      // more than one neighbourhood: it is a link point. Find predecessors in each neighbourhood
      if (linkPoint) {
	// first look for the preds

	if ((*mesh).point(nextPoint).getIsBoundary() && merge) {
	  std::pair<VertexID, VertexID> preds = computePredBoundary(nextPoint);
	  pred1 = preds.first;
	  pred2 = preds.second;
	}
	else {
	  std::pair<VertexID, VertexID> preds = computePred(nextPoint);
	  pred1 = preds.first;
	  pred2 = preds.second;
	}

	if (getEstimateLength(nextPoint, pred1, pred2) >= minLength) {
	  assert((*mesh).point(nextPoint).getIsBoundary() || (*mesh).point(pred1).getIsBoundary() || (length[pred1] <= length[nextPoint]));
	  assert((*mesh).point(nextPoint).getIsBoundary() || (*mesh).point(pred2).getIsBoundary() || (length[pred2] <= length[nextPoint]));
	  assert((*mesh).point(pred1).getIsBoundary() || (*mesh).point(nextPoint).getIsBoundary() || pred[pred1] != nextPoint);
	  assert((*mesh).point(pred2).getIsBoundary() || (*mesh).point(nextPoint).getIsBoundary() || pred[pred2] != nextPoint);


	  // pre: log flags
	  int * f = sFlags;
	  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++f)
	    *f = (*p).getFlag();

	  // get the non separating property
	  bool nonSeparating = meshPart.inSameCompCC(nextPoint);

	  // post: get last point flags
	  f = sFlags;
	  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++f) {
	    (*p).setFlag(*f);
	  }

	  // if a non separating cycle has been found: we found shortest path
	  if (nonSeparating) {
	    found = true;
	    break;
	  }
	}
      }
      else {
	// reinit flags
	if ((*mesh).point(nextPoint).getIsBoundary() && merge) {
	  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i)
	    if ((*mesh).point(*i).getFlag() != 0)
	      (*mesh).point(*i).setFlag(1);
	}
	else {
	  for(std::deque<VertexID>::const_iterator i = (*mesh).point(nextPoint).getNeighbours().begin(); i != (*mesh).point(nextPoint).getNeighbours().end(); ++i)
	    if ((*mesh).point(*i).getFlag() != 0)
	      (*mesh).point(*i).setFlag(1);
	}
      }
    }
    if ((maxLength > 0) && (length[nextPoint] > maxRadius)) {
      delete [] sFlags;
      return std::pair<VertexID, std::pair<VertexID, VertexID> >((*mesh).getNbPoints(), std::pair<VertexID, VertexID>());
    }

    ++idNextPoint;
    ++itL;

  }

  delete[] sFlags;

  return std::pair<VertexID, std::pair<VertexID, VertexID> > (nextPoint, std::pair<VertexID, VertexID>(pred1, pred2));
}

std::deque<VertexID> MeshManipulator::getShortestNonTrivialCycleFromPoint(VertexID basePoint) {
  VertexID pred1 = (*mesh).getNbPoints();
  VertexID pred2 = (*mesh).getNbPoints();
  double lengthCycle = std::numeric_limits<double>::max();

  MeshPart meshPart(*mesh, true);
  meshPart.initInsideSimpleCutFlags();
  computeTDijkstra(basePoint, 1, 1);


//      MeshMap mMap(*mesh);
//      mMap.setValuesFromFlags();
//      Display3D::displayMeshMap(512, 512, "debug", *mesh, mMap);

  meshPart.initSimpleCutFlags();


  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 3) {
      double l = length[(*p).getId()];
      double l2 = std::numeric_limits<double>::max();
      VertexID otherSide = (*mesh).getNbPoints();
      for(std::deque<VertexID>::const_iterator n = (*p).getNeighbours().begin(); n != (*p).getNeighbours().end(); ++n)
	if ((*mesh).point(*n).getFlag() == 3) {
	  bool o = false;
	  const Triangle & t1 = (*mesh).findTriangle((*p).getId(), *n);
	  if (t1.getFlag() != 2) {
	    try {
	      const Triangle & t2 = (*mesh).findOtherTriangle((*p).getId(), *n, t1.getId());
	      if (t2.getFlag() != 2) {
		o = true;
	      }
	    }
	    catch (Exception e) {
	      o = true;
	    }
	  }
	  if (o && (l2 > length[*n] + (*mesh).point(*n).distance(*p))) {
	    l2 = length[*n] + (*mesh).point(*n).distance(*p);
	    otherSide = *n;
	  }
	}

      if ((length[(*p).getId()] >= 0)  && (lengthCycle > l + l2)) {
	pred1 = (*p).getId();
	pred2 = otherSide;
	lengthCycle = l + l2;
      }
    }
  // build paths
  if ((pred1 == (*mesh).getNbPoints()) || (pred2 == (*mesh).getNbPoints()))
    throw Exception("getShortestNonTrivialCycleFromPoint(1): cannot find a pred point.");

  std::deque<VertexID> b1side;
  VertexID current = pred1;
  while(current != basePoint) {
    if (length[current] < 0)
      throw Exception("Wrong distance");
    b1side.push_back(current);
    current = pred[current];
  }
  b1side.push_back(current);

  std::vector<VertexID> b2side;
  current = pred2;
  while(current != basePoint) {
    b2side.push_back(current);
    current = pred[current];
  }
  b2side.push_back(current);


  std::reverse(b1side.begin(), b1side.end());
  b1side.insert(b1side.end(), b2side.begin(), b2side.end());

  return b1side;
}


std::deque<VertexID> MeshManipulator::getShortestNonSeparatingCycleFromPoint(VertexID basePoint, double maxLength, double minLength, bool merge) {
  if ((*mesh).point(basePoint).getIsBoundary())
    return std::deque<VertexID>();

  std::pair<VertexID, std::pair<VertexID, VertexID> > path = getTargetShortestNonSeparatingCycleFromPoint(basePoint, maxLength, minLength, merge);
  VertexID nextPoint = path.first;
  VertexID pred1 = path.second.first;
  VertexID pred2 = path.second.second;
  if ((nextPoint == (*mesh).getNbPoints()) || (pred1 >= (*mesh).getNbPoints()) || (pred2 >= (*mesh).getNbPoints()))
    return std::deque<VertexID>();

  assert(pred1 < (*mesh).getNbPoints());
  assert(pred2 < (*mesh).getNbPoints());

  return buildCycle(basePoint, nextPoint, pred1, pred2);
}


TriangleID MeshManipulator::computeCCUsingTriangles(VertexID idP, int iFlag, int fFlag) {
  TriangleID result = 0;
  if ((*mesh).triangle(idP).getFlag() == iFlag) {
    ++result;
    (*mesh).triangle(idP).setFlag(fFlag);
    try {
      result += computeCCUsingTriangles((*mesh).findOtherTriangle((*mesh).triangle(idP).getP1(),
							       (*mesh).triangle(idP).getP2(),
							       (*mesh).triangle(idP)).getId(), iFlag, fFlag);
    }
    catch (Exception e) { }
    try {
      result += computeCCUsingTriangles((*mesh).findOtherTriangle((*mesh).triangle(idP).getP2(),
							       (*mesh).triangle(idP).getP3(),
							       (*mesh).triangle(idP)).getId(), iFlag, fFlag);
    }
    catch (Exception e) { }
    try {
      result += computeCCUsingTriangles((*mesh).findOtherTriangle((*mesh).triangle(idP).getP3(),
							       (*mesh).triangle(idP).getP1(),
							       (*mesh).triangle(idP)).getId(), iFlag, fFlag);
    }
    catch (Exception e) { }

  }

  return result;
}

TriangleID MeshManipulator::computeCCUsingTriangles(VertexID idP, const CCRuleTriangle & rule, int iFlag, int fFlag) {
  TriangleID result = 0;
  if (rule.isInside((*mesh), idP, iFlag, fFlag)) {
    ++result;
    (*mesh).triangle(idP).setFlag(fFlag);
    try {
      result += computeCCUsingTriangles((*mesh).findOtherTriangle((*mesh).triangle(idP).getP1(),
							       (*mesh).triangle(idP).getP2(),
							       (*mesh).triangle(idP)).getId(), rule, iFlag, fFlag);
    }
    catch (Exception e) { }
    try {
      result += computeCCUsingTriangles((*mesh).findOtherTriangle((*mesh).triangle(idP).getP2(),
							       (*mesh).triangle(idP).getP3(),
							       (*mesh).triangle(idP)).getId(), rule, iFlag, fFlag);
    }
    catch (Exception e) { }
    try {
      result += computeCCUsingTriangles((*mesh).findOtherTriangle((*mesh).triangle(idP).getP3(),
							       (*mesh).triangle(idP).getP1(),
							       (*mesh).triangle(idP)).getId(), rule, iFlag, fFlag);
    }
    catch (Exception e) { }

  }

  return result;
}

VertexID MeshManipulator::computeCCUsingPointsSimple(VertexID id, int iFlag, int fFlag) {
  if ((*mesh).point(id).getFlag() != iFlag)
    return 0;

  std::deque<VertexID> & nb = (*mesh).point(id).getNeighbours();
  VertexID result = 1;

  (*mesh).point(id).setFlag(fFlag);

  for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
    if ((*mesh).point(*i).getFlag() == iFlag)
      result += computeCCUsingPointsSimple(*i, iFlag, fFlag);



  return result;
}

VertexID MeshManipulator::computeCCUsingPointsSimple(std::vector<int> & flags, VertexID id, int iFlag, int fFlag) const {
  if (flags[id] != iFlag)
    return 0;

  std::deque<VertexID> & nb = (*mesh).point(id).getNeighbours();
  VertexID result = 1;

  flags[id] = fFlag;

  for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
    if ((*mesh).point(*i).getFlag() == iFlag)
      result += computeCCUsingPointsSimple(flags, *i, iFlag, fFlag);

  return result;
}


VertexID MeshManipulator::computeFlagsUsingCC(std::vector<int> & flags) const {
  if (flags.size() != (*mesh).getNbPoints()) {
    flags.clear();
    flags.insert(flags.begin(), (*mesh).getNbPoints(), -1);
  }
  else
    for(std::vector<int>::iterator i = flags.begin(); i != flags.end(); ++i)
      *i = -1;

  VertexID nbCC = 0;

  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if (flags[(*p).getId()] == -1) {
      ++nbCC;
      computeCCUsingPointsSimple(flags, (*p).getId(), -1, (int)nbCC - 1);
    }

  return nbCC;

}

VertexID MeshManipulator::computeFlagsUsingCC() {
  (*mesh).setPointFlag(-1);
  VertexID nbCC = 0;

  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == -1) {
      ++nbCC;
      computeCCUsingPointsSimple((*p).getId(), -1, (int)nbCC - 1);
    }

  return nbCC;
}



VertexID MeshManipulator::computeCCUsingPoints(VertexID id, int iFlag, int fFlag, int tInside) {
  VertexID result = 0;

  if ((*mesh).point(id).getFlag() != iFlag)
    return 0;

  std::deque<VertexID> open;
  open.push_back(id);
  (*mesh).point(id).setFlag(fFlag);

  while(open.size() != 0) {
    VertexID next = open.front();
    open.pop_front();
    ++result;


    std::deque<VertexID> & nb = (*mesh).point(next).getNeighbours();
    for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
      if (((*mesh).point(*i).getFlag() == iFlag) && (isInsideEdgeTFlag(next, *i, tInside))) {
	open.push_back(*i);
	(*mesh).point(*i).setFlag(fFlag);
      }

  }
  return result;
}

VertexID MeshManipulator::computeBoundaryCC(VertexID id, int iFlag, int fFlag, int tInside) {
  if ((*mesh).point(id).getFlag() != iFlag)
    return 0;

  std::deque<VertexID> & nb = (*mesh).point(id).getNeighbours();
  VertexID result = 1;

  (*mesh).point(id).setFlag(fFlag);

  for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
    if (isBoundaryEdge(id, *i, tInside))
      result += computeBoundaryCC(*i, iFlag, fFlag, tInside);


  return result;
}


std::deque<VertexID> MeshManipulator::computeBoundaryCCList(VertexID id, int iFlag, int fFlag, int tInside) {
  std::deque<VertexID> result;
  VertexID currentId = id;
  if ((*mesh).point(id).getFlag() != iFlag)
    return result;

  while((*mesh).point(currentId).getFlag() == iFlag) {
    (*mesh).point(currentId).setFlag(fFlag);
    std::deque<VertexID> & nb = (*mesh).point(currentId).getNeighbours();
    result.push_back(currentId);

    for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
      if (isBoundaryEdge(currentId, *i, tInside) && ((*mesh).point(*i).getFlag() == iFlag)) {
	currentId = *i;
	break;
      }
  }

  if (currentId == id)
    result.push_back(id);

  return result;
}

std::deque<VertexID> MeshManipulator::computeBoundaryCCList(VertexID id, int iFlag, int fFlag) {
  std::deque<VertexID> result;
  VertexID currentId = id;
  if ((*mesh).point(id).getFlag() != iFlag)
    return result;

  while((*mesh).point(currentId).getFlag() == iFlag) {
    (*mesh).point(currentId).setFlag(fFlag);
    std::deque<VertexID> & nb = (*mesh).point(currentId).getNeighbours();
    result.push_back(currentId);

    for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
      if ((*mesh).isBoundaryEdge(currentId, *i) && ((*mesh).point(*i).getFlag() == iFlag)) {
	currentId = *i;
	break;
      }
  }

  if (currentId == id)
    result.push_back(id);

  return result;
}



VertexID MeshManipulator::computeBoundaryCCSubset(VertexID id, int iFlag, int fFlag, int tInside) {
  if ((*mesh).point(id).getFlag() != iFlag)
    return 0;

  std::deque<VertexID> & nb = (*mesh).point(id).getNeighbours();
  VertexID result = 0;

  (*mesh).point(id).setFlag(fFlag);

  for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
    if ((*mesh).isBoundaryEdge(id, *i) && ((*mesh).findTriangle(id, *i).getFlag() == tInside))
      result += computeBoundaryCCSubset(*i, iFlag, fFlag, tInside);


  return result;
}

bool MeshManipulator::isInsideEdgeTFlag(VertexID idP1, VertexID idP2, int tInside) const {
  std::deque<TriangleID> & tr = (*mesh).point(idP1).getTriangles();

  for(std::deque<TriangleID>::const_iterator i = tr.begin(); i != tr.end(); ++i)
    if (((*mesh).triangle(*i).getFlag() == tInside) && ((*mesh).triangle(*i).hasPoint(idP2)))
      return true;


  return false;
}

bool MeshManipulator::isBoundaryEdge(VertexID idP1, VertexID idP2, int tInside) const {
  std::deque<TriangleID> & tr = (*mesh).point(idP1).getTriangles();
  bool inside = false;

  for(std::deque<TriangleID>::const_iterator i = tr.begin(); i != tr.end(); ++i)
    if (((*mesh).triangle(*i).getFlag() == tInside) && (*mesh).triangle(*i).hasPoint(idP2)) {
      if (inside) // another neighbour triangle has been found inside
	return false;
      else
	inside = true;
    }

  return inside;
}

std::deque<TriangleID> MeshManipulator::getTNeighbours(VertexID idP) {
  std::deque<TriangleID> result;
  try {
    result.push_front((*mesh).findOtherTriangle((*mesh).triangle(idP).getP1(),
					     (*mesh).triangle(idP).getP2(),
					     (*mesh).triangle(idP)).getId());
  }
  catch (Exception e) { }
  try {
    result.push_front((*mesh).findOtherTriangle((*mesh).triangle(idP).getP2(),
					     (*mesh).triangle(idP).getP3(),
					     (*mesh).triangle(idP)).getId());
  }
  catch (Exception e) { }
  try {
    result.push_front((*mesh).findOtherTriangle((*mesh).triangle(idP).getP3(),
					     (*mesh).triangle(idP).getP1(),
					     (*mesh).triangle(idP)).getId());
  }
  catch (Exception e) { }

  return result;
}


VertexID MeshManipulator::findNearestPoint(const Point3D & center) const {
  if ((*mesh).getNbPoints() <= 0)
    throw Exception(std::string("MeshPart: cannot find nearest point in an empty mesh."));

  return (*mesh).getNearestPoint(center);
}


void MeshManipulator::computeDijkstra(VertexID center) {
  /* init flags
   * 0: not seen before
   * 1: inside connected component
   */
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);


  computeDijkstra(center, 0);
}


void MeshManipulator::computeDijkstra(VertexID center, int fInside, int fInsideCC) {

  // compute connected component
  (*mesh).setTriangleFlag(0);
  computeCCUsingPoints(center, fInside, fInsideCC, 0);

  computeDijkstra(center, fInsideCC);

}



void MeshManipulator::computeDijkstra(VertexID center, int fInside) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);
  computeDijkstra(sPoints, fInside);
}

void MeshManipulator::computeDijkstraMergeBoundaries(VertexID center) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);

  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  computeDijkstra(sPoints, 0, LengthEdgeMergeBoundary(*lengthEdgeMethod));
}

void MeshManipulator::computeDijkstra(VertexID center, const LengthEdge & lEdge) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);

  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  computeDijkstra(sPoints, 0, lEdge);
}


void MeshManipulator::computeDijkstraBound(VertexID center, const LengthEdge & lEdge, double bound) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);

  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  computeDijkstra(sPoints, 0, lEdge, bound);
}

void MeshManipulator::computeDijkstraBound(VertexID center, double bound) {
  computeDijkstraBound(center, *lengthEdgeMethod, bound);
}


void MeshManipulator::computeDijkstraTarget(VertexID center, int fInside, VertexID idTarget) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);

  computeDijkstraTarget(sPoints, fInside, *lengthEdgeMethod, idTarget);
}




void MeshManipulator::initLengthAndPredList() {
  // reset memory if allocated size is not good
  if (nbPoints != (*mesh).getNbPoints()) {
    if (length != NULL)
      delete[] length;
    if (pred != NULL)
      delete[] pred;
    pred = NULL;
    length = NULL;

    nbPoints = (*mesh).getNbPoints();
  }

  // length initialisation
  if (length == NULL)
    length = new double[nbPoints];
  // predecessor initialisation
  if (pred == NULL)
    pred = new VertexID[nbPoints];


  for(VertexID i = 0; i < nbPoints; ++i) { // uint better than iterator
    length[i] = -1;
    pred[i] = 0;
  }
}

bool MeshManipulator::updateLengthAndPredList() {
  bool result = true;

  if ((length == NULL) || (pred == NULL)) { // if resize is not possible
    result = false;
    if (length != NULL)
      delete[] length;
    if (pred != NULL)
      delete[] pred;

    // length initialisation
    length = new double[(*mesh).getNbPoints()];
    // predecessor initialisation
    pred = new VertexID[(*mesh).getNbPoints()];

    nbPoints = (*mesh).getNbPoints();

    for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) { // uint better than iterator
      length[i] = -1;
      pred[i] = 0;
    }
  }
  else if (nbPoints != (*mesh).getNbPoints()) { // assums that points has been added
    assert(nbPoints <= (*mesh).getNbPoints());

    double * newLength = new double[(*mesh).getNbPoints()];
    double * lengthIt = length;
    double * newLengthIt = newLength;

    VertexID * newPred = new VertexID[(*mesh).getNbPoints()];
    VertexID * predIt = pred;
    VertexID * newPredIt = newPred;

    // copy data
    for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) {
      if (i < nbPoints) {
	*newLengthIt = *lengthIt;
	*newPredIt = *predIt;
	++predIt;
	++lengthIt;
      }
      else {
	*newLengthIt = -1.0;
	*newPred = 0;
      }
      ++newLengthIt;
      ++newPredIt;
    }

    // then free old data
    delete[] length;
    delete[] pred;
    nbPoints = (*mesh).getNbPoints();
    length = newLength;
    pred = newPred;

  }

  return result;
}




void MeshManipulator::computeDijkstraTargetUpdateLength(VertexID center, int fInside, VertexID idTarget, double bound) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);

  computeDijkstraUpdateLength(sPoints, fInside, *lengthEdgeMethod, bound, -1, idTarget);
}


void MeshManipulator::computeDijkstraUpdateLength(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge,
						  double initBound, double targetBound, VertexID idTarget) {

  std::priority_queue<std::pair<VertexID, double>, std::vector<std::pair<VertexID, double> >, CompDijkstra> open;


  // first update length and pred arrays and run full dijkstra if needed
  if (!updateLengthAndPredList()) {
    std::deque<VertexID> targets;
    targets.push_back(idTarget);
    computeDijkstra(sPoints, fInside, lEdge, targetBound, targets);
    return;
  }

  // watch for all first points
  for(std::deque<VertexID>::const_iterator sp = sPoints.begin(); sp != sPoints.end(); ++sp) {
    assert((length[*sp] == 0) && (pred[*sp] == 0));
    assert((*mesh).point(*sp).getFlag() == fInside);
    (*mesh).point(*sp).setFlag(fInside + 1);
  }

  // then build the open set
  std::vector<VertexID> beginPoints;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i)
    if (((*mesh).point(i).getFlag() == fInside) && ((length[i] > initBound) || (length[i] < 0))) {
      length[i] = -1.0;
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(i).getNeighbours().begin();
	  nb != (*mesh).point(i).getNeighbours().end(); ++nb) {
	Point3D & p = (*mesh).point(*nb);
	if ((p.getFlag() == fInside) && (length[*nb] <= initBound) && (length[*nb] >= 0)) {
	  p.setFlag(fInside + 2);
	  open.push(std::pair<VertexID, double>(*nb, length[*nb]));
	  beginPoints.push_back(*nb);
	}
      }
    }


  // then clear flags
  for(std::vector<VertexID>::const_iterator o = beginPoints.begin(); o != beginPoints.end(); ++o)
    (*mesh).point(*o).setFlag(fInside);
  beginPoints.clear();


  // then run dijkstra algorithm
  while(open.size() != 0) {
    VertexID idNearest = open.top().first;
    double v = open.top().second;
    open.pop();

    if (length[idNearest] == v) {

      if (((targetBound != -1.0) && (length[idNearest] != 0) && (length[pred[idNearest]] > targetBound)) || ((idTarget != (*mesh).getNbPoints()) && (idTarget == idNearest))) {
	break;
      }

      // for each neighbour inside CC
      std::deque<VertexID> &ns = (*mesh).point(idNearest).getNeighbours();
      for(std::deque<VertexID>::const_iterator n = ns.begin(); n != ns.end(); ++n)
	if ((*mesh).point(*n).getFlag() == fInside) {
	  double l = length[idNearest] + lEdge(idNearest, *n);
	  if ((length[*n] == -1.0) || (length[*n] > l)) {
	    length[*n] = l;
	    pred[*n] = idNearest;
	    open.push(std::pair<VertexID, double>(*n, l));
	  }
	}
    }
  }

  // clear source flags
  for(std::deque<VertexID>::const_iterator sp = sPoints.begin(); sp != sPoints.end(); ++sp)
    (*mesh).point(*sp).setFlag(fInside);

}


void MeshManipulator::computeTDijkstra(VertexID center, int fInside, int fTInside) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);
  computeTDijkstra(sPoints, fInside, fTInside, *lengthEdgeMethod);
}

void MeshManipulator::computeTDijkstraTarget(VertexID center, VertexID target, int fInside, int fTInside) {
  std::deque<VertexID> sPoints;
  sPoints.push_front(center);
  std::deque<VertexID> tPoints;
  tPoints.push_front(target);
  computeTDijkstraTarget(sPoints, tPoints, fInside, fTInside, *lengthEdgeMethod);
}


void MeshManipulator::computeTDijkstra(const std::deque<VertexID> & sPoints, int fInside, int fTInside, const LengthEdge & lEdge) {
  computeTDijkstraTarget(sPoints, std::deque<VertexID>(), fInside, fTInside, lEdge);
}


void MeshManipulator::computeTDijkstraTarget(const std::deque<VertexID> & sPoints,
					     const std::deque<VertexID> & tPoints,
					     int fInside, int fTInside, const LengthEdge & lEdge, double bound) {
  // flag initialization
  for(std::deque<VertexID>::const_iterator i = tPoints.begin(); i != tPoints.end(); ++i)
    if ((*mesh).point(*i).getFlag() == fInside)
      (*mesh).point(*i).setFlag(fInside + 1);


  initLengthAndPredList();
  std::priority_queue<std::pair<VertexID, double>, std::vector<std::pair<VertexID, double> >, CompDijkstra> open;

  // init center length and open set
  for(std::deque<VertexID>::const_iterator i = sPoints.begin(); i != sPoints.end(); ++i) {
    length[*i] = 0;
    open.push(std::pair<VertexID, double>(*i, 0.));
  }

  while(open.size() != 0) {
    VertexID idNearest = open.top().first;
    double v = open.top().second;
    open.pop();

    if (length[idNearest] == v) {

      if (((*mesh).point(idNearest).getFlag() == fInside + 1) || ((bound > -1) && (length[idNearest] != 0) && (length[pred[idNearest]] > bound))) // break when target point found
	break;

      // for each neighbour inside CC
      std::deque<VertexID> &ns = (*mesh).point(idNearest).getNeighbours();
      for(std::deque<VertexID>::const_iterator n = ns.begin(); n != ns.end(); ++n)
	if ((((*mesh).point(*n).getFlag() == fInside) || ((*mesh).point(*n).getFlag() == fInside + 1))
	    && (isInsideEdgeTFlag(idNearest, *n, fTInside))) {
	  double l = length[idNearest] + lEdge(idNearest, *n);
	  if ((length[*n] == -1) || (length[*n] > l)) {
	    length[*n] = l;
	    pred[*n] = idNearest;
	    open.push(std::pair<VertexID, double>(*n, l));
	  }
	}
      }
  }

  // flag reinitialization
  for(std::deque<VertexID>::const_iterator i = tPoints.begin(); i != tPoints.end(); ++i)
    if ((*mesh).point(*i).getFlag() == fInside + 1)
      (*mesh).point(*i).setFlag(fInside);

}

void MeshManipulator::computeDijkstraStickCuttings(VertexID center) {
  /* init flags
   * 0: not seen before
   * 1: inside connected component
   * 2: final flag value
   */
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  std::deque<VertexID> sPoints;
  sPoints.push_front(center);

  computeDijkstraStickCuttings(sPoints, 0, *lengthEdgeMethod, -1);
}

void MeshManipulator::computeDijkstraStickCuttings(const std::deque<VertexID> & sPoints, int fInside) {
  computeDijkstraStickCuttings(sPoints, fInside, *lengthEdgeMethod, -1);
}

void MeshManipulator::computeDijkstraStickCuttings(const std::deque<VertexID> & sPoints, int fInside, const LengthEdge & lEdge, double bound) {
  std::priority_queue<std::pair<VertexID, double>, std::vector<std::pair<VertexID, double> >, CompDijkstra> open;

  if ((sameLocation == NULL) || (nbSameLocation != (*mesh).getNbPoints()))
    computeSameLocationList();

  initLengthAndPredList();

  // init center length and open set
  for(std::deque<VertexID>::const_iterator i = sPoints.begin(); i != sPoints.end(); ++i) {
    length[*i] = 0;
    open.push(std::pair<VertexID, double>(*i, 0.));
    for(std::deque<VertexID>::const_iterator others = sameLocation[*i].begin(); others != sameLocation[*i].end(); ++others) {
      length[*others] = 0;
      open.push(std::pair<VertexID, double>(*others, 0.));
    }
  }

  while(open.size() != 0) {
    VertexID idNearest = open.top().first;
    double v = open.top().second;
    open.pop();

    if (length[idNearest] == v) {

      if ((bound != -1) && (length[idNearest] > bound))
	break;

      // for each neighbour inside CC
      std::deque<VertexID> &ns = (*mesh).point(idNearest).getNeighbours();
      for(std::deque<VertexID>::const_iterator n = ns.begin(); n != ns.end(); ++n)
	if ((*mesh).point(*n).getFlag() == fInside) {
	  double l = length[idNearest] + lEdge(idNearest, *n);
	  if ((length[*n] == -1) || (length[*n] > l)) {
	    length[*n] = l;
	    pred[*n] = idNearest;
	    open.push(std::pair<VertexID, double>(*n, l));
	    for(std::deque<VertexID>::const_iterator others = sameLocation[*n].begin(); others != sameLocation[*n].end(); ++others) {
	      length[*others] = l;
	      pred[*others] = idNearest;
	      open.push(std::pair<VertexID, double>(*others, l));
	    }
	  }
	}
    }
  }
}


void MeshManipulator::computeEuclideanLength(VertexID pId) {
  if (length != NULL)
    delete[] length;

  // length initialisation
  length = new double[(*mesh).getNbPoints()];


  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    length[(*i).getId()] = (*i).distance((*mesh).point(pId));

}


std::vector<std::vector<VertexID> > MeshManipulator::computeShortestPaths(VertexID idP, const std::vector<VertexID> & points) {
  std::vector<std::vector<VertexID> > result;

  // compute dijkstra algorithm from idP1
  computeDijkstra(idP);

  // rebuild the paths
  for(std::vector<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p) {
    assert(length[*p] != -1);
    result.push_back(std::vector<VertexID>());
    result.back().push_back(*p);
    while(result.back().back() != idP) {
      result.back().push_back(pred[result.back().back()]);
    }
    std::reverse(result.back().begin(), result.back().end());
  }

  return result;
}


std::deque<VertexID> MeshManipulator::computeShortestPath(VertexID idP1, VertexID idP2) {
  std::deque<VertexID> result;

  // reset flags
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  // compute dijkstra algorithm from idP1
  computeDijkstraTarget(idP1, 0, idP2);

  // if idP2 is not in same connected component than idP1, path is empty
  if (length[idP2] == -1)
    return result;
  else { // else build path using pred values from dijkstra algorithm
    result.push_front(idP2);
    while(result.front() != idP1)
      result.push_front(pred[result.front()]);
  }

  return result;
}

std::deque<VertexID> MeshManipulator::computeShortestPathNoBoundary(VertexID idP1, VertexID idP2) {
  std::deque<VertexID> result;

  // reset flags
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getIsBoundary())
      (*p).setFlag(1);
  (*mesh).point(idP2).setFlag(0);
  (*mesh).point(idP1).setFlag(0);

  // compute dijkstra algorithm from idP1
  computeDijkstraTarget(idP1, 0, idP2);

  // if idP2 is not in same connected component than idP1, path is empty
  if (length[idP2] == -1) {
    return result;
  }
  else { // else build path using pred values from dijkstra algorithm
    result.push_front(idP2);
    while(result.front() != idP1)
      result.push_front(pred[result.front()]);
  }

  return result;
}


std::deque<VertexID> MeshManipulator::computeShortestPath(const std::deque<VertexID> & idPs1, const std::deque<VertexID> & idPs2) {
  std::deque<VertexID> result;

  if (idPs1.size() * idPs2.size() == 0)
    return result;

  // reset flags
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  // compute dijkstra algorithm from idPs1
  computeDijkstraTarget(idPs1, 0, idPs2);

  VertexID idPSelected;
  double lengthSelected = -1;

  for(std::deque<VertexID>::const_iterator tPoint = idPs2.begin(); tPoint != idPs2.end(); ++tPoint) {
    double localLength = length[*tPoint];
    if ((lengthSelected == -1.0) || (lengthSelected > localLength)) {
      idPSelected = *tPoint;
      lengthSelected = localLength;
    }
  }

  // if idP2 is not in same connected component than idP1, path is empty
  if (lengthSelected == -1)
      return result;
  else { // else build path using pred values from dijkstra algorithm
    result.push_front(idPSelected);
    while(length[result.front()] > 0.0)
      result.push_front(pred[result.front()]);
  }

  return result;
}


std::deque<VertexID> MeshManipulator::computeShortestPathUpdateLength(VertexID idP1, VertexID idP2, double bound) {
  std::deque<VertexID> result;

  // compute dijkstra algorithm from idP1
  computeDijkstraTargetUpdateLength(idP1, 0, idP2, bound);

  // if idP2 is not in same connected component than idP1, path is empty
  if (length[idP2] == -1)
    return result;
  else { // else build path using pred values from dijkstra algorithm
    result.push_front(idP2);
    while(result.front() != idP1) {
      result.push_front(pred[result.front()]);
      assert((*mesh).point(result.front()).getFlag() == 0);
    }
  }

  return result;
}


std::deque<VertexID> MeshManipulator::computeShortestPathInCC(VertexID idP1, VertexID idP2, int iFlag) {
  std::deque<VertexID> result;

  // compute dijkstra algorithm from idP1
  computeDijkstraTarget(idP1, iFlag, idP2);

  // if idP2 is not in same connected component than idP1, path is empty
  if (length[idP2] == -1)
    return result;
  else { // else build path using pred values from dijkstra algorithm
    result.push_front(idP2);
    while(result.front() != idP1) {
      assert((*mesh).point(pred[result.front()]).getFlag() == iFlag);
      result.push_front(pred[result.front()]);
    }
  }

  return result;
}

std::deque<VertexID> MeshManipulator::computeShortestPath(const std::deque<std::deque<VertexID> > & pathes) {
  std::deque<std::deque<VertexID> > result = computeShortestTreeFromPathes(pathes, 1);
  assert(result.size() == 1);

  return result.front();
}

void MeshManipulator::mergeWaveFronts(std::deque<std::pair<double *, VertexID *> > & dijkstraValues, std::deque<std::deque<VertexID> > & sortedPoints,
				      std::deque<VertexID> & idInPath,
				      int idPath1, int idPath2, const std::deque<VertexID> & path) {
  // the merge is proceed on #idPath1

  // first merge the dijkstra values
  std::pair<double *, VertexID *> & dijkstraValue1 = dijkstraValues[idPath1];
  std::pair<double *, VertexID *> & dijkstraValue2 = dijkstraValues[idPath2];

  double * length1 = dijkstraValue1.first;
  double * length2 = dijkstraValue2.first;
  VertexID * pred1 = dijkstraValue1.second;
  VertexID * pred2 = dijkstraValue2.second;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) {
    if (*length1 > *length2) {
      *length1 = *length2;
      *pred1 = *pred2;
    }
    ++length1;
    ++length2;
    ++pred1;
    ++pred2;
  }

  lengthPathBP lsort;
  lsort.length = dijkstraValue1.first;
  lsort.mesh = mesh;
  // update sortedPoints[idPath1]
  sort(sortedPoints[idPath1].begin(), sortedPoints[idPath1].end(), lsort);

  // set flag = idPath1 for all points in idPath2 and update idInPath
  unsigned int id = 0;
  for(std::deque<VertexID>::const_iterator i = sortedPoints[idPath1].begin(); i != sortedPoints[idPath1].end(); ++i) {
    if ((*mesh).point(*i).getFlag() == idPath2 + 1)
      (*mesh).point(*i).setFlag(idPath1 + 1);
    if ((*mesh).point(*i).getFlag() == 0)
      idInPath[idPath1] = id;
    ++id;
  }


  // set dijkstra length = 0 for each point of the path
  for(std::deque<VertexID>::const_iterator i = path.begin(); i != path.end(); ++i)
    dijkstraValue1.first[*i] = 0.0;

  // then clear the memory
  delete[] dijkstraValue2.first;
  delete[] dijkstraValue2.second;
  dijkstraValue2.first = NULL;
  dijkstraValue2.second = NULL;
  sortedPoints[idPath2].clear();

  idInPath[idPath2] = 0;

}


void MeshManipulator::computeShortestTree(std::deque<std::pair<double *, VertexID *> > & dijkstraValues, std::deque<std::deque<VertexID> > & sortedPoints,
					  std::deque<VertexID> & idInPath, unsigned int nbPathes, unsigned int nbValidBorders,
					  std::deque<std::deque<VertexID> > & resultTree) {

  bool found = false;
  VertexID idMiddle = 0;
  int idPath1 = 0;
  int idPath2 = 0;


  // recursive function:
  // at each step, the wavefronts are propagated until two of them are join. A corresponding path is created
  // and the propagation is then recalled to find the next joining path (if needed)

  assert(idInPath.size() == dijkstraValues.size());
  // then border propagation to find middle point
  while(!found) {

    // first find next point to add
    double length_t = std::numeric_limits<double>::max();
    VertexID idPath = 0;
    for(VertexID i = 0; i < idInPath.size(); ++i) // uint better than iterator
      if (idInPath[i] < sortedPoints[i].size()) {
	assert(sortedPoints[i][idInPath[i]] < (*mesh).getNbPoints());
	if (dijkstraValues[i].first[sortedPoints[i][idInPath[i]]] < length_t) {
	  idPath = i;
	  length_t = dijkstraValues[i].first[sortedPoints[i][idInPath[i]]];
	}
      }
    VertexID idPoint = sortedPoints[idPath][idInPath[idPath]];
    ++(idInPath[idPath]);
    assert(idPoint <= (*mesh).getNbPoints());
    // then check flag
    if (((*mesh).point(idPoint).getFlag() != 0) && ((unsigned int) (*mesh).point(idPoint).getFlag() - 1 != idPath)) {
      found = true; // if != 0, we found middle
      idMiddle = idPoint;
      idPath1 = (*mesh).point(idPoint).getFlag() - 1;
      idPath2 = idPath;
      assert(idPath1 != idPath2);
    }
    else // else set flag value according to idPath, then proceed
      (*mesh).point(idPoint).setFlag(idPath + 1);
  }

  // build path betwen path #idPath1, point #idMiddle and path #idPath2
  std::deque<VertexID> result;

  VertexID idP = idMiddle;
  while(dijkstraValues[idPath1].first[idP] != 0.0) {
    assert((result.size() == 0) || ((*mesh).point(idP).hasNeighbour(result.front())));
    result.push_front(idP);
    idP = dijkstraValues[idPath1].second[idP];
  }
  if ((result.size() == 0) || (dijkstraValues[idPath1].first[result.front()] != 0.0)) {
    result.push_front(idP);
  }

  idP = dijkstraValues[idPath2].second[idMiddle];
  if (dijkstraValues[idPath2].first[idMiddle] != 0.0) {
    while(dijkstraValues[idPath2].first[idP] != 0.0) {
      assert((result.size() == 0) || ((*mesh).point(idP).hasNeighbour(result.back())));
      result.push_back(idP);
      idP = dijkstraValues[idPath2].second[idP];
    }
    result.push_back(idP);
  }

  // if needed, apply the wavefront iteratively
  if (((nbPathes == 0) || (nbPathes > 1)) && (nbValidBorders != 2)) {
    if (nbPathes != 0)
      --nbPathes;
    // first merge the wavefronts idPath1 and idPath2
    mergeWaveFronts(dijkstraValues, sortedPoints, idInPath, idPath1, idPath2, result);

    // then iteratively call the tree computation method
    computeShortestTree(dijkstraValues, sortedPoints, idInPath, nbPathes, nbValidBorders - 1, resultTree);
  }

  // then add the path computed on the function to the result tree
  resultTree.push_front(result);
}

std::deque<std::deque<VertexID> > MeshManipulator::computeShortestTree(const std::deque<VertexID> & points, unsigned int nbPathes) {
  std::deque<std::deque<VertexID> > pointsList;
  for(std::deque<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p) {
    pointsList.push_back(std::deque<VertexID>());
    pointsList.back().push_back(*p);
  }

  return computeShortestTreeFromPathes(pointsList, nbPathes);
}


std::deque<std::deque<VertexID> > MeshManipulator::computeShortestTreeFromPathes(const std::deque<std::deque<VertexID> > & pathes, unsigned int nbPathes) {
  std::deque<std::deque<VertexID> > result;

  if (pathes.size() < 1)
    return result;

  (*mesh).setPointFlag(0);

  // compute dijkstra for each path
  if (length != NULL) delete[] length;
  if (pred != NULL) delete[] pred;
  std::deque<std::pair<double *, VertexID *> > dijkstraValues;
  for(std::deque<std::deque<VertexID> >::const_iterator i = pathes.begin(); i != pathes.end(); ++i) {
    length = NULL;
    pred = NULL;
    computeDijkstra(*i, 0);
    dijkstraValues.push_back(std::pair<double *, VertexID *>(length, pred));
  }
  length = NULL;
  pred = NULL;
  // build an ordered set of all mesh points using dijkstra length for each path
  std::deque<std::deque<VertexID> > sortedPoints;
  lengthPathBP lpath;
  for(std::deque<std::pair<double *, VertexID *> >::const_iterator p = dijkstraValues.begin();
      p != dijkstraValues.end(); ++p) {
    lpath.length = (*p).first;
    lpath.mesh = mesh;

    std::deque<VertexID> sList;
    for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
      sList.push_back(i);
    std::sort(sList.begin(), sList.end(), lpath);
    sortedPoints.push_back(sList);
  }

  // flag initialisation
  (*mesh).setPointFlag(0);
  std::deque<VertexID> idInPath;
  VertexID idPathS = 1;
  for(std::deque<std::deque<VertexID> >::const_iterator i = pathes.begin(); i != pathes.end(); ++i) {
    for(std::deque<VertexID>::const_iterator j = (*i).begin(); j != (*i).end(); ++j)
      (*mesh).point(*j).setFlag(idPathS);
    idInPath.push_back((*i).size()); // set next point id in path *i
    ++idPathS;
  }


  // then build iteratively the tree
  computeShortestTree(dijkstraValues, sortedPoints, idInPath, nbPathes, sortedPoints.size(), result);

  // delete allocated data
  for(std::deque<std::pair<double *, VertexID *> >::iterator p = dijkstraValues.begin();
      p != dijkstraValues.end(); ++p)
    if ((*p).first != NULL) {
      delete [] (*p).first;
      delete [] (*p).second;
    }

  return result;
}


Point3D MeshManipulator::computeBarycenter() {
  double sumx = 0;
  double sumy = 0;
  double sumz = 0;
  double nbPoints_t = (*mesh).getNbPoints();


  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i) {
    sumx += (*i).getX();
    sumy += (*i).getY();
    sumz += (*i).getZ();
  }

  return Point3D(sumx / nbPoints_t, sumy / nbPoints_t, sumz / nbPoints_t);
}


void MeshManipulator::addPointSetFlag(MeshPart & meshPart, VertexID pt) {
  meshPart.addPoint(pt);
  (*mesh).point(pt).setFlag(1);

  // add triangles if needed
  for(std::deque<TriangleID>::const_iterator i = (*mesh).point(pt).getTriangles().begin(); i != (*mesh).point(pt).getTriangles().end(); ++i)
    if (((*mesh).point((*mesh).triangle(*i).getP1()).getFlag() != 0) &&
	((*mesh).point((*mesh).triangle(*i).getP2()).getFlag() != 0) &&
	((*mesh).point((*mesh).triangle(*i).getP3()).getFlag() != 0)) {
      meshPart.addTriangle(*i);
      (*mesh).triangle(*i).setFlag(1);
    }
}


/** Computing the points' distribution modifying there flags */
VertexID MeshManipulator::computeNeighbourDistrib(const std::deque<VertexID> & bList) {
  VertexID nbPart = 0;

  std::deque<VertexID> nList;
  int flagValue = (std::numeric_limits<int>::max() / 3) * 2;
  int flagValueBorder = flagValue - 1;

  // pre: if there is no neighbour
  if (bList.size() == 0) {
    return 0;
  }

  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i)
    (*mesh).point(*i).setFlag(flagValueBorder);

  /* first compute boundary junctions */


  std::pair<std::deque<VertexID>, VertexID> j = getJunctionsSetFlags(flagValue, bList);

  // reinit flags of the triangles
  for(std::deque<VertexID>::const_iterator t = j.first.begin(); t != j.first.end(); ++t)
    (*mesh).triangle(*t).setFlag(1);

  /* then compute number of connection */

  // first build list of boundary points connected to an allready seen point (not boundary)
  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i) {
    // first look for triangles arround the selected boundary point
    for(std::deque<TriangleID>::const_iterator nb = (*mesh).point(*i).getTriangles().begin(); nb != (*mesh).point(*i).getTriangles().end(); ++nb)
      if (((*mesh).triangle(*nb).getFlag() == 1) && (!(*mesh).isBoundaryTriangle(*nb))) {
	(*mesh).point(*i).setFlag(flagValue);
	nList.push_front(*i);
	break;
      }
    // if not inside a triangle, look for the neighbours
    if (nList.front() != *i)
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(*i).getNeighbours().begin(); nb != (*mesh).point(*i).getNeighbours().end(); ++nb)
	if ((!(*mesh).point(*nb).getIsBoundary()) && ((*mesh).point(*nb).getFlag() != 0)) {
	  (*mesh).point(*i).setFlag(flagValue);
	  nList.push_front(*i);
	  break;
      }
  }

  // then compute connected components in boundary distrib (using points), reset flags
  for(std::deque<VertexID>::const_iterator i = nList.begin(); i != nList.end(); ++i)
    if ((*mesh).point(*i).getFlag() == flagValue) {
      computeBoundaryCCSubset(*i, flagValue, nbPart + 2, 1);
      ++nbPart;
    }

  // reinit border flags
  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i)
    if ((*mesh).point(*i).getFlag() == flagValueBorder)
      (*mesh).point(*i).setFlag(1);

  return nbPart;
}


VertexID MeshManipulator::computeNeighbourDistrib(VertexID nextPoint) {
  // watch neighbours
  std::deque<VertexID> bPoints;
  VertexID nbPart = 0;
  VertexID nbPtInside = 0;
  bool inside = (*mesh).point((*mesh).point(nextPoint).getNeighbour(0)).getFlag() == 1;
  if (inside) nbPart = 1;
  for(std::deque<VertexID>::const_iterator nb = (*mesh).point(nextPoint).getNeighbours().begin();
      nb != (*mesh).point(nextPoint).getNeighbours().end(); ++nb) {
    if ((*mesh).point(*nb).getFlag() != 0)
      ++nbPtInside;
    if (inside && ((*mesh).point(*nb).getFlag() == 0))
      inside = false;
	else if ((!inside) && ((*mesh).point(*nb).getFlag() != 0)) {
	  inside = true;
	  ++nbPart;
	}
    if ((*mesh).point(*nb).getFlag() != 0) {
      (*mesh).point(*nb).setFlag(nbPart);
      if ((*mesh).point(*nb).getIsBoundary()) {
	bPoints.push_front(*nb);
      }
    }
  }

  // adjust the number of neighbourhood
  // if the begin and end of neighbours are in the same connected components, need update.
  if (((*mesh).point((*mesh).point(nextPoint).getNeighbour(0)).getFlag() != 0) &&
      ((*mesh).point(*((*mesh).point(nextPoint).getNeighbours().rbegin())).getFlag() != 0)) {
    --nbPart;
    int flag = (*mesh).point((*mesh).point(nextPoint).getNeighbours().front()).getFlag();
    for(std::deque<VertexID>::reverse_iterator nb = (*mesh).point(nextPoint).getNeighbours().rbegin();
	nb != (*mesh).point(nextPoint).getNeighbours().rend(); ++nb)
      if((*mesh).point(*nb).getFlag() != 0)
	(*mesh).point(*nb).setFlag(flag);
      else
	break;
  }

  // adjust according to boundary points (they may be in the same cc)
  if ((bPoints.size() > 1)) {
    for(std::deque<VertexID>::const_iterator nb = bPoints.begin(); nb != bPoints.end(); ++nb)
      for(std::deque<VertexID>::const_iterator nb2 = bPoints.begin(); nb2 != bPoints.end(); ++nb2)
	if (nb != nb2)
	  if (((*mesh).point(*nb).getFlag() != (*mesh).point(*nb2).getFlag()) && // not in same connected component
	      ((*mesh).inSameBoundary(*nb, *nb2))) { // but in same boundary
	    --nbPart;
	    computeCCUsingPointsSimple(*nb2, (*mesh).point(*nb2).getFlag(), (*mesh).point(*nb).getFlag());
	  }
  }

  return nbPart;
}


std::pair<VertexID, VertexID> MeshManipulator::computePredBoundary(VertexID nextPoint) {
  assert((*mesh).point(nextPoint).getIsBoundary());
  std::pair<VertexID, VertexID> result;
  int flagValue = std::numeric_limits<int>::max() / 2;
  std::deque<VertexID> bList;
  std::deque<std::deque<VertexID> > bListCC;
  VertexID p = nextPoint;

  int currentFlag = -1;
  int firstFlag = -1;


  if ((*mesh).point(nextPoint).getFlag() > 1) { // if the first point is a point contains in a CC
    bListCC.push_back(std::deque<VertexID>());
    bListCC.back().push_back(p);
    currentFlag = (*mesh).point(p).getFlag();
    firstFlag = (*mesh).point(p).getFlag();
  }

  bList.push_back(p);

  p = (*mesh).getNextBPoint(p);
  VertexID pred_t = nextPoint;

  // first build list of boundary points and list of CC of boundary points
  while(p != nextPoint) {
    bList.push_back(p);
    assert((*mesh).point(p).getFlag() != 0);
    if ((*mesh).point(p).getFlag() != 1) {
      // a new CC is detected
      if ((firstFlag == -1) || (((*mesh).point(p).getFlag() != currentFlag) && ((*mesh).point(p).getFlag() != firstFlag))) {
	bListCC.push_back(std::deque<VertexID>());
	currentFlag = (*mesh).point(p).getFlag();
	if (firstFlag == -1)
	  firstFlag = currentFlag;
      }
      if ((*mesh).point(p).getFlag() != firstFlag)
	bListCC.back().push_back(p);
      else {
	bListCC.front().push_back(p);
	currentFlag = firstFlag;
      }
    }
    else
      (*mesh).point(p).setFlag(flagValue);

    VertexID tmp = p;
    p = (*mesh).getNextBPoint(p, pred_t);
    pred_t = tmp;
  }
  assert(bListCC.size() != 0);

  // then look for the neighbours of each boundary CC and build corresponding pred
  std::deque<VertexID> preds;
  for(std::deque<std::deque<VertexID> >::const_iterator cc = bListCC.begin(); cc != bListCC.end(); ++cc) {
    preds.push_back((*mesh).getNbPoints());
    double currLength = -1;
    for(std::deque<VertexID>::const_iterator bp = (*cc).begin(); bp != (*cc).end(); ++bp)
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(*bp).getNeighbours().begin();
	  nb != (*mesh).point(*bp).getNeighbours().end(); ++nb)
	if ((!(*mesh).point(*nb).getIsBoundary()) && ((*mesh).point(*nb).getFlag() == 1)) { // an inside neighbour
	  if ((currLength == -1) || (currLength > length[*nb])) {
	    currLength = length[*nb];
	    preds.back() = *nb;
	  }
	}
	else if ((*mesh).point(*nb).getIsBoundary() && ((*mesh).point(*nb).getFlag() != 0) && // this CC is connected to another cc (junction)
		 !(*mesh).isBoundaryEdge(*nb, *bp) &&
		 ((*mesh).point(*nb).getFlag() != (*mesh).point(*bp).getFlag()) &&
		 ((*mesh).point(*nb).getFlag() != flagValue) &&
		 ((*mesh).point(*nb).getFlag() != 1)) {
	  result.first = *bp;
	  result.second = *nb;
	  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i)
	    (*mesh).point(*i).setFlag(1);
	  return result;
	}
  }
  assert(preds.size() == bListCC.size());
  assert(preds.size() >= 2);

  // then find the two pred points with the smallest length
  result.first = (*mesh).getNbPoints();
  result.second = (*mesh).getNbPoints();

  for(std::deque<VertexID>::const_iterator i = preds.begin(); i != preds.end(); ++i)
    if (*i != (*mesh).getNbPoints()) {
      if (result.first == (*mesh).getNbPoints())
	result.first = *i;
      else if (result.second == (*mesh).getNbPoints())
	result.second = *i;
      else if (length[result.second] >= length[*i]) {
	if (length[result.first] >= length[*i]) {
	  result.second = result.first;
	  result.first = *i;
	}
	else
	  result.second = *i;
      }
    }

  assert(result.first != (*mesh).getNbPoints());
  assert(result.second != (*mesh).getNbPoints());

  // reset flags
  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i)
    (*mesh).point(*i).setFlag(1);


  return result;

}

std::pair<VertexID, VertexID> MeshManipulator::computePred(VertexID nextPoint) {
  return computePred((*mesh).point(nextPoint).getNeighbours());
}

std::pair<VertexID, VertexID> MeshManipulator::computePred(const std::deque<VertexID> & pList) {
  int idPart1 = 0;
  int idPart2 = 0;
  bool foundP1 = false;
  bool foundP2 = false;
  VertexID pred1;
  VertexID pred2 = 0;

  pred1 = pList[0];
  for(std::deque<VertexID>::const_reverse_iterator nb = pList.rbegin();
      nb != pList.rend(); ++nb) {
    int f = (*mesh).point(*nb).getFlag();
    if (f != 0) {
      if (!foundP1) {
	foundP1 = true;
	idPart1 = f;
	pred1 = *nb;
      }
      else if (!foundP2) {
	if (f != idPart1) {
	  foundP2 = true;
	  idPart2 = f;
	  pred2 = *nb;
	}
	else
	  if (length[*nb] < length[pred1]) {
	    pred1 = *nb;
	    idPart1 = f;
	  }
      }
      else {
	if ((length[pred1] < length[pred2]) && (length[*nb] < length[pred2]) && (f != idPart1)) {
	  pred2 = *nb;
	  idPart2 = f;
	}
	else if ((length[pred2] < length[pred1]) && (length[*nb] < length[pred1]) && (f != idPart2)) {
	  pred1 = *nb;
	  idPart1 = f;
	}
      }
    }
  }
  assert(foundP1 && foundP2);

  // reinit flags
  for(std::deque<VertexID>::const_iterator i = pList.begin(); i != pList.end(); ++i)
    if ((*mesh).point(*i).getFlag() != 0)
      (*mesh).point(*i).setFlag(1);

  return std::pair<VertexID, VertexID>(pred1, pred2);
}


VertexID MeshManipulator::findNextBPointFlag(VertexID ptBd, VertexID firstPoint, int flag) const {
  bool fNext = false;
  bool foundFirst = false;
  for(std::deque<VertexID>::const_iterator nb = (*mesh).point(ptBd).getNeighbours().begin();
      nb != (*mesh).point(ptBd).getNeighbours().end(); ++nb)
    if ((*mesh).point(*nb).getIsBoundary() && ((*mesh).point(*nb).getFlag() != flag) && (*mesh).isBoundaryEdge(ptBd, *nb)) {
      fNext = true;
      ptBd = *nb;
      break;
    }
    else {
      if (*nb == firstPoint)
	foundFirst = true;
    }

  assert(fNext || foundFirst);


  if (fNext)
    return ptBd;
  else
    return firstPoint;
}

bool MeshManipulator::inSameBoundaryCC(VertexID id1, VertexID id2) const {
  if (!(*mesh).point(id1).getIsBoundary() || !(*mesh).point(id2).getIsBoundary())
    return false;

  VertexID firstNext = (*mesh).getNextBPoint(id1);

  // first side
  VertexID next = firstNext;
  VertexID pred_t = id1;
  while((next != id1) && ((*mesh).findTriangle(next, pred_t).getFlag() == 1)) { // for each point in the same boundary CC
    if (next == id2)
      return true;
    VertexID tmp = next;
    next = (*mesh).getNextBPoint(next, pred_t);
    pred_t = tmp;
  }

  // other side
  next = (*mesh).getNextBPoint(id1, firstNext);
  pred_t = id1;
  while((next != id1) && ((*mesh).findTriangle(next, pred_t).getFlag() == 1)) { // for each point in the same boundary CC
    if (next == id2)
      return true;
    VertexID tmp = next;
    next = (*mesh).getNextBPoint(next, pred_t);
    pred_t = tmp;
  }

  return false;
}

std::deque<VertexID> MeshManipulator::buildCycle(VertexID basePoint, VertexID nextPoint, VertexID pred1, VertexID pred2) const {


  std::deque<VertexID> result;
  if ((pred1 == (*mesh).getNbPoints()) && (pred2 == (*mesh).getNbPoints()))
    return result;

  if ((pred1 == pred2) && (*mesh).point(nextPoint).getIsBoundary()) { // we are in a junction between two parts of a same boundary
    assert(!(*mesh).point(pred1).getIsBoundary());
    // we have to find two points of the boundary near pred1 that are not on the same side of the gap
    bool inBNb = false;
    std::deque<VertexID> b;
    VertexID pred_t = pred1;
    for(std::deque<VertexID>::const_iterator nb = (*mesh).point(pred1).getNeighbours().begin();
	nb != (*mesh).point(pred1).getNeighbours().end(); ++nb) {
      bool bP = (*mesh).point(*nb).getIsBoundary();
      if (bP && length[*nb] <= length[bP]) { // if the current neighbour is a boundary taking part in
	if (!inBNb) { // first boundary in a CC
	  inBNb = true;
	  if ((b.size() == 0) || !inSameBoundaryCC(bP, b.back())) // new CC
	    b.push_back(*nb);
	  else { // the same CC as the pred
	    if (length[b.back()] > length[*nb]) { // a neerest point
	      b.back() = *nb;
	    }
	  }
	}
	else if ((*mesh).isBoundaryEdge(pred_t, *nb)) { // another point in the same CC
	  if (length[b.back()] > length[*nb]) { // a neerest point
	    b.back() = *nb;
	  }
	}
	else {
	  if (inSameBoundaryCC(bP, b.back())) {
	    if (length[b.back()] > length[*nb]) { // a neerest point
	      b.back() = *nb;
	    }
	  } else
	    b.push_back(*nb);
	}
      }
      else // we are outside of a CC
	inBNb = false;

      pred_t = *nb;
    }
    assert(b.size() >= 2);
    result.push_front(b[0]);
    result.push_front(pred1);
    result.push_front(b[1]);


    return result;
  }

  //assert((pred1 != nextPoint) && (pred2 != nextPoint));

  if ((*mesh).point(pred1).hasNeighbour(pred2) && (*mesh).point(pred1).getIsBoundary() && (*mesh).point(pred2).getIsBoundary()) {
    // we are in a strong junction between two parts of a same boundary
    if (inSameBoundaryCC(pred1, pred2)) // TODO: the path should go through a junction part, and not here
      return result;
    assert(!(*mesh).isBoundaryEdge(pred1, pred2));
    result.push_back(pred1);
    result.push_front(pred2);
    return result;
  }

  result.push_front(nextPoint);

  // build path from pred1 to basePoint
  VertexID idP = pred1;

  assert((*mesh).point(nextPoint).getFlag() == 1);
  (*mesh).point(nextPoint).setFlag(2);

  // if there are missing points: it is a boundary junction
  if (!(*mesh).point(pred1).hasNeighbour(nextPoint)) {
    idP = nextPoint;
    while(!(*mesh).point(pred1).hasNeighbour(idP)) {
      idP = findNextBPointFlag(idP, nextPoint, 2);
      result.push_front(idP);
      (*mesh).point(idP).setFlag(2);
    }
    idP = pred1;
  }

  while(idP != basePoint) {
    result.push_front(idP);
    assert((*mesh).point(idP).hasNeighbour(pred[idP]));
    assert((*mesh).point(pred[idP]).hasNeighbour(idP));
    if ((*mesh).point(idP).getIsBoundary() && (*mesh).point(idP).hasNeighbour(basePoint))
      break;
    idP = pred[idP];
  }

  result.push_front(basePoint);

  // build path from pred2 to basePoint
  idP = pred2;

  // if there are missing points: it is a boundary junction
  if (!(*mesh).point(pred2).hasNeighbour(nextPoint)) {
    idP = nextPoint;
    while(!(*mesh).point(pred2).hasNeighbour(idP)) {
      idP = findNextBPointFlag(idP, nextPoint, 2);
      result.push_back(idP);
      (*mesh).point(idP).setFlag(2);
    }
    idP = pred2;
  }

  while(idP != basePoint) {
    result.push_back(idP);
    assert((*mesh).point(idP).hasNeighbour(pred[idP]));
    assert((*mesh).point(pred[idP]).hasNeighbour(idP));
    if ((*mesh).point(idP).getIsBoundary() && (*mesh).point(idP).hasNeighbour(basePoint))
      break;
    idP = pred[idP];
  }

  // if there are missing points: it is a boundary junction
  if (!(*mesh).point(result.back()).hasNeighbour(basePoint)) {
    VertexID p = result.back();
    idP = p;
    for(std::deque<VertexID>::const_iterator n = (*mesh).point(p).getNeighbours().begin();
	n != (*mesh).point(p).getNeighbours().begin(); ++n)
      if ((*mesh).point(*n).getIsBoundary() && (pred[*n] == basePoint)) {
	idP = *n;
	break;
      }
    assert(idP != p);
    while(idP != basePoint) {
      result.push_back(idP);
      idP = pred[idP];
    }
  }

  result.push_back(basePoint);

  // reset flag if needed
  for(std::deque<VertexID>::const_iterator i = result.begin(); i != result.end(); ++i)
    if ((*mesh).point(*i).getFlag() == 2)
      (*mesh).point(*i).setFlag(1);
#ifndef NDEBUG
    else {
      if ((*mesh).point(*i).getFlag() != 1) {
	(*mesh).tracePoint(*i);
      }
      assert((*mesh).point(*i).getFlag() == 1);
    }
#endif

  return result;

}

/** a junction in the neighbourhood of a boundary is a set of triangles, connected to this boundary, and taking part of the allready seen surface,
    that contains more than one peace of boundary on it. In other words, it is a site where two part of boundary are joining.
    Given a list of boundary points, this function return the triangles taking part in a junction, and return also the number of this junctions
*/
std::pair<std::deque<TriangleID>, unsigned int> MeshManipulator::getJunctionsSetFlags(int flagValue, const std::deque<VertexID> & bList) {
  std::deque<TriangleID> tList, result;


  // first compute triangle list
  for(std::deque<VertexID>::const_iterator i = bList.begin(); i != bList.end(); ++i)
    for(std::deque<TriangleID>::const_iterator t = (*mesh).point(*i).getTriangles().begin(); t != (*mesh).point(*i).getTriangles().end(); ++t)
      if (((*mesh).triangle(*t).getFlag() == 1) && ((*mesh).point((*mesh).triangle(*t).getP1()).getIsBoundary())
	  && ((*mesh).point((*mesh).triangle(*t).getP2()).getIsBoundary()) && ((*mesh).point((*mesh).triangle(*t).getP3()).getIsBoundary())) {
	(*mesh).triangle(*t).setFlag(flagValue);
	tList.push_front(*t);
      }
  // then compute connected components (using triangles)
  int newFlagValue = flagValue;
  for(std::deque<TriangleID>::const_iterator t = tList.begin(); t != tList.end(); ++t)
      if ((*mesh).triangle(*t).getFlag() == flagValue)
	computeCCUsingTriangles(*t, flagValue, ++newFlagValue);

  // then compute topology
  std::map<int, int> nbBorder;
  for(int i = flagValue + 1; i <= newFlagValue; ++i)
    nbBorder[i] = 0;
  for(std::deque<TriangleID>::const_iterator t = tList.begin(); t != tList.end(); ++t)
    for(unsigned int i = 0; i < 3; ++i) // no iterator available
      try {
	if ((!(*mesh).isBoundaryEdge((*mesh).triangle(*t).getVertexId(i), (*mesh).triangle(*t).getVertexId((i + 1) % 3))) &&
	    (*mesh).findOtherTriangle((*mesh).triangle(*t).getVertexId(i), (*mesh).triangle(*t).getVertexId((i + 1) % 3), *t).getFlag() == 0)
	  nbBorder[(*mesh).triangle(*t).getFlag()]++;
      }
      catch (Exception e) {
      }

  unsigned int nbJunctions = 0;
  for(int i = flagValue + 1; i <= newFlagValue; ++i)
    if (nbBorder[i] > 1)
      ++nbJunctions;

  for(std::deque<TriangleID>::const_iterator t = tList.begin(); t != tList.end(); ++t)
    if (nbBorder[(*mesh).triangle(*t).getFlag()] > 1)
      result.push_front(*t);
    else
      (*mesh).triangle(*t).setFlag(1);

  return std::pair<std::deque<TriangleID>, unsigned int>(result, nbJunctions);
}

void MeshManipulator::computeDistances(VertexID idCenter, std::deque<double> & distances) {
  distances.clear();
  computeDijkstra(idCenter);

  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    distances.push_back(length[i]);

}

void MeshManipulator::computeDistancesStickCuttings(VertexID idCenter, std::deque<double> & distances) {
  distances.clear();
  computeDijkstraStickCuttings(idCenter);

  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    distances.push_back(length[i]);

}


void MeshManipulator::computeSameLocationList() {
  nbSameLocation = (*mesh).getNbPoints();
  if (sameLocation != NULL)
    delete[] sameLocation;


  SpaceSpliter spliter(*mesh);
  sameLocation = new std::deque<VertexID>[(*mesh).getNbPoints()];

  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    spliter.getSameLocationPoints(i, sameLocation[i]);

}

VertexID MeshManipulator::findPointSameLocation(VertexID p) const {
  VertexID nbP = (*mesh).getNbPoints();
  Point3D & point = (*mesh).point(p);
  // find first point with same projection
  for(VertexID i = 1; i <= nbP; ++i) // uint better than iterator
    if ((nbP - i != p) && (point.sameLocation((*mesh).point(nbP - i))))
      return nbP - i;

  throw Exception(std::string("Other point with same location in the original mesh not found"));
}

std::deque<VertexID> MeshManipulator::getPointsSameLocation(VertexID p) const {
  std::deque<VertexID> result;
  Point3D & point = (*mesh).point(p);
  const Mesh::const_point_iterator endPoint = (*mesh).point_end();

  // find first point with same projection
  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != endPoint; ++i)
    if (Coord3D::sameLocation(point, *i))
      result.push_back((*i).getId());

  return result;
}


VertexID MeshManipulator::getNearestToBoundary(std::deque<VertexID> points) {
  std::deque<VertexID> bList;
  VertexID point = points[0];
  double lPoint;

  // first compute boundary list
  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getIsBoundary())
      bList.push_front((*i).getId());

  // then compute disjkstra form boundary
  (*mesh).setPointFlag(0);
  computeDijkstra(bList, 0);
  lPoint = length[point];

  // then compute nearest point
  for(std::deque<VertexID>::const_iterator p = points.begin();
      p != points.end(); ++p)
    if ((length[*p] < lPoint) && (length[*p] >= 0)) {
      lPoint = length[*p];
      point = *p;
    }

  return point;
}

std::deque<std::deque<VertexID> > MeshManipulator::splitPath(const std::deque<VertexID> & path) {
  std::deque<std::deque<VertexID> > pathes;
  std::deque<VertexID> currentPath;
  bool inside = false;

  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
    if ((*mesh).point(*p).getIsBoundary())
      if (inside) {
	inside = false;
	currentPath.push_back(*p);
	pathes.push_back(currentPath);
	currentPath.clear();
      }
      else {
	if (currentPath.size() == 0)
	  currentPath.push_back(*p);
	else {
	  if (!(*mesh).isBoundaryEdge(currentPath[0], *p)) { // an edge between two boundary points
	    currentPath.push_back(*p);
	    pathes.push_back(currentPath);
	    currentPath.clear();
	    currentPath.push_back(*p);
	  }
	  else
	    currentPath[0] = *p;
	}
      }
    else {
      inside = true;
      currentPath.push_back(*p);
    }

  }
  if (currentPath.size() > 1) {
    pathes.push_back(currentPath);
  }

  return pathes;
}


std::deque<VertexID> MeshManipulator::unstickPathFromBoundary(const std::deque<VertexID> & path, enum UnstickMethod method) {
  if (method == UMPreserveMesh)
    return unstickPathFromBoundaryPreserveMesh(path);
  else if (method == UMFast)
    return unstickPathFromBoundaryFast(path);
  else if (method == UMNoDijkstra)
    return unstickPathFromBoundaryNoDijkstra(path);
  else // if (method == UMNone)
    return path;
}

std::deque<VertexID> MeshManipulator::unstickPathFromBoundaryFast(const std::deque<VertexID> & path) {
  if (path.size() < 3)
    return path;

  VertexID pred_t = path.front();
  VertexID middle = path.front();
  for(std::deque<VertexID>::const_iterator next = path.begin(); next != path.end(); ++next) {
    if (pred_t != middle) {
      std::deque<VertexID> newIds = unstickPointFromBoundary(middle, pred_t, *next);
      if (newIds.size() != 0)
	pred_t = newIds.back();
      else
	pred_t = middle;
    }
    else
      pred_t = middle;
    middle = *next;
  }

  return computeShortestPathNoBoundary(path.front(), path.back());
}

std::deque<VertexID> MeshManipulator::unstickPathFromBoundaryNoDijkstra(const std::deque<VertexID> & path) {
  if (path.size() < 3)
    return path;
  std::vector<std::deque<VertexID> > newPaths;
  newPaths.push_back(std::deque<VertexID>());
  newPaths.back().push_back(path.front());

  VertexID pred_t = path.front();
  VertexID middle = path.front();
  for(std::deque<VertexID>::const_iterator next = path.begin(); next != path.end(); ++next) {
    if (pred_t != middle) {
      if ((*mesh).point(middle).hasNeighbour(pred_t) && (*mesh).point(*next).hasNeighbour(middle)) {
	std::deque<VertexID> newIds = unstickPointFromBoundary(middle, pred_t, *next);
	if (newIds.size() != 0) {
	  pred_t = newIds.back();
	  if (newPaths.back().size() != 0)
	    newPaths.push_back(std::deque<VertexID>());
	}
	else {
	  if ((*mesh).point(middle).getIsBoundary()) {
	    if (newPaths.back().size() != 0)
	      newPaths.push_back(std::deque<VertexID>());
	  }
	  else
	    newPaths.back().push_back(middle);
	  pred_t = middle;
	}
      }
      else {
	if (newPaths.back().size() != 0) {
	  newPaths.back().push_back(middle);
	  newPaths.push_back(std::deque<VertexID>());
	}
	else {
	  newPaths.back().push_back(middle);
	}
	pred_t = middle;
      }
    }
    else {
      pred_t = middle;
    }
    middle = *next;
  }
  if ((newPaths.back().size() == 0) || (newPaths.back().back() != path.back()))
    newPaths.back().push_back(path.back());

  if (newPaths.size() == 1)
    return newPaths.back();
  else {
    // we need to rebuild the missing elements without walking along the boundaries
    std::deque<VertexID> result = newPaths.front();
    MeshManipulator mManip(*mesh);
    for(std::vector<std::deque<VertexID> >::const_iterator p = newPaths.begin() + 1; p != newPaths.end(); ++p) {
      std::deque<VertexID> junction = computeShortestPathNoBoundary(result.back(), (*p).front());
      for(std::deque<VertexID>::const_iterator n = junction.begin() + 1; n != junction.end(); ++n)
	result.push_back(*n);
      for(std::deque<VertexID>::const_iterator n = (*p).begin() + 1; n != (*p).end(); ++n)
	result.push_back(*n);
    }
    return result;
  }
}

std::deque<VertexID> MeshManipulator::unstickPathFromBoundaryPreserveMesh(const std::deque<VertexID> & path) {
  if (path.size() >= 3) {
    std::deque<VertexID> finalPath;
    // first add point at the end of the path
    if ((*mesh).isBoundaryEdge(path.front(), path[1])) {
      unstickPointFromBoundary(path[1], path.front(), path[2]);
      VertexID otherBPoint = (*mesh).getNextBPoint(path.front(), path[1]);
      (*mesh).point(otherBPoint).setFlag(1);
    }
    else
      if ((*mesh).point(path.front()).getIsBoundary()) {
	VertexID bPoint1 = (*mesh).getNextBPoint(path.front());
	VertexID bPoint2 = (*mesh).getNextBPoint(path.front(), bPoint1);
	(*mesh).point(bPoint1).setFlag(1);
	(*mesh).point(bPoint2).setFlag(1);
      }

    // then add point at the end of the path
    VertexID nextToLast = *(++path.rbegin());
    VertexID nextToLast2 = *(++(++path.rbegin()));
    if ((*mesh).isBoundaryEdge(path.back(), nextToLast) && (nextToLast != path[1])) {
      unstickPointFromBoundary(nextToLast, path.back(), nextToLast2);
      VertexID otherBPoint = (*mesh).getNextBPoint(path.back(), nextToLast);
      (*mesh).point(otherBPoint).setFlag(1);
    }
    else
      if ((*mesh).point(path.back()).getIsBoundary()) {
	VertexID bPoint1 = (*mesh).getNextBPoint(path.back());
	VertexID bPoint2 = (*mesh).getNextBPoint(path.back(), bPoint1);
	(*mesh).point(bPoint1).setFlag(1);
	(*mesh).point(bPoint2).setFlag(1);
      }

    // we are computing first the path between this two points
    finalPath = computeShortestPathInCC(path.front(), path.back(), 0);
    assert(finalPath.size() > 2);

#ifndef NDEBUG
    assert(!(*mesh).isBoundaryEdge(finalPath[0], finalPath[1]));
    nextToLast = *(++finalPath.rbegin());
    assert(!(*mesh).isBoundaryEdge(finalPath.back(), nextToLast));
#endif

    while(true) {
      // find first point in boundary
      VertexID idPointPred = (*mesh).getNbPoints();
      VertexID idPoint = (*mesh).getNbPoints();
      VertexID idPointNext = (*mesh).getNbPoints();
      std::deque<VertexID>::const_iterator predpred = finalPath.begin();
      std::deque<VertexID>::const_iterator pred_t = finalPath.begin();
      for(std::deque<VertexID>::const_iterator i = ++(finalPath.begin()); i != finalPath.end(); ++i) {
	assert((*mesh).point(*pred_t).getFlag() == 0);
	if ((pred_t != finalPath.begin()) && (*mesh).point(*pred_t).getIsBoundary()) {
	  idPointPred = *predpred;
	  idPoint = *pred_t;
	  idPointNext = *i;
	  break;
	}
	predpred = pred_t;
	++pred_t;
      }
      // if a boundary edge has not been found, path is ok
      if (idPoint == (*mesh).getNbPoints()) {
	break;
      }

      // find last point in boundary
      std::deque<VertexID>::const_reverse_iterator rpredpred = finalPath.rbegin();
      std::deque<VertexID>::reverse_iterator rpred = finalPath.rbegin();
      for(std::deque<VertexID>::reverse_iterator i = ++(finalPath.rbegin()); i != finalPath.rend(); ++i) {
	assert((*mesh).point(*rpred).getFlag() == 0);
	if ((rpred != finalPath.rbegin()) && (*mesh).point(*rpred).getIsBoundary()) {
	  // a point is found, we are now watching for the point nearest to the extremity
	  double length1 = length[idPoint];
	  double length2 = length[finalPath.back()] - length[*pred];
	  assert(length1 > 0);
	  assert(length2 > 0);
	  if (length2 < length1) { // if the new one is nearest, it became the edge
	    idPointPred = *rpredpred;
	    idPoint = *rpred;
	    idPointNext = *i;
	  }
	  break;
	}
	rpredpred = rpred;
	++rpred;
      }

      assert(idPoint != finalPath.front());
      assert(idPoint != finalPath.back());

      std::deque<VertexID> listOfAddedPoints = unstickPointFromBoundary(idPoint, idPointPred, idPointNext);
      if (listOfAddedPoints.size() != 0) {
	finalPath = computeShortestPathUpdateLength(finalPath.front(), finalPath.back(), smallestLengthOfNeighbours(listOfAddedPoints));
      }
      else
	finalPath = computeShortestPathInCC(finalPath.front(), finalPath.back(), 0);


    }
    return finalPath;
  }
  else
    return path;
}

std::deque<VertexID> MeshManipulator::unstickPointFromBoundary(VertexID idPoint, VertexID idPointPred, VertexID idPointNext) {
  std::deque<VertexID> listOfAddedPoints;
  if (!(*mesh).point(idPoint).getIsBoundary())
    return listOfAddedPoints;

  // build edge list
  std::deque<VertexID> listOfNeighbours = (*mesh).getNeighboursBetweenPoints(idPoint, idPointPred, idPointNext);

  // if there is no edge between the two path edges
  if (listOfNeighbours.size() == 0) {
    (*mesh).point(idPoint).setFlag(1);
  }
  else {
    // add a point on the selected edge and set flag for the forbidden point

    for(std::deque<VertexID>::const_iterator i = listOfNeighbours.begin(); i != listOfNeighbours.end(); ++i) {
      VertexID idNewPoint = (*mesh).addPointOnEdge(idPoint, *i);
      (*mesh).point(idNewPoint).setFlag(0);
      listOfAddedPoints.push_back(idNewPoint);
    }
    (*mesh).point(idPoint).setFlag(1);

  }

  return listOfAddedPoints;
}


double MeshManipulator::smallestLengthOfNeighbours(VertexID point) const {
  if (length == NULL)
    throw Exception("smallestLengthOfNeighbours(1): length has not be computed before");
  double result = std::numeric_limits<double>::max();
  for(std::deque<VertexID>::const_iterator it = (*mesh).point(point).getNeighbours().begin();
      it != (*mesh).point(point).getNeighbours().end(); ++it) {
    if ((*it < nbPoints) && ((*mesh).point(*it).getFlag() == 0) && (result > length[*it]))
      result = length[*it];
  }

  return result;
}

double MeshManipulator::smallestLengthOfNeighbours(const std::deque<VertexID> & points) const {
  double resultLength = std::numeric_limits<double>::max();

  for(std::deque<VertexID>::const_iterator i = points.begin(); i != points.end(); ++i) {
    double localLength = smallestLengthOfNeighbours(*i);
    if (localLength < resultLength)
      resultLength = localLength;
  }

  return resultLength;
}


std::vector<VertexID> MeshManipulator::getPointsOrderedByLength(VertexID firstPoint) {
  lengthPathBP lpath;

  computeDijkstra(firstPoint);

  lpath.length = length;
  lpath.mesh = mesh;


  std::vector<VertexID> sList;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    if (length[i] != -1) {
      sList.push_back(i);
    }
  sort(sList.begin(), sList.end(), lpath);

  return sList;
}


std::vector<VertexID> MeshManipulator::getPointsOrderedByLength(VertexID firstPoint, double maxLength) {
  lengthPathBP lpath;

  computeDijkstraBound(firstPoint, *lengthEdgeMethod, maxLength);

  lpath.length = length;
  lpath.mesh = mesh;


  std::vector<VertexID> sList;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    if (length[i] != -1) {
      sList.push_back(i);
    }
  sort(sList.begin(), sList.end(), lpath);

  return sList;
}


double MeshManipulator::getGeodesicDistance(VertexID p1, VertexID p2) {
  computeDijkstra(p1);
  return length[p2];
}

double MeshManipulator::getHausdorffDistance(const std::vector<VertexID> & ps1, const std::vector<VertexID> & ps2) {
  double result = 0.;
  // first side
  std::deque<VertexID> dps1(ps1.begin(), ps1.end());
  std::deque<VertexID> dps2(ps2.begin(), ps2.end());
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  computeDijkstra(dps1.begin(), dps1.end(), 0);
  for (std::deque<VertexID>::const_iterator p = dps2.begin(); p != dps2.end(); ++p)
    if (length[*p] >= 0.) {
      if (length[*p] >= result) {
	result = length[*p];
      }
    }
    else
      result = -1.; // -1 means infty


  computeDijkstra(dps2.begin(), dps2.end(), 0);
  for (std::deque<VertexID>::const_iterator p = dps1.begin(); p != dps1.end(); ++p)
    if (length[*p] >= 0.) {
      if (length[*p] >= result) {
	result = length[*p];
      }
    }
    else
      result = -1.; // -1 means infty

  return result;
}


double MeshManipulator::getGeodesicDistance(const std::vector<VertexID> & ps1, const std::vector<VertexID> & ps2) {
  std::deque<VertexID> targets(ps2.begin(), ps2.end());
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  computeDijkstraTarget(ps1.begin(), ps1.end(), 0, targets);
  for (std::vector<VertexID>::const_iterator p = ps2.begin(); p != ps2.end(); ++p)
    if (length[*p] >= 0.)
      return length[*p];
  throw Exception("getGeodesicDistance(2): cannot compute distance between this set of points.");
}

std::vector<VertexID> MeshManipulator::getBoundaryFromBPoint(VertexID bPoint) {
  if (!(*mesh).point(bPoint).getIsBoundary())
    throw Exception("getBoundaryFromBPoint(1): the given point is not a boundary point");

  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  (*mesh).setBoundaryPointFlag(1);

  std::deque<VertexID> r = computeBoundaryCCList(bPoint, 1, 2, 0);
  return std::vector<VertexID>(r.begin(), r.end());
}

std::vector<std::vector<VertexID> > MeshManipulator::getBoundaries() {
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);
  (*mesh).setBoundaryPointFlag(1);

  int nextFlagValue = 2;
  // then set flags for boundaries not connected
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 1)
      computeBoundaryCC((*p).getId(), 1, nextFlagValue++, 0);


  // build real boundaries
  std::vector<std::vector<VertexID> > bdries;
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() != 0) {
      int flag = (*p).getFlag();
      bdries.push_back(std::vector<VertexID>());
      std::vector<VertexID> & bdry = bdries.back();
      for(Mesh::point_iterator j = p; j != (*mesh).point_end(); ++j) {
	if ((*j).getFlag() == flag) {
	  bdry.push_back((*j).getId());
	  (*j).setFlag(0);
	}
      }
    }

  return bdries;
}


std::vector<std::vector<VertexID> > MeshManipulator::getCutLocusPaths(VertexID point) {
  // init flags
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  // compute length from the starting point
  computeDijkstra(point, 0);

  // build an ordered list of points
  lengthPathBP lsort;
  lsort.length = length;
  lsort.mesh = mesh;
  std::vector<VertexID> sList;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    sList.push_back(i);
  sort(sList.begin(), sList.end(), lsort);

  // wavefront growing
  WaveFront wf(*mesh);

  wf.growSurfaceFull(sList);

  wf.removeTreeParts();

  // then return paths that takes part into the border of the wavefront
  return wf.getBorderPaths();
}


VertexID MeshManipulator::getOppositePoint(VertexID p) {
  VertexID result = p;
  double d = 0.;

  computeDijkstra(p);

  double * l = length;
  for(VertexID i = 0; i != (*mesh).getNbPoints(); ++i, ++l)
    if (*l > d) {
      d = *l;
      result = i;
    }

  return result;
}


const Triangle & MeshManipulator::findTriangleOnBorderSameCC(TriangleID idP, int iFlag, int fFlag) {
  TriangleID result = 0;

  if ((*mesh).triangle(idP).getFlag() != iFlag)
    throw Exception("findTriangleOnBorderSameCC(): no triangle inside region");

  std::deque<TriangleID> open;
  open.push_back(idP);
  (*mesh).triangle(idP).setFlag(fFlag);

  while(open.size() != 0) {
    TriangleID next = open.front();
    open.pop_front();
    ++result;

    for(unsigned int i = 0; i != 3; ++i) {
      unsigned int otheri = i == 2 ? 0 : i + 1;
      try {
	Triangle & tt = (*mesh).findOtherTriangle(i, otheri, next);
	if (tt.getFlag() == iFlag) {
	  open.push_back(tt.getId());
	  tt.setFlag(fFlag);
	}
	else {
	  computeTCC(idP, iFlag, fFlag);
	  return (*mesh).triangle(next);
	}
      }
      catch (...) {}
    }
  }

  throw Exception("findTriangleOnBorderSameCC(): No boundary triangle");

}

TriangleID MeshManipulator::computeTCC(TriangleID idP, int iFlag, int fFlag) {
  TriangleID result = 0;

  if ((*mesh).triangle(idP).getFlag() != iFlag)
    return 0;

  std::deque<TriangleID> open;
  open.push_back(idP);
  (*mesh).triangle(idP).setFlag(fFlag);

  while(open.size() != 0) {
    TriangleID next = open.front();
    open.pop_front();
    ++result;

    for(unsigned int i = 0; i != 3; ++i) {
      unsigned int otheri = i == 2 ? 0 : i + 1;
      try {
	Triangle & tt = (*mesh).findOtherTriangle(i, otheri, next);
	if (tt.getFlag() == iFlag) {
	  open.push_back(tt.getId());
	  tt.setFlag(fFlag);
	}
      }
      catch (...) {}
    }
  }

  return result;
}

