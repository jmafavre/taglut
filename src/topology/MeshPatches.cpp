
/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include<algorithm>
#include "MeshPatches.h"
#include "MeshPart.h"

using namespace Taglut;

namespace Taglut {
  /**
     @class CompSecond
     A comparator for paths with length
  */
  class CompSecond {
  public:
    /**
       Comparator of the second element of the pair
    */
    bool operator() (const std::pair<std::deque<VertexID>, double> & b1,
		     const std::pair<std::deque<VertexID>, double> & b2) const {
      return b1.second < b2.second;
    }

  };
}

MeshPatches::MeshPatches(Mesh & mesh_l, const LengthEdge & lMethod, float maxLength_l) : MeshPathes(mesh_l, lMethod), maxLength(maxLength_l) {
  ok = false;
}

MeshPatches::MeshPatches(Mesh & mesh_l, float maxLength_l) : MeshPathes(mesh_l), maxLength(maxLength_l) {
  ok = false;
}


void MeshPatches::addBPath(const std::deque<VertexID> & path) {
  // adding path to the list
  allPathes.push_back(path);

  // add patch using path
  addPatch(path);

  // log pathes if needed
  logPathes(allPathes);
}

void MeshPatches::addCPath(const std::deque<VertexID> & path) {
  MeshManipulator mManip(cMesh);

  std::deque<std::deque<VertexID> > pathes = mManip.splitPath(path);

  if ((pathes.size() != 1) || (cMesh.point(pathes.front().front()).getIsBoundary() && cMesh.point(pathes.front().back()).getIsBoundary())) {
    for(std::deque<std::deque<VertexID> >::iterator p = pathes.begin(); p != pathes.end(); ++p)
      if ((*p).size() > 1) {
	allPathes.push_back(*p);
	correctingPath(*p);
	addPath(*p);
      }
  }
  else {

    allPathes.push_back(path);
    std::pair<std::deque<VertexID>, std::deque<VertexID> > newPathes = addPathGetExactPathes(path);

    addPatch(newPathes.first);
    addPatch(newPathes.second);
  }

  logPathes(allPathes);
}


int MeshPatches::addPatchesInBoundaries(int maxPatch, const std::string & logMesh, const IndentManager & iManager) {
  clock_t tstart = clock();
  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);

  if (maxPatch == 0) {
    std::cout << iManager << " Break" << std::endl;
    return 0;
  }

  // first build a list of the boundaries, with the length
  std::vector<std::pair<std::deque<VertexID>, double> > boundaries;

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getIsBoundary() && ((*i).getFlag() == 0)) {
      std::deque<VertexID> boundary = computeBoundaryCCList((*i).getId(), 0, 1, 0);
      double bLength = (*mesh).getLength(boundary, *lengthEdgeMethod);
      std::cout << iManager <<  "  (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 1st step) boundary found" << std::endl;
      boundaries.push_back(std::pair<std::deque<VertexID>, double>(boundary, bLength));
    }

  std::sort(boundaries.begin(), boundaries.end(), CompSecond());

  for(std::vector<std::pair<std::deque<VertexID>, double> >::const_iterator b = boundaries.begin(); b != boundaries.end(); ++b) {
    if (maxPatch == 0) {
      std::cout << iManager << " Break" << std::endl;
      break;
    }
    if ((maxLength > 0) && ((*b).second >= maxLength)) {
      std::cout << iManager << " Break (boundary too long)" << std::endl;
      break;
    }
    --maxPatch;
    addBPath((*b).first);
    if (logMesh != "")
      logCMesh(logMesh);
  }

  clock_t tend = clock();
  std::cout << iManager << " Patching duration (1st step): " << (tend - tstart) / ((double) CLOCKS_PER_SEC) << "s" << std::endl;

  return maxPatch;
}


void MeshPatches::addPatchesInShortestCyclesPseudoVolumic(const UCharCImg & mask,
							  const UCharCImg & region,
							  bool inside,
							  int maxPatch,
							  const std::string & logMesh, const IndentManager & iManager) {
  clock_t tstart = clock();

  LengthEdge * lEdgeCMesh;
  MeshMap * mMap = NULL;
  if ((*lengthEdgeMethod).needMeshMap()) {
    if (mMap != NULL)
      delete mMap;
    mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
    lEdgeCMesh = (*lengthEdgeMethod).cloneMeshMap(*mMap);
  }
  else
    lEdgeCMesh = (*lengthEdgeMethod).clone(cMesh);

  LengthEdgeMergeBoundary lEdgeCMeshMerge(*lEdgeCMesh);

  // initialisation of topological properties
  MeshPart mpart(cMesh, cMesh.point(0));
  nbBoundaries = mpart.getNbBoundaries();
  fGroupSize = mpart.getBettiNumber();


  while (fGroupSize > nbBoundaries + 1) {
    if (maxPatch == 0) {
      std::cout << iManager << " Break (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 2nd step). nbBoundaries: " << nbBoundaries << ", fGroupSize: " << fGroupSize << std::endl;
      return;
    }

    std::cout << iManager << "  (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 2nd step) nbBoundaries: " << nbBoundaries << ", fGroupSize: " << fGroupSize << std::endl;
    std::deque<VertexID> path;
    try {
      path = getShortestNonSeparatingCyclePseudoVolumic(0., mask, region, inside);
    }
    catch (...) {
      std::cout << "Warning: cannot find cycle using pseudo-volumic approach. Back to the surface-based approach." << std::endl;
      path = getShortestNonSeparatingCycle(0.0, MeshPathes::OECbyStripAndPredCut);
    }

    double pLength = cMesh.getLength(path, lEdgeCMeshMerge);
    if ((maxLength < 0) || (pLength <= maxLength)) {
      addCPath(path);
      if (maxPatch >= 0)
        --maxPatch;
      if ((*lengthEdgeMethod).needMeshMap()) {
	if (mMap != NULL)
	  delete mMap;
	mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
	lEdgeCMesh = (*lengthEdgeMethod).cloneMeshMap(*mMap);
      }
      else
	lEdgeCMesh = (*lengthEdgeMethod).clone(cMesh);

    }
    else {
      std::cout << iManager << " Break (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 2nd step). path too long: " << pLength << " (maxLength: " << maxLength << ")" << std::endl;
      break;
    }
   // compute new topological properties
    MeshPart cmpart(cMesh, cMesh.point(0));

    if (logMesh != "")
      logCMesh(logMesh);

    nbBoundaries = cmpart.getNbBoundaries();
    fGroupSize = cmpart.getBettiNumber();

  }

  if (mMap != NULL)
    delete mMap;
  delete lEdgeCMesh;
  clock_t tend = clock();
  std::cout << iManager << " Patching duration (1st and 2nd step): " << (tend - tstart) / ((double) CLOCKS_PER_SEC) << "s" << std::endl;
}

void MeshPatches::addPatchesInShortestCycles(int maxPatch, enum OptimEC optimMethod, const std::string & logMesh, const IndentManager & iManager) {
  clock_t tstart = clock();

  LengthEdge * lEdgeCMesh;
  MeshMap * mMap = NULL;
  if ((*lengthEdgeMethod).needMeshMap()) {
    if (mMap != NULL)
      delete mMap;
    mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
    lEdgeCMesh = (*lengthEdgeMethod).cloneMeshMap(*mMap);
  }
  else
    lEdgeCMesh = (*lengthEdgeMethod).clone(cMesh);

  LengthEdgeMergeBoundary lEdgeCMeshMerge(*lEdgeCMesh);

  // initialisation of topological properties
  MeshPart mpart(cMesh, cMesh.point(0));
  nbBoundaries = mpart.getNbBoundaries();
  fGroupSize = mpart.getBettiNumber();


  while (fGroupSize > nbBoundaries + 1) {
    if (maxPatch == 0) {
      std::cout << iManager << " Break (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 2nd step). nbBoundaries: " << nbBoundaries << ", fGroupSize: " << fGroupSize << std::endl;
      return;
    }

    std::cout << iManager << "  (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 2nd step) nbBoundaries: " << nbBoundaries << ", fGroupSize: " << fGroupSize << std::endl;
    std::deque<VertexID> path = getShortestNonSeparatingCycle(0.0, optimMethod);
    double pLength = cMesh.getLength(path, lEdgeCMeshMerge);
    if ((maxLength < 0) || (pLength <= maxLength)) {
      addCPath(path);
      if (maxPatch >= 0)
        --maxPatch;
      if ((*lengthEdgeMethod).needMeshMap()) {
	if (mMap != NULL)
	  delete mMap;
	mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
	lEdgeCMesh = (*lengthEdgeMethod).cloneMeshMap(*mMap);
      }
      else
	lEdgeCMesh = (*lengthEdgeMethod).clone(cMesh);

    }
    else {
      std::cout << iManager << " Break (" << ((clock() - tstart)) / ((double) CLOCKS_PER_SEC) << " seconds, 2nd step). path too long: " << pLength << " (maxLength: " << maxLength << ")" << std::endl;
      break;
    }
   // compute new topological properties
    MeshPart cmpart(cMesh, cMesh.point(0));

    if (logMesh != "")
      logCMesh(logMesh);

    nbBoundaries = cmpart.getNbBoundaries();
    fGroupSize = cmpart.getBettiNumber();

  }

  if (mMap != NULL)
    delete mMap;
  delete lEdgeCMesh;
  clock_t tend = clock();
  std::cout << iManager << " Patching duration (1st and 2nd step): " << (tend - tstart) / ((double) CLOCKS_PER_SEC) << "s" << std::endl;
}

void MeshPatches::addPatch(const std::deque<VertexID> & path) {
  std::deque<Triangle> tList;
  Point3D b(0, 0, 0);
  b.setIsRealPoint(false); // patch center is not a real point
  VertexID  bId = cMesh.getNbPoints();

  if (path.size() <= 2)
    return;

  // compute barycenter and build triangle list
  VertexID lastId = path.back();
  for(std::deque<VertexID>::const_iterator i = path.begin(); i != path.end(); ++i) {
    if (cMesh.point(*i).hasNeighbour(lastId) && ((i != path.begin()) || (path.front() != path.back()))) {
      b(0) += cMesh.point(*i)(0);
      b(1) += cMesh.point(*i)(1);
      b(2) += cMesh.point(*i)(2);
      // check the orientation of the other triangle containing *i and lastId
      try {
	Triangle othert = cMesh.findTriangle(*i, lastId);
	if (othert.sameOrder(*i, lastId))
	  tList.push_front(Triangle(bId, lastId, *i));
	else
	  tList.push_front(Triangle(bId, *i, lastId));
      }
      catch (Exception e) {
	// if no triangle found, the order is the standard order
	tList.push_front(Triangle(bId, *i, lastId));
      }
    }
    lastId = *i;
  }

  if (tList.size() == 0)
    throw Exception("addPatch: empty triangle list.");
  b /= tList.size();
#ifdef NDEBUG
  cMesh.addPoint(b);
#else
  assert(bId == cMesh.addPoint(b));
#endif

  cMesh.addTriangles(tList);

}

void MeshPatches::patchMesh(int maxPatch, enum OptimEC optimMethod, const std::string & inputPathFile, const std::string & logMesh, const IndentManager & iManager) {
  int newMaxPatch = maxPatch;
  if (!ok) {

    // check for connected components
    std::deque<MeshPart> cc = cMesh.getConnectedComponents();
    if (cc.size() != 1)
      throw ExceptionTopology("MeshPatches: connected component number != 1");

    // if needed, load pathes from file and add it
    if (inputPathFile != "")
      fromFile(inputPathFile, iManager);

    // first add patches in boundaries
    newMaxPatch = addPatchesInBoundaries(newMaxPatch, logMesh, iManager);

    // then add patches in shortest cycles
    if (newMaxPatch != 0)
      addPatchesInShortestCycles(newMaxPatch, optimMethod, logMesh, iManager);

    ok = true;

    (*mesh) = cMesh;
  }

}

void MeshPatches::patchMeshPseudoVolumic(const UCharCImg & mask,
					 const UCharCImg & region,
					 bool inside,
					 int maxPatch,
					 const std::string & inputPathFile,
					 const std::string & logMesh,
					 const Taglut::IndentManager & iManager) {

  int newMaxPatch = maxPatch;
  if (!ok) {

    // check for connected components
    std::deque<MeshPart> cc = cMesh.getConnectedComponents();
    if (cc.size() != 1)
      throw ExceptionTopology("MeshPatches: connected component number != 1");

    // if needed, load pathes from file and add it
    if (inputPathFile != "")
      fromFile(inputPathFile, iManager);

    // first add patches in boundaries
    newMaxPatch = addPatchesInBoundaries(newMaxPatch, logMesh, iManager);

    // then add patches in shortest cycles
    if (newMaxPatch != 0)
      addPatchesInShortestCyclesPseudoVolumic(mask, region, inside, newMaxPatch, logMesh, iManager);

    ok = true;

    (*mesh) = cMesh;
  }

}

void MeshPatches::patchMeshInLoops(int maxPatch, enum OptimEC optimMethod, const std::string & inputPathFile, const std::string & logMesh, const IndentManager & iManager) {
  if (!ok) {

    // check for connected components
    std::deque<MeshPart> cc = cMesh.getConnectedComponents();
    if (cc.size() != 1)
      throw ExceptionTopology("MeshPatches: connected component number != 1");

    // if needed, load pathes from file and add it
    if (inputPathFile != "")
      fromFile(inputPathFile, iManager);

    addPatchesInShortestCycles(maxPatch, optimMethod, logMesh, iManager);

    ok = true;

    (*mesh) = cMesh;
  }

}


void MeshPatches::fromFile(const std::string & filename, const IndentManager & iManager) {
  std::cout << iManager << " Loading patches from file." << std::endl;
  // then load pathes from file
  std::deque<std::deque<VertexID> > pathes = MeshPathes::loadFromFile(filename);

  // then for each path
  for(std::deque<std::deque<VertexID> >::const_iterator path = pathes.begin(); path != pathes.end(); ++path) {
    // first check if path is inside mesh
    std::deque<VertexID>::const_iterator lasti = (*path).begin();
    for(std::deque<VertexID>::const_iterator i = (*path).begin(); i != (*path).end(); ++i) {
      if (*i >= cMesh.getNbPoints())
	throw Exception(std::string("Path cannot be applied in current mesh (wrong point id)."));
      if (lasti != i)
	if(!cMesh.point(*i).hasNeighbour(*lasti))
	  throw Exception(std::string("Path cannot be applied in current mesh (consecutive points are not neighbours)."));
      lasti = i;
    }

    // then add it to the mesh

    if((*path).size() != 0) {
      if (cMesh.isBoundaryCycle(*path)) {
	addBPath(*path);
	std::cout << iManager << "  Adding patch in boundary." << std::endl;
      }
      else {
	addCPath(*path);
	std::cout << iManager << "  Adding patches after cut." << std::endl;
      }
    }
  }

  std::cout << iManager << "  " << pathes.size() << " pathes loaded." << std::endl;
  std::cout << iManager << " Restarting patch algorithm." << std::endl;

}

void MeshPatches::closeBoundaries(int maxPatch, const std::string & logMesh, const IndentManager & iManager) {
  if (!ok) {
    addPatchesInBoundaries(maxPatch, logMesh, iManager);

    (*mesh) = cMesh;
    ok = true;
  }
}
