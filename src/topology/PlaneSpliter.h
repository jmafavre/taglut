/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef PLANE_SPLITER
#define PLANE_SPLITER

#include <vector>
#include <deque>
#include "IDTypes.h"
#include "Coord3D.h"
#include "Point3D.h"
#include "Point2D.h"
#include "Mapping2D3D.h"

namespace Taglut {
  class Mesh;

  /**
     @class PlaneSpliter

     @author Jean-Marie Favreau
     @brief Tool for point manipulation using geometrical localization
  */
  class PlaneSpliter : public Box2D {
  private:
    const std::deque<Point2D> & pointList;
    const std::vector<Triangle> & triangleList;
    double mX;
    double mY;
    PlaneSpliter ** children;
    std::vector<VertexID> * points;
    std::vector<TriangleID> * triangles;
    bool initialised;

    /**
       Add point inside structure
    */
    void addPoint(VertexID id);

    /**
       Add triangle inside structure
    */
    void addTriangle(const Triangle2D & t, TriangleID id);

    /**
       Init points and triangles on structure
    */
    void initPointsAndTriangles();

    /**
       Create structure
    */
    void createStructure(unsigned int depth);

    /**
       Compute min and max values
    */
    void computeMinMax();

    /**
       init initialised flag
    */
    void isInitialised(bool value);

  public:


    /**
       Default constructor with point list
    */
    PlaneSpliter(const Mapping2D3D & mapping);

    /**
       Constructor with point list and partial part of the plane

    */
    PlaneSpliter(unsigned int depth, const std::deque<Point2D> & points, const std::vector<Triangle> & triangles,
		 double minX, double minY, double maxX, double maxY);


    /**
       Destructor
    */
    ~PlaneSpliter();


    /**
       Find all triangles intersecting the ray defined by \p point
       @param point 2D coords of the ray
       @return list of triangles
    */
    std::vector<TriangleID> findTrianglesInRay(const Coord2D & point);

    /**
       Get number of triangles intersecting the ray defined by \p point
       @param point 2D coords of the ray
       @return number of triangles
    */
    unsigned int getNbTrianglesInRay(const Coord2D & point);
  };
}

#endif
