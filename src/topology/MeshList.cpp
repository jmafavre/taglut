/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMap.h"
#include "MeshList.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "Mapping2D3DList.h"
#include "FileTools.h"
#include "FileExceptions.h"

using namespace Taglut;

void MeshList::addFormats() {
  addFormat("wrl");
  addFormat("vrml");
  addFormat("obj");
  addFormat("ply");
  addFormat("off");
  addFormat("vtk");
  addLoadFormat("xyz");
  addLoadFormat("asc");
  addLoadFormat("dcm");
  addLoadFormat("hdr");
}

MeshList::MeshList() {
  addFormats();
}

MeshList::MeshList(const std::string & filename, const std::string & objectName) {
  addFormats();
  load(filename, objectName);
}

MeshList::MeshList(const char * filename, const char * objectName) {
  addFormats();
  if (filename != NULL) {
    if (objectName == NULL)
      load(filename);
    else
      load(filename, objectName);
  }
}


MeshList & MeshList::operator=(Mapping2D3DList & mappings) {
  unsigned int nb = size();
  for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping)
    push_back(*mapping);

  for(unsigned int i = 0; i < nb; ++i)
    pop_front();

  return *this;
}

MeshList & MeshList::operator=(const Mesh & mesh) {
  clear();
  push_back(mesh);

  return *this;
}

MeshList & MeshList::operator=(const MeshPart & meshPart) {
  Mesh mesh(meshPart);
  *this = mesh;

  return *this;
}


VertexID MeshList::getNbPoints() const {
  VertexID result = 0;

  for(MeshList::const_iterator m = begin(); m != end(); ++m)
    result += (*m).getNbPoints();

  return result;
}

unsigned long int MeshList::getNbCC() {
  unsigned long int result = 0;

  for(MeshList::iterator m = begin(); m != end(); ++m)
    result += (*m).getNbCC();

  return result;
}

TriangleID MeshList::getNbTriangles() const {
  TriangleID result = 0;

  for(MeshList::const_iterator m = begin(); m != end(); ++m)
    result += (*m).getNbTriangles();

  return result;
}

unsigned long int MeshList::getNbPointFlagValues() const {
  unsigned long int result = 0;

  for(MeshList::const_iterator m = begin(); m != end(); ++m)
    result += (*m).getNbPointFlagValues();

  return result;
}

bool MeshList::isEmpty() const {
  for(MeshList::const_iterator m = begin(); m != end(); ++m)
    if (!(*m).isEmpty())
      return false;

  return true;
}

std::deque<MeshPart> MeshList::getConnectedComponents() {
  std::deque<MeshPart> result;

  for(MeshList::iterator m = begin(); m != end(); ++m) {
    std::deque<MeshPart> mCC = (*m).getConnectedComponents();
    for(std::deque<MeshPart>::iterator cc = mCC.begin(); cc != mCC.end(); ++cc)
      result.push_back(*cc);
  }

  return result;
}

MeshPart MeshList::getLargestCC() {
  std::deque<MeshPart>::const_iterator ccSelected;
  unsigned int size_t = 0;
  std::deque<MeshPart> cc = getConnectedComponents();
  for(std::deque<MeshPart>::const_iterator cci = cc.begin(); cci != cc.end(); ++cci)
    if (size_t < (*cci).getNbPoints()) {
      size_t = (*cci).getNbPoints();
      ccSelected = cci;
    }

  return *ccSelected;
}

Mesh MeshList::getMesh() const {
  return Mesh(*this);
}

void MeshList::merge() {
  *this = getMesh();
}

bool MeshList::splitCC() {
  bool result = false;
  std::deque<std::deque<MeshPart> > ccs;

  for(MeshList::iterator m = begin(); m != end(); ++m)
    ccs.push_back((*m).getConnectedComponents());

  unsigned int id = 0;
  for(std::deque<std::deque<MeshPart> >::const_iterator mCC = ccs.begin(); mCC != ccs.end(); ++mCC) {
    if ((*mCC).size() != 1) {
      result = true;
      for(std::deque<MeshPart>::const_iterator cc = (*mCC).begin(); cc != (*mCC).end(); ++cc) {
	if (cc != (*mCC).begin())
	  push_back(*cc);
      }
      (*this)[id] = (*mCC)[0];
    }
    ++id;
  }

  return result;
}

void MeshList::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string & object) {
  if ((altFormat == "vrml") || (altFormat == "wrl"))
    loadVRML(fileName, object);
  else {
    try {
      Mesh mesh;
      mesh.loadAltFormat(fileName, altFormat, object);
      clear();
      push_back(mesh);
    }
    catch (Exception e) {
      throw e;
    }
  }

}

void MeshList::saveAltFormat(const std::string & fileName, const std::string & format, const std::string &) const {
  if ((format == "vrml") || (format == "wrl"))
    saveVRML(fileName);
  else
    throw Exception("Unknow format");
}



void MeshList::saveVRML(const std::string & filename) const {
  std::string basename = FileTools::basename(filename);
  std::ofstream outfile(filename.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << getHeaderVRML();

  unsigned int i = 0;

  for(MeshList::const_iterator m = begin(); m != end(); ++m) {
    outfile << (*m).toStringVRML(basename, i);
    ++i;
  }

  outfile << getMiddleVRML();
  outfile << getTransformationsVRML(basename, size());
  outfile << getFooterVRML();
  outfile.close();
}

bool MeshList::findMeshBlockVRML(std::ifstream & infile) const {

  std::string buf;

  /* find definition of "objectName" */
  while(!infile.eof()) {
    infile >> buf;
    if (buf == "Separator")
      return true; /* if found, return true */
  }

  return false; /* if not found in all file, return false */
}

void MeshList::loadVRML(const std::string & fileName, const std::string & objectName) {
  clear();
  if (objectName != "")
    push_front(Mesh(fileName, objectName));
  else {
    std::ifstream infile(fileName.c_str());

    if (!infile.is_open())
      throw ExceptionFileNotFound();

    if (!checkFormatVRML(infile))
      throw ExceptionUnknownFormat();

    if (!findMeshBlockVRML(infile))
      throw ExceptionErrorDuringReadingFile();

    do {
      push_back(Mesh());
    } while(back().loadNextMeshVRML(infile, true));
    pop_back();

    infile.close();
  }

  if (size() == 0)
    std::cout << "Warning: empty file." << std::endl;

}

