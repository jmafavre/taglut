/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESHCUT
#define MESHCUT

#include "Point3D.h"
#include "MeshPathes.h"
#include "IndentManager.h"
#include "SimulatedAnnealingNLoop.h"

#include <map>

namespace Taglut {

  class Mesh;

  /**
     @class MeshCut
     Mesh manipulator using path cuts
  */
  class MeshCut : public MeshPathes {
  public:

    /**
       Methods available to merge points
    */
    enum MergeMPointsMethod { MMPnone, MMPsimple, MMPonePoint };

    /**
       Axes available for PCA method
    */
    enum PCAAxis { PCAFirst, PCASecond, PCAThird };

    /**
       Available cutting methods
    */
    enum CuttingMethod { CMErickson, CMEricksonPlus, CMOld };

    /**
       Available quad methods
    */
    enum QuadMethod { QMSimple, QMABF, QMFloater };

  private:


    MeshManipulator cMeshManip;


    /**
       Add shortest non trivial cycle
    */
    double addShortestNontrivialCycle(enum OptimEC optimMethod, double minLength = 0.0, enum MergeMPointsMethod mergeMPoints = MMPnone, enum UnstickMethod uMethod = UMPreserveMesh);

    /**
       Cut sphere
    */
    void buildSphereCut();

    /**
       Add shortest path between nearest boundaries
    */
    void joinNearestBoundaries(enum MergeMPointsMethod mergeMPoints = MMPnone, enum UnstickMethod uMethod = UMPreserveMesh);


    /**
       Add shortest pathes between boundaries, as a cutting tree
    */
    void joinBoundaries(int nbCutMax, enum CuttingMethod cMethod, enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod, const Taglut::IndentManager iManager, const clock_t & tstart);


    /**
       Cutting the mesh using "Y" nodes: each neighbour of center
       which are boundary but was not boundary before is used as
       a start of loop cutting the mesh in two connected components
       @param center center of "Y"
       @param boundaries boolean value
    */
    void buildCutY(VertexID center, const std::deque<VertexID> & iBList);

    /**
       Get point with same location as nbInitial, in neighbourood of centers.
       If not found, throws exception.
    */
    VertexID getNbSameLocation(VertexID nbInitial, const std::deque<VertexID> & centers);

    /**
       Load pathes from file and build corresponding cuts
    */
    void fromFile(const std::string & filename, const Taglut::IndentManager & iManager);

    /**
       Compute point and triangle flags using given min curvature
       @param alpha min curvature
       @return MeshPart with triangles and points selected (flag = 1)
    */
    MeshPart computeFlagUsingCurvature(double alpha);


    /**
       Compute boundaries inside part flagged "1"
       @return list of boundaries
    */
    std::deque<std::deque<VertexID> > computeBoundariesInside();

    /** Compute list of boundaries of cMesh */
    std::deque<std::deque<VertexID> > getBoundariesCMesh();

    /**
       Given a point, return true if this point is a multipoint
    */
    bool isMultiPoint(VertexID point) const;

    /**
       Given a point and another boundary point, return a point with same location
       as the first point, in the same boundary of the second point
    */
    VertexID getPointSameLocationInBoundary(VertexID point, VertexID otherInB);

    /**
       List of multiple points
    */
    std::deque<VertexID> multiPoints;

    /**
       Compute multi points (cutting intersections)
       @param force Force computation of all multipoints
    */
    void computeMultiPoints(bool force = false);

    /**
       Given a point, compute list of cutting points on the same connected component.
       Points are ordered using the boundary order.
    */
    std::deque<VertexID> computeNMultiPoints(VertexID point);

    /**
       Given a list of multipoints, compute the best cut (the shortest one) between
       two points of the list.
    */
    std::deque<VertexID> findBestCut(const std::deque<VertexID> & multiPointsInside);


    /**
       Set flags of boundary neighbours if idP point has other neighbours not in boundary
    */
    void setFlagOfBoundaryNeighbours(VertexID idP, int flag);

    /**
       Given a point and a list of points, compute possible extremities for path
    */
    static std::deque<VertexID> computePossibleExtremities(VertexID idPoint, const std::deque<VertexID> & allPoints);


    /**
       Return a string (.4ms format)
    */
    std::string toString4MS();

    /**
       Return true if the given point is a fictive point
    */
    bool isFictivePoint(VertexID idPoint) const;

    /**
       Return true if the given point is a multiple point (ie corner point of a rectangular patch)
    */
    bool isCornerPoint(VertexID idPoint) const;

    /**
       Get the shortest non separating cycle using points as base point list, and unstick it from the boundaries
    */
    std::deque<VertexID> getShortestNonSeparatingCycleFromPointsUnstick(const std::deque<VertexID> & points, enum UnstickMethod uMethod, double minLength = 0.0, unsigned int halfSizeMiddle = 3);

    /**
       given a list of 4 points that are on boundaries of a surface,
       return a pair of pair of points. Each pair corresponds to a boundary.
       This method throws an exception if the distribution is not as expected (two points on each boundary).
    */
    void sortBySide(MeshPart & cc, const std::vector<VertexID> & mpInsideCC, VertexID sideA[2], VertexID sideB[2]);

  public:

    /**
       Default constructor
    */
    MeshCut(Mesh & mesh);

    /**
       Default constructor with length operator
    */
    MeshCut(Mesh & mesh, const LengthEdge & lMethod);

    /**
       Copy constructor
    */
    MeshCut(const MeshCut & meshCut);

    /**
       Destructor
    */
    virtual inline ~MeshCut() { }

    /**
       Cut the mesh according to minimal cut.
       @param nbCutMax Maximum number of cut
       @param cMethod Cutting algorithm
       @param mergeMPoints Merging method for multipoints
       @param uMethod Unstick method for paths
       @param optimMethod optimization method for the loop search
       @param inputPathFile Input file containing the paths to use
       @param logMesh Prefix of the log files containing meshes at each step of the cutting process
       @param iManager Indent manager used for outputs
       @return Result cut mesh
    */
    Mesh & cutMeshUsingMinimalCut(int nbCutMax = -1, enum CuttingMethod cMethod = CMEricksonPlus, enum MergeMPointsMethod mergeMPoints = MMPnone, enum UnstickMethod uMethod = UMPreserveMesh, enum OptimEC optimMethod = OECTruncMinMax, const std::string & inputPathFile = "", const std::string & logMesh = "", const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Cut the mesh using centers.
       @return Result cut mesh
    */
    Mesh & cutMeshUsingPoints(const std::deque<VertexID> & centers);

    /**
       Cut the mesh using centers, splitting mesh in many connected components.
       @return Result cut mesh
    */
    Mesh & cutMeshUsingPointsSplitCC(const std::deque<VertexID> & centers, const Taglut::IndentManager iManager);

    /**
       Build minimal cut
       Iterative method to build minimal cut
       @param nbCutMax Maximum number of cut
       @param cMethod Cutting algorithm
       @param mergeMPoints Merging method for multipoints
       @param uMethod Unstick method for paths
       @param optimMethod optimization method for the loop search
       @param inputPathFile Input file containing the paths to use
       @param logMesh Prefix of the log files containing meshes at each step of the cutting process
       @param iManager Indent manager used for outputs
       @return number of cut
    */
    unsigned int buildMinimalCut(int nbCutMax = -1, enum CuttingMethod cMethod = CMEricksonPlus, enum MergeMPointsMethod mergeMPoints = MMPnone, enum UnstickMethod uMethod = UMPreserveMesh, enum OptimEC optimMethod = OECTruncMinMax, const std::string & inputPathFile = "", const std::string & logMesh = "", const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Build cut from points
       If the mesh has a boundary, use buildCutFromPointsFromBoundary. Otherwise, use buildCutTreeFromPoints.
       @param centers center of clusters used by this cutting method
       @return Junctions computed during the cutting
    */
    std::deque<VertexID> buildCutFromPoints(const std::deque<VertexID> & centers);


    /**
       Build a cut tree from points
       @param centers center of clusters used by this cutting method
       @return Junctions computed during the cutting
    */
    std::deque<VertexID> buildCutTreeFromPoints(const std::deque<VertexID> & centers);

    /**
       Build cut from points
       Iterative method to build cuts from points to boundary
       @param centers center of clusters used by this cutting method
       @return Junctions computed during the cutting
    */
    std::deque<VertexID> buildCutFromPointsFromBoundary(const std::deque<VertexID> & centers);

    /**
       Build cut from centers, splitting mesh in many connected components
       Iterative method to build cuts from centers to boundary
    */
    void buildCutFromPointsSplitCC(const std::deque<VertexID> & centers, const Taglut::IndentManager iManager = Taglut::IndentManager());


    /**
       Build cut using local curvature.
       @param alpha Minimum curvature used during cut.
       @param maxCut Maximum cuttings before stop (-1: no stop)
       @param iManager indent manager
    */
    void buildCutUsingCurvature(double alpha, int maxCut, const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       return true if edge (p1, p2) is a cut edge
    */
    bool inCutEdge(VertexID p1, VertexID p2) const;

    /**
       Return a clone of the current object
    */
    MeshPathes * clone() const;

    /**
       return a clone of the current object (using the given mesh rather than original one
    */
    MeshPathes * clone(Mesh & mesh) const;

    /**
       Build rectangular cut
       First minimal cut is computed, then iterative method is used to cut meth
       in rectangular patches
       @param nbCutMax Maximum number of cut
       @param cMethod Cutting algorithm
       @param mergeMPoints Merging method for multipoints
       @param uMethod Unstick method for paths
       @param optimMethod optimization method for the loop search
       @param inputPathFile Input file containing the paths to use
       @param logMesh Prefix of the log files containing meshes at each step of the cutting process
       @param iManager Indent manager used for outputs
    */
    void buildRectangularCut(int nbCutMax = -1, enum CuttingMethod cMethod = CMEricksonPlus, enum MergeMPointsMethod mergeMPoints = MMPnone, enum UnstickMethod uMethod = UMPreserveMesh, enum OptimEC optimMethod = OECTruncMinMax, const std::string & inputPathFile = "", const std::string & logMesh = "", const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Return true if the given file is a known format for export (rectangular cut format).
    */
    virtual bool isExportFormat(const std::string & fileName) const;

    /**
       Export to file if the format is known (rectangular cut format).
    */
    virtual void exportToFile(const std::string & fileName);

    /**
       Cut surface according to the PCA method: find the extrema points on the axis given in parameter, and cut according to the shortest path
    */
    Mesh & cutMeshUsingPCA(const PCAAxis & axis = PCAFirst);

    /**
       Cut surface according to the PCA method: find the extrema points on the axis given in parameter, and cut according to the shortest path
    */
    void buildPCACut(const PCAAxis & axis = PCAFirst);

    /**
       Cut surface according to the Volumic PCA method: find the extrema points on the axis given in parameter, and cut according to the shortest path
    */
    Mesh & cutMeshUsingVolumicPCA(unsigned int voxelNumber, const PCAAxis & axis = PCAFirst);

    /**
       Cut surface according to the Volumic PCA method: find the extrema points on the axis given in parameter, and cut according to the shortest path
    */
    void buildVolumicPCACut(unsigned int voxelNumber, const PCAAxis & axis = PCAFirst);

    /**
       Cut the surface according to a NLoop (N = \p nbPaths) computed from the given \p basePoint
    */
    Mesh & cutMeshUsingNLoop(VertexID basePoint, unsigned int nbPaths = 3);

    /**
       Cut the surface according to a NLoop (N = \p nbPaths) computed from the given \p basePoint
    */
    void buildNLoopCut(VertexID basePoint, unsigned int nbPaths = 3);


    /**
       Cut the surface using a topological only method
       @param basePoint Starting point of the growing process
       @param nbTriangles Number of triangles to be added (break of the growing surface after this triangle). 0 means no threshold.
    */
    void buildSimpleCut(VertexID basePoint = 0, TriangleID nbTriangles = 0);


    /**
       Cut the surface using a topological only method
       @param basePoint Starting point of the growing process
       @param nbTriangles Number of triangles to be added (break of the growing surface after this triangle). 0 means no threshold.
       @return The cut mesh
    */
    Mesh & cutMeshUsingSimpleCut(VertexID basePoint = 0, TriangleID nbTriangles = 0);

    /**
       Cut the surface using a topological only method (ordering the points using the geodesic distance). 0 means no threshold.
       @param basePoint Starting point of the growing process
       @param nbPoints Number of points to be added (break of the growing surface after this point)
    */
    void buildSimpleCutDistance(VertexID basePoint = 0, VertexID nbPoints = 0);

    /**
       Cut the surface using a topological only method (ordering the points using the geodesic distance). 0 means no threshold.
       @param basePoint Starting point of the growing process
       @param nbPoints Number of points to be added (break of the growing surface after this point)
       @return The cut mesh
    */
    Mesh & cutMeshUsingSimpleCutDistance(VertexID basePoint = 0, VertexID nbPoints = 0);

    /**
       Using a simulated annealing approach to cut the manipulated mesh by 3-loops.
       The resulting surface is composed of cylinders
    */
    void buildCylinderCutUsingNLoops(const NLoopEvaluator & nLEvaluator, unsigned int aritymin = 3, unsigned int aritymax = 3, double temp0 = 5., unsigned int step_size = 100, double temp_coeff = 0.9, double accept_threshold = .01, double temp_min = 1e-3, enum SimulatedAnnealingNLoop::SAMethod method = SimulatedAnnealingNLoop::SAByMiddle, const std::string & logMesh = "", const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Using a simulated annealing approach to cut the manipulated mesh by 3-loops.
       The resulting surface is composed of cylinders
    */
    Mesh & cutMeshUsingCylinderCutUsingNLoops(const NLoopEvaluator & nLEvaluator, unsigned int aritymin = 3, unsigned int aritymax = 3, double temp0 = 5., unsigned int step_size = 100, double temp_coeff = 0.9, double accept_threshold = .01, double temp_min = 1e-3, enum SimulatedAnnealingNLoop::SAMethod method = SimulatedAnnealingNLoop::SAByMiddle, const std::string & logMesh = "");

    /**
       Using a simulated annealing approach to cut the manipulated mesh by 3-loops, then cut the cylinders to obtain quadrangles
       The resulting surface is composed of quadrangles
    */
    void buildRectangularCutUsingNLoops(const NLoopEvaluator & nLEvaluator,
					unsigned int aritymin = 3, unsigned int aritymax = 3, double temp0 = 5.,
					unsigned int step_size = 100, double temp_coeff = 0.9,
					double accept_threshold = .01, double temp_min = 1e-3,
					enum SimulatedAnnealingNLoop::SAMethod method = SimulatedAnnealingNLoop::SAByMiddle,
					enum UnstickMethod uMethod = UMPreserveMesh,
					enum QuadMethod qmethod = QMSimple,
					const std::string & logMesh = "", const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Using a simulated annealing approach to cut the manipulated mesh by 3-loops, then cut the cylinders to obtain quadrangles.
       The resulting surface is composed of quadrangles
    */
    Mesh & cutMeshUsingRectangularCutUsingNLoops(const NLoopEvaluator & nLEvaluator,
						 unsigned int aritymin = 3,
						 unsigned int aritymax = 3,
						 double temp0 = 5., unsigned int step_size = 100, double temp_coeff = 0.9,
						 double accept_threshold = .01, double temp_min = 1e-3,
						 enum SimulatedAnnealingNLoop::SAMethod method = SimulatedAnnealingNLoop::SAByMiddle,
						 enum UnstickMethod uMethod = UMPreserveMesh,
						 enum QuadMethod qmethod = QMSimple,
						 const std::string & logMesh = "");

    /**
       Build rectangular cut of a set of cylinders that has been computed using n-loops.
       Simple method using a first cut (shortest path) and a weighted distance for the second one
       @param iManager Indent manager used for outputs
    */
    void buildRectangularCutFromCylindricalTilingSimple(const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Build rectangular cut of a set of cylinders that has been computed using n-loops.
       Using ABF to build a parameterization of the surface, then compute shortest paths using the corresponding distances
       @param iManager Indent manager used for outputs
    */
    void buildRectangularCutFromCylindricalTilingABF(const Taglut::IndentManager iManager = Taglut::IndentManager(), const std::string & logMesh = "");


    /**
       Build rectangular cut of a set of cylinders that has been computed using n-loops.
       Using Floater to build a parameterization of the surface, then compute shortest paths using the corresponding distances
       @param iManager Indent manager used for outputs
    */
    void buildRectangularCutFromCylindricalTilingFloater(const Taglut::IndentManager iManager = Taglut::IndentManager(), const std::string & logMesh = "");

  };
}

#endif
