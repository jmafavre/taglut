/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "TGraph.h"

using namespace Taglut;

int TGraph::BORDER = 0;
int TGraph::CROSS = 1;


TGraph::TGraph() { }

TGraph::TGraph(const Graph & g) : Graph(g) {

}

Node & TGraph::addCrossNode() {
  return addNode(CROSS);
}

Node & TGraph::addBoundaryNode() {
  return addNode(BORDER);
}

bool TGraph::hasBorderNeighbour(const Node & n) const {
  for(std::deque<NodeID>::const_iterator b = n.getNeighbours().begin(); b != n.getNeighbours().end(); ++b)
    if ((*this)[*b].getFlag() == BORDER)
      return true;

  return false;
}

NodeID TGraph::getBorderNeighbour(const Node & n) const {
  for(std::deque<NodeID>::const_iterator b = n.getNeighbours().begin(); b != n.getNeighbours().end(); ++b)
    if ((*this)[*b].getFlag() == BORDER)
      return *b;

  throw Exception("getBorderNeighbour(1): cannot found a border neighbour");
}

void TGraph::simplification(bool verbose) {
  bool merge = true;
  // first remove edges that no modify the topology
  while(merge) {
    merge = false;
    for(TGraph::iterator n = begin(); n != end(); ++n)
      if ((*n).getFlag() != BORDER)
	for(std::deque<NodeID>::const_iterator b = (*n).getNeighbours().begin(); b != (*n).getNeighbours().end(); ++b)
	  if ((*b != (*n).getId()) && ((*this)[*b].getFlag() != BORDER)) {
	    merge = true;
	    if (verbose)
	      std::cout << "Merging " << *b << " and " << (*n).getId() << std::endl;
	    mergeNodes(*b, (*n).getId());
	    break;
	  }
  }

  // then merge the cross points with a connected boundary point if has no loop on it
  merge = true;
  while(merge) {
    merge = false;
    for(TGraph::iterator n = begin(); n != end(); ++n)
      if (((*n).getFlag() != BORDER) && !(*n).hasLoop() && hasBorderNeighbour(*n)) {
	merge = true;
	NodeID idB = getBorderNeighbour(*n);
	if (verbose)
	  std::cout << "Merging " << idB << " and " << (*n).getId() << " (boundary merge)" << std::endl;
	mergeNodes(idB, (*n).getId());
	break;
      }
  }
}


void TGraph::mergeNodes(NodeID id1, NodeID id2) {
  if ((id1 >= size()) || (id2 >= size()))
    throw Exception("Unknown node");


  // if the two given points are the same one, all is down
  if (id1 == id2)
    return;

  // invert the nodes if needed
  if (id1 > id2) {
    mergeNodes(id2, id1);
    return;
  }

  Node & node1 = (*this)[id1];
  Node & node2 = (*this)[id2];
  int newFlag = CROSS;
  if ((node1.getFlag() == BORDER) || (node2.getFlag() == BORDER))
    newFlag = BORDER;



  // first, add the neighbours of id2 to the point id1
  for(std::deque<NodeID>::const_iterator b = node2.getNeighbours().begin(); b != node2.getNeighbours().end(); ++b)
    if (*b != id2) {
      if (*b != id1)
	node1.addNeighbour(*b);
      (*this)[*b].replaceNeighbour(id2, id1);
    }

  // set flag
  node1.setFlag(newFlag);
  // remove a loop
  node1.removeNeighbour(id1);

  // then remove node2
  erase(begin() + id2);

  // and translate
  for(TGraph::iterator i = begin(); i != end(); ++i) {
    if ((*i).getId() > id2)
      (*i).incId(-1);
    for(std::deque<NodeID>::iterator b = (*i).getNeighbours().begin(); b != (*i).getNeighbours().end(); ++b)
      if (*b > id2)
	*b -= 1;
  }

}


void TGraph::trace() const {
  std::cout << ">> [Graph] <<" << std::endl;
  for(TGraph::const_iterator i = begin(); i != end(); ++i) {
    if ((*i).getFlag() == BORDER)
      std::cout << " Border #";
    else
      std::cout << " Cross  #";
    std::cout << (*i).getId() << ":";

    for(std::deque<NodeID>::const_iterator b = (*i).getNeighbours().begin(); b != (*i).getNeighbours().end(); ++b)
      std::cout << " " << *b;
    std::cout << std::endl;
  }
}
