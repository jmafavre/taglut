/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESHUNCUT
#define MESHUNCUT

#include "Point3D.h"
#include "Exception.h"
#include "MeshPathes.h"

namespace Taglut {
  class Mesh;

  /**
     @class MeshUncut
     Mesh manipulator sticking cuttings
  */
  class MeshUncut : public MeshManipulator {
  private:
    /** mesh after uncutting */
    Mesh Umesh;

    void buildUMesh();

  public:

    /**
       Default constructor
    */
    MeshUncut(Mesh & mesh);

    /**
       If needed, buid uncutted mesh and return it
    */
    Mesh & getUMesh();


  };
}

#endif
