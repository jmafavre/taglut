/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshRectPatcher.h"

using namespace Taglut;

void MeshRectPatcher::BEdge::computeAngleSum() {
  const Coord2D & c1 = mapping[firstO];
  const Coord2D & c2 = mapping[lastO];

  // the value of the angle depends of the order of the edge according to the surface

  if (isDirect)
    angleSum = c1.angle2D(mapping[predOPoint], c2) +
      c2.angle2D(c1, mapping[nextOPoint]);
  else
    angleSum = c1.angle2D(c2, mapping[predOPoint]) +
      c2.angle2D(mapping[nextOPoint], c1);

}


MeshRectPatcher::MeshRectPatcher(const MeshRectPatcher & mRect) : MeshPathes(mRect), mapping(mRect.mapping), ratioArea(mRect.ratioArea) {

}

MeshRectPatcher::MeshRectPatcher(const MeshRectPatcher & mRect, Mesh & mesh_t) : MeshPathes(mRect, mesh_t), mapping(mRect.mapping), ratioArea(mRect.ratioArea) {

}

MeshPathes * MeshRectPatcher::clone() const {
  return new MeshRectPatcher(*this);
}

MeshPathes * MeshRectPatcher::clone(Mesh & mesh_t) const {
  return new MeshRectPatcher(*this, mesh_t);
}

void MeshRectPatcher::flipTrianglesOnCorners() {
  // flip triangle on corners of "mesh" and "cMesh"
  for(std::list<BEdge>::iterator e = bEdgeList.begin(); e != bEdgeList.end(); ++e)
    flipTrianglesOnCorner((*e).getFirstOriginalPoint());
}

void MeshRectPatcher::flipTrianglesOnCorners(const BEdge & aEdge) {
  flipTrianglesOnCorner(aEdge.getFirstPoint());
  flipTrianglesOnCorner(aEdge.getLastPoint());
}

void MeshRectPatcher::flipTrianglesOnCorner(VertexID cPointId) {
  Point3D & pt = cMesh.point(cPointId);
  if (pt.getNbTriangles() == 1) {
    Triangle & t = cMesh.triangle(pt.getTriangles().front());
    VertexID p1 = t.getOtherPoint(cPointId);
    VertexID p2 = t.getOtherPoint(cPointId, p1);
    VertexID pp1 = getProjection(p1);
    VertexID pp2 = getProjection(p2);
    cMesh.flipEdge(p1, p2);
    std::pair<VertexID, VertexID> otherPoints = (*mesh).flipEdge(pp1, pp2);
    // TODO: unused otherPoints?
  }
}


void MeshRectPatcher::buildRectangleCut(IndentManager & iManager, bool verbose, unsigned int maxCut) {
  unsigned int nbCut = 0;
  assert((*mesh).getNbPoints() == mapping.size());
  bEdgeList.clear();

  (*mesh).setPointFlag(0);

  // first compute list of points in boundary
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 0) {
      VertexID nbSamePoint = getPointsSameLocation((*p).getId()).size();
      if (nbSamePoint > 2) {
	assert((*mesh).point((*p).getId()).getIsBoundary());
	(*p).setFlag(1);
      }
    }

  // then build list of open edges

  // find first multipoint
  VertexID mp = (*mesh).getNbPoints();
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 1) {
      mp = (*p).getId();
      break;
    }

  // if there is no multipoint, cutting is down.
  if (mp == (*mesh).getNbPoints())
    return;

  // else, build edges arround the boundary
  buildEdgesFromPoint(mp);

  if (bEdgeList.size() <= 4) {
    std::cout << iManager << " There is no rather multipoints in the boundary to cut mesh. " << std::endl;
    return;
  }

  // flip single triangles on corners
  std::cout << iManager << " Flipping triangles on corners." << std::endl;
  flipTrianglesOnCorners();

  std::cout << iManager << " There is " << bEdgeList.size() << " boundary edges at the begining of the process." << std::endl;
  if (verbose) {
    std::cout << iManager << " Intitial ratio: " << ratioArea << std::endl;
  }
  else {
    std::cout << iManager << " Cut [";
    std::cout.flush();
  }

  bool seen = true;
  float localRatioArea = ratioArea;
  while(bEdgeList.size() > 4) {
    if ((maxCut != 0) && (maxCut == nbCut))
      break;
    if (seen)
      bEdgeList.sort();
    seen = false;

    for(std::list<BEdge>::iterator e = bEdgeList.begin(); e != bEdgeList.end(); ++e) {
      MeshManipulator mManip(cMesh);
      assert((*e).getPredPoint() != (*mesh).getNbPoints());
      assert((*e).getNextPoint() != (*mesh).getNbPoints());

      // build flags for points (to remove boundary neighbour of pathes if possible)
      cMesh.setPointFlag(0);
      setFlagOfBoundaryNeighbours((*e).getNextPoint());
      setFlagOfBoundaryNeighbours((*e).getPredPoint());

      try {
	std::deque<VertexID> path = mManip.computeShortestPathInCC((*e).getNextPoint(), (*e).getPredPoint(), 0);

	if (path.size() > 1) {
	  Polygon p = computePolygonFromEdge(*e, path);
	  if (verbose)
	    std::cout << iManager;
	  if (p.isNotCrushed(localRatioArea, verbose)) {
	    // add path
	    std::deque<VertexID> ePoints = addPathAllowSplitBoundary(path);
	    seen = true;
	    logPathes(*this);
	    polygonList.push_back(p);
	    localRatioArea = ratioArea;

	    if (verbose) {
	      std::cout << iManager << " Cutting using edge " << *e << ". Reinit ratio area to " << ratioArea << std::endl;
	    }
	    else {
	      std::cout << ".";
	      std::cout.flush();
	    }

	    BEdge & aEdge = updateEdgeList(e, path, ePoints);

	    flipTrianglesOnCorners(aEdge);
	    ++nbCut;
	  }
	}
	else {
	  throw FatalException("buildRectangleCut(): path cannot be found");
	}
      }
      catch (FatalException ee) {
	throw ee;
      }
      catch (Exception ee) {
	// path starting from first point, then connect the border after walking in
	// center of mesh, then next go on the center of the mesh, producing a
	// two part rectangle: o-o -> bad
      }
      if (seen)
	break;
    }

    // if all polygons are crushed, decrease ratioArea
    if (!seen) {
      if (verbose)
	std::cout << iManager << " All edges has been checked, but no rectangle can be constructed with good properties. Ratio is decreased from " << localRatioArea << " to " << localRatioArea - 0.1 << std::endl;
      else {
	std::cout << "<";
	std::cout.flush();
      }
      localRatioArea -= 0.1;
    }
  }
  if (!verbose) {
    std::cout << "]" << std::endl;
  }

  if (maxCut > 0) {
    std::cout << iManager << " Break in cuttings using max-cut." << std::endl;
  }
  std::cout << iManager << " Number of edges at the end of the cut process: " << bEdgeList.size() << std::endl;


}

void MeshRectPatcher::updatePointFlags() {
  MeshManipulator mManip(cMesh);
  cMesh.setPointFlag(0);
  cMesh.setTriangleFlag(0);
  for(std::deque<Polygon>::const_iterator p = polygonList.begin(); p != polygonList.end(); ++p) {
    TriangleID idT = (*p).findFirstTriangleInside();
    VertexID firstPoint = cMesh.triangle(idT).getP1();
    mManip.computeCCUsingPointsSimple(firstPoint, 0, 1);
  }
}

void MeshRectPatcher::setFlagOfBoundaryNeighbours(VertexID idP) {
  Point3D & p = cMesh.point(idP);

  // point should have more than 2 points
  if (p.getNbNeighbours() <= 2)
    return;

  // for each neighbour, if it's a boundary point, flag it with 1
  bool hasInsideNb = false;
  for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin();
      nb != p.getNeighbours().end(); ++nb)
    if(cMesh.point(*nb).getIsBoundary()) {
      cMesh.point(*nb).setFlag(1);
    }
    else
      hasInsideNb = true;

  // if point has not neighbour inside mesh, set all flags to 0
  if (!hasInsideNb)
    for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin();
	nb != p.getNeighbours().end(); ++nb)
      cMesh.point(*nb).setFlag(0);

}

VertexID MeshRectPatcher::getOpenPoint(VertexID point) {
  if (cMesh.point(point).getFlag() == 0)
    return point;

  MeshManipulator mManip(cMesh);

  std::deque<VertexID> listSameLocation = mManip.getPointsSameLocation(point);
  for(std::deque<VertexID>::const_iterator p = listSameLocation.begin(); p != listSameLocation.end(); ++p)
    if (cMesh.point(*p).getFlag() == 0)
      return *p;

  throw Exception("getOpenPoint(1): Point with same location cannot be found.");
}


Polygon MeshRectPatcher::computePolygonFromEdge(const BEdge & bEdge, const std::deque<VertexID> & path) {
  return Polygon(mapping, getProjection(findPredEdge(bEdge).getPath()), getProjection(bEdge.getPath()), getProjection(findNextEdge(bEdge).getPath()), getProjection(path));
}

MeshRectPatcher::BEdge & MeshRectPatcher::updateEdgeList(std::list<BEdge>::iterator & e, const std::deque<VertexID> & path, const std::deque<VertexID> & ePoints) {
  assert(ePoints.size() == 4);
  updatePointFlags();

  // remove e
  BEdge edge(*e);
  bEdgeList.erase(e) ;

  // remove pred
  std::list<MeshRectPatcher::BEdge>::iterator pred_t = findPredEdgeIt(edge);

  VertexID predOP = (*pred_t).getPredOPoint();
  VertexID predP = (*pred_t).getPredPoint();
  VertexID first = (*pred_t).getFirstOriginalPoint();
  bEdgeList.erase(pred_t);

  // remove next
  std::list<MeshRectPatcher::BEdge>::iterator next = findNextEdgeIt(edge);
  VertexID nextOP = (*next).getNextOPoint();
  VertexID nextP = (*next).getNextPoint();
  VertexID last = (*next).getLastOriginalPoint();
  bEdgeList.erase(next);

  // then build and add new edge
  std::deque<VertexID> newPath;
  for(std::deque<VertexID>::const_iterator i = path.begin(); i != path.end(); ++i)
    newPath.push_front(*i);

  assert((newPath.front() == ePoints[3]) || (newPath.front() == ePoints[2]));
  assert((newPath.back() == ePoints[0]) || (newPath.back() == ePoints[1]));
  if (cMesh.point(newPath.front()).getFlag() != 0) {
    if (newPath.front() == ePoints[3])
      newPath.front() = ePoints[2];
    else
      newPath.front() = ePoints[3];
    assert(cMesh.point(newPath.front()).getFlag() == 0);
  }
  if (cMesh.point(newPath.back()).getFlag() != 0) {
    if (newPath.back() == ePoints[1])
      newPath.back() = ePoints[0];
    else
      newPath.back() = ePoints[1];
    assert(cMesh.point(newPath.back()).getFlag() == 0);
  }

  bEdgeList.push_back(BEdge(mapping, newPath, first, last, edge.getIsDirect()));
  bEdgeList.back().setPredPoint(predP);
  bEdgeList.back().setNextPoint(nextP);
  bEdgeList.back().setPredOPoint(predOP);
  bEdgeList.back().setNextOPoint(nextOP);


  (*(findPredEdgeIt(bEdgeList.back()))).setNextPoint(last).setNextOPoint(getProjection(last)).setLastPoint(newPath.front());
  (*(findNextEdgeIt(bEdgeList.back()))).setPredPoint(first).setPredOPoint(getProjection(first)).setFirstPoint(newPath.back());;

  // Then update next and pred id for each edge
  for(std::list<BEdge>::iterator i = bEdgeList.begin(); i != bEdgeList.end(); ++i) {
    (*i).setNextPoint(computeNewIdInside((*i).getNextPoint()));
    (*i).setPredPoint(computeNewIdInside((*i).getPredPoint()));
    assert(cMesh.point((*i).getPredPoint()).getFlag() == 0);
    assert(cMesh.point((*i).getNextPoint()).getFlag() == 0);
  }

  return bEdgeList.back();
}

VertexID MeshRectPatcher::computeNewIdInside(VertexID id) const {
  if (cMesh.point(id).getFlag() == 0)
    return id;
  VertexID idP = getProjection(id);

  // else, last created path has cut id into two points, and id
  // is not the point inside the open connected component
  for(VertexID i = cMesh.getNbPoints() - 1; ; --i) {
    if (mapping[getProjection(i)].same2DLocation(mapping[idP])) {
      if (cMesh.point(i).getFlag() == 0)
	return i;
    }
    assert(i != 0);
  }

  assert(false);
}

const MeshRectPatcher::BEdge & MeshRectPatcher::findPredEdge(const BEdge & bEdge) const {
  for(std::list<BEdge>::const_iterator i = bEdgeList.begin(); i != bEdgeList.end(); ++i)
    if ((*i).getFirstOriginalPoint() == bEdge.getPredOPoint())
      return *i;

  throw Exception("findPredEdge(1): edge not found");
}

const MeshRectPatcher::BEdge & MeshRectPatcher::findNextEdge(const BEdge & bEdge) const {
  for(std::list<BEdge>::const_iterator i = bEdgeList.begin(); i != bEdgeList.end(); ++i)
    if ((*i).getLastOriginalPoint() == bEdge.getNextOPoint())
      return *i;

  throw Exception("findNextEdge(1): edge not found");
}

std::list<MeshRectPatcher::BEdge>::iterator MeshRectPatcher::findPredEdgeIt(const BEdge & bEdge) {
  for(std::list<BEdge>::iterator i = bEdgeList.begin(); i != bEdgeList.end(); ++i)
    if ((*i).getFirstOriginalPoint() == bEdge.getPredOPoint())
      return i;

  throw Exception("findPredEdgeIt(1): edge not found");
}

std::list<MeshRectPatcher::BEdge>::iterator MeshRectPatcher::findNextEdgeIt(const BEdge & bEdge) {
  for(std::list<BEdge>::iterator i = bEdgeList.begin(); i != bEdgeList.end(); ++i)
    if ((*i).getLastOriginalPoint() == bEdge.getNextOPoint())
      return i;

  throw Exception("findNextEdgeIt(1): edge not found");
}



void MeshRectPatcher::buildEdgesFromPoint(VertexID mp) {
  std::deque<VertexID> path;
  VertexID pred_t = (*mesh).getNbPoints();
  Point3D & p = (*mesh).point(mp);

  // if we've seen all the boundary points, break
  if (p.getFlag() == 3) {
    assert(bEdgeList.size() != 0);
    bEdgeList.front().setPredPoints(bEdgeList.back().getFirstOriginalPoint());
    bEdgeList.back().setNextPoints(bEdgeList.front().getLastOriginalPoint());
    return;
  }
  else {
    assert(p.getFlag() == 1);
    // else, setting flag for the first vertex
    p.setFlag(3);
  }

  // then find next multi point
  VertexID idNextMp = (*mesh).getNextBPoint(mp);
  if ((*mesh).point(idNextMp).getFlag() == 2) // means seen non multi point
    idNextMp = (*mesh).getNextBPoint(mp, idNextMp);

  path.push_back(mp);

  while((*mesh).point(idNextMp).getFlag() == 0) {
    path.push_back(idNextMp);
    (*mesh).point(idNextMp).setFlag(2); // small point has been seen
    idNextMp = (*mesh).getNextBPoint(idNextMp, mp);
  }

  path.push_back(idNextMp);

  assert(((*mesh).point(idNextMp).getFlag() == 1) || ((*mesh).point(idNextMp).getFlag() == 3));

  // set next point for the last added edge if exists
  if (bEdgeList.size() != 0) {
    bEdgeList.back().setNextPoints(idNextMp);
    pred_t = bEdgeList.back().getFirstOriginalPoint();
  }

  // first check if (pred, c1, c2, next) is in good order in the boundary
  const Triangle & t = (*mesh).findTriangle(path[0], path[1]);
  VertexID other = t.getOtherPoint(path[0], path[1]);
  double order = (mapping[path[1]].get2DX() - mapping[path[0]].get2DX()) * (mapping[other].get2DY() - mapping[path[0]].get2DY()) -
    (mapping[path[1]].get2DY() - mapping[path[0]].get2DY()) * (mapping[other].get2DX() - mapping[path[0]].get2DX());

  // add the edge to the list
  bEdgeList.push_back(BEdge(mapping, path, path.front(), path.back(), !order));

  // set pred point if exists for the currently added point
  if (pred_t != (*mesh).getNbPoints())
    bEdgeList.back().setPredPoints(pred_t);

  // then find next edge
  buildEdgesFromPoint(idNextMp);
}

