/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SPACE_SPLITER
#define SPACE_SPLITER

#include <vector>
#include <deque>
#include "IDTypes.h"
#include "Coord3D.h"
#include "Point3D.h"
#include "Point2D.h"

namespace Taglut {
  class Mesh;

  /**
     @class SpaceSpliter

     @author Jean-Marie Favreau
     @brief Tool for point manipulation using geometrical localization
  */
  class SpaceSpliter : public Box3D {
  private:
    const std::vector<Point3D> & pointList;
    const std::vector<Triangle> * triangleList;
    double mX;
    double mY;
    double mZ;
    SpaceSpliter ** children;
    std::vector<VertexID> * points;
    std::vector<TriangleID> * triangles;
    bool initialised;
    bool flat;

    /**
       Add point inside structure
    */
    void addPoint(VertexID id);

    /**
       Add triangle inside structure
    */
    void addTriangle(const Triangle3D & t, TriangleID id);

    /**
       Init points and triangles on structure
    */
    void initPointsAndTriangles();

    /**
       Create structure
    */
    void createStructure(unsigned int depth);

    /**
       Compute min and max values
    */
    void computeMinMax();

    /**
       init initialised flag
    */
    void isInitialised(bool value);

  public:

    /**
       Default constructor with mesh
    */
    SpaceSpliter(const Mesh & mesh, bool flat = false);

    /**
       Default constructor with point list
    */
    SpaceSpliter(const std::vector<Point3D> & points, bool flat = false);

    /**
       Constructor with point list and partial part of space

    */
    SpaceSpliter(unsigned int depth, const std::vector<Point3D> & points,
		 const std::vector<Triangle> * triangles,
		 double minX, double minY, double minZ,
		 double maxX, double maxY, double maxZ, bool flat = false);


    /**
       Destructor
    */
    ~SpaceSpliter();

    /**
       Get all points with the same location as point given in parameter.
       result list is sorted by id value (min to max)
    */
    void getSameLocationPoints(VertexID id, std::deque<VertexID> & result);

    /**
       Get point with smallest x location
    */
    VertexID findFirstX();

    /**
       Find first (smallest y value) point with same Y location
       @return Point id, or out of range value if not found
    */
    VertexID findFirstSameX(VertexID id);

    /**
       Find next point with same X location
       @return Point id, or out of range value if not found
    */
    VertexID findNextSameX(VertexID id);

    /**
       Find next point with same Y location
       @return Point id, or out of range value if not found
    */
    VertexID findNextSameY(VertexID id);

    /**
       Find all triangles intersecting the ray defined by \p point
       @param point 2D coords of the ray
       @return list of triangles
    */
    std::vector<TriangleID> findTrianglesInRay(const Coord2D & point);
  };
}

#endif
