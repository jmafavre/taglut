/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshComparator.h"

using namespace Taglut;

MeshComparator::MeshComparator(const Mesh & mesh1_t, const Mesh & mesh2_t) : mesh1(mesh1_t), mesh2(mesh2_t), mMatcher(mesh1_t, mesh2_t) {

}


double MeshComparator::sumSquareDistance3D() const {
  double result = 0;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double distance = mesh1.point((*mm).first).distance(mesh2.point((*mm).second));
    result += distance * distance;
  }

  return result;
}

double MeshComparator::meanSquareDistance3D() const {
  double result = 0;
  unsigned int nbPts = mMatcher.size();
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double distance = mesh1.point((*mm).first).distance(mesh2.point((*mm).second));
    result += distance * distance / nbPts;
  }

  return result;
}

double MeshComparator::maxSquareDistance3D() const {
  double result = 0;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double distance = mesh1.point((*mm).first).distance(mesh2.point((*mm).second));
    const double d2 = distance * distance;
    if (d2 > result)
      result += d2;
  }

  return result;
}

double MeshComparator::useMethod(const std::string & method) const {
  if (method == "max-square-distance3D")
    return maxSquareDistance3D();
  else if (method == "mean-square-distance3D")
    return meanSquareDistance3D();
  else if (method == "sum-square-distance3D")
    return sumSquareDistance3D();
  else
    return 0.0;
}


bool MeshComparator::isKnownMethod(const std::string & method) {
  return ((method == "max-square-distance3D") ||
	  (method == "mean-square-distance3D") ||
	  (method == "sum-square-distance3D"));
}
