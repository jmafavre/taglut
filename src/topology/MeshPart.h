/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshManipulator.h"
#include "PLPath.h"
#include "IDTranslator.h"

#ifndef MESHPART
#define MESHPART

namespace Taglut {
  class Mesh;
  class NLoop;

  /**
     @class MeshPart
     Mesh manipulator defined by a part of a given mesh
  */
  class MeshPart : public MeshManipulator {
  private:

    /**
       Contains a cut of current mesh subset, homeomorphic to
       a disc.
    */
    MeshPart * simpleCut;

    /**
       boundaries number (-1: not computed)
    */
    int nbBoundaries;
    int fGroupSize;

    /** minimum value */
    double minXValue;
    /** maximum value */
    double maxXValue;
    /** minimum value */
    double minYValue;
    /** maximum value */
    double maxYValue;
    /** minimum value */
    double minZValue;
    /** maximum value */
    double maxZValue;


    /**
       Points inside subset
    */
    std::deque<bool> points;
    /**
       Number of points inside
    */
    VertexID nbPoints;

    /**
       Triangles inside subset
    */
    std::deque<bool> triangles;
    /**
       Number of triangles inside
    */
    TriangleID nbTriangles;

    /**
       Compute boundary connected component from point id.
       All points in CC should have flag value equal iFlag. At end of computing, these
       triangle has flag value equal to fFlag.
       For connection, edge e is a boundary if a triangle containing e is
       inside (flag value equal to fInside) and other (if exists) is outside
       @return number of points inside CC  */
    VertexID computeBCCUsingPoints(VertexID id, int iFlag, int fFlag, int tInside = 0);



    /**
       Return true if edge (idP1, idP2) is in a boundary of meshPart
       using triangle flags. Requirement: an edge e is boundary if a
       triangle containing e is inside (flag value equal to fInside)
       and other (if exists) is outside
    */
    bool isBoundaryEdgeTFlag(VertexID idP1, VertexID idP2, int tInside) const;

    /**
       Return the number of triangles containing edge (idP1, idP2).
    */
    unsigned int getNbTrianglesEdge(VertexID idP1, VertexID idP2, int tInside) const;


    /**
       Set point flag = 3 for flags in simple cut boundary
    */
    void initSimpleCutBoundaryFlags();

    /**
       Given an edge and a triangle, return 1 if triangle cannot
       be added together with this edge, 0 else.
    */
    unsigned int hasBeenChecked(VertexID idP1, VertexID idP2, TriangleID idT);



    /**
       Return 0 if strip containing idT is not a simple strip.
       Else, return flag of a neighbour crossroad.
       fFlag: value flag value of strip triangles after call.
    */
    int isSimpleStrip(TriangleID idT, int fFlag);


    /**
       Build part of mesh using a disc (defined by radius and center)
       \param center Disc center
       \param radius Disc radius
    */
    void buildDiscPart(const Point3D & center, double radius);


    /**
       Build part of mesh using previously computed length for each point from center
       \param center center id
       \param radius Disc radius
    */
    void buildDiscPart(double radius);



    /**
       Build part of mesh using a ball
       \param center center id
       \param radius ball radius
    */
    void buildBallPart(const Point3D & center, double radius);


    /**
       Build part of mesh using a box
       \param point1 first point
       \param point2 second point
    */
    void buildBoxPart(const Point3D & point1, const Point3D & point2);


    /**
       Given a point, build connected component nearest to center
    */
    void buildNearestCC(const Point3D & center);

    /**
       Build a simple cut of current surface
    */
    void buildSimpleCut(VertexID v = std::numeric_limits<VertexID>::max());

    /**
       Build the connected component on the complementary of \p paths in the mesh
       starting to the given triangle.
       Assuming that flags are correctly defined (triangles: 0 not seen, 1 seen; points: 1 inside NLoop, 0 outside)
    */
    void buildCompCC(const NLoop & paths, TriangleID idT);


    /**
       Compute connected component part inside the complementary of the current mesh
       (using triangles)
       @param idT triangle selected
       @param mpartCC resulting MeshPart
    */
    void computeCompCC(TriangleID idT, MeshPart & mpartCC);

    /**
       Compute connected component part inside the current mesh
       @param pt point selected
       @param mpartCC resulting MeshPart
    */
    void computeCC(Point3D & pt, MeshPart & mpartCC);


    /**
       function used by getSlimSimpleCutBorders
    */
    void setFlagsSlimBorders(VertexID point);

    /**
       Compute halfstrip (used by estimateBetterBasePoint)
    */
    void computeHalfStrip(VertexID point);


    /**
       Update extrema of the mesh part
    */
    void updateExtrema(VertexID idPoint);


    /**
       init extrema of the mesh part
    */
    void initExtrema();

    /**
       init part structure (points and triangles are inside or not according to \p inside)
    */
    void initPart(VertexID nbP, TriangleID nbT, bool inside = false);

  public:

    /**
       Default constructor
    */
    MeshPart();

    /**
       Main constructor
       \param mesh Supset Mesh
       \param full Add the full mesh on the mesh part
    */
    MeshPart(Mesh & mesh, bool full = false);

    /**
       Constructor (crop disc)
       \param mesh Supset Mesh
       \param center Disc center
       \param radius Disc radius
       \param disc if true, using disc method (geodesic length) else using ball method (euclidean length)
    */
    MeshPart(Mesh & mesh, const Point3D & center, double radius, bool disc = true);

    /**
       Constructor (crop box)
       \param mesh Supset Mesh
       \param point1 first point
       \param point2 second point
    */
    MeshPart(Mesh & mesh, const Point3D & point1, const Point3D & point2);

    /**
       Constructor select nearest connected component
       \param mesh Supset Mesh
       \param center Disc center
    */
    MeshPart(Mesh & mesh, const Point3D & center);

    /**
       Constructor select nearest connected component
       \param mesh Supset Mesh
       \param idP Disc center id
    */
    MeshPart(Mesh & mesh, VertexID idP);

    /**
       Constructor. Build the connected component on the complementary of \p paths in the mesh
       starting to the given triangle.
       Assuming that flags are correctly defined (triangles: 0 not seen, 1 seen; points: 1 inside NLoop, 0 outside)
    */
    MeshPart(Mesh & mesh, TriangleID idT, const NLoop & paths);

    /**
       Copy constructor
    */
    MeshPart(const MeshPart & meshPart);


    /**
       Destructor
    */
    ~MeshPart();

#ifndef SWIG
    /**
       Copy operator
    */
    MeshPart & operator=(const MeshPart & meshPart);
#endif

    /**
       Crop mesh using simple cut
    */
    void cropSimpleCut(VertexID v = std::numeric_limits<VertexID>::max());

    /**
     * @brief Return a simple cut of the current mesh from the given vertex
     * @param v
     * @return The mesh homeomorphic to a disc
     */
    Mesh buildCropMeshSimpleCut(VertexID v);


    /**
     * @brief Return a mesh where poles are removed
     * @param percentage Percentage for the two poles
     * @param axis The axis used to estimate poles
     * @param barycenter The barycenter used to estimate poles
     * @return The resulting mesh
     */
    inline Mesh buildRemovePoles(double percentage, const Coord3D & axis, const Coord3D & barycenter) {
        return buildRemovePoles(percentage, percentage, axis, barycenter);
    }

    /**
     * @brief Return a mesh where poles are removed
     * @param p1 Percentage of the 1st pole
     * @param p2 Percentage of the 2nd pole
     * @param axis The axis used to estimate poles
     * @param barycenter The barycenter used to estimate poles
     * @return The resulting mesh
     */
    Mesh buildRemovePoles(double p1, double p2, const Coord3D & axis, const Coord3D & barycenter);

    /**
       keep flag order but with smallest amplitude as possible
    */
    void stripTriangleFlags(int startValue);

    /**
       Switch flag values
    */
    void switchTriangleFlags(int f1, int f2);

    /**
       Add point to subset (if point is already in subset, point is all the
       same added)
    */
    void addPoint(VertexID idP);

    /**
       Add point to subset (if point is already in subset, point is all the
       same added)
    */
    void addPoint(const Point3D & pt);

    /**
       Add triangle to subset (if triangle is already in subset, triangle is all the
       same added)
    */
    void addTriangle(TriangleID idT);

    /**
       Add triangle to subset, and corresponding points
    */
    void  addTriangleAndPoints(TriangleID idT);

    /**
       Add all points and triangles with given flag value
       @param flagValueP Flag of selected points
       @param flagValueT Flag of selected triangles
    */
    void addPointsAndTriangles(int flagValueP, int flagValueT);

    /**
       Add all points and triangles
    */
    void addPointsAndTriangles();

    /**
       Return point number
    */
    VertexID getNbPoints() const;

    /**
       Return the number of boundary points with a number of boundary neighbour not equal to 2
    */
    VertexID getNbBadBPoints();

    /**
       Return point list
    */
    inline const std::deque<bool> & getPointMembership() const {
      return points;
    }

    /**
       Return triangle number
    */
    TriangleID getNbTriangles() const;

    /**
       Return the number of bad triangles (with incoherent point or with two identical points)
    */
    TriangleID getNbBadTriangles() const;

    /**
       Return triangle list
    */
    //const std::deque<TriangleID> & getTriangles() const;

    /**
       Return number of boundaries, compute number if needed
    */
    unsigned int getNbBoundaries();

    /* Limitations of the genus and Euler caracteristic computation method: the meshpart has to be:
       - orientable
       - without strange boundary configurations
       - only composed of one connected component
    */

    /**
       Return the genus of the surface
    */
    unsigned int getGenus();

    /**
       Return the number of edges of the mesh
    */
    unsigned int getNbEdges() const;

    /**
       Return the number of edges that does not take part in a triangle
    */
    unsigned int getNbSingleEdges();

    /**
       Return the Euler caracteristic of the surface
    */
    int getEulerCaracteristic() const;

    /**
       Return the Euler caracteristic of the surface, cut by the given \p paths.
       Warning: not yet fully implemented
    */
    int getEulerCaracteristic(const std::vector<std::vector<VertexID> > & paths) const;

    /**
       Return the second Betti number
    */
    int getBettiNumber();

    /**
       Set point flags = 3 for points in simple cut boundary
       Set flags = 2 for points and triangles inside simple cut
    */
    void initSimpleCutFlags();

    /**
       Set point flags = 1 for points and triangles inside simple
       cut and 0 for points and triangles outside.
    */
    void initInsideSimpleCutFlags();

    /**
       Init flag with inside/outside value for triangles and points.
       Flag = 0: ouside MeshPart
       Flag = 1: inside MeshPart
    */
    void initInsideFlags();

    /**
       Init flag with inside/outside value for triangles and points, and boundary for points.
       Flag = 0: ouside MeshPart
       Flag = 1: inside MeshPart
       Flag = 2: inside MeshPart and boundary
    */
    void initInsideAndBoundaryFlags();

    /**
       Return size of the simple cut boundary
    */
    VertexID getSimpleCutBoundarySize();

    /**
       Return strips id of the simple cut boundary
    */
    std::deque<VertexID> getSimpleCutStripsId();


    /**
       Return strips of the simple cut boundary
    */
    std::deque<std::deque<VertexID> > getSimpleCutStrips();

    /**
       build a crop mesh according to MeshPart.
       @return mesh
    */
    Mesh buildCropMesh(bool warning = true) const;

    /**
       crop mesh according to MeshPart.
    */
    void cropMesh(bool warning = true);

    /**
       Get points inside simpleCut border
    */
    std::deque<VertexID> getSimpleCutBorders();

    /**
       Get a skeleton of points inside simpleCut border
    */
    std::deque<VertexID> getSlimSimpleCutBorders();

    /**
       Get mesh
    */
    Mesh & getMesh();

    /**
       Get mesh
    */
    const Mesh & getMesh() const;

    /**
       Add in MeshPart all points with flag value given in parameter and their
       neighbours.
    */
    void addPointsAndNeighbours(int flag);

    /**
       Compute connected composents of the complementary of current meshpart.
       (using triangles)
    */
    std::deque<MeshPart> getCompCC();

    /**
       Return true if all the triangles arround \p point are in the same connected components of the complementary of
       current meshpart.
    */
    bool inSameCompCC(VertexID point);

    /**
       Compute connected composents of the current meshpart.
       (using points)
    */
    std::deque<MeshPart> getCC();

    /**
       Clean mesh (simple method): remove all points with bad local topology (i.e. points
       that cannot be ordered)
       @return number of points removed
    */
    VertexID cleanMesh();


    /**
       Given a point id inside simple cut border, compute an estimation of the length of the associated
       cycle.
    */
    double estimateCycleLength(VertexID point);

    /**
       Given a set of points (a strip inside simple cut border), compute
       the better base point according to the estimation length of the
       associated cycle. Do not compute a value if the corresponding path
       have a length higher than the given \p maxLength
    */
    std::pair<VertexID, double> estimateBetterBasePoint(const std::deque<VertexID> & strip, double maxLength = -1);


    /**
       @return list of boundary points inside mesh part
    */
    std::deque<VertexID> getBoundaryPoints() const;


    /**
       Given two set of points, compute the shortest path between both.
       @return path between the two sets, or empty path if not found
    */
    std::deque<VertexID> getShortestPath(const std::deque<VertexID> & set1,
					 const std::deque<VertexID> & set2);

    /**
       Return point with highest length value computed before, or out-of-range id if not found.
    */
    VertexID getPointHighestLength() const;


    /**
       Return point contained in the edge with maximum curvature
    */
    VertexID getPointMaxEdgeCurvature() const;

    /**
       @return true if point p is inside MeshPart
    */
    bool hasPoint(VertexID p) const;


    /**
       Return a string description of the box including the mesh part
    */
    std::string getBoxSize() const;

    /**
       Return list of nonreal points
    */
    std::deque<VertexID> getNonRealPoints() const;

    /**
       Return true if the given point is inside the boundary of the meshpart
    */
    bool isBoundaryPoint(VertexID id) const;

    /**
       Return true if the given point is inside the meshpart
    */
    inline bool isInsidePoint(VertexID id) const {
      return points[id];
    }

    /**
       Return true if the given triangle is inside the meshpart
    */
    inline bool isInsideTriangle(TriangleID id) const {
      return triangles[id];
    }

    /**
       Return true if the given edge is inside the meshpart (but not in the boundary)
    */
    bool isInsideEdge(VertexID id1, VertexID id2) const;

    /**
       Return a non boundary point inside the meshpart. If there is no corresponding point, return the number of points
       of the manipulated mesh.
    */
    VertexID getNonBoundaryPoint() const;

    /**
       Return a boundary point inside the meshpart. If there is no corresponding point, return the number of points
       of the manipulated mesh.
    */
    VertexID getBoundaryPoint() const;

    /**
       Return a point inside the meshpart. If there is no corresponding point, return the number of points
       of the manipulated mesh.
    */
    VertexID getInsidePoint() const;

    /** return the points of the given list that are inside the meshpart */
    std::vector<VertexID> getInsidePoints(const std::vector<VertexID> & ipoints) const;
  };
}

#endif
