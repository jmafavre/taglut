/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <limits>

#include "PointCloudOctree.h"
#include "PointCloud.h"
#include "Exception.h"

using namespace Taglut;


namespace Taglut {
  /**
     @class CompSecond
     A comparator for boxes with distance
  */
  class CompSecond {
  public:
    /**
       Comparator of the second element of the pair
    */
    bool operator() (const std::pair<unsigned int, double> & b1,
		     const std::pair<unsigned int, double> & b2) const {
      return b1.second < b2.second;
    }

  };
}


PointCloudOctree::PointCloudOctree(const PointCloud & pCloud_t,
				   unsigned int maximum_points_t, unsigned int maximum_depth_t, bool useSmallestDistance) : maximum_points(maximum_points_t),
															    maximum_depth(maximum_depth_t),
															    pCloud(pCloud_t), nbPoints(0) {
  children = NULL;

  // init
  minX = minY = minZ = std::numeric_limits<double>::max();
  maxX = maxY = maxZ = -std::numeric_limits<double>::max();
  for(PointCloud::const_iterator i = pCloud.begin(); i != pCloud.end(); ++i) {
    if ((*i).getX() < minX) minX = (*i).getX();
    if ((*i).getY() < minY) minY = (*i).getY();
    if ((*i).getZ() < minZ) minZ = (*i).getZ();
    if ((*i).getX() > maxX) maxX = (*i).getX();
    if ((*i).getY() > maxY) maxY = (*i).getY();
    if ((*i).getZ() > maxZ) maxZ = (*i).getZ();
  }
  // adjusting the box upper bound: the points cannot be located on the upper bound
  double offset = useSmallestDistance ? pCloud.getSmallestDistance() : 0.;
  if (offset == 0.0) offset = (maxX - minX) * 0.00001;
  maxX += offset;
  maxY += offset;
  maxZ += offset;

  if ((pCloud.size() > maximum_points) && (maximum_depth > 0))
    split();

  for(PointCloud::const_iterator i = pCloud.begin(); i != pCloud.end(); ++i)
    addPoint(*i);

}


PointCloudOctree::~PointCloudOctree() {
  if (children != NULL) {
    for(unsigned int i = 0; i < 8; ++i)
      delete(children[i]);
    delete [] children;
  }
}

PointCloudOctree::PointCloudOctree(const PointCloud & pCloud_t, unsigned int maximum_points_t, unsigned int maximum_depth_t,
				   double minx, double maxx, double miny, double maxy, double minz, double maxz) : Box3D(minx, maxx, miny, maxy, minz, maxz),
														   maximum_points(maximum_points_t), maximum_depth(maximum_depth_t),
														   pCloud(pCloud_t), nbPoints(0) {
  children = NULL;
}

void PointCloudOctree::addPoint(const Point3D & point) {
  assert((minX <= point.getX()) && (maxX > point.getX()));
  assert((minY <= point.getY()) && (maxY > point.getY()));
  assert((minZ <= point.getZ()) && (maxZ > point.getZ()));

  // if the box is full, first split it
  if ((children == NULL) && (points.size() == maximum_points) && (maximum_depth > 0))
    split();

  // add the point
  if (children == NULL) // if it's a leaf
    points.push_back(point.getId());
  else { // otherwise, look for the good child box
    double mX = minX + (maxX - minX) / 2;
    double mY = minY + (maxY - minY) / 2;
    double mZ = minZ + (maxZ - minZ) / 2;
    unsigned int idX = (point.getX() < mX) ? 0 : 1;
    unsigned int idY = (point.getY() < mY) ? 0 : 1;
    unsigned int idZ = (point.getZ() < mZ) ? 0 : 1;
    (*(children[idX + idY * 2 + idZ * 4])).addPoint(point);
  }
  ++nbPoints;
}


void PointCloudOctree::split() {
  if (maximum_depth == 0)
    return;
  if (children != NULL)
    return;

  // first build the children
  children = new PointCloudOctree *[8];

  if (children == NULL)
    throw Exception(std::string("Error during memory allocation."));

  for(unsigned int i = 0; i < 2; ++i)
    for(unsigned int j = 0; j < 2; ++j)
      for(unsigned int k = 0; k < 2; ++k)
	children[i + j * 2 + k * 4] = new PointCloudOctree(pCloud, maximum_points, maximum_depth - 1,
							   minX + i * (maxX - minX) / 2,
							   minX + (i + 1) * (maxX - minX) / 2,
							   minY + j * (maxY - minY) / 2,
							   minY + (j + 1) * (maxY - minY) / 2,
							   minZ + k * (maxZ - minZ) / 2,
							   minZ + (k + 1) * (maxZ - minZ) / 2);

  // then move the points of the current box in its child boxes
  for(std::vector<VertexID>::const_iterator p = points.begin(); p != points.end(); ++p)
    addPoint(pCloud[*p]);
  points.clear();

}




double PointCloudOctree::updateRadiusFromDirection(const Point3D & center, const Coord3D & vector, const Plane3DFunction & plane, double maxAngle, double previousResult) const {
  if (children == NULL) { // if it's a leaf, use all the points
    double result = previousResult;
    const double maxDist = cos(maxAngle);
    const double norm = 1.0; //vector.norm();
    assert(fabs(vector.norm() -  1.) <= 0.0000001);
    assert(maxDist >= 0.0);
    const VertexID cId = center.getId();
    const std::vector<VertexID>::const_iterator pcEnd = points.end();
    for(std::vector<VertexID>::const_iterator otherCenterID = points.begin(); otherCenterID != pcEnd; ++otherCenterID) {
      const Coord3D & otherCenter = pCloud[*otherCenterID];
      const double d1 = center.distance(otherCenter);
      if ((*otherCenterID != cId) && (d1 < 2 * result)) {
	const Coord3D n1 = otherCenter - center;
	double sproduct = (n1.getX() * vector.getX() + n1.getY() * vector.getY() + n1.getZ() * vector.getZ()) / (n1.norm() * norm);
	if (sproduct > 1.)
	  sproduct = 1.;
	if (sproduct > maxDist) { // if alpha >= M_PI / 2, no point on the vector can be both into the two spheres
	  const double d = d1 / sproduct / 2;
	  if (d < result) {
	    result = d;
	  }
	}
      }
    }
    return result;
  }
  else {
    std::vector<std::pair<unsigned int, double> > toBeProcessed;
    double result = previousResult;
    // first look for boxes in the good side of the plane, and close to the
    for(unsigned int i = 0; i < 8; ++i) {
      if (((*(children[i])).nbPoints != 0) && (plane.partInGoodSide((*(children[i])))))
	toBeProcessed.push_back(std::pair<unsigned int, double>(i, (*(children[i])).getMiddle().distance(center)));
    }
    // then sort according to the distance to the center
    std::sort(toBeProcessed.begin(), toBeProcessed.end(), CompSecond());
    // then check every needed child
    Coord3D centerSphere = center + vector * 2 * result;
    for (std::vector<std::pair<unsigned int, double> >::const_iterator p = toBeProcessed.begin(); p != toBeProcessed.end(); ++p) {
      if ((result == std::numeric_limits<double>::max()) || ((*(children[(*p).first])).getMiddle().distance(centerSphere) <= 2 * result + (*(children[(*p).first])).getRadius())) {
	assert((*p).first < 8);
	double result2 = (*(children[(*p).first])).updateRadiusFromDirection(center, vector, plane, maxAngle, result);
	if (result2 < result) {
	  result = result2;
	  centerSphere = center + vector * (2 * result);
	}
      }
    }

    return result;
  }
}


PointCloudOctree::Plane3DFunction::Plane3DFunction(const Coord3D & point_l, const Coord3D & vector_l) : Plane3D(point_l, vector_l) {
  assert(fabs(vector_l.norm() - 1.0) < 0.000001);
  direction = DZ;
  if (fabs(vector_l.getZ()) < 0.5) {
    if (fabs(vector_l.getX()) < 0.5)
      direction = DY;
    else
      direction = DX;
  }

  if (direction == DX) {
    a = - vector_l.getY() / vector_l.getX();
    b = - vector_l.getZ() / vector_l.getX();
    c = Coord3D::scalarProduct(vector_l, point_l) / vector_l.getX();
  }
  else if (direction == DY) {
    a = - vector_l.getX() / vector_l.getY();
    b = - vector_l.getZ() / vector_l.getY();
    c = Coord3D::scalarProduct(vector_l, point_l) / vector_l.getY();
  }
  else if (direction == DZ) {
    a = - vector_l.getX() / vector_l.getZ();
    b = - vector_l.getY() / vector_l.getZ();
    c = Coord3D::scalarProduct(vector_l, point_l) / vector_l.getZ();
  }
  vside = (*this)(point_l + vector_l) > 0;
  if (fabs((*this)(point_l + vector_l)) < 0.05) {
    std::cout << *this << std::endl;
  }
  assert(fabs((*this)(point_l + vector_l)) > 0.05);

  assert(direction != DX || (fabs((*this)(point_l + vector_l) + 1. / vector_l.getX()) < 0.000001));
  assert(direction != DY || (fabs((*this)(point_l + vector_l) + 1. / vector_l.getY()) < 0.000001));
  assert(direction != DZ || (fabs((*this)(point_l + vector_l) + 1. / vector_l.getZ()) < 0.000001));
  assert(fabs((*this)(point_l)) < 0.000001);
}


double PointCloudOctree::distanceNthNeighbour(const Point3D & center, unsigned int nbNb, std::vector<std::pair<VertexID, double> > & known_points, double distance) const {
  if (children == NULL) { // if it's a leaf, use all the points

    const std::vector<VertexID>::const_iterator pEnd = points.end();
    for(std::vector<VertexID>::const_iterator p = points.begin(); p != pEnd; ++p)
      if (*p != center.getId()) {
	const double d = pCloud[*p].distance(center);
	if (d < distance)
	  known_points.push_back(std::pair<VertexID, double>(*p,d));
      }

    if (known_points.size() >= nbNb) {
      std::sort(known_points.begin(), known_points.end(), CompSecond());
      known_points.erase(known_points.begin() + nbNb, known_points.end());
      return known_points[nbNb - 1].second;
    }
    else
      return distance;
  }
  else {
    std::vector<std::pair<unsigned int, double> > toBeProcessed;
    double result = distance;
    // first look for boxes in the good side of the plane, and close to the 
    for(unsigned int i = 0; i < 8; ++i) {
      if ((*(children[i])).nbPoints != 0)
	toBeProcessed.push_back(std::pair<unsigned int, double>(i, (*(children[i])).getMiddle().distance(center)));
    }
    // then sort according to the distance to the center
    std::sort(toBeProcessed.begin(), toBeProcessed.end(), CompSecond());
    // then check every needed child
    for (std::vector<std::pair<unsigned int, double> >::const_iterator p = toBeProcessed.begin(); p != toBeProcessed.end(); ++p)
      if ((result == std::numeric_limits<double>::max()) || ((*(children[(*p).first])).getMiddle().distance(pCloud[known_points[nbNb - 1].first]) <= result + (*(children[(*p).first])).getRadius())) {
	assert((*p).first < 8);
	result = (*(children[(*p).first])).distanceNthNeighbour(center, nbNb, known_points, result);
	assert((result == std::numeric_limits<double>::max()) || (result == known_points[nbNb - 1].second));
      }

    return result;

  }
}


std::vector<VertexID> PointCloudOctree::findPoints(const Coord3D & point, VertexID forbidden, double distance) const {
  std::vector<VertexID> result;
  if (children == NULL) { // if it's a leaf, use all the points
    const std::vector<VertexID>::const_iterator pEnd = points.end();
    for(std::vector<VertexID>::const_iterator p = points.begin(); p != pEnd; ++p)
      if ((*p != forbidden) && (pCloud[*p].distance(point) <= distance))
	result.push_back(*p);
    return result;
  }
  else {
    // check every needed child
    for (unsigned int i = 0; i < 8; ++i)
      if (((*(children[i])).nbPoints != 0) && ((*(children[i])).getMiddle().distance(point) <= distance + (*(children[i])).getRadius())) {
	std::vector<VertexID> result2 = (*(children[i])).findPoints(point, forbidden, distance);
	result.insert(result.end(), result2.begin(), result2.end());
      }

    return result;
  }
}

std::pair<VertexID, double> PointCloudOctree::findClosestPoint(const Coord3D & point, VertexID forbidden, double distance) const {
  if (children == NULL) { // if it's a leaf, use all the points

    const std::vector<VertexID>::const_iterator pEnd = points.end();
    std::pair<VertexID, double> result(std::numeric_limits<VertexID>::max(), std::numeric_limits<double>::max());
    for(std::vector<VertexID>::const_iterator p = points.begin(); p != pEnd; ++p)
      if (*p != forbidden) {
	const double d = pCloud[*p].distance(point);
	if (d < distance) {
	  result.first = *p;
	  result.second = distance = d;
	}
      }
    return result;
  }
  else {
    std::pair<VertexID, double> result(std::numeric_limits<VertexID>::max(), std::numeric_limits<double>::max());

    std::vector<std::pair<unsigned int, double> > toBeProcessed;
    // first look for boxes in the good side of the plane, and close to the 
    for(unsigned int i = 0; i < 8; ++i) {
      if ((*(children[i])).nbPoints != 0)
	toBeProcessed.push_back(std::pair<unsigned int, double>(i, (*(children[i])).getMiddle().distance(point)));
    }
    // then sort according to the distance to the point
    std::sort(toBeProcessed.begin(), toBeProcessed.end(), CompSecond());
    // then check every needed child
    for (std::vector<std::pair<unsigned int, double> >::const_iterator p = toBeProcessed.begin(); p != toBeProcessed.end(); ++p)
      if ((result.second == std::numeric_limits<double>::max()) || ((*(children[(*p).first])).getMiddle().distance(pCloud[result.first]) <= result.second + (*(children[(*p).first])).getRadius())) {
	assert((*p).first < 8);
	std::pair<VertexID, double> result2 = (*(children[(*p).first])).findClosestPoint(point, forbidden, result.second);
	if (result2.second < result.second)
	  result = result2;
      }

    return result;
  }
}

