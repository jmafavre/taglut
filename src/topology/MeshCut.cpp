/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <deque>
#include <list>
#include <set>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>

#include "Mesh.h"
#include "MeshCut.h"
#include "FileTools.h"
#include "FileExceptions.h"
#include "MeshPart.h"
#include "Length.h"
#include "Exception.h"
#include "MeshVoxelizer.h"
#include "WaveFront.h"
#include "NLoop.h"
#include "MeshMap.h"
#include "ABFMethod.h"
#include "WeightedEdges.h"
#include "CylinderParameterization.h"

#include "Display3D.h"

using namespace Taglut;

MeshCut::MeshCut(Mesh & mesh_t) : MeshPathes(mesh_t), cMeshManip(cMesh) { }

MeshCut::MeshCut(Mesh & mesh_t, const LengthEdge & lMethod) : MeshPathes(mesh_t, lMethod), cMeshManip(cMesh) { }

MeshCut::MeshCut(const MeshCut & meshCut) : MeshPathes(meshCut), cMeshManip(cMesh) { }

MeshPathes * MeshCut::clone() const {
  return new MeshCut(*this);
}

MeshPathes * MeshCut::clone(Mesh &) const {
  throw Exception("clone(): not yet implemented");
}

Mesh & MeshCut::cutMeshUsingMinimalCut(int nbCutMax, enum CuttingMethod cMethod, enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod, enum OptimEC optimMethod, const std::string & inputPathFile, const std::string & logMesh, const IndentManager iManager) {
  buildMinimalCut(nbCutMax, cMethod, mergeMPoints, uMethod, optimMethod, inputPathFile, logMesh, iManager);
  return cutMesh();
}

Mesh & MeshCut::cutMeshUsingPCA(const PCAAxis & axis) {
  buildPCACut(axis);
  return cutMesh();
}

Mesh & MeshCut::cutMeshUsingVolumicPCA(unsigned int voxelNumber, const PCAAxis & axis) {
  buildVolumicPCACut(voxelNumber, axis);
  return cutMesh();
}

Mesh & MeshCut::cutMeshUsingPoints(const std::deque<VertexID> & centers) {
  buildCutFromPoints(centers);
  return cutMesh();
}


Mesh & MeshCut::cutMeshUsingCylinderCutUsingNLoops(const NLoopEvaluator & nLEvaluator,
						   unsigned int aritymin, unsigned int aritymax, double temp0, unsigned int step_size,
						   double temp_coeff, double accept_threshold, double temp_min,
						   enum SimulatedAnnealingNLoop::SAMethod method,
						   const std::string &) {
  buildCylinderCutUsingNLoops(nLEvaluator, aritymin, aritymax, temp0, step_size, temp_coeff, accept_threshold, temp_min, method);
  return cutMesh();
}

Mesh & MeshCut::cutMeshUsingRectangularCutUsingNLoops(const NLoopEvaluator & nLEvaluator,
						      unsigned int aritymin, unsigned int aritymax, double temp0,
						      unsigned int step_size, double temp_coeff, double accept_threshold, double temp_min,
						      enum SimulatedAnnealingNLoop::SAMethod method,
						      enum UnstickMethod uMethod,
						      enum QuadMethod qMethod,
						      const std::string &) {
  buildRectangularCutUsingNLoops(nLEvaluator, aritymin, aritymax, temp0, step_size, temp_coeff, accept_threshold, temp_min, method, uMethod, qMethod);
  return cutMesh();
}


Mesh & MeshCut::cutMeshUsingSimpleCut(VertexID basePoint, TriangleID nbTriangles) {
  buildSimpleCut(basePoint, nbTriangles);
  return cutMesh();
}

Mesh & MeshCut::cutMeshUsingSimpleCutDistance(VertexID basePoint, VertexID nbPoints_t) {
  buildSimpleCutDistance(basePoint, nbPoints_t);
  return cutMesh();
}

std::deque<VertexID> MeshCut::buildCutFromPoints(const std::deque<VertexID> & centers) {
  if ((*mesh).hasBoundaryPoint())
    return buildCutFromPointsFromBoundary(centers);
  else
    return buildCutTreeFromPoints(centers);
}

std::deque<VertexID> MeshCut::buildCutTreeFromPoints(const std::deque<VertexID> & centers) {
  std::deque<VertexID> targets;

  // build the cutting tree
  MeshManipulator cManip(cMesh);
  std::deque<std::deque<VertexID> > tree = cManip.computeShortestTree(centers);

  // then cut according to this tree
  for(std::deque<std::deque<VertexID> >::iterator path = tree.begin(); path != tree.end(); ++path)
    if ((*path).size() >= 2) {
      // clear path before cutting: first, remove all the extremity points that are boundary points
      MeshManipulator mManip(cMesh);

      std::deque<std::deque<VertexID> > pathes = mManip.splitPath(*path);
      for(std::deque<std::deque<VertexID> >::iterator p = pathes.begin(); p != pathes.end(); ++p) {
	correctingPath(*p);
	addPath(*p);
	logPathes(*this);
      }
    }

  return targets;
}

std::deque<VertexID> MeshCut::buildCutFromPointsFromBoundary(const std::deque<VertexID> & centers) {
  assert((*mesh).hasBoundaryPoint());
  std::deque<VertexID> open = centers;
  std::deque<VertexID> targets;
  (*this).clear();
  projection.clear();
  clock_t tstart = clock();

  // for each center given in parameter
  while(open.size() != 0) {
    // first find cluster nearest to the boundary
    MeshManipulator mmanip((*mesh), *lengthEdgeMethod);
    VertexID center = mmanip.getNearestToBoundary(open);

    // then build path
    assert(center < (*mesh).getNbPoints());
    MeshPathes mp(cMesh, *lengthEdgeMethod);
    // find path to boundary
    std::deque<VertexID> path = mp.getShortestPathToBoundary(center);

    // then adding it
    if (path.size() > 1) {
      assert(path.front() == center);
      targets.push_back(path.back());
      addPath(path);
      logPathes(*this);
    }

    // then remove point from open list
    for(std::deque<VertexID>::iterator i = open.begin(); i != open.end(); ++i)
      if (*i == center) {
	open.erase(i);
	break;
      }

  }

  std::cout  << " Cutting duration: " << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds"  << std::endl;
  return targets;
}

Mesh & MeshCut::cutMeshUsingPointsSplitCC(const std::deque<VertexID> & centers, const IndentManager iManager) {
  buildCutFromPointsSplitCC(centers, iManager);
  return cutMesh();
}

void MeshCut::buildCutFromPointsSplitCC(const std::deque<VertexID> & centers, const IndentManager iManager) {
  std::deque<VertexID> iBList;

  // first save boundary property
  for(Mesh::const_point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p)
    if ((*p).getIsBoundary())
      iBList.push_back((*p).getFlag());

  // then cut mesh using centers
  std::deque<VertexID> targets = buildCutFromPoints(centers);

  // then cut mesh using "Y" cuttings
  std::cout << iManager << " Cutting using clusters [";
  std::cout.flush();
  for(std::deque<VertexID>::const_iterator t = targets.begin(); t != targets.end(); ++t) {
    std::cout << ".";
    std::cout.flush();
    buildCutY(*t, iBList);
  }

  std::cout << "]" << std::endl;
}


VertexID MeshCut::getNbSameLocation(VertexID nbInitial, const std::deque<VertexID> & centers) {
  for(std::deque<VertexID>::const_iterator c = centers.begin(); c != centers.end(); ++c)
    for(std::deque<VertexID>::const_iterator nb = cMesh.point(*c).getNeighbours().begin();
	nb != cMesh.point(*c).getNeighbours().end(); ++nb) {
      if ((nbInitial != *nb) && cMesh.point(nbInitial).sameLocation(cMesh.point(*nb)))
	return *nb;
    }

  throw Exception(std::string("Other point with same location in the original mesh not found"));
}

void MeshCut::buildCutY(VertexID center, const std::deque<VertexID> & iBList) {
  MeshManipulator mMesh(cMesh);
  std::deque<VertexID> centerClones = mMesh.getPointsSameLocation(center);
  std::deque<std::pair<VertexID, VertexID> > neighboursList;

  // first build neighbour list
  for(std::deque<VertexID>::const_iterator c = centerClones.begin(); c != centerClones.end(); ++c)
    for(std::deque<VertexID>::const_iterator nb = cMesh.point(*c).getNeighbours().begin();
	nb != cMesh.point(*c).getNeighbours().end(); ++nb)
      cMesh.point(*nb).setFlag(0);

  for(std::deque<VertexID>::const_iterator c = centerClones.begin(); c != centerClones.end(); ++c)
    for(std::deque<VertexID>::const_iterator nb = cMesh.point(*c).getNeighbours().begin();
	nb != cMesh.point(*c).getNeighbours().end(); ++nb)
      if (cMesh.point(*nb).getIsBoundary() && (cMesh.point(*nb).getFlag() == 0))
	try {
	  std::pair<VertexID, VertexID> nbrpair(*nb, getNbSameLocation(*nb, centerClones));
	  neighboursList.push_front(nbrpair);
	  cMesh.point(*nb).setFlag(1);
	  cMesh.point(neighboursList.front().second).setFlag(1);
	}
	catch (Exception e) {}

  if (neighboursList.size() == 0)
    return;

  // then remove from neighbours list points that will not be used for cutting
  bool isInOB = false;
  for(std::deque<VertexID>::const_iterator b = iBList.begin(); b != iBList.end(); ++b)
    if (center == *b) {
      isInOB = true;
      break;
    }

  std::deque<std::pair<VertexID, VertexID> > newNList;
  if (isInOB) {
    // compute flag value for each neighbour:
    //  0: not in initial cut
    //  1: in initial cut
    for(std::deque<std::pair<VertexID, VertexID> >::const_iterator i = neighboursList.begin();
	i != neighboursList.end(); ++i) {
      cMesh.point((*i).first).setFlag(0);
      cMesh.point((*i).second).setFlag(0);
    }
    for(std::deque<VertexID>::const_iterator b = iBList.begin(); b != iBList.end(); ++b)
      cMesh.point(*b).setFlag(1);

    // compute new neighbour list
    for(std::deque<std::pair<VertexID, VertexID> >::const_iterator i = neighboursList.begin();
	i != neighboursList.end(); ++i)
      if ((cMesh.point((*i).first).getFlag() == 0) && (cMesh.point((*i).second).getFlag() == 0))
	newNList.push_back(*i);
  }
  else {
    cMesh.setPointFlag(0);
    mMesh.computeDijkstraStickCuttings(iBList, 0);

    // compute nearest point among the neighbour list
    double l = mMesh.getLength(neighboursList.front().first);
    VertexID idN = 0;
    for(VertexID i = 1; i < neighboursList.size(); ++i) { // uint better than iterator
      double lTemp = mMesh.getLength(neighboursList[i].first);
      if (lTemp < l) {
	l = lTemp;
	idN = i;
      }
    }

    for(VertexID i = 0; i < neighboursList.size(); ++i) // uint better than iterator
      if (i != idN)
	newNList.push_front(neighboursList[i]);
  }
  neighboursList = newNList;


  for(std::deque<std::pair<VertexID, VertexID> >::const_iterator i = neighboursList.begin();
      i != neighboursList.end(); ++i) {
    // compute path between points
    std::deque<VertexID> path = mMesh.computeShortestPath((*i).first, (*i).second);

    // split mesh according to this path
    if (path.size() > 1) {
      addPathSplitBefore(path);
      logPathes(*this);
    }
  }

}

void MeshCut::joinBoundaries(int nbCutMax, enum CuttingMethod cMethod, enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod, const IndentManager iManager, const clock_t & tstart) {
  unsigned int nbCut = 0;

  if ((mergeMPoints != MMPnone) || (cMethod == CMEricksonPlus)) {
    MeshPart cmpart(cMesh, cMesh.point(0));

    // if a merging method is used for multipoints, the algorithm is an iteratif process that merge
    // two boundaries at each step
    nbBoundaries = cmpart.getNbBoundaries();
    while(nbBoundaries != 1) {
      ++nbCut;
      joinNearestBoundaries(mergeMPoints, uMethod);
      MeshPart cmpart_t(cMesh, cMesh.point(0));
      nbBoundaries = cmpart_t.getNbBoundaries();
      if ((nbCutMax >= 0) && (nbCut >= (unsigned int)nbCutMax)) {
	std::cout << iManager << " Break (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds) number of boundaries: " << nbBoundaries  << std::endl;
	break;
      }
      else
	std::cout << iManager << "  (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds) number of boundaries: " << nbBoundaries  << std::endl;
    }
  }
  else {
    // otherwise, if mergeMPoints == MMPnone, the classical Erickson method is used

    // first compute boundaries
    std::deque<std::deque<VertexID> > bdries = getBoundariesCMesh();

    // then compute the tree between boundaries
    MeshManipulator cManip(cMesh);
    std::deque<std::deque<VertexID> > tree = cManip.computeShortestTreeFromPathes(bdries, nbCutMax);

    // then cut according to this tree
    for(std::deque<std::deque<VertexID> >::iterator path = tree.begin(); path != tree.end(); ++path)
      if ((*path).size() >= 2) {
      // clear path before cutting: first, remove all the extremity points that are boundary points
	MeshManipulator mManip(cMesh);

	std::deque<std::deque<VertexID> > pathes = mManip.splitPath(*path);
	for(std::deque<std::deque<VertexID> >::iterator p = pathes.begin(); p != pathes.end(); ++p) {
	  correctingPath(*p);
	  addPath(*p);
	  logPathes(*this);
	}
      }
  }

}


unsigned int MeshCut::buildMinimalCut(int nbCutMax, enum CuttingMethod cMethod, enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod, enum OptimEC optimMethod, const std::string & inputPathFile, const std::string & logMesh, const IndentManager iManager) {
  clock_t tstart = clock();
  enum CuttingMethod lcMethod = cMethod;
  double predLength = 0.0;

  (*this).clear();
  projection.clear();
  VertexID nbCut = 0;

  if ((mergeMPoints == MMPonePoint) && ((cMethod == CMErickson) || (cMethod == CMEricksonPlus))) {
    std::cout << iManager << "Merging point method and cutting method are incompatible. Selecting the old cutting method." << std::endl;
    lcMethod = CMOld;
  }

  if (inputPathFile != "")
    fromFile(inputPathFile, iManager);

  // compute connected components
  std::deque<MeshPart> cc = cMesh.getConnectedComponents();
  if (cc.size() != 1) {
    std::cout << cc.size() << std::endl;
    throw ExceptionTopology("MeshCut: connected component number != 1");
  }
  MeshPart & mpart = cc[0];


  if ((lcMethod == CMErickson) || (lcMethod == CMEricksonPlus)) {
    if (lcMethod == CMErickson)
      std::cout << iManager << " Using the Erickson (2002) cutting method to build a minimal cut" << std::endl;
    else
      std::cout << iManager << " Using a modified Erickson (2002) cutting method to build a minimal cut (spanning-tree computed by step by step)" << std::endl;
    unsigned int genus = (*mesh).getGenus();

    if ((genus == 0) && ((*mesh).getNbBoundaryPoints() == 0))
      buildSphereCut();
    else {
      std::cout << iManager << "Boundary length: " << (*mesh).getBoundaryLength() << std::endl;
      while(genus != 0) {
	if ((nbCutMax >= 0) && ((unsigned int)nbCutMax == nbCut)) {
	  std::cout << iManager << " Break (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds, " << nbCutMax << " cuts) genus: " << genus  << "                      "  << std::endl;
	  break;
	}
	++nbCut;
	std::cout << iManager << "  (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds) genus: " << genus  << "                      " << std::endl;
	predLength = addShortestNontrivialCycle(optimMethod, predLength, mergeMPoints, uMethod);
	if ((optimMethod == OECTruncMax) || (optimMethod == OECnoTrunc))
	  predLength = 0.0;
	assert(cMesh.getNbCC() == 1);
	genus = cMesh.getGenus();
	if (logMesh != "")
	  logCMesh(logMesh);
      }

      std::cout << iManager << "Boundary length: " << cMesh.getBoundaryLength() << std::endl;

      if ((nbCutMax < 0) || ((unsigned int)nbCutMax > nbCut)) {
	std::cout << iManager <<  "Building the cutting tree (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds)..." << std::endl;

	joinBoundaries(nbCutMax < 0 ? nbCutMax : nbCutMax - nbCut, lcMethod, mergeMPoints, uMethod, iManager, tstart);
      }

      std::cout << iManager << "Boundary length: " << cMesh.getBoundaryLength() << std::endl;
    }

  }
  else { // cMethod == CMOld
    std::cout << iManager << "Using the old cutting method to build a minimal cut" << std::endl;
    // initialisation of topological properties
    nbBoundaries = mpart.getNbBoundaries();
    fGroupSize = mpart.getBettiNumber();

    // compute topological values
    if ((nbBoundaries == 0) && (fGroupSize == 0))
      buildSphereCut();
    else
      while((nbBoundaries != 1) || (fGroupSize != 0)) {
	++nbCut;
	std::cout << iManager << "  (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds) nbBoundaries: " << nbBoundaries << ", fGroupSize: " << fGroupSize << std::endl;
	// compute next cut path

	if (nbBoundaries <= 1)
	  predLength = addShortestNontrivialCycle(optimMethod, predLength, mergeMPoints, uMethod);
	else
	  joinNearestBoundaries(mergeMPoints, uMethod);

	// compute new topological properties
	MeshPart cmpart(cMesh, cMesh.point(0));
	if ((nbBoundaries == 1) && (cmpart.getNbBoundaries() == 1))
	  if (fGroupSize <= (unsigned int) cmpart.getBettiNumber())
	    throw Exception(std::string("Found shortest path is a wrong path."));

	nbBoundaries = cmpart.getNbBoundaries();
	fGroupSize = cmpart.getBettiNumber();

	if ((nbCutMax >= 0) && ((unsigned int)nbCutMax == nbCut)) {
	  std::cout << iManager << " Break (" << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds, " << nbCutMax << " cuts). nbBoundaries: " << nbBoundaries << ", fGroupSize: " << fGroupSize << std::endl;
	  break;
	}
      }
      std::cout << iManager << "Boundary length: " << cMesh.getBoundaryLength() << std::endl;

  }

  clock_t tend = clock();
  std::cout << iManager << " Cutting duration: " << (tend - tstart) / ((double) CLOCKS_PER_SEC) << " seconds" << std::endl;
  return nbCut;
}



double MeshCut::addShortestNontrivialCycle(enum OptimEC optimMethod, double minLength, enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod) {
  std::deque<VertexID> path;
  if ((mergeMPoints == MMPonePoint) && (multiPoints.size() != 0)) {
    path = getShortestNonSeparatingCycleFromPointsUnstick(multiPoints, uMethod, minLength);
  }
  else
    path = getShortestNonSeparatingCycle(minLength, optimMethod);

  // then add to current cut computed path
  if (path.size() <= 1)
    throw Exception(std::string("addShortestNontrivialCycle: shortest path cannot be computed: wrong path."));

  assert((getProjection(path.front()) == getProjection(path.back())) || (cMesh.point(path.front()).getIsBoundary() && cMesh.point(path.back()).getIsBoundary() &&
									 ((path.size() != 2) || (!cMesh.isBoundaryEdge(path.back(), path.front())))));

  LengthEdge * l;
  MeshMap * mMap = NULL;
  if ((*lengthEdgeMethod).needMeshMap()) {
    mMap = new MeshMap(*((*lengthEdgeMethod).getMeshMap()), cMesh);
    l = (*lengthEdgeMethod).cloneMeshMap(*mMap);
  }
  else
    l = (*lengthEdgeMethod).clone(cMesh);
  LengthEdgeMergeBoundary lb(*l);

  double rLength = cMesh.getLength(path, lb);

  addPathSplitBefore(path);
  logPathes(*this);

  if ((mergeMPoints == MMPonePoint) || (mergeMPoints == MMPsimple)) {
    computeMultiPoints(true);
  }

  if (mMap != NULL)
    delete mMap;
  delete l;
  return rLength;
}


void MeshCut::buildPCACut(const PCAAxis & axis) {
  if ((*mesh).getNbPoints() < 2)
    throw ExceptionTopology("MeshCut: a cut needs more than 2 points.");

  std::deque<Coord3D> axes = (*mesh).computePCAAxes();
  Coord3D barycenter = (*mesh).getIsobarycenter();
  Coord3D selectedVector;
  if (axis == PCAFirst)
    selectedVector = axes.front();
  else if (axis == PCASecond)
    selectedVector = axes[1];
  else // (axis == PCAThird)
    selectedVector = axes[2];
  Line3D line(barycenter, selectedVector);

  std::pair<VertexID, VertexID> intersections = (*mesh).getFirstLastIntersection(line);

  if ((intersections.first == (*mesh).getNbPoints()) || (intersections.second == (*mesh).getNbPoints()) || (intersections.first == intersections.second)) {
    std::cout << "Warning: cannot find points on the axis." << std::endl;
    return;
  }

  /* compute shortest length */
  std::deque<VertexID>  path = computeShortestPath(intersections.first, intersections.second);

  /* add it to the mesh cut */
  addPath(path);
  logPathes(*this);
}


void MeshCut::buildVolumicPCACut(unsigned int voxelNumber, const PCAAxis & axis) {
  if ((*mesh).getNbPoints() < 2)
    throw ExceptionTopology("MeshCut: a cut needs more than 2 points.");

  MeshVoxelizer mVox(*mesh, voxelNumber);

  std::deque<Coord3D> axes = mVox.getVolumicPCAAxes();
  Coord3D barycenter = mVox.getVolumicIsoBarycenter();

  Coord3D selectedVector;
  if (axis == PCAFirst)
    selectedVector = axes.front();
  else if (axis == PCASecond)
    selectedVector = axes[1];
  else // (axis == PCAThird)
    selectedVector = axes[2];
  Line3D line(barycenter, selectedVector);

  std::pair<VertexID, VertexID> intersections = (*mesh).getFirstLastIntersection(line);

  if ((intersections.first == (*mesh).getNbPoints()) || (intersections.second == (*mesh).getNbPoints()) || (intersections.first == intersections.second)) {
    std::cout << "Warning: cannot find points on the axis." << std::endl;
    return;
  }

  /* compute shortest length */
  std::deque<VertexID>  path = computeShortestPath(intersections.first, intersections.second);

  /* add it to the mesh cut */
  addPath(path);
  logPathes(*this);
}


void MeshCut::buildSphereCut() {
  if ((*mesh).getNbPoints() < 2)
    throw ExceptionTopology("MeshCut: a cut needs more than 2 points.");


  /* compute 2 points with max distance */
  // initialisation
  VertexID p1 = 0;
  VertexID p2 = 1;
  double distance = (*mesh).point(p1).distance((*mesh).point(p2));

  for(VertexID i = 0; i < (*mesh).getNbPoints() / 2 + 1; ++i) // uint better than iterator
    for(Mesh::const_point_iterator j = (*mesh).point_begin(); j != (*mesh).point_end(); ++j) {
      double d = (*mesh).point(i).distance(*j);
      if (d > distance) {
	p1 = i;
	p2 = (*j).getId();
	distance = d;
      }
    }

  /* compute shortest length */
  std::deque<VertexID>  path = computeShortestPath(p1, p2);

  /* add it to the mesh cut */
  addPath(path);
  logPathes(*this);
}



std::deque<std::deque<VertexID> > MeshCut::getBoundariesCMesh() {
  cMesh.setPointFlag(0);
  cMesh.setTriangleFlag(0);
  cMesh.setBoundaryPointFlag(1);

  MeshManipulator mManip(cMesh, *lengthEdgeMethod);

  int nextFlagValue = 2;
  // then set flags for boundaries not connected
  for(Mesh::const_point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p)
    if ((*p).getFlag() == 1)
      mManip.computeBoundaryCC((*p).getId(), 1, nextFlagValue++, 0);


  // build real boundaries
  std::deque<std::deque<VertexID> > bdries;
  for(Mesh::point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p)
    if ((*p).getFlag() != 0) {
      int flag = (*p).getFlag();
      bdries.push_front(std::deque<VertexID>());
      std::deque<VertexID> & bdry = bdries.front();
      for(Mesh::point_iterator j = p; j != cMesh.point_end(); ++j) {
	if ((*j).getFlag() == flag) {
	  bdry.push_front((*j).getId());
	  (*j).setFlag(0);
	}
      }
    }

  return bdries;
}

void MeshCut::joinNearestBoundaries(enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod) {
  MeshManipulator mManip(cMesh, *lengthEdgeMethod);

  // first compute boundaries
  std::deque<std::deque<VertexID> > bdries = getBoundariesCMesh();

  // then compute shortest path and add it to current meshcut
  std::deque<VertexID> path;
  if (mergeMPoints == MMPnone)
     path = mManip.computeShortestPath(bdries);
  else if ((mergeMPoints == MMPsimple) || (mergeMPoints == MMPonePoint)) {
    for(std::deque<std::deque<VertexID> >::iterator bdry = bdries.begin(); bdry != bdries.end(); ++bdry) {
      std::deque<VertexID> bdryMP;
      // first build multipoint list for the current boundary
      for(std::deque<VertexID>::const_iterator p = (*bdry).begin(); p != (*bdry).end(); ++p)
	if (isMultiPoint(*p))
	  bdryMP.push_back(*p);

      // if a multipoint has been found, the new target list for this boundary is the set of multipoints
      if (bdryMP.size() != 0)
	(*bdry) = bdryMP;
    }

    // then compute the shortest path between two boundaries
    path = mManip.computeShortestPath(bdries);

    // special case: if first and last points of the computed path are not multiple points, then
    // compute another path
    if (!isMultiPoint(path.front()) && !isMultiPoint(path.back()) && !cMesh.point(path.back()).sameLocation(cMesh.point(path.front()))) {
      VertexID p = getPointSameLocationInBoundary(path.front(), path.back());
      if (p < cMesh.getNbPoints()) {
	std::deque<VertexID> newPath = mManip.computeShortestPath(path.front(), p);
	if (newPath.size() != 0)
	  path = newPath;
      }
    }

  }

  // unstick path from boundary
  if ((mergeMPoints == MMPonePoint) || (mergeMPoints == MMPsimple)) {
    cMesh.setPointFlag(0);
    path = mManip.unstickPathFromBoundary(path, uMethod);
  }


  /* add it to the mesh cut */
  addPath(path);
  logPathes(*this);

  /** then compute multipoints if needed */
  if ((mergeMPoints == MMPonePoint) || (mergeMPoints == MMPsimple)) {
    computeMultiPoints(true);
  }


}

bool MeshCut::isMultiPoint(VertexID point) const {
  for(std::deque<VertexID>::const_iterator i = multiPoints.begin(); i != multiPoints.end(); ++i)
    if (*i == point)
      return true;

  return false;
}

VertexID MeshCut::getPointSameLocationInBoundary(VertexID point, VertexID otherInB) {
  cMesh.setPointFlag(0);
  cMesh.setTriangleFlag(0);
  cMesh.setBoundaryPointFlag(1);

  MeshManipulator mManip(cMesh, *lengthEdgeMethod);
  std::deque<VertexID> b = mManip.computeBoundaryCCList(otherInB, 1, 2, 0);

  for(std::deque<VertexID>::const_iterator i = b.begin(); i != b.end(); ++i)
    if (cMesh.point(*i).sameLocation(cMesh.point(point)))
      return *i;

  return cMesh.getNbPoints();
}



void MeshCut::fromFile(const std::string & filename, const IndentManager & iManager) {
  std::cout << iManager << " Loading pathes from file." << std::endl;

  std::deque<std::deque<VertexID> > pathes = MeshPathes::loadFromFile(filename);


  // then for each path
  for(std::deque<std::deque<VertexID> >::const_iterator path = pathes.begin(); path != pathes.end(); ++path) {
    // first check if path is inside mesh
    std::deque<VertexID>::const_iterator lasti = (*path).begin();
    for(std::deque<VertexID>::const_iterator i = (*path).begin(); i != (*path).end(); ++i) {
      if (*i >= cMesh.getNbPoints())
	throw Exception(std::string("Path cannot be applied in current mesh (wrong point id)."));
      if (lasti != i)
	if(!cMesh.point(*i).hasNeighbour(*lasti))
	  throw Exception(std::string("Path cannot be applied in current mesh (consecutive points are not neighbours)."));
      lasti = i;
    }

    // then add it to the mesh
    if((*path).size() != 0) {
      addPath(*path);
      logPathes(*this);
    }

  }

  std::cout << iManager << "  " << pathes.size() << " pathes loaded." << std::endl;
  std::cout << iManager << " Restarting cut algorithm." << std::endl;

}


bool MeshCut::inCutEdge(VertexID p1, VertexID p2) const {
  return inPathEdge(p1, p2);
}

std::deque<std::deque<VertexID> > MeshCut::computeBoundariesInside() {
  std::deque<std::deque<VertexID> > result;
  MeshManipulator mManip(cMesh);

  // first compute boundaries
  for(Mesh::const_point_iterator i = cMesh.point_begin(); i != cMesh.point_end(); ++i)
    if ((*i).getIsBoundary() && ((*i).getFlag() == 1)) // new boundary
      result.push_front(mManip.computeBoundaryCCList((*i).getId(), 1, 2));

  // then reinit flags
  for(std::deque<std::deque<VertexID> >::const_iterator bs = result.begin(); bs != result.end(); ++bs)
    for(std::deque<VertexID>::const_iterator b = (*bs).begin(); b != (*bs).end(); ++b)
      cMesh.point(*b).setFlag(1);

  return result;
}

void MeshCut::buildCutUsingCurvature(double alpha, int maxCut, const IndentManager iManager) {
  VertexID nbCut = 0;
  // first compute flags using curvature
  MeshPart selected = computeFlagUsingCurvature(alpha);

  std::cout << iManager << " Cutting using curvature [";
  std::cout.flush();

  // then cut mesh
  while (true) {
    bool multiB = false;
    std::deque<std::deque<VertexID> > boundaries = computeBoundariesInside();
    MeshPart mPart(cMesh);
    LengthEdgeInverseCurvature lengthEdge(cMesh);
    mPart.setLength(lengthEdge);
    // compute connected components of the "1" flag part.
    mPart.addPointsAndTriangles(1, 1);

    if (boundaries.size() > 1) { // if there is more than 2 connected components on the boundaries
      std::deque<VertexID> path;
      double pathLength = 0;

      // find shortest path between boundaries
      for(std::deque<std::deque<VertexID> >::const_iterator b1 = boundaries.begin();
	  b1 != boundaries.end(); ++b1)
	for(std::deque<std::deque<VertexID> >::const_iterator b2 = b1 + 1;
	    b2 != boundaries.end(); ++b2) {
	  std::deque<VertexID> newPath = mPart.getShortestPath(*b1, *b2);
	  double newPathLength = cMesh.getLength(newPath, lengthEdge);
	  if (newPathLength != 0) {
	    if (path.size() != 0)
	      pathLength = cMesh.getLength(path, lengthEdge);

	    if ((pathLength == 0) || (newPathLength < pathLength))
	      path = newPath;
	  }
	}

      // if found, add it
      if (path.size() > 1) {
	multiB = true;
	// add path
	addPath(path);
	// log it
	logPathes(*this);
	// update cMesh flags
      }
    }

    // if there is no path between two boundary parts
    if (!multiB) {
      bool modified = false;


      std::deque<MeshPart> mps = mPart.getCC();

      for(std::deque<MeshPart>::iterator mp = mps.begin(); mp != mps.end(); ++mp) {

	// compute number of real boundaries inside *mp
	VertexID nbBoundaries_t = 0;
	for(std::deque<std::deque<VertexID> >::const_iterator b1 = boundaries.begin();
	    b1 != boundaries.end(); ++b1)
	  if (((*b1).size() > 0) && ((*mp).hasPoint((*b1)[0])))
	    ++nbBoundaries_t;

	// get fundamental group size
	unsigned int fGroupSize_t = (*mp).getBettiNumber();

	if (!((fGroupSize_t == 0) || ((fGroupSize_t == 1) && (nbBoundaries_t == 1)))) {
	  (*mp).setLength(lengthEdge);
	  // first compute edge with largest local curvature
	  VertexID basePoint = cMesh.getNbPoints();
	  std::deque<VertexID> boundaries_t = (*mp).getBoundaryPoints();

	  // if there is boundaries inside connected component
	  if (boundaries_t.size() != 0) {
	    // compute farest point according to boundary location
	    (*mp).setLength(LengthEdgeEuclidean(cMesh));
	    (*mp).computeDijkstra(boundaries_t, 1);

	    basePoint = (*mp).getPointHighestLength();
	  }

	  if (basePoint == cMesh.getNbPoints()) { // otherwise, select point with highest edge curvature
	    basePoint = (*mp).getPointMaxEdgeCurvature();
	  }

	  (*mp).setLength(lengthEdge);
	  /* the function getShortestNonTrivialCycleFromPoint was not yet maintained and has been replaced
	     by getShortestNonSeparatingCycleFromPoint... It may be restored if needed */
	  std::deque<VertexID> path = (*mp).getShortestNonSeparatingCycleFromPoint(basePoint);

	  // then cut using path
	  if (path.size() > 1) {
	    // add path
	    addPath(path);
	    // log it
	    logPathes(*this);
	    modified = true;
	  }

	  if (modified)
	    break;
	}
      }

      if (!modified) // if mesh has not been modified, break
	break;

    }

    ++nbCut;
    if ((maxCut > 0) && (nbCut == (unsigned int) maxCut))
      break;

    std::cout << ".";
    std::cout.flush();

    // update cMesh flags
    selected.initInsideFlags();

    for(VertexID i = (*mesh).getNbPoints(); i < cMesh.getNbPoints(); ++i) // uint better than iterator
      cMesh.point(i).setFlag(1); // all added points were in original part flagged "1"
  }

  std::cout << "]" << std::endl;
}


MeshPart MeshCut::computeFlagUsingCurvature(double alpha) {
  MeshPart result(cMesh);
  // first init flag (all points are not yet seen)
  cMesh.setPointFlag(2);
  cMesh.setTriangleFlag(0);

  // then for each point
  for(Mesh::point_iterator i = cMesh.point_begin(); i != cMesh.point_end(); ++i) {
    int flag = (*i).getFlag();
    if (flag == 2) // if never seen before
      (*i).setFlag(0); // not selected
    else if (flag == 3) // if point has been preselected
      (*i).setFlag(1); // it is selected
    // then for each neighbour
    for(std::deque<VertexID>::const_iterator n = (*i).getNeighbours().begin();
	n != (*i).getNeighbours().end(); ++n)
      if (cMesh.point(*n).getFlag() >= 2) { // if it is a neighbour not yet seen with main loop
	std::pair<VertexID, VertexID> idTs = cMesh.findTriangles((*i).getId(), *n);
	if ((idTs.first < cMesh.getNbTriangles()) && (idTs.second < cMesh.getNbTriangles())) {
	  double angle = cMesh.computeAngle(idTs.first, idTs.second);
	  if ((angle > alpha) && (angle <= 2 * M_PI)) { // if curvature has good value
	    (*i).setFlag(1); // current point is selected
	    cMesh.point(*n).setFlag(3); // neighbour is preselected
	  }
	}
      }
    // set flag for "mesh" using "cMesh"
    if ((*i).getFlag() == 1) {
      result.addPoint((*i).getId());
      for(std::deque<VertexID>::const_iterator n = (*i).getTriangles().begin(); n != (*i).getTriangles().end(); ++n)
	cMesh.triangle(*n).setFlag(1); // triangle are selected

    }
  }

  // after, add the selected triangles and the ommited points
  for(Mesh::const_triangle_iterator t = cMesh.triangle_begin(); t != cMesh.triangle_end(); ++t)
    if ((*t).getFlag() == 1) {
      result.addTriangle((*t).getId());
      for(unsigned int i = 0; i < 3; ++i)
	if (cMesh.point((*t).getVertexId(i)).getFlag() == 0) {
	  result.addPoint((*t).getVertexId(i));
	  cMesh.point((*t).getVertexId(i)).setFlag(1);
	}
    }
  return result;
}

void MeshCut::buildRectangularCut(int nbCutMax, enum CuttingMethod cMethod,
				  enum MergeMPointsMethod mergeMPoints, enum UnstickMethod uMethod, enum OptimEC optimMethod,
				  const std::string & inputPathFile, const std::string & logMesh, const IndentManager iManager) {

  // first cut mesh using minimal cut
  unsigned int nbCutInit = buildMinimalCut(nbCutMax, cMethod, mergeMPoints, uMethod, optimMethod, inputPathFile, logMesh, iManager);
  unsigned int nbCut = nbCutInit;

  clock_t tstart = clock();
  bool updated = true;

  while (updated) {
    if ((nbCutMax != 0) && (nbCut == (unsigned int) nbCutMax)) {
      if (nbCut != nbCutInit)
	std::cout << iManager << " Break (" << (nbCutMax - nbCutInit)  << " cuts)" << std::endl;
      break;
    }
    updated = false;

    computeMultiPoints();

    std::cout << iManager << " There are " << multiPoints.size() << " multipoints." << std::endl;
    assert(multiPoints.size() % 2 == 0);

#ifndef NDEBUG
    for(Mesh::const_point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p)
      assert((*p).getFlag() == 0);
#endif

    for(std::deque<VertexID>::const_iterator mp = multiPoints.begin(); mp != multiPoints.end(); ++mp)
      if (cMesh.point(*mp).getFlag() == 0) {
	std::deque<VertexID> nbList = computeNMultiPoints(*mp);
	std::cout << iManager << "  Working on connected component containing #" << *mp << " (with " << nbList.size() << " multi points)" << std::endl;

	if (nbList.size() > 4) {
	  std::deque<VertexID> path = findBestCut(nbList);

	  if (path.size() >= 2) {
	    std::cout << iManager << "   A path between #" << path.front() << " and #" << path.back() << " where found. Length: " << cMesh.getLength(path) << std::endl;

	    path = cMeshManip.unstickPathFromBoundary(path, uMethod);

	    std::cout << iManager << "   After update cutting, length: " << cMesh.getLength(path) << std::endl;

	    addPath(path);

	    logPathes(*this);

	    if (logMesh != "")
	      logCMesh(logMesh);

	    ++nbCut;
	    updated = true;
	    break;
	  }
	  else
	  std::cout << iManager << "   Path not found." << std::endl;

	}
	else
	  std::cout << iManager << "   It is already a quadrilaterial." << std::endl;

	for(std::deque<VertexID>::const_iterator sameCC = nbList.begin(); sameCC != nbList.end(); ++sameCC)
	  cMesh.point(*sameCC).setFlag(1);

      }
  }

  std::cout << iManager << " Quadrangulation duration: " << ((clock() - tstart) / ((double) CLOCKS_PER_SEC)) << " seconds" << std::endl;


}

void MeshCut::computeMultiPoints(bool force) {
  std::deque<VertexID> newMultiPoints;
  MeshManipulator mManip((*mesh));

  cMesh.setPointFlag(0);
  // init flags for multipoints seen before
  for(std::deque<VertexID>::const_iterator p = multiPoints.begin(); p != multiPoints.end(); ++p)
    cMesh.point(*p).setFlag(1);

  for(Mesh::point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p) {
    bool ok = true;
    std::deque<VertexID> sameLoc = cMeshManip.getPointsSameLocation((*p).getId());
    VertexID nbSamePoint = sameLoc.size();
    // if multipoints has been seen before, only points with same location are selected
    if ((multiPoints.size() != 0) && !force) {
      ok = false;
      for(std::deque<VertexID>::const_iterator i = sameLoc.begin(); i != sameLoc.end(); ++i)
	if (cMesh.point(*i).getFlag() == 1) {
	  ok = true;
	  break;
	}
    }

    if (((nbSamePoint > 2) || ((nbSamePoint > 1) && !isFictivePoint((*p).getId()) && ((*mesh).point(getProjection((*p).getId())).getIsBoundary()) &&
			       (mManip.getPointsSameLocation(getProjection((*p).getId())).size() == 1))) && ok) {
      assert(cMesh.point((*p).getId()).getIsBoundary());
      newMultiPoints.push_back((*p).getId());
    }
  }

  // reset flags
  for(std::deque<VertexID>::const_iterator p = multiPoints.begin(); p != multiPoints.end(); ++p)
    cMesh.point(*p).setFlag(0);

  multiPoints = newMultiPoints;
}



std::deque<VertexID> MeshCut::computeNMultiPoints(VertexID point) {
  std::deque<VertexID> result;

  // set flags for multipoints not seen before
  for(std::deque<VertexID>::const_iterator mp = multiPoints.begin(); mp != multiPoints.end(); ++mp)
    if (cMesh.point(*mp).getFlag() == 0)
      cMesh.point(*mp).setFlag(2);


  // then walk arround the boundary
  VertexID idNextPoint = cMesh.getNextBPoint(point);
  VertexID idPoint = point;
  VertexID startingPoint = point;

  // go out the first "boundary arm"
  switch(cMesh.getNbNextBPoint(point)) {
  case 1:
    idNextPoint = cMesh.getNextBPointSingle(point);
    idPoint = point;

    while(cMesh.point(idNextPoint).getNbTriangles() == 0) {
      VertexID idNextPoint2 = cMesh.getNextBPointSingle(idNextPoint, idPoint);
      idPoint = idNextPoint;
      idNextPoint = idNextPoint2;
    }

    break;
  case 2:
    if (cMesh.point(point).getNbTriangles() == 0)
      throw Exception("computeNMultiPoints(1): first point is on a boundary arm, but not at the extremity.");
    // ok
    break;
  default:
    throw Exception("computeNMultiPoints(1): Wrong boundary topology.");
  }


  result.push_back(point);

  while(idNextPoint != startingPoint) {

    // if a start of boundary arm is detected, first go on this before walking arround the main loop
    if (cMesh.getNbNextBPoint(idNextPoint) != 2) {
      assert(cMesh.getNbNextBPoint(idNextPoint) == 3);
      if (cMesh.point(idNextPoint).getFlag() == 2)
	throw Exception("computeNMultiPoints(1): Cannot compute multipoints on a non-loop boundary.");

      VertexID idNextPointSingle = cMesh.getNextBPointSingle(idNextPoint);
      VertexID idPointSingle = idNextPoint;

      // go to the end of the arm
      while(cMesh.point(idNextPointSingle).getNbNeighbours() != 1) {
	if (cMesh.point(idNextPointSingle).getFlag() != 0)
	  throw Exception("computeNMultiPoints(1): a multipoint has been found on a boundary arm, but not at the extremity.");
	VertexID idNextPointSingle2 = cMesh.getNextBPointSingle(idNextPointSingle, idPointSingle);
	idPointSingle = idNextPointSingle;
	idNextPointSingle = idNextPointSingle2;
      }

      // if a multipoint has been found, add it to the result list
      if (cMesh.point(idNextPointSingle).getFlag() != 0)
	result.push_back(idNextPointSingle);
    }

    if (cMesh.point(idNextPoint).getFlag() == 2)
      result.push_back(idNextPoint);

    VertexID idNextPoint2 = cMesh.getNextBPoint(idNextPoint, idPoint);
    idPoint = idNextPoint;
    idNextPoint = idNextPoint2;
  }

  // reset flags
  for(std::deque<VertexID>::const_iterator mp = multiPoints.begin(); mp != multiPoints.end(); ++mp)
    if (cMesh.point(*mp).getFlag() == 2)
      cMesh.point(*mp).setFlag(0);

  return result;
}

std::deque<VertexID> MeshCut::findBestCut(const std::deque<VertexID> & multiPointsInside) {
  // TODO: the shortest path computed between two points on this function cannot be smaller than the same
  // path computed at the previous step (if it's not the first cutting step). This result may be saved to
  // increase the computing speed.
  std::deque<VertexID> path;
  double length_t = std::numeric_limits<double>::max();


  // for each multipoint on the same connected component
  for(std::deque<VertexID>::const_iterator mp = multiPointsInside.begin(); mp != multiPointsInside.end(); ++mp) {
    std::deque<VertexID> mpList;
    mpList.push_back(*mp);
    // compute list of possible extremities
    std::deque<VertexID> possibleNb = computePossibleExtremities(*mp, multiPointsInside);
    // then compute the shortest path between mp and the possibleNb
    std::deque<VertexID> newPath = cMeshManip.computeShortestPath(mpList, possibleNb);
    if (newPath.size() != 0) {
      double newLength = cMesh.getLength(newPath);
      if (newLength < length_t) {
	path = newPath;
	length_t = newLength;
      }
    }
  }

  return path;
}

std::deque<VertexID> MeshCut::computePossibleExtremities(VertexID idPoint, const std::deque<VertexID> & allPoints) {
  std::deque<VertexID> result;
  assert(allPoints.size() > 4);
  assert(allPoints.size() % 2 == 0);

  // first find idPoint
  std::deque<VertexID>::const_iterator mp = allPoints.begin();
  while(*mp != idPoint) {
    ++mp;
    assert(mp != allPoints.end());
  }

  std::deque<VertexID>::const_iterator mpStart = mp;

  // add all odd points
  bool even = false;
  for(; mp != allPoints.end(); ++mp) {
    ++mp;
    if (mp != allPoints.end())
      result.push_back(*mp);
    else {
      even = true;
      break;
    }
  }

  for(mp = allPoints.begin(); mp != mpStart; ++mp) {
    if (even) {
      even = false;
      result.push_back(*mp);
      ++mp;
      if (mp == mpStart)
	break;
    }
    ++mp;
    if (mp == mpStart)
      break;
    result.push_back(*mp);
  }

  // then remove first and last: they are allready neighbours on the boundary
  result.pop_back();
  result.pop_front();

  assert(result.size() != 0);

  return result;
}

void MeshCut::setFlagOfBoundaryNeighbours(VertexID idP, int flag) {
  Point3D & p = cMesh.point(idP);

  // point should have more than 2 points
  if (p.getNbNeighbours() <= 2)
    return;

  // for each neighbour, if it's a boundary point, flag it with flag value
  bool hasInsideNb = false;
  if (flag != 0)
    for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin();
	nb != p.getNeighbours().end(); ++nb)
      if(cMesh.point(*nb).getIsBoundary()) {
	cMesh.point(*nb).setFlag(flag);
      }
      else
	hasInsideNb = true;

  // if point has not neighbour inside mesh, set all flags to 0
  if (!hasInsideNb)
    for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin();
	nb != p.getNeighbours().end(); ++nb)
      cMesh.point(*nb).setFlag(0);

}

bool MeshCut::isExportFormat(const std::string & fileName) const {
  return (FileTools::extension(fileName) == "4ms");
}

void MeshCut::exportToFile(const std::string & fileName) {
  if ((*mesh).getNbPoints() == 0)
    throw ExceptionEmptyData();

  std::ofstream outfile(fileName.c_str(), std::ios::out);
  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << toString4MS();

  outfile.close();
}

std::string MeshCut::toString4MS() {
  bool first = true;
  std::ostringstream result;

  std::ostringstream pointsTxt;
  VertexID nbPoint3D = 0;
  VertexID * idPointIn = new VertexID[cMesh.getNbPoints()];

  computeMultiPoints();


  cMesh.setPointFlag(0);
  VertexID idP = 0;
  for(Mesh::const_point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p)
    if ((*p).getFlag() ==  0) {
      VertexID id = (*p).getId();
      std::deque<VertexID> samePoints = cMeshManip.getPointsSameLocation(id);
      ++nbPoint3D;
      pointsTxt << (*p).getX() << " " <<  (*p).getY() << " " << (*p).getZ() << " " << isFictivePoint(id);
      if (first) {
	first = false;
	pointsTxt << " // 3d location and \"original\" flag";
      }
      pointsTxt << std::endl;

      for(std::deque<VertexID>::const_iterator c2 = samePoints.begin(); c2 != samePoints.end(); ++c2) {
	idPointIn[*c2] = idP;
	cMesh.point(*c2).setFlag(1);
      }
      ++idP;
    }

  result << nbPoint3D << " // number of 3d points" << std::endl;
  result << pointsTxt.str();

  std::deque<MeshPart> ccs = cMesh.getConnectedComponents();
  result << ccs.size() << " // number of patches" << std::endl;

  bool firstFlag = true;
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc) {
    VertexID * idPointInCC = new VertexID[cMesh.getNbPoints()];

    first = true;
    result << (*cc).getNbPoints() << " // number of points in patch" << std::endl;
    VertexID idPCC = 0;
    for(Mesh::const_point_iterator p = cMesh.point_begin(); p != cMesh.point_end(); ++p)
      if ((*cc).isInsidePoint((*p).getId())) {
	idPointInCC[(*p).getId()] = idPCC;
	result << idPointIn[(*p).getId()];
	if (isCornerPoint((*p).getId()))
	  result << " c";
	else if ((*p).getIsBoundary())
	  result << " b";
	else
	  result << " i";
	if (first) {
	  first = false;
	  result << " // id of the point on the 3d list, and flag";
	  if (firstFlag) {
	    firstFlag = false;
	    result << " (i: inside, b: boundary; c: corner)";
	  }
	}
	result << std::endl;
	++idPCC;
    }

    first = true;
    result << (*cc).getNbTriangles() << " // number of triangles in patch" << std::endl;

    for(Mesh::const_triangle_iterator p = cMesh.triangle_begin(); p != cMesh.triangle_end(); ++p)
      if ((*cc).isInsideTriangle((*p).getId())) {
	result << idPointInCC[(*p).getP1()] << " " << idPointInCC[(*p).getP2()] << " " << idPointInCC[(*p).getP3()];
	if (first) {
	  first = false;
	  result << " // id of the points in the patch";
	}
	result << std::endl;
      }

    delete [] idPointInCC;
  }

  delete [] idPointIn;

  return result.str();
}


bool MeshCut::isFictivePoint(VertexID idPoint) const {
  // a new point that projection is himself
  return (idPoint >= (*mesh).getNbPoints()) && ((getProjection(idPoint) == idPoint) || (isFictivePoint(getProjection(idPoint))));
}

bool MeshCut::isCornerPoint(VertexID idPoint) const {
  for(std::deque<VertexID>::const_iterator p = multiPoints.begin(); p != multiPoints.end(); ++p)
    if (*p == idPoint)
      return true;

  return false;
}

std::deque<VertexID> MeshCut::getShortestNonSeparatingCycleFromPointsUnstick(const std::deque<VertexID> & points, enum UnstickMethod uMethod, double minLength, unsigned int halfSizeMiddle) {
  MeshManipulator mManip(cMesh, *lengthEdgeMethod);
  std::deque<VertexID> result;
  CycleDescriptor cDesc = getShortestNonSeparatingCycleDescFromPoints(points, minLength, false);

  if (!cDesc.getIsValid()) {
    return result;
  }

  const std::deque<VertexID> & prePath = cDesc.getPath();

  // build the middle path
  std::deque<VertexID> middlePath;
  middlePath.push_back(cDesc.getTargetPoint());

  // find target point in prePath
  VertexID idTarget = 0;
  while(prePath[idTarget] != cDesc.getTargetPoint()) { assert(idTarget < prePath.size()); ++idTarget; }

  // adding points before idTarget
  if (idTarget != 0) {
    VertexID id = idTarget - 1;
    while(/*!cMesh.point(prePath[id]).getIsBoundary() && */(id != 0) && (middlePath.size() != halfSizeMiddle + 1)) {
      middlePath.push_front(prePath[id]);
      --id;
    }
  }

  // adding points after idTarget
  if (idTarget != prePath.size() - 1) {
    VertexID id = idTarget + 1;
    while(/*!cMesh.point(prePath[id]).getIsBoundary() && */(id != prePath.size() - 1) && (middlePath.size() != halfSizeMiddle * 2 + 1)) {
      assert(id < prePath.size());
      middlePath.push_back(prePath[id]);
      ++id;
    }
  }

  // then build first part
  std::deque<VertexID> sPoint;
  std::deque<VertexID> firstPath = mManip.computeShortestPath(cDesc.getBasePoint(), middlePath.front());

  // unstick it
  cMesh.setPointFlag(0);
  std::deque<VertexID> firstPathUnstick = mManip.unstickPathFromBoundary(firstPath, uMethod);
  computeMultiPoints();

  // set flags for the allready selected points
  cMesh.setPointFlag(0);
  for(std::deque<VertexID>::const_iterator fp = firstPathUnstick.begin(); fp != firstPathUnstick.end(); ++fp)
    cMesh.point(*fp).setFlag(1);
  for(std::deque<VertexID>::const_iterator fp = middlePath.begin(); fp != middlePath.end(); ++fp)
    cMesh.point(*fp).setFlag(1);
  cMesh.point(middlePath.back()).setFlag(0);
  cMesh.point(cDesc.getBasePoint()).setFlag(0);

  // then build last part
  std::deque<VertexID> fPoint;
  std::deque<VertexID> lastPath = mManip.computeShortestPathInCC(middlePath.back(), cDesc.getBasePoint(), 0);

  // and unstick it
  std::deque<VertexID> lastPathUnstick = mManip.unstickPathFromBoundary(lastPath);

  // then build the final path
  result = firstPathUnstick;


  assert(result.back() == middlePath.front());

  for(std::deque<VertexID>::const_iterator i = middlePath.begin(); i != middlePath.end(); ++i)
    if (i != middlePath.begin())
      result.push_back(*i);
  assert(result.back() == lastPathUnstick.front());
  for(std::deque<VertexID>::const_iterator i = lastPathUnstick.begin(); i != lastPathUnstick.end(); ++i)
    if (i != lastPathUnstick.begin())
      result.push_back(*i);

  return result;
}

Mesh & MeshCut::cutMeshUsingNLoop(VertexID basePoint, unsigned int nbPaths) {
  buildNLoopCut(basePoint, nbPaths);
  return cutMesh();
}

void MeshCut::buildNLoopCut(VertexID basePoint, unsigned int nbPaths) {
  NLoop nl = getShortestNLoop(basePoint, nbPaths);

  for(std::vector<std::vector<VertexID> >::const_iterator p = nl.getPaths().begin(); p != nl.getPaths().end(); ++p) {
    std::deque<VertexID> deq((*p).size());
    copy((*p).begin(), (*p).end(), deq.begin());
    addPathSplitBefore(deq);
    logPathes(*this);
  }
}


void MeshCut::buildSimpleCutDistance(VertexID basePoint, VertexID nbPoints_t) {
  lengthPathBP lpath;
  WaveFront wf(*mesh);

  computeDijkstra(basePoint);

  lpath.length = length;
  lpath.mesh = mesh;

  // sort points using dijkstra length
  std::vector<VertexID> sList;
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    if (length[i] != -1) {
      sList.push_back(i);
    }
  sort(sList.begin(), sList.end(), lpath);
  if (nbPoints_t > 0)
    sList.erase(sList.begin() + nbPoints_t, sList.end());

  addFront(wf.growSurfaceFull(sList));
}

void MeshCut::buildSimpleCut(VertexID basePoint, TriangleID nbTriangles) {
  WaveFront wf(*mesh);

  addFront(wf.growSurfaceTriangleFull(basePoint, nbTriangles));
}


void MeshCut::buildCylinderCutUsingNLoops(const NLoopEvaluator & nLEvaluator, unsigned int aritymin, unsigned int aritymax, double temp0, unsigned int step_size, double temp_coeff, double accept_threshold, double temp_min, enum SimulatedAnnealingNLoop::SAMethod method, const std::string & logMesh, const Taglut::IndentManager iManager) {
  VertexID basePoint;
  std::cout << iManager << "Cutting using NLoops" << std::endl;
  // first clear the current object
  (*this).clear();
  projection.clear();

  bool inprogress = true;

  while(inprogress) {
    inprogress = false;
    MeshPart mPart(cMesh, true);
    std::deque<MeshPart> ccs = mPart.getCC();
    std::cout << iManager << "Connected components: " << ccs.size() << std::endl;
    for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc) {

      const unsigned int genus = (*cc).getGenus();
      const unsigned int nbBoundaries_t = (*cc).getNbBoundaries();
      std::cout << iManager << " genus: " << genus << ", nb boundaries: " << nbBoundaries_t << std::endl;
      std::cout << iManager << " points: " << (*cc).getNbPoints() << ", triangles: " << (*cc).getNbTriangles() << std::endl;
      if ((genus == 1) && ((nbBoundaries_t == 0))) {
	VertexID basePoint_t = (*cc).getNonBoundaryPoint();
	std::cout << iManager << "Cutting a torus using the shortest non separating cycle" << std::endl;
	inprogress = true;
	std::deque<VertexID> path = mPart.getShortestNonSeparatingCycleFromPoint(basePoint_t);
	VertexID nbPbefore = cMesh.getNbPoints();
	addPathSplitBefore(path);
	if(nbPbefore == cMesh.getNbPoints())
	  throw Exception("buildCylinderCutUsingNLoops(.): wrong NLoop");
	break;
      }
      else if ((genus != 0) || (nbBoundaries_t >= 3)) {
	inprogress = true;
	basePoint = (*cc).getNonBoundaryPoint();
	if (basePoint == cMesh.getNbPoints())
	  break;
	std::cout << iManager << "Cutting a non torus surface using a 3-loop approximated by simulated annealing." << std::endl;
	SimulatedAnnealingNLoop simAnnealing(cMesh, nLEvaluator, temp0, step_size, temp_coeff, accept_threshold, temp_min, method);
	LengthEdge * lE = (*lengthEdgeMethod).clone(cMesh);
	simAnnealing.setLength(*lE);
	const unsigned int aritymaxtopo = 2 * genus + nbBoundaries_t;
	NLoop nloop = simAnnealing.approximateShortestValidNLoop(aritymin, aritymax > aritymaxtopo ? aritymaxtopo : aritymax, basePoint);
	std::cout << "NLoop: " << cMesh.getStringFromNLoop(nloop);
	minLocalBasePoints.push_back(nloop.getBasePoint());
	minLocalBasePoints.push_back(nloop.getLastPoint());
	addNLoop(nloop);
	delete lE;
	if (logMesh != "")
	  logCMesh(logMesh);
	break;
      }
    }
  }

}

void MeshCut::sortBySide(MeshPart & cc, const std::vector<VertexID> & mpInsideCC, VertexID sideA[2], VertexID sideB[2]) {
  // first select a boundary of the cylinder
  std::vector<VertexID> boundary = cc.getBoundaryFromBPoint(mpInsideCC.front());


  // then build the two set of points
  cMesh.setPointFlag(0);
  for(std::vector<VertexID>::const_iterator b = boundary.begin(); b != boundary.end(); ++b)
    cMesh.point(*b).setFlag(1);
  unsigned int nbA, nbB;
  nbA = nbB = 0;
  for(std::vector<VertexID>::const_iterator mp = mpInsideCC.begin(); mp != mpInsideCC.end(); ++mp) {
    if (cMesh.point(*mp).getFlag() == 1) {
      if (nbA == 2)
	throw Exception("buildRectangularCutFromCylindricalTilingSimple(): wrong number of multipoints on A face.");
      sideA[nbA] = *mp;
      ++nbA;
    }
    else {
      if (nbB == 2)
	throw Exception("buildRectangularCutFromCylindricalTilingSimple(): wrong number of multipoints on B face.");
      sideB[nbB] = *mp;
      ++nbB;
    }
  }
  cMesh.setPointFlag(0);

}

void MeshCut::buildRectangularCutFromCylindricalTilingSimple(const Taglut::IndentManager iManager) {
  std::vector<std::deque<VertexID> > paths;

  computeMultiPoints();

  std::cout << iManager << " There are " << multiPoints.size() << " multipoints." << std::endl;

  MeshPart mPart(cMesh, true);
  std::deque<MeshPart> ccs = mPart.getCC();
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc) {
    if (((*cc).getGenus() != 0) || ((*cc).getNbBoundaries() != 2))
      throw Exception("buildRectangularCutFromCylindricalTilingSimple(): one of the connected components is not a cylinder.");
    std::vector<VertexID> mpInsideCC;
    for(std::deque<VertexID>::const_iterator m = multiPoints.begin(); m != multiPoints.end(); ++m)
      if ((*cc).isInsidePoint(*m))
	mpInsideCC.push_back(*m);

    if (mpInsideCC.size() == 4) {
      VertexID sideA[2];
      VertexID sideB[2];

      sortBySide(*cc, mpInsideCC, sideA, sideB);

      // then compute length from each of the points of the A face.
      MeshManipulator mManip1(cMesh);
      mManip1.computeDijkstra(sideA[0], 0);
      MeshManipulator mManip2(cMesh);
      mManip2.computeDijkstra(sideA[1], 0);


      // then found the shortest path
      unsigned int firstA = cMesh.getNbPoints(), firstB = cMesh.getNbPoints();
      double length_t = -1;
      for(unsigned char i = 0; i < 2; ++i) {
	if ((length_t == -1) || (length_t > mManip1.getLength(sideB[i]))) {
	  firstA = 0; firstB = i;
	  length_t = mManip1.getLength(sideB[i]);
	}
	if ((length_t == -1) || (length_t > mManip2.getLength(sideB[i]))) {
	  firstA = 1; firstB = i;
	  length_t = mManip2.getLength(sideB[i]);
	}
      }
      assert(firstA != cMesh.getNbPoints());
      assert(firstB != cMesh.getNbPoints());

      // and build it
      std::deque<VertexID> path;
      path.push_back(sideB[firstB]);
      while(path.back() != sideA[firstA]) {
	if (firstA == 0)
	  path.push_back(mManip1.getPredPoint(path.back()));
	else
	  path.push_back(mManip2.getPredPoint(path.back()));
      }
      paths.push_back(path);

      firstA = (firstA + 1) % 2;
      firstB = (firstB + 1) % 2;

      // then build a MeshMap according to the distance to this path.
      (*cc).computeDijkstra(path, 0);
      const double iso = ((*cc).getLength(sideA[firstA]) + (*cc).getLength(sideB[firstB])) / 2;

      MeshMap mMap(cMesh);
      mMap.setValuesFromLengths(*cc);

      // then compute the other path according to this map.
      LengthEdgeEuclidean lE(cMesh);
      LengthIsocurve distPath(mMap, iso, lE);
      mManip2.setLength(distPath);
      mManip2.computeDijkstra(sideA[firstA], 0);
      if (mManip2.getLength(sideB[firstB]) < 0)
	throw Exception("buildRectangularCutFromCylindricalTilingSimple(): Cannot compute a path without walking on the boundary.");
      path.clear();
      path.push_back(sideB[firstB]);
      while(path.back() != sideA[firstA]) {
	path.push_back(mManip2.getPredPoint(path.back()));
      }
      paths.push_back(path);

    }
    else if (mpInsideCC.size() == 2) {
      std::cout << "Not yet implemented" << std::endl;
    }
    else if (mpInsideCC.size() == 0)
      std::cout << "Not yet implemented" << std::endl;
    else {
      std::cout << "Number of multipoints: " << mpInsideCC.size() << std::endl;
      throw Exception("buildRectangularCutFromCylindricalTilingSimple(): wrong number of multipoints.");
    }

  }

  for(std::vector<std::deque<VertexID> >::iterator path = paths.begin(); path != paths.end(); ++path) {
    MeshManipulator mManip(cMesh);
    correctingPath(*path);
    *path = mManip.unstickPathFromBoundary(*path, MeshManipulator::UMNoDijkstra);
    addPath(*path);
  }

}

void MeshCut::buildRectangularCutFromCylindricalTilingABF(const Taglut::IndentManager iManager, const std::string & logMesh) {
  std::vector<std::deque<VertexID> > paths;

  computeMultiPoints();

  std::cout << iManager << " There are " << multiPoints.size() << " multipoints." << std::endl;

  // compute the unfolding
  ABFSolver abfs(cMesh);
  abfs.processUnfolding(1e-7, 30);
  WeightedEdges l = abfs.getLengths();
  MeshManipulator mManip(cMesh, LengthWeightedEdges(l));


  MeshPart mPart(cMesh, true);
  std::deque<MeshPart> ccs = mPart.getCC();
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc) {
    if (((*cc).getGenus() != 0) || ((*cc).getNbBoundaries() != 2))
      throw Exception("buildRectangularCutFromCylindricalTilingABF(): one of the connected components is not a cylinder.");
    std::vector<VertexID> mpInsideCC;
    for(std::deque<VertexID>::const_iterator m = multiPoints.begin(); m != multiPoints.end(); ++m)
      if ((*cc).isInsidePoint(*m))
	mpInsideCC.push_back(*m);

    if (mpInsideCC.size() == 4) {
      VertexID sideA[2];
      VertexID sideB[2];

      sortBySide(*cc, mpInsideCC, sideA, sideB);

      std::deque<VertexID> p11 = mManip.computeShortestPath(sideA[0], sideB[0]);
      std::deque<VertexID> p12 = mManip.computeShortestPath(sideA[1], sideB[1]);

      std::deque<VertexID> p21 = mManip.computeShortestPath(sideA[1], sideB[0]);
      std::deque<VertexID> p22 = mManip.computeShortestPath(sideA[0], sideB[1]);

      if (cMesh.getLength(p11) + cMesh.getLength(p12) < cMesh.getLength(p21) + cMesh.getLength(p22)) {
	paths.push_back(p11);
	paths.push_back(p12);
      }
      else {
	paths.push_back(p21);
	paths.push_back(p22);
      }

    }
    else if (mpInsideCC.size() == 2) {
      std::cout << "Not yet implemented" << std::endl;
    }
    else if (mpInsideCC.size() == 0)
      std::cout << "Not yet implemented" << std::endl;
    else {
      std::cout << "Number of multipoints: " << mpInsideCC.size() << std::endl;
      throw Exception("buildRectangularCutFromCylindricalTilingABF(): wrong number of multipoints.");
    }

  }

  for(std::vector<std::deque<VertexID> >::iterator path = paths.begin(); path != paths.end(); ++path) {
    MeshManipulator mManip_t(cMesh);
    correctingPath(*path);
    *path = mManip_t.unstickPathFromBoundary(*path, MeshManipulator::UMNoDijkstra);
    addPath(*path);
    if (logMesh != "")
      logCMesh(logMesh);
  }

}

void MeshCut::buildRectangularCutFromCylindricalTilingFloater(const Taglut::IndentManager iManager, const std::string & logMesh) {
  std::vector<std::deque<VertexID> > paths;

  computeMultiPoints();

  std::cout << iManager << " There are " << multiPoints.size() << " multipoints." << std::endl;


  MeshPart mPart(cMesh, true);
  std::deque<MeshPart> ccs = mPart.getCC();
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc) {
    if (((*cc).getGenus() != 0) || ((*cc).getNbBoundaries() != 2))
      throw Exception("buildRectangularCutFromCylindricalTilingFloater(): one of the connected components is not a cylinder.");
    std::vector<VertexID> mpInsideCC;
    for(std::deque<VertexID>::const_iterator m = multiPoints.begin(); m != multiPoints.end(); ++m)
      if ((*cc).isInsidePoint(*m))
	mpInsideCC.push_back(*m);

    if (mpInsideCC.size() == 4) {
      VertexID sideA[2];
      VertexID sideB[2];

      sortBySide(*cc, mpInsideCC, sideA, sideB);

      Mesh ccMesh = (*cc).buildCropMesh();
      IDTranslator idTranslator(*cc);

      VertexID sideALocal[2] = { idTranslator.g2l(sideA[0]), idTranslator.g2l(sideA[1]) };
      VertexID sideBLocal[2] = { idTranslator.g2l(sideB[0]), idTranslator.g2l(sideB[1]) };

      // first parameterization
      CylinderParameterization cParam;
      Mesh ccMeshParam1 = cParam.parameterize3DFixedPoints(ccMesh, sideALocal, sideBLocal);
      MeshManipulator mManipCCParam1(ccMeshParam1);

      std::deque<VertexID> p11 = idTranslator.l2gVector(mManipCCParam1.computeShortestPath(sideALocal[0], sideBLocal[0]));
      std::deque<VertexID> p12 = idTranslator.l2gVector(mManipCCParam1.computeShortestPath(sideALocal[1], sideBLocal[1]));

      // second parameterization
      {
	VertexID t = sideALocal[0];
	sideALocal[0] = sideALocal[1];
	sideALocal[1] = t;
      }
      Mesh ccMeshParam2 = cParam.parameterize3DFixedPoints(ccMesh, sideALocal, sideBLocal);
      MeshManipulator mManipCCParam2(ccMeshParam2);

      std::deque<VertexID> p21 = idTranslator.l2gVector(mManipCCParam2.computeShortestPath(sideALocal[0], sideBLocal[0]));
      std::deque<VertexID> p22 = idTranslator.l2gVector(mManipCCParam2.computeShortestPath(sideALocal[1], sideBLocal[1]));

      if (cMesh.getLength(p11) + cMesh.getLength(p12) < cMesh.getLength(p21) + cMesh.getLength(p22)) {
	paths.push_back(p11);
	paths.push_back(p12);
      }
      else {
	paths.push_back(p21);
	paths.push_back(p22);
      }
    }
    else if (mpInsideCC.size() == 2) {
      std::cout << "Not yet implemented" << std::endl;
    }
    else if (mpInsideCC.size() == 0)
      std::cout << "Not yet implemented" << std::endl;
    else {
      std::cout << "Number of multipoints: " << mpInsideCC.size() << std::endl;
      throw Exception("buildRectangularCutFromCylindricalTilingFloater(): wrong number of multipoints.");
    }

  }

  for(std::vector<std::deque<VertexID> >::iterator path = paths.begin(); path != paths.end(); ++path) {
    MeshManipulator mManip(cMesh);
    correctingPath(*path);
    *path = mManip.unstickPathFromBoundary(*path, MeshManipulator::UMNoDijkstra);
    addPath(*path);
    if (logMesh != "")
      logCMesh(logMesh);
  }
}

void MeshCut::buildRectangularCutUsingNLoops(const NLoopEvaluator & nLEvaluator, unsigned int aritymin, unsigned int aritymax, double temp0, unsigned int step_size, double temp_coeff,
					     double accept_threshold, double temp_min,
					     enum SimulatedAnnealingNLoop::SAMethod method,
					     enum UnstickMethod,
					     enum QuadMethod qmethod,
					     const std::string & logMesh,
					     const Taglut::IndentManager iManager) {
  buildCylinderCutUsingNLoops(nLEvaluator, aritymin, aritymax, temp0, step_size, temp_coeff, accept_threshold, temp_min, method, logMesh, iManager);

  if (qmethod == QMSimple)
    buildRectangularCutFromCylindricalTilingSimple(iManager);
  else if (qmethod == QMABF)
    buildRectangularCutFromCylindricalTilingABF(iManager, logMesh);
  else // if (qmethod == QMFloater)
    buildRectangularCutFromCylindricalTilingFloater(iManager, logMesh);

}
