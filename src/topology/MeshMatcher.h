/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_MATCHER
#define MESH_MATCHER

#include <deque>

namespace Taglut {
  class Mesh;

  /**
     @class MeshMatcher

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-01-22
     @brief Compute matching between similar meshes (similar meshes means only few
     modifications has been proceed from one to optain the second: randomly moving points
     OR removing points OR adding points).
  */
  class MeshMatcher : public std::deque<std::pair<unsigned int, unsigned int> > {

  public:
    /**
       Default constructor
    */
    MeshMatcher(const Mesh & mesh1, const Mesh & mesh2);

  };
}

#endif
