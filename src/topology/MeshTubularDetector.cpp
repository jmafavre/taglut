/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <list>
#include <iterator>
#include <algorithm>

#include "MeshTubularDetector.h"
#include "MeshManipulator.h"

using namespace Taglut;


MeshTubularDetector::MeshTubularDetector(Mesh & mesh_t) : MeshManipulator(mesh_t) {

}


const MeshMapT<unsigned int> & MeshTubularDetector::computeLocalFeatures(double radius) {
  (*mesh).setPointFlag(0);
  nbBdNeighbourhood.setMesh(*mesh, 0);
  for(Mesh::point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p) {
    // even flag: boundary
    setFlagsInBall(*p, radius, *p);
    while(cleanBoundariesInBall(*p)) { }
    setTriangleFlagsInBall(*p);
    nbBdNeighbourhood[(*p).getId()] = getNbBoundariesInsideBall(*p);
    resetFlagsInBall(*p);
  }

  return nbBdNeighbourhood;
}

void MeshTubularDetector::setFlagsInBall(const Point3D & center, double radius, const Point3D & point) {
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
    Point3D & pointNb = (*mesh).point(*nb);
    if ((pointNb.getFlag() == 0) && (pointNb.distance(center) <= radius)) {
      if (hasNeighbourOutBall(center, radius, pointNb))
	pointNb.setFlag(2);
      else
	pointNb.setFlag(1);
      setFlagsInBall(center, radius, pointNb);
    }
  }
}

bool MeshTubularDetector::hasNeighbourOutBall(const Point3D & center, double radius, const Point3D & point) const {
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb)
    if ((*mesh).point(*nb).distance(center) > radius)
      return true;
  return false;
}

bool MeshTubularDetector::cleanBoundariesInBall(Point3D & point) {
  bool result = false;
  if (point.getFlag() == 1) {
    point.setFlag(3);
  }
  else if (point.getFlag() == 2) { // we are in a boundary point
    point.setFlag(4);
    // compute the number of outside parts arround the current point
    unsigned int nbCCOutside = 0;
    unsigned int nbOutside = 0;
    bool start = true;
    for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
      if ((*mesh).point(*nb).getFlag() == 0) {
	++nbOutside;
	if (start)
	  ++nbCCOutside;
	start = false;
      }
      else {
	start = true;
      }
    }
    if (((*mesh).point(point.getNeighbours().front()).getFlag() == 0) &&
	((*mesh).point(point.getNeighbours().back()).getFlag() == 0))
      --nbCCOutside;
    // if the point has more than one outside part arround it, the point became an output point
    if ((nbCCOutside > 1) || (point.getNbNeighbours() - nbOutside == 1)) {
      result = true;
      point.setFlag(0);
      for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb)
	if (((*mesh).point(*nb).getFlag() == 3) || ((*mesh).point(*nb).getFlag() == 1))
	  (*mesh).point(*nb).setFlag(4);
    }
  }

  // recursive call
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
    Point3D & pointNb = (*mesh).point(*nb);
    if ((pointNb.getFlag() != 0) && (pointNb.getFlag() < 3))
      result = cleanBoundariesInBall(pointNb) or result;
  }

  return result;
}

void MeshTubularDetector::setTriangleFlagsInBall(Point3D & point) {
  if (point.getFlag() == 3)
    point.setFlag(5);
  else if (point.getFlag() == 4)
    point.setFlag(6);
  // set triangles' flags
  for(std::deque<TriangleID>::const_iterator t = point.getTriangles().begin(); t != point.getTriangles().end(); ++t) {
    Triangle & triangle = (*mesh).triangle(*t);
    if (((*mesh).point(triangle.getP1()).getFlag() != 0) &&
	((*mesh).point(triangle.getP2()).getFlag() != 0) &&
	((*mesh).point(triangle.getP3()).getFlag() != 0))
      triangle.setFlag(1);
  }

  // recursive call
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
    Point3D & pointNb = (*mesh).point(*nb);
    if ((pointNb.getFlag() != 0) && (pointNb.getFlag() < 5))
      setTriangleFlagsInBall(pointNb);
  }
}

void MeshTubularDetector::resetFlagsInBall(const Point3D & point) {
  // first set triangles' flags
  for(std::deque<TriangleID>::const_iterator t = point.getTriangles().begin(); t != point.getTriangles().end(); ++t) {
    Triangle & triangle = (*mesh).triangle(*t);
    if (triangle.getFlag() != 0) {
      triangle.setFlag(0);
    }
  }

  // then set points' flags
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
    Point3D & pointNb = (*mesh).point(*nb);
    if (pointNb.getFlag() != 0) {
      pointNb.setFlag(0);
      resetFlagsInBall(pointNb);
    }
  }
}

unsigned int MeshTubularDetector::getNbBoundariesInsideBall(Point3D & point) {
  unsigned int result = 0;
  if (point.getFlag() == 5)
    point.setFlag(7);
  // if it's a boundary point, compute the connected component modifying the flags of the points to 5, using 1 as
  // the value of the triangles inside the wanted area for edge connection.
  else if (point.getFlag() == 6) {
    result = 1;
    computeCCUsingPoints(point.getId(), 6, 20, 1);
  }


  // recursive call
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
    assert(*nb < (*mesh).getNbPoints());
    Point3D & pointNb = (*mesh).point(*nb);
    if ((pointNb.getFlag() != 0) && (pointNb.getFlag() != 7) && (pointNb.getFlag() != 20))
      result += getNbBoundariesInsideBall(pointNb);
  }

  return result;
}
