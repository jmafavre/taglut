/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POLYGON
#define POLYGON

#include <deque>
#include "Mapping2D3D.h"

namespace Taglut {
  /**
     @class Polygon
     @brief Polygon defined by a list of points in the plane.
  */
  class Polygon : public std::deque<VertexID> {
  private:
    /**
       mapping where polygon is defined
    */
    Mapping2D3D & mapping;

    /**
       true if the polygon is convex
    */
    bool isConvex;


    /**
       half part of the convex hull algorithm
    */
    std::deque<VertexID> computeConvexHullHalfPart(const std::deque<VertexID> & inList,
						   Point2D & pmin, Point2D & pmax, bool bottom) const;

    /**
       Compute area of the current polygon using triangles
    */
    double computeAreaUsingTriangles();

  public:
    /**
       Default constructor
    */
    inline Polygon(Mapping2D3D & mapping_t) : mapping(mapping_t), isConvex(false) {}

    /**
       Constructor from a list of points
    */
    inline Polygon(Mapping2D3D & mapping_t, const std::deque<VertexID> & path,
		   bool isConvex_t = false) : std::deque<VertexID>(path), mapping(mapping_t), isConvex(isConvex_t) { }

    /**
       Constructor from 4 pathes ("rectangular" data)
    */
    Polygon(Mapping2D3D & mapping, const std::deque<VertexID> & path1,
	    const std::deque<VertexID> & path2,
	    const std::deque<VertexID> & path3,
	    const std::deque<VertexID> & path4, bool verbose = false);

    /**
       Copy constructor constructor
    */
    inline Polygon(const Polygon & p) : std::deque<VertexID>(p), mapping(p.mapping), isConvex(p.isConvex) {}

    /**
       Compute convex hull (using 2d coordinates of the mapping)
    */
    Polygon computeConvexHull() const;

    /**
       Return true if ratio between the area of convex hull and the
       area of the current polygon is higher than the given ratio.
    */
    bool isNotCrushed(double ratio, bool verbose = false);

    /**
       return true if (p1, p2) is an edge of the polygon
    */
    bool isBorderEdge(VertexID p1, VertexID p2) const;


    /**
       Compute area of the current polygon (using triangles contained in
       the mapping if not convex, and a fast algorithm otherwise)
    */
    double computeArea();

    /**
       Return true if point is inside polygon
    */
    bool isInside(const Coord2D & point) const;

    /**
       Find a triangle inside the current polygon
    */
    TriangleID findFirstTriangleInside() const;

  };
}

#endif
