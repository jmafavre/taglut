/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include "Mesh.h"
#include "MeshPLCut.h"
#include <algorithm>
#include <set>
#include <vector>
#include "CylinderParameterization.h"
#include "GeodesicDistance.h"
#include "StringManipulation.h"
#include "MTiling.h"
#include "IDTranslator.h"
#include "VectorField.h"
#include "FileTools.h"

using namespace Taglut;


void MeshPLCut::sortBySide(Mesh & cMesh, MeshPart & cc, const std::vector<VertexID> & mpInsideCC, VertexID sideA[2], VertexID sideB[2]) {
  // first select a boundary of the cylinder
  std::vector<VertexID> boundary = cc.getBoundaryFromBPoint(mpInsideCC.front());


  // then build the two set of points
  cMesh.setPointFlag(0);
  for(std::vector<VertexID>::const_iterator b = boundary.begin(); b != boundary.end(); ++b)
    cMesh.point(*b).setFlag(1);
  unsigned int nbA, nbB;
  nbA = nbB = 0;
  for(std::vector<VertexID>::const_iterator mp = mpInsideCC.begin(); mp != mpInsideCC.end(); ++mp) {
    if (cMesh.point(*mp).getFlag() == 1) {
      if (nbA == 2)
	throw Exception("buildRectangularCutFromCylindricalTilingSimple(): wrong number of multipoints on A face.");
      sideA[nbA] = *mp;
      ++nbA;
    }
    else {
      if (nbB == 2)
	throw Exception("buildRectangularCutFromCylindricalTilingSimple(): wrong number of multipoints on B face.");
      sideB[nbB] = *mp;
      ++nbB;
    }
  }
  cMesh.setPointFlag(0);

}

void MeshPLCut::clearNewSFValues() {
  std::vector<MeshMap *>::const_iterator sf = scalarfunctions.begin();
  for(std::vector<std::vector<double> >::iterator sfv = newSFValues.begin(); sfv != newSFValues.end(); ++sfv, ++sf) {
    (*sfv).clear();
  }
}


void MeshPLCut::addNewScalarValues() {
  std::vector<MeshMap *>::iterator sf = scalarfunctions.begin();
  for(std::vector<std::vector<double> >::const_iterator sfv = newSFValues.begin(); sfv != newSFValues.end(); ++sfv, ++sf) {
    (**sf).addValues(*sfv);
  }
  clearNewSFValues();
}


bool MeshPLCut::isUpSide(VertexID v, Mesh & mesh, const MeshMap & mMap) {
  const Point3D & p = mesh.point(v);
  double value = mMap[v];
  assert(p.getIsBoundary());
  assert(mMap.getNbValues() == mesh.getNbPoints());

  // init flag
  mesh.setPointFlag(0);

  std::list<VertexID> nexts;
  nexts.push_back(v);
  mesh.point(v).setFlag(1);
  while (!nexts.empty()) {
    const VertexID vv = nexts.front();
    nexts.pop_front();
    for(std::deque<VertexID>::const_iterator nb = mesh.point(vv).getNeighbours().begin();
	nb != mesh.point(vv).getNeighbours().end(); ++nb)
      if (mesh.point(*nb).getFlag() == 0) {
	mesh.point(*nb).setFlag(1);
	if ((!mesh.point(*nb).getIsBoundary()) && (fabs(mMap[*nb] - value) > 1e-7)) {
	  return mMap[*nb] < value;
	}
	nexts.push_back(*nb);
      }
  }

  throw Exception("isUpSide(3): unknown side");
}

std::vector<PointOnEdge> MeshPLCut::getOnePerSide(const std::vector<PointOnEdge> & list,
						  Mesh & mesh, const MeshMap & mMap) {
  if (list.size() == 2)
    return list;
  std::vector<PointOnEdge> result;

  bool found[2] = {false, false};
  for(std::vector<PointOnEdge>::const_iterator p = list.begin(); p != list.end(); ++p) {
    const bool f = isUpSide((*p).getClosestVertex(), mesh, mMap);
    if (!found[f ? 1 : 0]) { // TODO: select the same point ?
      found[f ? 1 : 0] = true;
      result.push_back(*p);
      if (result.size() == 2)
	return result;
    }
  }
  throw Exception("getOnePerSide(3): not enough points");
}

Mesh MeshPLCut::cutMeshPNLUpdateScalarFunction(const Mesh & mesh, const std::vector<PreNLoop> & pnls, MeshMap & mMap) {
  std::vector<std::pair<Coord3D, Coord3D> > basePoints;
  addScalarFunction(mMap);

  Mesh result;

  std::vector<PLPath> levelsets;
  // first cut according to the integral lines, updating the scalar value
  for(std::vector<PreNLoop>::const_iterator pnl = pnls.begin(); pnl != pnls.end(); ++pnl) {
    PLPath ls = (*pnl).getLevelSet(mMap).removeBranchParts();
    // adjust the level sets to stick on the existing vertices
    ls.adjustOnVertices();
    ls.adjustForAdjacency();
    ls.removeDoublePoints();
    assert(ls.size() > 1);
    // compute the two middle points
    std::pair<std::pair<Coord3D, double>,
	      std::pair<Coord3D, double> > midPoints = (*pnl).getBasePointsCoords(mMap, ls);
    basePoints.push_back(std::pair<Coord3D, Coord3D>(midPoints.first.first, midPoints.second.first));
    levelsets.push_back(ls);
  }
  result = cutMeshPaths(mesh, levelsets);
  assert(result.getNbSinglePoints() == 0);

  mMap.setMesh(result);

  // then cut according to the shortest paths from the saddle points to the multipoints
  std::vector<PointOnEdge> newPoints;

  // compute new PointOnEdges and add it to the mesh
  std::vector<std::pair<Coord3D, Coord3D> >::const_iterator bps = basePoints.begin();
  for(std::vector<PreNLoop>::const_iterator pnl = pnls.begin(); pnl != pnls.end(); ++pnl, ++bps) {
    std::vector<PointOnEdge> pts1 = result.getPointOnEdge((*bps).first, true, 1e-5);
    assert(pts1.size() != 0);
    if (pts1.size() != 2) {
      pts1 = getOnePerSide(pts1, result, mMap);
    }
    if (pts1.front().isVertex()) {
      assert(pts1.back().isVertex());
      pts1.front() = PointOnEdge(result.point(pts1.front().getClosestVertex()));
      assert(pts1.front().distance(result.point(pts1.front().getClosestVertex())) == 0.);
      pts1.back() = PointOnEdge(result.point(pts1.back().getClosestVertex()));
      assert(pts1.back().distance(result.point(pts1.back().getClosestVertex())) == 0.);
      assert(result.point(pts1.front().getClosestVertex()).distance(result.point(pts1.back().getClosestVertex())) == 0.);
      assert(PointOnEdge(result.point(pts1.front().getClosestVertex())).distance(PointOnEdge(result.point(pts1.back().getClosestVertex()))) == 0.);
      assert(pts1.front().distance(pts1.back()) == 0.);
    }
    else {
      assert(((result.point(pts1.front().getFirstVertex()) == result.point(pts1.back().getFirstVertex())) && ((result.point(pts1.front().getSecondVertex()) == result.point(pts1.back().getSecondVertex())))) ||
	     ((result.point(pts1.front().getSecondVertex()) == result.point(pts1.back().getFirstVertex())) && ((result.point(pts1.front().getFirstVertex()) == result.point(pts1.back().getSecondVertex())))));
      pts1.front().adjustLocation((*bps).first);
      pts1.back().adjustLocation((*bps).first);
    }
    assert(pts1.size() == 2);
    newPoints.push_back(pts1.front());
    newPoints.push_back(pts1.back());

    std::vector<PointOnEdge> pts2 = result.getPointOnEdge((*bps).second, true, 1e-5);
    if (pts2.size() != 2)
      pts2 = getOnePerSide(pts2, result, mMap);
    if (pts2.front().isVertex()) {
      assert(pts2.back().isVertex());
      pts2.front() = PointOnEdge(result.point(pts2.front().getClosestVertex()));
      assert(pts2.front().distance(result.point(pts2.front().getClosestVertex())) == 0.);
      pts2.back() = PointOnEdge(result.point(pts2.back().getClosestVertex()));
      assert(pts2.back().distance(result.point(pts2.back().getClosestVertex())) == 0.);
      assert(result.point(pts2.front().getClosestVertex()).distance(result.point(pts2.back().getClosestVertex())) == 0.);
      assert(pts2.front().distance(pts2.back()) == 0.);
    }
    else {
      assert(((result.point(pts2.front().getFirstVertex()) == result.point(pts2.back().getFirstVertex())) && ((result.point(pts2.front().getSecondVertex()) == result.point(pts2.back().getSecondVertex())))) ||
	     ((result.point(pts2.front().getSecondVertex()) == result.point(pts2.back().getFirstVertex())) && ((result.point(pts2.front().getFirstVertex()) == result.point(pts2.back().getSecondVertex())))));
      pts2.front().adjustLocation((*bps).second);
      pts2.back().adjustLocation((*bps).second);
    }
    assert(pts2.size() == 2);

    newPoints.push_back(pts2.front());
    newPoints.push_back(pts2.back());

  }

  assert(newPoints.size() == 4 * pnls.size());

  std::vector<PLPath> pathsFromSaddlePoints;
  std::vector<PointOnEdge>::const_iterator nbp = newPoints.begin();
  std::vector<PointOnEdge> missingPoints;

  // then compute paths from the base points to the saddle points on each side
  for(std::vector<PreNLoop>::const_iterator pnl = pnls.begin(); pnl != pnls.end(); ++pnl) {
    std::vector<PointOnEdge>::const_iterator p[4];
    bool pside[4];
    bool seen[4] = {false, false, false, false};
#ifndef NDEBUG
    unsigned int nbminus = 0;
#endif
    for(unsigned char i = 0; i != 4; ++i) {
      p[i] = nbp; ++nbp;
      pside[i] = isUpSide((*(p[i])).getFirstVertex(), result, mMap);
#ifndef NDEBUG
      if (pside[i])
	++nbminus;
#endif
    }
    assert(nbminus == 2);

    FastMarching fm;
    for(std::vector<Pre3Loop>::const_iterator p3l = (*pnl).getPre3Loops().begin();
	p3l != (*pnl).getPre3Loops().end(); ++p3l) {
      MeshMap db(result);
      std::vector<PointOnEdge> bptarget;
      std::vector<VertexID> realbptarget;
      for(unsigned char i = 0; i != 4; ++i) {
	if (pside[i] == ((*p3l).getDirection())) {
	  bptarget.push_back(*(p[i]));
	  seen[i] = true;
	  if ((*(p[i])).isVertex()) {
	    assert(result.point((*(p[i])).getClosestVertex()).getIsBoundary());
	    realbptarget.push_back((*(p[i])).getClosestVertex());
	    realbptarget.push_back(result.getNextBPoint((*(p[i])).getClosestVertex()));
	    realbptarget.push_back(result.getNextBPoint((*(p[i])).getClosestVertex(), realbptarget.back()));
	  }
	  else {
	    realbptarget.push_back((*(p[i])).getFirstVertex());
	    realbptarget.push_back((*(p[i])).getSecondVertex());
	  }
	}
      }
      assert(bptarget.size() == 2);

      fm.computeDistanceFromPoint(db, result, (*p3l).getSaddlePoint(), -1., realbptarget, true);

      assert(db.getValue(bptarget.front().getFirstVertex()) > 0);
      assert(db.getValue(bptarget.front().getSecondVertex()) > 0);
      assert(db.getValue(bptarget.back().getFirstVertex()) > 0);
      assert(db.getValue(bptarget.back().getSecondVertex()) > 0);

      try {
	PLPath p1 = GeodesicDistance::getPathToOrigin(bptarget.front(), result, db);
	assert(p1.front() == bptarget.front());
	assert(p1.size() > 1);
	PLPath p2 = GeodesicDistance::getPathToOrigin(bptarget.back(), result, db);

	assert(p2.front() == bptarget.back());
	assert(p2.size() > 1);
	assert(p1.back().distance(p2.back()) < 1e-5);
	p2.reversePath();
	pathsFromSaddlePoints.push_back((p1 + p2).removeBranchParts());
	assert(!result.isBoundaryEdge(pathsFromSaddlePoints.back()[1]));
	assert(!result.isBoundaryEdge(pathsFromSaddlePoints.back()[pathsFromSaddlePoints.back().size() - 2]));
      }
      catch (...) {
	std::cout << "Warning: a path was not generated." << std::endl;
      }
    }

    for(unsigned int i = 0; i != 4; ++i)
      if ((!(seen[i])) && (!(*(p[i])).isVertex())) {
	missingPoints.push_back(*(p[i]));
      }
  }

  // then cut the mesh according to the paths
  result = cutMeshPaths(result, pathsFromSaddlePoints);
  assert(result.getNbSinglePoints() == 0);

  // then add the missing points
  result = addPoints(result, missingPoints);
  assert(result.getNbSinglePoints() == 0);


  removeScalarFunction();
  return result;
}


Mesh MeshPLCut::addPoints(const Mesh & mesh, const std::vector<PointOnEdge> & points) {
  Mesh result(mesh);

  clearNewSFValues();

  for(std::vector<PointOnEdge>::const_iterator p = points.begin(); p != points.end(); ++p) {
    result.addPointOnEdge(*p);
    addNewSFValue(*p);
  }

  addNewScalarValues();

  return result;
}


PointOnEdge MeshPLCut::findNextPointInDirection(const Mesh & mesh, const PointOnEdge & p1, const PointOnEdge & p2, const Triangle & triangle) {
  const Line3D line(p1, p2 - p1);
  PointOnEdge result(p1);
  const double distance = p1.distance(p2);

  if (p1.isVertex()) {
    const Point3D & pp2 = mesh.point(triangle.getOtherPoint(p1.getClosestVertex()));
    const Point3D & pp3 = mesh.point(triangle.getOtherPoint(p1.getClosestVertex(), pp2.getId()));

    Coord3D p;
    double location = -1.;

    try {
      p = line.getIntersection(Line3D(pp2, pp3 - pp2));
      location = p.getLocationOnEdge(pp2, pp3);
      if ((location >= 0.) && (location <= 1.)) {
	result = PointOnEdge(pp2, pp3, location);
      }

    }
    catch (...) {
    }


    return result;
  }
  else {
    const Point3D & pFirst = mesh.point(p1.getFirstVertex());
    const Point3D & pSecond = mesh.point(p1.getSecondVertex());
    const Point3D & pOther = mesh.point(triangle.getOtherPoint(p1.getFirstVertex(), p1.getSecondVertex()));
    Coord3D p;
    bool intersection = true;
    double location = -1.;

    try {
      p = line.getIntersection(Line3D(pFirst, pOther - pFirst));
    }
    catch (Exception e) {
      intersection = false;
    }

    if (intersection) {
      location = p.getLocationOnEdge(pFirst, pOther);
      if ((location < 0.) || (location > 1.)) {
	intersection = false;
      }
      else {
	result = PointOnEdge(pFirst, pOther, location);
	double d = p2.distance(result);
	if (d < distance)
	  intersection = true;
      }
    }

    if (!intersection) {
      // compute the second one
      intersection = true;
      try {
	p = line.getIntersection(Line3D(pSecond, pOther - pSecond));
      }
      catch (Exception e) {
	intersection = false;
      }

      if (intersection) {
	location = p.getLocationOnEdge(pSecond, pOther);
	if ((location < 0.) || (location > 1.)) {
	  intersection = false;
	}
	else {
	  result = PointOnEdge(pSecond, pOther, location);
	  double d = p2.distance(result);
	  if (d < distance)
	    intersection = true;
	}
      }
    }

    // TODO: add a check for the vertices

    if (!intersection)
      throw Exception("findNextPointInDirection(4): cannot find a new point");
  }

  return result;
}

PointOnEdge MeshPLCut::findNextPointInDirection(const Mesh & mesh, const PointOnEdge & p1, const PointOnEdge & p2) {
  // assuming that the mesh has been cut along another path, it find the next point-on-edge from p1 in the direction of p2
  const double distance = p1.distance(p2);
  if (p1.isVertex()) {
    const Point3D & pt = mesh.point(p1.getClosestVertex());

    for(std::deque<TriangleID>::const_iterator t = pt.getTriangles().begin(); t != pt.getTriangles().end(); ++t) {
      PointOnEdge r = findNextPointInDirection(mesh, p1, p2, mesh.triangle(*t));
      if (r.distance(p2) < distance)
	return r;
    }
    throw Exception("findNextPointInDirection(3) cannot find next point from a vertex");
  }
  else {
    const Triangle & t1 = mesh.findTriangle(p1.getFirstVertex(), p1.getSecondVertex());
    PointOnEdge r1 = findNextPointInDirection(mesh, p1, p2, t1);
    if (r1.distance(p2) > distance) {
      try {
	const Triangle & t2 = mesh.findOtherTriangle(p1.getFirstVertex(), p1.getSecondVertex(), t1);
	PointOnEdge r2 = findNextPointInDirection(mesh, p1, p2, t2);
	if (r2.distance(p2) > distance)
	  throw Exception("findNextPointInDirection(3): cannot find next point");
	else
	  return r2;
      }
      catch(...) {
      }
      throw Exception("findNextPointInDirection(3): cannot find next point (no other triangle)");

    }
    else
      return r1;
  }
}

PLPath & MeshPLCut::updatePLPathAfterCut(const Mesh & mesh, PLPath & path) {
  path = PLPath(path, mesh);

  try {
    // first correct the modified edges
    for(PLPath::iterator p = path.begin(); p != path.end(); ++p) {
      if ((*p).isVertex()) {
	if (mesh.point((*p).getClosestVertex()).getIsBoundary()) {
	  // stratégie:
	  // - trouver tous les sommets à la meme position
	  // - trouver le point suivant dans le chemin
	  // - trouver le sommet qui a un triangle dans la bonne direction
	  // TODO: not implemented
	  std::cout << "Not implemented (two paths are crossing in an existing vertex)" << std::endl;
	}
      }
      else if((*p).getFirstVertex() != (*p).getSecondVertex()) {
	if (!mesh.point((*p).getFirstVertex()).hasNeighbour((*p).getSecondVertex())) {
	  const Point3D & p1 = mesh.point((*p).getFirstVertex());
	  const Point3D & p2 = mesh.point((*p).getSecondVertex());
	  const Coord3D v(p2 - p1);
	  VertexID nbp1 = mesh.getNbPoints();
	  VertexID nbp2 = mesh.getNbPoints();
	  // find the neighbours on the same axis
	  for(std::deque<VertexID>::const_iterator nb = p1.getNeighbours().begin(); nb != p1.getNeighbours().end(); ++nb) {
	    if ((Coord3D::vectorialProduct(v, p1 - mesh.point(*nb)).norm() < 1e-7) && (p1.distance(p2) > p2.distance(mesh.point(*nb)))) {
	      nbp1 = *nb;
	      break;
	    }
	  }
	  for(std::deque<VertexID>::const_iterator nb = p2.getNeighbours().begin(); nb != p2.getNeighbours().end(); ++nb) {
	    if ((Coord3D::vectorialProduct(v, p2 - mesh.point(*nb)).norm() < 1e-7) && (p2.distance(p1) > p1.distance(mesh.point(*nb)))) {
	      nbp2 = *nb;
	      break;
	    }
	  }
	  assert(p1.getIsBoundary() || (nbp1 != mesh.getNbPoints()));
	  assert(p2.getIsBoundary() || (nbp2 != mesh.getNbPoints()));
	  const double d1 = (nbp1 == mesh.getNbPoints()) ? -1. : ((Coord3D &)(*p)).getLocationOnEdge(p1, mesh.point(nbp1));
	  const double d2 = (nbp2 == mesh.getNbPoints()) ? -1. : ((Coord3D &)(*p)).getLocationOnEdge(mesh.point(nbp2), p2);

	  const double e = 1e-5;
	  if (((fabs(d1) < e) && (fabs(d2 - 1.) < e)) ||
	      ((fabs(d2) < e) && (fabs(d1 - 1.) < e))) {
	    if (!p1.getIsBoundary() || !p2.getIsBoundary()) {
	      std::cout << "Not implemented (two paths are crossing in their inner part)" << std::endl;
	    }
	    // the cut is exactly at the same location... we have to choose the good side to add our point
	    PLPath::iterator otherp = p + 1;
	    if (otherp == path.end())
	      otherp = p - 1;
	    const Point3D & middle2 = mesh.point(nbp2);
	    const Triangle & triangle2 = mesh.findTriangle(nbp2, middle2.getId());
	    const Point3D & newP2 = mesh.point(triangle2.getOtherPoint(nbp2, p2.getId()));
	    if (fabs(middle2.angle(*otherp, p2) + middle2.angle(*otherp, newP2) - middle2.angle(p2, newP2)) < 1e-5) {
	      // otherp between p2 and newP2 (using angles)
	      (*p).setFirstVertex(nbp2);
	      (*p).setSecondVertex(nbp2);
	      (*p).setLocation(0.);
	    }
	    else {
	      (*p).setFirstVertex(nbp1);
	      (*p).setSecondVertex(nbp1);
	      (*p).setLocation(0.);
	    }
	  }
	  else {
	    if ((0. <= d1) && (d1 <= 1.)) {
	      assert(!((0. < d2) && (d2 < 1.)));
	      (*p).setSecondVertex(nbp1);
	      (*p).setLocation(d1);
	    }
	    else if ((0. <= d2) && (d2 <= 1.)) {
	      (*p).setFirstVertex(nbp2);
	      (*p).setLocation(d2);
	    }
	    else {
	      throw Exception("Point out of edge");
	    }
	  }
	}
      }
    }

    if (path.size() >= 2) {
      // then add the missing points because of triangle splits
      PLPath::iterator ppred = path.begin();
      for(PLPath::iterator p = path.begin() + 1; p != path.end(); ++p, ++ppred) {
	bool f = false;
	while(!f) {
	  f = true;
	  try {
	    mesh.findTriangle(*p, *ppred);
	  }
	  catch(...) {
	    f = false;
	  }
	  if (!f) {
	    ppred = path.insert(p, findNextPointInDirection(mesh, *ppred, *p));
	    p = ppred + 1;
	  }
	}
      }
    }
  }
  catch (...) {
    path = PLPath(mesh);
    std::cout << "Warning: a path cannot be updated after another cut" << std::endl;
  }
  return path;
}

Mesh MeshPLCut::cutMeshPaths(const Mesh & mesh, const std::vector<PLPath> & paths) {
  Mesh result(mesh);
  std::vector<PLPath> opaths(paths);
  while(!opaths.empty()) {
    result = cutMesh(result, opaths.back());
    opaths.pop_back();
    for(std::vector<PLPath>::iterator p = opaths.begin(); p != opaths.end(); ++p) {
      updatePLPathAfterCut(result, *p);
    }
  }

  return result;
}

Mesh MeshPLCut::cutMesh(const Mesh & mesh, const PLPath & path) {
  Mesh result;

  // clear the data structures
  trianglesNotClean.clear();
  newPoints.clear();
  clearNewSFValues();

  // add the original points
  result.addPoints(mesh.getPoints(), true);

  // compute the state of each triangle (is it a "clean" triangle, or a triangle that have to be split)
  // and add the new points
  const TriangleID nbTriangles = mesh.getNbTriangles();
  bool triangleClean[mesh.getNbTriangles()];
  bool * tc = triangleClean;
  for(TriangleID i = 0; i != nbTriangles; ++i, ++tc)
    (*tc) = true;


  PLPath corrected(path);
  corrected.adjustOnVertices();
  corrected.adjustForAdjacency();
  corrected.removeDoublePoints();
  PLPath::const_iterator pOepred = corrected.begin();
  for(PLPath::const_iterator pOe = corrected.begin(); pOe != corrected.end(); pOepred = pOe, ++pOe) {
    // add the new edges
    if (pOepred != pOe) {
      addNewPoint(result, *pOepred, mesh);
      addNewPoint(result, *pOe, mesh);
      assert(newPoints[*pOepred].getNbCopies() != 0);
      assert(newPoints[*pOe].getNbCopies() != 0);

      if ((*pOe).inSameEdge(*pOepred)) {
	std::pair<VertexID, VertexID> points = (*pOe).getEdge(*pOepred);
	try {
	  const Triangle & t1 = mesh.findTriangle(points.first, points.second);
	  addNonCleanTriangle(t1, *pOepred, *pOe);
	  triangleClean[t1.getId()] = false;
	  const Triangle & t2 = mesh.findOtherTriangle(points.first, points.second, t1);
	  addNonCleanTriangle(t2, *pOepred, *pOe);
	  triangleClean[t2.getId()] = false;
	}
	catch (Exception e) {
	}
      }
      else {
	const Triangle & t = mesh.findTriangle(*pOe, *pOepred);
	addNonCleanTriangle(t, *pOepred, *pOe);
	triangleClean[t.getId()] = false;
      }


      // if the current point is a splitted vertex, all the 1-star has to be managed
      if ((*pOe).isVertex() && (newPoints[*pOe].getNbCopies() == 2)) {
	const Point3D & p = mesh.point((*pOe).getClosestVertex());
	for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t) {
	  triangleClean[*t] = false;
	  trianglesNotClean[*t] = mesh.triangle(*t);
	}
      }

      // if the pred point is a splitted vertex, all the 1-star has to be managed
      if ((*pOepred).isVertex() && (newPoints[*pOepred].getNbCopies() == 2)) {
	const Point3D & p = mesh.point((*pOepred).getClosestVertex());
	for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t) {
	  triangleClean[*t] = false;
	  trianglesNotClean[*t] = mesh.triangle(*t);
	}
      }
    }
  }

  // add the "clean" triangles
  tc = triangleClean;
  for(Mesh::const_triangle_iterator tt = mesh.getTriangles().begin(); tt != mesh.getTriangles().end(); ++tt, ++tc)
    if (*tc)
      result.addTriangleFull(*tt);


  // compute the new ids for old vertices in each triangle
  for(std::map<PointOnEdge, CreatedPoint>::iterator p = newPoints.begin(); p != newPoints.end(); ++p)
    if ((*p).first.isVertex()) {
      const Point3D & pp = mesh.point((*p).first.getClosestVertex());
      std::vector<VertexID>::const_iterator pNewIds = (*p).second.getNewIds().begin();

      if ((*p).second.getNewIds().size() > 1) {
	// only neighbours are ordered in the 1-star. To get the triangles well ordered, we have to walk on the neighbours
	std::deque<VertexID>::const_iterator nbLast = pp.getNeighbours().begin() + (pp.getNbNeighbours() - 1);
	for(std::deque<VertexID>::const_iterator nb = pp.getNeighbours().begin(); nb != pp.getNeighbours().end(); ++nb) {
	  if ((pp.getNbNeighbours() == 2) && pp.getIsBoundary() && (nbLast == pp.getNeighbours().begin())) {
	    // specific configuration: on the boundary, and a single triangle. Do not use it two time
	    break;
	  }
	  try {
	    const Triangle & t = mesh.findTriangle(pp.getId(), *nbLast, *nb);
	    if (trianglesNotClean.find(t.getId()) != trianglesNotClean.end()) {
	      // count the number of new edges on this triangle, except the one located on (pp.getId(), *nb)
	      unsigned int nbPtsInNbEdge = trianglesNotClean[t.getId()].getNbCutInEdgeThroughCorner(pp.getId(), *nb);
	      assert(nbPtsInNbEdge <= 1);
	      unsigned int nbPtsInTriangle = trianglesNotClean[t.getId()].getNbCutThroughCorner(pp.getId());
	      unsigned int nbPtsInNbLastEdge = trianglesNotClean[t.getId()].getNbCutInEdgeThroughCorner(pp.getId(), *nbLast);
	      assert(nbPtsInNbLastEdge <= 1);
	      assert(nbPtsInTriangle >= nbPtsInNbLastEdge + nbPtsInNbEdge);
	      if (nbPtsInTriangle != 0) {
		assert((*p).second.getNewIds().size() >= nbPtsInTriangle);
		pNewIds = pNewIds + nbPtsInNbLastEdge;
		if (pNewIds == (*p).second.getNewIds().end()) {
		  pNewIds = (*p).second.getNewIds().begin();
		}

		std::vector<VertexID>::const_iterator pNewIdsEnd = pNewIds + (nbPtsInTriangle - nbPtsInNbEdge - nbPtsInNbLastEdge);
		if (pNewIdsEnd == (*p).second.getNewIds().end()) {
		  pNewIdsEnd = (*p).second.getNewIds().begin();
		}
		trianglesNotClean[t.getId()].setNewIdsOnCorner(pp.getId(), pNewIds, pNewIdsEnd,
							       (*p).second.getNewIds().end(), (*p).second.getNewIds().begin());
		pNewIds = pNewIdsEnd;
	      }
	      else {
		trianglesNotClean[t.getId()].setNewIdsOnCorner(pp.getId(), pNewIds, pNewIds,
							       (*p).second.getNewIds().end(), (*p).second.getNewIds().begin());
	      }
	    }
	    else {
	      assert(pp.getId() == *pNewIds);
	    }
	  }
	  catch (Exception e) {
	    // a jump: pp is a point on the boundary...
	    pNewIds++;
	    if (pNewIds == (*p).second.getNewIds().end())
	      pNewIds = (*p).second.getNewIds().begin();
	  }
	  nbLast = nb;
	}
      }
    }

  // FIXME(0): solve this limitation
#ifndef NDEBUG
  for(std::map<PointOnEdge, CreatedPoint>::const_iterator p = newPoints.begin(); p != newPoints.end(); ++p)
    if ((!(*p).first.isVertex()) && (*p).second.getNewIds().size() != 2)
      Exception("Multi cut on an edge: not able to manage with this configuration");
#endif

  // for each of the "non-clean" triangles, add the corresponding point to the triangle
  for(std::map<TriangleID, TriangleWithCut>::iterator t = trianglesNotClean.begin(); t != trianglesNotClean.end(); ++t)
    addNewTriangles(result, (*t).second, mesh);

  assert(result.point(0).getNbNeighbours() != 0);
  result.reorderNeighbours();
  result.initIsBoundaryFlag();

  addNewScalarValues();

  return result;
}

void MeshPLCut::addNonCleanTriangle(const Triangle & t, const PointOnEdge & pOe1, const PointOnEdge & pOe2) {
  std::map<TriangleID, TriangleWithCut>::iterator tit = trianglesNotClean.find(t.getId());
  if (tit == trianglesNotClean.end())
    trianglesNotClean[t.getId()] = t;

  assert(trianglesNotClean[t.getId()] == t);
  assert((pOe1.isVertex() && (t.hasPoint(pOe1.getFirstVertex()) || t.hasPoint(pOe1.getSecondVertex()))) ||
	 (!pOe1.isVertex() && t.hasPoint(pOe1.getFirstVertex()) && t.hasPoint(pOe1.getSecondVertex())));
  assert((pOe2.isVertex() && (t.hasPoint(pOe2.getFirstVertex()) || t.hasPoint(pOe2.getSecondVertex()))) ||
	 (!pOe2.isVertex() && t.hasPoint(pOe2.getFirstVertex()) && t.hasPoint(pOe2.getSecondVertex())));
  trianglesNotClean[t.getId()].addCut(pOe1, pOe2);
}

void MeshPLCut::addNewTriangles(Mesh & mesh, TriangleWithCut & triangle, const Mesh & oMesh) {
  if (triangle.nbCuts() == 0) {
    mesh.addTriangleFull(Triangle(triangle.getIdOnCorner(0),
				  triangle.getIdOnCorner(1),
				  triangle.getIdOnCorner(2)));
  }
  else {
    const std::vector<std::pair<PointOnEdge, PointOnEdge> > cuts = triangle.getCuts();
    // create a new multitriangle with a first child (corresponding to an alias in the new data structure)
    MultiTriangle splittedTriangles(triangle, triangle.popFirstIDOnCorner(0), triangle.popFirstIDOnCorner(1), triangle.popFirstIDOnCorner(2));


    for(std::vector<std::pair<PointOnEdge, PointOnEdge> >::const_iterator cut = cuts.begin(); cut != cuts.end(); ++cut) {
      if (!((*cut).first.isVertex() && (*cut).second.isVertex())) {
	const std::pair<EdgeSplitter, EdgeSplitter> cutNewIds = getSplitters(*cut, triangle, oMesh, splittedTriangles);
	splittedTriangles.addCut(*cut, cutNewIds, triangle, mesh);
      }
    }
    splittedTriangles.addInMesh(mesh);
  }
}

void MeshPLCut::addNewPoint(Mesh & mesh, const PointOnEdge & pOe, const Mesh & oMesh) {

  // count the number of copies
  newPoints[pOe].addNewCopy();

  if ((pOe.isVertex()) && (newPoints[pOe].getNbCopies() == 1)) {
    assert(pOe.distance(mesh.point(pOe.getClosestVertex())) == 0.);
    newPoints[pOe].addNewID(pOe.getClosestVertex());
    if (oMesh.point(pOe.getClosestVertex()).getIsBoundary()) {
      newPoints[pOe].addNewCopy();
      newPoints[pOe].addNewID(mesh.addPoint(Point3D(pOe.getX(), pOe.getY(), pOe.getZ())));
      assert(mesh.point(mesh.getNbPoints() - 1).distance(pOe) == 0.);
      addNewSFValue(pOe);
    }
  }
  else {
    assert(!pOe.isVertex() || pOe.distance(mesh.point(pOe.getClosestVertex())) == 0.);
    newPoints[pOe].addNewID(mesh.addPoint(Point3D(pOe.getX(), pOe.getY(), pOe.getZ())));
    assert(mesh.point(mesh.getNbPoints() - 1).distance(pOe) == 0.);
    addNewSFValue(pOe);
    if (oMesh.isBoundaryEdge(pOe) && (newPoints[pOe].getNbCopies() == 1)) {
      newPoints[pOe].addNewCopy();
      newPoints[pOe].addNewID(mesh.addPoint(Point3D(pOe.getX(), pOe.getY(), pOe.getZ())));
      assert(mesh.point(mesh.getNbPoints() - 1).distance(pOe) == 0.);
      addNewSFValue(pOe);
    }
  }
}


bool MeshPLCut::MultiTriangle::containedInChildren(const PointOnEdge & point, const EdgeSplitter & es, bool first) const {
  assert(isSplit());

  if (point.isVertex()) {
    assert(!(*splitter).isVertex());
    if ((*eSplitter).hasVertex(es.getV1()) && (*eSplitter).hasVertex(es.getV2()))
      return true; // in the opposite corner
    else if ((*eSplitter).hasVertex(es.getV1())) {
      // VertexID vPoint = (*eSplitter).getOtherVertex(es.getV1());
      const VertexID v1 = (*eSplitter).getV1();
      const VertexID v2 = (*eSplitter).getV2();
      return v1 > v2 || !first;
    }
    else {
      assert((*eSplitter).hasVertex(es.getV2()));
      // VertexID vPoint = (*eSplitter).getOtherVertex(es.getV2());
      const VertexID v1 = (*eSplitter).getV1();
      const VertexID v2 = (*eSplitter).getV2();
      return v2 > v1 || !first;
    }
  }
  else {
    if ((*eSplitter) == es) // the given point is the splitter
      return true;
    else if (point.inSameEdge(*splitter)) {
      if (point == *splitter) // the current point is the splitter (with other ids)
	return true;
      bool after = point.getLocationOnEdge(*splitter);
      if (after) { // es is in the (*splitter).getSecondVertex() side, i.e. in the (*eSplitter).getV2() side
	return (*eSplitter).getV2() > (*eSplitter).getV1() || !first;
      }
      else { // es is in the (*splitter).getFirstVertex() side, i.e. in the (*eSplitter).getV1() side
	return (*eSplitter).getV1() > (*eSplitter).getV2() || !first;
      }
    }
    else {
      const VertexID v1 = (*eSplitter).getV1();
      const VertexID v2 = (*eSplitter).getV2();
      assert(hasPoint(v1));
      assert(hasPoint(v2));

      // not in the same edge (in the original mesh) but with v1 and v2 as extremities
      // means that its the opposite corner
      if (es.hasVertex(v1) && es.hasVertex(v2))
	return true;
      assert(es.hasVertex(getOtherPoint(v1, v2)));
      if (es.hasVertex(v1)) {
	return (first && (v1 > v2)) || (!first && (v1 < v2));
      }
      else {
	assert(es.hasVertex(v2));
	return (first && (v2 > v1)) || (!first && (v2 < v1));
      }
    }
  }
}


void MeshPLCut::MultiTriangle::addPointOnEdge(const PointOnEdge & point, VertexID v1, VertexID v2, VertexID middle) {
  assert(isFinal());
  assert(hasPoint(v1));
  assert(hasPoint(v2));
  // create the children
  MultiTriangle * mm1 = new MultiTriangle(*this);
  MultiTriangle * mm2 = new MultiTriangle(*this);
  // add it to the structure
  mt1 = mm1;
  mt2 = mm2;
  assert((*mt1).isFinal());
  assert((*mt2).isFinal());
  // update the current triangle
  splitter = new PointOnEdge(point);
  eSplitter = new EdgeSplitter(v1, v2, middle, middle);
  // update the children
  if (v1 > v2) {
    (*mt1).replacePoint(v2, middle);
    (*mt2).replacePoint(v1, middle);
  }
  else {
    (*mt1).replacePoint(v1, middle);
    (*mt2).replacePoint(v2, middle);
  }
}

void MeshPLCut::MultiTriangle::addPointOnEdgeFullSplit(const PointOnEdge & point, const EdgeSplitter & ids, const EdgeSplitter & idCorner) {
  assert(ids.getId1() != ids.getId2());
  assert(idCorner.getId1() != idCorner.getId2());
  assert(!point.isVertex());
  assert(((ids.getV1() == idCorner.getV1()) && ((ids.getV2() == idCorner.getV2()))) ||
	 ((ids.getV2() == idCorner.getV1()) && ((ids.getV1() == idCorner.getV2()))));
  assert(isFinal());
  assert(hasPoint(ids.getV1()));
  assert(hasPoint(ids.getV2()));
  // create the children
  MultiTriangle * mm1 = new MultiTriangle(*this);
  MultiTriangle * mm2 = new MultiTriangle(*this);
  // add it to the structure
  mt1 = mm1;
  mt2 = mm2;
  assert((*mt1).isFinal());
  assert((*mt2).isFinal());
  // update the current triangle
  splitter = new PointOnEdge(point);
  eSplitter = new EdgeSplitter(ids);
  VertexID oCorner = getOtherPoint(idCorner.getV1(), idCorner.getV2());

  // update the children
  if (ids.getV1() > ids.getV2()) {
    (*mt1).replacePoint(ids.getV1(), ids.getId2());
    (*mt2).replacePoint(ids.getV2(), ids.getId1());
  }
  else {
    (*mt1).replacePoint(ids.getV2(), ids.getId1());
    (*mt2).replacePoint(ids.getV1(), ids.getId2());
  }

  if ((*mt1).hasPoint(idCorner.getV1())) {
    assert((*mt2).hasPoint(idCorner.getV2()));
    assert((*mt2).hasPoint(oCorner));
    (*mt1).replacePoint(oCorner, idCorner.getId1());
    (*mt2).replacePoint(oCorner, idCorner.getId2());
  }
  else {
    assert((*mt1).hasPoint(idCorner.getV2()));
    assert((*mt2).hasPoint(idCorner.getV1()));
    assert((*mt2).hasPoint(oCorner));
    (*mt1).replacePoint(oCorner, idCorner.getId2());
    (*mt2).replacePoint(oCorner, idCorner.getId1());
  }

}

void MeshPLCut::MultiTriangle::addCut(const std::pair<PointOnEdge, PointOnEdge> & cut,
				      const std::pair<EdgeSplitter, EdgeSplitter> & ids,
				      const Triangle & t,
				      const Mesh & mesh) {

  if (isAlias()) {
    // only replace points of the extremities
    (*mt1).addCut(cut, std::pair<EdgeSplitter, EdgeSplitter>(ids.first.getReplacePoints(*this, *mt1, false),
							     ids.second.getReplacePoints(*this, *mt1, false)),
		  t, mesh);
  }
  else if (isSplit()) {

    // if it's an edge cut and the middle point has been already added, break
    if (cut.first.inSameEdge(cut.second) && ((cut.first == *splitter) || (cut.second == *splitter))) {
      return;
    }

    // if the current splitter is one of the two points, we only apply a full split with the other one
    if ((cut.first == *splitter) && (isFinalSplit())) {
      addPointOnEdgeFullSplitChildren(cut, ids);
      return;
    }
    if ((cut.second == *splitter) && (isFinalSplit())) {
      addPointOnEdgeFullSplitChildren(std::pair<PointOnEdge, PointOnEdge>(cut.second, cut.first),
				      std::pair<EdgeSplitter, EdgeSplitter>(ids.second, ids.first));
      return;
    }

    bool added = false;
    if (containedInFirst(cut.first, ids.first) &&
	containedInFirst(cut.second, ids.second)) {
      (*mt1).addCut(cut, std::pair<EdgeSplitter, EdgeSplitter>(ids.first.getReplacePoints(*this, *mt1), ids.second.getReplacePoints(*this, *mt1)),
		    t, mesh);
      added = true;
    }
    if (containedInSecond(cut.first, ids.first) &&
	containedInSecond(cut.second, ids.second)) {
      (*mt2).addCut(cut, std::pair<EdgeSplitter, EdgeSplitter>(ids.first.getReplacePoints(*this, *mt2), ids.second.getReplacePoints(*this, *mt2)),
		    t, mesh);
      added = true;
    }
    if (!added)
      throw Exception("Crossing of cuts in the inner part of a triangle (" + StringManipulation::toString(t.getP1()) +
		      ", " + StringManipulation::toString(t.getP2()) +
		      ", " + StringManipulation::toString(t.getP3()) + "). Situation not handled.");
  }
  else {
    assert(isFinal());
    if (cut.first.isVertex()) {
      if (!cut.second.isVertex()) {
	if (cut.first.inSameEdge(cut.second)) {
	  VertexID newID;
	  if (cut.first.getClosestVertex() == cut.second.getFirstVertex())
	    newID = ids.second.getId2();
	  else {
	    assert(cut.first.getClosestVertex() == cut.second.getSecondVertex());
	    newID = ids.second.getId1();
	  }
	  addPointOnEdge(cut.second, ids.second.getV1(), ids.second.getV2(), newID);
	}
	else {
	  assert(!cut.second.isVertex());
	  addPointOnEdgeFullSplit(cut.second, ids.second, ids.first);
	}
      }
    }
    else {
      if (cut.second.isVertex()) {
	if (cut.first.inSameEdge(cut.second)) {
	  VertexID newID;
	  if (cut.second.getClosestVertex() == cut.first.getFirstVertex())
	    newID = ids.first.getId2();
	  else {
	    assert(cut.second.getClosestVertex() == cut.first.getSecondVertex());
	    newID = ids.first.getId1();
	  }
	  addPointOnEdge(cut.first, ids.first.getV1(), ids.first.getV2(), newID);
	}
	else {
	  assert(!cut.first.isVertex());
	  addPointOnEdgeFullSplit(cut.first, ids.first, ids.second);
	}
      }
      else {
	addPointOnEdge(cut.first, ids.first.getV1(), ids.first.getV2(), ids.first.getId1());
	addPointOnEdgeFullSplitChildren(cut, ids);
      }
    }
  }
}

void MeshPLCut::MultiTriangle::addPointOnEdgeFullSplitChildren(const std::pair<PointOnEdge, PointOnEdge> & cut,
							       const std::pair<EdgeSplitter, EdgeSplitter> & ids) {
  assert((splitter != NULL) && ((*splitter) == cut.first));
  if (cut.second.isVertex()) {

    VertexID oCorner = getOtherPoint(ids.second.getV1(), ids.second.getV2());
    if ((*mt1).hasPoint(ids.second.getV1())) {
      (*mt1).replacePoint(oCorner, ids.second.getId1());
    }
    else {
      assert((*mt2).hasPoint(ids.second.getV1()));
      (*mt2).replacePoint(oCorner, ids.second.getId1());
    }

    if ((*mt1).hasPoint(ids.second.getV2())) {
      (*mt1).replacePoint(oCorner, ids.second.getId2());
    }
    else {
      assert((*mt2).hasPoint(ids.second.getV2()));
      (*mt2).replacePoint(oCorner, ids.second.getId2());
    }


    if ((*mt1).hasPoint(ids.first.getV1()))
      (*mt1).replacePoint(ids.first.getId2(), ids.first.getId1());
    if ((*mt1).hasPoint(ids.first.getV2()))
      (*mt1).replacePoint(ids.first.getId1(), ids.first.getId2());

    if ((*mt2).hasPoint(ids.first.getV1()))
      (*mt2).replacePoint(ids.first.getId2(), ids.first.getId1());
    if ((*mt2).hasPoint(ids.first.getV2()))
      (*mt2).replacePoint(ids.first.getId1(), ids.first.getId2());
  }
  else if (containedInFirst(cut.second, ids.second)) {
    EdgeSplitter newIDEdge(ids.second);
    newIDEdge.replacePoints(*this, *mt1);
    EdgeSplitter newIDCorner(ids.first);
    newIDCorner.adjustSamePoints(newIDEdge);
    assert(!cut.second.isVertex());
    (*mt1).addPointOnEdgeFullSplit(cut.second, newIDEdge, newIDCorner);
    // adjust (*mt2) if required
    for(unsigned int i = 0; i < 3; ++i)
      if (newIDCorner.hasVertex((*mt2).getVertexId(i))) {
	if (newIDCorner.getIdFromSide((*mt2).getVertexId(i)) == ids.first.getId2()) {
	  (*mt2).replacePoint(ids.first.getId1(), newIDCorner.getIdFromSide((*mt2).getVertexId(i)));
	}
	else {
	  assert(newIDCorner.getIdFromSide((*mt2).getVertexId(i)) == ids.first.getId1());
	  (*mt2).replacePoint(ids.first.getId2(), newIDCorner.getIdFromSide((*mt2).getVertexId(i)));
	}
	break;
      }
  }
  else {
    assert(containedInSecond(cut.second, ids.second));
    EdgeSplitter newIDEdge(ids.second);
    newIDEdge.replacePoints(*this, *mt2);
    EdgeSplitter newIDCorner(ids.first);
    newIDCorner.adjustSamePoints(newIDEdge);
    assert(!cut.second.isVertex());
    (*mt2).addPointOnEdgeFullSplit(cut.second, newIDEdge, newIDCorner);
    // adjust (*mt1) if required
    for(unsigned int i = 0; i < 3; ++i)
      if (newIDCorner.hasVertex((*mt1).getVertexId(i))) {
	if (ids.first.getId2() == newIDCorner.getIdFromSide((*mt1).getVertexId(i))) {
	  (*mt1).replacePoint(ids.first.getId1(), newIDCorner.getIdFromSide((*mt1).getVertexId(i)));
	}
	else {
	  assert(ids.first.getId1() == newIDCorner.getIdFromSide((*mt1).getVertexId(i)));
	  (*mt1).replacePoint(ids.first.getId2(), newIDCorner.getIdFromSide((*mt1).getVertexId(i)));
	}
	break;
      }
  }
}

void MeshPLCut::MultiTriangle::addInMesh(Mesh & mesh) {
  if (isSplit()) {
    (*mt1).addInMesh(mesh);
    (*mt2).addInMesh(mesh);
  }
  else if (!isFinal())
    (*mt1).addInMesh(mesh);
  else {
    mesh.addTriangleFull(*this);
  }
}

std::pair<MeshPLCut::EdgeSplitter, MeshPLCut::EdgeSplitter> MeshPLCut::getSplitters(const std::pair<PointOnEdge, PointOnEdge> & cut,
										    const Triangle & t, const Mesh & mesh,
										    const MeshPLCut::MultiTriangle & mTriangle) {
  return std::pair<EdgeSplitter, EdgeSplitter>(getSplitter(cut.first, t, mesh, mTriangle), getSplitter(cut.second, t, mesh, mTriangle));
}

MeshPLCut::EdgeSplitter MeshPLCut::getSplitter(const PointOnEdge & cut, const Triangle & t, const Mesh & mesh,
					       const MeshPLCut::MultiTriangle & mTriangle) {
  assert(mesh.getNbPoints() > cut.getFirstVertex());
  assert(mesh.getNbPoints() > cut.getSecondVertex());
  assert(t.hasPoint(cut.getFirstVertex()) || t.hasPoint(cut.getSecondVertex()));
  if (cut.isVertex()) {
    VertexID v0 = cut.getClosestVertex();
    VertexID v1 = t.getOtherPoint(v0);
    VertexID v2 = t.getOtherPoint(v0, v1);
    // we have to reorder v1, v2 according to the ordering arround v0
    if (!mesh.point(v0).neighboursValidOrder(v1, v2)) {
      assert(mesh.point(v0).neighboursValidOrder(v2, v1));
      VertexID vtemp = v1;
      v1 = v2;
      v2 = vtemp;
    }

    TriangleWithCut & tCut = trianglesNotClean[t.getId()];
    VertexID v0bis = tCut.popIDOnCorner(t.getVertexPositionById(v0));
    assert(mTriangle.isAlias());
    VertexID v0in = mTriangle.getAlias(v0);
    //assert(v0bis != v0in);
    return EdgeSplitter(v1, v2, v0in, v0bis);
  }
  else {
    VertexID v1 = cut.getFirstVertex();
    VertexID v2 = cut.getSecondVertex();
    assert(v1 != v2);

    const CreatedPoint & cp = newPoints[cut];
    assert(cp.getNewIds().size() == 2); // FIXME(0): a limitation
    VertexID vv1 = cp.getNewIds().front();
    VertexID vv2 = cp.getNewIds().back();
    assert(vv1 != vv2);
    return EdgeSplitter(v1, v2, vv1, vv2).reorder();
  }
}


MeshPLCut::EdgeSplitter & MeshPLCut::EdgeSplitter::replacePoints(const VTriangle & t1, const VTriangle & t2, bool full) {
  v1 = t1.hasPoint(v1) ? t2.getVertexId(t1.getVertexPositionById(v1)) : v1;
  v2 = t1.hasPoint(v2) ? t2.getVertexId(t1.getVertexPositionById(v2)) : v2;
  if (full) {
    id1 = t1.hasPoint(id1) ? t2.getVertexId(t1.getVertexPositionById(id1)) : id1;
    id2 = t1.hasPoint(id2) ? t2.getVertexId(t1.getVertexPositionById(id2)) : id2;
  }

  return *this;
}

MeshPLCut::EdgeSplitter & MeshPLCut::EdgeSplitter::replacePoints(VertexID iid1, VertexID iid2, bool full) {
  v1 = (v1 == iid1) ? iid2 : v1;
  v2 = (v2 == iid1) ? iid2 : v2;

  if (full) {
    id1 = (id1 == iid1) ? iid2 : id1;
    id2 = (id2 == iid1) ? iid2 : id2;
  }

  return *this;
}

MeshPLCut::EdgeSplitter & MeshPLCut::EdgeSplitter::adjustSamePoints(const EdgeSplitter & es) {
  if (v1 == es.getV1()) {
    v2 = es.getV2();
  }
  else if (v1 == es.getV2()) {
    v2 = es.getV1();
  }
  else if (v2 == es.getV1()) {
    v1 = es.getV2();
  }
  else {
    assert(v2 == es.getV2());
    v1 = es.getV1();
  }

  return *this;
}


VertexID MeshPLCut::updateForCylindricalTiling(MeshMap & mmap, Mesh & mesh, bool side) {
  VertexID result = 0;

  std::vector<VertexID> multiPoints = mesh.computeMultiPoints();


  MeshPart mPart(mesh, true);
  std::deque<MeshPart> ccs = mPart.getCC();
  unsigned int idcc = 0;
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc, ++idcc) {
    if (((*cc).getGenus() == 0) && ((*cc).getNbBoundaries() == 2)) {
      std::vector<VertexID> mpInsideCC;
      for(std::vector<VertexID>::const_iterator m = multiPoints.begin(); m != multiPoints.end(); ++m)
	if ((*cc).isInsidePoint(*m))
	  mpInsideCC.push_back(*m);

      if (mpInsideCC.size() == 4) {

	VertexID sideA[2];
	VertexID sideB[2];

	sortBySide(mesh, *cc, mpInsideCC, sideA, sideB);

	Mesh ccMesh = (*cc).buildCropMesh();
	IDTranslator idTranslator(*cc);

	VertexID sideALocal[2] = { idTranslator.g2l(sideA[0]), idTranslator.g2l(sideA[1]) };
	assert(sideALocal[0] != sideALocal[1]);
	VertexID sideBLocal[2] = { idTranslator.g2l(sideB[0]), idTranslator.g2l(sideB[1]) };
	assert(sideBLocal[0] != sideBLocal[1]);

	if (!side) {
	  VertexID t = sideALocal[0];
	  sideALocal[0] = sideALocal[1];
	  sideALocal[1] = t;
	}

	CylinderParameterization cParam;

	MeshMap ccmap = cParam.parameterizeAxisDirectionFixedPoints(ccMesh, sideALocal, sideBLocal);
	for(VertexID v = 0; v != ccMesh.getNbPoints(); ++v) {
	  assert(v < ccmap.getNbValues());
	  assert(idTranslator.l2g(v) < mesh.getNbPoints());
	  assert(idTranslator.l2g(v) < mmap.getNbValues());
	  mmap[idTranslator.l2g(v)] = ccmap[v];
	  ++ result;
	}

      }
    }
  }

  return result;

}



Mesh MeshPLCut::cutMeshInQuadranglesFromCylindricalTiling(Mesh & mesh,
							  double maxratio,
							  double epsilonSnap,
							  const enum CylinderParamChoice & cpc,
							  const enum CylinderParamMethod & cpm) {
  std::vector<PLPath> paths;
  std::vector<VertexID> multiPoints = mesh.computeMultiPoints();

  std::vector<std::pair<VertexID, VertexID> > cylDesc;

  MeshMap * cmap = NULL;
  if (maxratio > 0.) {
    cmap = new MeshMap(mesh, -1.);
  }


  MeshPart mPart(mesh, true);
  std::deque<MeshPart> ccs = mPart.getCC();
  unsigned int idcc = 0;
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc, ++idcc) {
    if (((*cc).getGenus() == 0) && ((*cc).getNbBoundaries() == 2)) {
      std::vector<VertexID> mpInsideCC;
      for(std::vector<VertexID>::const_iterator m = multiPoints.begin(); m != multiPoints.end(); ++m)
	if ((*cc).isInsidePoint(*m))
	  mpInsideCC.push_back(*m);


      if (mpInsideCC.size() == 4) {
	VertexID sideA[2];
	VertexID sideB[2];

	sortBySide(mesh, *cc, mpInsideCC, sideA, sideB);

	Mesh ccMesh = (*cc).buildCropMesh();
	IDTranslator idTranslator(*cc);

	VertexID sideALocal[2] = { idTranslator.g2l(sideA[0]), idTranslator.g2l(sideA[1]) };
	assert(sideALocal[0] != sideALocal[1]);
	VertexID sideBLocal[2] = { idTranslator.g2l(sideB[0]), idTranslator.g2l(sideB[1]) };
	assert(sideBLocal[0] != sideBLocal[1]);

	// first parameterization
	CylinderParameterization cParam(false);
	Mesh ccMeshParam1;
	switch (cpm) {
	case CPMFloater2D: {
	  Mapping2D3D mapping = cParam.parameterizeFixedPoints(ccMesh, sideALocal, sideBLocal, true);
	  ccMeshParam1 = Mesh(mapping);
	  break;
	}
	case CPMFloater3D: {
	  ccMeshParam1 = cParam.parameterize3DFixedPoints(ccMesh, sideALocal, sideBLocal, true);
	  break;
	}
	case CPMFloaterIntegralLines: {
	  ccMeshParam1 = cParam.parameterize3DFixedPointsByIL(ccMesh, sideALocal, sideBLocal);
	  break;
	}
	case CPMFloater4IntegralLines: {
	  ccMeshParam1 = cParam.parameterize3DFixedPointsBy4IL(ccMesh, sideALocal, sideBLocal);
	  break;
	}
	}

	FastMarching fMarching;

	// second parameterization
	VertexID sideALocal2[2] = { sideALocal[1], sideALocal[0] };

	Mesh ccMeshParam2;
	switch (cpm) {
	case CPMFloater2D: {
	  Mapping2D3D mapping = cParam.parameterizeFixedPoints(ccMesh, sideALocal2, sideBLocal, true);
	  ccMeshParam2 = Mesh(mapping);
	  break;
	}
	case CPMFloater3D: {
	  ccMeshParam2 = cParam.parameterize3DFixedPoints(ccMesh, sideALocal2, sideBLocal, true);
	  break;
	}
	case CPMFloaterIntegralLines: {
	  ccMeshParam2 = cParam.parameterize3DFixedPointsByIL(ccMesh, sideALocal2, sideBLocal);
	  break;
	}
	case CPMFloater4IntegralLines: {
	  ccMeshParam2 = cParam.parameterize3DFixedPointsBy4IL(ccMesh, sideALocal2, sideBLocal);
	  break;
	}
	}


	if (cpc == CPCByLength) {
	  PLPath p11, p12;
	  MeshMap mmap1(ccMeshParam1, DTop, M_PI / 2);
	  try {
	    p11 = idTranslator.l2gPLPath(mmap1.computeLevelSetCC(PointOnEdge(ccMeshParam1.point(sideALocal[0]))), mesh);
	    assert(p11.front().distance(mesh.point(idTranslator.l2g(sideBLocal[0]))) < 1e-3);
	    p11.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[0])));
	    p12 = idTranslator.l2gPLPath(mmap1.computeLevelSetCC(PointOnEdge(ccMeshParam1.point(sideALocal[1]))), mesh);
	    assert(p12.front().distance(mesh.point(idTranslator.l2g(sideBLocal[1]))) < 1e-3);
	    p12.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[1])));
	  } catch (...) { }


	  PLPath p21, p22;
	  MeshMap mmap2(ccMeshParam2, DTop, M_PI / 2);
	  try {
	    p21 = idTranslator.l2gPLPath(mmap2.computeLevelSetCC(PointOnEdge(ccMeshParam2.point(sideALocal2[0]))), mesh);
	    assert(p21.front().distance(mesh.point(idTranslator.l2g(sideBLocal[0]))) < 1e-3);
	    p21.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[0])));
	    p22 = idTranslator.l2gPLPath(mmap2.computeLevelSetCC(PointOnEdge(ccMeshParam2.point(sideALocal2[1]))), mesh);
	    assert(p22.front().distance(mesh.point(idTranslator.l2g(sideBLocal[1]))) < 1e-3);
	    p22.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[1])));
	  } catch (...) { }


	  if (p11.size() * p12.size() == 0) {
	    assert(p21.size() * p22.size() != 0);
	    std::cout << "Warning: only one reconstruction was valid (1)" << std::endl;
	    paths.push_back(p21);
	    paths.push_back(p22);
	    if (cmap != NULL) {
	      (*cmap).setValues(MeshMap(ccMeshParam2, DTop), idTranslator);
	      cylDesc.push_back(getPointsOnQuadranglesFromCylinder(ccMeshParam2, idTranslator));
	    }
	  }
	  else if (p21.size() * p22.size() == 0) {
	    assert(p11.size() * p12.size() != 0);
	    std::cout << "Warning: only one reconstruction was valid (2)" << std::endl;
	    paths.push_back(p11);
	    paths.push_back(p12);
	    if (cmap != NULL) {
	      (*cmap).setValues(MeshMap(ccMeshParam1, DTop), idTranslator);
	      cylDesc.push_back(getPointsOnQuadranglesFromCylinder(ccMeshParam1, idTranslator));
	    }
	  }
	  else if (p11.length() + p12.length() < p21.length() + p22.length()) {
	    paths.push_back(p11);
	    paths.push_back(p12);
	    if (cmap != NULL) {
	      (*cmap).setValues(MeshMap(ccMeshParam1, DTop), idTranslator);
	      cylDesc.push_back(getPointsOnQuadranglesFromCylinder(ccMeshParam1, idTranslator));
	    }
	  }
	  else {
	    paths.push_back(p21);
	    paths.push_back(p22);
	    if (cmap != NULL) {
	      (*cmap).setValues(MeshMap(ccMeshParam2, DTop), idTranslator);
	      cylDesc.push_back(getPointsOnQuadranglesFromCylinder(ccMeshParam2, idTranslator));
	    }
	  }
	}
	else {
	  double cSim1;
	  double cSim2;

	  try {
	    if (cpc == CPCByNormals) {
	      cSim1 = CylinderParameterization::computeNormalsMSE(ccMeshParam1);
	      cSim2 = CylinderParameterization::computeNormalsMSE(ccMeshParam2);
	    }
	    else if (cpc == CPCByDistance) {
	      cSim1 = CylinderParameterization::computeWorstDistance(ccMeshParam1);
	      cSim2 = CylinderParameterization::computeWorstDistance(ccMeshParam2);
	    }
	    else if (cpc == CPCByDistanceAndConformity) {
	      cSim1 = CylinderParameterization::computeWorstDistance(ccMeshParam1, 0.3);
	      cSim2 = CylinderParameterization::computeWorstDistance(ccMeshParam2, 0.3);
	      if ((cSim1 < 0.) && (cSim2 < 0.)) {
		cSim1 = ccMeshParam1.conformalSimilarity(ccMesh);
		cSim2 = ccMeshParam2.conformalSimilarity(ccMesh);
	      }
	    }
	    else {
	      cSim1 = ccMeshParam1.conformalSimilarity(ccMesh);
	      cSim2 = ccMeshParam2.conformalSimilarity(ccMesh);
	    }

	    if (cSim1 < cSim2) {
	      PLPath p11, p12;
	      MeshMap mmap1(ccMeshParam1, DTop, M_PI / 2);
	      assert(fabs(mmap1[sideALocal[0]] - mmap1[sideBLocal[0]]) < 1e-3);
	      assert(fabs(mmap1[sideALocal[1]] - mmap1[sideBLocal[1]]) < 1e-3);
	      p11 = idTranslator.l2gPLPath(mmap1.computeLevelSetCC(PointOnEdge(ccMeshParam1.point(sideALocal[0]))), mesh);
	      if (p11.front().getLocation() != 0.) {
		assert(p11.front().distance(mesh.point(idTranslator.l2g(sideBLocal[0]))) < 1e-5);
		p11.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[0])));
	      }
	      try {
		p12 = idTranslator.l2gPLPath(mmap1.computeLevelSetCC(PointOnEdge(ccMeshParam1.point(sideALocal[1]))), mesh);
		if (p12.front().getLocation() != 0.) {
		  assert(p12.front().distance(mesh.point(idTranslator.l2g(sideBLocal[1]))) < 1e-5);
		  p12.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[1])));
		}
	      }
	      catch (...) {
		p12 = idTranslator.l2gPLPath(mmap1.computeLevelSetCC(PointOnEdge(ccMeshParam1.point(sideBLocal[1]))), mesh);
		if (p12.front().getLocation() != 0.) {
		  assert(p12.front().distance(mesh.point(idTranslator.l2g(sideALocal[1]))) < 1e-5);
		  p12.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideALocal[1])));
		}
	      }

	      paths.push_back(p11);
	      paths.push_back(p12);
	      if (cmap != NULL) {
		(*cmap).setValues(MeshMap(ccMeshParam1, DTop), idTranslator);
		cylDesc.push_back(getPointsOnQuadranglesFromCylinder(ccMeshParam1, idTranslator));
	      }
	    }
	    else {
	      PLPath p21, p22;
	      MeshMap mmap2(ccMeshParam2, DTop, M_PI / 2);
	      assert(fabs(mmap2[sideALocal2[0]] - mmap2[sideBLocal[0]]) < 1e-3);
	      assert(fabs(mmap2[sideALocal2[1]] - mmap2[sideBLocal[1]]) < 1e-3);
	      p21 = idTranslator.l2gPLPath(mmap2.computeLevelSetCC(PointOnEdge(ccMeshParam2.point(sideALocal2[0]))), mesh);
	      if (p21.front().getLocation() != 0.) {
		assert(p21.front().distance(mesh.point(idTranslator.l2g(sideBLocal[0]))) < 1e-5);
		p21.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[0])));
	      }
	      p22 = idTranslator.l2gPLPath(mmap2.computeLevelSetCC(PointOnEdge(ccMeshParam2.point(sideALocal2[1]))), mesh);
	      if (p22.front().getLocation() != 0.) {
		assert(p22.front().distance(mesh.point(idTranslator.l2g(sideBLocal[1]))) < 1e-5);
		p22.front() = PointOnEdge(mesh.point(idTranslator.l2g(sideBLocal[1])));
	      }

	      paths.push_back(p21);
	      paths.push_back(p22);
	      if (cmap != NULL) {
		(*cmap).setValues(MeshMap(ccMeshParam2, DTop), idTranslator);
		cylDesc.push_back(getPointsOnQuadranglesFromCylinder(ccMeshParam2, idTranslator));
	      }
	    }
	  }
	  catch (...) { }
	}

      }
      else if (mpInsideCC.size() == 2) {
	std::cout << "Not yet implemented" << std::endl;
      }
      else if (mpInsideCC.size() == 0)
	std::cout << "Not yet implemented" << std::endl;
      else {
	std::cout << "Number of multipoints: " << mpInsideCC.size() << std::endl;
	throw Exception("buildRectangularCutFromCylindricalTilingFloater(): wrong number of multipoints.");
      }

    }
  }

  if (epsilonSnap >= 0.)
    paths = PLPath::fitToVertices(paths, epsilonSnap);

  if (cmap != NULL) {
    addScalarFunction(*cmap);
    Mesh tmpmesh = cutMeshPaths(mesh, paths);
    removeScalarFunction();
    (*cmap).setMesh(tmpmesh);

    // subdivise using integral lines
    Mesh result = cutCylinders(tmpmesh, *cmap, maxratio, cylDesc);

    delete cmap;
    return result;
  }
  else
    return cutMeshPaths(mesh, paths);
}


std::pair<VertexID, VertexID> MeshPLCut::getPointsOnQuadranglesFromCylinder(Mesh & cylinder,
									    const IDTranslator & translator) {
  VertexID notFound = cylinder.getNbPoints();
  VertexID result[2] = {notFound, notFound};
  double aDist[2] = {0., 0.};

  MeshManipulator cmanip(cylinder);

  std::vector<std::vector<VertexID> > bds = cmanip.getBoundaries();
  assert(bds.size() == 2);

  for(std::vector<std::vector<VertexID> >::const_iterator bd = bds.begin(); bd != bds.end(); ++bd)
    for(std::vector<VertexID>::const_iterator bdp = (*bd).begin(); bdp != (*bd).end(); ++bdp) {
      const Point3D & p = cylinder.point(*bdp);
      const double r = sqrt(p.getX() * p.getX() + p.getY() * p.getY());
      double angle;
      if (p.getY() == 0.)
	if (p.getX() > 0.)
	  angle = 0.;
	else
	  angle = M_PI;
      else
	angle = (p.getY() > 0 ? 1. : -1.) * acos(p.getX() / r);
      const unsigned char side = angle > 0;
      const unsigned char subside = fabs(angle) < M_PI / 2;
      const double da = fabs((subside ? 0. : (side ? M_PI : -M_PI)) - angle);
      if (da > aDist[side]) {
	aDist[side] = da;
	result[side] = *bdp;
      }
    }


  if ((result[0] == notFound) ||
      (result[1] == notFound))
    throw Exception("getPointsOnQuadranglesFromCylinder(2): point not found");
  assert(result[0] != result[1]);

  return std::pair<VertexID, VertexID>(translator.l2g(result[0]),
				       translator.l2g(result[1]));
}

Mesh MeshPLCut::cutExtremaDiscs(Mesh & mesh, const std::vector<VertexID> & extrema) {
  std::vector<PLPath> paths;
  std::vector<VertexID> multiPoints = mesh.computeMultiPoints();

  MeshPart mPart(mesh, true);
  std::deque<MeshPart> ccs = mPart.getCC();
  for(std::deque<MeshPart>::iterator cc = ccs.begin(); cc != ccs.end(); ++cc) {
    if (((*cc).getGenus() == 0) && ((*cc).getNbBoundaries() == 1)) {
      std::vector<VertexID> mpInsideCC;
      for(std::vector<VertexID>::const_iterator m = multiPoints.begin(); m != multiPoints.end(); ++m)
	if ((*cc).isInsidePoint(*m))
	  mpInsideCC.push_back(*m);

      if (mpInsideCC.size() == 2) {
	std::vector<VertexID> insideExtrema = (*cc).getInsidePoints(extrema);
	if (insideExtrema.size() != 1) {
	  std::cout << "cutExtremaDiscs: wrong number of extrema points" << std::endl;
	  continue;
	}

	Mesh ccMesh = (*cc).buildCropMesh();
	IDTranslator idTranslator(*cc);
	FastMarching fMarching;

	PLPath p1 = idTranslator.l2gPLPath(fMarching.getShortestPath(ccMesh,
								     idTranslator.g2l(mpInsideCC[0]),
								     idTranslator.g2l(insideExtrema.front())), mesh);
	PLPath p2 = idTranslator.l2gPLPath(fMarching.getShortestPath(ccMesh,
								     idTranslator.g2l(insideExtrema.front()),
								     idTranslator.g2l(mpInsideCC[1])), mesh);

	paths.push_back(p2 + p1);
      }
      else {
	std::cout << "cutExtremaDiscs: Not yet implemented" << std::endl;
      }
    }
  }

  return cutMeshPaths(mesh, paths);
}


unsigned int MeshPLCut::getNbSectionsUpdateBorders(Mesh & mesh,
						   double maxratio,
						   const MTiling & mtiling,
						   const std::pair<unsigned int, unsigned int> & cylinder,
						   std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > & borders) {

  borders = mtiling.getCommonBorders(cylinder.first,
				     cylinder.second);

  // first, order the borders in the direct way
  for(std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > >::iterator bdpath = borders.begin(); bdpath != borders.end();
      ++bdpath) {
    if ((*bdpath).first.size() > 1) {
      const Triangle & t1 = mesh.findTriangle((*bdpath).first.front(), (*bdpath).first[1]);
      if (!t1.sameOrder((*bdpath).first.front(), (*bdpath).first[1]))
	std::reverse((*bdpath).first.begin(), (*bdpath).first.end());
    }
    if ((*bdpath).second.size() > 1) {
      const Triangle & t1 = mesh.findTriangle((*bdpath).second.front(), (*bdpath).second[1]);
      if (!t1.sameOrder((*bdpath).second.front(), (*bdpath).second[1]))
	std::reverse((*bdpath).second.begin(), (*bdpath).second.end());
    }
  }

  // then compute the length of the cylinder
  assert(borders.size() == 2);
  const double l1 = mesh.getLength(borders.front().first);
  assert(fabs(l1 - mesh.getLength(borders.front().second)) < 1e-5);
  const double l2 = mesh.getLength(borders[1].first);
  assert(fabs(l2 - mesh.getLength(borders[1].second)) < 1e-5);
  const double length = (l1 + l2) / 2;

  // then compute the perimeter of the cylinder
  double oBdLength = 0.;
  oBdLength += mesh.getLength(mtiling.getBorder(borders.front().first.front(),
						mesh.getNextBPoint(borders.front().first.front(),
								   borders.front().first[1])));
  oBdLength += mesh.getLength(mtiling.getBorder(borders.front().first.back(),
						mesh.getNextBPoint(borders.front().first.back(),
								   borders.front().first[borders.front().first.size() - 2])));
  oBdLength += mesh.getLength(mtiling.getBorder(borders.front().second.front(),
						mesh.getNextBPoint(borders.front().second.front(),
								   borders.front().second[1])));
  oBdLength += mesh.getLength(mtiling.getBorder(borders.front().second.back(),
						mesh.getNextBPoint(borders.front().second.back(),
								   borders.front().second[borders.back().second.size() - 2])));
  const double perimeter = oBdLength / 2;

  // compute the location of the new multipoints
  return floor(length / perimeter / maxratio) + 1;

}


std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > MeshPLCut::getPathExtremaCutCylinders(Mesh & mesh, double maxratio,
														 const MTiling & mtiling,
														 const std::vector<std::pair<unsigned int, unsigned int> > & cylinders,
														 const MeshMap & mmap) {
  std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > newLines;

  for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator cylinder = cylinders.begin(); cylinder != cylinders.end(); ++cylinder) {
    std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > borders;
    const unsigned int nbsections = getNbSectionsUpdateBorders(mesh, maxratio, mtiling, *cylinder, borders);
    assert(borders.size() == 2);
    if (nbsections > 1) {

      double l = mmap.getValue(borders.front().first.front());
      if (l < mmap.getValue(borders.front().first.back())) {
	l = mmap.getValue(borders.front().first.back());
      }
      assert(l != 0.);

      const double subl = l / nbsections;
      double location = 0.;


      for(unsigned int idcut = 0; idcut != (unsigned int)(nbsections - 1); ++idcut) {
	location += subl;
	assert(location > 0);

	newLines[(*cylinder).first].push_back(std::pair<PointOnEdge, PointOnEdge>(mmap.getPointOnEdgeFromValueAndPath(borders.front().first, location),
										  mmap.getPointOnEdgeFromValueAndPath(borders.back().first, location)));
	newLines[(*cylinder).second].push_back(std::pair<PointOnEdge, PointOnEdge>(mmap.getPointOnEdgeFromValueAndPath(borders.front().second, location),
										   mmap.getPointOnEdgeFromValueAndPath(borders.back().second, location)));
	// epsilon adjustment
	newLines[(*cylinder).second].back().first.setLocation(1. - newLines[(*cylinder).first].back().first.getLocation());
	newLines[(*cylinder).second].back().first.adjustLocation(newLines[(*cylinder).first].back().first);

	newLines[(*cylinder).second].back().second.setLocation(1. - newLines[(*cylinder).first].back().second.getLocation());
	newLines[(*cylinder).second].back().second.adjustLocation(newLines[(*cylinder).first].back().second);
      }
    }
  }

  return newLines;
}


std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > MeshPLCut::getPathExtremaCutCylinders(Mesh & mesh, double maxratio,
														 const MTiling & mtiling,
														 const std::vector<std::pair<unsigned int, unsigned int> > & cylinders) {
  std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > newLines;

  if (maxratio <= 0)
    return newLines;


  for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator cylinder = cylinders.begin(); cylinder != cylinders.end(); ++cylinder) {
    std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > borders;
    const unsigned int nbsections = getNbSectionsUpdateBorders(mesh, maxratio, mtiling, *cylinder, borders);

    if (nbsections > 1) {
      const double l1 = mesh.getLength(borders.front().first);
      const double l2 = mesh.getLength(borders[1].first);

      const double subl1 = l1 / nbsections;
      double location1 = 0.;
      const double subl2 = l2 / nbsections;
      double location2 = l2;
      for(unsigned int idcut = 0; idcut != (unsigned int)(nbsections - 1); ++idcut) {
	location1 += subl1;
	location2 -= subl2;
	assert(mesh.getPointOnPath(borders.front().first, location1).distance(mesh.getPointOnPath(borders.front().second, l1 - location1)) < 1e-3);
	assert(mesh.getPointOnPath(borders.back().first, location2).distance(mesh.getPointOnPath(borders.back().second, l2 - location2)) < 1e-3);

	newLines[(*cylinder).first].push_back(std::pair<PointOnEdge, PointOnEdge>(mesh.getPointOnPath(borders.front().first, location1),
										  mesh.getPointOnPath(borders.back().first, location2)));
	newLines[(*cylinder).second].push_back(std::pair<PointOnEdge, PointOnEdge>(mesh.getPointOnPath(borders.front().second, l1 - location1),
										   mesh.getPointOnPath(borders.back().second, l2 - location2)));
	// epsilon adjustment
	newLines[(*cylinder).second].back().first.setLocation(1. - newLines[(*cylinder).first].back().first.getLocation());
	newLines[(*cylinder).second].back().first.adjustLocation(newLines[(*cylinder).first].back().first);

	newLines[(*cylinder).second].back().second.setLocation(1. - newLines[(*cylinder).first].back().second.getLocation());
	newLines[(*cylinder).second].back().second.adjustLocation(newLines[(*cylinder).first].back().second);
      }
    }
  }


  return newLines;
}



std::vector<PLPath> MeshPLCut::getPathsCutCylinders(const std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > & newLines,
						    const MeshMap & mmap) {
  std::vector<PLPath> paths;
  for(std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > >::const_iterator ctile = newLines.begin(); ctile != newLines.end(); ++ctile) {
    for(std::vector<std::pair<PointOnEdge, PointOnEdge> >::const_iterator l = (*ctile).second.begin(); l != (*ctile).second.end(); ++l) {
      PLPath newpath = mmap.computeLevelSetCC((*l).first);
      assert(newpath.back().distance((*l).first) < 1e-5);
      assert(newpath.front().distance((*l).second) < 1e-5);
      newpath.front() = (*l).second;

#ifndef NDEBUG
      for(std::vector<PLPath>::const_iterator p = paths.begin(); p != paths.end(); ++p) {
	assert(!(*p).intersects(newpath));
      }
#endif
      paths.push_back(newpath);
    }
  }
  return paths;
}

Mesh MeshPLCut::cutCylinders(Mesh & mesh, const MeshMap & mmap, double maxratio,
			     std::vector<std::pair<VertexID, VertexID> > & cylDesc) {
  MTiling mtiling(mesh);

  MeshMapUInt idTiles = mtiling.getIdTiles();
  std::vector<std::pair<unsigned int, unsigned int> > cylinders;
  for(std::vector<std::pair<VertexID, VertexID> >::const_iterator cd = cylDesc.begin();
      cd != cylDesc.end(); ++cd) {
    cylinders.push_back(std::pair<unsigned int, unsigned int>(idTiles.getValue((*cd).first),
							      idTiles.getValue((*cd).second)));
    assert(cylinders.back().first != cylinders.back().second);
  }

  std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > newLines = getPathExtremaCutCylinders(mesh, maxratio, mtiling, cylinders, mmap);

#ifndef NDEBUG
  for(std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > >::const_iterator nl = newLines.begin(); nl != newLines.end(); ++nl)
    for(std::vector<std::pair<PointOnEdge, PointOnEdge> >::const_iterator pp = (*nl).second.begin(); pp != (*nl).second.end(); ++pp) {
      assert((*nl).first == idTiles.getValue((*pp).first));
      assert((*nl).first == idTiles.getValue((*pp).second));
      assert(fabs(mmap.getValue((*pp).first) - mmap.getValue((*pp).second)) < 1e-5);
    }
#endif

  std::vector<PLPath> paths = getPathsCutCylinders(newLines, mmap);

  return cutMeshPaths(mesh, paths);
}

Mesh MeshPLCut::cutCylindersByQuadParam(Mesh & mesh, double maxratio) {
  if (maxratio <= 0.)
    return mesh;

  // get a topological description of the mesh
  MTiling mtiling(mesh);

  std::vector<std::pair<unsigned int, unsigned int> > cylinders = mtiling.getCylindersByQuad();

  // get the extrema of each path
  std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > newLines = getPathExtremaCutCylinders(mesh, maxratio, mtiling, cylinders);
  std::vector<unsigned int> cutTiles;
  for(std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > >::const_iterator nl = newLines.begin(); nl != newLines.end(); ++nl)
    cutTiles.push_back((*nl).first);

  // compute the mapping that will be used for the path computation
  Mapping2D3D quadMapping = mtiling.getParameterization(cutTiles);
  MeshMap mmap2D(mesh, -1.);

  for(std::vector<unsigned int>::const_iterator t = cutTiles.begin(); t != cutTiles.end(); ++t) {
    assert(newLines[*t].size() != 0);
    assert(mtiling.getTile(*t).getNbMultiPoints() == 4);
    const Point2D & p11 = mmap2D[mtiling.getTile(*t).getMultiPoint(0)];
    const Point2D & p12 = mmap2D[mtiling.getTile(*t).getMultiPoint(1)];
    const Point2D & p21 = mmap2D[mtiling.getTile(*t).getMultiPoint(2)];
    const Point2D & p22 = mmap2D[mtiling.getTile(*t).getMultiPoint(3)];
    const Coord2D firstp2D = quadMapping.get2DCoord(newLines[*t].front().first);
    Box2D bbox2D(p11, p12);
    bbox2D.addPoint(p21).addPoint(p22);
    assert(bbox2D.inBorder(firstp2D));
    const bool px = ((fabs(bbox2D.getMinX() - firstp2D.get2DX()) <= 1e-7) ||
		     (fabs(bbox2D.getMaxX() - firstp2D.get2DX()) <= 1e-7));

    std::vector<VertexID> open;
    open.push_back(mtiling.getTile(*t).getMultiPoint(0));
    mmap2D[mtiling.getTile(*t).getMultiPoint(0)] = px ? quadMapping[mtiling.getTile(*t).getMultiPoint(0)].get2DY() : quadMapping[mtiling.getTile(*t).getMultiPoint(0)].get2DX();
    while(!open.empty()) {
      VertexID p = open.back();
      open.pop_back();
      Point3D & p3D = mesh.point(p);
      for(std::deque<VertexID>::const_iterator nb = p3D.getNeighbours().begin(); nb != p3D.getNeighbours().end(); ++nb)
	if (mmap2D[*nb] == -1) {
	  open.push_back(*nb);
	  Point2D & nb2D = quadMapping[*nb];
	  mmap2D[*nb] = px ? nb2D.get2DY() : nb2D.get2DX();
	}
    }
  }

  // finally, compute the corresponding paths
  std::vector<PLPath> paths = getPathsCutCylinders(newLines, mmap2D);

  return cutMeshPaths(mesh, paths);
}

void MeshPLCut::addScalarFunction(MeshMap & mmap) {
  scalarfunctions.push_back(&mmap);
  newSFValues.push_back(std::vector<double>());
}

void MeshPLCut::removeScalarFunction() {
  if (scalarfunctions.size() != 0) {
    scalarfunctions.pop_back();
    newSFValues.pop_back();
    assert(scalarfunctions.size() == newSFValues.size());
  }
  else {
    throw Exception("removeScalarFunction(): cannot remove scalar function (empty list)");
  }
}


void MeshPLCut::addNewSFValue(const PointOnEdge & pOe) {
  std::vector<MeshMap *>::const_iterator sf = scalarfunctions.begin();
  for(std::vector<std::vector<double> >::iterator sfv = newSFValues.begin(); sfv != newSFValues.end(); ++sfv, ++sf) {
    (*sfv).push_back((**sf).getValue(pOe));
  }
}


void MeshPLCut::setNewMeshSV(Mesh & mesh) {
  for(std::vector<MeshMap *>::iterator sf = scalarfunctions.begin(); sf != scalarfunctions.end(); ++sf) {
    if ((**sf).getNbValues() != mesh.getNbPoints())
      std::cout << "Warning: bad number of points in the scalar funciton" << std::endl;
    (**sf).setMesh(mesh);
  }
}

Mesh MeshPLCut::cutCylindersByShPathParam(Mesh & mesh, double maxratio) {
  if (maxratio <= 0.)
    return mesh;
  FastMarching fastmarching;

  // get a topological description of the mesh
  MTiling mtiling(mesh);

  std::vector<std::pair<unsigned int, unsigned int> > cylinders = mtiling.getCylindersByQuad();

  // get the extrema of each path
  std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > > newLines = getPathExtremaCutCylinders(mesh, maxratio, mtiling, cylinders);

  // then add the boundary points to the mesh
  std::vector<std::pair<VertexID, VertexID> > bdIds;

  clearNewSFValues();

  Mesh tmpmesh(mesh);
  for(std::map<unsigned int, std::vector<std::pair<PointOnEdge, PointOnEdge> > >::const_iterator nlptile = newLines.begin();
      nlptile != newLines.end(); ++nlptile)
    for(std::vector<std::pair<PointOnEdge, PointOnEdge> >::const_iterator pts = (*nlptile).second.begin(); pts != (*nlptile).second.end(); ++pts) {
      VertexID v1 = tmpmesh.addPointOnEdge((*pts).first);
      assert(tmpmesh.point(v1).distance((*pts).first) < 1e-3);
      VertexID v2 = tmpmesh.addPointOnEdge((*pts).second);
      assert(tmpmesh.point(v2).distance((*pts).second) < 1e-3);
      bdIds.push_back(std::pair<VertexID, VertexID>(v1, v2));
      addNewSFValue((*pts).first);
      addNewSFValue((*pts).second);
    }

  addNewScalarValues();
  setNewMeshSV(tmpmesh);

  // finally, compute the corresponding paths
  std::vector<PLPath> paths;
  for(std::vector<std::pair<VertexID, VertexID> >::const_iterator ids = bdIds.begin(); ids != bdIds.end(); ++ids) {
    PLPath newpath = fastmarching.getShortestPath(tmpmesh, (*ids).first, (*ids).second);

    if (newpath.size() > 2) {
      if (tmpmesh.isBoundaryEdge(newpath[1])) {
	std::cout << "Warning: computed path is starting by a subpart of an existing border" << std::endl;
	while((newpath.size() > 2) && tmpmesh.isBoundaryEdge(newpath[1]))
	  newpath.removePointAtBeginning();
      }
      if (tmpmesh.isBoundaryEdge(newpath[newpath.size() - 2])) {
	std::cout << "Warning: computed path is ending by a subpart of an existing border" << std::endl;
	while((newpath.size() > 2) && tmpmesh.isBoundaryEdge(newpath[newpath.size() - 2]))
	  newpath.removePointAtEnd();
      }
    }

#ifndef NDEBUG
    for(std::vector<PLPath>::const_iterator p = paths.begin(); p != paths.end(); ++p) {
      assert(!(*p).intersects(newpath));
    }
#endif
    paths.push_back(newpath);
  }

  return cutMeshPaths(tmpmesh, paths);
}

