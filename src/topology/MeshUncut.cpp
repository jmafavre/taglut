/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshUncut.h"
#include "Triangle.h"

using namespace Taglut;
MeshUncut::MeshUncut(Mesh & mesh_t) : MeshManipulator(mesh_t) { }


Mesh & MeshUncut::getUMesh() {
  if (Umesh.getNbPoints() == 0)
    buildUMesh();

  return Umesh;
}


void MeshUncut::buildUMesh() {
  // first compute "same location" property
  computeSameLocationList();
  std::deque<VertexID> inversedRef;
  VertexID * ref = new VertexID[(*mesh).getNbPoints()];

  // then compute merged references
  for(VertexID i = 0; i < (*mesh).getNbPoints(); ++i) // uint better than iterator
    if ((sameLocation[i].size() != 0) && (sameLocation[i][0] < i) && ((*mesh).point(i).getIsBoundary())) {
      bool ok = false; // check for first boundary point with same location, with id smaller than i
      for(std::deque<VertexID>::const_iterator it = sameLocation[i].begin(); it != sameLocation[i].end(); ++it)
	if (*it >= i)
	  break;
	else if ((*mesh).point(*it).getIsBoundary()) {
	  ok = true;
	  ref[i] = ref[sameLocation[i][0]];
	  break;
	}
      if (!ok) {
	ref[i] = inversedRef.size();
	inversedRef.push_back(i);
      }
    }
    else {
      ref[i] = inversedRef.size();
      inversedRef.push_back(i);
    }

  // then buid new mesh.
  // first, build point list
  std::deque<Point3D> points;
  for(std::deque<VertexID>::const_iterator i = inversedRef.begin(); i != inversedRef.end(); ++i) {
    // add point to the new mesh
    points.push_back((*mesh).point(*i));

    Point3D & point = points.back();

    // set new Id
    point.setId(ref[point.getId()]);

    // remove triangles and neighbours
    point.clearNeighboursAndTriangles();
  }

  Umesh.addPoints(points);

  // then build triangle list
  std::deque<Triangle> triangles;
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    triangles.push_back(*t);
    Triangle & triangle = triangles.back();
    for(unsigned int i = 0; i < 3; ++i)
      triangle.setVertexId(i, ref[triangle.getVertexId(i)]);
  }
  Umesh.addTriangles(triangles);
  Umesh.initIsBoundaryFlag(); // order neighbours and init isboundary flag

}

