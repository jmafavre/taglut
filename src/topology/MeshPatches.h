/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESHPATCHES
#define MESHPATCHES

#include "CImgUse.h"
#include "Point3D.h"
#include "Exception.h"
#include "MeshPathes.h"
#include "IndentManager.h"
#include "Length.h"

#include <map>

namespace Taglut {
  class Mesh;

  /**
     @class MeshPatches
     Mesh manipulator using path cuts and adding patches
  */
  class MeshPatches : public MeshPathes {
  private:
    /** Max length for pathes */
    float maxLength;

    /** true if patche has been applied, false otherwise */
    bool ok;

    /**
       Boundary pathes and cut pathes
    */
    std::deque<std::deque<VertexID> > allPathes;

    /**
       add patches in boundaries
    */
    int addPatchesInBoundaries(int maxPatch, const std::string & logMesh, const Taglut::IndentManager & iManager);

    /**
       add patches in shortest cycles
    */
    void addPatchesInShortestCycles(int maxPatch = -1, enum OptimEC optimMethod = OECTruncMinMax, const std::string & logMesh = "", const Taglut::IndentManager & iManager = Taglut::IndentManager());

    void addPatchesInShortestCyclesPseudoVolumic(const UCharCImg & mask,
						 const UCharCImg & region,
						 bool inside = true,
						 int maxPatch = -1,
						 const std::string & logMesh = "",
						 const IndentManager & iManager = Taglut::IndentManager());

    /**
       add boundary path and patch it
    */
    void addBPath(const std::deque<VertexID> & path);


    /**
       add patch using path given in parameter
    */
    void addPatch(const std::deque<VertexID> & path);

    /**
       Load pathes from file and build corresponding patches
    */
    void fromFile(const std::string & filename, const Taglut::IndentManager & iManager);


  public:
    /**
       Default constructor
       @param mesh Mesh used by this manipulator
       @param lMethod length method used by dijkstra and to compute path length
       @param maxLength Maximum length value to patch mesh (-1: no maximum value)
    */
    MeshPatches(Mesh & mesh, const LengthEdge & lMethod, float maxLength = -1);

    /**
       Default constructor with euclidean length
       @param mesh Mesh used by this manipulator
       @param maxLength Maximum length value to patch mesh (-1: no maximum value)
    */
    MeshPatches(Mesh & mesh, float maxLength = -1);

    /**
       Add patches to current mesh (if filename is defined, first load patch pathes from file) using a pseudo-volumic approach
       @param mask Mask corresponding to the surface (inside/ouside values on voxels)
       @param region Mask corresponding to the region where we want to apply a pseudo-volumic approach
       @param inside In the region, only add loops following the rule define by this boolean (inside: cuts inside the material)
       @param maxPatch Maximum number of patches
       @param inputPathFile Input file containing the paths to use
       @param logMesh Prefix of the log files containing meshes at each step of the patching process
       @param iManager Indent manager used for outputs
    */
    void patchMeshPseudoVolumic(const UCharCImg & mask,
				const UCharCImg & region,
				bool inside = true,
				int maxPatch = -1,
				const std::string & inputPathFile = "",
				const std::string & logMesh = "",
				const Taglut::IndentManager & iManager = Taglut::IndentManager());


    /**
       Add patches to current mesh (if filename is defined, first load patch pathes from file)
       @param maxPatch Maximum number of patches
       @param optimMethod optimization method for the loop search
       @param inputPathFile Input file containing the paths to use
       @param logMesh Prefix of the log files containing meshes at each step of the patching process
       @param iManager Indent manager used for outputs
    */
    void patchMesh(int maxPatch = -1, enum OptimEC optimMethod = OECTruncMinMax, const std::string & inputPathFile = "", const std::string & logMesh = "", const Taglut::IndentManager & iManager = Taglut::IndentManager());

    /**
       Add patches to current mesh, only in non-boundary loops (if filename is defined, first load patch pathes from file)
       @param maxPatch Maximum number of patches
       @param optimMethod optimization method for the loop search
       @param inputPathFile Input file containing the paths to use
       @param logMesh Prefix of the log files containing meshes at each step of the patching process
       @param iManager Indent manager used for outputs
    */
    void patchMeshInLoops(int maxPatch = -1, enum OptimEC optimMethod = OECTruncMinMax, const std::string & inputPathFile = "", const std::string & logMesh = "", const Taglut::IndentManager & iManager = Taglut::IndentManager());


    /**
       Close boundaries
    */
    void closeBoundaries(int maxPatch = -1, const std::string & logMesh = "", const Taglut::IndentManager & iManager = Taglut::IndentManager());

    /**
       add cut path and patch result pathes
    */
    void addCPath(const std::deque<VertexID> & path);

  };
}

#endif
