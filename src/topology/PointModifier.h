/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_MODIFIER
#define POINT_MODIFIER

#include <string>
#include <deque>
#include "Mesh.h"
#include "Coord3D.h"

namespace Taglut {
  /**
     @class PointModifier

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-01-22
     @brief Point modifier (using random methods, translations)
  */
  class PointModifier {
  private:
    /** available random methods */
    enum RandomMethod { RMuniform, RMgaussian, RMtranslation, RMbarycenter, RMbarycenter2 };

    /**
       Manipulated mesh
    */
    const Mesh * mesh;

    /** validity has been checked */
    bool validityChecked;

    /** method used to select point */
    const std::string method;

    /** options used during point manipulation */
    std::string options;

    /** random method used to move points */
    enum RandomMethod rMethod;

    /** true if the modification is uniform (defined for all axis at the same time) */
    bool isUniform;

    /* UNIFORM CASE */

    /** value of distance used during moving points */
    double value;

    /** true if the max value has to be adapted for each mesh (percentage) */
    bool isPercentage;

    /** value of distance used during moving points (percentage value) */
    double valueP;

    /* NON UNIFORM: X */

    /** value of distance used during moving points */
    double directionalValue[3];

    /** true if the max value has to be adapted for each mesh (percentage) */
    bool directionalIsPercentage[3];

    /** value of distance used during moving points (percentage value) */
    double directionalValueP[3];

    /** number of points on the mesh */
    VertexID nbPoints;

    /** computed value for moving along the given axis */
    double movingValue(unsigned int axis) const;

    /** set isUniform value. Return false if the number of parameters is not valid */
    bool setIsUniform(unsigned int nb);

    /** set values for each axis or global value if isUniform == true */
    void setValues(const std::deque<std::string> & optionsList);

    /** given a list of points, compute a random barycenter */
    Coord3D randomBarycenter(const std::deque<VertexID> & points) const;

    /** given a list of points, compute a random barycenter with a main point */
    Coord3D randomBarycenter2(const std::deque<VertexID> & points) const;

  public:
    /**
       Constructor using a string description of method and options for
       points' manipulation
    */
    PointModifier(const std::string & method, const std::string & options);

    /**
       Set mesh that will be manipulated
    */
    void setMesh(const Mesh & mesh);

    /**
       Return a string that describe available modification methods
    */
    static std::string helpModificationMethods();

    /**
       Return true if the current point modifier is a valid
       modifier (checking method and options)
    */
    bool isValidModifier();

    /**
       Given a point, modify using choosen method
    */
    void modify(Point3D & point);
  };
}

#endif
