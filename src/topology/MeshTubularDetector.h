/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_TUBULAR_DETECTOR
#define MESH_TUBULAR_DETECTOR

#include "MeshMap.h"

namespace Taglut {
  class MeshManipulator;

  /** @class MeshTubularDetector
      A tool to detect tubular regions on meshes
  **/
  class MeshTubularDetector : public MeshManipulator {
  private:
    /**
       A descriptor for tubular properties
    **/
    MeshMapT<unsigned int> nbBdNeighbourhood;

    void setFlagsInBall(const Point3D & center, double radius, const Point3D & point);

    bool hasNeighbourOutBall(const Point3D & center, double radius, const Point3D & point) const;

    bool cleanBoundariesInBall(Point3D & point);

    void setTriangleFlagsInBall(Point3D & point);

    void resetFlagsInBall(const Point3D & point);

    unsigned int getNbBoundariesInsideBall(Point3D & point);

  public:
    /**
       Default constructor
    **/
    MeshTubularDetector(Mesh & mesh);


    /**
       For a given radius, compute the tubular properties for each point of
       the manipulated mesh
    */
    const MeshMapT<unsigned int> & computeLocalFeatures(double radius);
  };

}

#endif
