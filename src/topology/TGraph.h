/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TGRAPH
#define TGRAPH

#include "IDTypes.h"
#include "Graph.h"

namespace Taglut {
  /**
     @class TGraph

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2007-05-31
     @brief A topological graph with \c Node corresponding to cross points and boundaries, and edges corresponding to ribbons.
  */
  class TGraph : public Graph {
  private:

    /** Merge the nodes defined by id */
    void mergeNodes(NodeID id1, NodeID id2);

  public:
    /** Flag of a border node */
    static int BORDER;
    /** Flag of a cross node */
    static int CROSS;

    /** Default constructor */
    TGraph();

    /** Copy constructor */
    TGraph(const Graph & g);

    /** add a cross node to the current graph */
    Node & addCrossNode();

    /** add a cross node to the current graph */
    Node & addBoundaryNode();

    /** topological simplification */
    void simplification(bool verbose = false);

    /** standard output of the current graph */
    void trace() const;

    /** return true if the given node \p n has a border neighbour */
    bool hasBorderNeighbour(const Node & n) const;

    /** return a border neighbour of the given node \p n (if exists) */
    NodeID getBorderNeighbour(const Node & n) const;

  };
}

#endif
