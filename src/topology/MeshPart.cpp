/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <set>
#include <map>
#include <string>
#include <sstream>
#include "MeshMap.h"
#include "MeshPart.h"
#include "Point3D.h"
#include "TGraph.h"
#include "NLoop.h"
#include "Mesh.h"

using namespace Taglut;

MeshPart::MeshPart(Mesh & mesh_l, bool full) : MeshManipulator(mesh_l), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  initPart(mesh_l.getNbPoints(), mesh_l.getNbTriangles());
  if (full) {
    for(VertexID i = 0; i < mesh_l.getNbPoints(); ++i)
      addPoint(i);
    for(TriangleID i = 0; i < mesh_l.getNbTriangles(); ++i)
      addTriangle(i);
  }
}

MeshPart::MeshPart() : MeshManipulator(), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  nbPoints = nbTriangles = 0;
}

MeshPart::MeshPart(Mesh & mesh_l, const Point3D & center, double radius, bool disc) : MeshManipulator(mesh_l), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  initPart(mesh_l.getNbPoints(), mesh_l.getNbTriangles());
  if (disc)
    buildDiscPart(center, radius);
  else // ball
    buildBallPart(center, radius);
}

MeshPart::MeshPart(Mesh & mesh_l, const Point3D & point1, const Point3D & point2) : MeshManipulator(mesh_l), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  initPart(mesh_l.getNbPoints(), mesh_l.getNbTriangles());
  buildBoxPart(point1, point2);
}

MeshPart::MeshPart(Mesh & mesh_l, const Point3D & center) : MeshManipulator(mesh_l), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  initPart(mesh_l.getNbPoints(), mesh_l.getNbTriangles());
  buildNearestCC(center);
}


MeshPart::MeshPart(Mesh & mesh_l, VertexID idP) : MeshManipulator(mesh_l), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  initPart(mesh_l.getNbPoints(), mesh_l.getNbTriangles());
  buildNearestCC(mesh_l.point(idP));
}

MeshPart::MeshPart(const MeshPart & meshPart) : MeshManipulator(meshPart),
						fGroupSize(meshPart.fGroupSize),
						points(meshPart.points),
						triangles(meshPart.triangles) {
  nbPoints = meshPart.nbPoints;
  nbTriangles = meshPart.nbTriangles;
  nbBoundaries = meshPart.nbBoundaries;
  initExtrema();
  if (meshPart.simpleCut != NULL)
    simpleCut = new MeshPart(*(meshPart.simpleCut));
  else
    simpleCut = NULL;
}

MeshPart::MeshPart(Mesh & mesh_l, TriangleID idT, const NLoop & paths) : MeshManipulator(mesh_l), nbBoundaries(-1), fGroupSize(-1) {
  initExtrema();
  simpleCut = NULL;
  length = NULL;
  initPart(mesh_l.getNbPoints(), mesh_l.getNbTriangles());
  buildCompCC(paths, idT);
}


void MeshPart::initPart(VertexID nbP, TriangleID nbT, bool inside) {
  if (points.size() != 0)
    points.clear();
  if (triangles.size() != 0)
    triangles.clear();
  points.insert(points.begin(), nbP, inside);
  triangles.insert(triangles.begin(), nbT, inside);
  nbPoints = inside * nbP;
  nbTriangles = inside * nbT;
}

MeshPart & MeshPart::operator=(const MeshPart & meshPart) {
  (*((MeshManipulator *) this)) = meshPart;
  nbBoundaries = meshPart.nbBoundaries;
  fGroupSize = meshPart.fGroupSize;
  points = meshPart.points;
  triangles = meshPart.triangles;
  nbPoints = meshPart.nbPoints;
  nbTriangles = meshPart.nbTriangles;
  initExtrema();
  if (meshPart.simpleCut != NULL)
    simpleCut = new MeshPart(*(meshPart.simpleCut));
  else
    simpleCut = NULL;
  return *this;
}


MeshPart::~MeshPart() {
  if (simpleCut != NULL)
    delete(simpleCut);
}

void MeshPart::cropSimpleCut(VertexID v) {
  if (simpleCut == NULL)
    buildSimpleCut(v);

  (*simpleCut).cropMesh();
}
Mesh MeshPart::buildCropMeshSimpleCut(VertexID v) {
  if (simpleCut == NULL)
    buildSimpleCut(v);

  return (*simpleCut).buildCropMesh();
}

Mesh MeshPart::buildRemovePoles(double p1, double p2, const Coord3D & axis, const Coord3D & barycenter) {
    initPart(mesh->getNbPoints(), mesh->getNbTriangles(), false);
    Line3D line(barycenter, axis.getNormalize());
    std::list<double> coords;

    for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i) {
        coords.push_back(line.getCoords((*i).projectionOnAxis(line)));
    }
    std::list<double>::iterator min = std::min_element(coords.begin(), coords.end());
    std::list<double>::iterator max = std::max_element(coords.begin(), coords.end());
    const double d = *max - *min;
    const double dMin = *min + p1 * d;
    const double dMax = *max - p2 * d;

    // set point flags according to length and add points in MeshPart
    Mesh::point_iterator i = mesh->point_begin();
    for(std::list<double>::const_iterator v = coords.begin(); v != coords.end(); ++v, ++i)
        if ((*v >= dMin) && (*v <= dMax))
            (*i).setFlag(1);
        else
            (*i).setFlag(0);

    // add triangles in MeshPart
    for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
        if(((*mesh).point((*t).getP1()).getFlag() > 0) &&
                ((*mesh).point((*t).getP2()).getFlag() > 0) &&
                ((*mesh).point((*t).getP3()).getFlag() > 0)) {
            addTriangle((*t).getId());
            (*mesh).point((*t).getP1()).setFlag(2);
            (*mesh).point((*t).getP2()).setFlag(2);
            (*mesh).point((*t).getP3()).setFlag(2);
        }
    }

    for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
        if ((*i).getFlag() == 2)
            addPoint((*i).getId());

    return buildCropMesh();
}



void  MeshPart::addPoint(VertexID idP) {
  if (points[idP] == false)
    ++nbPoints;
  points[idP] = true;
  nbBoundaries = -1;
  fGroupSize = -1;
  updateExtrema(idP);
  if (simpleCut != NULL) {
    delete simpleCut;
    simpleCut = NULL;
  }
}

void  MeshPart::addPoint(const Point3D & pt) {
  if (points[pt.getId()] == false)
    ++nbPoints;
  points[pt.getId()] = true;
  nbBoundaries = -1;
  fGroupSize = -1;
  updateExtrema(pt.getId());
  if (simpleCut != NULL) {
    delete simpleCut;
    simpleCut = NULL;
  }
}


void  MeshPart::addTriangleAndPoints(TriangleID idT) {
  addTriangle(idT);
  for(unsigned int i = 0; i < 3; ++i)
    if (!isInsidePoint((*mesh).triangle(idT).getVertexId(i)))
      addPoint((*mesh).triangle(idT).getVertexId(i));
}

void  MeshPart::addTriangle(TriangleID idT) {
  assert(triangles.size() == (*mesh).getNbTriangles());
  assert(idT < (*mesh).getNbTriangles());
  if (triangles[idT] == false)
    ++nbTriangles;
  triangles[idT] = true;
  nbBoundaries = -1;
  fGroupSize = -1;
  if (simpleCut != NULL) {
    delete simpleCut;
    simpleCut = NULL;
  }
}

VertexID  MeshPart::getNbPoints() const {
  return nbPoints;
}

VertexID MeshPart::getNbBadBPoints() {
  VertexID result = 0;
  initInsideAndBoundaryFlags();

  VertexID pid = 0;
  for(std::deque<bool>::const_iterator p = points.begin(); p != points.end(); ++p, ++pid)
    if ((*p) && (*mesh).point(pid).getFlag() == 2) {
      unsigned int nbBNb = 0;
      for(std::deque<VertexID>::const_iterator i = (*mesh).point(pid).getNeighbours().begin(); i != (*mesh).point(pid).getNeighbours().end(); ++i)
	if (isBoundaryEdgeTFlag(pid, *i, 1))
	  ++nbBNb;
      if (nbBNb != 2)
	++result;
  }

  return result;
}

TriangleID MeshPart::getNbBadTriangles() const {
  TriangleID result = 0;

  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    if ((*i) && (*mesh).triangle(tid).isEdge())
      ++result;

  return result;
}


TriangleID MeshPart::getNbTriangles() const {
  return nbTriangles;
}

Mesh & MeshPart::getMesh() {
  return (*mesh);
}

const Mesh & MeshPart::getMesh() const {
  return (*mesh);
}

VertexID MeshPart::cleanMesh() {
  VertexID result = (*mesh).getNbPoints();
  // check points
  VertexID nbP = 0;
  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((((*mesh).orderNeighbours((*i).getId(), false)) && !(*i).getIsBoundary()) || ((*mesh).getNbNextBPoint((*i).getId()) == 2)) {
      // if good flag value, add point to the meshpart
      (*i).setFlag(1);
      ++nbP;
    }
    else
      (*i).setFlag(0);

  // add triangles
  for(Mesh::const_triangle_iterator i = (*mesh).triangle_begin(); i != (*mesh).triangle_end(); ++i)
    if (((*mesh).point((*i).getP1()).getFlag() != 0) &&
	((*mesh).point((*i).getP2()).getFlag() != 0) &&
	((*mesh).point((*i).getP3()).getFlag() != 0)) {
      addTriangle((*i).getId());
      (*mesh).point((*i).getP1()).incFlag();
      (*mesh).point((*i).getP2()).incFlag();
      (*mesh).point((*i).getP3()).incFlag();
    }

  // add points if has a triangle inside
  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() > 1)
      addPoint((*i).getId());


  cropMesh();
  return result - (*mesh).getNbPoints();
}


std::deque<MeshPart> MeshPart::getCC() {
  std::deque<MeshPart> cc;
  VertexID nbP = 0;
  // flags initialisation
  initInsideFlags();


  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 1) {
      cc.push_front(MeshPart(*mesh));
      computeCC(*i, cc.front());

      for(Mesh::point_iterator j = (*mesh).point_begin(); j != (*mesh).point_end(); ++j)
	if (cc.front().isInsidePoint((*j).getId()))
	  (*j).setFlag(0);

      nbP += cc.front().getNbPoints();
      if (nbP >= nbPoints)
	break;
    }

  return cc;
}


void MeshPart::computeCC(Point3D & pt, MeshPart & mpartCC) {
  // point is selected
  pt.setFlag(3);
  mpartCC.addPoint(pt);

  for(std::deque<TriangleID>::const_iterator t = pt.getTriangles().begin(); t != pt.getTriangles().end(); ++t)
    if ((*mesh).triangle(*t).getFlag() == 1) {
      (*mesh).triangle(*t).setFlag(3);
      mpartCC.addTriangle(*t);
    }

  for(std::deque<VertexID>::const_iterator n = pt.getNeighbours().begin(); n != pt.getNeighbours().end(); ++n) {
    Point3D & nb = (*mesh).point(*n);
    if (nb.getFlag() == 1)
      computeCC(nb, mpartCC);
  }

}


std::deque<MeshPart> MeshPart::getCompCC() {
  std::deque<MeshPart> cc;
  TriangleID nbT = (*mesh).getNbTriangles() - nbTriangles;
  TriangleID idNextT = 0;
  // flags initialisation
  initInsideFlags();

  while (nbT > 0) {
    cc.push_front(MeshPart((*mesh)));

    // find point not seen before
    while((*mesh).triangle(idNextT).getFlag() != 0) {
      ++idNextT;
      assert(idNextT < (*mesh).getNbTriangles());
    }

    // then compute next CC
    computeCompCC(idNextT, cc.front());

    nbT -= cc.front().getNbTriangles();

    // and set flag for points seen in last CC
    if (nbT > 0)
      (*mesh).setPointFlags(3, 1);

  }
  return cc;
}


void MeshPart::computeCompCC(TriangleID idT, MeshPart & mpartCC) {
  /**
     Flags used:
     - 0: not seen before
     - 1: inside current meshPart or complementary meshpart seen before
     - 2: inside complementary meshpart
     - 3: both inside meshPart and the complementary meshpart currently built (only for points)
  */
  std::deque<TriangleID> open;

  (*mesh).triangle(idT).setFlag(3);

  open.push_back(idT);

  while(open.size() != 0) {
    TriangleID next = open.front();
    open.pop_front();
    mpartCC.addTriangle(next);
    Triangle & tnext = (*mesh).triangle(next);

    // add point inside meshpart
    for(unsigned int i = 0; i < 3; ++i) // iterator not available
      if ((*mesh).point(tnext.getVertexId(i)).getFlag() < 2) {
	mpartCC.addPoint(tnext.getVertexId(i));
	(*mesh).point(tnext.getVertexId(i)).incFlag(2);
      }

    // for each neighbour triangle of current triangle
    for(unsigned int i = 0; i < 3; ++i) { // iterator not available
      Point3D & pt = (*mesh).point(tnext.getVertexId(i));
      VertexID nextPointId = tnext.getVertexId((i + 1) % 3);
      if (!((pt.getFlag() % 2 == 1) && ((*mesh).point(nextPointId).getFlag() % 2 == 1)))
	for(std::deque<TriangleID>::const_iterator t = pt.getTriangles().begin(); t != pt.getTriangles().end(); ++t) {
	  Triangle & tt = (*mesh).triangle(*t);
	  if (tt.hasPoint(nextPointId) && (tt.getFlag() == 0)) {
	    tt.setFlag(3);
	    open.push_back(*t);
	  }
	}
    }
  }

}


void MeshPart::addPointsAndTriangles() {
  // add points
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
      addPoint((*p).getId());


  // add triangles
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
      addTriangle((*t).getId());
}


void MeshPart::addPointsAndTriangles(int flagValueP, int flagValueT) {
  // add points
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == flagValueP) // if good flag value, add point to the meshpart
      addPoint((*p).getId());


  // add triangles
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
    if ((*t).getFlag() == flagValueT) // if good flag value, add point to the meshpart
      addTriangle((*t).getId());
}


void MeshPart::addPointsAndNeighbours(int flag) {

  // add points
  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == flag) // if good flag value, add point to the meshpart
      addPoint((*i).getId());
    else // else watch for neighbours
      for(std::deque<VertexID>::const_iterator nb = (*i).getNeighbours().begin();
	  nb != (*i).getNeighbours().end(); ++nb)
	if ((*mesh).point(*nb).getFlag() == flag) {
	  addPoint((*i).getId());
	  break;
	}

  // add triangles
  for(Mesh::const_triangle_iterator i = (*mesh).triangle_begin(); i != (*mesh).triangle_end(); ++i)
    if (((*mesh).point((*i).getP1()).getFlag() == flag) ||
	((*mesh).point((*i).getP2()).getFlag() == flag) ||
	((*mesh).point((*i).getP3()).getFlag() == flag))
      addTriangle((*i).getId());

}


/**
   Build part of mesh using a box
   \param point1 first point
   \param point2 second point
*/
void MeshPart::buildBoxPart(const Point3D & point1, const Point3D & point2) {

  // set point flags according to length and add points in MeshPart
  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).isInside(point1, point2))
      (*i).setFlag(1);
    else
      (*i).setFlag(0);

  // add triangles in MeshPart
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    if(((*mesh).point((*t).getP1()).getFlag() > 0) &&
       ((*mesh).point((*t).getP2()).getFlag() > 0) &&
       ((*mesh).point((*t).getP3()).getFlag() > 0)) {
      addTriangle((*t).getId());
      (*mesh).point((*t).getP1()).setFlag(2);
      (*mesh).point((*t).getP2()).setFlag(2);
      (*mesh).point((*t).getP3()).setFlag(2);
    }
  }

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 2)
      addPoint((*i).getId());

}

void MeshPart::buildBallPart(const Point3D & center, double radius) {

  // set point flags according to length and add points in MeshPart
  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).distance(center) <= radius)
      (*i).setFlag(1);
    else
      (*i).setFlag(0);

  // add triangles in MeshPart
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    if(((*mesh).point((*t).getP1()).getFlag() > 0) &&
       ((*mesh).point((*t).getP2()).getFlag() > 0) &&
       ((*mesh).point((*t).getP3()).getFlag() > 0)) {
      addTriangle((*t).getId());
      (*mesh).point((*t).getP1()).setFlag(2);
      (*mesh).point((*t).getP2()).setFlag(2);
      (*mesh).point((*t).getP3()).setFlag(2);
    }
  }

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 2)
      addPoint((*i).getId());
}


void MeshPart::buildDiscPart(const Point3D & center, double radius) {
  VertexID idCenter = mesh->getNearestPoint(center);

  // compute geodesic length for each point to center (length = -1 if not in CC)
  computeDijkstra(idCenter);

  // build MeshPart using length (and radius)
  buildDiscPart(radius);

  // build simple cut
  buildSimpleCut();

}

void MeshPart::buildDiscPart(double radius) {

  // set point flags according to length and add points in MeshPart
  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((length[(*i).getId()] <= radius) && (length[(*i).getId()] >= 0)) {
      (*i).setFlag(1);
    }
    else
      (*i).setFlag(0);

  // add triangles in MeshPart
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    if(((*mesh).point((*t).getP1()).getFlag() > 0) &&
       ((*mesh).point((*t).getP2()).getFlag() > 0) &&
       ((*mesh).point((*t).getP3()).getFlag() > 0)) {
      addTriangle((*t).getId());
      (*mesh).point((*t).getP1()).setFlag(2);
      (*mesh).point((*t).getP2()).setFlag(2);
      (*mesh).point((*t).getP3()).setFlag(2);
    }
  }

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 2)
      addPoint((*i).getId());

}



Mesh MeshPart::buildCropMesh(bool warning) const {
  assert(points.size() == (*mesh).getNbPoints());
  std::vector<Coord3D> rPoints;
  rPoints.reserve(nbPoints);
  Coord3D p;
  std::vector<Triangle> rPrimitives;
  rPrimitives.reserve(nbTriangles);
  Triangle triangle;
  std::map<VertexID, VertexID, ltint<int> > idMap;
  VertexID id = 0;

  Mesh resultMesh;

  VertexID pid = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++pid)
    if (*i) {
      p(0) = (*mesh).point(pid).getX();
      p(1) = (*mesh).point(pid).getY();
      p(2) = (*mesh).point(pid).getZ();
      // add point to CImg structure
      rPoints.push_back(p);
      // add id correspondance
      idMap[pid] = id++;
    }

  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    if (*i) {
      Triangle t = (*mesh).triangle(tid);
      rPrimitives.push_back(Triangle(idMap[t.getP1()], idMap[t.getP2()], idMap[t.getP3()]));
    }

  resultMesh.buildMesh(rPoints, rPrimitives, warning);

  return resultMesh;
}


void MeshPart::cropMesh(bool warning) {
  (*mesh) = buildCropMesh(warning);
}

void MeshPart::initInsideFlags() {

  // points inside MeshPart set to "1", "0" otherwise
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    (*mesh).point(id).setFlag(*i);


  // triangles inside MeshPart set to "1", "0" otherwise
  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    (*mesh).triangle(tid).setFlag(*i);

}

void MeshPart::initSimpleCutBoundaryFlags() {
  if (simpleCut == NULL)
    buildSimpleCut();

  // for each point of MeshPart
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = (*simpleCut).points.begin();
      i != (*simpleCut).points.end(); ++i, ++id)
    if (*i)
      // for each triangle neighbour of id
      for(std::deque<TriangleID>::const_iterator t = (*mesh).point(id).getTriangles().begin();
	  t != (*mesh).point(id).getTriangles().end(); ++t)
	if ((*mesh).triangle(*t).getFlag() == 1) { // i.e. in MeshPart but not in simple cut ( = 2)
	  // set point flag and check next point
	  (*mesh).point(id).setFlag(3);
	  break;
	}

}

void MeshPart::initSimpleCutFlags() {
  if (simpleCut == NULL)
    buildSimpleCut();

  initInsideFlags();

  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = (*simpleCut).points.begin(); i != (*simpleCut).points.end(); ++i, ++id)
    if (*i) (*mesh).point(id).setFlag(2);


  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = (*simpleCut).triangles.begin(); i != (*simpleCut).triangles.end(); ++i, ++tid)
    if (*i) (*mesh).triangle(tid).setFlag(2);

  initSimpleCutBoundaryFlags();
}

void MeshPart::initInsideSimpleCutFlags() {
  if (simpleCut == NULL)
    buildSimpleCut();

  // points inside MeshPart set to "1", "0" otherwise
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = (*simpleCut).points.begin(); i != (*simpleCut).points.end(); ++i, ++id)
    (*mesh).point(id).setFlag(*i);


  // triangles inside MeshPart set to "1", "0" otherwise
  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = (*simpleCut).triangles.begin(); i != (*simpleCut).triangles.end(); ++i, ++tid)
    (*mesh).triangle(tid).setFlag(*i);
}


void MeshPart::initInsideAndBoundaryFlags() {
  // init flag with inside/outside value for triangles and points
  initInsideFlags();

  // for each point inside MeshPart, set flag value = 2 if is boundary one
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if ((*i) && ((*mesh).point(id).getFlag() == 1)) {
      if ((*mesh).point(id).getIsBoundary())
	(*mesh).point(id).setFlag(2);
      else {
	std::deque<TriangleID> & tr = (*mesh).point(id).getTriangles();
	for(std::deque<TriangleID>::const_iterator j = tr.begin(); j != tr.end(); ++j)
	  if ((*mesh).triangle(*j).getFlag() == 0) { // if a triangle connected to *i is outide of MeshPart, *i is a boundary point
	    (*mesh).point(id).setFlag(2);
	    break;
	  }
      }
    }
}


unsigned int MeshPart::getNbBoundaries() {

  // compute nbBoundaries if needed
  if (nbBoundaries == -1) {
    nbBoundaries = 0;

    initInsideAndBoundaryFlags();
    // for each point inside MeshPart
    VertexID id = 0;
    for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
      // if point is in a new boundary
      if ((*i) && ((*mesh).point(id).getFlag() == 2)) {
	// compute this boundary (set flag = 3 for each point inside boundary)
	computeBCCUsingPoints(id, 2, 3, 1);
	++nbBoundaries;
      }
  }

  return (unsigned int) nbBoundaries;

}


VertexID MeshPart::computeBCCUsingPoints(VertexID id, int iFlag, int fFlag, int tInside) {
  if ((*mesh).point(id).getFlag() != iFlag)
    return 0;

  std::deque<VertexID> & nb = (*mesh).point(id).getNeighbours();
  VertexID result = 0;

  (*mesh).point(id).setFlag(fFlag);

  for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
    if (((*mesh).point(*i).getFlag() == iFlag) && (isBoundaryEdgeTFlag(id, *i, tInside)))
      result += computeBCCUsingPoints(*i, iFlag, fFlag, tInside);


  return result;
}



unsigned int MeshPart::getNbTrianglesEdge(VertexID idP1, VertexID idP2, int tInside) const {
  std::deque<TriangleID> & tr = (*mesh).point(idP1).getTriangles();
  unsigned int result = 0;

  for(std::deque<TriangleID>::const_iterator i = tr.begin(); i != tr.end(); ++i)
    if (((*mesh).triangle(*i).hasPoint(idP2)) && ((*mesh).triangle(*i).getFlag() == tInside))
      ++result;

  return result;
}


bool MeshPart::isBoundaryEdgeTFlag(VertexID idP1, VertexID idP2, int tInside) const {
  std::deque<TriangleID> & tr = (*mesh).point(idP1).getTriangles();
  bool inside = false;
  bool outside = false;

  for(std::deque<TriangleID>::const_iterator i = tr.begin(); i != tr.end(); ++i)
    if ((*mesh).triangle(*i).hasPoint(idP2)) {
      if ((*mesh).triangle(*i).getFlag() == tInside) {
	if (inside) // another neighbour triangle has been found inside
	  return false;
	else {
	  if (outside) // another neighbour triangle has been found outside
	    return true;
	  else
	    inside = true;
	}
      }
      else {
	if (outside) // another neighbour triangle has been found outside
	  return false;
	else {
	  if (inside) // another neighbour triangle has been found inside
	    return true;
	  else
	    outside = true;
	}
      }
    }


  return inside;
}

std::deque<VertexID> MeshPart::getSimpleCutBorders() {
  initSimpleCutFlags();
  std::deque<VertexID> result;

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 3)
      result.push_front((*i).getId());

  return result;
}


std::deque<VertexID> MeshPart::getSlimSimpleCutBorders() {
  //   10: selected
  // 8, 9: rejected
  initSimpleCutFlags();
  std::deque<VertexID> result;

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 3)
      setFlagsSlimBorders((*i).getId());

  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 10)
      result.push_front((*i).getId());


  return result;
}

void MeshPart::setFlagsSlimBorders(VertexID point) {
  (*mesh).point(point).setFlag(9);

  for(std::deque<VertexID>::const_iterator i = (*mesh).point(point).getNeighbours().begin(); i != (*mesh).point(point).getNeighbours().end(); ++i)
    if ((*mesh).point(*i).getFlag() == 3) {
      if (isInsideEdgeTFlag(point, *i, 2))
	(*mesh).point(*i).setFlag(8);
      else
	(*mesh).point(*i).setFlag(10);
    }

  for(std::deque<VertexID>::const_iterator i = (*mesh).point(point).getNeighbours().begin(); i != (*mesh).point(point).getNeighbours().end(); ++i)
    if ((*mesh).point(*i).getFlag() == 8)
      setFlagsSlimBorders(*i);
}


VertexID MeshPart::getSimpleCutBoundarySize() {
  VertexID value = 0;
  if (simpleCut == NULL)
    buildSimpleCut();

  initSimpleCutFlags();

  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if ((*i) && ((*mesh).point(id).getFlag() == 3))
      ++value;

  return value;
}

std::deque<std::deque<VertexID> > MeshPart::getSimpleCutStrips() {
  std::deque<VertexID> stripsId = getSimpleCutStripsId();
  std::map<VertexID, VertexID> revertStripsId;
  std::deque<std::deque<VertexID> > strips;

  VertexID id = 1;
  for(std::deque<VertexID>::const_iterator i = stripsId.begin(); i != stripsId.end(); ++i) {
    revertStripsId[*i] = id;
    ++id;
    strips.push_front(std::deque<VertexID>());
  }

  for(Mesh::triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
    (*t).setFlag(revertStripsId[(*t).getFlag()]);

  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    for(std::deque<TriangleID>::const_iterator t = (*i).getTriangles().begin();
	t != (*i).getTriangles().end(); ++t) {
      int idS = (*mesh).triangle(*t).getFlag();
      if ((idS != 0) && ((strips[idS - 1].size() == 0) || (strips[idS - 1].front() != (*i).getId()))) {
	strips[idS - 1].push_front((*i).getId());
      }
    }
  return strips;
}

std::deque<VertexID> MeshPart::getSimpleCutStripsId() {

  if (simpleCut == NULL)
    buildSimpleCut();
  initSimpleCutFlags();

  /* compute number of strips in simple cut boundary. all triangles but crossroad triangles
     are flagged with 5 */

  // for each simple cut boundary point
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if ((*i) && ((*mesh).point(id).getFlag() == 3))
      for(std::deque<VertexID>::const_iterator n = (*mesh).point(id).getNeighbours().begin();
	  n != (*mesh).point(id).getNeighbours().end(); ++n)
	if ((*mesh).point(*n).getFlag() == 3) { // for each neighbour also in simple cut boundary
	  bool crossroad = true;
	  for(std::deque<TriangleID>::const_iterator t = (*mesh).point(id).getTriangles().begin();
	      t != (*mesh).point(id).getTriangles().end(); ++t)
	    if ((*mesh).triangle(*t).hasPoint(*n) && ((*mesh).triangle(*t).getFlag() == 2)) {
	      // if exists triangle with id and *n point and outside simple cut
	      // other triangle with id and n point is not a crossroad
	      crossroad = false;
	      break;
	    }
	  // if a triangle with id and *n ouside simple cut is not crossroad one
	  if (!crossroad)
	    // find it and set flag to 5
	    for(std::deque<TriangleID>::const_iterator t = (*mesh).point(id).getTriangles().begin();
	      t != (*mesh).point(id).getTriangles().end(); ++t)
	      if ((*mesh).triangle(*t).hasPoint(*n) && ((*mesh).triangle(*t).getFlag() == 1)) {
		(*mesh).triangle(*t).setFlag(5);
	      }
	}

  unsigned int idCrossroad = 6;
  unsigned int idStrip = 7;
  std::deque<VertexID> strips;
  // then compute connected components, using odd values for strips and even for crossroads
  VertexID tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    if (*i) {
      if ((*mesh).triangle(tid).getFlag() == 5) {
	computeCCUsingTriangles(tid, 5, idStrip);
	strips.push_front(tid);
	idStrip += 2;
      }
      else if ((*mesh).triangle(tid).getFlag() == 1) {
	computeCCUsingTriangles(tid, 1, idCrossroad);
	idCrossroad += 2;
      }
    }


  // then remove strips if they are not connected with two edges to a crossroad
  bool stop = false;
  while (!stop) {
    stop = true;
    for(std::deque<VertexID>::iterator i = strips.begin(); i != strips.end(); ++i) {
      int idCr = isSimpleStrip(*i, idStrip);
      idStrip += 2;
      if (idCr >= 6) {
	stop = false;
	computeCCUsingTriangles(*i, CCRuleStripAndCrossRoads(), (*mesh).triangle(*i).getFlag(), idCrossroad);
	idCrossroad += 2;
	strips.erase(i);
	break; // restart
      }
    }
  }

  // then update strips using flag instead of triangle
  for(std::deque<VertexID>::iterator i = strips.begin(); i != strips.end(); ++i)
    *i = (VertexID) (*mesh).triangle(*i).getFlag();

  return strips;
}



unsigned int MeshPart::getGenus() {
  // TODO assert(isOrientable());
  return (2 - getEulerCaracteristic() - getNbBoundaries()) / 2;
}

unsigned int MeshPart::getNbEdges() const {
  VertexID nbB = getBoundaryPoints().size();

  if (nbTriangles != 0)
    return (nbTriangles * 3 - nbB) / 2 + nbB;
  else
    return 0;
}

unsigned int MeshPart::getNbSingleEdges() {
  unsigned int result = 0;

  initInsideFlags();
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if (*i) {
      (*mesh).point(*i).setFlag(2);
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(*i).getNeighbours().begin();
	  nb != (*mesh).point(*i).getNeighbours().end(); ++nb)
	if (((*mesh).point(*nb).getFlag() == 1) && (getNbTrianglesEdge(*i, *nb, 1) == 0))
	  ++result;
    }

  return result;
}

int MeshPart::getEulerCaracteristic() const {
  return nbPoints - getNbEdges() + nbTriangles;
}

int MeshPart::getBettiNumber() {
  return 1 - getEulerCaracteristic() + ((*mesh).hasBoundaryPoint() ? 0 : 1);
}

int MeshPart::getEulerCaracteristic(const std::vector<std::vector<VertexID> > & ) const {
  int result = getEulerCaracteristic();
  // TODO
  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if ((*i) && isBoundaryPoint(id))
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(*i).getNeighbours().begin();
	  nb != (*mesh).point(*i).getNeighbours().end(); ++nb)
	if (isInsideEdge(id, *nb))
	  ++result;

  return result;
}


int MeshPart::isSimpleStrip(TriangleID idT, int fFlag) {
  std::deque<TriangleID> open;
  int flag = (*mesh).triangle(idT).getFlag();
  std::set<int, ltint<int> > crossroads;
  bool isSimpleStrip_l = true;
  open.push_front(idT);
  (*mesh).triangle(idT).setFlag(fFlag);

  while(open.size() != 0) {
    TriangleID id = open.front();
    open.pop_front();

    std::deque<TriangleID> nb = getTNeighbours(id);
    if (nb.size() != 3)
      isSimpleStrip_l = false; // strip connected to boundary cannot be removed
    for(std::deque<TriangleID>::const_iterator idT1 = nb.begin(); idT1 != nb.end(); ++idT1) {
      int f = (*mesh).triangle(*idT1).getFlag();
      if (f != fFlag) {
	if (f == flag) { // triangle idT1 is inside CC
	  open.push_front(*idT1);
	  (*mesh).triangle(*idT1).setFlag(fFlag);
	}
	else if (f >= 6) { // triangle is a crossroad one
	  if (crossroads.find(f) == crossroads.end()) // never seen before: we add it
	    crossroads.insert(f);
	  else // else: strip is not a simple strip
	    isSimpleStrip_l = false;
	}
	else if (f == 0)
	  isSimpleStrip_l = false; // strip connected to boundary cannot be removed
      }
    }


  }


  if ((isSimpleStrip_l) &&  (crossroads.size() > 0))
    return *(crossroads.begin());
  else
    return 0;

}

void MeshPart::buildSimpleCut(VertexID v) {
  std::deque<TriangleID> tList;
  unsigned int step = 0;

  if (nbTriangles == 0) {
      if (v < (*mesh).getNbPoints())
        buildNearestCC((*mesh).point(v));
      else
          buildNearestCC((*mesh).point(0));
  }

  if (simpleCut == NULL)
    delete(simpleCut);
  simpleCut = new MeshPart((*mesh));
  if ((*mesh).getNbPoints() == 0)
    return;


  initInsideFlags();

  for(TriangleID t = 0; t < nbTriangles; ++t)
    if ((*mesh).triangle(t).getFlag() == 1) {
      tList.push_front(t);

      while(tList.size() != 0) {
	++step;
	unsigned int nbEdgesCk = 0;
	int idT = tList.back();
	Triangle & t_l = (*mesh).triangle(idT);
	tList.pop_back();

	t_l.setFlag(1);

	// for each edge, we check for adding triangle
	nbEdgesCk += hasBeenChecked(t_l.getP1(), t_l.getP2(), t_l.getId());
	nbEdgesCk += hasBeenChecked(t_l.getP2(), t_l.getP3(), t_l.getId());
	nbEdgesCk += hasBeenChecked(t_l.getP3(), t_l.getP1(), t_l.getId());

	if ((nbEdgesCk < 2) || (step == 1)) { /* if only 0 or 1 edge is bad,
						 or if it is first step,
						 triangle can be added */
	  (*simpleCut).addTriangle(t_l.getId());
	  if ((*mesh).point(t_l.getP1()).getFlag() != 2)
	    (*simpleCut).addPoint(t_l.getP1());
	  if ((*mesh).point(t_l.getP2()).getFlag() != 2)
	    (*simpleCut).addPoint(t_l.getP2());
	  if ((*mesh).point(t_l.getP3()).getFlag() != 2)
	    (*simpleCut).addPoint(t_l.getP3());

	  t_l.setFlag(2);
	  (*mesh).point(t_l.getP1()).setFlag(2);
	  (*mesh).point(t_l.getP2()).setFlag(2);
	  (*mesh).point(t_l.getP3()).setFlag(2);


	  for(unsigned int e = 0; e < 3; ++e) // no available iterator
	    for(std::deque<TriangleID>::const_iterator i = (*mesh).point(t_l.getVertexId(e)).getTriangles().begin();
		i != (*mesh).point(t_l.getVertexId(e)).getTriangles().end(); ++i) {
	      if (((*mesh).triangle(*i).hasPoint(t_l.getP1()) && (e != 0)) ||
		  ((*mesh).triangle(*i).hasPoint(t_l.getP2()) && (e != 1)) ||
		  ((*mesh).triangle(*i).hasPoint(t_l.getP3()) && (e != 2)))
		if ((*mesh).triangle(*i).getFlag() == 1) { // add neighbour to list
		  tList.push_front(*i);
		  (*mesh).triangle(*i).setFlag(3);
		}
	    }

	}
      }
    }
}

unsigned int MeshPart::hasBeenChecked(VertexID idP1, VertexID idP2, TriangleID idT) {
  // if one of these points has not been seen before, we can add triangle
  if (((*mesh).point(idP1).getFlag() == 1) || ((*mesh).point(idP2).getFlag() == 1))
    return 0;

  // else find other triangle
  for(std::deque<TriangleID>::const_iterator i = (*mesh).point(idP1).getTriangles().begin();
      i != (*mesh).point(idP1).getTriangles().end(); ++i)
    if ((*mesh).triangle(*i).hasPoint(idP2)) {
      if (*i != idT) {
	if ((*mesh).triangle(*i).getFlag() == 2)
	  return 0; // triangle has been seen before: it is ok
	else
	  return 1;
      }
    }

  return 1; // we are in a boundary of the original mesh
}

void MeshPart::stripTriangleFlags(int startValue) {
  // first compute min and max values
  std::set<int, ltint<int> > values;
  std::map<int, int, ltint<int> > invertedValues;
  int v = startValue;

  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    if (*i) values.insert((*mesh).triangle(tid).getFlag());

  for(std::set<int, ltint<int> >::const_iterator i = values.begin(); i != values.end(); ++i)
    invertedValues[*i] = v++;

  tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    if (*i) (*mesh).triangle(tid).setFlag(invertedValues[(*mesh).triangle(tid).getFlag()]);
}


void MeshPart::switchTriangleFlags(int f1, int f2) {
  TriangleID tid = 0;
  for(std::deque<bool>::const_iterator i = triangles.begin(); i != triangles.end(); ++i, ++tid)
    if (*i) {
      int v = (*mesh).triangle(tid).getFlag();
      if (v == f1)
	(*mesh).triangle(tid).setFlag(f2);
      else if (v == f2)
	(*mesh).triangle(tid).setFlag(f1);
    }

}

void MeshPart::buildNearestCC(const Point3D & center) {
  VertexID idCenter = mesh->getNearestPoint(center);

  (*mesh).setPointFlag(0);
  (*mesh).setTriangleFlag(0);


  // compute connected component
  computeCCUsingPoints(idCenter, 0, 1, 0);

  // add points to MeshPart
  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    if ((*i).getFlag() == 1)
      addPoint((*i).getId());

  // add triangles to MeshPart
  for(Mesh::triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    if(((*mesh).point((*t).getP1()).getFlag() == 1) &&
       ((*mesh).point((*t).getP2()).getFlag() == 1) &&
       ((*mesh).point((*t).getP3()).getFlag() == 1))
      addTriangle((*t).getId());
  }

}

double MeshPart::estimateCycleLength(VertexID point) {
  double result = -1;
  computeTDijkstra(point, 1, 1);

  for(std::deque<VertexID>::const_iterator b = (*mesh).point(point).getNeighbours().begin(); b != (*mesh).point(point).getNeighbours().end(); ++b)
    if (!isInsideEdgeTFlag(point, *b, 1))
      if ((result == -1) || (length[*b] < result))
	result = length[*b];

  return result;
}

std::pair<VertexID, double> MeshPart::estimateBetterBasePoint(const std::deque<VertexID> & strip, double maxLength) {

  // first compute half strip
  std::deque<VertexID> halfStrip;
  std::deque<VertexID> stripMHalfStrip;
  std::deque<VertexID>::const_iterator firstPoint = strip.begin();
  bool isFound = false;

  // set flag of points inside the strip
  for(std::deque<VertexID>::const_iterator i = strip.begin(); i != strip.end(); ++i) {
    assert((*mesh).point(*i).getFlag() != 2);
    (*mesh).point(*i).setFlag(2);
  }

  // first find point inside strip and with neighbour inside simple
  // cut
  do {
    assert(firstPoint != strip.end());
    for(std::deque<TriangleID>::const_iterator t = (*mesh).point(*firstPoint).getTriangles().begin();
	t != (*mesh).point(*firstPoint).getTriangles().end(); ++t)
      if ((*mesh).triangle(*t).getFlag() == 1) {
	isFound = true;
	break;
      }

    if (!isFound)
      ++firstPoint;
  } while(!isFound);


  // then compute half strip from this point
  computeHalfStrip(*firstPoint);

  // build half strip deque and reinit flags
  for(std::deque<VertexID>::const_iterator i = strip.begin(); i != strip.end(); ++i) {
    if ((*mesh).point(*i).getFlag() == 4)
      halfStrip.push_front(*i);
    else
      stripMHalfStrip.push_front(*i);
    (*mesh).point(*i).setFlag(1);
  }

  assert(halfStrip.size() != strip.size());
  assert(halfStrip.size() != 0);

  // then compute dijsktra from halfStrip
  computeTDijkstraTarget(halfStrip, stripMHalfStrip, 1, 1, *lengthEdgeMethod, maxLength);

  // then find better base point
  VertexID basePoint = 0;
  double l = std::numeric_limits<double>::max();
  for(std::deque<VertexID>::const_iterator i = strip.begin(); i != strip.end(); ++i)
    if ((length[*i] > 0) && (length[*i] < l)) {
      basePoint = *i;
      l = length[*i];
    }

  return std::pair<VertexID, double>(basePoint, l);
}

void MeshPart::computeHalfStrip(VertexID point) {
  (*mesh).point(point).setFlag(4);

  for(std::deque<VertexID>::const_iterator b = (*mesh).point(point).getNeighbours().begin();
      b != (*mesh).point(point).getNeighbours().end(); ++b)
    if ((*mesh).point(*b).getFlag() == 2) { // if point is a strip point
					 // not seen before
      bool inside = false;
      bool outside = false;
      unsigned int t2Flag = 0;
      Triangle & t1 = (*mesh).findTriangle(point, *b);
      try {
	Triangle & t2 = (*mesh).findOtherTriangle(point, *b, t1.getId());
	t2Flag = t2.getFlag();
      } catch (Exception e) { }
      outside = ((t1.getFlag() == 0) || (t2Flag == 0));
      inside = ((t1.getFlag() == 1) || (t2Flag == 1));

      if (outside && inside)
	computeHalfStrip(*b);
    }
}

std::deque<VertexID> MeshPart::getBoundaryPoints() const {
  std::deque<VertexID> result;

  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if ((*i) && isBoundaryPoint(id))
      result.push_back(id);

  return result;
}


std::deque<VertexID> MeshPart::getShortestPath(const std::deque<VertexID> & set1,
						   const std::deque<VertexID> & set2) {
  VertexID target = (*mesh).getNbPoints();
  double lTarget = std::numeric_limits<double>::max();

  // first compute length to set1 inside meshpart
  initInsideFlags();
  computeDijkstra(set1, 1);

  // find nearest point
  for(std::deque<VertexID>::const_iterator s = set2.begin(); s != set2.end(); ++s)
    if (length[*s] >= 0)
      if (length[*s] < lTarget) {
	target = *s;
	lTarget = length[*s];
      }

  if (target == (*mesh).getNbPoints())
    return std::deque<VertexID>();
  else {
    std::deque<VertexID> result;

    result.push_front(target);
    while(length[result.front()] != 0)
      result.push_front(pred[result.front()]);

    return result;
  }
}

/*const std::deque<VertexID> & MeshPart::getPoints() const {
  return points;
  }

const std::deque<TriangleID> & MeshPart::getTriangles() const {
  return triangles;
  }*/

VertexID MeshPart::getPointHighestLength() const {
  VertexID result = (*mesh).getNbPoints();

  // if length is not computed, return out of range value
  if (length != NULL) {
    double rLength = -1;

    VertexID id = 0;
    for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
      if (*i) {
	double lL = length[id];
	if ((lL >= 0) && (lL > rLength)) {
	  result = id;
	  rLength = lL;
	}
      }
  }
  return result;
}

VertexID MeshPart::getPointMaxEdgeCurvature() const {
  VertexID result = 0;
  double cResult = -1;

  VertexID id = 0;
  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if (*i)
      for(std::deque<VertexID>::const_iterator n = (*mesh).point(id).getNeighbours().begin();
	  n != (*mesh).point(id).getNeighbours().end(); ++n) {
	double c = (*mesh).computeLocalCurvature(id, *n);
	if (c > cResult) {
	  cResult = c;
	  result = id;
	}
      }

  return result;
}

bool MeshPart::hasPoint(VertexID p) const {

  return (p < points.size()) && points[p];
}

std::string MeshPart::getBoxSize() const {
  std::ostringstream result;

  double diff = maxXValue - minXValue;
  double diffy = maxYValue - minYValue;
  double diffz = maxZValue - minZValue;
  if (diffy > diff) diff = diffy;
  if (diffz > diff) diff = diffz;

  result << " Box((" << minXValue << ", " << minYValue << ", " << minZValue << "), ("
	 << maxXValue << ", " << maxYValue << ", " << maxZValue << ")); max: " << diff;

  return result.str();
}

void MeshPart::updateExtrema(VertexID idPoint) {
  Point3D & p = (*mesh).point(idPoint);
  if (p.getX() > maxXValue) maxXValue = p.getX();
  if (p.getX() < minXValue) minXValue = p.getX();
  if (p.getY() > maxYValue) maxYValue = p.getY();
  if (p.getY() < minYValue) minYValue = p.getY();
  if (p.getZ() > maxZValue) maxZValue = p.getZ();
  if (p.getZ() < minZValue) minZValue = p.getZ();
}

void MeshPart::initExtrema() {
  minXValue = std::numeric_limits<double>::max();
  maxXValue = -std::numeric_limits<double>::max();
  minYValue = std::numeric_limits<double>::max();
  maxYValue = -std::numeric_limits<double>::max();
  minZValue = std::numeric_limits<double>::max();
  maxZValue = -std::numeric_limits<double>::max();
}

std::deque<VertexID> MeshPart::getNonRealPoints() const {
 std::deque<VertexID> result;

 VertexID id = 0;
 for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
   if ((*i) && !(*mesh).point(id).getIsRealPoint())
     result.push_front(id);

  return result;
}

bool MeshPart::inSameCompCC(VertexID point) {
  std::deque<TriangleID> tgles;
  MeshPart mPart(*mesh);
  initInsideFlags();

  for(std::deque<TriangleID>::const_iterator t = (*mesh).point(point).getTriangles().begin();
      t != (*mesh).point(point).getTriangles().end(); ++t)
    if ((*mesh).triangle(*t).getFlag() == 0) {
      tgles.push_front(*t);
    }

  if ((*mesh).point(point).getIsBoundary()) {
    // look for the triangles arround the other boundary points

    VertexID tmp;
    VertexID idPpred = point;
    VertexID idP;
    idP = (*mesh).getNextBPoint(point);
    while (idP != point) {
      for(std::deque<TriangleID>::const_iterator t = (*mesh).point(idP).getTriangles().begin();
	  t != (*mesh).point(idP).getTriangles().end(); ++t)
	if ((*mesh).triangle(*t).getFlag() == 0) {
	  tgles.push_front(*t);
	}
      tmp = idPpred;
      idPpred = idP;
      idP = (*mesh).getNextBPoint(idP, tmp);
    }
  }

  if (tgles.size() <= 1) {
    return false;
  }

  computeCompCC(tgles.front(), mPart);

  bool result = true;
  for(std::deque<TriangleID>::const_iterator t = tgles.begin(); t != tgles.end(); ++t)
    if ((*mesh).triangle(*t).getFlag() == 0) {
      result = false;
      break;
    }

  for(Mesh::triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
    if (mPart.isInsideTriangle((*t).getId()))
      (*t).setFlag(0);

  return result;
}

void MeshPart::buildCompCC(const NLoop & paths, TriangleID idT) {
  if ((*mesh).triangle(idT).getFlag() != 0)
    return;

  std::deque<VertexID> open;
  open.push_back(idT);
  (*mesh).triangle(idT).setFlag(1);
  addTriangleAndPoints(idT);

  while(open.size() != 0) {
    TriangleID next = open.front();
    open.pop_front();


    for(unsigned int i = 0; i < 3; ++i)
      try {
	VertexID v1 = (*mesh).triangle(next).getVertexId(i);
	VertexID v2 = (*mesh).triangle(next).getVertexId((i + 1) % 3);
	if (((*mesh).point(v1).getFlag() == 0) ||
	    ((*mesh).point(v2).getFlag() == 0) ||
	    !paths.isInsideEdge(v1, v2)) {
	  Triangle & ti = (*mesh).findOtherTriangle(v1, v2, next);
	  if (ti.getFlag() == 0) {
	    open.push_back(ti.getId());
	    ti.setFlag(1);
	    addTriangleAndPoints(ti.getId());
	  }
	}
      }
      catch (Exception e) {}
  }

}



VertexID MeshPart::getInsidePoint() const {
  VertexID result = 0;

  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++result)
    if (*i)
      return result;

  return (*mesh).getNbPoints();
}

VertexID MeshPart::getBoundaryPoint() const {
  VertexID result = 0;

  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++result)
    if ((*i) && (isBoundaryPoint(result)))
      return result;

  return (*mesh).getNbPoints();
}

VertexID MeshPart::getNonBoundaryPoint() const {
  VertexID result = 0;

  for(std::deque<bool>::const_iterator i = points.begin(); i != points.end(); ++i, ++result)
    if ((*i) && (!isBoundaryPoint(result)))
      return result;

  return (*mesh).getNbPoints();
}


bool MeshPart::isInsideEdge(VertexID id1, VertexID id2) const {
  if (!(*mesh).point(id1).hasNeighbour(id2))
    return false;
  const Triangle & t1 = (*mesh).findTriangle(id1, id2);
  if (!triangles[t1.getId()])
    return false;
  try {
    const Triangle & t2 = (*mesh).findOtherTriangle(id1, id2, t1);
    if (!triangles[t2.getId()])
      return false;

  }
  catch (Exception e) {
    return false;
  }

  return true;
}

bool MeshPart::isBoundaryPoint(VertexID id) const {
  if (!points[id])
    return false;

  if ((*mesh).point(id).getIsBoundary())
    return true;
  else {
    std::deque<TriangleID> & tr = (*mesh).point(id).getTriangles();
    for(std::deque<TriangleID>::const_iterator j = tr.begin(); j != tr.end(); ++j)
      if (!triangles[*j]) // if a triangle connected to *i is outide of MeshPart, *i is a boundary point
	return true;
  }

  return false;
}


std::vector<VertexID> MeshPart::getInsidePoints(const std::vector<VertexID> & ipoints) const {
  std::vector<VertexID> result;

  for(std::vector<VertexID>::const_iterator p = ipoints.begin(); p != ipoints.end(); ++p)
    if (points[*p])
      result.push_back(*p);

  return result;
}
