/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "PlaneSpliter.h"

using namespace Taglut;

void PlaneSpliter::addPoint(VertexID id) {
  assert(id < pointList.size());

  if (children == NULL)
    (*points).push_back(id);
  else {
    const Point2D & point = pointList[id];
    unsigned int idX = (point.get2DX() < mX) ? 0 : 1;
    unsigned int idY = (point.get2DY() < mY) ? 0 : 1;
    (*(children[idX + idY * 2])).addPoint(id);
  }
}

void PlaneSpliter::addTriangle(const Triangle2D & t, TriangleID id) {
  assert(id < triangleList.size());

  if (children == NULL)
    (*triangles).push_back(id);
  else {
    for(unsigned int i = 0; i < 2; ++i)
      for(unsigned int j = 0; j < 2; ++j)
	if ((*(children[i + j * 2])).intersect(t))
	  (*(children[i + j * 2])).addTriangle(t, id);

  }
}

void PlaneSpliter::initPointsAndTriangles() {
  isInitialised(true);
  for(VertexID i = 0; i < pointList.size(); ++i)
    addPoint(i);

  for(TriangleID i = 0; i < triangleList.size(); ++i) {
    addTriangle(Triangle2D(pointList[triangleList[i].getP1()],
			   pointList[triangleList[i].getP2()],
			   pointList[triangleList[i].getP3()]), i);
  }

}

void PlaneSpliter::createStructure(unsigned int depth) {
  mX = minX + (maxX - minX) / 2;
  mY = minY + (maxY - minY) / 2;

  if (depth <= 0) {
    children = NULL;
    points = new std::vector<VertexID>;
    triangles = new std::vector<TriangleID>;
  }
  else {
    points = NULL;
    triangles = NULL;
    children = new PlaneSpliter *[4];

    if (children == NULL)
      throw Exception(std::string("Error during memory allocation."));

    for(unsigned int i = 0; i < 2; ++i)
      for(unsigned int j = 0; j < 2; ++j)
	children[i + j * 2] = new PlaneSpliter(depth - 1, pointList, triangleList,
					       minX + i * (maxX - minX) / 2,
					       minY + j * (maxY - minY) / 2,
					       minX + (i + 1) * (maxX - minX) / 2,
					       minY + (j + 1) * (maxY - minY) / 2);

  }
}

void PlaneSpliter::computeMinMax() {
  minX = minY = std::numeric_limits<double>::max();
  maxX = maxY = -std::numeric_limits<double>::max();
  for(std::deque<Point2D>::const_iterator i = pointList.begin(); i != pointList.end(); ++i) {
    if ((*i).get2DX() < minX) minX = (*i).get2DX();
    if ((*i).get2DY() < minY) minY = (*i).get2DY();
    if ((*i).get2DX() > maxX) maxX = (*i).get2DX();
    if ((*i).get2DY() > maxY) maxY = (*i).get2DY();
  }
}

void PlaneSpliter::isInitialised(bool value) {
  initialised = value;
  if (children != NULL)
    for(unsigned int i = 0; i < 4; ++i)
      (*(children[i])).isInitialised(value);
}

PlaneSpliter::PlaneSpliter(const Mapping2D3D & mapping) : pointList(mapping), triangleList(mapping.getTriangles()), initialised(false) {
  computeMinMax();
  createStructure((unsigned int)(pow(pointList.size(), 1./3) / 5));
}

PlaneSpliter::PlaneSpliter(unsigned int depth_l, const std::deque<Point2D> & points_l, const std::vector<Triangle> & triangles_l,
			   double minX_l, double minY_l, double maxX_l, double maxY_l) :
  Box2D(minX_l, maxX_l, minY_l, maxY_l),
  pointList(points_l), triangleList(triangles_l), initialised(false) {
  createStructure(depth_l);
}


PlaneSpliter::~PlaneSpliter() {
  if (points != NULL)
    delete(points);
  if (triangles != NULL)
    delete(triangles);

  if (children != NULL) {
    for(unsigned int i = 0; i < 4; ++i)
      delete(children[i]);
    delete [] children;
  }

}


std::vector<TriangleID> PlaneSpliter::findTrianglesInRay(const Coord2D & point) {
  if (!initialised)
    initPointsAndTriangles();


  if (children == NULL) {
    std::vector<TriangleID> result;
    for(std::vector<TriangleID>::const_iterator i = (*triangles).begin(); i != (*triangles).end(); ++i) {
      Triangle2D t(pointList[triangleList[*i].getP1()],
		   pointList[triangleList[*i].getP2()],
		   pointList[triangleList[*i].getP3()]);
      if (t.isInside(point))
	result.push_back(*i);
    }
    return result;
  }
  else {
    unsigned int idX = (point.get2DX() < mX) ? 0 : 1;
    unsigned int idY = (point.get2DX() < mY) ? 0 : 1;
    return (*(children[idX + idY * 2])).findTrianglesInRay(point);
  }
}

unsigned int PlaneSpliter::getNbTrianglesInRay(const Coord2D & point) {
  if (!initialised)
    initPointsAndTriangles();


  if (children == NULL) {
    unsigned int result = 0;
    for(std::vector<TriangleID>::const_iterator i = (*triangles).begin(); i != (*triangles).end(); ++i) {
      Triangle2D t(pointList[triangleList[*i].getP1()],
		   pointList[triangleList[*i].getP2()],
		   pointList[triangleList[*i].getP3()]);
      if (t.isInside(point))
	++result;
    }
    return result;
  }
  else {
    unsigned int idX = (point.get2DX() < mX) ? 0 : 1;
    unsigned int idY = (point.get2DY() < mY) ? 0 : 1;
    return (*(children[idX + idY * 2])).getNbTrianglesInRay(point);
  }
}

