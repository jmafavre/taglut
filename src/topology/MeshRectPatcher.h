/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_RECT_PATCHER
#define MESH_RECT_PATCHER

#include <iostream>
#include "MeshPathes.h"
#include "Mesh.h"
#include "Mapping2D3D.h"
#include "Polygon.h"
#include "IndentManager.h"

namespace Taglut {
  /**
     @class MeshRectPatcher
     Mesh manipulator used to compute rectangular patches
  */
  class MeshRectPatcher : public MeshPathes {
  private:

    /**
       Current mapping
    */
    Mapping2D3D & mapping;

    /**
       Large boundary edge
    */
    class BEdge;

    /**
       List of open edges
    */
    std::list<BEdge> bEdgeList;

    /**
       List of found polygons
    */
    std::deque<Polygon> polygonList;

    /**
       Limit ratio between the area of the surface and the area of
       the polygon
    */
    float ratioArea;

    /**
       Given a multi point on the boundary, build recursively
       the list of multi edges from this point in the boundary.
    */
    void buildEdgesFromPoint(VertexID mp);

    /**
       Given an edge and a path (other side of a "rectangle"), build a polygon
    */
    Polygon computePolygonFromEdge(const BEdge & bEdge, const std::deque<VertexID> & path);

    /**
       Remove e and its neighbours edges from the list of edges, and add the new one defined
       by path.
       The extremity points of the added path are updated using ePoints
       @return The added edge
    */
    BEdge & updateEdgeList(std::list<BEdge>::iterator & e, const std::deque<VertexID> & path, const std::deque<VertexID> & ePoints);

    /**
       Given an edge, compute the pred edge
    */
    const BEdge & findPredEdge(const BEdge & bEdge) const;

    /**
       Given an edge, compute the next edge
    */
    const BEdge & findNextEdge(const BEdge & bEdge) const;

    /**
       Given an edge, compute the pred edge
    */
    std::list<MeshRectPatcher::BEdge>::iterator findPredEdgeIt(const BEdge & bEdge);

    /**
       Given an edge, compute the next edge
    */
    std::list<MeshRectPatcher::BEdge>::iterator findNextEdgeIt(const BEdge & bEdge);

    /**
       Get the open point in cMesh (with same location of the given point)
    */
    VertexID getOpenPoint(VertexID point);

    /**
       Update flags of points in cMesh
    */
    void updatePointFlags();

    /**
       Compute a point with same location of the given point, but with flag = 0 (i.e.
       in the part of the graph that will be cut next), starting from the end (first watching
       for recently added points)
    */
    VertexID computeNewIdInside(VertexID id) const;

    /**
       Set flags of boundary neighbours = 1 if idP point has other neighbours not in boundary
    */
    void setFlagOfBoundaryNeighbours(VertexID idP);

    /**
       Flip single triangles on corners
    */
    void flipTrianglesOnCorners();

    /**
       Flip single triangles on corners of added edges
    */
    void flipTrianglesOnCorners(const BEdge & aEdge);

    /**
       flip triangles on the given corner
    */
    void flipTrianglesOnCorner(VertexID cPointId);

  public:

    /**
       Default constructor
    */
    inline MeshRectPatcher(Mapping2D3D & mapping_t, float ratioArea_t = 0.5) : MeshPathes(mapping_t.getMesh()), mapping(mapping_t), ratioArea(ratioArea_t) { }

    /**
       Copy constructor
    */
    MeshRectPatcher(const MeshRectPatcher & mRect);

    /**
       Copy constructor using another mesh
    */
    MeshRectPatcher(const MeshRectPatcher & mRect, Mesh & mesh);

    /**
       Destructor
    */
    virtual inline ~MeshRectPatcher() { }

    /**
       Build a cut mesh using associated the mapping
    */
    void buildRectangleCut(Taglut::IndentManager & iManager, bool verbose = false, unsigned int maxCut = 0);

    /**
       Clone current object
    */
    MeshPathes * clone() const;

    /**
       Return a clone of the current object (using the given mesh rather than original one
    */
    MeshPathes * clone(Mesh & mesh) const;

    /**
       Set the ratio of areas
    */
    inline void setRatioArea(float ratioArea_t) { (*this).ratioArea = ratioArea_t; }
  };


  /**
     @class MeshRectPatcher::BEdge
     Boundary large edge
  */
  class MeshRectPatcher::BEdge {
  private:
    /**
       mapping
    */
    const Mapping2D3D & mapping;

    /**
       Path from first to last point
    */
    std::deque<VertexID> path;

    /**
       Angle sum used for ordering a list of edges
    */
    double angleSum;

    /**
       First point (id in the original mesh)
    */
    VertexID firstO;

    /**
       Last point (id in the original mesh)
    */
    VertexID lastO;

    /**
       Pred multi point in boundary (original id)
    */
    VertexID predOPoint;

    /**
       Next multi point in boundary (original id)
    */
    VertexID nextOPoint;

    /**
       Pred multi point in boundary
    */
    VertexID predPoint;

    /**
       Next multi point in boundary
    */
    VertexID nextPoint;

    /**
       True if edge is in direct order on the neighbour
    */
    bool isDirect;

    /**
       Compute angle sum
    */
    void computeAngleSum();

  public:
    /**
       Default constructor
    */
    inline BEdge(const Mapping2D3D & mapping_t, const std::deque<VertexID> & path_t,
		 VertexID firstO_t, VertexID lastO_t, bool isDirect_t) : mapping(mapping_t), path(path_t), angleSum(42.0),
								   firstO(firstO_t), lastO(lastO_t),
								   predOPoint(mapping_t.size()), nextOPoint(mapping_t.size()),
								   predPoint(mapping_t.size()), nextPoint(mapping_t.size()), isDirect(isDirect_t) { }

    /**
       Copy constructor
    */
    inline BEdge(const BEdge & e) : mapping(e.mapping), path(e.path), angleSum(e.angleSum), firstO(e.firstO), lastO(e.lastO),
				    predOPoint(e.predOPoint), nextOPoint(e.nextOPoint),
				    predPoint(e.predPoint), nextPoint(e.nextPoint), isDirect(e.isDirect) { }

    /**
       Angle sum accessor
    */
    inline double getAngleSum() const {
      return angleSum;
    }

    /**
       Set first point
    */
    inline BEdge & setFirstPoint(VertexID p) { path.front() = p; return *this; }

    /**
       Set last point
    */
    inline BEdge & setLastPoint(VertexID p) { path.back() = p; return *this; }
    /**
       Set next multi point
    */
    inline BEdge & setNextPoint(VertexID p) { nextPoint = p; return *this; }

    /**
       Set pred multi point
    */
    inline BEdge & setPredPoint(VertexID p) { predPoint = p; return *this; }

    /**
       Set next multi point
    */
    inline BEdge & setNextPoints(VertexID p) { setNextPoint(p); setNextOPoint(p); return *this; }

    /**
       Set pred multi point
    */
    inline BEdge & setPredPoints(VertexID p) { setPredPoint(p); setPredOPoint(p); return *this; }

    /**
       Set next multi point (id in the original mesh)
    */
    inline BEdge & setNextOPoint(VertexID p) { assert(p < mapping.size()); nextOPoint = p; if (predOPoint != mapping.size()) computeAngleSum(); return *this; }

    /**
       Set pred multi point (id in the original mesh)
    */
    inline BEdge & setPredOPoint(VertexID p) { assert(p < mapping.size()); predOPoint = p; if (nextOPoint != mapping.size()) computeAngleSum(); return *this; }

    /**
       Get next multi point
    */
    inline VertexID getNextPoint() const { return nextPoint; }

    /**
       Get pred multi point
    */
    inline VertexID getPredPoint() const { return predPoint; }

    /**
       Get next multi point (id in the original mesh)
    */
    inline VertexID getNextOPoint() const { return nextOPoint; }

    /**
       Get pred multi point (id in the original mesh)
    */
    inline VertexID getPredOPoint() const { return predOPoint; }

    /**
       Get "is direct" property
    */
    inline bool getIsDirect() const { return isDirect; }

    /**
       Accessor for the first vertex (id in the original mesh)
    */
    inline VertexID getFirstOriginalPoint() const { return firstO; }

    /**
       Accessor for the second vertex (id in the original mesh)
    */
    inline VertexID getLastOriginalPoint() const { return lastO; }

    /**
       Accessor for the first vertex
    */
    inline VertexID getFirstPoint() const { return path.front(); }

    /**
       Accessor for the second vertex
    */
    inline VertexID getLastPoint() const { return path.back(); }

    /**
       Comparison operator
    */
    inline bool operator<(const BEdge & b) const {
      if (angleSum == b.getAngleSum())
	return getFirstOriginalPoint() < b.getFirstOriginalPoint();
      else
	return angleSum < b.getAngleSum();
    }

    /**
       Return the associated path
    */
    const std::deque<VertexID> & getPath() const {
      return path;
    }

    /**
       Stream operator for MultiTriangle
    */
    template <typename T, typename S>
    friend std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const BEdge & t);

  };

  /**
     Stream operator for MultiTriangle
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const MeshRectPatcher::BEdge & t) {
    f << "BEdge(firstOriginal: " << t.getFirstOriginalPoint() << ", lastOriginal: " << t.getLastOriginalPoint() << ", nextPoint: "
      << t.getNextPoint() << ", predPoint: " << t.getPredPoint() << ", nextOPoint: "
      << t.getNextOPoint() << ", predOPoint: " << t.getPredOPoint() << ", angleSum: " << t.angleSum << ", path:";
    for (std::deque<VertexID>::const_iterator i = t.path.begin(); i != t.path.end(); ++i)
      f << " " << *i;
    f  << ")";

    return f;
  }

}

#endif
