/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshCut.h"
#include "NLoop.h"
#include "SimulatedAnnealingNLoop.h"
#include "Display3D.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename  = NULL;
static char*  filename_output  = NULL;
static int    sizex = 300;
static int    sizey = 300;
static char*  objectName = NULL;
static int    display = 0;
static int    help = 0;
static int    opengl = 0;
static int    first = -1;
static int    second = -1;
static int    simulatedAnnealing = 0;
static double simTempInit = 20.;
static int    simStepSize = 50;
static double simTempCoeff = 0.9;
static double simAcceptThreshold = 0.05;
static double simTempMin = 1e-3;
static int    arity = 3;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "first-basepoint", '1', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &first, 0, "ID of the first basepoint", NULL},
  { "second-basepoint", '2', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &second, 0, "ID of the second basepoint", NULL},
  { "arity", 'a', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &arity, 0, "Number of paths of the NLoop", NULL},
  { "simulated-annealing", 's', POPT_ARG_NONE, &simulatedAnnealing, 0, "Adjust the location using simulated annealing", NULL},
  { "temp-init", 0, POPT_ARG_DOUBLE, &simTempInit, 0, "Initial temperature", NULL},
  { "step-size", 0, POPT_ARG_INT, &simStepSize, 0, "Step size between two temperature modifications", NULL},
  { "temp-coeff", 0, POPT_ARG_DOUBLE, &simTempCoeff, 0, "Coefficient of the geometric progression", NULL},
  { "accept-threshold", 0, POPT_ARG_DOUBLE, &simAcceptThreshold, 0, "Value for acceptation", NULL},
  { "temp-min", 0, POPT_ARG_DOUBLE, &simTempMin, 0, "Minimal temperature value", NULL},
  { "x", 'x', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizex, 0, "3d window size (x)", NULL},
  { "y", 'y', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizey, 0, "3d window size (y)", NULL},
  { "opengl", 'g', POPT_ARG_NONE, &opengl, 0, "display using opengl", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("meshNLoopComputation", argc, argv, options, 0);


  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("NLoop computation", "Compute NLoops on a given mesh.");
    hMsg << "Input: mesh to display (available format: VRML, ...)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if ((filename == NULL) && (!display)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file or the display mode." << endl;
    return 1;
  }

  if ((sizex <= 0) || (sizey <= 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: 3d window size must be positive." << endl;
    return 1;
  }



  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }

  if ((first >= (long int)mesh.getNbPoints()) || (second >= (long int)mesh.getNbPoints())) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Wrong id (max: " << mesh.getNbPoints() << ")" << endl;
    return 1;
  }


  try {


    cout << "Processing..." << endl;
    MeshCut mCut(mesh);

    cout << "Computing NLoop..." << endl;
    NLoop nloop;
    if (simulatedAnnealing != 0) {
      SimulatedAnnealingNLoop simAnnealing(mesh, NLoopEvaluatorLength(), simTempInit, simStepSize, simTempCoeff, simAcceptThreshold, simTempMin);
      if (first < 0)
	first = mesh.getNbPoints();
      if (second < 0)
	second =  mesh.getNbPoints();
      nloop = simAnnealing.approximateShortestValidNLoopArityFixed(arity, first, second);
    }
    else {
      if (first == -1) {
	cout << "Automatic selection of the first point is not defined with this method" << endl;
	return -1;
      }

      if (second == -1) {
	cout << "Computing the second basepoint from " << first << "..." << std::endl;
	std::deque<VertexID> loop = mCut.getShortestNonTrivialCycleFromPoint(first);

	second = mesh.getMiddlePoint(loop);
	cout << " Selected: " << second << std::endl;

	nloop = mCut.getShortestValidNLoop(first, second, arity);
      }
    }

    std::cout << "Number of paths: " << nloop.getNbPaths() << ", longueur: " << mesh.getLength(nloop) << std::endl;
    std::cout << "NLoop: " << mesh.getStringFromNLoop(nloop);
    mCut.addNLoop(nloop);
    Mesh m2 = mCut.cutMesh();

    if (display) {
      Display3D::displayMesh(sizex, sizey, "Display", m2, False, True, opengl);
    }
    if (filename_output != NULL) {
      std::cout << "Saving file (" << filename_output << ")" << std::endl;
      m2.save(filename_output);
    }

  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
