/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "Display3D.h"
#include "MeshPart.h"
#include "Point3D.h"
#include "MeshSmoother.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    display = 0;
static int    help = 0;
static int    edges = 0;
static int    number = 1;
static double lambda = 0.5;
static int    openGL = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "lambda", 'l', POPT_ARG_DOUBLE, &lambda, 0, "Lambda used for smoothing", NULL},
  { "number-step", 's', POPT_ARG_INT, &number, 0, "Number of smooth step", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "opengl", 'g', POPT_ARG_NONE, &openGL, 0, "display edges", NULL},
  { "edges", 'e', POPT_ARG_NONE, &edges, 0, "display edges", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("meshsmoother", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh smoother", "Smooth the input.");
    hMsg << "Input: mesh to cut (available format: VRML)";
    hMsg << "Output: display cut mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
  }

  if (number <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the number of smooth step cannot be negative or null." << endl;
    return 1;
  }

  try {
    /* build mesh */
    Mesh mesh(filename_input, objectName);

    /* process mesh */
    cout << "Processing..." << endl;
    for(unsigned int i = 0; i < (unsigned int)number; ++i)
      MeshSmoother::smoothLaplacian(mesh, lambda);

    /* output: display and/or save mesh */
    if (display != 0)
      Display3D::displayMesh(256, 256, "3D Visualisation", mesh, edges != 0, true, openGL != 0);

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      mesh.save(string(filename_output), string(objectName));
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
