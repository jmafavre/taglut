/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MAPPING_LIST
#define MAPPING_LIST

#include "Mapping2D3D.h"
#include "FileManipulator.h"

namespace Taglut {
  class MeshList;


  /**
     @class Mapping2D3DList

     @author Jean-Marie Favreau
     @brief List of mapping
  */
  class Mapping2D3DList : public std::deque<Mapping2D3D>, public FileManipulator {
  private:
    MeshList & meshes;

    void addFormats();
  public:

    /**
       Constructor
    */
    Mapping2D3DList(MeshList & meshes);

    /**
       Return a string describing the cluster
    */
    std::string getClusterInfos() const;


    /**
       Load Mapping2D3DList from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Save Mapping2D3DList from file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;


    /**
       Save Mapping2D3D in file
    */
    void saveMAP(const std::string & filename) const;

    /**
       Load Mapping2D3D from file
    */
    void loadMAP(const std::string & filename);

    /**
       Import from a circleset file
    */
    void loadCST(const std::string & filename);

    /**
       get number of triangles
    */
    TriangleID getNbTriangles() const;

    /**
       get number of points
    */
    VertexID getNbPoints() const;

    /**
       Given a circle set id and a local circle id, compute global
       id
    */
    VertexID getGlobalId(unsigned int csetId, unsigned int cId) const;
  };
}
#endif
