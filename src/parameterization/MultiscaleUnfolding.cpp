/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MultiscaleUnfolding.h"
#include "MeshPrune.h"
#include "FileTools.h"

using namespace Taglut;

MultiscaleUnfolding::MultiscaleUnfolding(bool readjust_l, float epsilon_l, unsigned int maxIter_l,
					 const std::string & logFile_l,
					 const IndentManager & indentManager_l) : readjust(readjust_l), epsilon(epsilon_l), maxIter(maxIter_l),
									  logFile(logFile_l), indentManager(indentManager_l) {
  initFirst = true;
}

MultiscaleUnfolding::MultiscaleUnfolding(bool readjust_l, float epsilon_l, unsigned int maxIter_l,
					 bool initFirst_l,
					 const std::string & logFile_l,
					 const IndentManager & indentManager_l) : readjust(readjust_l), epsilon(epsilon_l), maxIter(maxIter_l),
										  initFirst(initFirst_l),
										  logFile(logFile_l), indentManager(indentManager_l) {
}


int MultiscaleUnfolding::processUnfoldingInternal(UnfoldingMethod & curMapping, unsigned int currDepth) {
  std::string realLogFile = logFile;
  if ((logFile != "") && (depthMax != 0))
    realLogFile = FileTools::computeFileName(logFile, depthMax + 1, currDepth);

  if (currDepth == 0) {
    if (depthMax != 1)
      std::cout << indentManager << " Unfolding, depth #" << currDepth << " (number of points: " << curMapping.size() << ")" << std::endl;
    else
        std::cout << indentManager << " Unfolding, 1st step" << std::endl;

    int result = curMapping.processUnfolding(epsilon, maxIter, initFirst, realLogFile, indentManager);
    curMapping.compute2DLocation();

    std::cout << indentManager << " Iteration number: " << result << std::endl;

    return result;
  }
  else {
    Mesh smallMesh(curMapping.getMesh());
    MeshPrune pMesh(smallMesh);
    pMesh.pruneMesh();
    std::cout << indentManager << " Multiscale, depth #" << currDepth << ". Number of points: " << curMapping.size() << "->" << smallMesh.getNbPoints() << std::endl;
    Mapping2D3D mSmall(smallMesh);
    UnfoldingMethod * mp = curMapping.clone(mSmall);

    // multiscale process
    int nbIter = processUnfoldingInternal(*mp, currDepth - 1);



    // compute 2D location's points of the major mapping using the minor mapping
    curMapping.getMapping().compute2DLocation(pMesh, mSmall);

    if (readjust) {
      std::cout << indentManager << " Unfolding, depth #" << currDepth << " (number of points: " << curMapping.size() << ")" << std::endl;
      curMapping.initDataUsing2D();
      int nbIterAdjust = curMapping.processUnfolding(epsilon, maxIter, false, realLogFile, indentManager);
      curMapping.compute2DLocation();
      std::cout << indentManager << " Iteration number: " << nbIterAdjust << std::endl;
      nbIter += nbIterAdjust;
    }

    return nbIter;
  }

}

int MultiscaleUnfolding::processUnfolding(UnfoldingMethod & curMapping, unsigned int depth) {
  clock_t tstart = clock();

  depthMax = depth;

  if (depthMax != 0)
    indentManager.incDepth();


  int result = processUnfoldingInternal(curMapping, depth);

  if (depthMax != 0) {
    indentManager.decrDepth();
    clock_t tend = clock();
    std::cout << indentManager << " Global unfolding duration: " << ((double)(tend - tstart)) / (CLOCKS_PER_SEC) << " seconds" << std::endl;
  }

  return result;
}
