/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CNR-GE IMATI
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef FLOATER_METHOD
#define FLOATER_METHOD

#include "UnfoldingMethod.h"
#include "Mapping2D3D.h"
#include "BarycentricMapping.h"

namespace Taglut {

  class FloaterMethod : public UnfoldingMethod {
  public:
    /** default constructor */
    FloaterMethod(Mapping2D3D & mapping_t) : UnfoldingMethod(mapping_t) { }

    /** copy constructor */
    FloaterMethod(const FloaterMethod & uMethod) : UnfoldingMethod(uMethod) { }

    /** destructor */
    ~FloaterMethod() { }

    /**
       Compute 2D location
    */
    inline virtual void compute2DLocation(const Taglut::IndentManager &, bool = true) {
      /* nothing to do */
    }


    /**
       Process unfolding
       Return number of iteration or -1 if fails
    */
    virtual int processUnfolding(float, unsigned int, bool, const std::string &,
				const Taglut::IndentManager & ) {
      DiscParameterizationFloater mParam;
      mapping = mParam.parameterize(mapping.getMesh());
      return 1;
    }


    /**
       clone instruction
    */
    inline virtual UnfoldingMethod * clone(Mapping2D3D & mapping_t) {
      return new FloaterMethod(mapping_t);
    }

    /**
       Init unfolding data using 2d location of points
    */
    inline virtual void initDataUsing2D() { /* nothing to do*/ }

  };


  class TutteMethod : public UnfoldingMethod {
  public:
    /** default constructor */
    TutteMethod(Mapping2D3D & mapping_t) : UnfoldingMethod(mapping_t) { }

    /** copy constructor */
    TutteMethod(const FloaterMethod & uMethod) : UnfoldingMethod(uMethod) { }

    /** destructor */
    ~TutteMethod() { }

    /**
       Compute 2D location
    */
    inline virtual void compute2DLocation(const Taglut::IndentManager &, bool = true) {
      /* nothing to do */
    }


    /**
       Process unfolding
       Return number of iteration or -1 if fails
    */
    virtual int processUnfolding(float, unsigned int, bool, const std::string &,
				const Taglut::IndentManager & ) {
      DiscParameterizationTutte mParam;
      mapping = mParam.parameterize(mapping.getMesh());
      return 1;
    }


    /**
       clone instruction
    */
    inline virtual UnfoldingMethod * clone(Mapping2D3D & mapping_t) {
      return new TutteMethod(mapping_t);
    }

    /**
       Init unfolding data using 2d location of points
    */
    inline virtual void initDataUsing2D() { /* nothing to do*/ }

  };


  class QuadFloaterMethod : public UnfoldingMethod {
  public:
    /** default constructor */
    QuadFloaterMethod(Mapping2D3D & mapping_t) : UnfoldingMethod(mapping_t) { }

    /** copy constructor */
    QuadFloaterMethod(const FloaterMethod & uMethod) : UnfoldingMethod(uMethod) { }

    /** destructor */
    ~QuadFloaterMethod() { }

    /**
       Compute 2D location
    */
    inline virtual void compute2DLocation(const Taglut::IndentManager &, bool = true) {
      /* nothing to do */
    }


    /**
       Process unfolding
       Return number of iteration or -1 if fails
    */
    virtual int processUnfolding(float, unsigned int, bool, const std::string &,
				const Taglut::IndentManager & ) {
      QuadParameterizationFloater mParam;
      mapping = mParam.parameterize(mapping.getMesh());
      return 1;
    }


    /**
       clone instruction
    */
    inline virtual UnfoldingMethod * clone(Mapping2D3D & mapping_t) {
      return new FloaterMethod(mapping_t);
    }

    /**
       Init unfolding data using 2d location of points
    */
    inline virtual void initDataUsing2D() { /* nothing to do*/ }

  };
}

#endif
