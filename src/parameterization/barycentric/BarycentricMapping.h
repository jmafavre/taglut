/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010-2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                 CNRS / Univ. Blaise Pascal / IMATI-GE CNR / Univ. d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef BARYCENTRIC_MAPPING
#define BARYCENTRIC_MAPPING

#include <vector>
#include <map>
#include "OpenNLSolver.h"
#include "Point3D.h"
#include "Mapping2D3D.h"

namespace Taglut {
  class Mesh;


  class BorderParameterization {

  protected:

    virtual void parameterizeBorder3D(Mesh & mesh, Mesh & resultMesh) = 0;

    virtual void parameterizeBorder(Mapping2D3D & mapping) = 0;

  public:
    BorderParameterization() { }

    virtual ~BorderParameterization() { }
  };

  class BorderParameterization2D {
  protected:
    virtual std::map<VertexID, Coord2D> getBorder2D(Mesh & mesh) = 0;

    void parameterizeBorder3D(Mesh & mesh, Mesh & resultMesh) {
      std::map<VertexID, Coord2D> r = getBorder2D(mesh);
      for(std::map<VertexID, Coord2D>::const_iterator p = r.begin(); p != r.end(); ++p) {
	resultMesh.point((*p).first).setX((*p).second.get2DX());
	resultMesh.point((*p).first).setY((*p).second.get2DY());
	resultMesh.point((*p).first).setZ(0.);
      }
    }

    void parameterizeBorder(Mapping2D3D & mapping) {
      std::map<VertexID, Coord2D> r = getBorder2D(mapping.getMesh());
      for(std::map<VertexID, Coord2D>::const_iterator p = r.begin(); p != r.end(); ++p) {
	mapping[(*p).first].set2DCoords((*p).second);
      }
    }
  public:
    BorderParameterization2D() { }
    virtual ~BorderParameterization2D() { }
  };

  class CircleBorderParameterization : public BorderParameterization2D {
  protected:

    virtual std::map<VertexID, Coord2D> getBorder2D(Mesh & mesh);
  public:
    CircleBorderParameterization() { }

    virtual ~CircleBorderParameterization() { }
  };

  class QuadBorderParameterization : public BorderParameterization2D {
  private:
    VertexID corners[4];

    /** get length of the 4 sides, assuming that bd is the path along the boundary of the given mesh */
    std::vector<double> getLengthSide(const std::vector<VertexID> & bd, const Mesh & mesh) const;

    /** return true if the given vertex is a known corner */
    inline bool isCorner(VertexID v) const {
      return (v == corners[0] ||
	      v == corners[1] ||
	      v == corners[2] ||
	      v == corners[3]);
    }

  protected:
    virtual std::map<VertexID, Coord2D> getBorder2D(Mesh & mesh);

  public:
    QuadBorderParameterization() {
      corners[0] = corners[1] = corners[2] = corners[3] = 0;
    }
    virtual ~QuadBorderParameterization() { }

    inline void setCorners(VertexID c1, VertexID c2, VertexID c3, VertexID c4) {
      corners[0] = c1;
      corners[1] = c2;
      corners[2] = c3;
      corners[3] = c4;
    }
    inline void resetCorners() {
      corners[0] = corners[1] = corners[2] = corners[3] = 0;
    }

    /** select the 4 corners automatically using an angle approach:
	difference between the two angles defined by the adjacent
	edges, and the normals on the two corresponding triangles */
    void setCornersAuto(const Mesh & mesh);
  };


  /**
     @class BarycentricMapping
     @author Jean-Marie Favreau (LIMOS / Blaise Pascal Univ. / IMATI-GE / Univ. d'Auvergne)
     @brief Generic class that contains a solver for barycentric unfoldings
  */
  template <class BParam>
  class BarycentricMapping : public BParam {

  protected:

    /** will be implemented by the barycentric methods.
	Initialize the solution vector
    */
    virtual void initSolutionVector(OpenNLSolver & solver, unsigned char i) = 0;

    /** will be implemented by the barycentric methods.
	fill the matrix according to the constraints defined by the neighborhoods
    */
    virtual void fillMatrix(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mesh & resultMesh) = 0;

    /** will be implemented by the barycentric methods.
	fill the vector according to the constraints defined by the neighborhoods
    */
    virtual void fillVector(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mesh & resultMesh, unsigned int id) = 0;

    /** will be implemented by the barycentric methods.
	fill the matrix according to the constraints defined by the neighborhoods
    */
    virtual void fillMatrix(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mapping2D3D & result) = 0;

    /** will be implemented by the barycentric methods.
	fill the vector according to the constraints defined by the neighborhoods
    */
    virtual void fillVector(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mapping2D3D & result, unsigned int id) = 0;


  public:
    /** default constructor */
    BarycentricMapping() {

    }

    /** destructor */
    virtual ~BarycentricMapping() {

    }

    /** given a mesh, compute a parameterization in R^3 */
    Mesh parameterize3D(Mesh & mesh, bool x = true, bool y = true, bool z = true);

    /** given a mesh, compute a parameterization in R^2 */
    Mapping2D3D parameterize(Mesh & mesh);

  };

  /**
     @class MeanValuesParameterization
     @author Jean-Marie Favreau (LIMOS / Blaise Pascal Univ. / IMATI-GE)
     @brief Floater Mean Value Coordinates parameterization (Floater 03)
  */
  template <class BParam>
  class TutteParameterization : public BarycentricMapping<BParam> {
  protected:

    virtual void initSolutionVector(OpenNLSolver & solver, unsigned char) {
      for(VertexID v = 0; v != solver.getSize(); ++v)
	solver.setVariable(v, 0.);
    }

    virtual void fillMatrix(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mesh & resultMesh);

    virtual void fillVector(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mesh & resultMesh, unsigned int id);

    virtual void fillMatrix(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mapping2D3D & result);

    virtual void fillVector(OpenNLSolver & solver, const std::vector<VertexID> & ids,
			    const Mesh & mesh, const Mapping2D3D & result, unsigned int id);

    /** compute the weights of the barycentric system */
    virtual const std::vector<double> computeWeights(const Mesh & mesh, const Point3D & point);

  public:
    // default constructor
    TutteParameterization() {

    }

    // destructor
    virtual ~TutteParameterization() {

    }

  };

  /**
     @class MeanValuesParameterization
     @author Jean-Marie Favreau (LIMOS / Blaise Pascal Univ. / IMATI-GE)
     @brief Floater Mean Value Coordinates parameterization (Floater 03)
  */
  template <class BParam>
  class MeanValuesParameterization : public TutteParameterization<BParam> {
  protected:

    // Return tangent of half the angle between vectors b-a and c-a
    // without using trig functions.
    // Use fact that tan(alpha/2) = (1-cos(alpha)) / sin(alpha).
    // and use scalar and dot products to get cos(alpha) and sin(alpha).
    //  M.F. Apr. 2002.
    // Idea from the Floater implementation of the Mean Value approach
    static double tanThetaOverTwo(const Coord3D & a, const Coord3D & b, const Coord3D & c);

    /** compute the weights of the barycentric system */
    virtual const std::vector<double> computeWeights(const Mesh & mesh, const Point3D & point);

  public:
    // default constructor
    MeanValuesParameterization() {

    }
  };


  template <class BParam>
  Mesh BarycentricMapping<BParam>::parameterize3D(Mesh & mesh, bool x, bool y, bool z) {
    Mesh result(mesh);

    BParam::parameterizeBorder3D(mesh, result);


    VertexID nbPoints = mesh.getNbPoints();
    VertexID nbPointsInside = nbPoints - mesh.getNbBoundaryPoints();

    if (nbPointsInside == 0)
      return result;

    std::vector<VertexID> ids(nbPoints, nbPoints);

    OpenNLSolver solver(nbPointsInside);


    initSolutionVector(solver, 0);

    // first compute the map between the two index systems
    VertexID i = 0;
    std::vector<VertexID>::iterator id = ids.begin();
    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p, ++id)
      if (!((*p).getIsBoundary())) {
	*id = i;
	++i;
      }

    // then create the matrix
    fillMatrix(solver, ids, mesh, result);

    // then for each x, y, z, solve the system
    bool first = true;
    for(unsigned char i_t = 0; i_t != 3; ++i_t)
      if (((i_t == 0) && x) || ((i_t == 1) && y) || ((i_t == 2) && z)) {
	if (!first)
	  initSolutionVector(solver, i_t);
	first = false;
	fillVector(solver, ids, mesh, result, i_t);
	if (solver.solve()) {
	  // and fill the resulting mesh
	  VertexID idf = 0;
	  for(std::vector<VertexID>::const_iterator id_t = ids.begin(); id_t != ids.end(); ++id_t, ++idf)
	    if ((*id_t) != nbPoints) {
	      assert(!result.point(idf).getIsBoundary());
	      result.point(idf)(i_t) = solver.getVariable(*id_t);
	    }
	}
	else
	  throw Exception("parameterize3D(1): error when solving the linear system");
    }

    return result;
  }

  template <class BParam>
  Mapping2D3D BarycentricMapping<BParam>::parameterize(Mesh & mesh) {
    Mapping2D3D result(mesh);

    BParam::parameterizeBorder(result);

    VertexID nbPoints = mesh.getNbPoints();
    VertexID nbPointsInside = nbPoints - mesh.getNbBoundaryPoints();

    if (nbPointsInside == 0)
      return result;

    std::vector<VertexID> ids(nbPoints, nbPoints);

    OpenNLSolver solver(nbPointsInside);


    initSolutionVector(solver, 0);

    // first compute the map between the two index systems
    VertexID i = 0;
    std::vector<VertexID>::iterator id = ids.begin();
    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p, ++id)
      if (!((*p).getIsBoundary())) {
	*id = i;
	++i;
      }

    // then create the matrix
    fillMatrix(solver, ids, mesh, result);

    // then for each x, y, solve the system
    for(unsigned char i_t = 0; i_t != 2; ++i_t) {
      if (i_t != 0)
	initSolutionVector(solver, i_t);
      fillVector(solver, ids, mesh, result, i_t);
      if (solver.solve()) {
	// and fill the resulting mesh
	VertexID idf = 0;
	for(std::vector<VertexID>::const_iterator id_t = ids.begin(); id_t != ids.end(); ++id_t, ++idf)
	  if ((*id_t) != nbPoints) {
	    assert(!mesh.point(idf).getIsBoundary());
	    result[idf][i_t] = solver.getVariable(*id_t);
	  }
      }
      else
	throw Exception("parameterize(1): error when solving the linear system");
    }
    result.setFlatten();

    return result;
  }

  template <class BParam>
  double MeanValuesParameterization<BParam>::tanThetaOverTwo(const Coord3D & a, const Coord3D & b, const Coord3D & c) {
    Coord3D bb = b - a;
    Coord3D cc = c - a;

    const double cp = a.vectorialProduct(b, c).norm();
    // length of cross product of bb and cc
    const double dp = Coord3D::scalarProduct(bb, cc); // scalar product of bb and cc
    const double bc = bb.norm() * cc.norm();
    return (bc - dp) / cp;

    return true;

  }

  template <class BParam>
  const std::vector<double> MeanValuesParameterization<BParam>::computeWeights(const Mesh & mesh, const Point3D & point) {
    std::vector<double> result;

    double wSum = 0.;
    std::deque<VertexID>::const_iterator predNb = point.getNeighbours().begin() + (point.getNeighbours().size() - 1);
    for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); predNb = nb, ++nb) {
      const double t1 = tanThetaOverTwo(point, mesh.point(*predNb), mesh.point(*nb));
      std::deque<VertexID>::const_iterator nextNb = nb + 1;
      if (nextNb == point.getNeighbours().end())
	nextNb = point.getNeighbours().begin();
      const double t2 = tanThetaOverTwo(point, mesh.point(*nb), mesh.point(*nextNb));
      const double w = (t1 + t2) / point.distance(mesh.point(*nb));
      wSum += w;
      result.push_back(w);
    }

    // sum of the weights = 0
    const double ratio = 1. / wSum;
    for(std::vector<double>::iterator w = result.begin(); w != result.end(); ++w) {
      *w *= ratio;
      assert((*w >= 0.) && (*w <= 1.));
    }

    return result;
  }

  template <class BParam>
  const std::vector<double> TutteParameterization<BParam>::computeWeights(const Mesh &, const Point3D & point) {
    std::vector<double> result;

    const double ratio = 1. / point.getNbNeighbours();

    for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
      result.push_back(ratio);
    }

    return result;
  }

  template <class BParam>
  void TutteParameterization<BParam>::fillMatrix(OpenNLSolver & solver, const std::vector<VertexID> & ids,
					 const Mesh & mesh, const Mesh &) {

    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      if (!((*p).getIsBoundary())){
	const std::vector<double> weights = computeWeights(mesh, *p);
	assert(weights.size() == (*p).getNbNeighbours());
	solver.addMatrixValue(ids[(*p).getId()], ids[(*p).getId()], 1.);
	std::vector<double>::const_iterator w = weights.begin();
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb, ++w)
	  if (!(mesh.point(*nb).getIsBoundary())) {
	    solver.addMatrixValue(ids[(*p).getId()], ids[*nb], -(*w));
	  }
      }
  }

  template <class BParam>
  void TutteParameterization<BParam>::fillVector(OpenNLSolver & solver, const std::vector<VertexID> & ids,
					 const Mesh & mesh, const Mesh & resultMesh, unsigned int id) {

    assert(id < 3);
    for(VertexID i = 0; i != solver.getSize(); ++i)
      solver.addVectorValue(i, 0.);

    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      if (!((*p).getIsBoundary())){
	const std::vector<double> weights = computeWeights(mesh, *p);
	assert(weights.size() == (*p).getNbNeighbours());
	std::vector<double>::const_iterator w = weights.begin();
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb, ++w)
	  if (mesh.point(*nb).getIsBoundary()) {
	    solver.addVectorValue(ids[(*p).getId()], (*w) * resultMesh.point(*nb)(id));
	  }
      }
  }


  template <class BParam>
  void TutteParameterization<BParam>::fillMatrix(OpenNLSolver & solver, const std::vector<VertexID> & ids,
					 const Mesh & mesh, const Mapping2D3D &) {

    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      if (!((*p).getIsBoundary())){
	const std::vector<double> weights = computeWeights(mesh, *p);
	assert(weights.size() == (*p).getNbNeighbours());
	solver.addMatrixValue(ids[(*p).getId()], ids[(*p).getId()], 1.);
	std::vector<double>::const_iterator w = weights.begin();
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb, ++w)
	  if (!(mesh.point(*nb).getIsBoundary())) {
	    solver.addMatrixValue(ids[(*p).getId()], ids[*nb], -(*w));
	  }
      }
  }

  template <class BParam>
  void TutteParameterization<BParam>::fillVector(OpenNLSolver & solver, const std::vector<VertexID> & ids,
					 const Mesh & mesh, const Mapping2D3D & result, unsigned int id) {

    assert(id < 2);
    for(VertexID i = 0; i != solver.getSize(); ++i)
      solver.addVectorValue(i, 0.);

    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      if (!((*p).getIsBoundary())){
	const std::vector<double> weights = computeWeights(mesh, *p);
	assert(weights.size() == (*p).getNbNeighbours());
	std::vector<double>::const_iterator w = weights.begin();
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb, ++w)
	  if (mesh.point(*nb).getIsBoundary()) {
	    solver.addVectorValue(ids[(*p).getId()], (*w) * result[*nb][id]);
	  }
      }
  }



  /**
     @class DiscParameterizationFloater
     @author Jean-Marie Favreau (LIMOS / Blaise Pascal Univ. / IMATI-GE)
     @brief Floater Mean Value Coordinates parameterization (Floater 03) of
     a surface homeomorphic to a disc onto a planar disc. The boundary is a circle
     where edges are located using the distances of the original mesh (as integral distance
     along the disc).
  */
  typedef MeanValuesParameterization<CircleBorderParameterization> DiscParameterizationFloater;

  /**
     @class DiscParameterizationTutte
     @author Jean-Marie Favreau (LIMOS / Blaise Pascal Univ. / IMATI-GE)
     @brief Tutte parameterization of
     a surface homeomorphic to a disc onto a planar disc. The boundary is a circle
     where edges are located using the distances of the original mesh (as integral distance
     along the disc).
  */
  typedef TutteParameterization<CircleBorderParameterization> DiscParameterizationTutte;

  template <class P>
  class QuadParameterization : public P {
  public:
    QuadParameterization() {
    }

    /**
       compute the parameterization given four points on the boundary
       to define the border parameterization
       @param mesh The mesh to be parameterized (required: it have to be a cylinder)
       @param v1 First corner
       @param v2 Second corner
       @param v3 Third corner
       @param v4 Fourth corner
    */
    inline Mapping2D3D parameterize(Mesh & mesh, VertexID v1, VertexID v2, VertexID v3, VertexID v4) {
      P::setCorners(v1, v2, v3, v4);
      return P::parameterize(mesh);
    }

    /**
       compute the parameterization using a quad described by the most "angular" boundary points
       @param mesh The mesh to be parameterized (required: it have to be homeomorphic to a disc)
    */
    inline Mapping2D3D parameterize(Mesh & mesh) {
      P::setCornersAuto(mesh);
      return P::parameterize(mesh);
    }

  };

  typedef QuadParameterization<MeanValuesParameterization<QuadBorderParameterization> > QuadParameterizationFloater;

  typedef QuadParameterization<TutteParameterization<QuadBorderParameterization> > QuadParameterizationTutte;

}


#endif


