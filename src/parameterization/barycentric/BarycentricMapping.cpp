/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010-2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                 CNRS / Univ. Blaise Pascal / IMATI-GE CNR / Univ. d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include "BarycentricMapping.h"
#include "Mesh.h"
#include "MeshManipulator.h"

using namespace Taglut;





/// Border parameterization

std::map<VertexID, Coord2D> CircleBorderParameterization::getBorder2D(Mesh & mesh) {
  std::map<VertexID, Coord2D> result;
  assert(mesh.getNbCC() == 1);
  assert(mesh.getGenus() == 0);
  assert(mesh.getNbBoundaries() == 1);
  MeshManipulator mManip(mesh);

  std::vector<VertexID> bd = mManip.getBoundaryFromBPoint(mesh.getBoundaryPoint());

  const double perimeter = mesh.getLength(bd) + mesh.point(bd.front()).distance(mesh.point(bd.back()));
  const double radius = perimeter / (2 * M_PI);
  const double ratio = 2 * M_PI / perimeter;
  double angle = 0.;

  for(std::vector<VertexID>::const_iterator bdp = bd.begin(); bdp != bd.end(); ++bdp) {
    result[*bdp] = Coord2D(radius * cos(angle), radius * sin(angle));
    std::vector<VertexID>::const_iterator next = bdp + 1;
    if (next != bd.end())
      angle += mesh.point(*bdp).distance(mesh.point(*next)) * ratio;
  }

  return result;
}

std::map<VertexID, Coord2D> QuadBorderParameterization::getBorder2D(Mesh & mesh) {
  std::map<VertexID, Coord2D> result;

  if ((corners[0] == corners[1]) ||
      (corners[0] == corners[2]) ||
      (corners[0] == corners[3]) ||
      (corners[1] == corners[2]) ||
      (corners[1] == corners[3]) ||
      (corners[2] == corners[3]))
    throw Exception("Wrong corner description for quad unfolding");
  if ((!mesh.point(corners[0]).getIsBoundary()) ||
      (!mesh.point(corners[1]).getIsBoundary()) ||
      (!mesh.point(corners[2]).getIsBoundary()) ||
      (!mesh.point(corners[3]).getIsBoundary()))

  assert(mesh.getNbCC() == 1);
  assert(mesh.getGenus() == 0);
  assert(mesh.getNbBoundaries() == 1);

  MeshManipulator mManip(mesh);

  std::vector<VertexID> bd = mManip.getBoundaryFromBPoint(corners[0]);

  std::vector<double> lSides = getLengthSide(bd, mesh);
  assert(lSides.size() == 4);
  std::vector<double> finalLengths;
  finalLengths.push_back((lSides[0] + lSides[2]) / 2.);
  finalLengths.push_back((lSides[1] + lSides[3]) / 2.);
  finalLengths.push_back((lSides[0] + lSides[2]) / 2.);
  finalLengths.push_back((lSides[1] + lSides[3]) / 2.);

  Coord2D direction(0., 1.);
  Coord2D location(0., 0.);
  std::vector<double>::const_iterator currentOriginalLength = lSides.begin();
  std::vector<double>::const_iterator currentFinalLength = finalLengths.begin();
  if (*currentOriginalLength == 0.)
    throw Exception("Empty border");
  double ratio = (*currentFinalLength) / (*currentOriginalLength);

  result[bd.front()] = location;

  std::vector<VertexID>::const_iterator pred = bd.begin();
  for(std::vector<VertexID>::const_iterator bdp = bd.begin(); bdp != bd.end(); ++bdp) {
    if (bdp != bd.begin()) {
      location = location + direction * ratio * mesh.point(*pred).distance(mesh.point(*bdp));
      result[*bdp] = location;
      if (isCorner(*bdp)) {
	++currentOriginalLength;
	++currentFinalLength;
	if (currentFinalLength != finalLengths.end()) {
	  assert(currentOriginalLength != lSides.end());
	  ratio = (*currentFinalLength) / (*currentOriginalLength);
	  direction = Coord2D(direction.get2DY(), -direction.get2DX());
	}
      }
    }
    pred = bdp;
  }

  assert(fabs(result[bd.front()].distance2D(result[bd.back()]) -
	      mesh.point(bd.front()).distance(mesh.point(bd.back())) * finalLengths[3] / lSides[3]) < 0.0001);

  return result;
}

std::vector<double> QuadBorderParameterization::getLengthSide(const std::vector<VertexID> & bd, const Mesh & mesh) const {
  assert(isCorner(bd.front()));
  std::vector<double> result;
  std::vector<std::vector<VertexID> > sides;

  sides.push_back(std::vector<VertexID>());
  for(std::vector<VertexID>::const_iterator pb = bd.begin(); pb != bd.end(); ++pb) {
    if ((pb != bd.begin()) && isCorner(*pb)) {
      sides.back().push_back(*pb);
      sides.push_back(std::vector<VertexID>());
      sides.back().push_back(*pb);
    }
    else {
      sides.back().push_back(*pb);
    }
  }
  if (sides.size() != 4)
    throw Exception("Wrong boundary structure");
  sides.back().push_back(bd.front());

  for(std::vector<std::vector<VertexID> >::const_iterator s = sides.begin(); s != sides.end(); ++s)
    result.push_back(mesh.getLength(*s));

  return result;
}


struct sortSecond {
  bool operator()(const std::pair<VertexID, double> & p1, const std::pair<VertexID, double> & p2) const {
    return p1.second < p2.second;
  }
};


void QuadBorderParameterization::setCornersAuto(const Mesh & mesh) {
  std::vector<std::pair<VertexID, double> > valueList;

  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    if ((*p).getIsBoundary()) {
      unsigned char n = 0;
      VertexID nbbd[2];
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	if (mesh.isBoundaryEdge(*nb, (*p).getId())) {
	  if (n == 2)
	    throw Exception("Non valid configuration on boundary");
	  nbbd[n] = *nb;
	  ++n;
	}
      double angle1 = fabs(M_PI / 2. - (*p).angle(mesh.point(nbbd[0]), mesh.point(nbbd[1])));
      const Coord3D n1 = mesh.computeTriangleNormal(mesh.findTriangle((*p).getId(), nbbd[0]).getId());
      const Coord3D n2 = mesh.computeTriangleNormal(mesh.findTriangle((*p).getId(), nbbd[1]).getId());
      const double angle2 = fabs(Coord3D::angleVector(n1, n2));
      valueList.push_back(std::pair<VertexID, double>((*p).getId(), angle1 + angle2));
    }


  assert(valueList.size() >= 4);
  std::sort(valueList.begin(), valueList.end(), sortSecond());
  corners[0] = valueList[0].first;
  corners[1] = valueList[1].first;
  corners[2] = valueList[2].first;
  corners[3] = valueList[3].first;
}
