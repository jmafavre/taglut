/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "UnfoldingMethod.h"

using namespace Taglut;

UnfoldingMethod::UnfoldingMethod(Mapping2D3D & mapping_t) : mapping(mapping_t), mesh(mapping_t.getMesh()) { }

UnfoldingMethod::UnfoldingMethod(const UnfoldingMethod & uMethod) : mapping(uMethod.mapping), mesh(uMethod.mesh) { }

UnfoldingMethod * UnfoldingMethod::clone(Mapping2D3D & mapping_t) {
  UnfoldingMethod * result = new UnfoldingMethod(mapping_t);

  return result;
}

void UnfoldingMethod::compute2DCoordsUsingDistances(VertexID id, VertexID idP1, VertexID idP2, double d1, double d2, double d3) {
  assert(id < mapping.size());
  assert(idP1 < mapping.size());
  assert(idP2 < mapping.size());

  double ytmp;
  const Triangle & t = mesh.findTriangle(id, idP1, idP2);
  Point2D & cpCurrent = mapping[id];
  Point3D & cpCurrent3D = mapping.point3D(id);
  const Point2D & cp1 = mapping[idP1];
  const Point2D & cp2 = mapping[idP2];

  if (d1 == 0.0) {
    mapping.set2DCoords(id, cp1.get2DX(), cp1.get2DY());
    return;
  }
  if (d2 == 0.0) {
    mapping.set2DCoords(id, cp2.get2DX(), cp2.get2DY());
    return;
  }

  /* this coords in base defined by c1c2 and normal vector */
  double xtmp = (d1 * d1 - d2 * d2 + d3 * d3) / (2 * d3);


  if (d1 * d1 < xtmp * xtmp) {
    // mapping position cannot be computed because of bad round up
    std::cout << "Bad round up in " << cpCurrent << " position computation." << std::endl;
    std::cout << "  using: " << cp1 << " and " << cp2 << std::endl;
    cpCurrent3D.setFlag(2);
    ytmp = 0;
  }
  else
    ytmp = std::sqrt(d1 * d1 - xtmp * xtmp);

  if (!(t.sameOrder(id, idP1, idP2))) /* invert to keep original order */
    ytmp = - ytmp;


  /* add rotation and translation */
  double rx = (cp2.get2DX() - cp1.get2DX()) / d3;
  double ry = (cp2.get2DY() - cp1.get2DY()) / d3;

  mapping.set2DCoords(id, cp1.get2DX() + rx * xtmp - ry * ytmp, cp1.get2DY() + rx * ytmp + ry * xtmp);
}
