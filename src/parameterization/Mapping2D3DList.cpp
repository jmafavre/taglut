/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "FileExceptions.h"
#include "Mapping2D3DList.h"
#include "CPMethod.h"
#include "MeshList.h"
#include "FileTools.h"

using namespace Taglut;

void Mapping2D3DList::addFormats() {
  addLoadFormat("cst");
  addFormat("map");
}

Mapping2D3DList::Mapping2D3DList(MeshList & meshes_l) : meshes(meshes_l) {
  addFormats();
}

std::string Mapping2D3DList::getClusterInfos() const {
  std::string result;

  for(Mapping2D3DList::const_iterator mapping = begin(); mapping != end(); ++mapping)
    result += (*mapping).getClusterInfos();

  return result;
}


void Mapping2D3DList::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string &) {
  if (altFormat == "cst")
    loadCST(fileName);
  else if (altFormat == "map")
    loadMAP(fileName);
  else
    throw Exception("Unknow format");

}

void Mapping2D3DList::saveAltFormat(const std::string & fileName, const std::string & format, const std::string &) const {
  if (format == "map")
    saveMAP(fileName);
  else
    throw Exception("Unknow format");
}

void Mapping2D3DList::saveMAP(const std::string & filename) const {
  std::ofstream outfile(filename.c_str(), std::ios::out);

  if ((*this).size() == 0)
    throw ExceptionEmptyData();
  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
  outfile << "<mappinglist>" << std::endl;
  for(Mapping2D3DList::const_iterator mapping = begin(); mapping != end(); ++mapping)
    outfile << (*mapping).toStringXML();
  outfile << "</mappinglist>" << std::endl;

  outfile.close();
}


void Mapping2D3DList::loadCST(const std::string & filename) {
#ifdef LIBXML_READER_ENABLED
  xmlTextReaderPtr reader;
  float version = 1;
  int ret;
  const char * v;

  reader = xmlReaderForFile(filename.c_str(), NULL, 0);
  clear();
  meshes.clear();

  if (reader == NULL)
    throw ExceptionFileNotFound();
  ret = xmlTextReaderRead(reader);

  // first find file version
  bool cst = (std::string("circleset") == (const char *) xmlTextReaderConstName(reader));
  bool cstl = (std::string("circlesetlist") == (const char *) xmlTextReaderConstName(reader));
  if (ret != 1)
    throw ExceptionErrorDuringReadingFile();
  if (cst || cstl) {
    v = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"version");
    if (v != NULL)
      version = atof(v);
    else
      version = 1;
  }
  else
    throw ExceptionErrorDuringReadingFile();

  // if no child
  if (xmlTextReaderIsEmptyElement(reader) != 0)
    throw ExceptionErrorDuringReadingFile();

  // else jump next child if needed
  if (cstl) {
    do {
      ret = xmlTextReaderRead(reader);
      if (ret == 0)
	throw ExceptionErrorDuringReadingFile();
    } while (std::string("circleset") != (const char *) xmlTextReaderConstName(reader));
  }

  v = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"version");
  if (v != NULL)
    version = atof(v);

  // then load circle set
  if (cst) {
    meshes.push_back(Mesh());
    push_back(meshes.back());
    CPMethod cSet(back());
    cSet.loadCST(reader, version);
  }
  else
    do {
      meshes.push_back(Mesh());
      push_back(meshes.back());
      CPMethod cSet(back());
      cSet.loadCST(reader, version);

      do {
	ret = xmlTextReaderRead(reader);
	if (ret == 0)
	  throw ExceptionErrorDuringReadingFile();
      } while ((xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) && (xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT));

      v = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"version");
      if (v != NULL)
	version = atof(v);

    } while(std::string("circlesetlist") != (const char *) xmlTextReaderConstName(reader));


  // and clear xml reader
  xmlFreeTextReader(reader);

  return;
#else
  throw ExceptionXMLReaderNotAvailable(filename + " not opened");
#endif
}

void Mapping2D3DList::loadMAP(const std::string & filename) {
#ifdef LIBXML_READER_ENABLED
  xmlTextReaderPtr reader;
  int ret;

  reader = xmlReaderForFile(filename.c_str(), NULL, 0);
  clear();
  meshes.clear();

  if (reader == NULL)
    throw ExceptionFileNotFound();
  ret = xmlTextReaderRead(reader);

  // first find file version
  bool mp = (std::string("mapping") == (const char *) xmlTextReaderConstName(reader));
  bool mpl = (std::string("mappinglist") == (const char *) xmlTextReaderConstName(reader));
  if (ret != 1)
    throw ExceptionErrorDuringReadingFile();
  if (!mp & !mpl)
    throw ExceptionErrorDuringReadingFile();

  // if no child
  if (xmlTextReaderIsEmptyElement(reader) != 0)
    throw ExceptionErrorDuringReadingFile();

  // else jump next child if needed
  if (mpl) {
    do {
      ret = xmlTextReaderRead(reader);
      if (ret == 0)
	throw ExceptionErrorDuringReadingFile();
    } while (std::string("mapping") != (const char *) xmlTextReaderConstName(reader));
  }

  // then load circle set
  if (mp) {
    meshes.push_front(Mesh());
    push_front(Mapping2D3D(meshes.front()));
    front().loadXML(reader);
  }
  else
    do {
      meshes.push_front(Mesh());
      push_front(Mapping2D3D(meshes.front()));
      front().loadXML(reader);
      do {
	ret = xmlTextReaderRead(reader);
	if (ret == 0)
	  throw ExceptionErrorDuringReadingFile();
      } while ((xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) && (xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT));
    } while(std::string("mappinglist") != (const char *) xmlTextReaderConstName(reader));


  // and clear xml reader
  xmlFreeTextReader(reader);

  return;
#else
  throw ExceptionXMLReaderNotAvailable(filename + " not opened");
#endif
}

TriangleID Mapping2D3DList::getNbTriangles() const {
  TriangleID result = 0;

  for(Mapping2D3DList::const_iterator mapping = begin(); mapping != end(); ++mapping)
    result += (*mapping).getNbTriangles();

  return result;
}

VertexID Mapping2D3DList::getNbPoints() const {
  VertexID result = 0;

  for(Mapping2D3DList::const_iterator mapping = begin(); mapping != end(); ++mapping)
    result += (*mapping).size();

  return result;
}

VertexID Mapping2D3DList::getGlobalId(unsigned int mappingId, unsigned int cId) const {
  unsigned int i = 0;
  VertexID result = 0;

  for(Mapping2D3DList::const_iterator mapping = begin(); mapping != end(); ++mapping)
    if (i < mappingId) {
      result += (*mapping).size();
      ++i;
    }
    else break;

  return result + cId;
}
