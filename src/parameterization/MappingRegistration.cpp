/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>
#include <deque>
#include "MappingRegistration.h"
#include "Mapping2D3D.h"
#include "IndentManager.h"

using namespace Taglut;

MappingRegistration::MappingRegistration(Mapping2D3D & mapping_l, const Mapping2D3D & reference_l) : mapping(mapping_l), reference(reference_l), mMatcher(mapping_l.getMesh(), reference_l.getMesh()) {

}

double MappingRegistration::crossProduct(const Coord2D & p1, const Coord2D & p2, const Coord2D & p3) {
  Coord2D v1 = p2 - p1;
  Coord2D v2 = p3 - p1;

  return (v1.get2DX() * v2.get2DY() - v2.get2DX() * v1.get2DY());
}

bool MappingRegistration::needFlip() const {
  if (mapping.getNbTriangles() == 0)
    return false;
  const Triangle & t = mapping.getTriangle(0);

  double cProductM = crossProduct(mapping[t.getP1()], mapping[t.getP2()], mapping[t.getP3()]);
  double cProductR = crossProduct(reference[t.getP1()], reference[t.getP2()], reference[t.getP3()]);

  return (cProductR * cProductM < 0);
}

double MappingRegistration::getMedian(std::deque<double> & alphas) {
  std::sort(alphas.begin(), alphas.end());
  unsigned int size = alphas.size();

  if (size == 0)
    return 0.0;
  else if (size == 1)
    return alphas[0];
  else if (size % 2 == 1)
    return alphas[(unsigned int)trunc(((double)size) / 2) + 1];
  else
    return (alphas[size / 2] + alphas[size / 2 + 1]) / 2;
}


void MappingRegistration::naiveRotation(const Coord2D & center, const IndentManager & iManager) {
  // find the farest point
  std::pair<VertexID, VertexID> id;
  double norm = 0;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double localNorm = mapping[(*mm).first].distance2D(center);
    if (localNorm > norm) {
      id = *mm;
      norm = localNorm;
    }
  }

  // then compute rotation
  Coord2D vm = mapping[id.first] - center;
  Coord2D vr = reference[id.second] - center;
  double alpha = vr.angle2D() - vm.angle2D();

  mapping.rotate(alpha, center);
  std::cout << iManager << " Naive rotation: " << alpha << std::endl;

}

void MappingRegistration::apply(const IndentManager & iManager) {
  const double epsilon = 0.000000001;

  std::deque<VertexID> mappingPoints;
  std::deque<VertexID> referencePoints;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    mappingPoints.push_back((*mm).first);
    referencePoints.push_back((*mm).second);
  }

  // compute translation
  Coord2D mCenter = mapping.compute2DBarycenter(mappingPoints);
  Coord2D refCenter = reference.compute2DBarycenter(referencePoints);

  mappingPoints.clear();
  referencePoints.clear();

  // apply translation
  mapping.translate(refCenter - mCenter);
  std::cout << iManager << " Translation: " << (refCenter - mCenter) << std::endl;

  // compute flip if needed
  if (needFlip()) {
    mapping.flip();
    std::cout << iManager << " Flip" << std::endl;
  }
  assert(!needFlip());

  // realign with basic method
  naiveRotation(refCenter, iManager);

  // compute rotation
  std::deque<double> alphas;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double mx = mapping[(*mm).first].get2DX() - refCenter.get2DX();
    double my = mapping[(*mm).first].get2DY() - refCenter.get2DY();
    double rx = reference[(*mm).second].get2DX() - refCenter.get2DX();
    double ry = reference[(*mm).second].get2DY() - refCenter.get2DY();
    double norm = mx * mx + my * my;
    if (norm > epsilon)
      alphas.push_back(asin((mx * ry - rx * my) / norm));
  }
  double alpha = getMedian(alphas);
  alphas.clear();

  // apply rotation
  mapping.rotate(alpha, refCenter);
  std::cout << iManager << " Rotation: " << alpha << std::endl;


  // compute scale
  std::deque<double> ratios;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double norm1 = mapping[(*mm).first].distance2D(refCenter);
    double norm2 = reference[(*mm).second].distance2D(refCenter);
    if (norm1 > epsilon)
      ratios.push_back(norm2 / norm1);
  }
  double ratio = getMedian(ratios);
  ratios.clear();

  // apply scale
  mapping.scale(ratio, refCenter);
  std::cout << iManager << " Scale: " << ratio << std::endl;

}

