/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef ABF_METHOD
#define ABF_METHOD

#include <map>
#include "UnfoldingMethod.h"

namespace Taglut {
  class Mapping2D3D;
  class WeightedEdges;

  /**
     @class ABFSolver
     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @brief Implementation of the ABF algorithm
     A large part of this class come from the source code of
     blender (http://www.blender.org)
  */
  class ABFSolver {
  protected:
    VertexID ninterior;
    TriangleID nfaces;
    TriangleID nangles;

    float * alpha;
    float * beta;
    float * sine;
    float * cosine;
    float * weight;

    float * bAlpha;
    float * bTriangle;
    float * bInterior;

    float * lambdaTriangle;
    float * lambdaPlanar;
    float * lambdaLength;

    static float minangle;
    static float maxangle;

    float * bstar;
    float * dstar;

    float (*J2dt)[3];

    class ABFEdge;
    class ABFFace;
    class ABFVertex;

    ABFEdge * edges;
    ABFFace * faces;
    ABFVertex * vertices;

    const Mesh & oMesh;

    /**
       Id of interior points
    */
    std::map<VertexID, VertexID> interiorId;

    /**
       init data structures
    */
    void initData(bool iData);

    /**
       Compute initial angles
    */
    void computeInitialAngles(bool iData);


    /**
       Given a triangle and a point id (on the mesh), compute id of
       the angle on the ABFMethod data structure
    */
    TriangleID getAngleId(TriangleID idT, VertexID idP) const;

    /**
       Given a triangle and a point id (on the mesh), compute id of
       the pred angle (in the triangle order) on the ABFMethod data structure
    */
    TriangleID getPredAngleId(TriangleID idT, VertexID idP) const;

    /**
       Given a triangle and a point id (on the mesh), compute id of
       the next angle (in the triangle order) on the ABFMethod data structure
    */
    TriangleID getNextAngleId(TriangleID idT, VertexID idP) const;

    /**
       Given a triangle and an angle id (in global list), return the corresponding point
    */
    const Point3D & getPoint(TriangleID idT, TriangleID angleId);

    /**
       Clear memory (including alpha[] data)
    */
    void clearAllMemory();

    /**
       Clear memory (except alpha[] data)
    */
    void clearMemory();

    /**
       Compute gradient (cf ABF algorithm)
    */
    float computeGradient();

    /**
       Compute sinus product (used by computeGradient())
    */
    float computeSinProduct(const Point3D & p, int aid) const;

    /**
       Compute alpha grad (used by computeGradient())
    */
    float computeGradAlpha(const Triangle & t, TriangleID angleId);

    /**
       Compute sines (cf ABF algorithm)
    */
    void computeSines();

    /**
       inverse the matrix (cf ABF algorithm)
    */
    bool matrixInverse();

    /**
       Log initial angles
    */
    void logInitialAngles(const std::string & logFile) const;

    /**
       Log final values
    */
    void logFinalValues(const std::string & logFile, bool success) const;

  public:
    /** default constructor */
    ABFSolver(const Mesh & mesh);

    /** destructor */
    virtual ~ABFSolver();

    /**
       Process unfolding using ABF algorithm
       Return number of iteration or -1 if fails
    */
    int processUnfolding(float limit, unsigned int maxIter, bool initData = true, const std::string & logFile = "", const Taglut::IndentManager & indentManager = Taglut::IndentManager());

    /**
       Clear all tempory data
    */
    void clearTempData();

    /**
       return the lengths computed using the unfolding.
       The scale is defined by fixing the length of the edge (v1, v2) to the given value.
       If v1 = v2 or length <= 0.0, the scale is fixed.
     */
    WeightedEdges getLengths(VertexID v1 = 0, VertexID v2 = 0, double length = 0.) const;
  };



  /**
     @class ABFMethod
     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @brief Unfolding a surface homeomorphic to a disc using ABF
  */
  class ABFMethod : public UnfoldingMethod, public ABFSolver {
  private:


    /**
       Given a triangle and an edge of points on this triangle allready computed,
       compute the other point location (if needed) and update flags
       Return false if point has been computed with a bad location
    */
    bool compute2DLocation(Triangle & t, const std::pair<VertexID, VertexID> & edge);


  public:
    /**
       Constructor
    */
    ABFMethod(Mapping2D3D & mapping);

    /**
       Copy constructor
    */
    ABFMethod(const ABFMethod & abfMethod);

    /**
       Destructor
    */
    virtual ~ABFMethod();


    /**
       Compute 2D location
    */
    void compute2DLocation(const Taglut::IndentManager & indentManager, bool adjustMeanDistance = true);


    /**
       Clone method
    */
    virtual UnfoldingMethod * clone(Mapping2D3D & mapping);

    /**
       Process unfolding using ABF algorithm
       Return number of iteration or -1 if fails
    */
    int processUnfolding(float limit, unsigned int maxIter, bool initData = true, const std::string & logFile = "", const Taglut::IndentManager & indentManager = Taglut::IndentManager());


    /**
       Init unfolding data using 2d location of points
    */
    virtual void initDataUsing2D();

  };
}

#endif
