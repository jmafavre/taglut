/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "ABFMethod.h"
#include "FileExceptions.h"
#include "WeightedEdges.h"
#include "ONL_opennl.h"

using namespace Taglut;

float ABFSolver::minangle = 7.5 * M_PI / 180.0;
float ABFSolver::maxangle = M_PI - minangle;

ABFSolver::~ABFSolver() {
  clearAllMemory();
}


ABFSolver::ABFSolver(const Mesh & mesh) : oMesh(mesh) {
  nangles = nfaces = ninterior = 0;
  alpha = beta = sine = cosine = weight = NULL;
  bAlpha = bTriangle = bInterior = NULL;
  lambdaTriangle = lambdaPlanar = lambdaLength = NULL;
  J2dt = NULL;
  bstar = dstar = NULL;
}

ABFMethod::ABFMethod(Mapping2D3D & mapping_l) : UnfoldingMethod(mapping_l), ABFSolver(mapping_l.getMesh()) {
}

ABFMethod::ABFMethod(const ABFMethod & abfMethod) : UnfoldingMethod(abfMethod), ABFSolver(abfMethod.getMesh()) {
}


ABFMethod::~ABFMethod() {
}

void ABFSolver::clearAllMemory() {
  if (alpha != NULL) {
    delete [] alpha;

    alpha = NULL;
  }
  clearMemory();
}

void ABFSolver::clearMemory() {
  if (beta != NULL) {
    delete [] beta;
    delete [] sine;
    delete [] cosine;
    delete [] weight;

    delete [] bAlpha;
    delete [] bTriangle;
    delete [] bInterior;

    delete [] lambdaTriangle;
    delete [] lambdaPlanar;
    delete [] lambdaLength;

    delete [] J2dt;
    delete [] bstar;
    delete [] dstar;

    beta = NULL;

    interiorId.clear();
  }
}

void ABFSolver::clearTempData() {
  clearAllMemory();
}


const Point3D & ABFSolver::getPoint(TriangleID idT, TriangleID angleId) {
  if ((angleId < idT * 3) || (angleId > idT * 3 + 2))
    throw Exception("getPoint(2): wrong angle id.");
  VertexID idP = angleId - idT * 3;
  return oMesh.point(oMesh.triangle(idT).getVertexId(idP));
}


TriangleID ABFSolver::getAngleId(TriangleID idT, VertexID idP) const {
  const Triangle & t = oMesh.triangle(idT);

  if (t.getP1() == idP)
    return idT * 3;
  else if (t.getP2() == idP)
    return idT * 3 + 1;
  else if (t.getP3() == idP)
    return idT * 3 + 2;

  throw Exception("getAngleId(2): the given triangle does not contain the given point.");
}

TriangleID ABFSolver::getPredAngleId(TriangleID idT, VertexID idP) const {
  const Triangle & t = oMesh.triangle(idT);

  if (t.getP1() == idP)
    return idT * 3 + 2;
  else if (t.getP2() == idP)
    return idT * 3;
  else if (t.getP3() == idP)
    return idT * 3 + 1;

  throw Exception("getPredAngleId(2): the given triangle does not contain the given point.");
}

TriangleID ABFSolver::getNextAngleId(TriangleID idT, VertexID idP) const {
  const Triangle & t = oMesh.triangle(idT);

  if (t.getP1() == idP)
    return idT * 3 + 1;
  else if (t.getP2() == idP)
    return idT * 3 + 2;
  else if (t.getP3() == idP)
    return idT * 3;

  throw Exception("getNextAngleId(2): the given triangle does not contain the given point.");
}


void ABFSolver::initData(bool iData) {
  if (iData || (alpha == NULL))
    clearAllMemory();
  else
    clearMemory();

  // compute memory size
  nfaces = oMesh.getNbTriangles();
  nangles = nfaces * 3;

  ninterior = 0;
  for(Mesh::const_point_iterator p = oMesh.point_begin(); p != oMesh.point_end(); ++p)
    if (!(*p).getIsBoundary())
      interiorId[(*p).getId()] = ninterior++;

  assert(nangles != 0);

  // memory allocation
  if (iData)
    alpha = new float[nangles];
  beta = new float[nangles];
  sine = new float[nangles];
  cosine = new float[nangles];
  weight = new float[nangles];

  bAlpha = new float[nangles];
  bTriangle = new float[nangles];
  bInterior = new float[2 * ninterior];

  lambdaTriangle = new float[nfaces];
  lambdaPlanar = new float[ninterior];
  lambdaLength = new float[ninterior];

  J2dt = new float[nangles][3];
  bstar = new float[nfaces];
  dstar = new float[nfaces];

  for(VertexID i = 0; i < ninterior; ++i) {
    lambdaPlanar[i] = 0.0;
    lambdaLength[i] = 1.0;
  }

  for(VertexID i = 0; i < ninterior * 2; ++i)
    bInterior[i] = 0.0;

  for(TriangleID i = 0; i < nangles; ++i) {
    beta[i] = sine[i] = cosine[i] = weight[i] = bAlpha[i] = bTriangle[i] = 0.0;
    J2dt[i][0] = J2dt[i][1] = J2dt[i][2] = 0.0;
    if (iData)
      alpha[i] = 0.0;
  }


  for(TriangleID i = 0; i < nfaces; ++i)
    bstar[i] = dstar[i] = lambdaTriangle[i] =  0.0;

}

void ABFSolver::computeInitialAngles(bool iData) {
  minangle = 7.5f * M_PI / 180.0f;
  maxangle = M_PI - minangle;

  for(Mesh::const_triangle_iterator t = oMesh.triangle_begin(); t != oMesh.triangle_end(); ++t) {
    TriangleID idA1 = (*t).getId() * 3;
    TriangleID idA2 = idA1 + 1;
    TriangleID idA3 = idA1 + 2;

    float a1, a2, a3;
    a1 = fabs(oMesh.point((*t).getP1()).angle(oMesh.point((*t).getP2()), oMesh.point((*t).getP3())));
    a2 = fabs(oMesh.point((*t).getP2()).angle(oMesh.point((*t).getP1()), oMesh.point((*t).getP3())));
    a3 = M_PI - a1 - a2;

    if (a1 < minangle)
      a1 = minangle;
    else if (a1 > maxangle)
      a1 = maxangle;

    if (a2 < minangle)
      a2 = minangle;
    else if (a2 > maxangle)
      a2 = maxangle;

    if (a3 < minangle)
      a3 = minangle;
    else if (a3 > maxangle)
      a3 = maxangle;

    beta[idA1] = a1;
    beta[idA2] = a2;
    beta[idA3] = a3;
    if (iData) {
      alpha[idA1] = a1;
      alpha[idA2] = a2;
      alpha[idA3] = a3;
    }

    weight[idA1] = 2.0 / (a1 * a1);
    weight[idA2] = 2.0 / (a2 * a2);
    weight[idA3] = 2.0 / (a3 * a3);
  }

  for(Mesh::const_point_iterator p = oMesh.point_begin(); p != oMesh.point_end(); ++p) {
    if (!(*p).getIsBoundary()) {
      float anglesum = 0.0, scale;

      for(std::deque<TriangleID>::const_iterator t = (*p).getTriangles().begin(); t != (*p).getTriangles().end(); ++t)
	anglesum += beta[getAngleId(*t, (*p).getId())];

      scale = (anglesum == 0.0f) ? 0.0f : 2 * M_PI / anglesum;

      for(std::deque<TriangleID>::const_iterator t = (*p).getTriangles().begin(); t != (*p).getTriangles().end(); ++t) {
	TriangleID aId = getAngleId(*t, (*p).getId());
	beta[aId] = beta[aId] * scale;
	if (iData)
	  alpha[aId] = beta[aId];
      }
    }
  }
}


int ABFMethod::processUnfolding(float limit_l, unsigned int maxIter_l, bool initData_l, const std::string & logFile_l,
				const Taglut::IndentManager & indentManager_l) {
  return ABFSolver::processUnfolding(limit_l, maxIter_l, initData_l, logFile_l, indentManager_l);
}

int ABFSolver::processUnfolding(float limit, unsigned int maxIter, bool iData, const std::string & logFile, const IndentManager & indentManager) {
  clock_t tstart = clock();
  unsigned int nbIter = 0;
  std::cout << indentManager << " Data initialisation." << std::endl;
  initData(iData);

  std::cout << indentManager << " Initial angles computation." << std::endl;
  computeInitialAngles(iData);

  logInitialAngles(logFile);

  std::cout << indentManager << " Running ABF algorithm..." << std::endl;

  if (ninterior > 0) {
    computeSines();

    /* iteration */
    for (nbIter = 0; nbIter <maxIter; ++nbIter) {
      float norm = computeGradient();
      std::cout << indentManager << "  Iteration " << nbIter << " (norm: " << norm << ")..." << std::endl;

      if (norm < limit)
	break;

      if (!matrixInverse()) {
	std::cout << indentManager <<  " ABF: failed to invert matrix." << std::endl;
	logFinalValues(logFile, false);
	clearAllMemory();
	return -1;
      }

      computeSines();
    }

    if (nbIter == maxIter) {
      std::cout << indentManager <<  " ABF: maximum iterations reached." << std::endl;
      logFinalValues(logFile, false);
      clearAllMemory();
      return -1;
    }
  }
  clock_t tend = clock();
  std::cout << indentManager << " ABF duration: " << ((double)(tend - tstart)) / (CLOCKS_PER_SEC) << " seconds" << std::endl;

  logFinalValues(logFile, true);

  clearMemory();
  return nbIter;
}

void ABFSolver::logInitialAngles(const std::string & logFile) const {
  if (logFile == "")
    return;

  std::ofstream outfile(logFile.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  // compute date
  time_t rawtime;
  struct tm * timeinfo;
  char buffer [80];
  time ( &rawtime );
  timeinfo = localtime(&rawtime);
  strftime(buffer, 80, "%c", timeinfo);

  // then log to file
  outfile.precision(20);

  outfile << "Logs of the ABF algorithm - " << buffer << std::endl;
  outfile << "Number of angles: " << nangles << std::endl;
  outfile << "Number of lambda (triangles): " << nfaces << std::endl;
  outfile << "Number of lambda (planar): " << ninterior << std::endl;
  outfile << "Number of lambda (length): " << ninterior << std::endl;
  outfile << "Angle values at begin of ABF:" << std::endl;

  for(TriangleID i = 0; i < nangles; ++i)
    outfile << "alpha: " << alpha[i] << ", beta: " << beta[i] << std::endl;

  outfile.close();
}

void ABFSolver::logFinalValues(const std::string & logFile, bool success) const {
  if (logFile == "")
    return;

  std::ofstream outfile(logFile.c_str(), std::ios::out | std::ios::app);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  // compute date
  time_t rawtime;
  struct tm * timeinfo;
  char buffer [80];
  time ( &rawtime );
  timeinfo = localtime(&rawtime);
  strftime(buffer, 80, "%c", timeinfo);

  // then log to file
  outfile.precision(20);

  if (success)
    outfile << "End of ABF unfolding process - " << buffer << std::endl;
  else
    outfile << "End of ABF unfolding process (failed) - " << buffer << std::endl;

  for(TriangleID i = 0; i < nangles; ++i)
    outfile << "alpha: " << alpha[i] << std::endl;

  for(TriangleID i = 0; i < nfaces; ++i)
    outfile << "lambdaTriangle: " << lambdaTriangle[i] << std::endl;

  for(VertexID i = 0; i < ninterior; ++i)
    outfile << "lambdaPlanar: " << lambdaPlanar[i] << std::endl;

  for(VertexID i = 0; i < ninterior; ++i)
    outfile << "lambdaLength: " << lambdaLength[i] << std::endl;

  outfile.close();

}

float ABFSolver::computeSinProduct(const Point3D & p, int aid) const {
  float sin1, sin2;

  sin1 = sin2 = 1.0;

  for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t) {
    TriangleID angleId1 = getNextAngleId(*t, p.getId());
    TriangleID angleId2 = getPredAngleId(*t, p.getId());

    if (aid == (int)angleId1) {
      /* we are computing a derivative for this angle,
	 so we use cos and drop the other part */
      sin1 *= cosine[angleId1];
      sin2 = 0.0;
    }
    else
      sin1 *= sine[angleId1];

    if (aid == (int)angleId2) {
      /* see above */
      sin1 = 0.0;
      sin2 *= cosine[angleId2];
    }
    else
      sin2 *= sine[angleId2];

  }

  return (sin1 - sin2);
}


float ABFSolver::computeGradAlpha(const Triangle & t, TriangleID angleId) {
  assert(angleId < nangles);
  const Point3D & v = getPoint(t.getId(), angleId);
  const Point3D & v1 = oMesh.point(t.getNextVertexId(v.getId()));
  const Point3D & v2 = oMesh.point(t.getNextVertexId(v1.getId()));

  float deriv = (alpha[angleId] - beta[angleId]) * weight[angleId];
  deriv += lambdaTriangle[t.getId()];

  if (!v.getIsBoundary()) {
    assert(interiorId[v.getId()] < ninterior);
    deriv += lambdaPlanar[interiorId[v.getId()]];
  }

  if (!v1.getIsBoundary()) {
    float product = computeSinProduct(v1, angleId);
    deriv += lambdaLength[interiorId[v1.getId()]] * product;
  }

  if (!v2.getIsBoundary()) {
    float product =  computeSinProduct(v2, angleId);
    deriv += lambdaLength[interiorId[v2.getId()]] * product;
  }

  return deriv;
}

float ABFSolver::computeGradient() {
  float norm = 0.0;

  for(Mesh::const_triangle_iterator t = oMesh.triangle_begin(); t != oMesh.triangle_end(); ++t) {
    TriangleID idA1 = (*t).getId() * 3;
    TriangleID idA2 = idA1 + 1;
    TriangleID idA3 = idA1 + 2;

    float gtriangle, galpha1, galpha2, galpha3;

    galpha1 = computeGradAlpha(*t, idA1);
    galpha2 = computeGradAlpha(*t, idA2);
    galpha3 = computeGradAlpha(*t, idA3);

    bAlpha[idA1] = - galpha1;
    bAlpha[idA2] = - galpha2;
    bAlpha[idA3] = - galpha3;

    norm += galpha1 * galpha1 + galpha2 * galpha2 + galpha3 * galpha3;

    gtriangle = alpha[idA1] + alpha[idA2] + alpha[idA3] - M_PI;
    bTriangle[(*t).getId()] = - gtriangle;
    norm += gtriangle * gtriangle;
  }


  for(Mesh::const_point_iterator p = oMesh.point_begin(); p != oMesh.point_end(); ++p) {
    if (!(*p).getIsBoundary()) {
      float gplanar = - 2 * M_PI, glength;

      for(std::deque<TriangleID>::const_iterator t = (*p).getTriangles().begin(); t != (*p).getTriangles().end(); ++t)
	gplanar += alpha[getAngleId(*t, (*p).getId())];

      bInterior[interiorId[(*p).getId()]] = - gplanar;
      norm += gplanar * gplanar;

      glength = computeSinProduct(*p, -1);
      bInterior[ninterior + interiorId[(*p).getId()]] = - glength;
      norm += glength * glength;
    }
  }

  return norm;
}

void ABFSolver::computeSines() {
  float *ptsine = sine, *ptcosine = cosine, *ptalpha = alpha;
  for (TriangleID i = 0; i < nangles; ++i, ++ptsine, ++ptcosine, ++ptalpha) {
    *ptsine = sin(*ptalpha);
    *ptcosine = cos(*ptalpha);
  }
}

bool ABFSolver::matrixInverse() {
  VertexID i, j, nvar = 2 * ninterior;
  bool success;

  nlNewContext();
  nlSolverParameteri(NL_NB_VARIABLES, nvar);

  nlBegin(NL_SYSTEM);

  nlBegin(NL_MATRIX);

  for (i = 0; i < nvar; ++i) {
    nlRightHandSideAdd(i, bInterior[i]);
  }

  for(Mesh::const_triangle_iterator t = oMesh.triangle_begin(); t != oMesh.triangle_end(); ++t) {
    TriangleID idA1 = (*t).getId() * 3;
    TriangleID idA2 = idA1 + 1;
    TriangleID idA3 = idA1 + 2;

    float wi1, wi2, wi3, b, si, beta_l[3], j2[3][3], W[3][3];
    float row1[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    float row2[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    float row3[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    int vid[6];
    const Point3D & v1 = getPoint((*t).getId(), idA1);
    const Point3D & v2 = oMesh.point((*t).getNextVertexId(v1.getId()));
    const Point3D & v3 = oMesh.point((*t).getNextVertexId(v2.getId()));

    wi1 = 1.0 / weight[idA1];
    wi2 = 1.0 / weight[idA2];
    wi3 = 1.0 / weight[idA3];

    /* bstar1 = (J1*dInv*bAlpha - bTriangle) */
    b = bAlpha[idA1] * wi1;
    b += bAlpha[idA2] * wi2;
    b += bAlpha[idA3] * wi3;
    b -= bTriangle[(*t).getId()];

    /* si = J1*d*J1t */
    si = 1.0 / (wi1 + wi2 + wi3);

    /* J1t*si*bstar1 - bAlpha */
    beta_l[0] = b * si - bAlpha[idA1];
    beta_l[1] = b * si - bAlpha[idA2];
    beta_l[2] = b * si - bAlpha[idA3];

    /* use this later for computing other lambda's */
    bstar[(*t).getId()] = b;
    dstar[(*t).getId()] = si;

    /* set matrix */
    W[0][0] = si - weight[idA1]; W[0][1] = si; W[0][2] = si;
    W[1][0] = si; W[1][1] = si - weight[idA2]; W[1][2] = si;
    W[2][0] = si; W[2][1] = si; W[2][2] = si - weight[idA3];

    vid[0] = vid[1] = vid[2] = vid[3] = vid[4] = vid[5] = -1;

    if (!v1.getIsBoundary()) {
      vid[0] = interiorId[v1.getId()];
      vid[3] = ninterior + interiorId[v1.getId()];

      J2dt[idA1][0] = j2[0][0] = 1.0 * wi1;
      J2dt[idA2][0] = j2[1][0] = computeSinProduct(v1, idA2) * wi2;
      J2dt[idA3][0] = j2[2][0] = computeSinProduct(v1, idA3) * wi3;

      nlRightHandSideAdd(interiorId[v1.getId()], j2[0][0] * beta_l[0]);
      nlRightHandSideAdd(ninterior + interiorId[v1.getId()], j2[1][0] * beta_l[1] + j2[2][0] * beta_l[2]);

      row1[0] = j2[0][0] * W[0][0];
      row2[0] = j2[0][0] * W[1][0];
      row3[0] = j2[0][0] * W[2][0];

      row1[3] = j2[1][0] * W[0][1] + j2[2][0] * W[0][2];
      row2[3] = j2[1][0] * W[1][1] + j2[2][0] * W[1][2];
      row3[3] = j2[1][0] * W[2][1] + j2[2][0] * W[2][2];
    }

    if (!v2.getIsBoundary()) {
      vid[1] = interiorId[v2.getId()];
      vid[4] = ninterior + interiorId[v2.getId()];

      J2dt[idA1][1] = j2[0][1] = computeSinProduct(v2, idA1) * wi1;
      J2dt[idA2][1] = j2[1][1] = 1.0 * wi2;
      J2dt[idA3][1] = j2[2][1] = computeSinProduct(v2, idA3) * wi3;

      nlRightHandSideAdd(interiorId[v2.getId()], j2[1][1] * beta_l[1]);
      nlRightHandSideAdd(ninterior + interiorId[v2.getId()], j2[0][1] * beta_l[0] + j2[2][1] * beta_l[2]);

      row1[1] = j2[1][1] * W[0][1];
      row2[1] = j2[1][1] * W[1][1];
      row3[1] = j2[1][1] * W[2][1];

      row1[4] = j2[0][1] * W[0][0] + j2[2][1] * W[0][2];
      row2[4] = j2[0][1] * W[1][0] + j2[2][1] * W[1][2];
      row3[4] = j2[0][1] * W[2][0] + j2[2][1] * W[2][2];
    }

    if (!v3.getIsBoundary()) {
      vid[2] = interiorId[v3.getId()];
      vid[5] = ninterior + interiorId[v3.getId()];

      J2dt[idA1][2] = j2[0][2] = computeSinProduct(v3, idA1) * wi1;
      J2dt[idA2][2] = j2[1][2] = computeSinProduct(v3, idA2) * wi2;
      J2dt[idA3][2] = j2[2][2] = 1.0 * wi3;

      nlRightHandSideAdd(interiorId[v3.getId()], j2[2][2] * beta_l[2]);
      nlRightHandSideAdd(ninterior + interiorId[v3.getId()], j2[0][2] * beta_l[0] + j2[1][2] * beta_l[1]);

      row1[2] = j2[2][2] * W[0][2];
      row2[2] = j2[2][2] * W[1][2];
      row3[2] = j2[2][2] * W[2][2];

      row1[5] = j2[0][2] * W[0][0] + j2[1][2] * W[0][1];
      row2[5] = j2[0][2] * W[1][0] + j2[1][2] * W[1][1];
      row3[5] = j2[0][2] * W[2][0] + j2[1][2] * W[2][1];
    }

    for (i = 0; i < 3; ++i) {
      int r = vid[i];

      if (r == -1)
	continue;

      for (j = 0; j < 6; ++j) {
	int c = vid[j];

	if (c == -1)
	  continue;

	if (i == 0)
	  nlMatrixAdd(r, c, j2[0][i] * row1[j]);
	else
	  nlMatrixAdd(r + ninterior, c, j2[0][i] * row1[j]);

	if (i == 1)
	  nlMatrixAdd(r, c, j2[1][i] * row2[j]);
	else
	  nlMatrixAdd(r + ninterior, c, j2[1][i] * row2[j]);


	if (i == 2)
	  nlMatrixAdd(r, c, j2[2][i] * row3[j]);
	else
	  nlMatrixAdd(r + ninterior, c, j2[2][i] * row3[j]);
      }
    }
  }

  nlEnd(NL_MATRIX);

  nlEnd(NL_SYSTEM);

  success = nlSolve();

  if (success) {
    for(Mesh::const_triangle_iterator t = oMesh.triangle_begin(); t != oMesh.triangle_end(); ++t) {
      TriangleID idA1 = (*t).getId() * 3;
      TriangleID idA2 = idA1 + 1;
      TriangleID idA3 = idA1 + 2;
      float dlambda1, pre[3], dalpha;
      const Point3D & v1 = getPoint((*t).getId(), idA1);
      const Point3D & v2 = oMesh.point((*t).getNextVertexId(v1.getId()));
      const Point3D & v3 = oMesh.point((*t).getNextVertexId(v2.getId()));

      pre[0] = pre[1] = pre[2] = 0.0;

      if (!v1.getIsBoundary()) {
	float x = nlGetVariable(interiorId[v1.getId()]);
	float x2 = nlGetVariable(ninterior + interiorId[v1.getId()]);
	pre[0] += J2dt[idA1][0] * x;
	pre[1] += J2dt[idA2][0] * x2;
	pre[2] += J2dt[idA3][0] * x2;
      }

      if (!v2.getIsBoundary()) {
	float x = nlGetVariable(interiorId[v2.getId()]);
	float x2 = nlGetVariable(ninterior + interiorId[v2.getId()]);
	pre[0] += J2dt[idA1][1] * x2;
	pre[1] += J2dt[idA2][1] * x;
	pre[2] += J2dt[idA3][1] * x2;
      }

      if (!v3.getIsBoundary()) {
	float x = nlGetVariable(interiorId[v3.getId()]);
	float x2 = nlGetVariable(ninterior + interiorId[v3.getId()]);
	pre[0] += J2dt[idA1][2] * x2;
	pre[1] += J2dt[idA2][2] * x2;
	pre[2] += J2dt[idA3][2] * x;
      }

      dlambda1 = pre[0] + pre[1] + pre[2];
      dlambda1 = dstar[(*t).getId()] * (bstar[(*t).getId()] - dlambda1);

      lambdaTriangle[(*t).getId()] += dlambda1;

      dalpha = (bAlpha[idA1] - dlambda1);
      alpha[idA1] += dalpha / weight[idA1] - pre[0];

      dalpha = (bAlpha[idA2] - dlambda1);
      alpha[idA2] += dalpha / weight[idA2] - pre[1];

      dalpha = (bAlpha[idA3] - dlambda1);
      alpha[idA3] += dalpha / weight[idA3] - pre[2];

      /* clamp */
      assert(idA1 < nangles);
      assert(idA2 < nangles);
      assert(idA3 < nangles);

      if (alpha[idA1] > M_PI)
	alpha[idA1] = M_PI;
      else if (alpha[idA1] < 0.0f)
	alpha[idA1] = 0.0f;

      if (alpha[idA2] > M_PI)
	alpha[idA2] = M_PI;
      else if (alpha[idA2] < 0.0f)
	alpha[idA2] = 0.0f;

      if (alpha[idA3] > M_PI)
	alpha[idA3] = M_PI;
      else if (alpha[idA3] < 0.0f)
	alpha[idA3] = 0.0f;

    }

    for (i = 0; i < ninterior; ++i) {
      lambdaPlanar[i] += nlGetVariable(i);
      lambdaLength[i] += nlGetVariable(ninterior + i);
    }
  }

  nlDeleteContext(nlGetCurrent());

  return success;
}

void ABFMethod::compute2DLocation(const IndentManager & indentManager, bool adjustMeanDistance) {
  VertexID fP = mesh.getMiddlePoint().getId();
  VertexID nbBad = 0;

  if (alpha == NULL) {
    std::cout << indentManager << "Cannot compute 2d location (unfolding step was not applied)." << std::endl;
    return;
  }

  std::cout << indentManager << " Computing 2D location." << std::endl;
  std::deque<std::pair<VertexID, VertexID> > open;
  mapping.reset2DCoords();

  mesh.setTriangleFlag(0);
  mesh.setPointFlag(0);

  // find the first edge of the mesh
  assert(mesh.point(fP).getNeighbours().size() != 0);
  Point3D & firstPoint = mapping.point3D(fP);

  Point3D & secondPoint = mapping.point3D(mesh.point(firstPoint.getId()).getNeighbours().front());
  assert(firstPoint.getId() == fP);
  assert(secondPoint.getId() == mesh.point(firstPoint.getId()).getNeighbours().front());

  firstPoint.setFlag(1);
  secondPoint.setFlag(1);

  // update min and max locations
  mapping.set2DCoords(firstPoint.getId(), 0.0, 0.0);
  mapping.set2DCoords(secondPoint.getId(), firstPoint.distance(secondPoint), 0.0);

  open.push_back(std::pair<VertexID, VertexID>(firstPoint.getId(), secondPoint.getId()));

  while(open.size() != 0) {
    std::pair<VertexID, VertexID> edge = open.back();
    open.pop_back();

    // first triangle
    Triangle & t = mesh.findTriangle(edge.first, edge.second);
    if (t.getFlag() == 0) {
      if (!compute2DLocation(t, edge))
	++nbBad;
      VertexID otherPoint = t.getOtherPoint(edge.first, edge.second);
      open.push_back(std::pair<VertexID, VertexID>(otherPoint, edge.first));
      open.push_back(std::pair<VertexID, VertexID>(edge.second, otherPoint));
    }

    // other triangle if exists
    try {
      Triangle & t2 = mesh.findOtherTriangle(edge.first, edge.second, t.getId());
      if (t2.getFlag() == 0) {
	if (!compute2DLocation(t2, edge))
	  ++nbBad;
	VertexID otherPoint = t2.getOtherPoint(edge.first, edge.second);
	open.push_back(std::pair<VertexID, VertexID>(otherPoint, edge.first));
	open.push_back(std::pair<VertexID, VertexID>(edge.second, otherPoint));
      }
    }
    catch (Exception) {}
  }
  if (nbBad != 0)
    std::cout << indentManager << "Warning: " << nbBad << " points has been computed with a bad location" << std::endl;

  mapping.setFlatten();

  if (adjustMeanDistance)
    mapping.adjust2DLocationMeanDistance();
}

/**
   Compute 2D location using the law of sines (http://en.wikipedia.org/wiki/Law_of_sines)
*/
bool ABFMethod::compute2DLocation(Triangle & t, const std::pair<VertexID, VertexID> & edge) {
  bool result = true;
  VertexID otherPoint = t.getOtherPoint(edge.first, edge.second);

  Point2D & pt1 = mapping[edge.first];
  Point2D & pt2 = mapping[edge.second];
  Coord2D oOther = mapping[otherPoint];
  float angle1 = alpha[getAngleId(t.getId(), edge.second)];
  float angle2 = alpha[getAngleId(t.getId(), edge.first)];
  float angle3 = alpha[getAngleId(t.getId(), otherPoint)];

  minangle = 1e-20;
  maxangle = M_PI - minangle;
  if (angle3 > maxangle)
    angle3 = maxangle;
  if (angle3 < minangle)
    angle3 = minangle;

  double d3 = pt1.distance2D(pt2);
  double ratio = d3 / sin(angle3);

  double d1 = sin(angle1) * ratio;
  double d2 = sin(angle2) * ratio;

  compute2DCoordsUsingDistances(otherPoint, edge.first, edge.second, d1, d2, d3);

  if ((mapping.point3D(otherPoint).getFlag() != 0) && (oOther.distance2D(mapping[otherPoint]) > 0.1 * oOther.norm2D())) {
    result = false;
  }

  mapping.point3D(otherPoint).setFlag(1);

  t.setFlag(1);

  return result;
}

UnfoldingMethod * ABFMethod::clone(Mapping2D3D & mapping_l) {
  UnfoldingMethod * result = new ABFMethod(mapping_l);

  return result;
}

void ABFMethod::initDataUsing2D() {
  if (alpha != NULL)
    delete [] alpha;

  nfaces = mesh.getNbTriangles();
  nangles = nfaces * 3;
  alpha = new float[nangles];

  minangle = 7.5f * M_PI / 180.0f;
  maxangle = M_PI - minangle;

  for(Mesh::const_triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t) {
    TriangleID idA1 = (*t).getId() * 3;
    TriangleID idA2 = idA1 + 1;
    TriangleID idA3 = idA1 + 2;

    float a1, a2, a3;
    a1 = fabs(mapping[(*t).getP1()].angle2D(mapping[(*t).getP2()], mapping[(*t).getP3()]));
    a2 = fabs(mapping[(*t).getP2()].angle2D(mapping[(*t).getP1()], mapping[(*t).getP3()]));
    a3 = M_PI - a1 - a2;

    if (a1 < minangle)
      a1 = minangle;
    else if (a1 > maxangle)
      a1 = maxangle;

    if (a2 < minangle)
      a2 = minangle;
    else if (a2 > maxangle)
      a2 = maxangle;

    if (a3 < minangle)
      a3 = minangle;
    else if (a3 > maxangle)
      a3 = maxangle;

    alpha[idA1] = a1;
    alpha[idA2] = a2;
    alpha[idA3] = a3;
  }
}


WeightedEdges ABFSolver::getLengths(VertexID v1, VertexID v2, double length) const {
  WeightedEdges result(oMesh);
  double ratio = 1.;
  if ((v1 != v2) && (length > 0.))
    ratio = length / oMesh.point(v1).distance(oMesh.point(v2));

  for(Mesh::const_point_iterator p = oMesh.point_begin(); p != oMesh.point_end(); ++p)
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
      if ((*p).getId() < *nb) {
	const Triangle & t = oMesh.findTriangle((*p).getId(), *nb);
	float angle = alpha[getAngleId(t.getId(), t.getOtherPoint((*p).getId(), *nb))];

	minangle = 1e-20;
	maxangle = M_PI - minangle;
	if (angle > maxangle)
	  angle = maxangle;
	if (angle < minangle)
	  angle = minangle;

	result((*p).getId(), *nb) = ratio * sin(angle);
      }

  return result;
}

