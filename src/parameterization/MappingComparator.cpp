/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MappingComparator.h"

using namespace Taglut;

MappingComparator::MappingComparator(const Mapping2D3D & mapping_t, const Mapping2D3D & reference_t) : mapping(mapping_t), reference(reference_t), mMatcher(mapping_t.getMesh(), reference_t.getMesh()) {

}

double MappingComparator::sumSquareDistance2D() const {
  double result = 0;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double distance = mapping[(*mm).first].distance2D(reference[(*mm).second]);
    result += distance * distance;
  }

  return result;
}

double MappingComparator::meanSquareDistance2D() const {
  double result = 0;
  unsigned int nbPts = mMatcher.size();
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    double distance = mapping[(*mm).first].distance2D(reference[(*mm).second]);
    result += distance * distance / nbPts;
  }

  return result;
}

double MappingComparator::maxSquareDistance2D() const {
  double result = 0;
  for(MeshMatcher::const_iterator mm = mMatcher.begin(); mm != mMatcher.end(); ++mm) {
    const double distance = mapping[(*mm).first].distance2D(reference[(*mm).second]);
    const double d2 = distance * distance;
    if (d2 > result)
      result += d2;
  }

  return result;
}


double MappingComparator::useMethod(const std::string & method) const {
  if (method == "max-square-distance2D")
    return maxSquareDistance2D();
  else if (method == "mean-square-distance2D")
    return meanSquareDistance2D();
  else if (method == "sum-square-distance2D")
    return sumSquareDistance2D();
  else
    return 0.0;
}

bool MappingComparator::isKnownMethod(const std::string & method) {
  return ((method == "max-square-distance2D") ||
	  (method == "mean-square-distance2D") ||
	  (method == "sum-square-distance2D"));
}
