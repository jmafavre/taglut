/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <sstream>
#include <algorithm>

#include "Mapping2D3D.h"
#include "Messages.h"
#include "FileExceptions.h"
#include "SurfaceToolbox.h"
#include "ColorChooser.h"
#include "MeshManipulator.h"
#include "FileTools.h"
#include "MeshUncut.h"
#include "MeshPrune.h"
#include "Triangle.h"
#include "Length.h"
#include "PlaneSpliter.h"
#include "StringManipulation.h"
#include "PointOnEdge.h"
#include "PLY.h"

#ifdef USE_LIBXML
#include <libxml/xmlreader.h>
#endif

using namespace Taglut;

namespace Taglut {
  class CompRatio {
  private:
    const std::vector<double> & ratios;
  public:
    /** default constructor */
    CompRatio(const std::vector<double> & ratios_l) : ratios(ratios_l) { }

    /** copy constructor */
    CompRatio(const CompRatio & cp) : ratios(cp.ratios) {}

    /** binary operator */
    bool operator()(VertexID v1, VertexID v2) const {
      if ((v1 >= ratios.size()) || (v2 >= ratios.size()))
	throw Exception("Bad ID (CompRatio)");
      return ratios[v1] > ratios[v2];
    }
  };

}

void Mapping2D3D::addFormats() {
  addFormat("obj");
  addFormat("xml");
  addFormat("map");
  addFormat("ply");
  addSaveFormat("ums");
}


Mapping2D3D::Mapping2D3D() : clusterG() {
  addFormats();
  mesh = NULL;
  flatten = false;
}

Mapping2D3D::Mapping2D3D(Mesh & mesh_l) : mesh(&mesh_l) {
  addFormats();
  // first reorder triangles and neighbours
  mesh_l.reorient();

  for(Mesh::const_point_iterator p = mesh_l.point_begin(); p != mesh_l.point_end(); ++p)
    push_back((*p).getId());

  minXValue = std::numeric_limits<double>::max();
  maxXValue = -std::numeric_limits<double>::max();
  minYValue = std::numeric_limits<double>::max();
  maxYValue = -std::numeric_limits<double>::max();
  flatten = false;
}

Mapping2D3D::Mapping2D3D(const Mapping2D3D & mapping) : std::deque<Point2D>(mapping), FileManipulator(mapping), mesh(mapping.mesh) {
  addFormats();

  minXValue = mapping.minXValue;
  maxXValue = mapping.maxXValue;
  minYValue = mapping.minYValue;
  maxYValue = mapping.maxYValue;
  flatten = mapping.flatten;
}

Mapping2D3D & Mapping2D3D::operator=(const Mapping2D3D & mapping) {
  *( (std::deque<Point2D> *)this) = mapping;
  mesh = mapping.mesh; // added recently. Is it correct everywhere?

  minXValue = mapping.minXValue;
  maxXValue = mapping.maxXValue;
  minYValue = mapping.minYValue;
  maxYValue = mapping.maxYValue;
  flatten = mapping.flatten;
  clusterG = mapping.clusterG;

  return *this;
}

Mapping2D3D & Mapping2D3D::setPoints(const std::list<Point2D> & points) {
  clear();
  insert(begin(), points.begin(), points.end());
  flatten = true;
  reset2DExtrema();
  for(std::list<Point2D>::const_iterator p = points.begin(); p != points.end(); ++p)
      update2DExtrema(*p);
  return *this;
}


UnfoldingMethod & UnfoldingMethod::operator=(const UnfoldingMethod & uMethod) {
  mapping = uMethod.mapping;
  mesh = uMethod.mesh;

  return *this;
}

VertexID UnfoldingMethod::size() const { return mapping.size(); }


void Mapping2D3D::reinit() {
  (*this).clear();
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    push_back(Point2D((*p).getId()));

  flatten = false;
  reset2DExtrema();
}


void Mapping2D3D::resize2DCoords(double minX, double minY, double maxX, double maxY) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  /* first compute real min and max values */
  double rminX, rmaxX, rminY, rmaxY;
  double rX = (maxX - minX) / (maxXValue - minXValue);
  double rY = (maxY - minY) / (maxYValue - minYValue);
  if (rX < rY) {
    rminX = minX; rmaxX = maxX;
    rminY = (maxY - minY) / 2 - (maxYValue - minYValue) * rX / 2;
    rmaxY = (maxY - minY) / 2 + (maxYValue - minYValue) * rX / 2;
  }
  else {
    rminY = minY; rmaxY = maxY;
    rminX = (maxX - minX) / 2 - (maxXValue - minXValue) * rY / 2;
    rmaxX = (maxX - minX) / 2 + (maxXValue - minXValue) * rY / 2;
  }

  /* then update points' location */
  for (Mapping2D3D::iterator point = (*this).begin();
       point != (*this).end(); ++point) {
    (*point).set2DX(((*point).get2DX() - minXValue) * (rmaxX - rminX) / (maxXValue - minXValue) + rminX);
    (*point).set2DY(((*point).get2DY() - minYValue) * (rmaxY - rminY) / (maxYValue - minYValue) + rminY);
  }

  /* then update min and max values */
  minXValue = rminX;
  minYValue = rminY;
  maxXValue = rmaxX;
  maxYValue = rmaxY;
}


VertexID Mapping2D3D::getNearestPointId(double x2d, double y2d) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  Mapping2D3D::const_iterator select = (*this).begin();
  double length = (*select).distance2D(x2d, y2d);

  // find point nearest to this center
  for (Mapping2D3D::const_iterator point = (*this).begin(); point != (*this).end(); ++point) {
    double lengthTmp = (*point).distance2D(x2d, y2d);
    if (length > lengthTmp) {
      select = point;
      length = lengthTmp;
    }
  }

  return (*select).getId();
}


const Point2D3D Mapping2D3D::getNearestPoint(double x2d, double y2d) const {
  VertexID selected = getNearestPointId(x2d, y2d);
  return Point2D3D((*mesh).point(selected), (*this)[selected]);
}

const Point2D3D Mapping2D3D::getNearestPoint(double x3d, double y3d, double z3d) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  Mesh::const_point_iterator select = (*this).point3D_begin();
  double length = (*select).distance(x3d, y3d, z3d);
  bool inSlice = false;

  // find point nearest to this center
  for (Mesh::const_point_iterator point = (*this).point3D_begin(); point != (*this).point3D_end(); ++point) {
      double lengthTmp = (*point).distance(x3d, y3d, z3d);
    if (((*point).getZ() == z3d) && !inSlice) {
      select = point;
      length = lengthTmp;
      inSlice = true;
    }
    else if (!inSlice || ((*point).getZ() == z3d)) {
      if (length > lengthTmp) {
	select = point;
	length = lengthTmp;
      }
    }
  }

  return Point2D3D(*select, (*this)[(*select).getId()]);
}

void Mapping2D3D::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string & objectName) {
  if ((altFormat == "xml") || (altFormat == "map"))
    loadXML(fileName);
  else if (altFormat == "obj")
    loadOBJ(fileName, objectName);
  else if (altFormat == "ply")
    loadPLY(fileName);
  else
    throw Exception("Unknow format");
}


void Mapping2D3D::loadPLY(const std::string & fileName) {
    PLYLoader plyLoader(fileName);
    plyLoader.loadMapping2D3DFromPLY(*this);
    if (mesh != NULL)
        plyLoader.loadMeshFromPLY(*mesh);
}

bool Mapping2D3D::checkFormatPLY(std::ifstream & file) {
  std::string firstLine, secondLine;
  getline(file, firstLine);
  getline(file, secondLine);
  return (((firstLine == "ply") || (firstLine == "ply\r")) &&
	  ((secondLine == "format ascii 1.0") || (secondLine == "format ascii 1.0\r")));
}

void Mapping2D3D::loadOBJ(const std::string & fileName, const std::string & objectName) {

  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (objectName != "")
    if (!findObjectOBJ(infile, objectName))
      throw ExceptionErrorDuringReadingFile();

  if (!loadOBJ(infile))
    throw ExceptionErrorDuringReadingFile();

  infile.close();

}


bool Mapping2D3D::findObjectOBJ(std::ifstream & file, const std::string & objectName) {
  std::string buf;

  while(!file.eof()) {
    file >> buf;
    if (buf == "g") {
      file >> buf;
      if (buf == objectName)
	return true;
    }
  }
  return false;
}

bool Mapping2D3D::loadOBJ(std::ifstream & file) {
  std::string buf;
  bool idTr = false;
  std::deque<Point3D> pts3d;
  std::deque<Triangle> triangles;

  // clear current object
  (*mesh).clear();
  clear();

  while(!file.eof()) {
    file >> buf;
    if (buf == "v") {
      // a point
      double x, y, z;
      file >> x >> y >> z;
      pts3d.push_back(Point3D(x, y, z, pts3d.size()));
    }
    else if (buf == "vt") {
      // a 2d point
      Point2D p2D;
      double x, y;
      getline(file, buf);

      std::deque<std::string> params = StringManipulation::splitString(buf, ' ');
      x = y = std::numeric_limits<double>::max();
      for(std::deque<std::string>::const_iterator p = params.begin(); p != params.end(); ++p)
	if (*p != "") {
	  if (!StringManipulation::isSignedFloat(*p))
	    throw ExceptionErrorDuringReadingFile();

	  if (x == std::numeric_limits<double>::max())
	    x = StringManipulation::getFloat(*p);
	  else {
	    y = StringManipulation::getFloat(*p);
	    break;
	  }
	}
      if ((x == std::numeric_limits<double>::max()) || (y == std::numeric_limits<double>::max()))
	throw ExceptionErrorDuringReadingFile();

      // set point
      p2D.set2DX(x);
      p2D.set2DY(y);
      p2D.setId(size());
      update2DExtrema(p2D);

      // add point to the mapping
      push_back(p2D);

    }
    else if (buf == "f") {
      // a face
      VertexID pts[4];
      getline(file, buf);
      std::deque<std::string> params = StringManipulation::splitString(buf, ' ');

      unsigned int i = 0;
      for(std::deque<std::string>::const_iterator p = params.begin(); p != params.end(); ++p)
	if (*p != "") {
	  if (i == 4)
	    throw ExceptionErrorDuringReadingFile();
	  std::string id = StringManipulation::splitString(*p, '/')[0];
	  if (!StringManipulation::isUnsignedInt(id))
	    throw ExceptionErrorDuringReadingFile();
	  pts[i] = StringManipulation::getVertexID(id);
	  ++i;
      }
      triangles.push_back(Triangle(pts[0], pts[1], pts[2], triangles.size()));
      if (i == 4) {
	triangles.push_back(Triangle(pts[0], pts[2], pts[3]));
      }
    }
    else {
      // skip line
      getline(file, buf);
    }
  }

  // if needed, translate vertex ids in triangles
  VertexID nbPoints = size();
  for(std::deque<Triangle>::const_iterator t = triangles.begin(); t != triangles.end(); ++t)
    if (((*t).getP1() >= nbPoints) || ((*t).getP2() >= nbPoints) || ((*t).getP3() >= nbPoints)) {
      idTr = true;
      break;
    }
  if (idTr)
    for(std::deque<Triangle>::iterator t = triangles.begin(); t != triangles.end(); ++t) {
      (*t).setP1((*t).getP1() - 1);
      (*t).setP2((*t).getP2() - 1);
      (*t).setP3((*t).getP3() - 1);
    }


  // if needed, add triangles (and neighbours relations) to points
  for(std::deque<Triangle>::const_iterator i = triangles.begin(); i != triangles.end(); ++i) {
    pts3d[(*i).getP1()].addTriangle((*i).getP2(), (*i).getP3(), (*i).getId());
    pts3d[(*i).getP2()].addTriangle((*i).getP1(), (*i).getP3(), (*i).getId());
    pts3d[(*i).getP3()].addTriangle((*i).getP1(), (*i).getP2(), (*i).getId());
  }

  (*mesh).setPointsAndbuildMesh(pts3d, triangles);


  if (size() == (*mesh).getNbPoints())
    flatten = true;
  else {
    if(size() == 0)
      std::cout << "Warning: no UV data available" << std::endl;
    else {
      std::cout << "Warning: UV data not fully available, cleaning." << std::endl;
      clear();
    }
    reinit();
  }
  return true;
}


void Mapping2D3D::saveAltFormat(const std::string & fileName, const std::string & format, const std::string & objectName) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");
  if ((*this).size() == 0)
    throw ExceptionEmptyData();

  std::ofstream outFile(fileName.c_str(), std::ios::out);

  if (!outFile.is_open())
    throw ExceptionCannotOpenOutputFile();

  if(format == "ums")
    outFile << toStringUMS();
  else if(format == "obj") {
    outFile << "# Generated by saveOBJ" << std::endl << std::endl;
    if (objectName != "")
      outFile << "# " << objectName << std::endl << std::endl;;

    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
      outFile << "v " << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;

    outFile << std::endl;

    for(Mapping2D3D::const_iterator p = begin(); p != end(); ++p)
      outFile << "vt " << (*p).get2DX() << " " << (*p).get2DY() << std::endl;

    outFile << std::endl;

    for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
      outFile << "f " << ((*t).getP1() + 1) << "/" << ((*t).getP1() + 1) << " " << ((*t).getP2() + 1) << "/" << ((*t).getP2() + 1) << " " << ((*t).getP3() + 1) << "/" << ((*t).getP3() + 1) << std::endl;
  }
  else if (format == "ply") {
      PLYSaver plysaver;

      plysaver.savePLY(*this, fileName);
  }
  else if ((format == "xml") || (format == "map"))
    outFile <<  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl << toStringXML();
  else
    throw Exception("Unknow format");

  outFile.close();
}



std::string Mapping2D3D::toStringUMS() const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  std::ostringstream result;
  std::deque<std::pair<VertexID, VertexID> > singleEdges;
  std::vector<int> flags;

  VertexID nbPoint3D = 0;
  MeshManipulator mManip((*mesh));

  unsigned int nbCC = mManip.computeFlagsUsingCC(flags);

  std::ostringstream pointsTxt;
  VertexID id = 0;
  // first compute 2d/3d correspondance, and compute number of 3d points. Set point flag = 0
  for(Mesh::const_point_iterator c = point3D_begin(); c != point3D_end(); ++c) {
    if (flags[(*c).getId()] < (int)nbCC) {
      std::deque<VertexID> samePoints = mManip.getPointsSameLocation((*c).getId());
      ++nbPoint3D;
      pointsTxt << (*mesh).point(id).getX() << " " <<  (*mesh).point(id).getY() << " " << (*mesh).point(id).getZ();
      if (id == 0)
	pointsTxt << " // 3d location";
      pointsTxt << std::endl;
      pointsTxt << " " << samePoints.size();
      if (id == 0)
	pointsTxt << " // number of associated 2d points";
      pointsTxt << std::endl;
      for(std::deque<VertexID>::const_iterator c2 = samePoints.begin(); c2 != samePoints.end(); ++c2) {
	const Point2D & cc2 = (*this)[*c2];
	const Point3D & cc23D = point3D(*c2);
	pointsTxt << " " << cc2.getId() << " " << cc2.get2DX() << " " << cc2.get2DY() << " " << flags[cc23D.getId()];
	if ((id == 0) && c2 == samePoints.begin())
	  pointsTxt << " // id of the point, 2d location and conneted component id";
	pointsTxt << std::endl;
	flags[cc23D.getId()] += nbCC;
      }
    }
    flags[(*c).getId()] = 0;
    ++id;
  }

  // compute single edges
  for(Mesh::const_point_iterator c = point3D_begin(); c != point3D_end(); ++c) {
    flags[(*c).getId()] = 1;
    for(std::deque<VertexID>::const_iterator nbC = (*c).getNeighbours().begin(); nbC != (*c).getNeighbours().end(); ++nbC)
      try {
	(*mesh).findTriangle((*c).getId(), *nbC);
      }
      catch (Exception e) {
	if (flags[*nbC] != 1)
	  singleEdges.push_back(std::pair<VertexID, VertexID>((*c).getId(), *nbC));
      }
  }


  // then writing into file creating string
  result << nbPoint3D << " // number of 3d points" << std::endl;
  result << size() << " // number of 2d points" << std::endl;
  result << nbCC << " // number of connected components" << std::endl;
  result << pointsTxt.str();

  // then adding into file triangles
  result << (*mesh).getNbTriangles() << " // number of triangles" << std::endl;

  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    result << (*t).getP1() << " " << (*t).getP2() << "  " << (*t).getP3();
    if (t == (*mesh).triangle_begin())
      result << " // id of each 2d point";
    result << std::endl;
  }

  if (singleEdges.size() != 0) {
    result << singleEdges.size() << " // number of single edges" << std::endl;
    for(std::deque<std::pair<VertexID, VertexID> >::const_iterator e = singleEdges.begin(); e != singleEdges.end(); ++e) {
      result << (*e).first << " " << (*e).second;
      if (e == singleEdges.begin())
	result << " // id of each 2d point";
      result << std::endl;
    }
  }

  return result.str();
}


void Mapping2D3D::computeUnfoldedCImgPointsRedim(CImgList<double> & uPoints,
					  double minX, double minY, double maxX, double maxY, double z) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  resize2DCoords(minX, minY, maxX, maxY);

  computeUnfoldedCImgPoints(uPoints, z);
}

void Mapping2D3D::computeUnfoldedCImgPoints(CImgList<double> & uPoints, double z) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  CImg<double> pCImg = CImg<double>::vector(0., 0., 0.);
  uPoints.clear();

  for(Mapping2D3D::const_iterator point = (*this).begin(); point != (*this).end(); ++point) {
    pCImg(0) = (*point).get2DX();
    pCImg(1) = (*point).get2DY();
    pCImg(2) = z;
    uPoints.insert(pCImg);
  }

}

void Mapping2D3D::computeUnfoldedCImgPointsRedim(CImgList<double> & uPoints, double z) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  double min  = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p) {
    if((*p).getX() < min) min = (*p).getX();
    if((*p).getX() > max) max = (*p).getX();
    if((*p).getY() < min) min = (*p).getY();
    if((*p).getY() > max) max = (*p).getY();
    if((*p).getZ() < min) min = (*p).getZ();
    if((*p).getZ() > max) max = (*p).getZ();

  }

  computeUnfoldedCImgPointsRedim(uPoints, min, min, max, max, z);

}


void Mapping2D3D::computeClusters(double limitRatio, int nbClusters, const enum ClusterGenerator::ClusteringMethod & cMethod, double maxCurv, const IndentManager iManager) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  assert(((*mesh).getNbPoints() == size()) && ((*mesh).getNbPoints() != 0));
  std::vector<VertexID> centers;

  double * ratios;
  ratios = new double[(*mesh).getNbPoints()];



  double min = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  Mapping2D3D::const_iterator ci = begin();

  VertexID i = 0;
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++i) {
    double ratio = 0;
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin();
	nb != (*p).getNeighbours().end(); ++nb) {
      ratio += (*p).distance((*mesh).point(*nb)) / (*ci).distance2D((*this)[*nb]);
    }
    ++ci;
    ratios[i] = ratio;
    if (ratio > max) max = ratio;
    if (ratio < min) min = ratio;
  }


  clusterG.setData(mesh, ratios);

  // build center list

  i = 0;
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++i) {
    bool localMin = true;
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin();
	nb != (*p).getNeighbours().end(); ++nb) {
      assert(*nb < (*mesh).getNbPoints());
      if (ratios[*nb] > ratios[i]) {
	localMin = false;
	break;
      }
    }

    if (localMin)
      centers.push_back(i);
  }

  std::cout << iManager << " Number of centers before computing clusters: " << centers.size() << std::endl;

  clusterG.clustering(centers, cMethod, maxCurv);

  std::cout << iManager << " Number of cluster before removing smallest: " << clusterG.getNbClusters() << std::endl;
  if (limitRatio > 0)
    clusterG.removeSmallRatioClustersByRatio(limitRatio);
  if (nbClusters > 0)
    clusterG.removeSmallRatioClusters(nbClusters);
  std::cout << iManager << " Number of cluster after removing: " << clusterG.getNbClusters() << std::endl;

  delete [] ratios;


  // then set extrema as cluster-center
  {
    extrema.clear();
    MeshManipulator mmanip((*mesh));
    std::vector<std::vector<VertexID> > clusters = clusterG.getClusters();

    for(std::vector<std::vector<VertexID> >::const_iterator cluster = clusters.begin();
	cluster != clusters.end(); ++cluster) {
      double x = 0;
      double y = 0;
      unsigned int size_l = (*cluster).size();
      assert(size_l != 0);
      for(std::vector<VertexID>::const_iterator p = (*cluster).begin();
	  p != (*cluster).end(); ++p) {
	x += (*this)[*p].get2DX();
	y += (*this)[*p].get2DY();
      }
      extrema.push_back(getNearestPointId(x / size_l, y / size_l));
    }
  }

}


UCharCImg Mapping2D3D::clustersToImage(unsigned int sizex, unsigned int sizey) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned char grey = 50;
  unsigned int nbClusters = clusterG.getNbClusters();

  // build image
  UCharCImg result(sizex, sizey, 1, 3);
  result.fill(0);

  // draw triangles
  toImageDrawBorderTriangles(result, grey);

  // then draw colored points
  clusterG.setFlagsOnMesh();
  std::vector<Point3D>::const_iterator p = (*mesh).point_begin();
  for(Mapping2D3D::const_iterator c = begin(); c != end(); ++c, ++p) {
    unsigned int flag = (*p).getFlag();
    if (flag != 0) {
      result.draw_point((unsigned int)((*c).get2DX()), (unsigned int)((*c).get2DY()), ColorChooser::getColorFromId(flag, nbClusters)._data);
    }
  }

  return result;

}

UCharCImg Mapping2D3D::extremaToImage(unsigned int sizex, unsigned int sizey) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned char grey = 50;
  UCharCImg result(sizex, sizey, 1, 3);
  result.fill(0);

  // draw triangles
  toImageDrawBorderTriangles(result, grey);

  // then draw colored points
  toImageDrawPoints(result, extrema);

  return result;
}


void Mapping2D3D::toImageDrawPoints(UCharCImg & outImage, const std::deque<VertexID> & points) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned int id = 0;
  for(std::deque<VertexID>::const_iterator i = points.begin(); i != points.end(); ++i, ++id)
    if (*i < size())
      outImage.draw_point((unsigned int)((*this)[*i].get2DX()), (unsigned int)((*this)[*i].get2DY()), ColorChooser::getColorFromId(id, points.size())._data);

}





void Mapping2D3D::clearClusters() {
  clusterG.clear();
}


void Mapping2D3D::updatePoints(const MeshPathes & mPathes) {
  VertexID id = size();
  VertexID idMesh = (*mesh).getNbPoints();
  MeshManipulator mManip((*mesh));
  std::deque<VertexID> openSet;
  std::map<VertexID, VertexID> projection;

  // init flags
  for(Mesh::point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i)
    (*i).setFlag(0);

  // Adding new points
  while(id != idMesh) {
    VertexID proj = mPathes.getProjection(id);
    push_back(Point2D(id));
    back().set2DX((*this)[proj].get2DX());
    back().set2DY((*this)[proj].get2DY());
    ++id;
  }

}



void Mapping2D3D::computeTriangleRatios(CImgList<unsigned char> & ratios) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  std::list<double> values;
  ratios.clear();
  UCharCImg r(3);
  double min = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  for(Mesh::triangle_iterator i = (*mesh).triangle_begin(); i != (*mesh).triangle_end(); ++i)
    if ((*mesh).point((*i).getP1()).getIsRealPoint() &&
	(*mesh).point((*i).getP2()).getIsRealPoint() &&
	(*mesh).point((*i).getP3()).getIsRealPoint()) {
      double surfaceRatio =
	SurfaceToolbox::surface((*mesh).point((*i).getP1()),
				(*mesh).point((*i).getP2()),
				(*mesh).point((*i).getP3())) /
	SurfaceToolbox::surface((*this)[(*i).getP1()],
				(*this)[(*i).getP2()],
				(*this)[(*i).getP3()]);
      if (surfaceRatio < min) min = surfaceRatio;
      if (surfaceRatio > max) max = surfaceRatio;
      values.push_front(surfaceRatio);
    }
  values.sort();
  std::list<double>::const_iterator idI = values.begin();
  for(unsigned int i = 0; i< values.size() / 2; ++i) ++idI;

  double median = *idI;

  for(Mesh::triangle_iterator i = (*mesh).triangle_begin(); i != (*mesh).triangle_end(); ++i)
    if ((*mesh).point((*i).getP1()).getIsRealPoint() &&
	(*mesh).point((*i).getP2()).getIsRealPoint() &&
	(*mesh).point((*i).getP3()).getIsRealPoint()) {
      double surfaceRatio =
	SurfaceToolbox::surface((*mesh).point((*i).getP1()),
				(*mesh).point((*i).getP2()),
				(*mesh).point((*i).getP3())) /
	SurfaceToolbox::surface((*this)[(*i).getP1()],
				(*this)[(*i).getP2()],
				(*this)[(*i).getP3()]);

      r(0) = r(1) = r(2) = 255;
      if (surfaceRatio < 1)
	r(0) -= (unsigned char) ((median - surfaceRatio) * 255 / median);
      else
	r(2) -= (unsigned char) ((surfaceRatio - median) * 255 / (max - median));

      ratios.insert(r);
    }
    else {
      r(0) = r(1) = r(2) = 0;
      ratios.insert(r);
    }


}

Coord2D Mapping2D3D::compute2DBarycenter() const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  double x = 0;
  double y = 0;
  for(Mapping2D3D::const_iterator c = begin(); c != end(); ++c) {
    x += (*c).get2DX();
    y += (*c).get2DY();
  }


  if (size() == 0) {
    return Coord2D();
  }
  else {
    Coord2D result(x, y);

    result.set2DX(x / size());
    result.set2DY(y / size());

    return result;
  }
}

Coord2D Mapping2D3D::compute2DBarycenter(const std::deque<VertexID> & points) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  double x = 0;
  double y = 0;
  for(std::deque<VertexID>::const_iterator c = points.begin(); c != points.end(); ++c) {
    x += (*this)[*c].get2DX();
    y += (*this)[*c].get2DY();
  }


  if (points.size() == 0) {
    return Coord2D();
  }
  else {
    Coord2D result(x, y);

    result.set2DX(x / points.size());
    result.set2DY(y / points.size());

    return result;
  }

}


std::string Mapping2D3D::helpAdjustMapping() {
  Message result;
  result << "Available post-treatments:";
  result << "- \"0\" (default value): no post-treatment;";
  result << "- \"1\": local optimisation for all points (except boundary points).";
  result << "Available post-treatments option:";
  result << "- \"epsilon\": allowed error value.";
  result << "- \"max-iteration\": maximum iteration allowed.";

  return result.toString();
}



unsigned int Mapping2D3D::adjustMapping(unsigned int idMethod, unsigned int maxIt, double epsilon, IndentManager &) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  if (idMethod == 1)
    return adjustMapping1(maxIt, epsilon);
  return 0;
}

unsigned int Mapping2D3D::adjustMapping1(unsigned int postTreatmentIt, double epsilon) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  std::deque<std::deque<double> > weights;


  // first compute weights
  for (Mesh::const_point_iterator point = (*mesh).point_begin();
       point != (*mesh).point_end(); ++point) {
    weights.push_back(std::deque<double>());
    if (!(*point).getIsBoundary()) {
      double sum = 0;
      for(std::deque<VertexID>::const_iterator nb = (*point).getNeighbours().begin();
	  nb != (*point).getNeighbours().end(); ++nb) {
	double l = (*point).distance((*mesh).point(*nb));
	weights.back().push_back(l);
	sum += l;
      }
      for(std::deque<double>::iterator w = weights.back().begin(); w != weights.back().end(); ++w)
	(*w) /= sum;
    }
  }

  double diff;
  unsigned int step = 0;
  for(step = 0; step < postTreatmentIt; ++step) {
    std::deque<std::deque<double> >::const_iterator w = weights.begin();
    diff = 0;
    Mesh::const_point_iterator point3D_l = (*mesh).point_begin();
    for (Mapping2D3D::iterator point = (*this).begin();
	 point != (*this).end(); ++point, ++point3D_l) {
      if (!(*point3D_l).getIsBoundary()) {
	std::deque<double>::const_iterator wi = (*w).begin();
	double x = 0;
	double y = 0;
	for(std::deque<VertexID>::const_iterator nb = (*point3D_l).getNeighbours().begin();
	    nb != (*point3D_l).getNeighbours().end(); ++nb) {
	  x += (*this)[*nb].get2DX() * (*wi);
	  y += (*this)[*nb].get2DY() * (*wi);
	  ++wi;
	}
	if ((fabs((*point).get2DX() - x) > diff)) diff = fabs((*point).get2DX() - x);
	if ((fabs((*point).get2DY() - y) > diff)) diff = fabs((*point).get2DY() - y);
	(*point).set2DX(x);
	(*point).set2DY(y);
      }
      ++w;
    }
    if (epsilon >= diff)
      break;
  }

  return step;
}



std::string Mapping2D3D::toStringXML() const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  std::ostringstream result;
  result.precision(2);


  result << "<mapping>" << std::endl;

  result.precision(20);

  Mesh::const_point_iterator point3D_l = (*mesh).point_begin();
  for (Mapping2D3D::const_iterator point = (*this).begin(); point != (*this).end(); ++point, ++point3D_l) {
    result << "\t<point id=\"" << (*point3D_l).getId() << "\""
	   << " x=\"" << (*point3D_l).getX() << "\""
	   << " y=\"" << (*point3D_l).getY() << "\""
	   << " z=\"" << (*point3D_l).getZ() << "\""
	   << " x2d=\"" << (*point).get2DX() << "\""
	   << " y2d=\"" << (*point).get2DY() << "\""
	   << " boundary=\"" << (*point3D_l).getIsBoundary() << "\" />" << std::endl;
  }

  for(Mesh::triangle_iterator i = (*mesh).triangle_begin(); i != (*mesh).triangle_end(); ++i)
    result << "\t<triangle id=\"" << (*i).getId() << "\""
	   << " idP1=\"" << (*i).getP1() << "\""
	   << " idP2=\"" << (*i).getP2() << "\""
	   << " idP3=\"" << (*i).getP3() << "\""
	   << " />" << std::endl;

  result << "</mapping>" << std::endl;
  return result.str();
}

void Mapping2D3D::loadXML(const std::string & filename) {
#ifdef LIBXML_READER_ENABLED
  xmlTextReaderPtr reader;
  int ret = 0;

  reader = xmlReaderForFile(filename.c_str(), NULL, 0);
  (*this).clear();

  if (reader == NULL)
    throw ExceptionFileNotFound();
  ret = xmlTextReaderRead(reader);

  // first find file version
  bool mp = (std::string("mapping") == (const char *) xmlTextReaderConstName(reader));
  bool mpl = (std::string("mappinglist") == (const char *) xmlTextReaderConstName(reader));

  if (ret != 1)
    throw ExceptionErrorDuringReadingFile();
  if (!mp && !mpl)
    throw ExceptionErrorDuringReadingFile();

  // if no child
  if (xmlTextReaderIsEmptyElement(reader) != 0)
    throw ExceptionErrorDuringReadingFile();

  // else jump next child if needed
  if (mpl) {
    do {
      ret = xmlTextReaderRead(reader);
      if (ret == 0)
	throw ExceptionErrorDuringReadingFile();
    } while (std::string("mapping") != (const char *) xmlTextReaderConstName(reader));
  }

  // then load mapping
  loadXML(reader);

  // and clear xml reader
  xmlFreeTextReader(reader);

  return;
#else
  throw ExceptionXMLReaderNotAvailable(filename + " not opened");
#endif
}


#ifdef LIBXML_READER_ENABLED
void Mapping2D3D::loadXML(xmlTextReaderPtr & reader) {
  double x, y, z, x2d, y2d;
  VertexID id = 0, idP1, idP2, idP3;
  bool isBoundary = false;
  int ret = xmlTextReaderRead(reader); // reading next node
  std::deque<Triangle> tList;
  std::deque<Point3D> oPoints;
  Point3D p3D;
  Point2D p2D;
  Triangle t;

  // read data
#ifndef NDEBUG
  VertexID cpt = 0;
#endif

  reset2DExtrema();

  while (ret != 0) {
    if (std::string("point") == (const char *) xmlTextReaderConstName(reader)) {
      // read coords
      x = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"x"));
      y = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"y"));
      z = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"z"));
      // read 2d coords
      x2d = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"x2d"));
      y2d = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"y2d"));
      isBoundary = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"boundary"));
      id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));
      // set values
      p2D.setId(id);
      p3D.setId(id);
      p3D.setX(x);
      p3D.setY(y);
      p3D.setZ(z);
      p2D.set2DX(x2d);
      p2D.set2DY(y2d);
      p3D.setIsBoundary(isBoundary);
      // reinit point info
      p3D.clearNeighboursAndTriangles();
      update2DExtrema(p2D);

      // add point to the mapping
      push_back(p2D);
      oPoints.push_back(p3D);
    }
    else if (std::string("triangle") == (const char *) xmlTextReaderConstName(reader)) {
      // read coords
      idP1 = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"idP1"));
      idP2 = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"idP2"));
      idP3 = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"idP3"));
      id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));

      // check if file order is ok
      assert(id == cpt++);

      // build triangle and add it to list
      t.setId(id);
      t.setPoints(idP1, idP2, idP3);
      tList.push_back(t);
      assert(idP1 < size());
      assert(idP2 < size());
      assert(idP3 < size());
    }
    else if (std::string("mapping") == (const char *) xmlTextReaderConstName(reader))
      break;

    ret = xmlTextReaderRead(reader);
  }

  if (ret == 0)
    throw ExceptionErrorDuringReadingFile();

  // all data is loaded, we can now compute others needed infos (triangles, mesh, etc)

  // if needed, add triangles (and neighbours relations) to points
  for(std::deque<Triangle>::const_iterator i = tList.begin(); i != tList.end(); ++i) {
    oPoints[(*i).getP1()].addTriangle((*i).getP2(), (*i).getP3(), (*i).getId());
    oPoints[(*i).getP2()].addTriangle((*i).getP1(), (*i).getP3(), (*i).getId());
    oPoints[(*i).getP3()].addTriangle((*i).getP1(), (*i).getP2(), (*i).getId());
  }

  // then set mesh data
  (*mesh).setPointsAndbuildMesh(oPoints, tList);

  flatten = true;
}


#endif

void  Mapping2D3D::update2DExtrema(const Coord2D & pt) {
  if (pt.get2DX() > maxXValue) maxXValue = pt.get2DX();
  if (pt.get2DX() < minXValue) minXValue = pt.get2DX();
  if (pt.get2DY() > maxYValue) maxYValue = pt.get2DY();
  if (pt.get2DY() < minYValue) minYValue = pt.get2DY();
}





void Mapping2D3D::reset2DExtrema() {
  minXValue = std::numeric_limits<double>::max();
  maxXValue = -std::numeric_limits<double>::max();
  minYValue = std::numeric_limits<double>::max();
  maxYValue = -std::numeric_limits<double>::max();
}

void Mapping2D3D::translate(const Coord2D & tr) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  reset2DExtrema();
  for(Mapping2D3D::iterator c = begin(); c != end(); ++c) {
    *((Coord2D *) &(*c)) += tr;
    update2DExtrema(*c);
  }
}

void Mapping2D3D::rotate(double angle, const Coord2D & center) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  reset2DExtrema();
  for(Mapping2D3D::iterator c = begin(); c != end(); ++c) {
    (*c).rotate(angle, center);
    update2DExtrema(*c);
  }
}

void Mapping2D3D::scale(double ratio, const Coord2D & center) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  reset2DExtrema();
  for(Mapping2D3D::iterator c = begin(); c != end(); ++c) {
    *((Coord2D *) &(*c)) = center + ((*((Coord2D *) &(*c))) - center) * ratio;
    update2DExtrema(*c);
  }
}

void Mapping2D3D::flip() {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  reset2DExtrema();
  for(Mapping2D3D::iterator c = begin(); c != end(); ++c) {
    (*c).set2DX(-(*c).get2DX());
    update2DExtrema(*c);
  }
}

std::string Mapping2D3D::getBox2DSize() const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  std::ostringstream result;

  double diff = maxXValue - minXValue;
  double diffy = maxYValue - minYValue;
  if (diffy > diff) diff = diffy;

  result << " Box((" << minXValue << ", " << minYValue << "), ("
	 << maxXValue << ", " << maxYValue << ")); max: " << diff;

  return result.str();
}

void Mapping2D3D::compute2DLocation(MeshPrune & meshPrune, const Mapping2D3D & mapping) {
  TriangleID nbtriangles = 0;
  assert(meshPrune.getNbOrignalPoints() == size());

  std::deque<MultiTriangle> & mTriangles = meshPrune.getMultiTriangles();
  (*mesh).setPointFlag(0);

  for(std::deque<MultiTriangle>::const_iterator t = mTriangles.begin(); t != mTriangles.end(); ++t) {
    // compute location for vertices
    for(unsigned int i = 0; i < 3; ++i) {
      VertexID idP = (*t).getVertexId(i);
      if ((*mesh).point(idP).getFlag() == 0) {
	(*mesh).point(idP).setFlag(1);
	(*this)[idP].set2DCoords(mapping[meshPrune.getNewId(idP)]);
      }
    }

    // and compute location for points on edges
    for(unsigned int i = 0; i < 3; ++i) {
      const MultiEdge & mEdge = (*t).getEdge(i);
      if (mEdge.hasMiddlePoint()) {
	VertexID idM = mEdge.getMiddleId();
	(*mesh).point(idM).setFlag(1);
	Coord2D coords = (*mesh).computeBarycenterCoords(idM, mEdge.getFirstId(), mEdge.getLastId());
	compute2DLocationBarycenterCoords(coords, idM, mEdge.getFirstId(), mEdge.getLastId());
      }
    }

    // then compute location for points inside the multitriangle
    for(std::deque<TriangleID>::const_iterator stid = (*t).getTriangles().begin(); stid != (*t).getTriangles().end(); ++stid) {
      ++nbtriangles;
      const Triangle & st = (*mesh).triangle(*stid);
      for(unsigned int i = 0; i < 3; ++i) {
	VertexID idP = st.getVertexId(i);
	if ((*mesh).point(idP).getFlag() == 0) {
	  (*mesh).point(idP).setFlag(1);
	  Coord3D coords = (*mesh).computeBarycenterCoords(idP, (*t).getP1(), (*t).getP2(), (*t).getP3());
	  compute2DLocationBarycenterCoords(coords, idP, (*t).getP1(), (*t).getP2(), (*t).getP3());
	}
      }
    }
  }


  VertexID nbPO = 0;
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
    if ((*p).getFlag() == 0)
      ++nbPO;

  if (nbPO != 0) {
    std::cout << "Warning, multiscale process: " << nbPO << " points cannot be located." << std::endl;
    throw Exception("compute2DLocation(2): found a point outside of the triangles");
  }
}



void Mapping2D3D::compute2DLocationBarycenterCoords(const Coord2D & coords, VertexID idP, VertexID idP1, VertexID idP2) {
  const double epsilon = 0.00000000001;
  if (idP1 == idP2)
    throw Exception("compute2DLocationBarycenterCoords(5): wrong barycenter coordinates; (idP1, idP2) is not an edgee");
  if (fabs(coords.get2DX() + coords.get2DY() - 1) > epsilon) {
    std::cout << coords << std::endl;
    throw Exception("compute2DLocationBarycenterCoords(4): wrong barycenter coordinates");
  }

  double x = (*this)[idP1].get2DX() * coords.get2DX() + (*this)[idP2].get2DX() * coords.get2DY();
  double y = (*this)[idP1].get2DY() * coords.get2DX() + (*this)[idP2].get2DY() * coords.get2DY();
  (*this)[idP].set2DCoords(x, y);
}

void Mapping2D3D::compute2DLocationBarycenterCoords(const Coord3D & coords, VertexID idP, VertexID idP1, VertexID idP2, VertexID idP3) {
  const double epsilon = 0.00000000001;
  if ((idP1 == idP2) || (idP2 == idP3) || (idP3 == idP1))
    throw Exception("compute2DLocationBarycenterCoords(5): wrong barycenter coordinates; (idP1, idP2, idP3) is not a triangle");
  if (fabs(coords.getX() + coords.getY() + coords.getZ() - 1) > epsilon) {
    throw Exception("compute2DLocationBarycenterCoords(5): wrong barycenter coordinates");
  }

  double x = (*this)[idP1].get2DX() * coords.getX() + (*this)[idP2].get2DX() * coords.getY() + (*this)[idP3].get2DX() * coords.getZ();
  double y = (*this)[idP1].get2DY() * coords.getX() + (*this)[idP2].get2DY() * coords.getY() + (*this)[idP3].get2DY() * coords.getZ();
  (*this)[idP].set2DCoords(x, y);
}


void Mapping2D3D::set2DCoords(VertexID idPoint, double x, double y) {
  assert(idPoint < size());

  (*this)[idPoint].set2DCoords(x, y);

  update2DExtrema((*this)[idPoint]);
}

void Mapping2D3D::reset2DCoords() {
  reset2DExtrema();
}

void Mapping2D3D::buildTexturedTriangleList(const UCharCImg & image, CImgList<VertexID> & primitives, CImgList<unsigned char> & textures, double zratio) {

  (*mesh).buildTexturedTriangleList(image, primitives, textures, zratio);
}


void Mapping2D3D::buildTexturedTriangleList(const UCharCImg & image, CImgList<VertexID> & primitives) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  CImg<VertexID> primitive(9);

  // resize circle set (2d location) using textured image
  resize2DCoords(0, 0, image._width - 1, image._height - 1);


  // then build texture list
  primitives.clear();
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    VertexID idP1 = (*t).getP1();
    VertexID idP2 = (*t).getP2();
    VertexID idP3 = (*t).getP3();
    primitive[0] = idP1;
    primitive[1] = idP2;
    primitive[2] = idP3;

    primitive[3] = (VertexID)(*this)[idP1].get2DX();
    primitive[4] = (VertexID)(*this)[idP1].get2DY();

    primitive[5] = (VertexID)(*this)[idP2].get2DX();
    primitive[6] = (VertexID)(*this)[idP2].get2DY();

    primitive[7] = (VertexID)(*this)[idP3].get2DX();
    primitive[8] = (VertexID)(*this)[idP3].get2DY();

    primitives.insert(primitive);
  }
}

void Mapping2D3D::toImageDrawDistortions(UCharCImg & outImage) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  CImg<VertexID> primitive(9);

  // resize circle set (2d location) using textured image
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  // compute the distortion ratio for each triangle of the mapping
  std::vector<double> ratio;
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    double a3d = Coord3D::computeTriangleArea((*mesh).point((*t).getP1()), (*mesh).point((*t).getP2()), (*mesh).point((*t).getP3()));
    double a2d = Coord2D::compute2DTriangleArea((*this)[(*t).getP1()], (*this)[(*t).getP2()], (*this)[(*t).getP3()]);
    ratio.push_back(a2d / a3d);
  }

  // compute median
  std::vector<double> ratio2(ratio);
  std::sort(ratio2.begin(), ratio2.end());
  double median = ratio2[ratio2.size() / 2];
  double min = ratio2.front() / median;
  double max = ratio2.back() / median;

  // readjust ratio according to the median
  long double sdev = 0.0;
  long double mean = 0.0;
  for(std::vector<double>::iterator r = ratio.begin(); r != ratio.end(); ++r) {
    *r /= median;
    sdev += *r * *r;
    mean += *r;
  }
  sdev /= ratio.size();
  mean /= ratio.size();
  sdev = sqrt(sdev);

  std::cout << "Distortion | Min: " << min << ", Max: " << max << ", Mean: " << mean << ", Standard deviation: " << sdev << std::endl;

  // draw triangles using random value
  std::vector<double>::const_iterator rr = ratio.begin();
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t, ++rr) {
    unsigned char c[3];
    VertexID idP1 = (*t).getP1();
    VertexID idP2 = (*t).getP2();
    VertexID idP3 = (*t).getP3();
    if ((*rr) > 1) {
      c[0] = c[1] = (unsigned char) (255. / sqrt(*rr));
      c[2] = 255;
    }
    else {
      c[0] = 255;
      c[1] = c[2] = (unsigned char) (255. * sqrt(*rr));
    }
    outImage.draw_triangle((int)((*this)[idP1].get2DX()), (int)((*this)[idP1].get2DY()),
			   (int)((*this)[idP2].get2DX()), (int)((*this)[idP2].get2DY()),
			   (int)((*this)[idP3].get2DX()), (int)((*this)[idP3].get2DY()),
			   c);
  }

}


UCharCImg Mapping2D3D::toImageStats(unsigned int sizex, unsigned int sizey) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  UCharCImg result(sizex, sizey);
  std::deque<std::pair<double, double> > distances;
  double max2D = 0, max3D = 0;

  result.fill(std::numeric_limits<unsigned char>::max());
  const unsigned char black = 0;
  const unsigned char grey = 128;
  const char * msg2D = "2D";
  const char * msg3D = "3D";

  result.draw_arrow(5, sizey - 3, 5, 10, &black, 30, -2);
  result.draw_arrow(3, sizey - 5, sizex - 15, sizey - 5, &black, 30, -2);

  result.draw_text(2, 0, msg2D, &black);
  result.draw_text(sizex - 12, sizey - 10, msg3D, &black);

  (*mesh).setPointFlag(0);

  Mesh::point_iterator p3D = (*mesh).point_begin();
  for(Mapping2D3D::iterator p = begin(); p != end(); ++p, ++p3D) {
    (*p3D).setFlag(1);
    for(std::deque<VertexID>::const_iterator nb = (*p3D).getNeighbours().begin(); nb != (*p3D).getNeighbours().end(); ++nb)
      if ((*mesh).point(*nb).getFlag() == 0) {
	distances.push_back(std::pair<double, double>((*p3D).distance((*mesh).point(*nb)), (*p).distance2D((*this)[*nb])));
	if (max2D < distances.back().second)
	  max2D = distances.back().second;
	if (max3D < distances.back().first)
	  max3D = distances.back().first;
      }
  }

  double min = (max3D > max2D)? max2D : max3D;
  result.draw_line(5, sizey - 5,
		   5 + (int)((sizex - 20) * min / max3D), sizey - 5 - (int)((sizey - 15) * min / max2D), &grey);

  for(std::deque<std::pair<double, double> >::const_iterator d = distances.begin(); d != distances.end(); ++d) {
    result.draw_line(5 + (int)((sizex - 20) * (*d).first / max3D), sizey - 5 - (int)((sizey - 15) * (*d).second / max2D),
		     5 + (int)((sizex - 20) * (*d).first / max3D), sizey - 5 - (int)((sizey - 15) * (*d).second / max2D), &black);
  }


  std::ostringstream str;
  str << distances.size() << " edges";
  result.draw_text(sizex - 5 - str.str().size() * 6, 5, str.str().c_str(), &black);

  return result;
}


void Mapping2D3D::toImageDrawBorder(UCharCImg & outImage, const unsigned char & color) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  // resize circle set
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  // init flags and find first boundary vertex
  (*mesh).setPointFlag(0);
  Mesh::point_iterator bVIt = (*mesh).point_begin();
  while(!((*bVIt).getIsBoundary())) {
    ++bVIt;
    assert(bVIt != (*mesh).point_end());
  }

  (*bVIt).setFlag(1);
  // draw boundary
  drawBorder(outImage, (*bVIt).getId(), color);
}


void Mapping2D3D::drawBorder(UCharCImg & outImage,
			   const VertexID & bVIt,
			   const unsigned char & color) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  for (std::deque<VertexID>::iterator i = (*mesh).point(bVIt).getNeighbours().begin(); i != (*mesh).point(bVIt).getNeighbours().end(); ++i)
    if ((*mesh).point(*i).getIsBoundary()) {
      outImage.draw_line((int)(*this)[bVIt].get2DX(), (int)(*this)[bVIt].get2DY(),
			 (int)(*this)[*i].get2DX(), (int)(*this)[*i].get2DY(),
			 &color);
      if ((*mesh).point(*i).getFlag() == 0) {
	(*mesh).point(*i).setFlag(1);
	drawBorder(outImage, *i, color);
      }
    }
}

void Mapping2D3D::toImageDrawBorderTriangles(UCharCImg & outImage, const unsigned char & color) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  // resize the mapping
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  // init flags
  (*mesh).setPointFlag(0);

  (*mesh).point(0).setFlag(1);
  // draw borders
  drawBorderTriangles(outImage, 0, color);
}

void Mapping2D3D::toImageFromMap(UCharCImg & outImage, const MeshMap & mmap) {
    if (!flatten)
      throw Exception("Mapping has not been computed.");

    // get min and max values
    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::min();
    for(VertexID i = 0; i != mmap.getNbValues(); ++i) {
        const double v = mmap[i];
        if(v < min) min = v;
        if(v > max) max = v;
    }

    // resize the mapping
    resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);
    const unsigned char c[] = {std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max()};

    // then draw each triangle according to the normal (RGB <=> xyz)
    for(Mesh::const_triangle_iterator tr = (*mesh).triangle_begin(); tr != (*mesh).triangle_end(); ++tr) {
      const Point2D &c1 = (*this)[(*tr).getP1()];
      const Point2D &c2 = (*this)[(*tr).getP2()];
      const Point2D &c3 = (*this)[(*tr).getP3()];

      // TODO: correct values...
      const float v1 = ((mmap[(*tr).getP1()]) / max);
      const float v2 = ((mmap[(*tr).getP2()]) / max);
      const float v3 = ((mmap[(*tr).getP3()]) / max);

      outImage.draw_triangle((int)c1.get2DX(), (int)c1.get2DY(), (int)c2.get2DX(), (int)c2.get2DY(), (int)c3.get2DX(), (int)c3.get2DY(), c, v1, v2, v3);
    }
}

void Mapping2D3D::toImageDrawNormals(UCharCImg & outImage) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");


  // resize the mapping
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  // then draw each triangle according to the normal (RGB <=> xyz)
  for(Mesh::const_triangle_iterator tr = (*mesh).triangle_begin(); tr != (*mesh).triangle_end(); ++tr) {
    unsigned char c[3];
    Coord3D n = (*mesh).computeTriangleNormal((*tr).getId()).normalize();

    Point2D &c1 = (*this)[(*tr).getP1()];
    Point2D &c2 = (*this)[(*tr).getP2()];
    Point2D &c3 = (*this)[(*tr).getP3()];

    c[0] = 127 + (char) (n.getX() * 127);
    c[1] = 127 + (char) (n.getY() * 127);
    c[2] = 127 + (char) (n.getZ() * 127);

    outImage.draw_triangle((int)c1.get2DX(), (int)c1.get2DY(), (int)c2.get2DX(), (int)c2.get2DY(), (int)c3.get2DX(), (int)c3.get2DY(), c);
  }

}

void Mapping2D3D::toImageDrawTexture(UCharCImg & outImage, const UCharCImg & texture, float zratio) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  assert(zratio != 0);
  unsigned char c[] = {std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max()};

  // resize the mapping
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  // check for max texture value
  unsigned char min = std::numeric_limits<unsigned char>::max();
  unsigned char max = -std::numeric_limits<unsigned char>::max();
  for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p) {
    assert(((*p).getX() >= 0) && ((*p).getX() < texture._width));
    assert(((*p).getY() >= 0) && ((*p).getY() < texture._height));
    assert(((*p).getZ() >= 0) && (trunc((*p).getZ() / zratio) < texture._depth));
    if (min > texture((unsigned int) (*p).getX(), (unsigned int) (*p).getY(), (unsigned int) trunc((*p).getZ() / zratio)))
      min = texture((unsigned int) (*p).getX(), (unsigned int) (*p).getY(), (unsigned int) trunc((*p).getZ() / zratio));
    if (max < texture((unsigned int) (*p).getX(), (unsigned int) (*p).getY(), (unsigned int) trunc((*p).getZ() / zratio)))
      max = texture((unsigned int) (*p).getX(), (unsigned int) (*p).getY(), (unsigned int) trunc((*p).getZ() / zratio));
  }


  for(Mesh::const_triangle_iterator tr = (*mesh).triangle_begin(); tr != (*mesh).triangle_end(); ++tr) {
    Point3D & p1 = (*mesh).point((*tr).getP1());
    Point3D & p2 = (*mesh).point((*tr).getP2());
    Point3D & p3 = (*mesh).point((*tr).getP3());
    Point2D &c1 = (*this)[(*tr).getP1()];
    Point2D &c2 = (*this)[(*tr).getP2()];
    Point2D &c3 = (*this)[(*tr).getP3()];

    outImage.draw_triangle((int)c1.get2DX(), (int)c1.get2DY(), (int)c2.get2DX(), (int)c2.get2DY(), (int)c3.get2DX(), (int)c3.get2DY(), c,
			   0.1 + 0.9 * ((float) texture((unsigned int) p1.getX(), (unsigned int) p1.getY(), (unsigned int) trunc(p1.getZ() / zratio)) - min) / (max - min + 1),
			   0.1 + 0.9 * ((float) texture((unsigned int) p2.getX(), (unsigned int) p2.getY(), (unsigned int) trunc(p2.getZ() / zratio)) - min) / (max - min + 1),
			   0.1 + 0.9 * ((float) texture((unsigned int) p3.getX(), (unsigned int) p3.getY(), (unsigned int) trunc(p3.getZ() / zratio)) - min) / (max - min + 1));
  }


}


void Mapping2D3D::toImageDrawDistanceMap(UCharCImg & outImage, const Point3D & point) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned char c[] = {std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max()};


  // resize circle set
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  // build maps
  for(Mesh::const_triangle_iterator tr = (*mesh).triangle_begin(); tr != (*mesh).triangle_end(); ++tr) {
    Point3D & p1 = (*mesh).point((*tr).getP1());
    Point3D & p2 = (*mesh).point((*tr).getP2());
    Point3D & p3 = (*mesh).point((*tr).getP3());
    Point2D &c1 = (*this)[(*tr).getP1()];
    Point2D &c2 = (*this)[(*tr).getP2()];
    Point2D &c3 = (*this)[(*tr).getP3()];

    outImage.draw_triangle((int)c1.get2DX(), (int)c1.get2DY(), (int)c2.get2DX(), (int)c2.get2DY(), (int)c3.get2DX(), (int)c3.get2DY(), c,
			   1. / (1. + 0.5 * point.distance(p1)),
			   1. / (1. + 0.5 * point.distance(p2)),
			   1. / (1. + 0.5 * point.distance(p3)));
  }

}


void Mapping2D3D::drawBorderTriangles(UCharCImg & outImage,
				    const VertexID & bVIt,
				    const unsigned char & color) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned char * realColor;
  realColor = new unsigned char[outImage._spectrum];
  for(unsigned int i = 0; i < outImage._spectrum; ++i) // uint better than iterator
    realColor[i] = color;


  for (std::deque<VertexID>::iterator i = (*mesh).point(bVIt).getNeighbours().begin(); i != (*mesh).point(bVIt).getNeighbours().end(); ++i) {
    outImage.draw_line((int)(*this)[bVIt].get2DX(), (int)(*this)[bVIt].get2DY(),
		       (int)(*this)[*i].get2DX(), (int)(*this)[*i].get2DY(),
		       realColor);
    if ((*mesh).point(*i).getFlag() == 0) {
      (*mesh).point(*i).setFlag(1);
      drawBorderTriangles(outImage, *i, color);
    }
  }

  delete [] realColor;
}


void Mapping2D3D::toImageOverlap(UCharCImg & outImage) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  // resize mapping
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);

  PlaneSpliter ps(*this);

  cimg_forXY(outImage, i, j) {
    unsigned char nbT = ps.getNbTrianglesInRay(Coord2D(i, j));
    unsigned char pt[3] = { nbT, nbT, nbT };
    outImage.draw_point(i, j, pt);
  }

  outImage.normalize(0, 255);
}

void Mapping2D3D::toImage(UCharCImg & outImage, const CImgList<unsigned char> & colorValues) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned char value = std::numeric_limits<unsigned char>::max();

  // resize mapping
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);


  // draw triangles using colorValues values
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    VertexID idP1 = (*t).getP1();
    VertexID idP2 = (*t).getP2();
    VertexID idP3 = (*t).getP3();
    outImage.draw_triangle((int)((*this)[idP1].get2DX()), (int)((*this)[idP1].get2DY()),
			   (int)((*this)[idP2].get2DX()), (int)((*this)[idP2].get2DY()),
			   (int)((*this)[idP3].get2DX()), (int)((*this)[idP3].get2DY()),
			   &value,
			   (double)colorValues[(int)((*mesh).point(idP1).getId())](0) / std::numeric_limits<unsigned char>::max(),
			   (double)colorValues[(int)((*mesh).point(idP2).getId())](0) / std::numeric_limits<unsigned char>::max(),
			   (double)colorValues[(int)((*mesh).point(idP3).getId())](0) / std::numeric_limits<unsigned char>::max());
  }

}


void Mapping2D3D::toImage(UCharCImg & outImage, const UCharCImg & colorValues) {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  unsigned char value = std::numeric_limits<unsigned char>::max();

  // resize circle set
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);


  // draw triangles using colorValues values
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    VertexID idP1 = (*t).getP1();
    VertexID idP2 = (*t).getP2();
    VertexID idP3 = (*t).getP3();
    outImage.draw_triangle((int)((*this)[idP1].get2DX()), (int)((*this)[idP1].get2DY()),
			   (int)((*this)[idP2].get2DX()), (int)((*this)[idP2].get2DY()),
			   (int)((*this)[idP3].get2DX()), (int)((*this)[idP3].get2DY()),
			   &value,
			   (double)colorValues((int)((*mesh).point(idP1).getX()), (int)((*mesh).point(idP1).getY())) / std::numeric_limits<unsigned char>::max(),
			   (double)colorValues((int)((*mesh).point(idP2).getX()), (int)((*mesh).point(idP2).getY())) / std::numeric_limits<unsigned char>::max(),
			   (double)colorValues((int)((*mesh).point(idP3).getX()), (int)((*mesh).point(idP3).getY())) / std::numeric_limits<unsigned char>::max());
  }

}

void Mapping2D3D::toImageRandom(UCharCImg & outImage) {
  srandom(42);

  unsigned char value[3] = {std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max(), std::numeric_limits<unsigned char>::max()};

  // resize circle set
  resize2DCoords(0, 0, outImage._width - 1, outImage._height - 1);


  // draw triangles using random value
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
    VertexID idP1 = (*t).getP1();
    VertexID idP2 = (*t).getP2();
    VertexID idP3 = (*t).getP3();
    double r = (1 + (double)random() / RAND_MAX) / 2;
    outImage.draw_triangle((int)((*this)[idP1].get2DX()), (int)((*this)[idP1].get2DY()),
			   (int)((*this)[idP2].get2DX()), (int)((*this)[idP2].get2DY()),
			   (int)((*this)[idP3].get2DX()), (int)((*this)[idP3].get2DY()),
			   value, r, r, r);
  }

}

void Mapping2D3D::setFlatten() {
  flatten = true;
}

bool Mapping2D3D::isFlatten() const {
  return flatten;
}

void Mapping2D3D::adjust2DLocationMeanDistance() {
  double meanDistance2D = 0.0;
  double meanDistance3D = 0.0;
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  (*mesh).setPointFlag(0);
  Mesh::point_iterator p3D = (*mesh).point_begin();
  for(Mapping2D3D::const_iterator p = begin(); p != end(); ++p, ++p3D) {
    (*p3D).setFlag(1);
    for(std::deque<VertexID>::const_iterator nb = (*p3D).getNeighbours().begin(); nb != (*p3D).getNeighbours().end(); ++nb)
      if ((*mesh).point(*nb).getFlag() == 0) {
	meanDistance2D += (*p).distance2D((*this)[*nb]) / (*mesh).getNbPoints();
	meanDistance3D += (*p3D).distance((*mesh).point(*nb)) / (*mesh).getNbPoints();
      }
  }
  scale(meanDistance3D / meanDistance2D, Coord2D(0.0, 0.0));
}


void Mapping2D3D::computeExtremaFromBorder(const LengthEdge & le, bool smooth, bool useMean, bool useMedian, int nbSelected, const std::string & logFile) {
  extrema.clear();

  // get list of boundary points
  std::deque<VertexID> bPoints = (*mesh).getBoundaryPoints();

  // compute a Dijkstra using ratio as length of edges
  LengthEdge * tle = le.clone(*mesh);
  MeshManipulator mManip(*mesh);
  mManip.setLength(*tle);
  (*mesh).setPointFlag(0);
  mManip.computeDijkstra(bPoints, 0);

  // smooth
  if (smooth)
    mManip.smoothLength();

  double threshold = 0.0;
  std::vector<double> ratios;
  double mean = 0.0;
  if (useMean || useMedian || (nbSelected > 0) || (logFile != "")) {
    Mapping2D3D::const_iterator ci = begin();

    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p) {
      double r = 0.0;
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin();
	  nb != (*p).getNeighbours().end(); ++nb) {
	r += (*p).distance((*mesh).point(*nb)) / (*ci).distance2D((*this)[*nb]);
      }
      ++ci;
      ratios.push_back(r);
      mean += r / (*mesh).getNbPoints();
    }

    if (logFile != "") {
      std::ofstream outFile(logFile.c_str(), std::ios::out);
      outFile.precision(20);
      Mapping2D3D::const_iterator ci_l = begin();
      for(std::vector<double>::const_iterator r = ratios.begin(); r != ratios.end(); ++r, ++ci_l) {
	outFile << (*ci_l).get2DX() << " " << (*ci_l).get2DY() << " " << *r << std::endl;
      }
      outFile.close();
    }
  }

  if (useMean) {
    threshold = mean;
  }
  if (useMedian) {
    std::vector<double> ratiosBis(ratios);
    std::sort(ratiosBis.begin(), ratiosBis.end());
    double median = ratiosBis[ratiosBis.size() / 2];
    if (median > threshold)
      threshold = median;
  }

  // get local maxima
  for(Mesh::const_point_iterator i = (*mesh).point_begin(); i != (*mesh).point_end(); ++i) {
    double ratio = mManip.getLength((*i).getId());
    if ((!useMean && !useMedian) || (ratios[(*i).getId()] > threshold)) {
      bool extremum = true;
      for(std::deque<VertexID>::const_iterator nb = (*i).getNeighbours().begin(); nb != (*i).getNeighbours().end(); ++nb)
	if (mManip.getLength(*nb) > ratio) {
	  extremum = false;
	  break;
	}
      if (extremum)
	extrema.push_back((*i).getId());
    }
  }

  if ((nbSelected > 0) && ((int)extrema.size() > nbSelected)) {
    sort(extrema.begin(), extrema.end(), CompRatio(ratios));
    extrema.erase(extrema.begin() + nbSelected, extrema.end());
  }

  delete tle;
}


void Mapping2D3D::exportMapping(const std::string & prefix) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");


  {
    std::string file2DPoints = prefix + "-coords2D.txt";
    std::ofstream outFile(file2DPoints.c_str(), std::ios::out);

    if (!outFile.is_open())
      throw ExceptionCannotOpenOutputFile();

    for(Mapping2D3D::const_iterator p = begin(); p != end(); ++p)
      outFile << (*p).get2DX() << " " << (*p).get2DY() << std::endl;

    outFile.close();
  }

  {
    std::string fileRatios = prefix + "-ratios.txt";
    std::ofstream outFile(fileRatios.c_str(), std::ios::out);

    if (!outFile.is_open())
      throw ExceptionCannotOpenOutputFile();

    LengthEdgeMappingRatio lemr(*this);

    for(Mesh::const_point_iterator p1 = (*mesh).point_begin(); p1 != (*mesh).point_end(); ++p1) {
      for(Mesh::const_point_iterator p2 = (*mesh).point_begin(); p2 != (*mesh).point_end(); ++p2) {
	outFile << " ";
	if ((*p1).hasNeighbour(*p2)) {
	  outFile << lemr((*p1).getId(), (*p2).getId());
	}
	else
	  outFile << "inf";
      }
      outFile << std::endl;
    }

    outFile.close();
  }

  {
    std::string file2DLength = prefix + "-length2D.txt";
    std::ofstream outFile(file2DLength.c_str(), std::ios::out);

    if (!outFile.is_open())
      throw ExceptionCannotOpenOutputFile();

    const_iterator p12D = begin();
    for(Mesh::const_point_iterator p1 = (*mesh).point_begin(); p1 != (*mesh).point_end(); ++p1, ++p12D) {
      const_iterator p22D = begin();
      for(Mesh::const_point_iterator p2 = (*mesh).point_begin(); p2 != (*mesh).point_end(); ++p2, ++p22D) {
	outFile << " ";
	if ((*p1).hasNeighbour(*p2)) {
	  outFile << (*p12D).distance2D(*p22D);
	}
	else
	  outFile << "inf";
      }
      outFile << std::endl;
    }

    outFile.close();
  }

}

Coord2D Mapping2D3D::get2DCoord(const PointOnEdge & pOe) const {
  if (!flatten)
    throw Exception("Mapping has not been computed.");

  assert(size() > pOe.getFirstVertex());
  assert(size() > pOe.getSecondVertex());

  return (*this)[pOe.getFirstVertex()] * (1 - pOe.getLocation()) + (*this)[pOe.getSecondVertex()] * pOe.getLocation();
}
