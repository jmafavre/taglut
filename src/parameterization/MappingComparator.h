/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MAPPING_COMPARATOR
#define MAPPING_COMPARATOR

#include "Mapping2D3D.h"
#include "MeshMatcher.h"

namespace Taglut {

  /**
     @class MappingComparator

     @author Jean-Marie Favreau
     @brief Comparison between mappings (square distances & co)
  */
  class MappingComparator {
  private:
    /**
       Original mapping
    */
    const Mapping2D3D & mapping;

    /**
       Reference mapping
    */
    const Mapping2D3D & reference;

    /**
       Matching tool for points' correspondance
    */
    MeshMatcher mMatcher;

  public:
    /**
       Default constructor
    */
    MappingComparator(const Mapping2D3D & mapping, const Mapping2D3D & reference);

    /**
       get the sum of the square distances between the two compared mappings
    */
    double sumSquareDistance2D() const;

    /**
       get the mean of the square distances between the two compared mappings
    */
    double meanSquareDistance2D() const;

    /**
       get the max of the square distances between the two compared mappings
    */
    double maxSquareDistance2D() const;

    /**
       apply the method described by the string given in parametter
    */
    double useMethod(const std::string & method) const;

    /**
       return true if the given method is part of the available methods
    */
    static bool isKnownMethod(const std::string & method);


  };
}

#endif
