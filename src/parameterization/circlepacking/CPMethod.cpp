/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <sstream>
#ifdef USE_OMP
#include <omp.h>
#endif
#include <sys/times.h>

#include "CPMethod.h"
#include "Messages.h"
#include "FileExceptions.h"
#include "SurfaceToolbox.h"
#include "ColorChooser.h"
#include "MeshManipulator.h"
#include "FileTools.h"
#include "MeshUncut.h"

#ifdef USE_LIBXML
#include <libxml/xmlreader.h>
#endif

using namespace Taglut;

#define VERSION(version, valeur) (fabs(version - valeur) <= 0.01)

void CPMethod::addFormats() {
  addFormat("cst");
}

CPMethod::CPMethod(Mapping2D3D & mapping_t, bool originalMethod_t) : UnfoldingMethod(mapping_t), FileManipulator(mapping_t), originalMethod(originalMethod_t) {
  radii = NULL;
  initTempData();
}

CPMethod::CPMethod(const CPMethod & cpMethod) : UnfoldingMethod(cpMethod),  FileManipulator(cpMethod), originalMethod(cpMethod.originalMethod) {
  addFormats();
  radii = NULL;
  initTempData();
}


CPMethod::~CPMethod() {
  if (radii != NULL)
    delete [] radii;
}

CPMethod & CPMethod::operator=(const CPMethod & cset) {
  *( (UnfoldingMethod *)this) = cset;
  originalMethod = cset.originalMethod;
  nbCircles = 0;
  radii = NULL;
  return *this;
}


void CPMethod::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string &) {
  if (altFormat == "cst")
    loadCST(fileName);
  else
    throw Exception("Unknown format");
}

void CPMethod::saveAltFormat(const std::string & fileName, const std::string & format, const std::string &) const {
  if (format == "cst")
    saveCST(fileName);
  else
    throw Exception("Unknown format");
}


void CPMethod::reinit() {
  clearTempData();
  mapping.reinit();
}


void CPMethod::clearTempData() {
  if (radii != NULL)
    delete [] radii;
  radii = NULL;
  nbCircles = 0;
}

void CPMethod::initTempData() {
  clearTempData();
  nbCircles = mapping.size();
  if (nbCircles != 0)
    radii = new double[nbCircles];
}




void CPMethod::setCircleRadiuses(const double & radius) {
  assert(mapping.size() == nbCircles);
  double * r = radii;
  for(VertexID i = 0; i != nbCircles; ++r, ++i)
    *r = radius;
}



void CPMethod::computeInitialCircleRadiuses() {
  if (mapping.size() <= 1)
    return;

  initTempData();

  double * r = radii;
  for(Mesh::const_point_iterator point = mapping.point3D_begin(); point != mapping.point3D_end(); ++point, ++r) {
    if (originalMethod && (*point).getIsBoundary()) {
      double radius = 0;
      int nb = 0;
      for (std::deque<VertexID>::const_iterator i = (*point).getNeighbours().begin(); i != (*point).getNeighbours().end(); ++i)
	if (mapping.point3D(*i).getIsBoundary()) {
	  radius += (*point).distance(mapping.point3D(*i));
	  ++nb;
	}
      assert((nb != 0) && (radius != 0));
      *r = radius / nb / 2;
    }
    else {
      double radius = 0;
      for (std::deque<VertexID>::const_iterator i = (*point).getNeighbours().begin(); i != (*point).getNeighbours().end(); ++i)
	radius += (*point).distance(mapping.point3D(*i));
      *r = radius / (*point).getNbNeighbours() / 2;
    }
  }
}



void CPMethod::compute2DLocation(bool adjustMeanDistance) {
  compute2DLocation(IndentManager(), adjustMeanDistance);
}

void CPMethod::compute2DLocation(const IndentManager & iManager, bool adjustMeanDistance) {

  if ((nbCircles == 0) || (mapping.size() != nbCircles))
     std::cout << iManager << "Cannot compute 2d location of an empty circle set." << std::endl;

  // first reorder triangles and neighbours
  mesh.reorient();

  // then check for number of circles inside set
  if (mapping.size() <= 1) {
    mapping.reset2DCoords();
    if (mapping.size() == 1)
      mapping.set2DCoords(0, 0.0, 0.0);
    std::cout << iManager << "Cannot compute 2d location of an empty circle set." << std::endl;
  }

  /* Choose first point, in middle of circle list (Should be near mesh center)
     choose second one inside neighbours
  */
  const Point3D & center = mesh.getMiddlePoint();

  if (center.getNbNeighbours() == 0)
    throw Exception(std::string("Impossible de trouver un voisin au point choisi pour initialiser la reconstruction 2D du maillage."));

  VertexID idP1 = center.getId();
  VertexID idP2 = center.getNeighbours().front();


  assert(mapping.point3D(idP1).hasNeighbour(idP2));

  // No one 2d value is known
  mapping.getMesh().setPointFlag(0);


  /* Compute max and min values */
  mapping.reset2DCoords();

  int nbBad = 0;
  int nbCirclesC = mapping.size();


  std::list<std::pair<VertexID, VertexID> > list;
  list.push_back(std::pair<VertexID, VertexID>(idP1, idP2));
  list.push_back(std::pair<VertexID, VertexID>(idP2, idP1));

  // Set c and c2 2D position
  mapping.set2DCoords(idP1, 0.0, 0.0);
  mapping.set2DCoords(idP2, 0.0, radii[idP1] + radii[idP2]);
  mapping.point3D(idP1).setFlag(1);
  mapping.point3D(idP2).setFlag(1);
  nbCirclesC -= 2;

  while (list.size() != 0) {
    std::pair<VertexID, VertexID> p = list.front();
    list.pop_front();
    std::deque<VertexID> & pfneighbours = mapping.point3D(p.first).getNeighbours();
    VertexID pf = p.first;
    VertexID ps = p.second;

    // find p.second iterator
    std::deque<VertexID>::const_iterator psei = pfneighbours.begin();

    assert(pf == mapping.point3D(pf).getId());
    assert(ps == mapping.point3D(ps).getId());


    while (ps != *psei) {
      ++psei;
      assert(psei != pfneighbours.end());
    }

    std::deque<VertexID>::const_iterator beforei = psei;
    bool limit = true;

    // add neighbours from p.second to end of neighbour list
    for (std::deque<VertexID>::const_iterator i = psei + 1; i != pfneighbours.end(); ++i) {
      if (mapping.point3D(*i).hasNeighbour(*beforei)) {
        if ((mapping.point3D(*i).getFlag() == 0)) {
	  compute2DCoords(*i, *beforei, pf);
	  if (mapping.point3D(*i).getFlag() == 2)
	    ++nbBad;
	  mapping.point3D(*i).setFlag(1);
	  list.push_front(std::pair<VertexID, VertexID>(*i, pf));
	  --nbCirclesC;
        }
      }
      else {
	limit = false;
	break;
      }
      beforei = i;
    }
    if (limit)    // if needed, add neighbours from end of neighbour list to boundary neighbour

      for (std::deque<VertexID>::const_iterator i = pfneighbours.begin(); i != pfneighbours.end(); ++i) {
	if (mapping.point3D(*i).hasNeighbour(*beforei)) {
	  if ((mapping.point3D(*i).getFlag() == 0)) {
	    compute2DCoords(*i, *beforei, pf);
	    if (mapping.point3D(*i).getFlag() == 2)
	      ++nbBad;
	    mapping.point3D(*i).setFlag(1);
	    list.push_front(std::pair<VertexID, VertexID>(*i, pf));
	    --nbCirclesC;
          }
        }
	else break;
        beforei = i;
      }



    // add neighbours from neighbour before p.second to start of neighbour list
    if (psei != pfneighbours.begin()) {
      bool stop = true;
      limit = true;
      beforei = psei;
      for (std::deque<VertexID>::const_iterator i = psei - 1; stop; --i) {
        stop = (i != pfneighbours.begin());
	if (mapping.point3D(*i).hasNeighbour(*beforei)) {
	  if ((mapping.point3D(*i).getFlag() == 0)) {
            compute2DCoords(*i, pf, *beforei);
            if (mapping.point3D(*i).getFlag() == 2)
              ++nbBad;
            mapping.point3D(*i).setFlag(1);
            list.push_front(std::pair<VertexID, VertexID>(*i, pf));
            --nbCirclesC;
          }
        }
        else {
          limit = false;
          break;
        }
        beforei = i;
      }

      // add neighbours from end of list to p.second
      stop = true;
      if (limit) {
	for (std::deque<VertexID>::const_iterator i = pfneighbours.end() - 1; stop; --i) {
	  stop = (i != pfneighbours.begin());
	  if (mapping.point3D(*i).hasNeighbour(*beforei)) {
	    if ((mapping.point3D(*i).getFlag() == 0)) {
	      compute2DCoords(*i, pf, *beforei);
	      if (mapping.point3D(*i).getFlag() == 2)
		++nbBad;
	      mapping.point3D(*i).setFlag(1);
              list.push_front(std::pair<VertexID, VertexID>(*i, pf));
              --nbCirclesC;
            }
          }
          else
            break;
          beforei = i;
        }
      }
    }

  }


  if (nbCirclesC != 0)
    std::cout << iManager << "The number of non processed circles by 2d calculation is: " << nbCirclesC << std::endl;

  if (nbBad != 0)
    std::cout << iManager << "The 2D location of "<< nbBad << " points has been computed with imprecision." << std::endl;

  mapping.setFlatten();

  if (adjustMeanDistance)
    mapping.adjust2DLocationMeanDistance();
}


/** given two neighbours with already computed location, and given mesh structure,
    current circle's position is computed using radii.
    Note: mesh is needed to check triangle order. */

/* http://fribotte.free.fr/bdtech/Balises_theorie/balises_theorie.html */
const std::pair<double, double> CPMethod::compute2DCoords(VertexID idCurrent, VertexID idC1, VertexID idC2) {
  double & rCurrent = radii[idCurrent];
  double & rc1 = radii[idC1];
  double & rc2 = radii[idC2];
  Point2D & cpCurrent = mapping[idCurrent];
  Point3D & cpCurrent3D = mapping.point3D(idCurrent);
  Point2D & cp1 = mapping[idC1];
  Point2D & cp2 = mapping[idC2];
  double d1 = rCurrent + rc1;
  double d2 = rCurrent + rc2;
  double d3 = cp1.distance2D(cp2);

  assert(cpCurrent3D.hasNeighbour(idC1) && cpCurrent3D.hasNeighbour(idC2));

  if ((d3 < rc1 + rc2 - 0.1) ||
      (d3 > rc1 + rc2 + 0.1)) {
    std::cout << "Incoherent distance (" << d3 << "; radii sum: " << (rc1 + rc2) << ") value between " << cp1 << " and " << cp2 << std::endl;

    cpCurrent3D.setFlag(2);
  }

  compute2DCoordsUsingDistances(idCurrent, idC1, idC2, d1, d2, d3);

  return cpCurrent.get2DCoords();
}





std::string CPMethod::toStringXML(float version) const {
  std::ostringstream result;
  result.precision(2);


  if (version >= 1)
    result << "<circleset version=\"" << version << "\">" << std::endl;
  else
    result << "<circleset>" << std::endl;

  result.precision(20);

  if (VERSION(version, 1.2)) {
    Mesh::const_point_iterator point = mapping.point3D_begin();
    double * radius = radii;
    for (VertexID i = 0; i < nbCircles; ++i, ++radius) {
      result << "\t<circle id=\"" << (*point).getId() << "\""
	     << " x=\"" << (*point).getX() << "\""
	     << " y=\"" << (*point).getY() << "\""
	     << " z=\"" << (*point).getZ() << "\""
	     << " radius=\"" << *radius << "\""
	     << " boundary=\"" << (*point).getIsBoundary() << "\" />" << std::endl;
      ++point;
    }

    for(Mesh::triangle_iterator i = mesh.triangle_begin(); i != mesh.triangle_end(); ++i)
      result << "\t<triangle id=\"" << (*i).getId() << "\""
	     << " idP1=\"" << (*i).getP1() << "\""
	     << " idP2=\"" << (*i).getP2() << "\""
	     << " idP3=\"" << (*i).getP3() << "\""
	     << " />" << std::endl;

  }
  else { // version == 1
    Mesh::const_point_iterator point = mapping.point3D_begin();
    double * radius = radii;
    for (VertexID i = 0; i < nbCircles; ++i, ++radius, ++point) {
      result << "\t<circle id=\"" << (*point).getId() << "\""
	     << " x=\"" << (*point).getX() << "\""
	     << " y=\"" << (*point).getY() << "\""
	     << " z=\"" << (*point).getZ() << "\""
	     << " radius=\"" << *radius << "\""
	     << " boundary=\"" << (*point).getIsBoundary() << "\">" << std::endl;
      for (std::deque<VertexID>::const_iterator i_t = (*point).getNeighbours().begin(); i_t != (*point).getNeighbours().end(); ++i_t)
	result << "\t\t<neighbour id=\"" << mapping.point3D(*i_t).getId() << "\" />" << std::endl;
      result  << "\t</circle>" << std::endl;
    }
  }

  result << "</circleset>" << std::endl;
  return result.str();
}



void CPMethod::saveCST(const std::string & filename, float version) const {

  std::ofstream outfile(filename.c_str(), std::ios::out);

  if (mapping.size() == 0)
    throw ExceptionEmptyData();
  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
  outfile << toStringXML(version);

  outfile.close();

}

#ifdef LIBXML_READER_ENABLED
void CPMethod::loadCST(xmlTextReaderPtr & reader, float version) {
  double x, y, z;
  double radius = 0;
  VertexID id = 0, idP1, idP2, idP3;
  bool isBoundary = false;
  int ret = xmlTextReaderRead(reader); // reading next node
  std::deque<Triangle> tList;
  std::deque<double> radiiTmp;
  std::deque<Point3D> oPoints;
  Point3D p3D;
  Point2D p2D;

  // read data
  if (version > 1.1) {
    Triangle t;
#ifndef NDEBUG
    unsigned int cpt = 0;
#endif
    while (ret != 0) {
      if (std::string("circle") == (const char *) xmlTextReaderConstName(reader)) {
	// read coords
	x = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"x"));
	y = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"y"));
	z = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"z"));
	// read circle infos
	radius = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"radius"));
	id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));
	isBoundary = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"boundary"));
	// set values
	p2D.setId(id);
	p3D.setId(id);
	p3D.setX(x);
	p3D.setY(y);
	p3D.setZ(z);
	p3D.setIsBoundary(isBoundary);
	// reinit circle info
	p3D.clearNeighboursAndTriangles();

	// add circle to the circle set
	mapping.push_back(p2D);
	oPoints.push_back(p3D);
	radiiTmp.push_back(radius);
      }
      else if (std::string("triangle") == (const char *) xmlTextReaderConstName(reader)) {
	// read coords
	idP1 = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"idP1"));
	idP2 = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"idP2"));
	idP3 = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"idP3"));
	id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));

	// check if file order is ok
	assert(id == cpt++);

	// build triangle and add it to list
	t.setId(id);
	t.setPoints(idP1, idP2, idP3);
	tList.push_back(t);
      }
      else if (std::string("circleset") == (const char *) xmlTextReaderConstName(reader))
	break;

      ret = xmlTextReaderRead(reader);
    }
  }
  else { // version == 1
    while (ret != 0) {
      // for each circle in xml
      if (std::string("circle") == (const char *) xmlTextReaderConstName(reader))
	if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT) {

	  // read coords
	  x = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"x"));
	  y = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"y"));
	  z = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"z"));
	  // read circle infos
	  radius = atof((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"radius"));
	  id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));
	  isBoundary = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"boundary"));
	  // set values
	  p2D.setId(id);
	  p3D.setId(id);
	  p3D.setX(x);
	  p3D.setY(y);
	  p3D.setZ(z);
	  p3D.setIsBoundary(isBoundary);
	  // reinit circle info
	  p3D.clearNeighboursAndTriangles();
	}
	else {
	  // add circle to the circle set
	  mapping.push_back(p2D);
	  oPoints.push_back(p3D);
	  radiiTmp.push_back(radius);
	}
      else if (std::string("neighbour") == (const char *) xmlTextReaderConstName(reader)) {
	// add neighbours in circle
	id = atoi((const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"id"));
	p3D.addNeighbour(id);
      }
      else if (std::string("circleset") == (const char *) xmlTextReaderConstName(reader))
	break;

      ret = xmlTextReaderRead(reader);
    }
  }

  if (ret == 0)
    throw ExceptionErrorDuringReadingFile();

  // all data is loaded, we can now compute others needed infos (triangles, mesh, etc)

  // build circle set
  initTempData();
  assert(radiiTmp.size() == mapping.size());
  double * rr = radii;
  for(std::deque<double>::const_iterator r = radiiTmp.begin(); r != radiiTmp.end(); ++r, ++rr)
    *rr = *r;

  // if needed, add triangles (and neighbours relations) to points
  if (version > 1.1) {
    for(std::deque<Triangle>::const_iterator i = tList.begin(); i != tList.end(); ++i) {
      oPoints[(*i).getP1()].addTriangle((*i).getP2(), (*i).getP3(), (*i).getId());
      oPoints[(*i).getP2()].addTriangle((*i).getP1(), (*i).getP3(), (*i).getId());
      oPoints[(*i).getP3()].addTriangle((*i).getP1(), (*i).getP2(), (*i).getId());
    }
  }


  // then for each version, set mesh data
  if (version > 1.1)
    mesh.setPointsAndbuildMesh(oPoints, tList);
  else // version == 1
    mesh.setPointsAndComputeTriangles(oPoints);

  // then update point location
  compute2DLocation();
}
#endif

void CPMethod::loadCST(const std::string & filename) {
#ifdef LIBXML_READER_ENABLED
  xmlTextReaderPtr reader;
  float version = 1;
  int ret = 0;

  reader = xmlReaderForFile(filename.c_str(), NULL, 0);
  mapping.clear();

  if (reader == NULL)
    throw ExceptionFileNotFound();
  ret = xmlTextReaderRead(reader);

  // first find file version
  bool cst = (std::string("circleset") == (const char *) xmlTextReaderConstName(reader));
  bool cstl = (std::string("circlesetlist") == (const char *) xmlTextReaderConstName(reader));
  if (ret != 1)
    throw ExceptionErrorDuringReadingFile();
  if (cst || cstl) {
    const char * v = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"version");
    if (v != NULL)
      version = atof(v);
    else
      version = 1;
  }
  else
    throw ExceptionErrorDuringReadingFile();

  // if no child
  if (xmlTextReaderIsEmptyElement(reader) != 0)
    throw ExceptionErrorDuringReadingFile();

  // else jump next child if needed
  if (cstl) {
    do {
      ret = xmlTextReaderRead(reader);
      if (ret == 0)
	throw ExceptionErrorDuringReadingFile();
    } while (std::string("circleset") != (const char *) xmlTextReaderConstName(reader));

    const char * v = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"version");
    if (v != NULL)
      version = atof(v);
  }

  // then load circle set
  loadCST(reader, version);

  // and clear xml reader
  xmlFreeTextReader(reader);

  return;
#else
  throw ExceptionXMLReaderNotAvailable(filename + " not opened");
#endif
}

/** UNFOLDING TOOL **/

int CPMethod::computeRadiusesFixedBoundaries(double epsilon, unsigned int maxIter, bool initRadius,
					     const IndentManager iManager, const std::string logFile) {


  // compute boundary circle radius & initial circle radius for others
  if (initRadius)
    computeInitialCircleRadiuses();

  assert(nbCircles == mapping.size());

  logInitialAngles(logFile);

  // compute circle packing algorithm
  int result = computeCirclePackingFixedBoundaries(epsilon, maxIter, iManager);

  logFinalValues(logFile);

  return result;
}


int CPMethod::computeCirclePackingFixedBoundaries(double epsilon, unsigned int maxIter, const IndentManager & iManager) {
  clock_t tstart = clock();
  struct timeval tstart_real, tend_real;
  gettimeofday(&tstart_real, NULL);

  bool adjust;
  double angle_sum, m1, m2, first_m, denom, b, d, diff, maxDiff, v;
  int nbNeighbours;
  maxDiff = 0;
  assert(nbCircles == mapping.size());

#ifdef USE_OMP
  std::cout << iManager << " Number of threads: " << omp_get_max_threads()  << std::endl;
#endif

  // circlepacking algorithm
  unsigned int iter = 0;
  while((maxIter == 0) || (iter < maxIter)) {
    ++iter;
    adjust = false;


    // for each circle, except boundary circles
#ifdef USE_OMP
#pragma omp parallel for private(angle_sum, nbNeighbours, first_m, diff, m1, m2, v, denom, b, d)
#endif
    for(int ii = 0; ii < (int)mapping.size(); ++ii) {
      const Point3D & i = mapping.getPoints()[ii];

      if (!i.getIsBoundary()) {
	assert(i.getNbNeighbours() != 0);
	/* compute angle sum */
	// init infos about c
	angle_sum = 0.0;
	double & radius = radii[ii];
	nbNeighbours = i.getNbNeighbours();

	// first step
	std::deque<VertexID>::const_iterator first_nb = i.getNeighbours().begin();
	double rad = radii[*first_nb];
	first_m = m2 = rad / (radius + rad);


	for(std::deque<VertexID>::const_iterator n = i.getNeighbours().begin(); n != i.getNeighbours().end(); ++n)
	  if (n != first_nb) { // not first edge, but all others
	    m1 = m2;
	    rad = radii[*n];
	    m2 = rad / (radius + rad);
	    v = (double)1.0 - 2 * m1 * m2;
	    if (v > 1) v = 1;
	    if (v < -1) v = -1;
	    angle_sum += acos(v);
	  }

	// last angle
	v = (double)1.0 - 2 * m2 * first_m;
	if (v > 1) v = 1;
	if (v < -1) v = -1;
	angle_sum += acos(v);

	diff = angle_sum - 2 * M_PI;

	if (diff < 0) diff = - diff;
	if (maxDiff < diff) {
	  maxDiff = diff;
	}

	/* adjust radius if needed */
	if ((angle_sum > 2 * M_PI + epsilon) || (angle_sum < 2 * M_PI - epsilon)) {
	  adjust = true;
	  /* adjust radius using 'uniform' method */
	  denom = (double)1.0 / (2.0 * nbNeighbours);
	  d = sin((double)2 * M_PI * denom);
	  b = sin(angle_sum * denom);


	  radius = radius * b * (1 - d) / (d * (1 - b));
#ifdef USE_OMP
#pragma omp flush
#endif
	}
      }
    }
    if (iter % 1000 == 0) {
      std::cout << iManager << " Maximum difference for angles (for 1000 iterations, at iteration #" << iter << "): " << maxDiff << std::endl;
      maxDiff = 0;
    }

    // break if no adjust last pass
    if (!adjust) {
      clock_t tend = clock();
      gettimeofday(&tend_real, NULL);
      std::cout << iManager << " Circle packing duration: " << ((double)(tend - tstart)) / (CLOCKS_PER_SEC) << " seconds (user: " << (((double)(tend_real.tv_usec - tstart_real.tv_usec)) / 1000000L + tend_real.tv_sec - tstart_real.tv_sec) << " s)." << std::endl;
      return iter;
    }

  }

  clock_t tend = clock();
  gettimeofday(&tend_real, NULL);
  std::cout << iManager << " Circle packing duration: " << ((double)(tend - tstart)) / (CLOCKS_PER_SEC) << " seconds (user: " << (((double)(tend_real.tv_usec - tstart_real.tv_usec)) / 1000000L + tend_real.tv_sec - tstart_real.tv_sec) << " s). Maximum iteration limit." << std::endl;
  return maxIter;


}


int CPMethod::computeRadiuses(double epsilon, unsigned int maxIter, bool initRadius,
			      const IndentManager iManager, const std::string logFile) {
  double * angles;
  int result;
  // compute boundary circle radius & initial circle radius for others
  if (initRadius)
    computeInitialCircleRadiuses();

  assert(nbCircles == mapping.size());

  // compute boundary angles
  angles = computeAngles(iManager);

  logInitialAngles(angles, logFile);

  // compute circle packing algorithm
  result = computeCirclePacking(epsilon, maxIter, angles, iManager);

  logFinalValues(logFile);

  delete [] angles;
  return result;
}

void CPMethod::logInitialAngles(const std::string & logFile) const {
  if (logFile == "")
    return;

  std::ofstream outfile(logFile.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  // compute date
  time_t rawtime;
  struct tm * timeinfo;
  char buffer [80];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, 80, "%c", timeinfo);

  // then log to file
  outfile.precision(20);

  outfile << "Logs of the Circle Packing algorithm - " << buffer << std::endl;
  outfile << "Number of circles: " << mapping.size() << std::endl;

  Mesh::const_point_iterator p = mesh.point_begin();
  double * r = radii;
  for (VertexID i = 0; i < nbCircles; ++i, ++r, ++p) {
    outfile << "radius";
    if ((*p).getIsBoundary())
      outfile << "(b)";
    outfile << ": " << *r << std::endl;
  }

  outfile.close();
}

void CPMethod::logInitialAngles(double * angles, const std::string & logFile) const {
  if (logFile == "")
    return;

  std::ofstream outfile(logFile.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  // compute date
  time_t rawtime;
  struct tm * timeinfo;
  char buffer [80];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, 80, "%c", timeinfo);

  // then log to file
  outfile.precision(20);

  outfile << "Logs of the Circle Packing algorithm - " << buffer << std::endl;
  outfile << "Number of circles: " << mapping.size() << std::endl;

  double * radius = radii;
  for (VertexID i = 0; i < nbCircles; ++i, ++radius)
    outfile << "radius: " << *radius << ", angle: " << angles[i] << std::endl;

  outfile.close();
}

void CPMethod::logFinalValues(const std::string & logFile) const {
  if (logFile == "")
    return;

  std::ofstream outfile(logFile.c_str(), std::ios::out | std::ios::app);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  // compute date
  time_t rawtime;
  struct tm * timeinfo;
  char buffer [80];
  time ( &rawtime );
  timeinfo = localtime(&rawtime);
  strftime(buffer, 80, "%c", timeinfo);

  // then log to file
  outfile.precision(20);

  outfile << "End of Circle Packing unfolding process - " << buffer << std::endl;
  Mesh::const_point_iterator p = mesh.point_begin();
  double * r = radii;
  for (VertexID i = 0; i < nbCircles; ++i, ++r, ++p) {
    outfile << "radius";
    if ((*p).getIsBoundary())
      outfile << "(b)";
    outfile << ": " << *r << std::endl;
  }

  outfile.close();

}


double * CPMethod::computeAngles(const IndentManager & iManager) {
  double * result;
  double bdSum = 0;
  double aMax = 0, aMin = 0;
  double nbBPoint = 0;
  assert(nbCircles == mapping.size());

  result = new double[mapping.size()];

  for(Mesh::const_point_iterator i = mapping.point3D_begin(); i != mapping.point3D_end(); ++i) {
    if ((*i).getIsBoundary()) {
      double angle = 0;

      for(std::deque<TriangleID>::const_iterator t = (*i).getTriangles().begin(); t != (*i).getTriangles().end(); ++t) {
	Point3D & c1 = mapping.point3D(mapping.getTriangle(*t).getOtherPoint((*i).getId()));
	Point3D & c2 = mapping.point3D(mapping.getTriangle(*t).getOtherPoint((*i).getId(), c1.getId()));
	angle += (*i).angle(c1, c2);
      }
      result[(*i).getId()] = angle;
      bdSum += M_PI - angle;
      if (aMax < angle) aMax = angle;
      if (aMin > angle) aMin = angle;
      ++nbBPoint;
    }
    else
      result[(*i).getId()] = 2 * M_PI;
  }


  if (((M_PI - ((M_PI - aMax) * 2. * M_PI) / bdSum) > 2 * M_PI) ||
      ((M_PI - ((M_PI - aMax) * 2. * M_PI) / bdSum) < 0)) { // linear correction
    double aMean = M_PI - bdSum / nbBPoint;
    double a1 = (2 * M_PI / nbBPoint - M_PI) / (aMin - aMean);
    double a = (M_PI + 2 * M_PI / nbBPoint) / (aMax - aMean);
    if (a1 < a) a = a1;
    double b = M_PI - 2 * M_PI / nbBPoint - a * aMean;

    std::cout << iManager << "Linear boundary correction. a: " << a << "; b: " << b << std::endl;
    for(Mesh::const_point_iterator i = mapping.point3D_begin(); i != mapping.point3D_end(); ++i)
      if ((*i).getIsBoundary()) {
	result[(*i).getId()] = a * result[(*i).getId()] + b;
	assert((result[(*i).getId()] <= 2 * M_PI) && (result[(*i).getId()] >= 0));
      }

  }
  else {
    std::cout << iManager << "Boundary correction: " << ((2. * M_PI) / bdSum) << " (angle sum: " << bdSum << ")" << std::endl;

    for(Mesh::const_point_iterator i = mapping.point3D_begin(); i != mapping.point3D_end(); ++i)
      if ((*i).getIsBoundary()) {
	result[(*i).getId()] = M_PI - ((M_PI - result[(*i).getId()]) * 2. * M_PI) / bdSum;
	assert((result[(*i).getId()] <= 2 * M_PI) && (result[(*i).getId()] >= 0));
      }
  }

  return result;
}

int CPMethod::computeCirclePacking(double epsilon, unsigned int maxIter, double * angles, const IndentManager & iManager) {
  clock_t tstart = clock();
  struct timeval tstart_real, tend_real;
  gettimeofday(&tstart_real, NULL);

  bool adjust;
  double angle_sum, radius, m1, m2, first_m, denom, b, d, diff, maxDiff, v;
  int nbNeighbours;
  VertexID idFixed1 = 0;
  maxDiff = 0;
  assert(nbCircles == mapping.size());

#ifdef USE_OMP
  std::cout << iManager << " Number of threads: " << omp_get_max_threads() << std::endl;
#endif

  // compute the id of the two fixed radii in boundary
  double * r = radii;
  Mesh::const_point_iterator p = mapping.point3D_begin();
  for (VertexID i = 0; i < nbCircles; ++i, ++r, ++p) {
    if ((*p).getIsBoundary()) {
      idFixed1 = (*p).getId();
      if (fabs(*r) < 0.00000000001)
	*r = 1;
      break;
    }
  }

  // circlepacking algorithm
  unsigned int iter = 0;
  while((maxIter == 0) || (iter < maxIter)) {
    ++iter;
    adjust = false;


    // for each circle, except boundary circles
    double * cc;
    Mesh::const_point_iterator i;
#ifdef USE_OMP
#pragma omp parallel for private(angle_sum, radius, nbNeighbours, first_m, diff, m1, m2, v, i, cc, denom, b, d)
#endif
    for(int ii = 0; ii < (int)mapping.size(); ++ii) {
      i = mapping.point3D_begin() + ii;
      cc = radii + ii;
      if ((*i).getId() != idFixed1) {
	/* compute angle sum */
	// init infos about c
	angle_sum = 0;
	radius = *cc;
	nbNeighbours = (*i).getNbNeighbours();

	// first step
	std::deque<VertexID>::const_iterator first_nb = (*i).getNeighbours().begin();
	first_m = m2 = radii[*first_nb] / (radius + radii[*first_nb]);

	for(std::deque<VertexID>::const_iterator n = (*i).getNeighbours().begin(); n != (*i).getNeighbours().end(); ++n)
	  if (n != first_nb) { // not first edge, but all others
	    m1 = m2;
	    m2 = radii[*n] / (radius + radii[*n]);
	    v = (double)1.0 - 2 * m1 * m2;
	    if (v > 1) v = 1;
	    if (v < -1) v = -1;
	    angle_sum += acos(v);
	  }

	// last angle
	if (!(*i).getIsBoundary()) {
	  v = (double)1.0 - 2 * m2 * first_m;
	  if (v > 1) v = 1;
	  if (v < -1) v = -1;
	  angle_sum += acos(v);
	}

	diff = angle_sum - angles[(*i).getId()];
	if (diff < 0) diff = - diff;
	if (maxDiff < diff) {
	  maxDiff = diff;
	}

	/* adjust radius if needed */
	if ((angle_sum > angles[(*i).getId()] + epsilon) || (angle_sum < angles[(*i).getId()] - epsilon)) {
	  adjust = true;
	  /* adjust radius using 'uniform' method */
	  denom = (double)1.0 / (2.0 * nbNeighbours);
	  d = sin(angles[(*i).getId()] * denom);
	  b = sin(angle_sum * denom);


	  *cc = radius * b * (1 - d) / (d * (1 - b));
	}
      }
    }
    if (iter % 1000 == 0) {
      std::cout << iManager << " Maximum difference for angles (for 1000 iterations, at iteration #" << iter << "): " << maxDiff << std::endl;
      maxDiff = 0;
    }

    // break if no adjust last pass
    if (!adjust) {
      clock_t tend = clock();
      gettimeofday(&tend_real, NULL);
      std::cout << iManager << " Circle packing duration: " << ((double)(tend - tstart)) / (CLOCKS_PER_SEC) << " seconds (user: " << (((double)(tend_real.tv_usec - tstart_real.tv_usec)) / 1000000L + tend_real.tv_sec - tstart_real.tv_sec) << " s)." << std::endl;
      return iter;
    }

  }

  clock_t tend = clock();
  gettimeofday(&tend_real, NULL);
  std::cout << iManager << " Circle packing duration: " << ((double)(tend - tstart)) / (CLOCKS_PER_SEC) << " seconds (user: " << (((double)(tend_real.tv_usec - tstart_real.tv_usec)) / 1000000L + tend_real.tv_sec - tstart_real.tv_sec) << " s). Maximum iteration limit." << std::endl;
  return maxIter;


}


int CPMethod::processUnfolding(float limit, unsigned int maxIter, bool initData, const std::string & logFile, const IndentManager & indentManager) {
  if (originalMethod)
    return computeRadiusesFixedBoundaries(limit, maxIter, initData, indentManager, logFile);
  else
    return computeRadiuses(limit, maxIter, initData, indentManager, logFile);

}

UnfoldingMethod * CPMethod::clone(Mapping2D3D & mapping_t) {
  UnfoldingMethod * result = new CPMethod(mapping_t, originalMethod);

  return result;
}


void CPMethod::saveCST(const std::string & filename, const std::deque<CPMethod> & cplist) {
  std::ofstream outfile(filename.c_str(), std::ios::out);

  if (cplist.size() == 0)
    throw ExceptionEmptyData();
  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
  outfile << "<circlesetlist>" << std::endl;
  for(std::deque<CPMethod>::const_iterator cset = cplist.begin(); cset != cplist.end(); ++cset)
    outfile << (*cset).toStringXML();
  outfile << "</circlesetlist>" << std::endl;

  outfile.close();
}


double CPMethod::initDataRandom(double maxValue) {
  srand(time(NULL));

  if (mapping.size() <= 1)
    return -1;

  initTempData();

  double maxRad = 0.0;
  double * r = radii;
  for(Mesh::const_point_iterator point = mapping.point3D_begin(); point != mapping.point3D_end(); ++point, ++r)
    if (originalMethod && (*point).getIsBoundary()) {
      double radius = 0;
      int nb = 0;
      for (std::deque<VertexID>::const_iterator i = (*point).getNeighbours().begin(); i != (*point).getNeighbours().end(); ++i)
	if (mapping.point3D(*i).getIsBoundary()) {
	  radius += (*point).distance(mapping.point3D(*i));
	  ++nb;
	}
      assert((nb != 0) && (radius != 0));
      *r = radius / nb / 2;
      if (*r > maxRad)
	maxRad = *r;
    }

  r = radii;
  double maxRadIn = 0.0;
  for(Mesh::const_point_iterator point = mapping.point3D_begin(); point != mapping.point3D_end(); ++point, ++r)
    if (!originalMethod || !(*point).getIsBoundary()) {
      if (maxValue > 0)
	*r = maxValue * (rand() / (RAND_MAX + 1.0));
      else
	*r = 2 * maxRad * (rand() / (RAND_MAX + 1.0));
      if (maxRadIn < *r)
	maxRadIn = *r;
    }

  return maxRadIn;
}


void CPMethod::initDataUsing2D() {
  double ratio = 0;
  VertexID nbBPoints = 0;
  if (mapping.size() <= 1)
    return;

  initTempData();

  double * r = radii;
  Mesh::const_point_iterator point3D = mapping.point3D_begin();
  for(Mapping2D3D::const_iterator point = mapping.begin(); point != mapping.end(); ++point, ++r, ++point3D) {
    double radius = 0.0;

    if (originalMethod && (*point3D).getIsBoundary()) {
      double radius2D = 0.0;
      int nb = 0;
      for (std::deque<VertexID>::const_iterator i = (*point3D).getNeighbours().begin(); i != (*point3D).getNeighbours().end(); ++i)
	if (mapping.point3D(*i).getIsBoundary()) {
	  radius += (*point3D).distance(mapping.point3D(*i));
	  radius2D += (*point).distance2D(mapping[*i]);
	  ++nb;
	}
      ratio += radius / radius2D;
      assert((nb != 0) && (radius != 0));
      *r =  radius / nb / 2;
      ++nbBPoints;
    }
    else {
      for (std::deque<VertexID>::const_iterator i = (*point3D).getNeighbours().begin(); i != (*point3D).getNeighbours().end(); ++i)
	radius += (*point).distance2D(mapping[*i]);
      *r = radius / (*point3D).getNbNeighbours() / 2;
    }
  }

  if (nbBPoints != 0) {
    ratio /= nbBPoints;
    Mesh::const_point_iterator point = mapping.point3D_begin();

    double * r_t = radii;
    for (VertexID i = 0; i < nbCircles; ++i, ++r_t, ++point)
      if (!(*point).getIsBoundary())
	*r_t /= ratio;
  }
}
