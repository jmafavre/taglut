/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CP_METHOD
#define CP_METHOD

#include <deque>
#include <list>
#include <string>
#include <fstream>

#include "UnfoldingMethod.h"
#include "IndentManager.h"
#include "FileManipulator.h"

#ifdef USE_LIBXML
#include <libxml/xmlreader.h>
#endif

namespace Taglut {
  class Mesh;
  class Mapping2D3D;

  /**
     @class CPMethod

     @author Jean-Marie Favreau
     @brief Unfolding tool using the Circle Packing algorithm
  */
  class CPMethod : public UnfoldingMethod, public FileManipulator {
  private:

    /** number of circles manipulated by the current Circle packing algorithm */
    VertexID nbCircles;

    /** radii of the circles manipulated by the current Circle packing algorithm  */
    double * radii;

    bool originalMethod;

    int computeCirclePackingFixedBoundaries(double epsilon, unsigned int maxIter, const Taglut::IndentManager & iManager);

    int computeCirclePacking(double epsilon, unsigned int maxIter, double * angles, const Taglut::IndentManager & iManager);

    double * computeAngles(const Taglut::IndentManager & iManager);

    void logInitialAngles(const std::string & logFile) const;

    void logInitialAngles(double * angles, const std::string & logFile) const;

    void logFinalValues(const std::string & logFile) const;

    /**
       Unfold circle set using circle packging method (adapted from http://www.math.utk.edu/%7Ekens/DownLoad.html).
       @param maxIter Maximum iteration number (0 means no limit)
       @param epsilon Epsilon value used to stop algorithm (angle sum == 2Pi +/- epsilon)
       @param initRadius if true, call computeInitialCircleRadiuses function.
       @param iManager indent manager
       @result Iteration number
    */
    int computeRadiusesFixedBoundaries(double epsilon, unsigned int maxIter, bool initRadius = true,
				       const Taglut::IndentManager iManager = Taglut::IndentManager(), const std::string logFile = "");

    /**
       Unfold circle set using circle packging method, with non fixed radius in the boundaries
       @param maxIter Maximum iteration number (0 means no limit)
       @param epsilon Epsilon value used to stop algorithm (angle sum == 2Pi +/- epsilon)
       @param initRadius if true, call computeInitialCircleRadiuses function.
       @param iManager indent manager
       @result Iteration number
    */
    int computeRadiuses(double epsilon, unsigned int maxIter, bool initRadius = true,
			const Taglut::IndentManager iManager = Taglut::IndentManager(), const std::string logFile = "");

    void addFormats();

  public:


    /**
       CPMethod is defined by a mesh.
       \param mapping mapping that will be unfolded
       \param originalMethod use original method
    */
    CPMethod(Mapping2D3D & mapping, bool originalMethod = true);

    /**
       Copy constructor
    */
    CPMethod(const CPMethod & cpMethod);

    /** destructor */
    virtual ~CPMethod();

#ifndef SWIG
    /**
       affectation operator
    */
    CPMethod & operator=(const CPMethod & cset);
#endif

    /** reinit using mesh */
    void reinit();

    /**
       compute 2d location.
       @param adjustMeanDistance adjust unfolding size according to the mean distabce
       @param iManager indent manager
    */
    void compute2DLocation(const Taglut::IndentManager & iManager, bool adjustMeanDistance = true);

    /**
       compute 2d location.
    */
    void compute2DLocation(bool adjustMeanDistance = true);

    /** given a point and two neighbours with already computed location,
	first point's position is computed using radii. */
    const std::pair<double, double> compute2DCoords(VertexID idCurrent, VertexID idC1, VertexID idC2);


    /**
       Init Circles' radius
    */
    void computeInitialCircleRadiuses();

    /**
       Set default radius value for each circle of the current CPMethod
    */
    void setCircleRadiuses(const double & radius);

    /**
       Load CPMethod from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Save CPMethod from file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;



    /**
       Save CPMethod in file
    */
    void saveCST(const std::string & filename, float version = 1.2) const;
    /**
       Load CPMethod from file
    */
    void loadCST(const std::string & filename);

#ifdef LIBXML_READER_ENABLED
    void loadCST(xmlTextReaderPtr & reader, float version);
#endif


    /**
       Clear all tempory data
    */
    void clearTempData();

    /**
       init temp data
    */
    void initTempData();

    /**
       Build a subset of xml file
    */
    std::string toStringXML(float version = 1.2) const;

    /**
       return list of radii
    */
    double * getRadii() {return radii; }

    /**
       return list of radii
    */
    const double * getRadii() const { return radii; }

    /**
       Return the number of circles manipulated by the current circle packing algorithm
    */
    VertexID getNbCircles() const { return nbCircles; }

    /**
       Process unfolding using Circle Packing algorithm
       Return number of iteration or -1 if fails
    */
    int processUnfolding(float limit, unsigned int maxIter, bool initData = true, const std::string & logFile = "", const Taglut::IndentManager & indentManager = Taglut::IndentManager());

    /**
       Clone method
    */
    virtual UnfoldingMethod * clone(Mapping2D3D & mapping);

    /**
       Init unfolding data using 2d location of points
    */
    virtual void initDataUsing2D();

    /**
       Init unfolding data using a random method
       @param maxValue maximum radii value allowed. If negative, radii are initialized using 2*r as maximum
       value, where r is maximum boundary radius
       @return Effective maximum value of radii not in boundary.
    */
    double initDataRandom(double maxValue = -1);

    /**
       Save a circle set list
    */
    static void saveCST(const std::string & filename, const std::deque<CPMethod> & cplist);

  };


  /**
     CPMethod's operator for stream output (using Circle output operator)
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const CPMethod & cset) {
    double * radii = cset.getRadii();
    for (VertexID i = 0; i < cset.getNbCircles(); ++i)
      f << "Radius: " << *radii << std::endl;
    return f;
  }

}


#endif
