/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT2D3D
#define POINT2D3D

#ifndef SWIG
#include <string>
#include <iostream>
#endif

#include "Point3D.h"
#include "Coord2D.h"

namespace Taglut {

  /**
     @brief Point2D3DT
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @brief 2d/3d matching point
  */
  template <typename S, typename T>
  class Point2D3DT : public Coord2DT<S>, public Point3DT<T> {
  public:

    /**
       Default constructor
    */
    inline Point2D3DT() : Coord2DT<S>(), Point3DT<T>() { }

    /**
       Constructor with 3d location
       a 2d/3d point is a 3d point
    */
    inline Point2D3DT(const T & x_l, const T & y_l, const T & z_l) : Coord2DT<S>(), Point3DT<T>(x_l, y_l, z_l) {
    }

    /**
       Constructor with 2d location
       a 2d/3d point is a 2d point
    */
    inline Point2D3DT(const S & x_l, const S & y_l) : Coord2DT<S>(x_l, y_l), Point3DT<T>() {
    }

    /**
       Constructor with 3d location
    */
    inline Point2D3DT(const Point3DT<T> & point) : Point3DT<T>(point) {
    }

    /**
       Constructor with 3d location
    */
    inline Point2D3DT(const Point3DT<T> & point, const Coord2DT<S> & point2D) : Coord2DT<S>(point2D), Point3DT<T>(point) {
    }

    /**
       Constructor with 2d and 3d location
    */
    inline Point2D3DT(const T & x_l, const T & y_l, const T & z_l, const S & x2d_l, const S & y2d_l) : Coord2DT<S>(x2d_l, y2d_l),
												       Point3DT<T>(x_l, y_l, z_l) {
    }


#ifndef SWIG
    /**
       Set operator
    */
    inline Point2D3DT<S, T>& operator=(const Point2D3DT<S, T>& p) {
      (*((Point3DT<T>*)this)) = p;
      (*((Coord2DT<T>*)this)) = p;

      return *this;
    }
#endif

    /** return 2D location (z location: 0) */
    inline Point3DT<T> get2DLocation() const {
      return Point3DT<T>((*this).x2d, (*this).y2d, 0);
    }

  };

  /**
     Stream operator
  */
  template <typename T, typename S, typename U, typename V>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Point2D3DT<U, V> & p) {
    f << "Point2D3DT(" << std::endl << "\t";
    f << *((Point3DT<V> *) & p)
      << std::endl << "\tPoint2D(";
    f << p.get2DX() << ", " << p.get2DY()  << "))";

    return f;
  }

  /**
     Define an alias to Point2D3DT with double
  */
  typedef Point2D3DT<double, double> Point2D3D;
}

#endif
