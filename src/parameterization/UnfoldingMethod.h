/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef UNFOLDING_METHOD
#define UNFOLDING_METHOD


#include "Mapping2D3D.h"

namespace Taglut {
  /**
     @class UnfoldingMethod
     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @brief Interface for unfolding methods
  */
  class UnfoldingMethod {
  protected:
    /** mapping where unfolding data will be saved */
    Mapping2D3D & mapping;

    /** mesh that will be unfolded */
    Mesh & mesh;

    /**
       Given 3 distances, compute the location of the first point given in parameter, according
       to the two others. Points are ordering on the plane according to the order of the corresponding triangle
       in mesh.
       @param id Id of the point that will be located
       @param idP1 Id of the first point allready located
       @param idP2 Id of the second point allready located
       @param d1 Distance between id and idP1
       @param d2 Distance between id and idP2
       @param d3 Distance between idP1 and idP2
    */
    void compute2DCoordsUsingDistances(VertexID id, VertexID idP1, VertexID idP2, double d1, double d2, double d3);

  public:
    /** default constructor */
    UnfoldingMethod(Mapping2D3D & mapping);

    /** copy constructor */
    UnfoldingMethod(const UnfoldingMethod & uMethod);

    /** destructor */
    virtual ~UnfoldingMethod() {}

#ifndef SWIG
    /**
       Affectation operator
    */
    UnfoldingMethod & operator=(const UnfoldingMethod & uMethod);

#endif

    /**
       compute 2d location.
       @param adjustMeanDistance adjust unfolding size according to the mean distance
    */
    inline void compute2DLocation(bool adjustMeanDistance = true) {
      compute2DLocation(Taglut::IndentManager(), adjustMeanDistance);
    }

    /**
       Compute 2D location
    */
    virtual void compute2DLocation(const Taglut::IndentManager &, bool = true) {  /* will be defined in other classes */ }


    /**
       Process unfolding
       Return number of iteration or -1 if fails
    */
    virtual int processUnfolding(float, unsigned int, bool = true,
				 const std::string & = "", const Taglut::IndentManager & = Taglut::IndentManager()) {
      /* will be defined in other classes */
      return -1;
    }


    /**
       clone instruction
    */
    virtual UnfoldingMethod * clone(Mapping2D3D & mapping);

    /**
       Init unfolding data using 2d location of points
    */
    virtual void initDataUsing2D() { /* will be defined in other classes */ }

    /**
       Get number of points in the manipulated mapping
    */
    VertexID size() const;

    /**
       Get the associated mesh
    */
    inline Mesh & getMesh() { return mesh; }

    /**
       Get the associated mesh
    */
    inline const Mesh & getMesh() const { return mesh; }

    /**
       Get the associated mesh
    */
    inline Mapping2D3D & getMapping() { return mapping; }

    /**
       Get the associated mesh
    */
    inline const Mapping2D3D & getMapping() const { return mapping; }

    /**
       Clear all tempory data
    */
    virtual void clearTempData() {}

  };

}

#endif
