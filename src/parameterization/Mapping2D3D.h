/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MAPPING2D3D
#define MAPPING2D3D

#include <deque>
#include <list>
#include <string>
#include <fstream>

#include "Mesh.h"
#include "Point2D3D.h"
#include "Point2D.h"
#include "Coord2D.h"
#include "ClusterGenerator.h"
#include "IndentManager.h"
#include "UnfoldingMethod.h"
#include "FileManipulator.h"
#include "MeshMap.h"

#ifdef USE_LIBXML
#include <libxml/xmlreader.h>
#endif

namespace Taglut {
  class UnfoldingAlgorithm;
  class MeshPrune;
  class MeshPathes;
  class PointOnEdge;

  /**
     @class Mapping2D3D

     @author Jean-Marie Favreau
     @brief Set of 2d/3d points used by unfolding algorithms
  */
  class Mapping2D3D : public std::deque<Point2D>, public FileManipulator {
  protected:

    /** Min x value */
    double minXValue;
    /** max x value */
    double maxXValue;
    /** min y value */
    double minYValue;
    /** max y value */
    double maxYValue;

    /**
       Original 3D mesh
    */
    Mesh * mesh;

    /**
       Cluster generator
    */
    ClusterGenerator clusterG;

    /**
       list of extrema of the 3D surface
    */
    std::deque<VertexID> extrema;

    /**
       Flag to check the flatten state
    */
    bool flatten;

    /**
       Given a new point location (x, y), update 2d extrema.
    */
    void update2DExtrema(const Coord2D & pt);

    /**
       Draw border of the mapping on the given image
    */
    void drawBorder(UCharCImg & outImage, const VertexID & bCircleIt, const unsigned char & color);

    /**
       Draw triangles of the mapping on the given image
    */
    void drawBorderTriangles(UCharCImg & outImage, const VertexID & bCircleIt, const unsigned char & color);


    /**
       Clear clusters
    */
    void clearClusters();

    /**
       First method of mapping adjustement
    */
    unsigned int adjustMapping1(unsigned int postTreatmentIt, double epsilon);

    /**
       reset extrema values
    */
    void reset2DExtrema();

    void addFormats();

    /**
       Look for the wanted object in the file
    */
    bool findObjectOBJ(std::ifstream & file, const std::string & objectName);

    /**
       Load the mapping from an obj file
    */
    bool loadOBJ(std::ifstream & file);


    /**
       Check the ply format, reading the header
    */
    bool checkFormatPLY(std::ifstream & file);

  public:

    /**
       empty Mapping2D3D.
    */
    Mapping2D3D();

    /**
       Mapping2D3D is defined by a mesh.
    */
    Mapping2D3D(Mesh & mesh);

    /**
       copy constructor.
    */
    Mapping2D3D(const Mapping2D3D & mapping);

    virtual inline ~Mapping2D3D() {}

#ifndef SWIG
    /**
       affectation operator
    */
    Mapping2D3D & operator=(const Mapping2D3D & mapping);
#endif

    /**
     * @brief setPoints init the structure using a list of points
     * @param points a list of points
     * @return
     */
    Mapping2D3D & setPoints(const std::list<Point2D> & points);

    /** reinitialization */
    void reinit();

    /** given a point id and a new coord, set location point and update 2d extrema */
    void set2DCoords(VertexID idPoint, double x, double y);

    /** reset 2d coords (clearing 2d extrema)  */
    void reset2DCoords();

    /** return the 2D coordinate associated to the given point on edge */
    Coord2D get2DCoord(const PointOnEdge & pOe) const;

    /** draw unfolded map on the output image, drawing triangles with the
     map defined by \p mmap for each vertex */
    void toImageFromMap(UCharCImg & outImage, const MeshMap & mmap);

    /**
       Draw unfolded map on the output image, drawing triangles by additive method
       to display overlaps.
    */
    void toImageOverlap(UCharCImg & outImage);

    /**
       Draw unfolded map on the output image (using color from colorValues data)
    */
    void toImage(UCharCImg & outImage, const UCharCImg & colorValues);

    /**
       Draw unfolded map on the output image (using color from colorValues image)
    */
    void toImage(UCharCImg & outImage, const CImgList<unsigned char> & colorValues);

    /**
       Draw unfolded map on the output image (using random color)
    */
    void toImageRandom(UCharCImg & outImage);

    /**
       Draw map's border from current Mapping2D3D on given the image.
    */
    void toImageDrawBorder(UCharCImg & outImage, const unsigned char & color);

    /**
       Draw triangles' border from current Mapping2D3D on the output image.
    */
    void toImageDrawBorderTriangles(UCharCImg & outImage, const unsigned char & color);

    /**
       Draw texture using current Mapping2D3D 2d/3d correspondance on the output image.
    */
    void toImageDrawTexture(UCharCImg & outImage, const UCharCImg & texture, float zratio = 1);

    /**
       Draw a distance map using current Mapping2D3D 2d/3d correspondance and a location on the output image.
    */
    void toImageDrawDistanceMap(UCharCImg & outImage, const Point3D & point);

    /**
       Draw a normal map using current Mapping2D3D 2d/3d.
    */
    void toImageDrawNormals(UCharCImg & outImage);

    /**
       Draw a colored representation of the distortion using current Mapping2D3D 2d/3d.
    */
    void toImageDrawDistortions(UCharCImg & outImage);

    /**
       Draw the given points in image.
    */
    void toImageDrawPoints(UCharCImg & outImage, const std::deque<VertexID> & points);

    /**
       Draw stats of the unfolding result on the output image
    */
    UCharCImg toImageStats(unsigned int sizex, unsigned int sizey);

    /**
       Update 2d location for each circle using (minX, minY)-(maxX, maxY)
       extrema.
    */
    void resize2DCoords(double minX, double minY, double maxX, double maxY);

    /** return triangle selecting it by id */
    inline const Triangle & getTriangle(TriangleID t) const { return (*mesh).triangle(t); }

    /** return number of triangles  */
    inline TriangleID getNbTriangles() const { return (*mesh).getNbTriangles(); }

    /** return 3d points. Deprecated. */
    inline CImgList<double> getCImgPoints() { return (*mesh).getCImgPoints(); }

    /** return triangles. Deprecated. */
    inline CImgList<VertexID> getCImgPrimitives() { return (*mesh).getCImgPrimitives(); }

    /** return 3d points. */
    inline const std::vector<Point3D> & getPoints() const { return (*mesh).getPoints(); }

    /** return triangles. */
    inline const std::vector<Triangle> & getTriangles() const { return (*mesh).getTriangles(); }

    /** mesh accessor */
    inline Mesh & getMesh() { return (*mesh); }

    /** mesh accessor */
    inline const Mesh & getMesh() const { return (*mesh); }

    /**
       Given complete texture image, build texture list using 2d location.
    */
    void buildTexturedTriangleList(const UCharCImg & image, CImgList<VertexID> & primitives);

    /**
       Given complete texture image, build texture list using 3d location.
    */
    void buildTexturedTriangleList(const UCharCImg & image, CImgList<VertexID> & primitives, CImgList<unsigned char> & textures, double zratio = 1);

    /** return true if edge (i1, i2) is an edge. */
    inline bool isBoundaryEdge(VertexID i1, VertexID i2) const { return (*mesh).isBoundaryEdge(i1, i2); }

    /**
       Compute clusters in a mesh using length distorsion information
       @param limitRatio Clusters with mean ratio smaller than this parameter are removed.
       @param nbClusters Maximum number of clusters kept. if value = -1, keep all clusters
       @param cMethod Clustering method
       @param maxCurv Maximum curvature value
       @param iManager indent manager
    */
    void computeClusters(double limitRatio = -1, int nbClusters = -1,
			 const enum ClusterGenerator::ClusteringMethod & cMethod = ClusterGenerator::CMratiosimple,
			 double maxCurv = 0.2,
			 const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Compute the extrema points of the mesh. Alternative method to the cluster approach
    */
    void computeExtremaFromBorder(const LengthEdge & le, bool smooth = false, bool useMean = false, bool useMedian = false, int nbSelected = -1, const std::string & logFile = "");

    /**
       Build an image of clusters
    */
    UCharCImg clustersToImage(unsigned int sizex, unsigned int sizey);

    /**
       Build an image of extrema
    */
    UCharCImg extremaToImage(unsigned int sizex, unsigned int sizey);

    /**
       Get center of clusters
    */
    inline const std::deque<VertexID> & getExtrema() const {
      return extrema;
    }

    /**
       Return string infos about clusters
    */
    inline std::string getClusterInfos() const { return clusterG.getInfos(); }


    /**
       Find point that 2d coords are nearest to (x2d, y2d)
    */
    VertexID getNearestPointId(double x2d, double y2d) const;

    /**
       Find point that 2d coords are nearest to (x2d, y2d)
    */
    const Point2D3D getNearestPoint(double x2d, double y2d) const;


    /**
       Find point that 3d coords are nearest to (x3d, y3d, z3d)
    */
    const Point2D3D getNearestPoint(double x3d, double y3d, double z3d) const;

    /**
       Assumes that mesh has been updated (cut).
       Points added in mesh are now added in mapping, using mPathes and its projection property.
    */
    void updatePoints(const MeshPathes & mPathes);

    /** compute unfolded coordinates : (x, y) from 2d location  */
    void computeUnfoldedCImgPointsRedim(CImgList<double> & uPoints, double minX, double minY, double maxX, double maxY, double z = 0);

    /** compute unfolded coordinates : (x, y) from 2d location.
	Min and max coordinate values are computed from initial mesh.  */
    void computeUnfoldedCImgPointsRedim(CImgList<double> & uPoints, double z = 0);

    /** compute unfolded coordinates : (x, y) from 2d location.  */
    void computeUnfoldedCImgPoints(CImgList<double> & uPoints, double z = 0);

    /** compute for each triangle the ratio between the original size and the size on unfolded mesh
     */
    void computeTriangleRatios(CImgList<unsigned char> & ratios) const;


    /**
       Load Mapping2D3D from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Load Mapping2D3D from an xml file
    */
    void loadXML(const std::string & filename);

#ifdef LIBXML_READER_ENABLED
    void loadXML(xmlTextReaderPtr & reader);
#endif

    /**
       Load Mapping2D3D from a obj file
    */
    void loadOBJ(const std::string & fileName, const std::string & objectName = "");


    /**
       Load Mapping2D3D from a ply file
    */
    void loadPLY(const std::string & fileName);

    /**
       Save Mapping2D3D from file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;

    /**
       Export the mapping (2D locations, edges' length, edges' ratio) in files with prefix given by \p prefix
    */
    void exportMapping(const std::string & prefix) const;

    /** export a string representation in UMS format */
    std::string toStringUMS() const;

    /** export a string representation in XML format */
    std::string toStringXML() const;


    /**
       Compute 2D barycenter of the complete mapping
    */
    Coord2D compute2DBarycenter() const;

    /**
       Compute 2D barycenter of the given points
    */
    Coord2D compute2DBarycenter(const std::deque<VertexID> & points) const;

    /**
       Compute 2D barycenter of a given triangle
    */
    inline Coord2D compute2DBarycenter(const Triangle & t) const {
      std::deque<VertexID> cList;
      cList.push_back(t.getP1());
      cList.push_back(t.getP2());
      cList.push_back(t.getP3());
      return compute2DBarycenter(cList);
    }

    /**
       Help message for adjusting methods
    */
    static std::string helpAdjustMapping();

    /**
       Adjust cutting using method given by idMethod, with a maximum iteration given by
       maxIt, or when error is less than epsilon.
    */
    unsigned int adjustMapping(unsigned int idMethod, unsigned int maxIt, double epsilon, Taglut::IndentManager & iManager);


    /**
       Compute 2D location using a prune mesh and its 2d mapping using interpolation method
    */
    void compute2DLocation(MeshPrune & meshPrune, const Mapping2D3D & mapping);

    /**
       Translate the 2D location of the map using \p tr
    */
    void translate(const Coord2D & tr);

    /**
       rotate the 2D location of the map using the given \p angle arround the given \p center
    */
    void rotate(double angle, const Coord2D & center);

    /**
       scale the 2D location of the map using the given \p ratio using the given \p center
    */
    void scale(double ratio, const Coord2D & center);

    /**
       flip the 2D location using the y axis
    */
    void flip();

    /**
       Return a string description of the box including the mapping (2D)
    */
    std::string getBox2DSize() const;

    /**
       Update 2D location of the point \p idP using \p coords coordinates on the barycentric system (idP1, idP2)
    */
    void compute2DLocationBarycenterCoords(const Coord2D & coords, VertexID idP, VertexID idP1, VertexID idP2);

    /**
       Update 2D location of the point \p idP using \p coords coordinates on the barycentric system (idP1, idP2, idP3)
    */
    void compute2DLocationBarycenterCoords(const Coord3D & coords, VertexID idP, VertexID idP1, VertexID idP2, VertexID idP3);

    /**
       return true if the mapping is computed
    */
    bool isFlatten() const;

    /**
       set the flatten state
    */
    void setFlatten();

    /**
       Adjust an allready unfolded mapping according to the mean distance
    */
    void adjust2DLocationMeanDistance();


    /**
       iterator from Mesh
    */
    Mesh::point_iterator point3D_begin() { return (*mesh).point_begin(); }

    /**
       iterator from Mesh
    */
    Mesh::const_point_iterator point3D_begin() const { return (*mesh).point_begin(); }

    /**
       iterator from Mesh
    */
    Mesh::point_iterator point3D_end() { return (*mesh).point_end(); }

    /**
       iterator from Mesh
    */
    Mesh::const_point_iterator point3D_end() const { return (*mesh).point_end(); }

    /**
       Accessor for the mapping's points
    */
    inline const Point2D & point2D(VertexID p) const {
      return (*this)[p];
    }

    /**
       Accessor for the mesh's points
    */
    inline Point3D & point3D(VertexID p) {
      return (*mesh).point(p);
    }

    /**
       Accessor for the mesh's points
    */
    inline const Point3D & point3D(VertexID p) const {
      return (*mesh).point(p);
    }

  };

}

#endif
