/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef REGISTRATION
#define REGISTRATION
#include "MeshMatcher.h"

#include "Coord2D.h"

namespace Taglut {
  class Mapping2D3D;
  class IndentManager;


  /**
     @class MappingRegistration

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-01-22
     @brief Affine registration of mappings
  */
  class MappingRegistration {
  private:
    /**
       Original mapping
    */
    Mapping2D3D & mapping;

    /**
       Reference mapping
    */
    const Mapping2D3D & reference;

    /**
       Matching tool for points' correspondance
    */
    MeshMatcher mMatcher;

    /**
       return median of the current list, first sorting it
    */
    static double getMedian(std::deque<double> & alphas);

    /**
       return true if the mapping need a flip before rotation
    */
    bool needFlip() const;

    /**
       Compute the z component of the cross product defined by (p1, p2), (p1, p3)
    */
    static double crossProduct(const Coord2D & p1, const Coord2D & p2, const Coord2D & p3);

    /**
       Using a naive method to reorient (rotate) the mapping
    */
    void naiveRotation(const Coord2D & center, const Taglut::IndentManager & iManager);

  public:
    /**
       Default constructor
    */
    MappingRegistration(Mapping2D3D & mapping, const Mapping2D3D & reference);

    /**
       Apply registration
    */
    void apply(const Taglut::IndentManager & iManager);

  };
}

#endif
