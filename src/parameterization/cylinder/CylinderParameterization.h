/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-GE CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef CYLINDER_PARAMETERIZATION
#define CYLINDER_PARAMETERIZATION

#include "BarycentricMapping.h"
#include "OpenNLSolver.h"
#include "IDTypes.h"
#include "Mesh.h"
#include "Coord2D.h"

namespace Taglut {

  class TwoCirclesBorderParameterization {
  protected:
    /** boundary points */
    VertexID bdPoint[2][2];

    /** radius of the boundary circles */
    double radius;
    /** length of the cylinder */
    double length;
    /** true if length and radius has to be adjusted according to the input mesh */
    bool adjust;

    std::vector<VertexID> parameterizeBorderAngles(Mesh & mesh,
						   std::vector<double> & angles,
						   std::vector<bool> & firstSide);

    void parameterizeBorder3D(Mesh & mesh, Mesh & resultMesh);

    void parameterizeBorder(Mapping2D3D & mapping);

    void adjustParameters(Mesh & mesh, const std::vector<VertexID> * bd);

    inline void initValues(double radius_t = 1., double length_t = 2.) {
      bdPoint[0][0] = bdPoint[0][1] = bdPoint[1][0] = bdPoint[1][1] = 0;
      radius = radius_t;
      length = length_t;
      adjust = false;
    }
    inline void setAdjust(bool a) {
      adjust = a;
    }

    inline void setBPoints(VertexID bdPoints1[2], VertexID bdPoints2[2]) {
      bdPoint[0][0] = bdPoints1[0];
      bdPoint[0][1] = bdPoints1[1];
      bdPoint[1][0] = bdPoints2[0];
      bdPoint[1][1] = bdPoints2[1];
    }

  public:
    TwoCirclesBorderParameterization() {
      initValues();
    }

    /** compute the angle located between angle1 modulo [startAngle, startAngle + 2pi] and
	angle2 modulo [startAngle, startAngle + 2pi], and located at the given ratio.
	Two sides are possible (1->2) or (2->1). The point is in the smallest side.
	If the two sides are equal, the initial ordering is choosen.
    */
    static double getAngle(double angle1, double angle2, double ratio, double startAngle) {
      return getValue(angle1, angle2, startAngle, startAngle + 2 * M_PI, ratio);
    }


    /** compute the value located between v1 modulo [start, end] and
	v2 modulo [start, end], and located at the given ratio.
	Two sides are possible (1->2) or (2->1). If \p direct=false, the point is in the smallest side.
	Otherwise, or if the two sides are equal, the initial ordering is choosen.
    */
    static double getValue(double v1, double v2, double start, double end, double ratio, bool direct = false);

    /** return the direct distance between the two given points located in [start, end] (modulo)
	in the direct order (v1->v2).
     */
    static double getDirectDistance(double v1, double v2, double start, double end);

    /**
       return true if ov is between v1 and v2
     */
    static bool inBetween(double v1, double v2, double start, double end, double ov);
  };

  /**
     @class CylinderParameterization
     @author Jean-Marie Favreau (LIMOS / Blaise Pascal Univ. / IMATI-GE)
     @brief Implementation of a barycentric mapping of a tubular surface by cylinder
  */
  class CylinderParameterization : public MeanValuesParameterization<TwoCirclesBorderParameterization> {
  private:
    bool epsilonbdPoints;
  protected:


    virtual void initSolutionVector(OpenNLSolver & solver, unsigned char i);

    /** compute a plane that corresponds to the thinnest part of the mesh, and use
	intersections between this plane and edges of the mesh to compute the barycenter */
    Coord2D getMiddleAxisByPlane(Mesh & mesh) const;

    /** ratio corresponds to the proportion of the cylinder (in z direction) that
	will be used to compute the barycenter. */
    Coord2D getMiddleAxisBySection(Mesh & mesh, double ratio = .3) const;

    void projectOnCylinder(Coord3D & point, const Coord2D & middle) const;

    void projectMeshOnCylinder(Mesh & mesh) const;

    Mesh parameterize3DByIL(Mesh & mesh);

    Mesh parameterize3DBy4IL(Mesh & mesh);

    double getAngle(const Mesh & mesh, const PointOnEdge & point) const;

    double getAngle(const Mesh & mesh, VertexID point) const;

    void addEpsilonBdPoints(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2]);

    PLPath getLineFromPoint(VertexID v, const Mesh & mesh, const MeshMap & mmap, const VectorField & vf) const;

    PointOnEdge getPointOnLineFromPoint(VertexID v1, VertexID v2,
					double d1, double d2,
					const Mesh & mesh) const;

  public:
    // default constructor
    CylinderParameterization(bool epsilon_bdPoints = true, double radius_t = 1., double length_t = 2.) : epsilonbdPoints(epsilon_bdPoints) {
      initValues(radius_t, length_t);
    }

    /** accessor */
    inline double getRadius() const { return radius; }
    /** accessor */
    inline double getLength() const { return length; }

    /** compute the parameterization given two points on each boundary
	to define the border parameterization
	@param mesh The mesh to be parameterized (required: it have to be a cylinder)
	@param bdPoints1 Two points in the first boundary
	@param bdPoints2 Two points in the second boundary
	@param project Project the resulting mesh on the cylinder
	@param adjust Adjust the radius and length of the resulting structure according to the average of the length of the boundaries, and the length of the shortest path between the two boundaries

    */
    inline Mesh parameterize3DFixedPoints(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2], bool project = false, bool adjust_t = true) {
      setAdjust(adjust_t);
      // select points on boundary
      setBPoints(bdPoints1, bdPoints2);

      // compute the parameterization
      Mesh r = parameterize3D(mesh);
      if (project)
	projectMeshOnCylinder(r);

      if (epsilonbdPoints)
	addEpsilonBdPoints(r, bdPoints1, bdPoints2);

      (*this).adjust = false;
      return r;
    }

    /** compute the parameterization given two points on each boundary
	to define the border parameterization. Using a mixed method between Floater parameterization
	and integral line usage (naive approach)
	@param mesh The mesh to be parameterized (required: it have to be a cylinder)
	@param bdPoints1 Two points in the first boundary
	@param bdPoints2 Two points in the second boundary
	@param adjust Adjust the radius and length of the resulting structure according to the average of the length of the boundaries, and the length of the shortest path between the two boundaries

    */
    inline Mesh parameterize3DFixedPointsByIL(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2], bool adjust_t = true) {
      setAdjust(adjust_t);
      // select points on boundary
      setBPoints(bdPoints1, bdPoints2);

      Mesh r = parameterize3DByIL(mesh);

      if (epsilonbdPoints)
	addEpsilonBdPoints(r, bdPoints1, bdPoints2);

      (*this).adjust = false;
      return r;
    }

    /** compute the parameterization given two points on each boundary
	to define the border parameterization. Using a mixed method between Floater parameterization
	and integral line usage (use only 4 integral lines)
	@param mesh The mesh to be parameterized (required: it have to be a cylinder)
	@param bdPoints1 Two points in the first boundary
	@param bdPoints2 Two points in the second boundary
	@param adjust Adjust the radius and length of the resulting structure according to the average of the length of the boundaries, and the length of the shortest path between the two boundaries

    */
    inline Mesh parameterize3DFixedPointsBy4IL(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2], bool adjust_t = true) {
      setAdjust(adjust_t);
      // select points on boundary
      setBPoints(bdPoints1, bdPoints2);

      Mesh r = parameterize3DBy4IL(mesh);

      if (epsilonbdPoints)
	addEpsilonBdPoints(r, bdPoints1, bdPoints2);

      (*this).adjust = false;
      return r;
    }

    /** compute only the Z axis parameterization of the cylinder */
    MeshMap parameterizeAxisDirection(Mesh & mesh);

    /** compute only the Z axis parameterization of the cylinder */
    MeshMap parameterizeAxisDirectionFixedPoints(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2], bool adjust_t = true) {
      setAdjust(adjust_t);
      // select points on boundary
      setBPoints(bdPoints1, bdPoints2);

      return parameterizeAxisDirection(mesh);
    }


    inline Mapping2D3D parameterizeFixedPoints(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2], bool adjust_t = true) {
      setAdjust(adjust_t);
      // select points on boundary
      setBPoints(bdPoints1, bdPoints2);

      Mapping2D3D r = parameterize(mesh);

      (*this).adjust = false;
      return r;
    }

    /** given a mesh result of a cylindrical parameterization,
	it estimates the mean square error of the triangle normals with the
	direction defined by the center of triangles and their projection on the middle axis */
    static double computeNormalsMSE(const Mesh & mesh);

    /** given a  mesh result of a cylindrical parameterization,
	it estimates the worst distance between an edge and the cylinder.
	if the distance if less than epsilon time the radius, it returns -1. */
    static double computeWorstDistance(const Mesh & mesh, double epsilon = -1.);

  };

}

#endif
