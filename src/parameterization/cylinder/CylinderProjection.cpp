/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2013 Jean-Marie Favreau <J-Marie.Favreau@udamail.fr>
 *                    ISIT, UMR 6284 UdA - CNRS
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "CylinderProjection.h"
#include "Point3D.h"

using namespace Taglut;

CylinderProjection::CylinderProjection(Mapping2D3D & mapping): UnfoldingMethod(mapping),
                                                               center(0., 0., 0.), axis(0., 0., 0.), line(center, axis) {
}

CylinderProjection::CylinderProjection(Mapping2D3D & mapping,
                                       const Coord3D & ax,
                                       const Coord3D & ce) : UnfoldingMethod(mapping),
                                                             center(ce), axis(ax), line(ce, ax) {
}


CylinderProjection::CylinderProjection(const CylinderProjection & cProjection) : UnfoldingMethod(cProjection),
                                                                                 center(cProjection.center),
                                                                                 axis(cProjection.axis),
                                                                                 line(cProjection.line) {

}

UnfoldingMethod * CylinderProjection::clone(Mapping2D3D & mapping_t) {
  UnfoldingMethod * result = new CylinderProjection(mapping_t);

  mapping.reset2DCoords();


  mapping.setFlatten();
  return result;
}


void CylinderProjection::compute2DLocation(const Taglut::IndentManager & indentManager, bool adjustMeanDistance) {
  assert(axis.norm() != 0.);
  initAxis();

  VertexID fP = mesh.getMiddlePoint().getId();
  VertexID nbBad = 0;
  estimateRadius();

  mapping.reset2DCoords();

  mesh.setTriangleFlag(0);
  mesh.setPointFlag(0);
  Point3D & firstPoint = mapping.point3D(fP);

  Point3D & secondPoint = mapping.point3D(mesh.point(firstPoint.getId()).getNeighbours().front());
  assert(firstPoint.getId() == fP);
  assert(secondPoint.getId() == mesh.point(firstPoint.getId()).getNeighbours().front());

  firstPoint.setFlag(1);
  mapping.set2DCoords(firstPoint.getId(), 0.0, 0.0);

  compute2DLocation(secondPoint.getId(), firstPoint.getId());

  std::queue<std::pair<VertexID, VertexID> > open;
  open.push(std::pair<VertexID, VertexID>(firstPoint.getId(), secondPoint.getId()));

  while(!open.empty()) {
    assert(open.size() < mesh.getNbTriangles());
    std::pair<VertexID, VertexID> edge = open.front();
    open.pop();

    // first triangle
    Triangle & t = mesh.findTriangle(edge.first, edge.second);
    if (t.getFlag() == 0) {
      if (!compute2DLocation(t, edge))
    ++nbBad;
      VertexID otherPoint = t.getOtherPoint(edge.first, edge.second);
      open.push(std::pair<VertexID, VertexID>(otherPoint, edge.first));
      open.push(std::pair<VertexID, VertexID>(edge.second, otherPoint));
    }

    // other triangle if exists
    try {
      Triangle & t2 = mesh.findOtherTriangle(edge.first, edge.second, t.getId());
      if (t2.getFlag() == 0) {
    if (!compute2DLocation(t2, edge))
      ++nbBad;
    VertexID otherPoint = t2.getOtherPoint(edge.first, edge.second);
    open.push(std::pair<VertexID, VertexID>(otherPoint, edge.first));
    open.push(std::pair<VertexID, VertexID>(edge.second, otherPoint));
      }
    }
    catch (Exception) {}
  }
  if (nbBad != 0)
    std::cout << indentManager << "Warning: " << nbBad << " points has been computed with a bad location" << std::endl;

  mapping.setFlatten();

  if (adjustMeanDistance)
    mapping.adjust2DLocationMeanDistance();

}

int CylinderProjection::processUnfolding(float, unsigned int, bool, const std::string &, const Taglut::IndentManager &) {
    cylinderProjection.clear();
    initAxis();

    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p) {
        cylinderProjection.push_back(((*p).projectionOnAxis(line) - *p).normalize());
    }
    return 1;
}

bool CylinderProjection::compute2DLocation(Triangle & t, const std::pair<VertexID, VertexID> & edge) {
  t.setFlag(1);
  return compute2DLocation(t.getOtherPoint(edge.first, edge.second), edge.first);
}

bool CylinderProjection::compute2DLocation(VertexID vnew, VertexID vknown) {
    // points
    const Coord3D & ccnew = mesh.point(vnew);
    const Coord3D & ccknown = mesh.point(vknown);

    // projection of the points in the axis
    const Coord3D cnew = ccnew.projectionOnAxis(line);
    const Coord3D cknown = ccknown.projectionOnAxis(line);
    // distance between the two projections plus the x coordinate of the known point gives the new x coordinate
    const double x = mapping[vknown].get2DX() + line.getCoords(cnew) - line.getCoords(cknown);
    // projection of the known point in the base plane of the cylinder, and obtain the corresponding vector
    const Coord3D pknown = plane.getOrthogonalProjection(cylinderProjection[vknown]);
    // projection of the new point in the base plane of the cylinder, and obtain the corresponding vector
    const Coord3D pnew = plane.getOrthogonalProjection(cylinderProjection[vnew]);

    // the y coordinate corresponds to the one of y plus the angle difference in the base plane
    const bool d = Coord3D::scalarProduct(axis, Coord3D::vectorialProduct(pnew, pknown)) > 0;
    const double y = mapping[vknown].get2DY() + Coord3D::angleVector(pnew, pknown) * (d ? radius : (- radius));
    mapping.set2DCoords(vnew, x, y);
    mesh.point(vnew).setFlag(1);
    return true;
}

void CylinderProjection::initAxis() {
    if (axis.norm() == 0.) {
        std::deque<Coord3D> x = mesh.computePCAAxes();
        axis = x.front().normalize();
        center = mesh.getIsobarycenter();
    }
    line = Line3D(center, axis);
    plane = Plane3D(center, axis);
}

void CylinderProjection::estimateRadius() {
    assert(axis.norm() != 0.);
    radius = 0.;
    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p) {
        const double d = (*p).distance(line);
        if (d > radius)
            radius = d;
    }
 }
