/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-GE CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include <assert.h>
#include <math.h>
#include "CylinderParameterization.h"
#include "Mesh.h"
#include "MeshManipulator.h"
#include "VectorField.h"
#include "StringManipulation.h"
#include "FileTools.h"

#ifdef USE_OMP
#include <omp.h>
#endif

using namespace Taglut;


std::vector<VertexID> TwoCirclesBorderParameterization::parameterizeBorderAngles(Mesh & mesh,
										 std::vector<double> & angles,
										 std::vector<bool> & firstSide) {
  if ((bdPoint[0][0] == bdPoint[0][1]) ||
      (bdPoint[1][0] == bdPoint[1][1]))
    throw Exception("parametrizeBorder3D(2): no border points selected");

  MeshManipulator mManip(mesh);
  std::vector<VertexID> bd[2] = { mManip.getBoundaryFromBPoint(bdPoint[0][0]), mManip.getBoundaryFromBPoint(bdPoint[1][0])};

  if (adjust)
    adjustParameters(mesh, bd);

  std::vector<VertexID> result;
  angles.clear();
  firstSide.clear();

  for(unsigned char b = 0; b != 2; ++b) {
    const unsigned char b2 = b == 0 ? 1 : 0;
    // first compute the number of points on each side
    unsigned int nbFirstBow = 0;
    if (bd[b].front() != bdPoint[b][0])
      throw Exception("parametrizeBorder3D(2): incoherent boundary");
    for(std::vector<VertexID>::const_iterator bdb = bd[b].begin(); bdb != bd[b].end(); ++bdb, ++nbFirstBow) {
      if (((*bdb) == bdPoint[b2][0]) ||
	  ((*bdb) == bdPoint[b2][1]))
	throw Exception("parametrizeBorder3D(2): incoherent boundaries");
      if ((*bdb) == bdPoint[b][1])
	break;
    }
    if ((nbFirstBow == bd[b].size() - 1) && (bd[b].back() != bdPoint[b][1]))
      throw Exception("parametrizeBorder3D(2): incoherent boundary (missing point)");

    // compute the length of each subpart of the border
    double sizes[2] = {0., 0.};
    unsigned char side = 0;
    std::vector<VertexID>::const_iterator bdbpred = bd[b].begin();
    for(std::vector<VertexID>::const_iterator bdb = bd[b].begin() + 1; bdb != bd[b].end(); ++bdb, ++bdbpred) {
      sizes[side] += mesh.point(*bdbpred).distance(mesh.point(*bdb));
      if (*bdb == bdPoint[b][1]) {
	assert(side == 0);
	++side;
      }
    }
    assert(bd[b].front() != bd[b].back());
    sizes[1] += mesh.point(bd[b].front()).distance(mesh.point(bd[b].back()));
    assert(sizes[0] != 0.);
    assert(sizes[1] != 0.);
    sizes[0] = M_PI / sizes[0]; // it corresponds to a ratio
    sizes[1] = M_PI / sizes[1];

    // then adjust the location of the boundary points in the result mesh
    unsigned int i = 0;
    double angle = 0.;
    bool inverse = true;
    side = 0;
    // adjust the order
    {
      assert(bd[b].size() > 1);
      const Triangle & t = mesh.findTriangle(bd[b].front(), bd[b][1]);
      const bool o = t.sameOrder(bd[b].front(), bd[b][1]);
      if (((b == 0) && o) ||
	  ((b == 1) && !o))
	inverse = false;
    }
    if (inverse) {
      angle = 2 * M_PI;
    }
    {
      std::vector<VertexID>::const_iterator bdbpred = bd[b].begin();
      angles.push_back(angle);
      firstSide.push_back(b == 0);
      result.push_back(*bdbpred);
      for(std::vector<VertexID>::const_iterator bdb = bd[b].begin() + 1; bdb != bd[b].end(); ++bdb, ++bdbpred, ++i) {
	angle += (inverse ? -1 : 1) * mesh.point(*bdb).distance(mesh.point(*bdbpred)) * sizes[side];
	assert(angle >= 0.);
	assert(angle <= 2 * M_PI);
	angles.push_back(angle);
	firstSide.push_back(b == 0);
	result.push_back(*bdb);
	if (*bdb == bdPoint[b][1])
	  side += 1;
      }
    }

  }


  assert(result.size() == angles.size());
  assert(result.size() == firstSide.size());
  return result;

}

void TwoCirclesBorderParameterization::parameterizeBorder(Mapping2D3D & mapping) {
  std::vector<double> angles;
  std::vector<bool> firstSide;
  std::vector<VertexID> borders = parameterizeBorderAngles(mapping.getMesh(), angles, firstSide);
  double ratio = length / radius;

  std::vector<double>::const_iterator a = angles.begin();
  std::vector<bool>::const_iterator s = firstSide.begin();
  for(std::vector<VertexID>::const_iterator b = borders.begin(); b != borders.end(); ++b, ++a, ++s) {
    mapping[*b].set2DX((1 + (*s) ? ratio : 0) * cos(*a));
    mapping[*b].set2DY((1 + (*s) ? ratio : 0) * sin(*a));
  }
}


void TwoCirclesBorderParameterization::parameterizeBorder3D(Mesh & mesh, Mesh & resultMesh) {
  std::vector<double> angles;
  std::vector<bool> firstSide;
  std::vector<VertexID> borders = parameterizeBorderAngles(mesh, angles, firstSide);

  std::vector<double>::const_iterator a = angles.begin();
  std::vector<bool>::const_iterator s = firstSide.begin();
  for(std::vector<VertexID>::const_iterator b = borders.begin(); b != borders.end(); ++b, ++a, ++s) {
    resultMesh.point(*b).setX(radius * cos(*a));
    resultMesh.point(*b).setY(radius * sin(*a));
    resultMesh.point(*b).setZ((!(*s)) * length);
  }
}

void CylinderParameterization::initSolutionVector(OpenNLSolver & solver, unsigned char i) {
  float value = 0.;
  if (i == 2)
    value = length * .5;

  for(VertexID v = 0; v != solver.getSize(); ++v)
    solver.setVariable(v, value);

}

Coord2D CylinderParameterization::getMiddleAxisByPlane(Mesh & mesh) const {
  // get the points on the boundaries, with maximal X
  VertexID v1, v2;
  {
    double x1 = 0., x2 = 0.;
    const double m = length / 2;
    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      if ((*p).getIsBoundary()) {
	const double x = (*p).getX();
	if ((*p).getZ() > m) {
	  if (x < x2) {
	    v2 = (*p).getId();
	    x2 = x;
	  }
	}
	else {
	  if (x < x1) {
	    v1 = (*p).getId();
	    x1 = x;
	  }
	}
      }
  }

  // compute a shortest path between these two points
  MeshManipulator mManip(mesh);
  std::deque<VertexID> path = mManip.computeShortestPath(v1, v2);


  // get the point along the path with the minimal X value
  VertexID v = path.front();
  double x = mesh.point(v).getX();
  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
    const double px = mesh.point(*p).getX();
    if (px > x) {
      x = px;
      v = *p;
    }
  }

  // build the corresponding plane
  Plane3D plane(mesh.point(v), Coord3D(0., 0., 1.));

  // compute intersection between this plane and the mesh (i.e. with edges)
  std::vector<Coord2D> points;
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin();
	nb != (*p).getNeighbours().end(); ++nb)
      if (*nb > (*p).getId()) {
	if (!plane.sameSide(*p, mesh.point(*nb))) {
	  Line3D line(*p, *p - mesh.point(*nb));
	  Coord3D i = plane.getIntersection(line);
	  points.push_back(Coord2D(i.getX(), i.getY()));
	}
      }
  assert(points.size() > 1);
  // compute the isobarycenter of these intersections
  return Coord2D::getIsoBarycenter(points);
}

Coord2D CylinderParameterization::getMiddleAxisBySection(Mesh & mesh, double ratio) const {
  const double min = length * (1 - ratio) / 2;
  const double max = length - min;
  // compute points in the wanted section
  std::vector<Coord2D> points;
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    if (((*p).getZ() >= min) && ((*p).getZ() <= max)) {
      points.push_back(Coord2D((*p).getX(), (*p).getY()));
    }
  assert(points.size() > 1);
  // compute the isobarycenter of these intersections
  return Coord2D::getIsoBarycenter(points);
}

void CylinderParameterization::projectOnCylinder(Coord3D & point, const Coord2D & middle) const {
  // for each non-boundary point, compute the intersection between the cylinder (radius, length)
  // and the line defined by the point, and the orthogonal projection of this point on the
  // middle axis (vertical, with (x,y) position defined by middle
  // two intersections are computed. The good one is the one in the same side of middle
  Coord2D p2D(point.getX(), point.getY());
  const Circle2D circle(Coord2D(0., 0.), radius);
  p2D = circle.getProjection(p2D, middle);
  point.setX(p2D.get2DX());
  point.setY(p2D.get2DY());
}

void CylinderParameterization::projectMeshOnCylinder(Mesh & mesh) const {
  Coord2D middle = getMiddleAxisBySection(mesh);
  for(Mesh::point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    if (!((*p).getIsBoundary())) {
      projectOnCylinder(*p, middle);
      assert(fabs((*p).getX() * (*p).getX() + (*p).getY() * (*p).getY() - radius * radius) < 1e-4);
    }
}

void TwoCirclesBorderParameterization::adjustParameters(Mesh & mesh, const std::vector<VertexID> * bd) {
  MeshManipulator mManip(mesh);
  radius = (mesh.getLength(bd[0]) + mesh.getLength(bd[0])) / (4 * M_PI);
  length = mManip.getGeodesicDistance(bd[0], bd[1]);
}


MeshMap CylinderParameterization::parameterizeAxisDirection(Mesh & mesh) {
  // parameterize border and z coordinate
  Mesh result = parameterize3D(mesh, false, false, true);

  // create a z-map corresponding to the new Z coordinate
  MeshMap zmap(mesh);
  for(Mesh::const_point_iterator p = result.point_begin(); p != result.point_end(); ++p)
    zmap.setValue((*p).getId(), (*p).getZ());
  return zmap;
}

Mesh CylinderParameterization::parameterize3DByIL(Mesh & mesh) {
  // parameterize border and z coordinate
  Mesh result = parameterize3D(mesh, false, false, true);

  // create a z-map corresponding to the new Z coordinate
  MeshMap zmap(mesh);
  for(Mesh::const_point_iterator p = result.point_begin(); p != result.point_end(); ++p)
    zmap.setValue((*p).getId(), (*p).getZ());

  VectorField vf(zmap, mesh);

  Mesh::point_iterator pr = result.point_begin();
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p, ++pr) {
    if (!(*p).getIsBoundary()) {
      const PointOnEdge point1 = PLPath(PointOnEdge(*p), true, mesh, zmap, vf).back();
      const PointOnEdge point2 = PLPath(PointOnEdge(*p), false, mesh, zmap, vf).back();

      const double angle1 = getAngle(result, point1);
      const double angle2 = getAngle(result, point2);

      const double r = (*pr).getZ() / length;
      double resultAngle = TwoCirclesBorderParameterization::getAngle(angle1, angle2, r, -M_PI);
      (*pr).setX(radius * cos(resultAngle));
      (*pr).setY(radius * sin(resultAngle));
    }
    else {
      assert(((*pr).getZ() == 0.) || ((*pr).getZ() == length));
    }
  }

  return result;
}


PointOnEdge CylinderParameterization::getPointOnLineFromPoint(VertexID v1, VertexID v2,
							      double d1, double d2,
							      const Mesh & mesh) const {
  return PointOnEdge(mesh.point(v1), mesh.point(v2), d1 / (d1 + d2));
}


PLPath CylinderParameterization::getLineFromPoint(VertexID v, const Mesh & mesh,
						  const MeshMap & mmap, const VectorField & vf) const {
  assert(mesh.point(v).getIsBoundary());
  const bool dir = mmap.getValue(v) <= 0.;
  const Point3D & pv = mesh.point(v);

  PLPath result(mesh);
  result.addPointAtEnd(mesh.point(v));

  VertexID v1 = mesh.getNextBPoint(v);
  VertexID v2 = mesh.getNextBPoint(v, v1);
  double d1 = pv.distance(mesh.point(v1));
  double d2 = pv.distance(mesh.point(v2));

  TriangleID t1 = mesh.getNbTriangles();

  try {
    t1 = mesh.findTriangle(v, v1, v2).getId();
  }
  catch (...) {
  }
  if (t1 != mesh.getNbTriangles()) {
    result.addPointAtEnd(getPointOnLineFromPoint(v1, v2, d1, d2, mesh));
    bool c = true;
    // unstick from borders
    while(c) {
      assert(!mesh.isBoundaryEdge(v1, v2));
      const Triangle & t2 = mesh.findOtherTriangle(v1, v2, t1);
      const VertexID vo = t2.getOtherPoint(v1, v2);
      if (mesh.isBoundaryEdge(vo, v1)) {
	d1 += mesh.point(vo).distance(mesh.point(v1));
	v1 = vo;
	t1 = t2.getId();
	result.addPointAtEnd(getPointOnLineFromPoint(v1, v2, d1, d2, mesh));
      }
      else if (mesh.isBoundaryEdge(vo, v2)) {
	d2 += mesh.point(vo).distance(mesh.point(v2));
	v2 = vo;
	t1 = t2.getId();
	result.addPointAtEnd(getPointOnLineFromPoint(v1, v2, d1, d2, mesh));
      }
      else
	c = false;
    }
  }

  PLPath r2(result.back(), dir, mesh, mmap, vf);
  return result + r2;
}

Mesh CylinderParameterization::parameterize3DBy4IL(Mesh & mesh) {
  // parameterize border and z coordinate
  Mesh result = parameterize3D(mesh, false, false, true);

  // create a z-map corresponding to the new Z coordinate
  MeshMap zmap(mesh);
  for(Mesh::const_point_iterator p = result.point_begin(); p != result.point_end(); ++p)
    zmap.setValue((*p).getId(), (*p).getZ());

  const double epsilon = zmap.getEpsilonOnEdge();
  assert(epsilon > 0.);

  assert(zmap[bdPoint[0][0]] == 0.);
  assert(zmap[bdPoint[0][1]] == 0.);
  assert(zmap[bdPoint[1][0]] == length);
  assert(zmap[bdPoint[1][1]] == length);
  zmap[bdPoint[0][0]] = -epsilon;
  zmap[bdPoint[0][1]] = -epsilon;
  zmap[bdPoint[1][0]] += epsilon;
  zmap[bdPoint[1][1]] += epsilon;

  VectorField vf(zmap, mesh);

  PLPath bdPaths[2][2] = { { getLineFromPoint(bdPoint[0][0], mesh, zmap, vf),
			     getLineFromPoint(bdPoint[0][1], mesh, zmap, vf) },
			   { getLineFromPoint(bdPoint[1][0], mesh, zmap, vf),
			     getLineFromPoint(bdPoint[1][1], mesh, zmap, vf) } };

  for(unsigned char i = 0; i != 2; ++i)
    for(unsigned char j = 0; j != 2; ++j) {
      if (!mesh.isBoundaryEdge(bdPaths[i][j].back())) {
	if (bdPaths[i][j].back().isVertex())
	  for(std::deque<VertexID>::const_iterator nb = mesh.point(bdPaths[i][j].back().getClosestVertex()).getNeighbours().begin();
	      nb != mesh.point(bdPaths[i][j].back().getClosestVertex()).getNeighbours().end(); ++nb)
	    if (mesh.point(*nb).getIsBoundary()) {
	      bdPaths[i][j].push_back(mesh.point(*nb));
	      break;
	    }
      }
    }

  assert(bdPaths[0][0].size() > 1);
  assert(bdPaths[1][0].size() > 1);
  assert(bdPaths[0][1].size() > 1);
  assert(bdPaths[1][1].size() > 1);

  /*assert(mesh.isBoundaryEdge(bdPaths[0][0].front()) && mesh.isBoundaryEdge(bdPaths[0][0].back()));
  assert(mesh.isBoundaryEdge(bdPaths[0][1].front()) && mesh.isBoundaryEdge(bdPaths[0][1].back()));
  assert(mesh.isBoundaryEdge(bdPaths[1][0].front()) && mesh.isBoundaryEdge(bdPaths[1][0].back()));
  assert(mesh.isBoundaryEdge(bdPaths[1][1].front()) && mesh.isBoundaryEdge(bdPaths[1][1].back()));*/

  assert(bdPaths[0][0].front().getClosestVertex() == bdPoint[0][0]);
  assert(zmap[bdPaths[0][0].back().getClosestVertex()] >= length);
  assert(bdPaths[0][1].front().getClosestVertex() == bdPoint[0][1]);

  assert(zmap[bdPaths[0][1].back().getClosestVertex()] >= length);

  assert(bdPaths[1][0].front().getClosestVertex() == bdPoint[1][0]);
  assert(zmap[bdPaths[1][0].back().getClosestVertex()] <= 0.);
  assert(bdPaths[1][1].front().getClosestVertex() == bdPoint[1][1]);
  assert(zmap[bdPaths[1][1].back().getClosestVertex()] <= 0.);

  bool crossing[2];
  for(unsigned char i = 0; i < 2; ++i)
    crossing[i] = bdPaths[0][i].intersects(bdPaths[1][i]);

  bool invert[2];
  // the ordering should be set using the shortest side for the middle level set (length/2)
  {
    const std::vector<PLPath> lsmiddles = zmap.computeLevelSet(length / 2, true);
    assert(lsmiddles.size() == 1);
    const PLPath & lsmiddle = lsmiddles.front();
    const double llsmiddle = lsmiddle.length();
    const double llsmiddle2 = llsmiddle / 2;
    assert(llsmiddle != 0.);
    assert(lsmiddle.isLoop());

    double bdAngles[2][2];
    for(unsigned char i = 0; i != 2; ++i)
      for(unsigned char j = 0; j != 2; ++j) {
	assert(bdPaths[i][j].length() != 0.);
	bdAngles[i][j] = lsmiddle.getIntersectionValue(bdPaths[i][j]);
	assert(bdAngles[i][j] >= 0);
	assert(bdAngles[i][j] <= llsmiddle);
      }

    // TODO: in case of a-b-a-b, we have to select the same order...
    for (unsigned char i = 0; i != 2; ++i) {
      if ((!crossing[i]) && (TwoCirclesBorderParameterization::getDirectDistance(bdAngles[0][i], bdAngles[1][i], 0., llsmiddle) > llsmiddle2)) {
	assert(TwoCirclesBorderParameterization::getDirectDistance(bdAngles[1][i], bdAngles[0][i], 0., llsmiddle) < llsmiddle2);
	unsigned char oi = (i + 1) % 2;
	if (TwoCirclesBorderParameterization::inBetween(bdAngles[0][i], bdAngles[1][i],
							0., llsmiddle, bdAngles[0][oi]) &&
	    TwoCirclesBorderParameterization::inBetween(bdAngles[0][i], bdAngles[1][i],
							0., llsmiddle, bdAngles[1][oi])) {
	  PLPath tmp = bdPaths[0][i];
	  bdPaths[0][i] = bdPaths[1][i];
	  bdPaths[1][i] = tmp;
	  double atmp = bdAngles[0][i];
	  bdAngles[0][i] = bdAngles[1][i];
	  bdAngles[1][i] = atmp;
	  invert[i] = true;
	}
	else {
	  invert[i] = false;
	}
      }
      else {
	invert[i] = false;
      }
    }
  }

#ifdef USE_OMP
  std::cout << " Number of threads: " << omp_get_max_threads()  << std::endl;
  int idcurrentii[omp_get_max_threads()];
#endif

#ifdef USE_OMP
#pragma omp parallel for
#endif
  for(int ii = 0; ii < (int)mesh.getNbPoints(); ++ii) {
    Mesh::const_point_iterator p = mesh.point_begin() + ii;
    Mesh::point_iterator pr = result.point_begin() + ii;
    idcurrentii[omp_get_thread_num()] = ii;

    if (!(*p).getIsBoundary() && (zmap.getValue((*p).getId()) != 0.) && (fabs(zmap.getValue((*p).getId()) - length) > 1e-7)) {
      if ((mesh.getNbPoints() > 2000) && ((*p).getId() % 20 == 0)) {
#ifdef USE_OMP
	if (omp_get_thread_num() == 0) {
	  std::cout << "\r Points";
	  for(unsigned int nbi = 0; nbi != omp_get_max_threads(); ++nbi)
	    std::cout << " : " << idcurrentii[nbi] << " / " << mesh.getNbPoints();
	  std::cout.flush();
	}

#else
	std::cout << "\r Points " << (*p).getId() << " / " << mesh.getNbPoints();
	std::cout.flush();
#endif
      }
      const PLPath ls = zmap.computeDirectLevelSetCC(PointOnEdge(*p));
      assert(ls.size() > 1);
      const double llength = ls.length();
      assert(llength >= 0.);
      const double zzValue = zmap[(*p).getId()] / length;
      assert(zzValue > 0.);
      assert(zzValue < 1.);

      assert((zzValue >= 0.) && (zzValue <= 1.));
      double intersection[2][2];

      // compute intersections
      for(unsigned char i = 0; i != 2; ++i)
	for(unsigned char j = 0; j != 2; ++j) {
	  intersection[i][j] = ls.getIntersectionValue(bdPaths[i][j]);
	}

      double position[2];
      for(unsigned char i= 0; i != 2; ++i) {
	if (crossing[i])
	  position[i] = TwoCirclesBorderParameterization::getValue(intersection[0][i], intersection[1][i],
								   0., llength, zzValue, false);
	else {
	  position[i] = TwoCirclesBorderParameterization::getValue(intersection[0][i], intersection[1][i],
								   0., llength, invert[i] ? 1 - zzValue : zzValue, true);

	  assert(TwoCirclesBorderParameterization::getDirectDistance(intersection[0][i], position[i], 0., llength) <=
		 TwoCirclesBorderParameterization::getDirectDistance(intersection[0][i], intersection[1][i], 0., llength));
	}
      }

      double positionPIover2 = TwoCirclesBorderParameterization::getValue(position[0], position[1],
									  0., llength, .5, true);

      assert(positionPIover2 >= 0.);
      assert(positionPIover2 <= llength);
      double angle = .5 * M_PI - 2 * M_PI * (positionPIover2 / llength);

      (*pr).setX(radius * cos(angle));
      (*pr).setY(radius * sin(angle));

    }
    else {
      assert(((*pr).getZ() == 0.) || (fabs((*pr).getZ() - length) <= 1e-7));
    }
  }
  if (mesh.getNbPoints() > 2000)
    std::cout << std::endl;

  // adjust points with length = 0. or length
  for(Mesh::point_iterator p = result.point_begin(); p != result.point_end(); ++p) {
    if (!(*p).getIsBoundary() && ((zmap.getValue((*p).getId()) == 0.) || (zmap.getValue((*p).getId()) == length))) {
      double x = 0.;
      double y = 0.;
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb) {
	x += result.point(*nb).getX();
	y += result.point(*nb).getY();
      }
      if ((*p).getNbNeighbours() != 0) {
	x /= (*p).getNbNeighbours();
	y /= (*p).getNbNeighbours();
      }
      (*p).setX(x);
      (*p).setX(y);
    }
  }

  return result;
}


double CylinderParameterization::getAngle(const Mesh & mesh, const PointOnEdge & point) const {
  const double angle1 = getAngle(mesh, point.getFirstVertex());
  const double angle2 = getAngle(mesh, point.getSecondVertex());
  return TwoCirclesBorderParameterization::getAngle(angle1, angle2, point.getLocation(), - M_PI);
}

double CylinderParameterization::getAngle(const Mesh & mesh, VertexID point) const {
  assert(mesh.point(point).getIsBoundary());
  assert(fabs(sqrt(mesh.point(point).getX() * mesh.point(point).getX() +
		   mesh.point(point).getY() * mesh.point(point).getY()) - radius) < 1e-4);

  if (mesh.point(point).getY() == 0.)
    if (mesh.point(point).getX() > 0.)
      return 0.;
    else
      return M_PI;
  else
    return (mesh.point(point).getY() > 0 ? 1. : -1.) * acos(mesh.point(point).getX() / radius);
}

double TwoCirclesBorderParameterization::getValue(double v1, double v2, double start, double end, double ratio, bool direct) {
#ifndef NDEBUG
  const double ov1 = v1;
  const double ov2 = v2;
#endif

  assert((start <= v1) && (v1 <= end));
  assert((start <= v2) && (v2 <= end));
  assert((ratio >= 0.) && (ratio <= 1.));

  double l1 = fabs(v1 - v2);
  const double full = end - start;
  assert(l1 <= full);

  if (direct) {
    if (v1 > v2) {
      v2 += full;
      l1 = full - l1;
    }
  }
  else {
    if (l1 > full / 2) {
      if (v1 < v2)
	v1 += full;
      else
	v2 += full;
      l1 = full - l1;
    }
  }

  assert(l1 >= 0.);

  double result = v1 * (1 - ratio) + v2 * ratio;
  assert(fabs(l1 - fabs(v1 - result) - fabs(v2 - result)) < 1e-4);


  while (result > end)
    result -= full;

  assert((!direct) || (((ov1 <= ov2) && ((result >= ov1) && (result <= ov2))) ||
		       ((ov1 >= ov2) && ((result >= ov1) || (result <= ov2)))));

  return result;
}


bool TwoCirclesBorderParameterization::inBetween(double v1, double v2, double start, double end, double ov) {
  assert((start <= v1) && (v1 <= end));
  assert((start <= v2) && (v2 <= end));

  if (v1 <= v2)
    return ((v1 <= ov) && (ov <= v2));
  else {
    assert(fabs(v2 - v1) <= (end - start));
    return (ov >= v1) || (ov <= v2);
  }
}


double TwoCirclesBorderParameterization::getDirectDistance(double v1, double v2, double start, double end) {
  assert((start <= v1) && (v1 <= end));
  assert((start <= v2) && (v2 <= end));

  if (v1 < v2)
    return fabs(v2 - v1);
  else {
    assert(fabs(v2 - v1) <= (end - start));
    return (end - start) - fabs(v2 - v1);
  }
}

void CylinderParameterization::addEpsilonBdPoints(Mesh & mesh, VertexID bdPoints1[2], VertexID bdPoints2[2]) {
  double epsilon = 1e-1 * radius;
  assert(mesh.point(bdPoints1[0]).getZ() == 0.);
  assert(mesh.point(bdPoints1[1]).getZ() == 0.);
  mesh.point(bdPoints1[0]).setZ(-epsilon);
  mesh.point(bdPoints1[1]).setZ(-epsilon);

  assert(mesh.point(bdPoints2[0]).getZ() == length);
  assert(mesh.point(bdPoints2[1]).getZ() == length);
  mesh.point(bdPoints2[0]).setZ(length + epsilon);
  mesh.point(bdPoints2[1]).setZ(length + epsilon);
}

double CylinderParameterization::computeNormalsMSE(const Mesh & mesh) {
  double result = 0.;
  Line3D axis(Coord3D(0., 0., 0.), DBottom);

  for(Mesh::const_triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t) {
    const Coord3D n = mesh.computeTriangleNormal((*t).getId());
    const Coord3D middle = (mesh.point((*t).getP1()) +
			    mesh.point((*t).getP2()) +
			    mesh.point((*t).getP3())) / 3;
    const Coord3D proj = middle.projectionOnAxis(axis);
    const Coord3D vect = (middle - proj).normalize();
    double angle = Coord3D::angleVector(n, vect);
    if (angle > M_PI / 2)
      angle = M_PI - angle;
    const double v = angle * mesh.getArea((*t).getId());
    result += v * v;
  }

  return sqrt(result);
}

double CylinderParameterization::computeWorstDistance(const Mesh & mesh, double epsilon) {
  Coord3D tmp(mesh.point(0));
  tmp.setZ(0.);
  const double radius = tmp.norm();
  double result = 0.;

  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin();
	nb != (*p).getNeighbours().end(); ++nb)
      if (*nb > (*p).getId()) {
	Coord3D tmp(((*p) + mesh.point(*nb)) / 2);
	tmp.setZ(0.);
	const double d = radius - tmp.norm();
	assert(d >= 0.);
	if (d > result)
	  result = d;
      }

  if (result < epsilon * radius)
    return -1.;
  else
    return result;
}
