/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2013 Jean-Marie Favreau <J-Marie.Favreau@udamail.fr>
 *                    ISIT, UMR 6284 UdA - CNRS
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef CYLINDERPROJECTION_H
#define CYLINDERPROJECTION_H

#include "IDTypes.h"
#include "Mesh.h"
#include "Coord2D.h"
#include "UnfoldingMethod.h"

namespace Taglut {

/**
   @class ABFMethod
   @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
   @brief Unfolding a surface by projecting it on a cylinder oriented along the PCA axis (or the given one), assuming that the surface is homeomorphic to a disc
*/
class CylinderProjection : public UnfoldingMethod {
private:
    Coord3D center;
    Coord3D axis;
    Line3D line;
    Plane3D plane;
    double radius;


    std::vector<Coord3D> cylinderProjection;

  /**
     Given a triangle and an edge of points on this triangle allready computed,
     compute the other point location (if needed) and update flags
     Return false if point has been computed with a bad location
  */
  bool compute2DLocation(Triangle & t, const std::pair<VertexID, VertexID> & edge);

  bool compute2DLocation(VertexID v1, VertexID v2);
  void initAxis();
  void estimateRadius();
public:
    /**
       Constructor
    */
  CylinderProjection(Mapping2D3D & mapping);

   /**
      Constructor
   */
   CylinderProjection(Mapping2D3D & mapping, const Coord3D & ax, const Coord3D & ce);

   /**
       Copy constructor
    */
    CylinderProjection(const CylinderProjection & cProjection);
    /**
       Destructor
    */
    virtual ~CylinderProjection() {}

    /**
      Set axis of the cylinder
    */
    void setAxis(const Coord3D & ax, const Coord3D & ce) {
        center = ce;
        axis = ax;
    }

    /**
       Compute 2D location
    */
    void compute2DLocation(const Taglut::IndentManager & indentManager, bool adjustMeanDistance = false);


    /**
       Clone method
    */
    virtual UnfoldingMethod * clone(Mapping2D3D & mapping);

    /**
       Process unfolding using ABF algorithm
       Return number of iteration or -1 if fails
    */
    int processUnfolding(float limit, unsigned int maxIter, bool initData = true, const std::string & logFile = "", const Taglut::IndentManager & indentManager = Taglut::IndentManager());


    /**
       Init unfolding data using 2d location of points
    */
    virtual void initDataUsing2D() {
        initAxis();
    }
};

}

#endif // CYLINDERPROJECTION_H
