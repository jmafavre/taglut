/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MULTISCALE_UNFOLDING
#define MULTISCALE_UNFOLDING


#include "Mapping2D3D.h"
#include "UnfoldingMethod.h"
#include "IndentManager.h"

namespace Taglut {
  class MultiscaleUnfolding {
  private:
    /** maximum depth during multiscale unfolding process */
    unsigned int depthMax;

    /** readjust mapping using an unfolding process */
    bool readjust;

    /** epsilon value for unfolding tool */
    double epsilon;

    /** maximum iteration for unfolding tool */
    unsigned int maxIter;

    bool initFirst;

    /** log file or "" if none */
    std::string logFile;

    /** indent manager */
    Taglut::IndentManager indentManager;

    /** internal method */
    int processUnfoldingInternal(UnfoldingMethod & curMapping, unsigned int currDepth);

  public:
    /**
       Default constructor
       @param readjust Readjust points' location after each multiscale step
       @param epsilon Error on unfolding process
       @param maxIter Maximum iteration
       @param logFile Name of the file where be log the infolding process
       @param indentManager Indent manager
    */
    MultiscaleUnfolding(bool readjust, float epsilon, unsigned int maxIter, const std::string & logFile = "", const Taglut::IndentManager & indentManager = Taglut::IndentManager());

    /**
       Default constructor
       @param readjust Readjust points' location after each multiscale step
       @param epsilon Error on unfolding process
       @param maxIter Maximum iteration
       @param initFirst Initialize temporary data at begin of process
       @param logFile Name of the file where be log the infolding process
       @param indentManager Indent manager
    */
    MultiscaleUnfolding(bool readjust, float epsilon, unsigned int maxIter, bool initFirst, const std::string & logFile = "", const Taglut::IndentManager & indentManager = Taglut::IndentManager());

    /**
       Unfolding method using method defined by mapping
       @param mapping method and data
       @param depth Depth of the multiscale process
    */
    int processUnfolding(UnfoldingMethod & mapping, unsigned int depth);

  };
}

#endif
