/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */



#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "Mapping2D3D.h"
#include "Display3D.h"

#include "Messages.h"

using namespace Taglut;

static char*  filename  = NULL;
static int    sizex = 300;
static int    sizey = 300;
static int    threeD = 0;
static int    color = 0;
static char*  imagefilename = NULL;
static int    help = 0;
static int    opengl = 0;
static int    border = 0;
static int    overlaps = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input mapping file", NULL},
  { "x", 'x', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizex, 0, "window size (x)", NULL},
  { "y", 'y', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizey, 0, "window size (y)", NULL},
  { "3d", '3', POPT_ARG_NONE | POPT_ARGFLAG_SHOW_DEFAULT, &threeD, 0, "3d mesh", NULL},
  { "overlaps", 'v', POPT_ARG_NONE, &overlaps, 0, "display overlaps", NULL},
  { "opengl", 'g', POPT_ARG_NONE, &opengl, 0, "display using opengl", NULL},
  { "border", 'b', POPT_ARG_NONE, &border, 0, "display borders", NULL},
  { "color", 'c', POPT_ARG_NONE | POPT_ARGFLAG_SHOW_DEFAULT, &color, 0, "Use colored primitives", NULL},
  { "texture", 't', POPT_ARG_STRING | POPT_ARGFLAG_SHOW_DEFAULT, &imagefilename, 0, "Image texture", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("mappingViewer", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mappign viewer", "Display a 2d/3d mapping.");
    hMsg << "Input: mapping to display (available format: map). Optional: image texture.";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((color != 0) && (imagefilename != NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: incompatible options (color, image)." << endl;
    return 1;
  }


  if ((sizex <= 0) || (sizey <= 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: 3d window size must be positive." << endl;
    return 1;
  }

  /* load mapping */
  cout << "Loading mapping..." << endl;
  Mesh mesh;
  Mapping2D3D mapping(mesh);
  try {
    mapping.load(filename);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }


  if (!mapping.isFlatten()) {
    std::cout << "Error: this file does not contains an available flatten data. Abording." << std::endl;
    return 1;
  }

  try {
    if (overlaps != 0) {
	CImg<unsigned char> result(sizex, sizey);
	result.fill(cimg::type<unsigned char>::min());

	std::cout << "Computing the overlaps" << std::endl;
	mapping.toImageOverlap(result);

	CImgDisplay disp(result);
	while (!disp._is_closed) {
	  disp.wait();
	  if (disp.key() == cimg::keyESC) {
	    break;
	  }
	}

    }
    else if (imagefilename == NULL)
      Display3D::displayMapping((unsigned int)sizex, (unsigned int)sizey, std::string("3D Visualisation"), mapping, threeD != 0, color == 0, border != 0, false, opengl != 0);
    else {
      cout << "Loading texture image..." << endl;
      CImg<unsigned char> imgTexture(imagefilename);

      Display3D::displayMapping((unsigned int)sizex, (unsigned int)sizey, std::string("3D Visualisation"), mapping, imgTexture, threeD != 0, border != 0);
    }
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }





  return 0;

}
