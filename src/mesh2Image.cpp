/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal, IMATI-CNR, CSIRO
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "Mesh.h"
#include "MeshVoxelizer.h"

#include "Messages.h"
using namespace Taglut;

static char* filename  = NULL;
static char* objectName = NULL;
static char* filenameOutput = NULL;
static int   help = 0;
static int   maxSize = 64;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Mesh file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filenameOutput, 0, "Image file", NULL},
  { "max-size", 'm', POPT_ARG_INT, &maxSize, 0, "Maximum number of voxels in every direction (x, y or z). Default: 64.", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("mesh2Image", argc, argv, options, 0);


  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh to image", "A tool to convert a mesh to a 3D image.");
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (maxSize <= 0) {
    cerr << "Error: maximum size has a negative value. Abort." << endl;
    return -1;
  }

  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if (filenameOutput == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }



  /* load mesh */
  try {
    std::cout << "Loading mesh (" << filename << ")..." << std::endl;
    Mesh mesh(filename, objectName);

    std::cout << "Voxelization" << std::endl;
    MeshVoxelizer mVox(mesh, maxSize);
    CImg<unsigned char> img = mVox.getVoxelization();

    // convert the values.
    // input:
    //  inside = 1;
    //  outside = 3;
    //  surface = 2;
    //  unknown = 0;
    // output:
    //  inside = 1;
    //  outside = 0;
    //  surface = 1;
    //  unknown = 0;
    cimg_forXYZ(img, x, y, z) {
      unsigned char & cur = img(x, y, z);
      if (cur == 3) cur = 0;
      if (cur == 2) cur = 1;
    }


    std::cout << "Saving file (" << filenameOutput << ")..." << std::endl;
    img.save(filenameOutput);

  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }



  return 0;
}
