/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CSIRO
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "Mesh.h"
#include "FileTools.h"
#include "MeshMap.h"
#include "Exception.h"

#include "Messages.h"
using namespace Taglut;

static char* filename   = NULL;
static char* objectName = NULL;
static char* output     = NULL;
static int   help       = 0;
static int   stats      = 0;
static int   edges      = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input file (mesh)", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &output, 0, "Output file (vtk, txt, usr).", NULL},
  { "stats", 's', POPT_ARG_NONE, &stats, 0, "Generate stats rather than scalar values (output file: txt).", NULL},
  { "edges", 'e', POPT_ARG_NONE, &edges, 0, "Generate values on the edges (output file: txt).", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("computeLocalEdgeSize", argc, argv, options, 0);


  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Compute local edge size", "Compute a scalar value for each vertex associated the average from the length of the adjacent edges (variations are possible, as statistics or values on edges).");
    hMsg << "Input: mesh";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  std::string fileOutput;
  if (output == NULL) {
    if (stats == 1)
      fileOutput = FileTools::skipExtension(std::string(filename)) + "-stats.txt";
    else if (edges == 1)
      fileOutput = FileTools::skipExtension(std::string(filename)) + "-edges.txt";
    else
      fileOutput = FileTools::skipExtension(std::string(filename)) + "-esize.vtk";
  }
  else
    fileOutput = std::string(output);

  try {
    std::cout << "Loading file (" << filename << ")" << std::endl;
    Mesh mesh(filename);
    MeshMap mMap(mesh);

    if (stats) {
      double min = std::numeric_limits<double>::max();
      double max = 0.;
      double mean = 0.;
      double standardDeviation = 0.;
      VertexID nbEdges = 0;
      for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	  if (*nb > (*p).getId()) {
	    ++nbEdges;
	    const double d = (*p).distance(mesh.point(*nb));
	    if (d < min) min = d;
	    if (d > max) max = d;
	    mean += d;
	    standardDeviation += d * d;
	  }
      if (nbEdges != 0) {
	mean /= nbEdges;
	standardDeviation /= nbEdges;
      }
      standardDeviation -= mean * mean;

      std::cout << "Saving file (" << fileOutput << ")" << std::endl;
      std::ofstream outfile(fileOutput.c_str(), std::ios::out);

      if (!outfile.is_open())
	throw ExceptionCannotOpenOutputFile();

      outfile << "min: " << min << std::endl;
      outfile << "max: " << max << std::endl;
      outfile << "mean: " << mean << std::endl;
      outfile << "standard deviation: " << standardDeviation << std::endl;

      outfile.close();
    }
    else if (edges) {
      std::cout << "Saving file (" << fileOutput << ")" << std::endl;
      std::ofstream outfile(fileOutput.c_str(), std::ios::out);

      if (!outfile.is_open())
	throw ExceptionCannotOpenOutputFile();


      for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	  if (*nb > (*p).getId()) {
	    outfile << (*p).distance(mesh.point(*nb)) << std::endl;
	  }
      outfile.close();
    }
    else {
      std::cout << "Scalar computation" << std::endl;
      for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p) {
	double v = 0.;
	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	  v += (*p).distance(mesh.point(*nb));
	if ((*p).getNeighbours().size() != 0.)
	  v /= (*p).getNeighbours().size();
	mMap[(*p).getId()] = v;
      }

      std::cout << "Saving file (" << fileOutput << ")" << std::endl;
      mMap.save(fileOutput);
    }

  }
  catch (Exception e) {
    std::cerr << e << " Abort." << std::endl;
    return 1;
  }

  return 0;
}
