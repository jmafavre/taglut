/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DISPLAYGL
#define DISPLAYGL
#include <string>
#include <deque>

#include "IDTypes.h"
#include "Coord3D.h"
#include "CImgUse.h"
#include "Mapping2D3DList.h"

namespace Taglut {

  class PointCloud;
  class PointCloudShapeEstimator;
  /**
     @class DisplayGL
     Class used to display a mesh or a circle set using OpenGL
  */
  class DisplayGL {
  protected:
    /** window size */
    unsigned int sizex;
    /** window size */
    unsigned int sizey;

    /** window title */
    const std::string title;

    /** display edges */
    bool meshes;

    /** smooth display */
    bool smooth;

    /** auto light */
    bool autolight;

    /** negative colors */
    bool negative;

    /** display edges and normals */
    bool drawedgesandnormals;

    /** max location */
    double maxX;
    /** max location */
    double minX;
    /** max location */
    double maxY;
    /** max location */
    double minY;
    /** max location */
    double maxZ;
    /** max location */
    double minZ;

    /** display initialization */
    void initDisplay();

    /** compute largest width (according x, y and z axes) */
    virtual double computeLargestWidth();

    /** edges and normals that will be displayed */
    std::vector<std::pair<Coord3D, Coord3D > > edgesAndNormals;

  public:

    /** default constructor */
    DisplayGL(unsigned int sizex, unsigned int sizey,
	      const std::string & title, bool meshes, bool smooth = false, bool autolight = false, bool negative = false);

    /** destructor */
    virtual ~DisplayGL();

    /** Return true if opengl display is available */
    static bool isAvailable();

    /** get points */
    virtual const std::deque<Coord3D> & getPoints() const = 0;
    /** get triangles */
    virtual const CImgList<VertexID> & getTriangles() const = 0;

    /** get colors */
    virtual const CImgList<unsigned char> & getColors() const = 0;

    /** get normals */
    inline const std::vector<std::pair<Coord3D, Coord3D> > & getEdgesAndNormals() const {
      return edgesAndNormals;
    }


    /** dispay mesh using opengl */
    virtual void disp() = 0;
  };




  /**
     @class DisplaySimpleGL
     Class used to display a mesh or a circle set using OpenGL. Static version
  */
  class DisplaySimpleGL : public DisplayGL {
  protected:
    /** mesh points */
    const std::deque<Coord3D> & points;
    /** mesh triangles */
    const CImgList<VertexID> & triangles;

    /** mesh colors */
    const CImgList<unsigned char> & colors;


  public:

    /** default constructor */
    DisplaySimpleGL(unsigned int sizex, unsigned int sizey,
		    const std::string & title, const std::deque<Coord3D> & points,
		    const CImgList<VertexID> & triangles,
		    const CImgList<unsigned char> & colors, bool meshes, bool smooth = false, bool autolight = false, bool negative = false);

    /** get points */
    const std::deque<Coord3D> & getPoints() const;
    /** get triangles */
    const CImgList<VertexID> & getTriangles() const;


    /** get colors */
    const CImgList<unsigned char> & getColors() const;

    /** dispay mesh using opengl */
    void disp();
  };




  /**
     @class DisplayDynamicGL
     Class used to display a mesh or a circle set using OpenGL. Dynamic version
     with associated map.
  */
  class DisplayDynamicGL : public DisplayGL {
  protected:
    /** mesh points */
    std::deque<Coord3D> points;
    /** mesh triangles */
    CImgList<VertexID> triangles;

    /** mesh colors */
    CImgList<unsigned char> colors;

    /** circle set to be displayed */
    Mapping2D3DList & mappings;


  public:

    /** default constructor */
    DisplayDynamicGL(unsigned int sizex, unsigned int sizey,
		     const std::string & title, Mapping2D3DList & mappings,
		     const CImgList<unsigned char> & colors, bool meshes = true, bool smooth = false, bool autolight = false, bool negative = false);

    /** get points */
    const std::deque<Coord3D> & getPoints() const;
    /** get triangles */
    const CImgList<VertexID> & getTriangles() const;

    /** get colors */
    const CImgList<unsigned char> & getColors() const;

    /** dispay mesh using opengl */
    void disp();
  };



  /**
     @class DisplayPointCLoudGL
     Class used to display a point cloud using OpenGL.
  */
  class DisplayPointCLoudGL : public DisplayGL {

  public:
    /**
       available methods for sphere coloring
    */
    enum SphereColor { SCNone, SCNormal, SCDistortion, SCBoundary, SCBoundaryDistortion };

  protected:
    /** mesh points */
    std::deque<Coord3D> points;
    /** mesh triangles */
    CImgList<VertexID> triangles;

    /** mesh colors */
    CImgList<unsigned char> colors;

    /** coloring method */
    enum SphereColor cMethod;

    /** a sphere that will be drawn at every point of the cloud */
    const Mesh sphere;

    /** splat balls */
    bool splat_balls;

    /** add a generic ball with the given properties */
    void addBall(const Coord3D & center, double radius, const Coord3D & normal, const CImg<unsigned char> & color);

    /** add a ball with the normal properties */
    void addBall(const Coord3D & center, double radius, const Coord3D & normal = Coord3D());


    /** add a ball with the normal and boundary properties */
    void addBall(const Coord3D & center, double radius, const Coord3D & normal, bool boundary);

    /** add a ball with the normal and distortion properties */
    void addBall(const Coord3D & center, double radius, const Coord3D & normal, double localDistortion, double minDist, double maxDist);

    /** add a splat ball using the normal */
    void addSplatBall(const Coord3D & center, double radius, const Coord3D & normal, const CImg<unsigned char> & color);

  public:
    /**
       default constructor
    */
    DisplayPointCLoudGL(unsigned int sizex, unsigned int sizey,
			const std::string & title, const PointCloud & pCloud, double radius = -1, bool negative = false);

    /**
       default constructor
    */
    DisplayPointCLoudGL(unsigned int sizex, unsigned int sizey,
			const std::string & title, const PointCloudShapeEstimator & pCloudNEstimator, bool negative = false,
			bool splat_balls = false,
			const enum SphereColor & cMethod = DisplayPointCLoudGL::SCNone, bool useRealSize = false, bool drawEdges = false,
			bool drawOnlyBdEdges = false, bool drawNormals = true, double normalRatio = 5., bool use_flags = false,
			bool use_threshold = false, double threshold = 5.0, double maxSize = std::numeric_limits<double>::max());

    /** get points */
    const std::deque<Coord3D> & getPoints() const;
    /** get triangles */
    const CImgList<VertexID> & getTriangles() const;

    /** get colors */
    const CImgList<unsigned char> & getColors() const;


    /** dispay mesh using opengl */
    void disp();
  };


}

#endif
