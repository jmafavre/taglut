/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DDSCROLLBAR
#define DDSCROLLBAR

#include <assert.h>
#include "CImgUse.h"
#include "DDBox.h"

namespace Taglut {
  class DDisplay;

  /**
     @class DDScrollBar
     Scroll bar for DDisplay
  */
  class DDScrollBar : public DDBox {
  private:

    /** Internal status (left button, scrollbar or right button activated)  */
    unsigned int status;
    /** Number of steps of the scrollbar */
    unsigned int nbSteps;
    /** Location of the scrollbar */
    unsigned int location;
    /** real location of the scrollbar, using cursor size */
    unsigned int realLocation;
    /** first location of scrollbar (used when using mouse to move scrollbar) */
    int firstLocation;
    /** first location of the mouse (used when using mouse to move scrollbar) */
    int firstLocationMouse;
    /** true if subset has to be drawn */
    bool dSubset;

    /** Associated display */
    DDisplay & ddisplay;

    /** First button arrow */
    DDBox firstArrow;
    /** Second button arrow */
    DDBox secondArrow;
    /** Scrollbar box */
    DDBox scrollBox;
    /** real size */
    double size;
    /** cursor box */
    DDBox cursorBox;
    /** subset box */
    DDBox subset;


  public:

    /**
       Default constructor
    */
    DDScrollBar(DDisplay & ddisplay, int x1, int y1,
		int sizex, int sizey,
		unsigned int nbSteps);

    /**
       Set location of current scrollbar
    */
    void setLocation(unsigned int l);

    /**
       Redraw scrollbar
       @param activeBox current active box
    */
    void redraw(unsigned int activeBox);

    /**
       Return id of the box were (x, y) pointer is located
    */
    unsigned int getIdInside(int x, int y) const;

    /**
       Return true if scrollbar is active
    */
    bool isActive() const;

    /**
       Update slice using mouse location
    */
    unsigned int updateSlice(int x, int y);

    /**
       Set inactive status for scrollbar
    */
    void setInactive();

    /**
       Define subset start and end
    */
    void setSubset(int subsetX1, int subsetX2);

    /**
       Enable or disable subset display
    */
    void displaySubset(bool value);

  };
}

#endif
