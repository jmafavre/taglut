/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "DDisplay.h"
#include <sstream>

using namespace Taglut;

const unsigned int DDisplay::buttonHeight = 10;

DDisplay::DDisplay(std::string title_l,
		   Mapping2D3D & mapping_l,
		   const CImg<unsigned char> & imgTexture_l, const CImg<unsigned char> & img_l,
		   double zratio_l) : title(title_l), mapping(mapping_l), img(img_l), uMap(img_l._height, img_l._height), zratio(zratio_l),
				      resultImage(img_l._width + img_l._height + 40, img_l._height + 30), uMapBox(5, 5, uMap._width, uMap._height),
				      sliceBox(img_l._width + 35, 5, img_l._width, img_l._height),
				      sliceSB(*this, img_l._width + 47, img_l._height + 7, img_l._width - (buttonHeight + 2), buttonHeight, img_l._depth),
				      toogleBBox(img_l._width + 35, img_l._height + 7, buttonHeight, buttonHeight) {

  // build map
  uMap.fill((unsigned char) 0);
  mapping.toImageDrawTexture(uMap, imgTexture_l, zratio);

  numElectrode = 0;

  // init display
  initDisplay();
}

DDisplay::DDisplay(std::string title_l,
		   Mapping2D3D & mapping_l,
		   const ElectrodeList & eList_l, const CImg<unsigned char> & img_l,
		   double zratio_l) : title(title_l), mapping(mapping_l), img(img_l), uMap(img_l._height, img_l._height), zratio(zratio_l),
				      resultImage(img_l._width + img_l._height + 40, img_l._height + 30), uMapBox(5, 5, uMap._width, uMap._height),
				      sliceBox(img_l._width + 35, 5, img_l._width, img_l._height),
				      sliceSB(*this, img_l._width + 47, img_l._height + 7, img_l._width - (buttonHeight + 2), buttonHeight, img_l._depth),
				      toogleBBox(img_l._width + 35, img_l._height + 7, buttonHeight, buttonHeight), eList(eList_l) {
  assert(eList.size() != 0);

  // build map
  uMap.fill((unsigned char) 255);
  mapping.toImageDrawDistanceMap(uMap, eList.front());
  numElectrode = 0;

  // init display
  initDisplay();
}


void DDisplay::initDisplay() {
  dCross = false;
  displayBBox = true;


  // init result image
  resultImage.fill((unsigned char) 0);

  activeBox = oldActiveBox = 0;

  // init colors
  activeColor1 = 180;
  activeColor2 = 220;
  inactiveColor1 = 100;
  inactiveColor2 = 150;

  // compute min and max z values for maps and set subset display in scrollbar
  double dminz = img._depth * zratio;
  double dmaxz = 0;
  double dminx = img._width;
  double dmaxx = 0;
  double dminy = img._height;
  double dmaxy = 0;
  for(Mesh::const_point_iterator c = mapping.point3D_begin(); c != mapping.point3D_end(); ++c) {
    if ((*c).getX() < dminx) dminx = (*c).getX();
    if ((*c).getX() > dmaxx) dmaxx = (*c).getX();
    if ((*c).getY() < dminy) dminy = (*c).getY();
    if ((*c).getY() > dmaxy) dmaxy = (*c).getY();
    if ((*c).getZ() < dminz) dminz = (*c).getZ();
    if ((*c).getZ() > dmaxz) dmaxz = (*c).getZ();
  }
  sliceSB.setSubset((unsigned int)(dminz / zratio), (unsigned int)(dmaxz / zratio));
  maxx = (int) dmaxx;
  minx = (int) dminx;
  maxy = (int) dmaxy;
  miny = (int) dminy;
  maxz = (int) dmaxz;
  minz = (int) dminz;

}

void DDisplay::drawCross(unsigned int x, unsigned int y, const unsigned char color, unsigned int size) {
  resultImage.draw_line(x - size, y, x + size, y, &color);
  resultImage.draw_line(x, y - size, x, y + size, &color);
}

void DDisplay::drawCross(unsigned int x, unsigned int y, unsigned int size) {
  for(int i = -size; i <= (int) size; ++i) {
    if (resultImage(x + i, y) > 127)
      resultImage(x + i, y) = 0;
    else
      resultImage(x + i, y) = 255;
    if (resultImage(x, y + i) > 127)
      resultImage(x, y + i) = 0;
    else
      resultImage(x, y + i) = 255;
  }
}


bool DDisplay::display() {
  if (mapping.size() == 0) {
    std::cerr << "Display error: the mapping is empty" << std::endl;
    throw Exception();
  }
  std::cout << "(Display)" << std::endl;


  // init cross location and redraw image
  crossLocation = mapping.getMesh().computePseudoCenter();
  dCross = true;
  redraw();


  // then display image
  CImgDisplay disp(resultImage, title.c_str());

  while (!disp._is_closed) {
    disp.wait();

    int x = disp._mouse_x * resultImage._width / disp._window_width;
    int y = disp._mouse_y * resultImage._height / disp._window_height;

    oldActiveBox = activeBox;

    if (!(sliceSB.isActive() && disp._button)) {
      if (uMapBox.isInside(x, y))
	activeBox = uMapBox.getId();
      else if (sliceBox.isInside(x, y))
	activeBox = sliceBox.getId();
      else if (toogleBBox.isInside(x, y))
	activeBox = toogleBBox.getId();
      else if (sliceSB.isInside(x, y))
	activeBox = sliceSB.getIdInside(x, y);
      else
	activeBox = 0;
    }

    if (disp._button) {
      if (sliceSB.isActive() || (sliceSB.isInside(x, y))) {
	crossLocation.setZ(sliceSB.updateSlice(x, y) * zratio);
	dCross = false;

	redraw().display(disp);
      }
      else if (activeBox == uMapBox.getId()) {
	std::pair<int, int> mouse = uMapBox.absolute2local(x, y);
	crossLocation = mapping.getNearestPoint(mouse.first, mouse.second);
	dCross = true;

	redraw().display(disp);
      }
      else if (activeBox == sliceBox.getId()) {
	std::pair<int, int> mouse = sliceBox.absolute2local(x, y);
	crossLocation = mapping.getNearestPoint(mouse.first, mouse.second, crossLocation.getZ());
	dCross = true;

	redraw().display(disp);
      }
      else if (toogleBBox.isInside(x, y)) {
	displayBBox = !displayBBox;
	sliceSB.displaySubset(displayBBox);

	redraw().display(disp);
      }
    }
    else {
      sliceSB.setInactive();
      if (disp._wheel) {
	if (sliceBox.isInside(x, y)) {
	  dCross = false;
	  crossLocation.setZ(crossLocation.getZ() + disp._wheel * zratio);
	  if (crossLocation.getZ() < 0) crossLocation.setZ(0);
	  if (crossLocation.getZ() > (img._depth - 1) * zratio) crossLocation.setZ((img._depth - 1) * zratio);

	  redraw().display(disp);
	}
	if (uMapBox.isInside(x, y) && (eList.size() != 0)) {
	  numElectrode += disp._wheel;
	  if (numElectrode < 0)
	    numElectrode = 0;
	  if ((unsigned int)numElectrode >= eList.size())
	    numElectrode = eList.size() - 1;

	  mapping.toImageDrawDistanceMap(uMap, eList[numElectrode]);

	  redraw().display(disp);
	}
      }
      else if (activeBox != oldActiveBox)
	redrawBorders().display(disp);
    }
    disp._wheel = 0;
  }

  return true;
}

CImg<unsigned char> & DDisplay::redraw() {
  // redraw map
  redrawMap();

  // redraw 3d data
  redrawSlice();

  // redraw slice scroll bar
  sliceSB.setLocation((unsigned int)(crossLocation.getZ() / zratio));
  sliceSB.redraw(activeBox);

  // redraw toogle button for bounding box
  redrawToogleBBox();

  return resultImage;
}

CImg<unsigned char> & DDisplay::redrawBorders() {
  // redraw map
  redrawMapBorder();

  // redraw 3d data
  redrawSliceBorder();

  // redraw slice scroll bar
  sliceSB.redraw(activeBox);

  // redraw toogle button for bounding box
  redrawToogleBBox();

  return resultImage;
}

CImg<unsigned char> &  DDisplay::redrawToogleBBox() {
  const unsigned char colorBlack = 0;

  if (activeBox == toogleBBox.getId()) {
    drawBox(toogleBBox, true, true);
    if (displayBBox)
      drawBox(toogleBBox.getX1() + 2, toogleBBox.getY1() + 2, toogleBBox.getX2() - 2, toogleBBox.getY2() - 2, colorBlack);
    else
      drawBox(toogleBBox.getX1() + 2, toogleBBox.getY1() + 2, toogleBBox.getX2() - 2, toogleBBox.getY2() - 2, activeColor1);
  }
  else {
    drawBox(toogleBBox, true, false);
    if (displayBBox)
      drawBox(toogleBBox.getX1() + 2, toogleBBox.getY1() + 2, toogleBBox.getX2() - 2, toogleBBox.getY2() - 2, inactiveColor1);
    else
      drawBox(toogleBBox.getX1() + 2, toogleBBox.getY1() + 2, toogleBBox.getX2() - 2, toogleBBox.getY2() - 2, colorBlack);
  }

  return resultImage;
}

CImg<unsigned char> &  DDisplay::redrawMap() {
  const unsigned char color = std::numeric_limits<unsigned char>::max();
  const unsigned char colorBlack = 0;

  resultImage.draw_image(uMapBox.getX1(), uMapBox.getY1(), uMap);

  resultImage.draw_rectangle(uMapBox.getX1() + 3, uMapBox.getY1() + uMap._height + 2, uMapBox.getX1() + uMap._width, uMapBox.getY1() + uMap._height + 13, &colorBlack);
  if (dCross) {
    drawCross(uMapBox.getX1() + (unsigned int)crossLocation.get2DX(), uMapBox.getY1() + (unsigned int)crossLocation.get2DY());
    // write coordinates
    std::ostringstream str;
    str << "x: " << crossLocation.getX() << ", y: " << crossLocation.getY() << ", z: " << crossLocation.getZ() << " (ratio: " << zratio << ")";
    resultImage.draw_text(uMapBox.getX1() + 3, uMapBox.getY1() + uMap._height + 2, str.str().c_str(), &color, &colorBlack);
  }

  redrawMapBorder();

  return resultImage;
}

CImg<unsigned char> &  DDisplay::redrawMapBorder() {
  if (activeBox == uMapBox.getId())
    drawBox(uMapBox, true, true);
  else
    drawBox(uMapBox, true, false);

  return resultImage;
}

CImg<unsigned char> & DDisplay::redrawSlice() {
  const unsigned char color = std::numeric_limits<unsigned char>::max();
  const unsigned char colorBlack = 0;

  CImg<unsigned char> slice = img.get_slice((unsigned int)((double)crossLocation.getZ() / zratio));
  resultImage.draw_image(sliceBox.getX1(), sliceBox.getY1(), slice);
  int x3d = sliceBox.getX1() + (unsigned int) crossLocation.getX();
  int y3d = sliceBox.getY1() + (unsigned int) crossLocation.getY();
  if (dCross)
    drawCross(x3d, y3d);

  // write slice
  std::ostringstream str;
  str << "slice: " << ((unsigned int)(crossLocation.getZ() / zratio)) << " (" << img._depth << ")";
  resultImage.draw_text(sliceBox.getX1() + 3, sliceBox.getY1() + uMap._height - 14, str.str().c_str(), &color, &colorBlack);

  // draw bounding box
  if (displayBBox) {
    if ((crossLocation.getZ() >= minz) && (crossLocation.getZ() <= maxz)) {
      std::pair<int, int> coord1 = sliceBox.local2Absolute(minx - 1, miny - 1);
      std::pair<int, int> coord2 = sliceBox.local2Absolute(maxx + 1, maxy + 1);
      drawBox(coord1.first, coord1.second, coord2.first, coord2.second, color, 0.2);
      resultImage.draw_rectangle(sliceBox.getX1() + 1, sliceBox.getY1() + 1, coord1.first - 1, sliceBox.getY2() - 1, &colorBlack, 0.5);
      resultImage.draw_rectangle(coord1.first, sliceBox.getY1() + 1, sliceBox.getX2() - 1, coord1.second - 1, &colorBlack, 0.5);
      resultImage.draw_rectangle(coord1.first, coord2.second + 1, sliceBox.getX2() - 1, sliceBox.getY2() - 1, &colorBlack, 0.5);
      resultImage.draw_rectangle(coord2.first + 1, coord1.second, sliceBox.getX2() - 1, coord2.second, &colorBlack, 0.5);
    }
    else
      resultImage.draw_rectangle(sliceBox.getX1() + 1, sliceBox.getY1() + 1, sliceBox.getX2() - 1, sliceBox.getY2() - 1, &colorBlack, 0.5);
  }

  redrawSliceBorder();

  return resultImage;
}

CImg<unsigned char> & DDisplay::redrawSliceBorder() {
  if (activeBox == sliceBox.getId())
    drawBox(sliceBox, true, true);
  else
    drawBox(sliceBox, true, false);

  return resultImage;
}


void DDisplay::drawBox(int x1, int y1, int x2, int y2, const unsigned char color, float alpha) {
  resultImage.draw_line(x1, y1, x1, y2, &color, alpha);
  resultImage.draw_line(x2, y1, x2, y2, &color, alpha);
  resultImage.draw_line(x1, y1, x2, y1, &color, alpha);
  resultImage.draw_line(x1, y2, x2, y2, &color, alpha);
}

void DDisplay::fillBox(const DDBox & box, const unsigned char color) {
  resultImage.draw_rectangle(box.getX1(), box.getY1(), box.getX2(), box.getY2(), &color);
}


void DDisplay::drawBox(const DDBox & box, unsigned char color1, unsigned char color2) {
  resultImage.draw_line(box.getX1(), box.getY1(), box.getX1(), box.getY2(), &color1);
  resultImage.draw_line(box.getX2(), box.getY1(), box.getX2(), box.getY2(), &color2);
  resultImage.draw_line(box.getX1(), box.getY1(), box.getX2(), box.getY1(), &color1);
  resultImage.draw_line(box.getX1(), box.getY2(), box.getX2(), box.getY2(), &color2);
}

void DDisplay::drawBox(const DDBox & box, bool inset, bool active) {

  if (active)
    if (inset)
      drawBox(box, activeColor1, activeColor2);
    else
      drawBox(box, activeColor2, activeColor1);
  else
    if (inset)
      drawBox(box, inactiveColor1, inactiveColor2);
    else
      drawBox(box, inactiveColor2, inactiveColor1);

}

void DDisplay::drawTriangle(unsigned int x1, unsigned int y1,
			    unsigned int x2, unsigned int y2,
			    unsigned int x3, unsigned int y3,
			    bool active) {

  resultImage.draw_line(x1, y1, x2, y2, (active?&activeColor1:&inactiveColor1));
  resultImage.draw_line(x2, y2, x3, y3, (active?&activeColor1:&inactiveColor1));
  resultImage.draw_line(x3, y3, x1, y1, (active?&activeColor1:&inactiveColor1));

}
