/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DDISPLAY
#define DDISPLAY

#include "DDScrollBar.h"
#include "DDBox.h"

#include "Mapping2D3D.h"
#include "Electrodes.h"

namespace Taglut {
  /**
     @class DDisplay
     Dynamic display
  */
  class DDisplay {
    friend class DDScrollBar;

  private:
    static const unsigned int buttonHeight;

    unsigned char activeColor1;
    unsigned char activeColor2;
    unsigned char inactiveColor1;
    unsigned char inactiveColor2;

    int minx;
    int maxx;
    int miny;
    int maxy;
    int minz;
    int maxz;

    bool displayBBox;

    /** Window title */
    std::string title;

    /** Mapping2D3D to display */
    Mapping2D3D & mapping;

    /** MRI or 3d image */
    const CImg<unsigned char> & img;

    /** Unfolded map image */
    CImg<unsigned char> uMap;

    /** Slice ratio */
    double zratio;

    /** Display or not cross */
    bool dCross;

    /** result image */
    CImg<unsigned char> resultImage;

    /**
       cross location
    */
    Point2D3D crossLocation;

    /** map box */
    DDBox uMapBox;

    /** Slice box */
    DDBox sliceBox;

    /** active box id */
    unsigned int activeBox;

    /** old active box id */
    unsigned int oldActiveBox;

    /** scrollbar slice */
    DDScrollBar sliceSB;

    DDBox toogleBBox;

    /** electrode list  */
    ElectrodeList eList;

    /** id of the current electrode */
    int numElectrode;

    /**
       Redraw display
    */
    CImg<unsigned char> & redraw();

    /**
       Redraw map
    */
    CImg<unsigned char> & redrawMap();

    /**
       Redraw slice
    */
    CImg<unsigned char> & redrawSlice();

    /**
       Redraw display borders
    */
    CImg<unsigned char> & redrawBorders();

    /**
       Redraw map border
    */
    CImg<unsigned char> & redrawMapBorder();

    /**
       Redraw slice border
    */
    CImg<unsigned char> & redrawSliceBorder();

    /**
       Redraw toogle button for bounding box
    */
    CImg<unsigned char> &  redrawToogleBBox();


    /**
       Draw cross at given location
    */
    void drawCross(unsigned int x, unsigned int y, unsigned int size = 2);

    /**
       Draw cross at given location
    */
    void drawCross(unsigned int x, unsigned int y, const unsigned char color, unsigned int size = 2);

    /**
       Draw box border
    */
    void drawBox(int x1, int y1, int x2, int y2, const unsigned char color, float alpha = 1.);

    /**
       Draw box border
    */
    void drawBox(const DDBox & box, unsigned char color1 = 100, unsigned char color2 = 150);

    /**
       Draw box border (active or not, inset or outset)
    */
    void drawBox(const DDBox & box, bool inset, bool active);

    /**
       Draw triangle (active or not)
    */
    void drawTriangle(unsigned int x1, unsigned int y1,
		      unsigned int x2, unsigned int y2,
		      unsigned int x3, unsigned int y3,
		      bool active);

    /**
       fill box using color given in parameter
    */
    void fillBox(const DDBox & box, const unsigned char color);


    /**
       Init display
    */
    void initDisplay();


  public:

    /**
       Default constructor
    */
    DDisplay(std::string title, Mapping2D3D & mapping,
	     const CImg<unsigned char> & imgTexture, const CImg<unsigned char> & img,
	     double zratio);

    /**
       Default constructor
    */
    DDisplay(std::string title, Mapping2D3D & mapping,
	     const ElectrodeList & eList, const CImg<unsigned char> & img,
	     double zratio);


    /**
       Display interface
    */
    bool display();

  };
}

#endif
