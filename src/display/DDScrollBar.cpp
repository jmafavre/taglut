/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "DDisplay.h"
#include "DDScrollBar.h"

using namespace Taglut;

DDScrollBar::DDScrollBar(DDisplay & ddisplay_l, int x1_l, int y1_l,
			 int sizex_l, int sizey_l,
			 unsigned int nbSteps_l) : DDBox(x1_l, y1_l, sizex_l, sizey_l), nbSteps(nbSteps_l), ddisplay(ddisplay_l),
						   firstArrow(x1_l, y1_l, sizey_l, sizey_l), secondArrow(x2 - sizey_l + 1, y1_l, sizey_l, sizey_l),
						   scrollBox(x1_l + sizey_l + 1, y1_l, sizex_l - 2 * (sizey_l + 1), sizey_l),
						   size(((double)scrollBox.getSizeX() - 2) / nbSteps_l), cursorBox(x1_l + sizey_l + 2, y1_l + 1, (unsigned int)size, sizey_l - 2, false),
						 subset(cursorBox.getX1(), cursorBox.getY1(), -1, cursorBox.getSizeY(), false) {
  location = 0;
  status = 0;
  dSubset = false;
}

void DDScrollBar::setLocation(unsigned int l) {
  assert(l < nbSteps);
  location = l;
  cursorBox.setX1(scrollBox.getX1() + 1 + (unsigned int) (size * location));
}

bool DDScrollBar::isActive() const {
  return (status != 0);
}

void DDScrollBar::setInactive() {
  status = 0;
}

void DDScrollBar::setSubset(int subsetX1, int subsetX2) {
  subset.setX1(scrollBox.getX1() + 1 + (int)(subsetX1 * size));
  subset.setX2(scrollBox.getX1() + 1 + (int)(subsetX2 * size));
  dSubset = true;
}

void DDScrollBar::displaySubset(bool value) {
  dSubset = value;
}


void DDScrollBar::redraw(unsigned int activeBox) {
  const unsigned char colorBlack = 0;
  const unsigned char subset1 = 90;
  const unsigned char subset2 = 50;

  ddisplay.fillBox(*this, colorBlack);
  if (activeBox == scrollBox.getId()) {
    ddisplay.drawBox(scrollBox, true, true);
    if (dSubset && (subset.getSizeX() != -1))
      ddisplay.fillBox(subset, subset1);
    ddisplay.drawBox(cursorBox, false, true);
  }
  else {
    ddisplay.drawBox(scrollBox, true, false);
    if (dSubset && (subset.getSizeX() != -1))
      ddisplay.fillBox(subset, subset2);
    ddisplay.drawBox(cursorBox, false, false);
  }

  ddisplay.drawBox(firstArrow, activeBox == firstArrow.getId(), activeBox == firstArrow.getId());
  ddisplay.drawTriangle(firstArrow.getX1() + 1, firstArrow.getY1() + firstArrow.getSizeY() / 2,
			firstArrow.getX2() - 1, firstArrow.getY1() + 1,
			firstArrow.getX2() - 1, firstArrow.getY2() - 1, (activeBox == firstArrow.getId()));


  ddisplay.drawBox(secondArrow, activeBox == secondArrow.getId(), activeBox == secondArrow.getId());
  ddisplay.drawTriangle(secondArrow.getX1() + 1, secondArrow.getY1() + 1,
			secondArrow.getX2() - 1, secondArrow.getY1() + secondArrow.getSizeY() / 2,
			secondArrow.getX1() + 1, secondArrow.getY2() - 1, (activeBox == secondArrow.getId()));


}

unsigned int DDScrollBar::getIdInside(int x, int y) const {
  if (firstArrow.isInside(x, y))
    return firstArrow.getId();
  else if (secondArrow.isInside(x, y))
    return secondArrow.getId();
  else if (scrollBox.isInside(x, y))
    return scrollBox.getId();
  else
    return id;
}

unsigned int DDScrollBar::updateSlice(int x, int y) {
  if (status == 3) {
    double l = firstLocation + (((double) x - firstLocationMouse) / size);
    if (l < 0) location = 0;
    else if (l > nbSteps - 1) location = nbSteps - 1;
    else location = (unsigned int)l;
  }
  else if (firstArrow.isInside(x, y) || (status == 1)) {
    if (location > 0) --location;
    status = 1;
  }
  else if (secondArrow.isInside(x, y) || (status == 2)) {
    if (location < nbSteps - 1) ++location;
    status = 2;
  }
  else if (scrollBox.isInside(x, y)) {
    status = 3;
    firstLocation = location;
    firstLocationMouse = x;
  }

  return location;
}

