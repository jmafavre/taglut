/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "DDBox.h"

using namespace Taglut;

unsigned int DDBox::lastId = 0;

DDBox::DDBox(int x1_t, int y1_t, int sizex_t, int sizey_t, bool active) :
  x1(x1_t), y1(y1_t), x2(x1_t + sizex_t - 1), y2(y1_t + sizey_t - 1), sizex(sizex_t), sizey(sizey_t) {
  if (active) {
    ++lastId;
    id = lastId;
  }
  else
    id = 0;
}

bool DDBox::isInside(int x, int y) const {
  return ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2));
}

std::pair<int, int> DDBox::local2Absolute(int x, int y) const {
  return std::pair<int, int>(x1 + x, y1 + y);
}

std::pair<int, int> DDBox::absolute2local(int x, int y) const {
  return std::pair<int, int>(x - x1, y - y1);
}

void DDBox::setX1(int x) {
  x1 = x;
  x2 = x + sizex - 1;
}

void DDBox::setX2(int x) {
  x2 = x;
  sizex = x2 - x1 + 1;
}

int DDBox::getX1() const {
  return x1;
}

int DDBox::getY1() const {
  return y1;
}

int DDBox::getX2() const {
  return x2;
}

int DDBox::getY2() const {
  return y2;
}

int DDBox::getSizeX() const {
  return sizex;
}

int DDBox::getSizeY() const {
  return sizey;
}

unsigned int DDBox::getId() const {
  return id;
}
