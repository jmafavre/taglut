/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <sstream>

#include "MeshMap.h"
#include "CImgUse.h"
#include "Display3D.h"
#include "DisplayGL.h"
#include "Mesh.h"
#include "MeshPathes.h"
#include "Mapping2D3D.h"
#include "Mapping2D3DList.h"
#include "MeshList.h"
#include "ColorChooser.h"
#include "MeshPart.h"
#include "PointCloud.h"

using namespace Taglut;

bool Display3D::displayMesh(unsigned int sizex, unsigned int sizey, std::string title, Mesh & mesh, bool meshes, bool border, bool opengl, bool smooth, bool autolight) {
  CImgList<double> points = mesh.getCImgPoints();
  if (border) {
    CImgList<unsigned char> colors;
    CImgList<VertexID> primitives;
    mesh.buildBorderColor(colors, primitives);
    return displayCImg(sizex, sizey, title, points, primitives, colors, meshes, opengl);
  }
  else {
    CImgList<VertexID> primitives = mesh.getCImgPrimitives();
    return displayCImg(sizex, sizey, title, points, primitives, meshes, opengl, smooth, autolight);
  }
}


bool Display3D::displayMeshMapTriangles(unsigned int sizex, unsigned int sizey, std::string title, Mesh & mesh, const MeshMap & meshMap, double margin, bool meshes, bool opengl, bool smooth, bool autolight) {
  CImgList<double> points = mesh.getCImgPoints();
  CImgList<VertexID> primitives;
  CImgList<unsigned char> colors(mesh.getNbTriangles(), CImg<unsigned char>::vector(255, 255, 255));


  CImg<unsigned int> primitive(3);

  double min = meshMap.getMinValue() - margin;
  double scale = meshMap.getMaxValue() - min - 2 * margin;

  // build triangle values
  for(Mesh::const_triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t) {
    double value = (meshMap[(*t).getVertexId(0)] + meshMap[(*t).getVertexId(1)] + meshMap[(*t).getVertexId(2)]) / 3;
    for(unsigned int i = 0; i < 3; ++i)
      primitive[i] = (*t).getVertexId(i);
    primitives.insert(primitive);
    colors((*t).getId())[1] = colors((*t).getId())[2] = (value - min) * 255 / scale;
  }

  return displayCImg(sizex, sizey, title, points, primitives, colors, meshes, opengl, smooth, autolight);
}




bool Display3D::displayPointsOnMesh(unsigned int sizex, unsigned int sizey, std::string title,
				    Mesh & mesh, const std::deque<VertexID> & hPoints,
				    bool meshes, bool opengl, bool smooth, bool autolight) {
  CImgList<double> points = mesh.getCImgPoints();
  CImgList<VertexID> primitives = mesh.getCImgPrimitives();

  CImgList<unsigned char> colors(mesh.getNbTriangles(), CImg<unsigned char>::vector(255, 255, 255));


  unsigned int it = 0;
  for(std::deque<VertexID>::const_iterator p = hPoints.begin(); p != hPoints.end(); ++p, ++it) {
    CImg<unsigned char> c = ColorChooser::getColorFromId(it, hPoints.size());
    for(std::deque<TriangleID>::const_iterator t = mesh.point(*p).getTriangles().begin();
	t != mesh.point(*p).getTriangles().end(); ++t)
      colors(*t) = c;
  }

  return displayCImg(sizex, sizey, title, points, primitives, colors, meshes, opengl, smooth, autolight);
}

bool Display3D::displayMeshPart(unsigned int sizex, unsigned int sizey, std::string title,
				MeshPart & meshPart,
				bool meshes, bool opengl, bool smooth, bool autolight) {

  CImgList<double> points = meshPart.getMesh().getCImgPoints();
  CImgList<VertexID> primitives = meshPart.getMesh().getCImgPrimitives();

  CImg<unsigned char> inside = CImg<unsigned char>::vector(255, 255, 255);


  CImgList<unsigned char> colors(meshPart.getMesh().getNbTriangles(), CImg<unsigned char>::vector(0, 0, 0));


  for(TriangleID itT = 0; itT < meshPart.getMesh().getNbTriangles(); ++itT)
    if (meshPart.isInsideTriangle(itT))
      colors(itT) = inside;

  return displayCImg(sizex, sizey, title, points, primitives, colors, meshes, opengl, smooth, autolight);
}

bool Display3D::displayMeshTexture(unsigned int sizex, unsigned int sizey, std::string title, Mesh & mesh, const UCharCImg & imgTexture, double zratio, bool opengl, bool smooth, bool autolight) {
  CImgList<double> points = mesh.getCImgPoints();
  CImgList<VertexID> primitives;
  CImgList<unsigned char> textures;

  mesh.buildTexturedTriangleList(imgTexture, primitives, textures, zratio);

  return displayCImg(sizex, sizey, title, points, primitives, textures, false, opengl, smooth, autolight);
}


bool Display3D::displayMeshPathes(unsigned int sizex, unsigned int sizey, std::string title, MeshPathes & pathes, bool meshes, bool opengl, bool smooth, bool autolight) {
  if (opengl)
    std::cout << "Warning: Opengl display not available for MeshCuts." << std::endl;

  CImgList<double> points = pathes.getMesh().getCImgPoints();

  CImgList<unsigned char> colors;
  CImgList<VertexID> primitives;
  std::deque<unsigned int> caption = pathes.buildBorderColor(colors, primitives);

  try {
    CImgDisplay captionDisplay = buildCaption(caption, caption.size() != pathes.size());
    CImgDisplay meshDisplay = buildDisplayCImg(sizex, sizey, title, points, primitives, colors, meshes, smooth, autolight);
  }
  catch (Exception e) {
    return false;
  }
  return true;
}


bool Display3D::displayMapping(unsigned int sizex, unsigned int sizey, std::string title, Mapping2D3D & mapping, bool dim3d, bool meshes, bool border, bool ratio, bool opengl, bool smooth, bool autolight) {
  CImgList<VertexID> primitives = mapping.getCImgPrimitives();

  if (dim3d) {
    CImgList<double> points = mapping.getCImgPoints();

    if (border) {
      Mesh mesh(points, primitives);
      return displayMesh(sizex, sizey, title, mesh, meshes, border, opengl, smooth, autolight);
    }
    else if (ratio) {
      CImgList<unsigned char> ratios;
      mapping.computeTriangleRatios(ratios);
      return displayCImg(sizex, sizey, title, points, primitives, ratios, meshes, opengl, smooth, autolight);
    }
    else
      return displayCImg(sizex, sizey, title, points, primitives, meshes, opengl, smooth, autolight);
  }
  else {
    if (!mapping.isFlatten())
      throw Exception("Mapping has not been computed.");
    CImgList<double> points;

    try {
      mapping.computeUnfoldedCImgPoints(points);
    }
    catch (Exception e) {
      std::cout << "Error: " << e << std::endl;
      return false;
    }

    if (border) {
      Mesh mesh(points, primitives);
      return displayMesh(sizex, sizey, title, mesh, meshes, border, opengl, smooth, autolight);
    }
    else if (ratio) {
      CImgList<unsigned char> ratios;
      mapping.computeTriangleRatios(ratios);
      return displayCImg(sizex, sizey, title, points, primitives, ratios, meshes, opengl, smooth, autolight);
    }
    else {
      return displayCImg(sizex, sizey, title, points, primitives, meshes, opengl, smooth, autolight);
    }
  }

}

bool Display3D::displayMapping(unsigned int sizex, unsigned int sizey, std::string title, Mapping2D3D & mapping, const CImg<unsigned char> & imgTexture, bool dim3d, bool opengl, double zratio, bool smooth, bool autolight) {
  CImgList<unsigned char> textures;
  CImgList<VertexID> primitives;

  if (imgTexture._depth == 0) {
    textures.insert(imgTexture);
    mapping.buildTexturedTriangleList(imgTexture, primitives);
  } else
    mapping.buildTexturedTriangleList(imgTexture, primitives, textures, zratio);

  if (dim3d) {
    CImgList<double> points = mapping.getCImgPoints();
    return displayCImg(sizex, sizey, title, points, primitives, textures, false, opengl, smooth, autolight);
  }
  else {
    if (!mapping.isFlatten())
      throw Exception("Mapping has not been computed.");
    CImgList<double> points;
    mapping.computeUnfoldedCImgPoints(points);
    return displayCImg(sizex, sizey, title, points, primitives, textures, false, opengl, smooth, autolight);
  }

}


bool Display3D::displayMappingDynamicMap(unsigned int sizex, unsigned int sizey, std::string title, Mapping2D3D & mapping, bool smooth, bool autolight) {
  if (!mapping.isFlatten())
    throw Exception("Mapping has not been computed.");

  MeshList meshes;
  meshes.push_front(mapping.getMesh());
  Mapping2D3DList mappings(meshes);
  mappings.push_front(mapping);

  const CImgList<unsigned char> colors(mapping.getNbTriangles(), CImg<unsigned char>::vector(100, 200, 255));

  DisplayDynamicGL displayOGL(sizex, sizey, title, mappings, colors, true, smooth, autolight);
  displayOGL.disp();
  return true;

}



bool Display3D::displayCImg(unsigned int sizex, unsigned int sizey, std::string title, const CImgList<double>  & points, const CImgList<VertexID> & primitives, bool meshes, bool opengl, bool smooth, bool autolight) {
  const CImgList<unsigned char> colors(primitives._width, CImg<unsigned char>::vector(100, 200, 255));

  return displayCImg(sizex, sizey, title, points, primitives, colors, meshes, opengl, smooth, autolight);
}

bool Display3D::displayCImg(unsigned int sizex, unsigned int sizey, std::string title, const CImgList<double>  & points, const CImgList<VertexID> & primitives, const CImgList<unsigned char> & colors, bool meshes, bool opengl, bool smooth, bool autolight) {

  if (opengl) {
    if (DisplayGL::isAvailable()) {
      std::deque<Coord3D> coords;
      Coord3D coord;
      cimglist_for(points, p) {
	coord.setX(points[p](0));
	coord.setY(points[p](1));
	coord.setZ(points[p](2));
	coords.push_back(coord);
      }
      DisplaySimpleGL displayOGL(sizex, sizey, title, coords, primitives, colors, meshes, smooth, autolight);
      displayOGL.disp();
      return true;
    }
    else
      std::cout << "Warning: Opengl not available. Using default randerer." << std::endl;
  }

  try {
    buildDisplayCImg(sizex, sizey, title, points, primitives, colors, meshes, smooth, autolight);
  } catch (Exception e) {
    return false;
  }

  return true;
}


CImgDisplay Display3D::buildDisplayCImg(unsigned int sizex, unsigned int sizey, std::string title, const CImgList<double>  & points, const CImgList<VertexID> & primitives, const CImgList<unsigned char> & colors, bool meshes, bool smooth, bool) {

  if (primitives._width == 0) {
    std::cerr << "Display error: Mesh is empty" << std::endl;
    throw Exception();
  }
  std::cout << "(Display)" << std::endl;

  CImgDisplay disp3d(sizex, sizey, title.c_str(), 0);
  UCharCImg visu3d(disp3d._width, disp3d._height, 1, 3, 0);

  if (smooth)
    visu3d.display_object3d(disp3d, points > 'x', primitives, colors, true, 4, -1, true, 500.0f, 0.0, 0.0);
  else if (meshes)
    visu3d.display_object3d(disp3d, points > 'x', primitives, colors, true, 1, -1, true, 500.0f, 0.0, 0.0);
  else
    visu3d.display_object3d(disp3d, points > 'x', primitives, colors, true, 3, -1, true, 500.0f, 0.0, 0.0);

  return disp3d;
}

CImgDisplay Display3D::buildCaption(const std::deque<unsigned int> & caption, bool borderColor) {
  unsigned int width = 35;
  unsigned int height = 50;
  unsigned char color[3];
  CImg<unsigned char> visu(width * caption.size() + (borderColor ? 10 : 0), height, 1, 3, 0);
  unsigned int i = 0;

  for(std::deque<unsigned int>::const_iterator c = caption.begin(); c != caption.end(); ++c) {
    color[0] = *c % 256;
    color[1] = (*c / 256) % 256;
    color[2] = *c / 256 / 256;
    visu.draw_rectangle(i, 0, i + width - 1, height - 1, color, 1);
    i += width;
    if ((c == caption.begin()) && borderColor)
      i += 10;
  }


  CImgDisplay disp(visu, "Caption");
  return disp;
}


bool Display3D::displayMappingDynamicDistance(unsigned int sizex, unsigned int sizey, std::string title,
					    Mapping2D3D & mapping) {
  if (!mapping.isFlatten())
    throw Exception("Mapping has not been computed.");
  bool displayTriangles = true;
  bool needRedraw = false;
  int lastX = 0, lastY = 0;
  const unsigned char white = 255;
  MeshManipulator meshManip(mapping.getMesh());
  std::deque<double> distances;

  if (mapping.size() == 0)
    throw Exception(std::string("Display error: mapping is empty"));
  std::cout << "(Display)" << std::endl;

  CImg<unsigned char> img(sizex, sizey, 1, 1, 0);
  CImgDisplay display(img, title.c_str());


  mapping.toImageDrawBorderTriangles(img, (unsigned char)255);

  while(!display._is_closed) {
    if (display._is_resized) {
      display.resize(false);
      mapping.resize2DCoords(0, 0, display._width, display._height);
      img.resize(display._width, display._height);
      img.fill(0);
      needRedraw = true;
    }

    if (display.key() == 't') {
      displayTriangles = !displayTriangles;
      needRedraw = true;
      display.set_key();
    }

    if (display._button & 1) // left mouse button pressed
      if ((lastX != display._mouse_x) || (lastY != display._mouse_y)) {
	int x = display._mouse_x;
	int y = display._mouse_y;
	needRedraw = true;

	// find nearest point
	const Point2D3D cCenter = mapping.getNearestPoint(x, y);
	// then compute distance from this point
	meshManip.computeDistancesStickCuttings(cCenter.getId(), distances);

	lastX = x;
	lastY = y;
      }


    if (display._button & 2) {
      displayTriangles = !displayTriangles;
      needRedraw = true;
      display._button -= 2;
    }

    // if needed, redraw
    if (needRedraw) {
      needRedraw = false;
      if (distances.size() != 0)
	// then redraw triangles in image
	for(TriangleID t = 0; t < mapping.getNbTriangles(); ++t) {
	  const Triangle & tr = mapping.getTriangle(t);
	  img.draw_triangle((int) mapping[tr.getP1()].get2DX(), (int) mapping[tr.getP1()].get2DY(),
			    (int) mapping[tr.getP2()].get2DX(), (int) mapping[tr.getP2()].get2DY(),
			    (int) mapping[tr.getP3()].get2DX(), (int) mapping[tr.getP3()].get2DY(),
			    &white,
			    1 / (1 + distances[tr.getP1()]), 1 / (1 + distances[tr.getP2()]), 1 / (1 + distances[tr.getP3()]));
	}
      if (displayTriangles)
	if (distances.size() != 0)
	  mapping.toImageDrawBorderTriangles(img, (unsigned char)0);
    }
    img.display(display);
    display.wait(30);
  }

  return true;
}




bool Display3D::displayMeshList(unsigned int sizex, unsigned int sizey, std::string title,
				MeshList & meshes,
				bool meshesb, bool border, bool opengl, bool smooth, bool autolight) {
  CImgList<double> pointsAll;
  for(MeshList::iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
    pointsAll.insert((*mesh).getCImgPoints());
  }

  if (border) {
    CImgList<unsigned char> colorsAll;
    CImgList<VertexID> primitivesAll;
    VertexID trPoints = 0;
    for(MeshList::iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
      CImgList<unsigned char> colors;
      CImgList<VertexID> primitives;
      (*mesh).buildBorderColor(colors, primitives);
      cimglist_for(primitives, p) {
	primitives(p)(0) += trPoints;
	primitives(p)(1) += trPoints;
	primitives(p)(2) += trPoints;
      }
      colorsAll.insert(colors);
      primitivesAll.insert(primitives);
      trPoints += (*mesh).getNbPoints();
    }
    return displayCImg(sizex, sizey, title, pointsAll, primitivesAll, colorsAll, meshesb, opengl, smooth, autolight);
  }
  else {
    CImgList<VertexID> primitivesAll;
    VertexID trPoints = 0;

    for(MeshList::iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
      CImgList<VertexID> primitives = (*mesh).getCImgPrimitives();
      cimglist_for(primitives, p) {
	primitives(p)(0) += trPoints;
	primitives(p)(1) += trPoints;
	primitives(p)(2) += trPoints;
      }
      primitivesAll.insert(primitives);
      trPoints += (*mesh).getNbPoints();
    }

    return displayCImg(sizex, sizey, title, pointsAll, primitivesAll, meshesb, opengl, smooth, autolight);
  }
}



bool Display3D::displayMappingList(unsigned int sizex, unsigned int sizey, std::string title,
				   Mapping2D3DList & mappings,
				   bool dim3d, bool meshes, bool border, bool ratio, bool opengl, bool smooth, bool autolight) {

  CImgList<VertexID> primitivesAll;
  VertexID trPoints = 0;
  for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
    CImgList<VertexID> primitives = (*mapping).getCImgPrimitives();
    cimglist_for(primitives, p) {
      primitives(p)(0) += trPoints;
      primitives(p)(1) += trPoints;
      primitives(p)(2) += trPoints;
    }
    primitivesAll.insert(primitives);
    trPoints += (*mapping).size();
  }

  if (dim3d) {
    CImgList<double> pointsAll;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
      pointsAll.insert((*mapping).getCImgPoints());
    }

    if (border) {
      Mesh mesh(pointsAll, primitivesAll);
      return displayMesh(sizex, sizey, title, mesh, meshes, border, opengl);
    }
    else if (ratio) {
      CImgList<unsigned char> ratiosAll;
      for(Mapping2D3DList::const_iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
	CImgList<unsigned char> ratios;
	(*mapping).computeTriangleRatios(ratios);
	ratiosAll.insert(ratios);
      }
      return displayCImg(sizex, sizey, title, pointsAll, primitivesAll, ratiosAll, meshes, opengl);
    }
    else
      return displayCImg(sizex, sizey, title, pointsAll, primitivesAll, meshes, opengl);
  }
  else {
    CImgList<double> pointsAll;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
      if (!(*mapping).isFlatten())
	throw Exception("Mapping has not been computed.");
      CImgList<double> points;

      try {
	(*mapping).computeUnfoldedCImgPoints(points);
      }
      catch (Exception e) {
	std::cout << "Error: " << e << std::endl;
	return false;
      }
      pointsAll.insert(points);
    }

    if (border) {
      Mesh mesh(pointsAll, primitivesAll);
      return displayMesh(sizex, sizey, title, mesh, meshes, border, smooth, autolight);
    }
    else if (ratio) {
      CImgList<unsigned char> ratiosAll;
      for(Mapping2D3DList::const_iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
	CImgList<unsigned char> ratios;
	(*mapping).computeTriangleRatios(ratios);
	ratiosAll.insert(ratios);
      }
      return displayCImg(sizex, sizey, title, pointsAll, primitivesAll, ratiosAll, meshes, opengl, smooth, autolight);
    }
    else
      return displayCImg(sizex, sizey, title, pointsAll, primitivesAll, meshes, opengl, smooth, autolight);
  }

}


bool Display3D::displayMappingListDynamicMap(unsigned int sizex, unsigned int sizey, std::string title, Mapping2D3DList & mappings, bool smooth, bool autolight) {
  const CImgList<unsigned char> colors(mappings.getNbTriangles(), CImg<unsigned char>::vector(100,200,255));

  DisplayDynamicGL displayOGL(sizex, sizey, title, mappings, colors, true, smooth, autolight);
  displayOGL.disp();
  return true;
}

bool Display3D::displayPointCloud(unsigned int sizex, unsigned int sizey, std::string title, PointCloud & pCloud) {

  DisplayPointCLoudGL displayOGL(sizex, sizey, title, pCloud);
  displayOGL.disp();
  return true;
}

