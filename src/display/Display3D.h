/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DISPLAY3D
#define DISPLAY3D

#include <string>
#include <iostream>
#include <deque>


#ifndef MESH_MAP_DEFINED
#include "MeshMap.h"
#endif
#include "CImgUse.h"
#include "IDTypes.h"

namespace Taglut {
  class Mesh;
  class MeshPathes;
  class Mapping2D3D;
  class Mapping2D3DList;
  class MeshList;
  class MeshPart;
  class PointCloud;

  /**
     @class Display3D
     Class used to display a mesh or a circle set
  */
  class Display3D {
  private:
    static CImgDisplay buildDisplayCImg(unsigned int sizex, unsigned int sizey, std::string title,
					const CImgList<double>  & points,
					const CImgList<VertexID> & primitives, const CImgList<unsigned char> & colors,
					bool meshes, bool smooth = false, bool autolight = false);

    static CImgDisplay buildCaption(const std::deque<unsigned int> & caption, bool borderColor);


  public:
    /**
       Display a mesh
    */
    static bool displayMesh(unsigned int sizex, unsigned int sizey, std::string title,
			    Mesh & mesh,
			    bool meshes = false, bool border = false, bool opengl = false,
			    bool smooth = false, bool autolight = false);

    /**
       Display a mesh map using a color for each triangle computed as the mean of the values shared by the vertices.
    */
    static bool displayMeshMapTriangles(unsigned int sizex, unsigned int sizey, std::string title,
					Mesh & mesh, const MeshMap & meshMap, double margin = 0.1,
					bool meshes = false, bool opengl = false,
					bool smooth = false, bool autolight = false);

    /**
       Display a mesh map.
    */
    template <typename T>
    static bool displayMeshMap(unsigned int sizex, unsigned int sizey, std::string title,
			       Mesh & mesh, const MeshMapT<T> & meshMap, double margin = 0.1,
			       bool meshes = false, bool opengl = false,
			       bool smooth = false, bool autolight = false);

    /**
       Display a mesh map (double).
    */
    inline static bool displayMeshMapDouble(unsigned int sizex, unsigned int sizey, std::string title,
					    Mesh & mesh, const MeshMapT<double> & meshMap, double margin = 0.1,
					    bool meshes = false, bool opengl = false,
					    bool smooth = false, bool autolight = false) {
      return displayMeshMap(sizex, sizey, title, mesh, meshMap, margin, meshes, opengl, smooth, autolight);
    }

    /**
       Display a mesh map (unsigned int).
    */
    inline static bool displayMeshMapUInt(unsigned int sizex, unsigned int sizey, std::string title,
					  Mesh & mesh, const MeshMapT<unsigned int> & meshMap, double margin = 0.1,
					  bool meshes = false, bool opengl = false,
					  bool smooth = false, bool autolight = false) {
      return displayMeshMap(sizex, sizey, title, mesh, meshMap, margin, meshes, opengl, smooth, autolight);
    }

    /**
       Display a mesh highlighting the given points using a specific color.
    */
    static bool displayPointsOnMesh(unsigned int sizex, unsigned int sizey, std::string title,
				    Mesh & mesh, const std::deque<VertexID> & hPoints,
				    bool meshes = false, bool opengl = false,
				    bool smooth = false, bool autolight = false);

    /**
       Display a mesh using texture
    */
    static bool displayMeshTexture(unsigned int sizex, unsigned int sizey, std::string title,
				   Mesh & mesh, const UCharCImg & imgTexture, double zratio = 1, bool opengl = false,
				   bool smooth = false, bool autolight = false);

    /**
       Display a mesh list
    */
    static bool displayMeshList(unsigned int sizex, unsigned int sizey, std::string title,
				MeshList & meshes,
				bool meshesb = false, bool border = false, bool opengl = false,
				bool smooth = false, bool autolight = false);

    /**
       Display a mesh and pathes
    */
    static bool displayMeshPathes(unsigned int sizex, unsigned int sizey, std::string title,
				  MeshPathes & pathes,
				  bool meshes = false, bool opengl = false,
				  bool smooth = false, bool autolight = false);

    /**
       Display a circle set list
    */
    static bool displayMappingList(unsigned int sizex, unsigned int sizey, std::string title,
				   Mapping2D3DList & mappings,
				   bool dim3d = false, bool meshes = false, bool border = false, bool ratio = false, bool opengl = false,
				   bool smooth = false, bool autolight = false);

    /**
       Display a circle set
    */
    static bool displayMapping(unsigned int sizex, unsigned int sizey, std::string title,
			       Mapping2D3D & mapping,
			       bool dim3d = false, bool meshes = false, bool border = false, bool ratio = false, bool opengl = false,
			       bool smooth = false, bool autolight = false);

    /**
       Display a circle set using texture
    */
    static bool displayMapping(unsigned int sizex, unsigned int sizey, std::string title,
			       Mapping2D3D & mapping, const CImg<unsigned char> & imgTexture,
			       bool dim3d = false, bool opengl = false, double zratio = 1,
			       bool smooth = false, bool autolight = false);

    /**
       Display a circle set using dynamic display (distance to clic)
    */
    static bool displayMappingDynamicDistance(unsigned int sizex, unsigned int sizey, std::string title,
					      Mapping2D3D & mapping);

    /**
       Display a CImg mesh (colors are defined automatically)
    */
    static bool displayCImg(unsigned int sizex, unsigned int sizey, std::string title,
			    const CImgList<double>  & points, const CImgList<VertexID> & primitives,
			    bool meshes = false, bool opengl = false, bool smooth = false, bool autolight = false);

    /**
       Display a CImg mesh
    */
    static bool displayCImg(unsigned int sizex, unsigned int sizey, std::string title,
			    const CImgList<double>  & points, const CImgList<VertexID> & primitives, const CImgList<unsigned char> & colors,
			    bool meshes = false, bool opengl = false, bool smooth = false, bool autolight = false);

    /**
       Display a circle set using dynamic display
    */
    static bool displayMappingDynamicMap(unsigned int sizex, unsigned int sizey, std::string title, Mapping2D3D & mapping, bool smooth = false, bool autolight = false);


    /**
       Display a circle set using dynamic display
    */
    static bool displayMappingListDynamicMap(unsigned int sizex, unsigned int sizey, std::string title, Mapping2D3DList & mappings, bool smooth = false, bool autolight = false);


    /**
       Display a MeshPart
    */
    static bool displayMeshPart(unsigned int sizex, unsigned int sizey, std::string title,
				MeshPart & meshPart,
				bool meshes = false, bool opengl = false, bool smooth = false, bool autolight = false);


    /**
       Display a PointCloud
    */
    static bool displayPointCloud(unsigned int sizex, unsigned int sizey, std::string title, PointCloud & pCloud);

  };

  template <typename T>
  bool Display3D::displayMeshMap(unsigned int sizex, unsigned int sizey, std::string title, Mesh & mesh, const MeshMapT<T> & meshMap, double margin, bool meshes, bool opengl, bool smooth, bool autolight) {
    CImgList<double> points = mesh.getCImgPoints();
    CImgList<VertexID> primitives;
    CImgList<unsigned char> textures;

    CImg<unsigned int> primitive(9);
    CImg<unsigned char> texture(64, 64, 1, 3);
    unsigned char c[] = {255, 255, 255};

    primitive[3] = 0; primitive[4] = 0;
    primitive[5] = 63; primitive[6] = 0;
    primitive[7] = 0; primitive[8] = 63;

    double min = meshMap.getMinValue() - margin;
    double scale = meshMap.getMaxValue() - min - 2 * margin;

    // build textured triangle list
    for(Mesh::const_triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t) {
      for(unsigned int i = 0; i < 3; ++i) {
	primitive[i] = (*t).getVertexId(i);
      }
      primitives.insert(primitive);
      texture.draw_triangle(0, 0, 63, 0, 0, 63, c,
			    (meshMap[(*t).getP1()] - min) / scale, (meshMap[(*t).getP2()] - min) / scale, (meshMap[(*t).getP3()] - min) / scale);
      textures.insert(texture);
    }

    return displayCImg(sizex, sizey, title, points, primitives, textures, meshes, opengl, smooth, autolight);
  }

}

#endif
