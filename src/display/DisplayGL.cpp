/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <algorithm>
#include <assert.h>
#include "Mapping2D3D.h"
#include "Mapping2D3DList.h"
#include "DisplayGL.h"
#include "Exception.h"
#include "PointCloud.h"
#include "PointCloudShapeEstimator.h"

using namespace Taglut;

#ifdef USE_GL
#include <unistd.h>
#include <sys/wait.h>
#include <sstream>
#include <fcntl.h>
#include <GL/glut.h>

#if !defined(GLUT_WHEEL_UP)
#  define GLUT_WHEEL_UP   3
#  define GLUT_WHEEL_DOWN 4
#endif


/* mouse location */
double x, y, z;
/* old mouse location */
int xold, yold;
/* button pressed ( 1: left) */
bool pressed;
/* angle */
int anglex, angley;
/* x/y screen ratio */
float ratio;
/* mesh center*/
double middleX, middleY, middleZ;
/* selected point */
VertexID selectedPoint;
/* ball radius */
double ballRadius;
/* display lines or fill triangles */
bool displayLines;
/* use smooth layout */
bool displaySmooth;
/* auto light */
bool autoLight;
/* draw edgesAndNormals */
bool drawEdgesAndNormals;
/* bool invert colors */
bool negative_colors;

GLfloat L0pos[] = {-5.0, -5.0, 20.0};
GLfloat L0dif[] = {0.6, 0.6, 0.6};
GLfloat L0spec[] = {0.6, 0.6, 0.6};
GLfloat Mspec[] = {0.5, 0.5, 0.5};
GLfloat Mshiny = 30;


DisplayGL * displaygl;
std::deque<Coord3D> normal;


void initNormalOGL() {
  ballRadius = std::numeric_limits<double>::max();

  const std::deque<Coord3D> & points = (*displaygl).getPoints();
  const CImgList<VertexID> & triangles = (*displaygl).getTriangles();
  selectedPoint = points.size() + 1;

  normal.clear();
  Coord3D n;

  cimglist_for(triangles, t) {
    const Coord3D & p1 = points[triangles[t](0)];
    const Coord3D & p2 = points[triangles[t](1)];
    const Coord3D & p3 = points[triangles[t](2)];

    n[0] = (p2(1) - p1(1)) * (p3(2) - p1(2)) -
      (p2(2) - p1(2)) * (p3(1) - p1(1));
    n[1] = (p2(2) - p1(2)) * (p3(0) - p1(0)) -
      (p2(0) - p1(0)) * (p3(2) - p1(2));
    n[2] = (p2(0) - p1(0)) * (p3(1) - p1(1)) -
      (p2(1) - p1(1)) * (p3(0) - p1(0));

    normal.push_back(n);

    for(unsigned int i = 0; i < 3; ++i) {
      double l = points[triangles[t](i)].distance(points[triangles[t]((i + 1) % 3)]);
      if (l < ballRadius)
	ballRadius = l;
    }
  }

  ballRadius /= 3;
}

void clearNormalOGL() {
  normal.clear();
}


void displayOGL() {
  const std::deque<Coord3D> & points = (*displaygl).getPoints();
  const CImgList<unsigned char> & colors = (*displaygl).getColors();
  const CImgList<VertexID> & triangles = (*displaygl).getTriangles();
  GLfloat color[3];
  GLfloat white[3] = {1, 1, 1};
  GLfloat black[3] = {0, 0, 0};
  GLfloat red[3] = {1, 0, 0};
  GLfloat redLight[3] = {0.1, 0, 0};

  // clear display
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // init rotation

  glLoadIdentity();

  glLightfv(GL_LIGHT0, GL_POSITION, L0pos);

  glTranslatef(0, 0, -z);

  glRotatef(-angley, 1.0, 0.0, 0.0);
  glRotatef(-anglex, 0.0, 1.0, 0.0);

  if (displayLines)
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  else
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  if (displaySmooth)
    glShadeModel(GL_SMOOTH);
  else
    glShadeModel(GL_FLAT);

  // for each triangle, draw it
  if ((triangles._width > 0) && (triangles[0].size() == 9)) {
    cimglist_for(triangles, t) {
      glBegin(GL_TRIANGLES);
      // TODO: Normals have to be updated according to the rotation
      if (!displaySmooth)
	glNormal3f(normal[t](0), normal[t](1), normal[t](2));

      for(unsigned int i = 0; i < 3; ++i) {
	const CImg<unsigned char> & c = colors[t];
	color[0] = (float)c(triangles[t](3 + 2 * i), triangles[t](4 + 2 * i), 0, 0) / 256;
	color[1] = (float)c(triangles[t](3 + 2 * i), triangles[t](4 + 2 * i), 0, 1) / 256;
	color[2] = (float)c(triangles[t](3 + 2 * i), triangles[t](4 + 2 * i), 0, 2) / 256;
	if (autoLight) {
	  glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, color);
	  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, negative_colors ? white : black);
	}
	else {
	  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);
	  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, color);
	}
	glVertex3f(points[triangles[t](i)](0) - middleX, points[triangles[t](i)](1) - middleY, points[triangles[t](i)](2) - middleZ);
      }
      glEnd();
    }
  }
  else if ((triangles._width > 0) && ((triangles[0].size() == 3) || (triangles[0].size() == 4))) {
    cimglist_for(triangles, t) {
      TriangleID size = triangles[t].size();
      glBegin(GL_POLYGON);
      if (!displaySmooth)
	glNormal3f(normal[t](0), normal[t](1), normal[t](2));
      color[0] = (float)colors[t](0) / 256;
      color[1] = (float)colors[t](1) / 256;
      color[2] = (float)colors[t](2) / 256;
      if (autoLight) {
 	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, color);
 	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, black);
 	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, black);
 	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, black);
      }
      else {
 	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);
 	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, black);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, black);
      }
      glColor3d(color[0], color[1], color[2]);
      for(TriangleID i = 0; i < size; ++i)
	glVertex3f(points[triangles[t](i)](0) - middleX, points[triangles[t](i)](1) - middleY, points[triangles[t](i)](2) - middleZ);
      glEnd();
    }
  }
  else {
    std::cerr << "Warning: unknown triangle size (" << triangles[0].size() << ")" << std::endl;
  }

  if (drawEdgesAndNormals) {
    glEnable(GL_AUTO_NORMAL);
    if (autoLight) {
      glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, negative_colors ? black : white);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, negative_colors ? black : white);
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, negative_colors ? black : white);
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, negative_colors ? black : white);
    }
    else {
      glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, negative_colors ? black : white);
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, negative_colors ? black : white);
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, negative_colors ? black : white);
    }
    if (negative_colors)
      glColor3d(0., 0., 0.);
    else
      glColor3d(1., 1., 1.);
    const std::vector<std::pair<Coord3D, Coord3D> > edgesAndNormals = (*displaygl).getEdgesAndNormals();
    for(std::vector<std::pair<Coord3D, Coord3D> >::const_iterator n = edgesAndNormals.begin(); n != edgesAndNormals.end(); ++n) {
      glBegin(GL_LINES);
      glVertex3f((*n).first(0) - middleX, (*n).first(1) - middleY, (*n).first(2) - middleZ);
      glVertex3f((*n).second(0) - middleX, (*n).second(1) - middleY, (*n).second(2) - middleZ);
      glEnd();
    }
    glDisable(GL_AUTO_NORMAL);
  }

  // if point has been selected in 2d view
  if (selectedPoint < points.size()) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glTranslatef(points[selectedPoint](0) - middleX, points[selectedPoint](1) - middleY, points[selectedPoint](2) - middleZ);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, red);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, redLight);
    glutSolidSphere(ballRadius, 10, 10);
  }

  glFlush();

  // swap buffers
  glutSwapBuffers();

  glutPostRedisplay();

}

void reshapeOGL(int x_l, int y_l) {
  if (x_l < y_l)
    glViewport(0, (y_l - x_l) / 2, x_l, x_l);
  else
    glViewport((x_l - y_l) / 2, 0, y_l, y_l);
}

void mouseOGL(int button, int state, int x_l, int y_l) {
  if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN)) { // left button pressed
    pressed = true;
    xold = x_l;
    yold = y_l;
  }

  if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_UP)) // left button unpressed
    pressed = false;

  if (state == GLUT_UP) {
    if (button == GLUT_WHEEL_UP) {
      z *= 1.2;
      glutPostRedisplay();
    }
    else if(button == GLUT_WHEEL_DOWN) {
      z *= 0.8;
      glutPostRedisplay();
    }
  }
}

void mousemotionOGL(int x_l, int y_l) {
  if (pressed) { // if left button pressed
    anglex = anglex - (x_l - xold);
    angley = angley - (y_l - yold);
    glutPostRedisplay();
  }
  // old location saved
  xold = x_l;
  yold = y_l;
}

void keyboardOGL(unsigned char key, int, int) {
  if (key == 27)
    exit(0);
}

void updateCurrentLocationOGL(int fdesc) {
  VertexID location;

  // read in pipe
  if (read(fdesc, &location, sizeof(VertexID)) != -1) {
    // if stop signal has been sended
    if (location > (*displaygl).getPoints().size())
      exit(0);
    selectedPoint = location;

    displayOGL();
  }

  glutTimerFunc(200, updateCurrentLocationOGL, fdesc);
}


bool glDisplayIsDown(pid_t pid) {
  int status = 0;

  pid_t pidR = waitpid (-1, &status, WNOHANG);

  return (pidR == pid);
}

void DisplayGL::initDisplay() {
  int valNb = 0;
  displaygl = this;
  initNormalOGL();
  anglex = angley = 0;
  ratio = sizex / sizey;

  /* compute mesh and camera location */
  z = 2 * computeLargestWidth();
  middleX = (maxX + minX) / 2;
  middleY = (maxY + minY) / 2;
  middleZ = (maxZ + minZ) / 2;

  /* glut and window initialization */
  glutInit(&valNb, NULL);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(sizex, sizey);
  glutCreateWindow((title + " (3D)").c_str());

  /* OpenGL initialization */
  if (negative)
    glClearColor(1.0, 1.0, 1.0, 1.0);
  else
    glClearColor(0.0, 0.0, 0.0, 0.0);
  glPointSize(2.0);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);

  /* define lights */
  displayLines = meshes;
  displaySmooth = smooth;
  autoLight = autolight;

  /* define colors */
  negative_colors = negative;

  /* define drawings */
  drawEdgesAndNormals = drawedgesandnormals;

  if (autoLight) {
    L0dif[0] = L0dif[1] = L0dif[2] = 0;
    L0spec[0] = L0spec[1] = L0spec[2] = 0.6;
    Mspec[0] = Mspec[1] = Mspec[2] = 0.2;
    Mshiny = 0;
  }

  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, L0dif);
  glLightfv(GL_LIGHT0, GL_SPECULAR, L0spec);

  /* define material properties */
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Mspec);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, Mshiny);

  /* set recall functions */
  glutDisplayFunc(displayOGL);
  glutReshapeFunc(reshapeOGL);
  glutKeyboardFunc(keyboardOGL);
  glutMouseFunc(mouseOGL);
  glutMotionFunc(mousemotionOGL);

  /* set perspective properties*/
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum( .01 * -z, .01 * z, .01 * -z, .01 * z, .01 * z, 3 * z);
  glMatrixMode(GL_MODELVIEW);

}


void DisplaySimpleGL::disp() {
  // fork
  pid_t pid;
  int status = 0;

  pid = fork();

  if (pid > 0) {
    wait(&status);
    return;
  }
  else if (pid == 0) {

    initDisplay();

    /* start glut loop */
    glutMainLoop();
    clearNormalOGL();

  }
  else {
    std::cerr << "Fork is needed by opengl display but not available" << std::endl;
    throw Exception(std::string("Fork not available."));
  }

}

void DisplayDynamicGL::disp() {
  const unsigned char red[3] = {255, 0, 0};
  const unsigned char white[3] = {255, 255, 255};
  const unsigned char black[3] = {0, 0, 0};

  // first create pipe
  int pfd[2];

  if (pipe(pfd) == -1) {
    std::cerr << "Pipe is needed by opengl display but not available" << std::endl;
    throw Exception(std::string("Pipe not available."));
  }

  // set non blocking property on pipe
  fcntl(pfd[0], F_SETFL, fcntl(pfd[0], F_GETFL) | O_NONBLOCK);

  // fork
  pid_t pid;
  int status = 0;
  VertexID id;

  pid = fork ();

  if (pid > 0) {
    close(pfd[0]); // close reading part of the pipe

    if (mappings.size() > 0) {
      VertexID lastDisplay = mappings.size();
      CImgList<unsigned char> uMaps;
      CImgList<unsigned char> uMapsDisplay;
      for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
	if (!(*mapping).isFlatten())
	  throw Exception("Mapping has not been computed.");
	CImg<unsigned char> uMap(sizex, sizey, 1, 3);
	uMap.fill((unsigned char) 0);
	if (meshes)
	  (*mapping).toImageDrawBorderTriangles(uMap, (unsigned char) 255);
	else
	  (*mapping).toImage(uMap, colors);
	uMaps.insert(uMap);
      }

      CImgDisplay ** displays;
      displays = new CImgDisplay *[mappings.size()];
      cimglist_for(uMaps, i) {
	CImg<unsigned char> uMapDisplay(sizex, sizey + 60, 1, 3);
	uMapDisplay.fill((unsigned char) 0);
	uMapDisplay.draw_image(0, 0, uMaps[i]);
	uMapsDisplay.insert(uMapDisplay);

	displays[i] = new CImgDisplay(uMapDisplay, (title + " (2D)").c_str());
      }

      bool running = true;
      while (running) {
	(*displays[0]).wait(200);

	// check for opengl status
	if (glDisplayIsDown(pid)) break;

	for(VertexID i = 0; i < uMaps._width; ++i) {
	  if ((*displays[i])._button) {
	    // find nearest point
	    const Point2D3D selected = mappings[i].getNearestPoint((*displays[i])._mouse_x, (*displays[i])._mouse_y);

	    // redraw mesh
	    uMapsDisplay[i].draw_image(0, 0, uMaps[i]);

	    // draw cursor
	    uMapsDisplay[i].draw_line((int)selected.get2DX() - 4, (int)selected.get2DY(), (int)selected.get2DX() + 4, (int)selected.get2DY(), red);
	    uMapsDisplay[i].draw_line((int)selected.get2DX(), (int)selected.get2DY() - 4, (int)selected.get2DX(), (int)selected.get2DY() + 4, red);

	    // clear message box
	    uMapsDisplay[i].draw_rectangle(0, sizey + 1, sizex - 1, uMapsDisplay[i]._height - 1, black);
	    // write text
	    std::ostringstream str;
	    str << selected;
	    uMapsDisplay[i].draw_text(3, sizey + 4, str.str().c_str(), white, black);

	    // send id to the 3d process using pipe
	    id = mappings.getGlobalId(i, selected.getId());
	    write(pfd[1], &id, sizeof(unsigned int));

	    // redraw display
	    uMapsDisplay[i].display(*displays[i]);

	    // clear old display
	    if ((lastDisplay != i) && (lastDisplay < mappings.size())) {
	      uMapsDisplay[lastDisplay].draw_image(0, 0, uMaps[lastDisplay]);
	      uMapsDisplay[lastDisplay].draw_rectangle(0, sizey + 1, sizex - 1, uMapsDisplay[lastDisplay]._height - 1, black);
	      uMapsDisplay[lastDisplay].display(*displays[lastDisplay]);
	    }
	    lastDisplay = i;
	  }
	  else if ((*displays[i]).key() == cimg::keyESC) {
	    running = false;
	    break;
	  }
	  if ((*displays[i])._is_closed) {
	    running = false;
	    break;
	  }
	}
      }

      cimglist_for(uMaps, i)
	delete(displays[i]);
      delete[] displays;
    }
    id = mappings.getNbPoints() + 1;
    write(pfd[1], &id, sizeof(unsigned int));

    wait(&status);

    close(pfd[1]); // close other part
    return;
  }
  else if (pid == 0) {
    close(pfd[1]); // close writing part of the pipe


    initDisplay();

    glutTimerFunc(200, updateCurrentLocationOGL, pfd[0]);

    /* start glut loop */
    glutMainLoop();
    clearNormalOGL();
    close(pfd[0]); // close other part

  }
  else {
    std::cerr << "Fork is needed by opengl display but not available" << std::endl;
    throw Exception(std::string("Fork not available."));
  }


}



void DisplayPointCLoudGL::disp() {
  // fork
  pid_t pid;
  int status = 0;

  pid = fork();

  if (pid > 0) {
    wait(&status);
    return;
  }
  else if (pid == 0) {

    initDisplay();

    /* start glut loop */
    glutMainLoop();
    clearNormalOGL();

  }
  else {
    std::cerr << "Fork is needed by opengl display but not available" << std::endl;
    throw Exception(std::string("Fork not available."));
  }
}


#else
void DisplaySimpleGL::disp() {
  return;
}

void DisplayDynamicGL::disp() {
  return;
}

void DisplayPointCLoudGL::disp() {
  return;
}
#endif



DisplaySimpleGL::DisplaySimpleGL(unsigned int sizex_t, unsigned int sizey_t,
				 const std::string & title_t, const std::deque<Coord3D> & points_t,
				 const CImgList<VertexID> & triangles_t,
				 const CImgList<unsigned char> & colors_t, bool meshes_t, bool smooth_t, bool autoLight_t, bool negative_t) :
  DisplayGL(sizex_t, sizey_t, title_t, meshes_t, smooth_t, autoLight_t, negative_t), points(points_t), triangles(triangles_t), colors(colors_t) {


  for(std::deque<Coord3D>::const_iterator p = points.begin(); p != points.end(); ++p) {
    if ((*p)(0) > maxX) maxX = (*p)(0);
    if ((*p)(0) < minX) minX = (*p)(0);
    if ((*p)(1) > maxY) maxY = (*p)(1);
    if ((*p)(1) < minY) minY = (*p)(1);
    if ((*p)(2) > maxZ) maxZ = (*p)(2);
    if ((*p)(2) < minZ) minZ = (*p)(2);
  }

}


const std::deque<Coord3D> & DisplaySimpleGL::getPoints() const {
  return points;
}

const CImgList<VertexID> & DisplaySimpleGL::getTriangles() const {
  return triangles;
}





/* DisplayDynamicGL methods */

DisplayDynamicGL::DisplayDynamicGL(unsigned int sizex_t, unsigned int sizey_t,
				   const std::string & title_t, Mapping2D3DList & mappings_t,
				   const CImgList<unsigned char> & colors_t, bool meshes_t, bool smooth_t, bool autoLight_t, bool negative_t) :
  DisplayGL(sizex_t, sizey_t, title_t, meshes_t, smooth_t, autoLight_t, negative_t), colors(colors_t), mappings(mappings_t) {
  VertexID idShift = 0;

  for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping) {
    for(Mesh::const_point_iterator c = (*mapping).point3D_begin(); c != (*mapping).point3D_end(); ++c) {
      if ((*c).getX() > maxX) maxX = (*c).getX();
      if ((*c).getX() < minX) minX = (*c).getX();
      if ((*c).getY() > maxY) maxY = (*c).getY();
      if ((*c).getY() < minY) minY = (*c).getY();
      if ((*c).getZ() > maxZ) maxZ = (*c).getZ();
      if ((*c).getZ() < minZ) minZ = (*c).getZ();

      points.push_back(*c);
    }

    CImgList<VertexID> pr = (*mapping).getCImgPrimitives();
    cimglist_for(pr, t)
      pr[t] += idShift;

    triangles.insert(pr);
    idShift += (*mapping).size();
  }

}


const std::deque<Coord3D> & DisplayDynamicGL::getPoints() const {
  return points;
}

const CImgList<VertexID> & DisplayDynamicGL::getTriangles() const {
  return triangles;
}




/* DisplayGL methods */

double DisplayGL::computeLargestWidth() {
  double difX = maxX - minX;
  double difY = maxY - minY;
  double difZ = maxZ - minZ;
  if ((difX >= difY) && (difX >= difZ))
    return difX;
  else if ((difY >= difZ) && (difY >= difX))
    return difY;
  else
    return difZ;

}


DisplayGL::DisplayGL(unsigned int sizex_t, unsigned int sizey_t,
		     const std::string & title_t, bool meshes_t, bool smooth_t, bool autoLight_t, bool negative_t) :
  sizex(sizex_t), sizey(sizey_t), title(title_t), meshes(meshes_t),
  smooth(smooth_t), autolight(autoLight_t), negative(negative_t), drawedgesandnormals(false) {

  maxX = -std::numeric_limits<double>::max();
  minX = std::numeric_limits<double>::max();
  maxY = -std::numeric_limits<double>::max();
  minY = std::numeric_limits<double>::max();
  maxZ = -std::numeric_limits<double>::max();
  minZ = std::numeric_limits<double>::max();
}

DisplayGL::~DisplayGL() {}

bool DisplayGL::isAvailable() {
#ifdef USE_GL
  return true;
#else
  return false;
#endif
}


const CImgList<unsigned char> & DisplaySimpleGL::getColors() const {
  return colors;
}

const CImgList<unsigned char> & DisplayDynamicGL::getColors() const {
  return colors;
}


/* DisplayGL methods */
DisplayPointCLoudGL::DisplayPointCLoudGL(unsigned int sizex_t, unsigned int sizey_t,
					 const std::string & title_t, const PointCloud & pCloud, double radius, bool negative_t) : DisplayGL(sizex_t, sizey_t, title_t, false, false, false, negative_t), sphere(Mesh::createSphere(1., 1)), splat_balls(false) {
  double size = radius <= 0 ? pCloud.getSmallestDistance() / 4 : radius;
  for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p)
    addBall(*p, size);

  if (pCloud.hasNormals()) {
    drawedgesandnormals = true;
    PointCloud::const_iterator p = pCloud.begin();
    for(std::vector<Coord3D>::const_iterator n = pCloud.getNormals().begin(); n != pCloud.getNormals().end(); ++n, ++p) {
      const Coord3D rr = (*n) * size * 5;
      edgesAndNormals.push_back(std::pair<Coord3D, Coord3D>(*p - rr, *p + rr));
    }
  }
}

DisplayPointCLoudGL::DisplayPointCLoudGL(unsigned int sizex_t, unsigned int sizey_t,
					 const std::string & title_t, const PointCloudShapeEstimator & pCloudNEstimator, bool negative_t,
					 bool splat_balls_t,
					 const enum SphereColor & cMethod_t, bool useRealSize, bool drawEdges, bool drawOnlyBdEdges, bool drawNormals, double normalRatio,
					 bool use_flags, bool use_threshold, double threshold,
					 double maxSize) : DisplayGL(sizex_t, sizey_t, title_t, false, false, false, negative_t),
							   cMethod(cMethod_t), sphere(Mesh::createSphere(1., 1)), splat_balls(splat_balls_t) {
  const PointCloud & pCloud = pCloudNEstimator.getPointCloud();
  std::vector<Coord3D>::const_iterator normal_t =  pCloudNEstimator.getNormals().begin();
  std::vector<double>::const_iterator radius = pCloudNEstimator.getRadii().begin();
  std::vector<double>::const_iterator localDistortion = pCloudNEstimator.getLocalDistortions().begin();
  std::vector<bool>::const_iterator boundaryFlag = pCloudNEstimator.getBoundaryFlags().begin();

  if ((pCloudNEstimator.getNormals().size() != pCloud.size()) || (pCloudNEstimator.getRadii().size() != pCloud.size()))
    throw Exception("DisplayPointCLoudGL(): wrong PointCloudShapeEstimator data.");

  double r = 0.0;
  if (!useRealSize)
    r = pCloud.getSmallestDistance() / 4;

  if ((cMethod == DisplayPointCLoudGL::SCDistortion) && (pCloudNEstimator.getLocalDistortions().size() == pCloud.size())) {
    const double min = pCloudNEstimator.getMinLocalDistortion();
    const double max = pCloudNEstimator.getMaxLocalDistortion();
    const double th = threshold * pCloud.getSmallestDistance();
    for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p, ++normal_t, ++radius, ++localDistortion)
      if ((use_threshold && (*radius > th)) || (use_flags && ((*p).getFlag() == 0)))
	addBall(*p, r, *normal_t);
      else
	addBall(*p, useRealSize ? (maxSize < *radius ? maxSize : *radius ) : r, *normal_t, *localDistortion, min, max);
  }
  else if ((cMethod == DisplayPointCLoudGL::SCBoundary) && (pCloudNEstimator.getBoundaryFlags().size() == pCloud.size()))
    for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p, ++normal_t, ++radius, ++boundaryFlag)
      addBall(*p, useRealSize ? (maxSize < *radius ? maxSize : *radius ) : r, *normal_t, *boundaryFlag);
  else if ((cMethod == DisplayPointCLoudGL::SCBoundaryDistortion) && (pCloudNEstimator.getLocalBoundaryDistortions().size() == pCloud.size())) {
    const double min = pCloudNEstimator.getMinLocalBoundaryDistortion();
    const double max = pCloudNEstimator.getMaxLocalBoundaryDistortion();
    std::vector<double>::const_iterator localBoundaryDistortion = pCloudNEstimator.getLocalBoundaryDistortions().begin();
    for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p, ++normal_t, ++radius, ++localBoundaryDistortion)
      addBall(*p, useRealSize ? (maxSize < *radius ? maxSize : *radius ) : r, *normal_t, *localBoundaryDistortion, min, max);
  }
  else // if (cMethod == DisplayPointCLoudGL::SCNormal) || (cMethod == DisplayPointCLoudGL::SCNone)
    for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p, ++normal_t, ++radius)
      addBall(*p, useRealSize ? (maxSize < *radius ? maxSize : *radius ) : r, *normal_t);

  drawedgesandnormals = drawNormals || drawEdges;

  if (drawNormals) {
    radius = pCloudNEstimator.getRadii().begin();
    PointCloud::const_iterator p = pCloud.begin();
    for(std::vector<Coord3D>::const_iterator n = pCloudNEstimator.getNormals().begin(); n != pCloudNEstimator.getNormals().end(); ++n, ++radius, ++p) {
      const Coord3D rr = (*n) * ((useRealSize ? (maxSize < *radius ? maxSize : *radius ) : r) * normalRatio);
      edgesAndNormals.push_back(std::pair<Coord3D, Coord3D>(*p - rr, *p + rr));
    }
  }
  if (drawEdges || drawOnlyBdEdges) {
    PointCloud::const_iterator p = pCloud.begin();
    std::vector<bool>::const_iterator bdP = pCloudNEstimator.getBoundaryFlags().begin();
    for(std::vector<std::map<VertexID, unsigned int> >::const_iterator nbs = pCloudNEstimator.getNeighbours().begin(); nbs != pCloudNEstimator.getNeighbours().end(); ++nbs, ++p, ++bdP)
      if ((!drawOnlyBdEdges) || (*bdP))
	for(std::map<VertexID, unsigned int>::const_iterator nb = (*nbs).begin(); nb != (*nbs).end(); ++nb)
	  if ((!drawOnlyBdEdges) || (pCloudNEstimator.getBoundaryNeighbours()[(*p).getId()].first == (*nb).first) || (pCloudNEstimator.getBoundaryNeighbours()[(*p).getId()].second == (*nb).first))
	    edgesAndNormals.push_back(std::pair<Coord3D, Coord3D>(*p, *p +  (pCloud[(*nb).first] - *p) * 0.48));
  }
}

const std::deque<Coord3D> & DisplayPointCLoudGL::getPoints() const {
  return points;
}

const CImgList<VertexID> & DisplayPointCLoudGL::getTriangles() const {
  return triangles;
}

const CImgList<unsigned char> & DisplayPointCLoudGL::getColors() const {
  return colors;
}



void DisplayPointCLoudGL::addBall(const Coord3D & center, double radius, const Coord3D & normal_t, bool boundary) {
  CImg<unsigned char> color(3);
  if (boundary) {
    color(0) = 255;
    color(1) = color(2) = 0;
  }
  else
    color.fill(255);

  addBall(center, radius, normal_t, color);
}


void DisplayPointCLoudGL::addBall(const Coord3D & center, double radius, const Coord3D & normal_t) {
  assert(splat_balls || (normal_t.norm() <= 0.0000001));
  CImg<unsigned char> color(3);
  if (cMethod == DisplayPointCLoudGL::SCNormal) {
    color(0) = (normal_t.getX() + 1.) * 128;
    color(1) = (normal_t.getY() + 1.) * 128;
    color(2) = (normal_t.getZ() + 1.) * 128;
  }
  else
    color.fill(128);

  addBall(center, radius, normal_t, color);
}

void DisplayPointCLoudGL::addBall(const Coord3D & center, double radius, const Coord3D & normal_t, double localDistortion, double minDist, double maxDist) {
  CImg<unsigned char> color(3);

  if (maxDist == minDist) {
    color.fill(negative ? 0 : 255);
  }
  else if (localDistortion < minDist) {
    color.fill(- 32 * localDistortion);
  }
  else {
    if (negative) {
      color(0) = ((localDistortion - minDist) / (maxDist - minDist)) * 255;
      color(1) = 0;
      color(2) = 255 - ((localDistortion - minDist) / (maxDist - minDist)) * 255;
    }
    else {
      color(0) = 255;
      color(1) = color(2) = 255 - ((localDistortion - minDist) / (maxDist - minDist)) * 255;
    }
  }
  addBall(center, radius, normal_t, color);
}

void DisplayPointCLoudGL::addBall(const Coord3D & center, double radius, const Coord3D & normal_t, const CImg<unsigned char> & color) {
  if (splat_balls)
    addSplatBall(center, radius, normal_t, color);
  else {
    TriangleID offsetTriangle = points.size();
    for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p) {
      points.push_back((*p) * radius + center);
      if (points.back().getX() > maxX) maxX = points.back().getX();
      if (points.back().getX() < minX) minX = points.back().getX();
      if (points.back().getY() > maxY) maxY = points.back().getY();
      if (points.back().getY() < minY) minY = points.back().getY();
      if (points.back().getZ() > maxZ) maxZ = points.back().getZ();
      if (points.back().getZ() < minZ) minZ = points.back().getZ();
    }

    for(Mesh::const_triangle_iterator tt = sphere.triangle_begin(); tt != sphere.triangle_end(); ++tt) {
      CImg<VertexID> t(3);
      t(0) = offsetTriangle + (*tt).getP1();
      t(1) = offsetTriangle + (*tt).getP2();
      t(2) = offsetTriangle + (*tt).getP3();
      triangles.push_back(t);
      colors.push_back(color);
    }
  }
}

void DisplayPointCLoudGL::addSplatBall(const Coord3D & center, double radius, const Coord3D & normal_t, const CImg<unsigned char> & color) {
  const double resize = 0.6;
  const Point3D zero(0., 0., 0.);
  TriangleID offsetTriangle = points.size();
  const Plane3D plane(zero, normal_t);
  for(Mesh::const_point_iterator p = sphere.point_begin(); p != sphere.point_end(); ++p) {
    char dir = Coord3D::scalarProduct(*p, normal_t) >= 0 ? -1 : 1;
    double distance = plane.distance(*p) * radius;
    Coord3D newP = *p * radius + normal_t * (dir * distance * resize);
    points.push_back(newP + center);
    if (points.back().getX() > maxX) maxX = points.back().getX();
    if (points.back().getX() < minX) minX = points.back().getX();
    if (points.back().getY() > maxY) maxY = points.back().getY();
    if (points.back().getY() < minY) minY = points.back().getY();
    if (points.back().getZ() > maxZ) maxZ = points.back().getZ();
    if (points.back().getZ() < minZ) minZ = points.back().getZ();
  }

  for(Mesh::const_triangle_iterator tt = sphere.triangle_begin(); tt != sphere.triangle_end(); ++tt) {
    CImg<VertexID> t(3);
    t(0) = offsetTriangle + (*tt).getP1();
    t(1) = offsetTriangle + (*tt).getP2();
    t(2) = offsetTriangle + (*tt).getP3();
    triangles.push_back(t);
    colors.push_back(color);
  }
}

