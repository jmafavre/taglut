/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DDBOX
#define DDBOX


#include <list>

namespace Taglut {
  /**
     @class DDBox
     Box location in DDisplay
  */
  class DDBox {
  protected:

    /** X coord of the first point */
    int x1;
    /** Y coord of the first point */
    int y1;
    /** X coord of the second point */
    int x2;
    /** Y coord of the second point */
    int y2;
    /** X size of the box */
    int sizex;
    /** Y size of the box */
    int sizey;
    /** Box id (0 if not active) */
    unsigned int id;
    /** id of the lastest active box */
    static unsigned int lastId;

  public:

    /**
       Default constructor
    */
    DDBox(int x1, int y1, int sizex, int sizey, bool active = true);

    /**
       Return true if (x, y) is inside ddbox
    */
    bool isInside(int x, int y) const;

    /**
       Convert local location to absolute location
    */
    std::pair<int, int> local2Absolute(int x, int y) const;

    /**
       Convert absolute location to local location
    */
    std::pair<int, int> absolute2local(int x, int y) const;

    /** x1 modifier */
    void setX1(int x);

    /** x2 modifier */
    void setX2(int x);

    /** x1 accessor */
    int getX1() const;

    /** y1 accessor */
    int getY1() const;

    /** x2 accessor */
    int getX2() const;

    /** y1 accessor */
    int getY2() const;

    /** sizex accessor */
    int getSizeX() const;

    /** sizey accessor */
    int getSizeY() const;

    /** id accessor */
    unsigned int getId() const;
  };
}

#endif
