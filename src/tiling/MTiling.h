/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne Clermont1
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef M_TILING
#define M_TILING

#include<vector>

#include "Mesh.h"
#include "MeshList.h"
#include "MeshPart.h"
#include "Mapping2D3D.h"
#include "IDTypes.h"
#ifndef SWIG
#include "GoTools/creators/ApproxSurf.h"
#include "GoTools/creators/ApproxCurve.h"
#endif

namespace Taglut {

  /**
     An MTile is a structure to describe a connected component in a mesh resulting from
     a cutting. Specific properties as multipoints are contained in this structure.
   */
  class MTile {
  private:
    /** list of multipoint on the boundary, i.e. points
	which location is the same as points in other MTiles (or in another
	part of the current tile) */
    std::vector<VertexID> multipoints;
    /** id of the current MTile */
    unsigned int id;
    /** the neighbours of the current tile */
    std::vector<unsigned int> neighbours;
    /** a vertex of the mesh contained in this tile */
    VertexID v;
    /** the manipulated mesh */
    Mesh * mesh;
    /** genus of the tile */
    unsigned int genus;
    /** number of boundaries of the tile */
    unsigned int nbBoundaries;

  public:
    /** default constructor
	@param m The MeshPart we are focussing on
	@param i ID of the current MTile in the MTiling */
    MTile(MeshPart & m, unsigned int i);

    /** copy constructor */
    MTile(const MTile & mt);

#ifndef SWIG
    /** copy operator */
    MTile & operator=(MTile & mt);
#endif

    /** add a multipoint on the boundary */
    void addMultiPoint(VertexID multipoint);

    /** add a neighbour tile */
    void addNeighbour(const MTile & mtile);

    /** accessor */
    inline VertexID getInsidePoint() const { return v; }

    /** accessor */
    inline unsigned int getId() const { return id; }

    /** accessor */
    inline unsigned int getNbMultiPoints() const { return multipoints.size(); }

    /** accessor */
    inline VertexID getMultiPoint(unsigned int id) const {
      assert(id < multipoints.size());
      return multipoints[id];
    }

    /** accessor */
    inline const std::vector<unsigned int> & getNeighbours() const { return neighbours; }

    /** return true if the current tile have the given one as neighbour */
    inline bool hasNeighbour(unsigned int idnb) const {
      for(std::vector<unsigned int>::const_iterator nb = neighbours.begin();
	  nb != neighbours.end(); ++nb)
	if (idnb == *nb)
	  return true;
      return false;
    }

    /** return true if the current tile have the given one as neighbour */
    inline bool hasNeighbour(const MTile & mt) const {
      return hasNeighbour(mt.getId());
    }

    /** return true if the current tile have the given multipoint */
    inline bool hasMultiPoint(VertexID p) const {
      for(std::vector<VertexID>::const_iterator mp = multipoints.begin();
	  mp != multipoints.end(); ++mp)
	if (p == *mp)
	  return true;
      return false;
    }

    /** accessor */
    inline unsigned int getGenus() const { return genus; }

    /** accessor */
    inline unsigned int getNbBoundaries() const { return nbBoundaries; }

    /** accessor */
    inline bool isQuad() const {
      return (getGenus() == 0) && (getNbBoundaries() == 1) && (getNbMultiPoints() == 4);
    }

    /** accessor */
    inline const std::vector<VertexID> & getMultiPoints() const { return multipoints; }

    /** return the multipoints of the current MTile located at the given location \p coord */
    std::vector<VertexID> getMultiPointsSameLocation(const Coord3D & coord, const Mesh & mesh) const;

    /** assuming that each connection has been added 2 time, it removes the twins */
    void removeTwiceConnections();
  };


  /** this structure is able to manipulate meshes separated by connected components, produced by cuttings:
      some vertices are in the same location... */
  class MTiling {
  private:
    /** description of the tiles */
    std::vector<MTile> mtiles;
    /** the manipulated mesh */
    Mesh & mesh;
    /** id of the CC for each of the points */
    std::vector<unsigned int> ccIds;
    /** the produced mapping */
    Mapping2D3D * mapping;
    /** a list of boolean: if true, the corresponding tile has been parameterized */
    std::vector<bool> parameterized;

#ifndef SWIG
    /** the reconstructed splines */
    std::vector<boost::shared_ptr<Go::SplineSurface> > splines;
#endif
    /** a size for each quadrangle (estimation) */
    std::vector<std::pair<double, double> > sizes;

    /** parameters of the fitting step */
    unsigned int nb_points_fitting;
    unsigned int degree_fitting;
    unsigned int nbiter_fitting;
    unsigned int nbiter_bd_fitting;

    void setNeighbourConnections(const std::vector<VertexID> & mPoints);

    inline void addConnection(unsigned int id1, unsigned int id2) {
      assert(id1 < mtiles.size());
      assert(id2 < mtiles.size());
      mtiles[id1].addNeighbour(mtiles[id2]);
      mtiles[id2].addNeighbour(mtiles[id1]);
    }

    /** return the number of neighbour tiles of the given one with the
	required topology.
	@param idTile ID of the given tile
	@param g genus of the wanted neighbours
	@param nbb number of boundaries of the wanted neighbours
	@param nbMP number of wanted multipoints on the neighbour
	@return ID list of the corresponding tiles
    */
    std::vector<unsigned int> getNeighbours(unsigned int idTile,
					    unsigned int g = 0,
					    unsigned int nbb = 1,
					    unsigned int nbMP = 2) const;


    /** assuming that the two tiles are adjacent discs with only two multipoints,
	this function adds two multipoints points on the shared boundary to create quadrangles */
    void adjustQuads(MTile & mt1, MTile & mt2);

    /** assuming that the two tiles are adjacent discs with only two multipoints,
	this function adds two multipoints points on the shared boundary to create quadrangles,
	using the scalar function (\see adjustQuads) */
    void adjustQuads(MTile & mt1, MTile & mt2, const MeshMap & mmap);

    /** clear the internal structure */
    void clear(const std::vector<unsigned int> & idtiles = std::vector<unsigned int>());

#ifndef SWIG
    /** get a spline that fits the given tile, assuming that it is a quadrangle. \p nb_points corresponds to the
	number of control points in each curve, \p degree is the degree of the polynoms.
	if \p nb_points = 0, the number of control points
	is defined by 1/6 of the number of points in the curve.
	sx and sy are results, corresponding to the size of the quad. */
    boost::shared_ptr<Go::SplineSurface> computeFitting(const MTile & mtile,
							double & sx, double & sy,
							unsigned int degree,
							unsigned int nbIter,
							unsigned int nbIterBd,
							unsigned int nb_points) const;

    /** get splines that fit the border of the given tile, assuming that it has only one border. \p nb_points
	corresponds to the number of control points in the curve, \p degree is the degree of the polynoms.
	if \p nb_points = 0, the number of control points
	is defined by 1/6 of the number of points in the curve. */
    std::vector<boost::shared_ptr<Go::SplineCurve> > computeBdFitting(const MTile & mtile,
								      unsigned int degree,
								      unsigned int nbIterBd,
								      unsigned int nb_points) const;
#endif

    /** return a list of points corresponding to the border of the given mtile (stop corresponds to multipoints),
	assuming that it have only one border. */
    std::vector<std::vector<VertexID> > getBoundaries(const MTile & mtile) const;

    /** return the first corner of the quadrangle, described by the ID of the corner, and the first boundary vertex
	in the U direction (using the previously mapping */
    std::pair<VertexID, VertexID> getFirstCorner(const std::vector<VertexID> & multipoints) const;

    bool writeApproxSurf(std::ostream& os, const Go::SplineSurface & spline);

    /** remesh a given tile */
    static Mesh remeshQuadByBSplines(const Go::SplineSurface & spline, double sx, double sy, double distance);

    bool isComputedParam(const std::vector<unsigned int> & idtiles) const;
  public:
    /** constructor using a mesh */
    MTiling(Mesh & m);

    MTiling(const MTiling & mtiling) : mtiles(mtiling.mtiles), mesh(mtiling.mesh), mapping(NULL) { }

    ~MTiling() {
      if (mapping != NULL)
	delete mapping;
    }

    /** accessor */
    inline const std::vector<MTile> & getTiles() const {
      return mtiles;
    }

    /** accessor */
    inline unsigned int getNbTiles() const {
      return mtiles.size();
    }

    /** accessor */
    inline const MTile & getTile(unsigned int id) const {
      if (id > mtiles.size()) throw Exception("getTile(1): bad id value");
      else return mtiles[id];
    }

    /** assuming that all the tiles are 0-genus, and all the tiles are quad, except adjacent pairs of 2-side tiles,
	it add multipoints in the common boundary of each pair to obtain quadrangles.
	New locations: 2/7 and  5/7 of the original path. */
    void adjustQuads();

    /** assuming that all the tiles are 0-genus, and all the tiles are quad, except adjacent pairs of 2-side tiles,
	it add multipoints in the common boundary of each pair to obtain quadrangles. This method exploits the scalar
	function to adjust the multipoints' location, assuming that an extremum is contained in the path. the multipoints
	are located using the mean of the scalar value of the extremum and the corresponding multipoint. */
    void adjustQuads(const MeshMap & mmap);

    /** return true if the current tiling is a quad tiling */
    bool isQuadTiling() const;

    /** compute a 2D mapping of each tile, using the following rule:
	if the tile is a quadrangle, the unfolding is produced in a rectangle, using a Floater
	parameterization. Otherwise, a Floater parameterization with a circle boundary is applied. */
    const Mapping2D3D & parameterize(const std::vector<unsigned int> & idtiles = std::vector<unsigned int>(), bool quad = false);

    /** compute a 2D mapping of each tile, using the following rule:
	if the tile is a quadrangle, the unfolding is produced in a rectangle, using a Floater
	parameterization. Otherwise, a Floater parameterization with a circle boundary is applied. */
    inline const Mapping2D3D & parameterize(const MeshMap& mmap, const std::vector<unsigned int> & idtiles = std::vector<unsigned int>()) {
      adjustQuads(mmap);
      return parameterize(idtiles);
    }

    /** return the computed mapping if it has been computed previously, or compute it. */
    const Mapping2D3D & getParameterization(const std::vector<unsigned int> & idtiles = std::vector<unsigned int>(), bool quad = false) {
      if (isComputedParam(idtiles)) {
	if (quad != isQuadTiling())
	  return parameterize(idtiles, quad);
	else
	  return *mapping;
      }
      else
	return parameterize(idtiles, quad);
    }

    /** return the computed mapping if it has been computed previously, or compute it. */
    const Mapping2D3D & getParameterization(const MeshMap & mmap, const std::vector<unsigned int> & idtiles = std::vector<unsigned int>()) {
      if (mapping != NULL) {
	if (!isQuadTiling())
	  return parameterize(mmap, idtiles);
	else
	  return *mapping;
      }
      else
	return parameterize(mmap, idtiles);
    }

#ifndef SWIG
    /** describe each tile by using an approximated surface (B-splines) on each tile. Apply first \p adjustQuads()
	and \p parameterize() to have good topology and a parameterization to fit with.
	\p nbIter corresponds to the number of iterations
	if \p nb_points = 0, the number of control points
	is defined by 1/6 of the number of points in the curve*/
    const std::vector<boost::shared_ptr<Go::SplineSurface> > & computeFitting(unsigned int degree = 4,
									      unsigned int nbIter = 3,
									      unsigned int nbIterBd = 3,
									      unsigned int nb_points = 0);

    /** return the computed fitting if it has been computed previously, or compute it.
	if \p nb_points = 0, the number of control points
	is defined by 1/6 of the number of points in the curve */
    const std::vector<boost::shared_ptr<Go::SplineSurface> > & getBSplines(unsigned int degree = 4,
									   unsigned int nbIter = 3,
									   unsigned int nbIterBd = 3,
									   unsigned int nb_points = 0) {
      if ((splines.size() != mtiles.size()) || (nb_points != nb_points_fitting) ||
	  (degree != degree_fitting) || (nbIter != nbiter_fitting) || (nbIterBd != nbiter_bd_fitting))
	return computeFitting(degree, nbIter, nbIterBd, nb_points);
      else
	return splines;
    }
#endif

    /** remesh the patches using the bspline.
	@param distance Distance between points in the resulting mesh (approximation)
	@param degree \see computeFitting
	@param nb_points \see computeFitting
	@return The generated mesh
    */
    inline MeshList remeshQuadsByBSplines(double distance,
					  unsigned int degree = 4,
					  unsigned int nbIter = 3,
					  unsigned int nbIterBd = 3,
					  unsigned int nb_points = 0) {
      getBSplines(degree, nbIter, nbIterBd, nb_points);
      MeshList result;
      std::vector<std::pair<double, double> >::const_iterator si = sizes.begin();
      for(std::vector<boost::shared_ptr<Go::SplineSurface> >::const_iterator s = splines.begin();
	  s != splines.end(); ++s, ++si)
	result.push_back(remeshQuadByBSplines(**s, (*si).first, (*si).second, distance));
      return result;
    }

    /**
       Remesh the patches (\see remeshQuadsByBSplines) and merge the result
     */
    inline Mesh remeshByBSplines(double distance,
				 unsigned int degree = 4,
				 unsigned int nbIter = 3,
				 unsigned int nbIterBd = 3,
				 unsigned int nb_points = 0) {
      MeshList mList = remeshQuadsByBSplines(distance, degree, nbIter, nbIterBd, nb_points);
      return mList.getMesh();
    }

    /** save the approximated surface (B-Splines) in a obj file. if \p nb_points = 0, the number of control points
     is defined by 1/6 of the number of points in the curve */
    bool saveApproxSurf(const std::string & filename,
			unsigned int degree = 4,
			unsigned int nbIter = 3,
			unsigned int nbIterBd = 3,
			unsigned int nb_points = 0);

    /** given two tiles, it return two paths that corresponds to a common border.
	the first element of a pair is a multipoint of the first tile. */
    std::pair<std::vector<VertexID>, std::vector<VertexID> > getCommonBorder(const MTile & mt1, const MTile & mt2) const;

    /** given two tiles (described by id), it return two paths that corresponds to a common border */
    inline std::pair<std::vector<VertexID>, std::vector<VertexID> > getCommonBorder(unsigned int id1, unsigned int id2) const {
      return getCommonBorder(getTile(id1), getTile(id2));
    }

    /** given two tiles, it return a list of two paths that corresponds to the common borders */
    std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > getCommonBorders(const MTile & mt1, const MTile & mt2) const;

    /** given two tiles (described by id), it return a list of two paths that corresponds to the common borders */
    inline std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > getCommonBorders(unsigned int id1, unsigned int id2) const {
      return getCommonBorders(getTile(id1), getTile(id2));
    }

    /** return pair of quad tiles that describe a cylinder */
    std::vector<std::pair<unsigned int, unsigned int> > getCylindersByQuad() const;

    /** assuming that \p mp is a multipoint, and \p next an adjacent and
	boundary vertex, it computes the corresponding border */
    std::vector<VertexID> getBorder(VertexID mp, VertexID next) const;

    /** get the tile corresponding to the given multipoint */
    const MTile & getTileFromMultipoint(VertexID mp) const;

    /**
       return a meshmap that gives for each vertex the corresponding tile id
     */
    MeshMapUInt getIdTiles() const;
  };

}

#endif
