/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne Clermont1
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include<iomanip>
#include<algorithm>
#include "MTiling.h"
#include "Point3D.h"
#include "Triangle.h"
#include "BarycentricMapping.h"

#include "GoTools/creators/SmoothSurf.h"
#include "GoTools/geometry/CurveLoop.h"
#include "GoTools/creators/CoonsPatchGen.h"

using namespace Taglut;


MTile::MTile(MeshPart & mPart, unsigned int i) : id(i),
						 v(mPart.getInsidePoint()),
						 mesh(&(mPart.getMesh())),
						 genus(mPart.getGenus()),
						 nbBoundaries(mPart.getNbBoundaries()) {
}

MTile::MTile(const MTile & mt) : multipoints(mt.multipoints), id(mt.id),
				 neighbours(mt.neighbours), v(mt.v),
				 mesh(mt.mesh), genus(mt.genus), nbBoundaries(mt.nbBoundaries) {
}

MTile & MTile::operator=(MTile & mt) {
  multipoints = mt.multipoints;
  id = mt.id;
  neighbours = mt.neighbours;
  v = mt.v;
  mesh = mt.mesh;
  genus = mt.genus;
  nbBoundaries = mt.nbBoundaries;
  return *this;
}

void MTile::addMultiPoint(VertexID multipoint) {
  if(find(multipoints.begin(), multipoints.end(), multipoint) == multipoints.end())
    multipoints.push_back(multipoint);
}

void MTile::addNeighbour(const MTile & mtile) {
  neighbours.push_back(mtile.getId());
}


std::vector<VertexID> MTile::getMultiPointsSameLocation(const Coord3D & coord, const Mesh & mesh) const {
  std::vector<VertexID> result;
  for(std::vector<VertexID>::const_iterator m = multipoints.begin(); m != multipoints.end(); ++m)
    if (coord == mesh.point(*m))
      result.push_back(*m);

  return result;
}

void MTile::removeTwiceConnections() {
  std::vector<unsigned int> result;
  std::map<unsigned int, unsigned int> nbC;
  for(std::vector<unsigned int>::const_iterator n = neighbours.begin(); n != neighbours.end(); ++n)
    nbC[*n] += 1;

  for(std::map<unsigned int, unsigned int>::iterator nn = nbC.begin(); nn != nbC.end(); ++nn) {
    assert(!(((*nn).second) & 1)); // even number of similar connections
    result.insert(result.end(), (*nn).second / 2, (*nn).first);
  }

  neighbours = result;
}

MTiling::MTiling(Mesh & m) : mesh(m), mapping(NULL) {
  std::deque<MeshPart> cc = m.getConnectedComponents();
  unsigned int i = 0;
  // build the connected components
  for(std::deque<MeshPart>::iterator c = cc.begin(); c != cc.end(); ++c, ++i)
    mtiles.push_back(MTile(*c, i));

  // set the flags according to the CC
  mesh.setPointFlag(-1);
  MeshManipulator mManip(mesh);
  for(std::vector<MTile>::const_iterator t = mtiles.begin(); t != mtiles.end(); ++t) {
    assert(mesh.point((*t).getInsidePoint()).getFlag() == -1);
    mManip.computeCCUsingPointsSimple((*t).getInsidePoint(), -1, (*t).getId());
  }
  // save the flags in a dedicated structure
  ccIds.reserve(mesh.getNbPoints());
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p) {
    assert((*p).getFlag() >= 0);
    assert((*p).getFlag() < (int)mtiles.size());
    ccIds.push_back((unsigned int)(*p).getFlag());
  }

  // compute multipoints
  std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord> mPoints = mesh.computeMultiPointsByCoords();

  // add multipoints and neighbours to the tiles
  for(std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord>::const_iterator mpl = mPoints.begin(); mpl != mPoints.end(); ++mpl) {
    for(std::vector<VertexID>::const_iterator mp = (*mpl).second.begin(); mp != (*mpl).second.end(); ++mp) {
      assert(mtiles[mesh.point(*mp).getFlag()].getId() == (unsigned int)(mesh.point(*mp).getFlag()));
      mtiles[mesh.point(*mp).getFlag()].addMultiPoint(*mp);
    }
    setNeighbourConnections((*mpl).second);
  }

  // remove twin connections (each connection has been added twice because of the structure)
  for(std::vector<MTile>::iterator t = mtiles.begin(); t != mtiles.end(); ++t)
    (*t).removeTwiceConnections();
}

void MTiling::setNeighbourConnections(const std::vector<VertexID> & mPoints) {
  // add connections using multipoints
  for(std::vector<VertexID>::const_iterator p = mPoints.begin(); p != mPoints.end(); ++p) {
    unsigned int pFlag = mesh.point(*p).getFlag();
    VertexID nbB[2];
    unsigned int n = 0;
    // first get the two neighbours
    for(std::deque<VertexID>::const_iterator nbP = mesh.point(*p).getNeighbours().begin();
	nbP != mesh.point(*p).getNeighbours().end(); ++nbP)
      if (mesh.isBoundaryEdge(*nbP, *p)) {
	if (n == 2)
	  throw Exception("Bad boundary structure");
	nbB[n] = *nbP;
	++n;
      }

    // then check if the other multipoints have same location
    for(std::vector<VertexID>::const_iterator pp = p + 1; pp != mPoints.end(); ++pp) {
      for(std::deque<VertexID>::const_iterator nbPP = mesh.point(*pp).getNeighbours().begin();
	  nbPP != mesh.point(*pp).getNeighbours().end(); ++nbPP) {
	const Point3D & nPPC = mesh.point(*nbPP);
	if(nPPC.getIsBoundary() && (mesh.point(nbB[0]).sameLocation(nPPC) ||
				    mesh.point(nbB[1]).sameLocation(nPPC))) {
	  addConnection(mesh.point(*pp).getFlag(), pFlag);
	  break;
	}
      }
    }
  }
}


std::vector<unsigned int> MTiling::getNeighbours(unsigned int idTile,
						 unsigned int g,
						 unsigned int nbb,
						 unsigned int nbMP) const {
  std::vector<unsigned int> result;
  const std::vector<unsigned int> & neighbours = mtiles[idTile].getNeighbours();

  for(std::vector<unsigned int>::const_iterator n = neighbours.begin(); n != neighbours.end(); ++n) {
    const MTile & mt = mtiles[*n];
    if ((mt.getGenus() == g) && (mt.getNbBoundaries() == nbb) && (mt.getNbMultiPoints() == nbMP))
      result.push_back(*n);
  }
  return result;
}



void MTiling::adjustQuads(MTile & mt1, MTile & mt2, const MeshMap & mmap) {
  assert(mt1.hasNeighbour(mt2));
  assert((mt1.getGenus() == 0) && (mt1.getNbBoundaries() == 1) && (mt1.getNbMultiPoints() == 2));
  assert((mt2.getGenus() == 0) && (mt2.getNbBoundaries() == 1) && (mt2.getNbMultiPoints() == 2));

  std::pair<std::vector<VertexID>, std::vector<VertexID> > bd = getCommonBorder(mt1, mt2);
  assert(bd.first.size() != 0);
  assert(bd.first.size() == bd.second.size());

  assert(mesh.point(bd.second.front()).distance(mesh.point(bd.first.front())) < 1e-5);
  assert(fabs(mmap[bd.second.front()] - mmap[bd.first.front()]) < 1e-5);

  assert(mesh.point(bd.second.back()).distance(mesh.point(bd.first.back())) < 1e-5);
  assert(fabs(mmap[bd.second.back()] - mmap[bd.first.back()]) < 1e-5);

  double sfc1 = mmap[bd.first.front()];
  double sfc2 = mmap[bd.first.back()];
  const double extr = mmap.getExtremum(bd.first);
  sfc1 = (sfc1 + extr) / 2;
  sfc2 = (sfc2 + extr) / 2;

  mt1.addMultiPoint(mmap.getPointOnEdgeFromValueAndPath(bd.first, sfc1).getClosestVertex());
  std::reverse(bd.first.begin(), bd.first.end());
  mt1.addMultiPoint(mmap.getPointOnEdgeFromValueAndPath(bd.first, sfc2).getClosestVertex());

  mt2.addMultiPoint(mmap.getPointOnEdgeFromValueAndPath(bd.second, sfc1).getClosestVertex());
  std::reverse(bd.second.begin(), bd.second.end());
  mt2.addMultiPoint(mmap.getPointOnEdgeFromValueAndPath(bd.second, sfc2).getClosestVertex());

  assert(mt1.getNbMultiPoints() == 4);
  assert(mt2.getNbMultiPoints() == 4);
}

void MTiling::adjustQuads(MTile & mt1, MTile & mt2) {
  assert(mt1.hasNeighbour(mt2));
  assert((mt1.getGenus() == 0) && (mt1.getNbBoundaries() == 1) && (mt1.getNbMultiPoints() == 2));
  assert((mt2.getGenus() == 0) && (mt2.getNbBoundaries() == 1) && (mt2.getNbMultiPoints() == 2));

  std::pair<std::vector<VertexID>, std::vector<VertexID> > bd = getCommonBorder(mt1, mt2);
  assert(bd.first.size() != 0);
  assert(bd.first.size() == bd.second.size());
  double l = mesh.getLength(bd.first);
  assert(l != 0.);
  assert(l == mesh.getLength(bd.second));

  double l1 = l * 2. / 7.;
  double l2 = l * 5. / 7.;

  std::pair<VertexID, VertexID> pts1 = mesh.getClosestVertices(bd.first, l1, l2);
  std::pair<VertexID, VertexID> pts2 = mesh.getClosestVertices(bd.second, l1, l2);
  mt1.addMultiPoint(pts1.first);
  mt1.addMultiPoint(pts1.second);
  mt2.addMultiPoint(pts2.first);
  mt2.addMultiPoint(pts2.second);
  assert(mt1.getNbMultiPoints() == 4);
  assert(mt2.getNbMultiPoints() == 4);
}

std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > MTiling::getCommonBorders(const MTile & mt1, const MTile & mt2) const {
  std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > result;

  if (!mt1.hasNeighbour(mt2))
    return result;

  for(std::vector<VertexID>::const_iterator mmt1 = mt1.getMultiPoints().begin();
      mmt1 != mt1.getMultiPoints().end(); ++mmt1) {
    std::vector<VertexID> mpts2 = mt2.getMultiPointsSameLocation(mesh.point(*mmt1), mesh);
    if (mpts2.size() != 0) {
      bool seen = false;
      for(std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > >::const_iterator r = result.begin();
	  r != result.end(); ++r) {
	if ((*mmt1) == (*r).first.back()) { // this point has been seen before in another path
	  seen = true;
	  break;
	}
      }

      if (!seen) {
	for(std::vector<VertexID>::const_iterator mmt2 = mpts2.begin(); mmt2 != mpts2.end(); ++mmt2) {
	  VertexID nextmmt1 = mesh.getNextBPoint(*mmt1);
	  VertexID nextmmt2 = mesh.getNextBPoint(*mmt2);
	  if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
	    nextmmt2 = mesh.getNextBPoint(*mmt2, nextmmt2);
	    if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
	      nextmmt1 = mesh.getNextBPoint(*mmt1, nextmmt1);
	      if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
		nextmmt2 = mesh.getNextBPoint(*mmt2, nextmmt2);
		if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
		  continue;
		}
	      }
	    }
	  }
	  assert(mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2)));
	  result.push_back(std::pair<std::vector<VertexID>, std::vector<VertexID> >());
	  result.back().first.push_back(*mmt1);
	  result.back().second.push_back(*mmt2);
	  result.back().first.push_back(nextmmt1);
	  result.back().second.push_back(nextmmt2);
	  while(!mt1.hasMultiPoint(result.back().first.back())) {
	    result.back().first.push_back(mesh.getNextBPoint(result.back().first.back(), result.back().first[result.back().first.size() - 2]));
	    result.back().second.push_back(mesh.getNextBPoint(result.back().second.back(), result.back().second[result.back().second.size() - 2]));
	    assert(result.back().first.size() == result.back().second.size());
	    assert(mesh.point(result.back().first.back()).sameLocation(mesh.point(result.back().second.back())));
	  }
	}
      }
    }
  }

  return result;
}


std::pair<std::vector<VertexID>, std::vector<VertexID> > MTiling::getCommonBorder(const MTile & mt1, const MTile & mt2) const {
  std::pair<std::vector<VertexID>, std::vector<VertexID> > result;

  if (!mt1.hasNeighbour(mt2))
    return result;

  for(std::vector<VertexID>::const_iterator mmt1 = mt1.getMultiPoints().begin();
      mmt1 != mt1.getMultiPoints().end(); ++mmt1) {
    std::vector<VertexID> mpts2 = mt2.getMultiPointsSameLocation(mesh.point(*mmt1), mesh);
    if (mpts2.size() != 0) {
      for(std::vector<VertexID>::const_iterator mmt2 = mpts2.begin(); mmt2 != mpts2.end(); ++mmt2) {
	VertexID nextmmt1 = mesh.getNextBPoint(*mmt1);
	VertexID nextmmt2 = mesh.getNextBPoint(*mmt2);
	if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
	  nextmmt2 = mesh.getNextBPoint(*mmt2, nextmmt2);
	  if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
	    nextmmt1 = mesh.getNextBPoint(*mmt1, nextmmt1);
	    if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
	      nextmmt2 = mesh.getNextBPoint(*mmt2, nextmmt2);
	      if (!mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2))) {
		continue;
	      }
	    }
	  }
	}
	assert(mesh.point(nextmmt1).sameLocation(mesh.point(nextmmt2)));
	result.first.push_back(*mmt1);
	result.second.push_back(*mmt2);
	result.first.push_back(nextmmt1);
	result.second.push_back(nextmmt2);
	while(!mt1.hasMultiPoint(result.first.back())) {
	  result.first.push_back(mesh.getNextBPoint(result.first.back(), result.first[result.first.size() - 2]));
	  result.second.push_back(mesh.getNextBPoint(result.second.back(), result.second[result.second.size() - 2]));
	  assert(result.first.size() == result.second.size());
	  assert(mesh.point(result.first.back()).sameLocation(mesh.point(result.second.back())));
	}
	return result;
      }
    }
  }

  return result;
}


const MTile & MTiling::getTileFromMultipoint(VertexID mp) const {
  for(std::vector<MTile>::const_iterator tile = mtiles.begin(); tile != mtiles.end(); ++tile)
    if ((*tile).hasMultiPoint(mp))
      return *tile;
  throw Exception("getTileFromMultipoint(1): cannot found tile from multipoint");
}

std::vector<VertexID> MTiling::getBorder(VertexID mp, VertexID next) const {
  std::vector<VertexID> result;

  if (!mesh.point(mp).getIsBoundary() ||
      !mesh.point(next).getIsBoundary() ||
      !mesh.point(mp).hasNeighbour(next))
    return result;

  try {
    const MTile & tile = getTileFromMultipoint(mp);
    result.push_back(mp);
    result.push_back(next);
    while(!tile.hasMultiPoint(result.back())) {
      result.push_back(mesh.getNextBPoint(result.back(), result[result.size() - 2]));
    }
  }
  catch (...) {
  }
  return result;
}

bool MTiling::isQuadTiling() const {

  for(std::vector<MTile>::const_iterator t = mtiles.begin(); t != mtiles.end(); ++t)
    if (!(*t).isQuad())
      return false;
  return true;
}

void MTiling::adjustQuads(const MeshMap & mmap) {
  std::vector<bool> checked(mtiles.size(), false);


  for(std::vector<MTile>::iterator t = mtiles.begin(); t != mtiles.end(); ++t)
    if ((!checked[(*t).getId()]) && ((*t).getGenus() == 0) && ((*t).getNbBoundaries() == 1) && ((*t).getNbMultiPoints() == 2)) {
      checked[(*t).getId()] = true;
      std::vector<unsigned int> neighbours = getNeighbours((*t).getId(), 0, 1, 2);
      if (neighbours.size() != 1)
	throw Exception("adjustQuads(): configuration not handled. Abort.");
      if (getNeighbours(neighbours.front(), 0, 1, 2).size() != 1)
	throw Exception("adjustQuads(): configuration not handled. Abort.");
      adjustQuads(*t, mtiles[neighbours.front()], mmap);
      clear(); /* the mapping is now not valid */
      checked[neighbours.front()] = true;
    }
}

void MTiling::adjustQuads() {
  std::vector<bool> checked(mtiles.size(), false);


  for(std::vector<MTile>::iterator t = mtiles.begin(); t != mtiles.end(); ++t)
    if ((!checked[(*t).getId()]) && ((*t).getGenus() == 0) && ((*t).getNbBoundaries() == 1) && ((*t).getNbMultiPoints() == 2)) {
      checked[(*t).getId()] = true;
      std::vector<unsigned int> neighbours = getNeighbours((*t).getId(), 0, 1, 2);
      if (neighbours.size() != 1)
	throw Exception("adjustQuads(): configuration not handled. Abort.");
      if (getNeighbours(neighbours.front(), 0, 1, 2).size() != 1)
	throw Exception("adjustQuads(): configuration not handled. Abort.");
      adjustQuads(*t, mtiles[neighbours.front()]);
      clear(); /* the mapping is now not valid */
      checked[neighbours.front()] = true;
    }
}

void MTiling::clear(const std::vector<unsigned int> & idtiles) {
  if (mapping != NULL)
    delete mapping;
  mapping = NULL;
  splines.clear();
  sizes.clear();
  parameterized.clear();
  if (idtiles.size() != 0) {
    parameterized = std::vector<bool>(mtiles.size(), false);
    for(std::vector<unsigned int>::const_iterator id = idtiles.begin();
	id != idtiles.end(); ++id) {
      if (*id >= mtiles.size())
	std::cout << "Warning: bad id" << std::endl;
      else
	parameterized[*id] = true;
    }
  }
}



const Mapping2D3D & MTiling::parameterize(const std::vector<unsigned int> & idtiles,
					  bool quad) {
  if (mapping != NULL)
    clear(idtiles);
  if (quad)
    adjustQuads();
  mapping = new Mapping2D3D(mesh);

  std::vector<bool>::const_iterator pzd = parameterized.begin();
  for(std::vector<MTile>::const_iterator t = mtiles.begin();
      t != mtiles.end(); ++t, ++pzd)
    if ((parameterized.size() == 0) || (*pzd)) {
      MeshPart mPart(mesh, (*t).getInsidePoint());
      Mesh ccMesh = mPart.buildCropMesh();
      IDTranslator idTranslator(mPart);

      if (((*t).getGenus() != 0) || (*t).getNbBoundaries() != 1)
	throw Exception("Wrong topology configuration");

      Mapping2D3D rMapping(ccMesh);
      if ((*t).getNbMultiPoints() == 4) {
	QuadParameterizationFloater mParam;
	assert(mPart.hasPoint((*t).getMultiPoint(0)));
	assert(mPart.hasPoint((*t).getMultiPoint(1)));
	assert(mPart.hasPoint((*t).getMultiPoint(2)));
	assert(mPart.hasPoint((*t).getMultiPoint(3)));
	rMapping = mParam.parameterize(ccMesh,
				       idTranslator.g2l((*t).getMultiPoint(0)),
				       idTranslator.g2l((*t).getMultiPoint(1)),
				       idTranslator.g2l((*t).getMultiPoint(2)),
				       idTranslator.g2l((*t).getMultiPoint(3)));
      }
      else {
	DiscParameterizationFloater mParam;
	rMapping = mParam.parameterize(ccMesh);
      }

      VertexID lID = 0;
      for(Mapping2D3D::const_iterator p2D = rMapping.begin(); p2D != rMapping.end(); ++p2D, ++lID)
	(*mapping).set2DCoords(idTranslator.l2g(lID), (*p2D).get2DX(), (*p2D).get2DY());
    }

  // the mesh is flatten
  (*mapping).setFlatten();

  return (*mapping);
}

std::pair<VertexID, VertexID> MTiling::getFirstCorner(const std::vector<VertexID> & multipoints) const {
  if (mapping == NULL)
    throw Exception("getFirstCorner(1): mapping not computed");

  std::pair<VertexID, VertexID> result;
  result.first = multipoints.front();
  Coord2D origin(0., 0.);
  double distance = (*mapping)[result.first].distance2D(origin);
  for(std::vector<VertexID>::const_iterator p = multipoints.begin(); p != multipoints.end(); ++p) {
    const double d = (*mapping)[*p].distance2D(origin);
    if (d < distance) {
      distance = d;
      result.first = *p;
    }
  }
  assert((*mapping)[result.first].distance2D(origin) < 1e-3);

  const VertexID other1 = mesh.getNextBPoint(result.first);
  const VertexID other2 = mesh.getNextBPoint(result.first, other1);
  if (fabs((*mapping)[other1].get2DX()) < fabs((*mapping)[other2].get2DX()))
    result.second = other2;
  else
    result.second = other1;

  return result;
}

std::vector<std::vector<VertexID> > MTiling::getBoundaries(const MTile & mtile) const {
  std::vector<std::vector<VertexID> > result;

  std::vector<VertexID> multipoints = mtile.getMultiPoints();
  if (multipoints.size() == 0)
    throw Exception("getFirstCorner(1): no corner");

  std::pair<VertexID, VertexID> firsts = getFirstCorner(multipoints);

  result.push_back(std::vector<VertexID>());
  result.back().push_back(firsts.first);

  VertexID next = firsts.second;
  while(next != firsts.first) {
    VertexID c = result.back().back();
    result.back().push_back(next);
    bool mp = false;
    for(std::vector<VertexID>::const_iterator m = multipoints.begin();
	m != multipoints.end(); ++m)
      if (next == *m) {
	mp = true;
	break;
      }
    if (mp) {
      result.push_back(std::vector<VertexID>());
      result.back().push_back(next);
    }
    next = mesh.getNextBPoint(next, c);
  }
  assert(next == firsts.first);
  result.back().push_back(next);
  return result;
}

std::vector<boost::shared_ptr<Go::SplineCurve> > MTiling::computeBdFitting(const MTile & mtile,
									   unsigned int degree,
									   unsigned int nbIterBd,
									   unsigned int nb_points) const {
  std::vector<boost::shared_ptr<Go::SplineCurve> > surf_boundary_curves(4);
  std::vector<std::vector<VertexID> > boundaries = getBoundaries(mtile);
  assert(boundaries.size() == 4);
  std::vector<boost::shared_ptr<Go::SplineCurve> >::iterator sc = surf_boundary_curves.begin();
  for(std::vector<std::vector<VertexID> >::const_iterator b = boundaries.begin();
      b != boundaries.end(); ++b, ++sc) {
    std::vector<double> crv_points;
    std::vector<double> crv_param;
    std::vector<VertexID>::const_iterator predp = (*b).begin();
    double dd = 0.;
    // set points and parameter values
    for(std::vector<VertexID>::const_iterator p = (*b).begin(); p != (*b).end(); predp = p, ++p) {
      crv_points.push_back(mesh.point(*p).getX());
      crv_points.push_back(mesh.point(*p).getY());
      crv_points.push_back(mesh.point(*p).getZ());
      if (predp != p) {
	dd += mesh.point((*predp)).distance(mesh.point(*p));
      }
      crv_param.push_back(dd);
    }
    // parameter values between 0 and 1
    for(std::vector<double>::iterator p = crv_param.begin(); p != crv_param.end(); ++p)
      *p /= dd;

    unsigned int nbpoints = nb_points;
    if (nb_points == 0)
      nbpoints = ceil((double)crv_param.size() / 12);
    if (nbpoints < degree)
      nbpoints = degree;
    Go::ApproxCurve approx_curve(crv_points, crv_param, 3, 1e-5, nbpoints, degree);
    double maxdist, avdist;
    (*sc) = approx_curve.getApproxCurve(maxdist, avdist, nbIterBd);
  }

  return surf_boundary_curves;
}

boost::shared_ptr<Go::SplineSurface> MTiling::computeFitting(const MTile & mtile, double & sx, double & sy,
							     unsigned int degree,
							     unsigned int nbIter,
							     unsigned int nbIterBd,
							     unsigned int nb_points) const {
  assert(mapping != NULL);
  std::vector<double> all_pts, all_pars;
  assert(ccIds.size() == mesh.getNbPoints());
  assert(mtile.getNbMultiPoints() != 0);
  Mapping2D3D::const_iterator p2d = (*mapping).begin();
  const unsigned int idCC = ccIds[mtile.getMultiPoint(0)];
  Box2D bbox;
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p, ++p2d)
    if (idCC == ccIds[(*p).getId()]) {
      all_pts.push_back((*p).getX());
      all_pts.push_back((*p).getY());
      all_pts.push_back((*p).getZ());
      if (all_pars.size() != 0)
	bbox.addPoint(*p2d);
      else
	bbox = Box2D((*p2d).get2DX(), (*p2d).get2DY(), (*p2d).get2DX(), (*p2d).get2DY());
      all_pars.push_back((*p2d).get2DX());
      all_pars.push_back((*p2d).get2DY());
    }
  sx = bbox.getMaxX() - bbox.getMinX();
  sy = bbox.getMaxY() - bbox.getMinY();
  double domain[4] = {bbox.getMinX(), bbox.getMaxX(), bbox.getMinY(), bbox.getMaxY()};
  std::vector<boost::shared_ptr<Go::SplineCurve> > bd = computeBdFitting(mtile, degree, nbIterBd, nb_points);
  if (nbIter == 0) {
    std::vector<boost::shared_ptr<Go::ParamCurve> > bdcrvs(bd.size());
    std::copy(bd.begin(), bd.end(), bdcrvs.begin());
    Go::CurveLoop boundary(bdcrvs, 10e-6);
    boost::shared_ptr<Go::SplineSurface> curr_srf_;
    curr_srf_ = boost::shared_ptr<Go::SplineSurface>(Go::CoonsPatchGen::createCoonsPatch(boundary));
    // Scale surface to live on the given domain
    curr_srf_->setParameterDomain(domain[0], domain[1],
				  domain[2], domain[3]);
    return curr_srf_;
  }
  else {
    Go::ApproxSurf approx(bd, all_pts, all_pars, domain, 3, 1e-4);
    double maxdist, avdist;
    int nmb_out;
    boost::shared_ptr<Go::SplineSurface> result = approx.getApproxSurf(maxdist, avdist, nmb_out, nbIter);
    return result;
  }
}

const std::vector<boost::shared_ptr<Go::SplineSurface> > & MTiling::computeFitting(unsigned int degree,
										   unsigned int nbIter,
										   unsigned int nbIterBd,
										   unsigned int nb_points) {
  if (splines.size() != 0) {
    splines.clear();
    sizes.clear();
  }

  if ((mapping == NULL) || (!isQuadTiling())) {
    adjustQuads();
    parameterize();
  }
  nb_points_fitting = nb_points;
  degree_fitting = degree;
  nbiter_fitting = nbIter;
  nbiter_bd_fitting = nbIterBd;

  for(std::vector<MTile>::const_iterator t = mtiles.begin(); t != mtiles.end(); ++t) {
    if (((*t).getGenus() != 0) || (*t).getNbBoundaries() != 1)
      throw Exception("Wrong topology configuration");
    double sx = -1.;
    double sy = -1.;
    splines.push_back(computeFitting(*t, sx, sy, degree, nbIter, nbIterBd, nb_points));
    sizes.push_back(std::pair<double, double>(sx, sy));
  }

  return splines;
}


bool MTiling::saveApproxSurf(const std::string & filename, unsigned int degree,
			     unsigned int nbIter,
			     unsigned int nbIterBd,
			     unsigned int nb_points) {
  getBSplines(degree, nbIter, nbIterBd, nb_points);

  std::ofstream ofile(filename.c_str(), std::ios::out);

  if (!ofile.is_open())
    throw Exception("saveApproxSurf(): cannot open output file");

  ofile << std::setprecision(15);
  for(std::vector<boost::shared_ptr<Go::SplineSurface> >::const_iterator spline = splines.begin();
      spline != splines.end(); ++spline) {
    writeApproxSurf(ofile, **spline);
  }

  ofile.close();
  return true;
}

bool MTiling::writeApproxSurf(std::ostream& os, const Go::SplineSurface & spline) {
  if (spline.rational())
      os << "sctype rat bmatrix" << std::endl;
    else
      os << "sctype bmatrix" << std::endl;
    os << "deg " << (spline.order_u() - 1) << " " << (spline.order_v() - 1) << std::endl;
    os << "step 1 1" << std::endl;

    os << "bmat u";
    for(std::vector< double >::const_iterator bmat = spline.basis_u().begin(); bmat != spline.basis_u().end(); ++bmat)
      os << " " << *bmat;
    os << std::endl;
    os << "bmat v ";
    for(std::vector< double >::const_iterator bmat = spline.basis_v().begin(); bmat != spline.basis_v().end(); ++bmat)
      os << " " << *bmat;
    os << std::endl;

  return true;
}


Mesh MTiling::remeshQuadByBSplines(const Go::SplineSurface & spline, double originu, double originv, double distance) {
  Mesh result;
  std::vector<double> points;
  std::vector<double> param_u;
  std::vector<double> param_v;
  unsigned int nbu = (int)(originu / distance) + 1;
  unsigned int nbv = (int)(originv / distance) + 1;
  if (nbu < 2)
    nbu = 2;
  if (nbv < 2)
    nbv = 2;

  spline.gridEvaluator(nbu, nbv, points, param_u, param_v);
  std::vector<Point3D> finalPoints;
  std::deque<Triangle> finalTriangles;
  assert(points.size() % 3 == 0);
  assert(points.size() != 0);
  for(std::vector<double>::const_iterator p = points.begin(); p != points.end(); p += 3) {
    finalPoints.push_back(Point3D(*p, *(p + 1), *(p + 2)));
  }
  result.addPoints(finalPoints, true);

  for(unsigned int i = 0; i < nbu - 1; ++i)
    for(unsigned int j = 0; j < nbv - 1; ++j) {
      VertexID first = i + nbu * j;
      finalTriangles.push_back(Triangle(first, first + 1, first + nbu));
      finalTriangles.push_back(Triangle(first + 1, first + nbu + 1, first + nbu));
    }
  result.addTriangles(finalTriangles);

  return result;
}

std::vector<std::pair<unsigned int, unsigned int> > MTiling::getCylindersByQuad() const {
  std::vector<std::pair<unsigned int, unsigned int> > result;

  unsigned int id1 = 0;
  for(std::vector<MTile>::const_iterator t1 = mtiles.begin(); t1 != mtiles.end(); ++t1, ++id1)
    if ((*t1).isQuad()) {
      std::vector<unsigned int> seen;
      for(std::vector<unsigned int>::const_iterator id2 = (*t1).getNeighbours().begin(); id2 != (*t1).getNeighbours().end(); ++id2) {
	if ((*id2 > id1) && (find(seen.begin(), seen.end(), *id2) == seen.end())) {
	  const MTile & t2 = mtiles[*id2];
	  if (t2.isQuad()) {
	    const std::vector<std::pair<std::vector<VertexID>, std::vector<VertexID> > > borders = getCommonBorders(*t1, t2);
	    if (borders.size() == 2) {
	      if ((borders.front().first.back() != // t1
		   borders[1].first.back()) &&
		  (borders.front().first.front() !=
		   borders[1].first.back()) &&
		  (borders.front().first.front() !=
		   borders[1].first.front()) &&
		  (borders.front().first.back() !=
		   borders[1].first.front()) && // t2
		  (borders.front().second.back() !=
		   borders[1].second.back()) &&
		  (borders.front().second.front() !=
		   borders[1].second.back()) &&
		  (borders.front().second.front() !=
		   borders[1].second.front()) &&
		  (borders.front().second.back() !=
		   borders[1].second.front())) {
		// the two borders of t1 are not adjacent, the same for t2
		result.push_back(std::pair<unsigned int, unsigned int>(id1, *id2));
	      }
	    }
	  }
	}
	seen.push_back(*id2);
      }
    }

  return result;
}

bool MTiling::isComputedParam(const std::vector<unsigned int> & idtiles) const {
  if (mapping == NULL)
    return false;

  if (parameterized.size() == 0)
    return true;

  assert(parameterized.size() == mtiles.size());

  if (idtiles.size() == 0) {
    for(std::vector<bool>::const_iterator pzd = parameterized.begin();
	pzd != parameterized.end(); ++pzd)
      if (!(*pzd))
	return false;
  }

  for(std::vector<unsigned int>::const_iterator id = idtiles.begin();
      id != idtiles.end(); ++id) {
    if (*id >= mtiles.size())
      std::cout << "Warning: bad id" << std::endl;
    else {
      if (!parameterized[*id])
	return false;
    }
  }

  return true;
}


MeshMapUInt MTiling::getIdTiles() const {
  const unsigned int unknown = mtiles.size();
  MeshMapUInt result(mesh, unknown);

  for(std::vector<MTile>::const_iterator mt = mtiles.begin(); mt != mtiles.end(); ++mt) {
    std::vector<VertexID> open;
    open.push_back((*mt).getInsidePoint());
    assert(result.getValue((*mt).getInsidePoint()) == unknown);
    result.setValue((*mt).getInsidePoint(), (*mt).getId());

    while(!open.empty()) {
      VertexID v = open.back();
      const Point3D & p = mesh.point(v);
      open.pop_back();
      assert(result.getValue(v) == (*mt).getId());
      for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin();
	  nb != p.getNeighbours().end(); ++nb)
	if (result.getValue(*nb) != (*mt).getId()) {
	  assert(result.getValue(*nb) == unknown);
	  result.setValue(*nb, (*mt).getId());
	  open.push_back(*nb);
	}
    }

  }

  return result;
}
