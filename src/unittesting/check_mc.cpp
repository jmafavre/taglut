/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / CNR-IMATI
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <iostream>
#include "CImg.h"
#include "Mesh.h"

using namespace cimg_library;
using namespace Taglut;


int main (int, const char **) {

  std::cout << "Create a mesh from a marching cube. " << std::endl;
  const unsigned int imagesize = 4;
  const unsigned int cubesize = 2;
  const unsigned int begincube = imagesize / 2 - cubesize / 2;


  CImg<unsigned char> image(imagesize, imagesize, imagesize, 1);

  const unsigned char inside = 255;
  const unsigned char outside = 0;

  image.fill(outside);

  for(unsigned int i = 0; i < cubesize; ++i)
    for(unsigned int j = 0; j < cubesize; ++j)
      for(unsigned int k = 0; k < cubesize; ++k)
    image(begincube +i, begincube + j, begincube + k) = inside;


  Mesh mesh(image);

  if (mesh.getNbCC() != 1) {
    std::cout << "Wrong number of connected components (" << mesh.getNbCC() << ")" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 2;
  }

  if (mesh.getGenus() != 0) {
    std::cout << "Wrong genus" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 2;
  }
  if (mesh.getNbBoundaries() != 0) {
    std::cout << "Wrong number of boundaries" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 2;
  }

  Box3D b = mesh.getBox();

  if (b.getMinX() != begincube - 0.5) {
    std::cout << "Wrong minx value" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 1;
  }
  if (b.getMinY() != begincube - 0.5) {
    std::cout << "Wrong miny value" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 1;
  }
  if (b.getMinZ() != begincube - 0.5) {
    std::cout << "Wrong minz value" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 1;
  }


  if (b.getMaxX() != begincube + cubesize - 0.5) {
    std::cout << "Wrong maxx value (" << b.getMaxX() << " rather than " << (begincube + cubesize - 0.5) << ")" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 1;
  }
  if (b.getMaxY() != begincube + cubesize - 0.5) {
    std::cout << "Wrong maxy value" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 1;
  }
  if (b.getMaxZ() != begincube + cubesize - 0.5) {
    std::cout << "Wrong maxz value" << std::endl;
    std::cerr << "Fail" << std::endl;
    return 1;
  }

  std::cout << "Pass" << std::endl;
  return 0;
}
