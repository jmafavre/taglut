/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <j-marie.favreau@u-clermont1.fr>
 *                    ISIT / Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <iostream>
#include "CylinderParameterization.h"

using namespace Taglut;


unsigned int TEST_step = 0;

#define TEST_EQUAL(v1, v2, msg) \
  TEST_ZERO(fabs(v1 - v2), 1e-6, msg, v1 << " (!= " << v2 << ")")

#define TEST_ZERO(v, epsilon, msg, msg2) \
  TEST_BOOL(v > epsilon, msg, msg2)

#define TEST_BOOL(b, msg, msg2) \
  if (b) { \
    std::cout << "!! Wrong " << msg << " result: " << msg2 << std::endl; \
    std::cerr << " Fail" << std::endl; \
    return 1; \
  } \
  else \
    std::cout << " Pass " << msg << " test " << TEST_step++ << std::endl;


int main (int, const char **) {

  const double value = TwoCirclesBorderParameterization::getValue(0.,1., 0., 2., .3);
  TEST_EQUAL(value, .3, "getValue()")

  double angle = TwoCirclesBorderParameterization::getAngle(0., M_PI / 3, .5, 0);
  TEST_EQUAL(angle, M_PI / 6, "getAngle()")

  angle = TwoCirclesBorderParameterization::getAngle(M_PI / 3, 0., .5, 0);
  TEST_EQUAL(angle, M_PI / 6, "getAngle()")

    angle = TwoCirclesBorderParameterization::getAngle(1.5 * M_PI, 0., .5, 0);
  TEST_EQUAL(angle, 1.75 * M_PI, "getAngle()")

  angle = TwoCirclesBorderParameterization::getAngle(0., 1.5 * M_PI, .5, 0);
  TEST_EQUAL(angle, 1.75 * M_PI, "getAngle()")

  std::cout << "PASS" << std::endl;
  return 0;
}
