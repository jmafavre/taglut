/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "CPMethod.h"
#include "Mapping2D3D.h"
#include "Exception.h"
#include "FileTools.h"

using namespace std;
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static double epsilon = 0.000000000001;
static int    maxIter = 40000;
static int    randomValue = 0;
static int    border = 0;
static int    borderTriangles = 0;
static int    size = 1;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input image", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output file (image, cst)", NULL},
  { "epsilon", 'e', POPT_ARG_DOUBLE, &epsilon, 0, "Epsilon (used in circle packing algorithm)", NULL},
  { "maxIter", 'm', POPT_ARG_INT, &maxIter, 0, "Maximum iteration (used in circle packing algorithm)", NULL},
  { "random", 'r', POPT_ARG_NONE, &randomValue, 0, "Use random color on output image.", NULL},
  { "border", 'b', POPT_ARG_NONE, &border, 0, "Draw border.", NULL},
  { "triangle-border", 't', POPT_ARG_NONE, &borderTriangles, 0, "Draw border for each triangle.", NULL},
  { "size", 's', POPT_ARG_INT, &size, 0, "Scale ratio for output image (after circle packing).", NULL},
 POPT_AUTOHELP
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("distortImage", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if (filename_output == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }

  if (maxIter <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: maximum iteration not positive." << endl;
    return 1;
  }

  if (epsilon <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: epsilon not positive." << endl;
    return 1;
  }

  try {
    /* load image */
    cout << "Loading image..." << endl;
    CImg<unsigned char> src(filename_input);

    cout << "Grayscale image conversion..." << endl;
    CImg<unsigned char> image(src._width, src._height);
    cimg_forXY(src, x, y)
      image(x, y) = src(x, y);

    cout << "Building mesh..." << endl;
    /* build mesh then circle set */
    Mesh mesh;
    mesh.createFromImage(image);
    Mapping2D3D mapping(mesh);
    CPMethod cset(mapping);

    cout << "Circle packing: unfolding..." << endl;
    /* unfold using circle packing */
    int nbIter = cset.processUnfolding(epsilon, maxIter);
    cout << " Iteration number: " << nbIter << endl;


    if (cset.isSaveFormat(filename_output)) {
      cout << "Saving circle set..." << endl;
      cset.save(filename_output);
    }
    else {
      CImg<unsigned char> result(src._width * size, src._height * size);
      result.fill(std::numeric_limits<unsigned char>::max());

      cout << "Conversion to image..." << endl;
      if (randomValue != 0)
	mapping.toImageRandom(result);
      else
	mapping.toImage(result, image);

      if (border != 0)
	mapping.toImageDrawBorder(result, (unsigned char)0);

      if (borderTriangles != 0)
	mapping.toImageDrawBorderTriangles(result, (unsigned char)0);


      /* save result image */
      cout << "Saving image..." << endl;
      result.save(filename_output);

    }
  }
  catch(Exception e) {
    cerr << e << "Abort." << endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }

  return 0;
}
