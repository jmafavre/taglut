/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "MeshPatches.h"
#include "Display3D.h"
#include "Length.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_mask  = NULL;
static char*  filename_region  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    forceInside = -1;
static int    help = 0;
static char*  length = NULL;
static double ratio = -1;
static int    exact = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML, OBJ or PLY file", NULL},
  { "mask", 'm', POPT_ARG_STRING, &filename_mask, 0, "DICOM file with inside/outside brain map (for pseudo-volumic approach or to generate the initial mesh)", NULL},
  { "region", 'r', POPT_ARG_STRING, &filename_region, 0, "DICOM file with inside/outside region of interest (ROI) for pseudo-volumic approach", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "exact", 'e', POPT_ARG_NONE, &exact, 0, "Use an exact method to compute the shortest loops", NULL},
  { "force-inside", 'f', POPT_ARG_INT, &forceInside, 0, "Use a pseudo-volumic method to compute the shortest loops, and force the cuts in the ROI to be inside (if 1) or outside (if 0) of the object.", NULL},
  { "length", 'l', POPT_ARG_STRING, &length, 0, "Select length method used to cut surface (or \"list\" for list them)", NULL},
  { "ratio-length", 'a', POPT_ARG_DOUBLE, &ratio, 0, "Ratio of euclidean length that can be modified by other length methods. 0 <= ratio <= 1", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  LengthFactory lFactory;

  poptContext context = poptGetContext("brainCorrection", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Brain correction", "Correct a surface by patching to obtain a surface homeomorphic to a sphere.");
    hMsg << "Input: mesh to patch (available format: VRML, OBJ, PLY)";
    hMsg << "Output: display patch mesh or save it (available format: VRML, OBJ, PLY)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if ((length != NULL) && (strcmp(length, "list") == 0)) {
    cout << lFactory << endl;
    return 0;
  }

  lFactory.setLength(length);
  if (!lFactory.existsLength()) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: length unknown (try -l list)." << endl;
    return 1;
  }

  if ((filename_input == NULL) && (filename_mask == NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file or a mask file." << endl;
    return 1;
  }

  if ((filename_output == NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }

  if (forceInside != -1) {
    if ((forceInside == 0) || (forceInside == 1)) {
      if (filename_mask == NULL) {
	cerr << "Error: Specify a mask file." << endl;
	return 1;
	}
      if (filename_region == NULL) {
	cerr << "Error: Specify a region file." << endl;
	return 1;
      }
    }
    else {
      if (exact != 0) {
	cout << "Cannot have both pseudo-volumic approach and exact method. Force pseudo-volumic" << endl;
      }
    }
  }

  cout << "Building mesh..." << endl;
  Mesh mesh;

  /* load mesh */
  try {
    if (filename_input != NULL)
      mesh.load(filename_input, objectName);
    else {
      assert(filename_mask != NULL);
      mesh.load(filename_mask);
    }
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }

  /* load images */
  UCharCImg mask, region;
  if (forceInside != -1) {
    try {
      mask.load(filename_mask);
      region.load(filename_region);
    }
    catch (CImgException e) {
      cerr << e.what() << endl;
    }
  }

  MeshPathes::OptimEC oec = MeshPathes::OECApproximation;
  if (exact != 0)
    oec = MeshPathes::OECbyStripAndPredCut;


  cout << "Cleaning mesh..." << endl;
  while(mesh.getNbBadBPoints() != 0) {
    MeshPart mPart(mesh);
    mPart.cleanMesh();
  }

  cout << "Selecting main connected component..." << endl;
  std::cout << mesh.getInfos() << std::endl;
  MeshPart mPart = mesh.getLargestCC();
  cout << "Crop main connected component..." << endl;
  mPart.cropMesh();


  cout << "Computing patches..." << endl;
  if (ratio >= 0)
    lFactory.setRatio(ratio);
  MeshPatches mPatches(mesh, lFactory.getLength(mesh));

  try {
    cout << " Number of points before patch: " << mesh.getNbPoints() << endl;
    if (forceInside == -1)
      mPatches.patchMesh(-1, oec);
    else
      mPatches.patchMeshPseudoVolumic(mask, region, forceInside == 1);
    cout << " Number of points after patch: " << mesh.getNbPoints() << endl;

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      mesh.save(filename_output, objectName);
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
