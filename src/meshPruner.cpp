/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "MeshPrune.h"
#include "Display3D.h"
#include "Length.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    display = 0;
static int    cComponent = 0;
static int    help = 0;
static int    drawEdges = 0;
static int    pruneDepth = 1;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "connected-component", 'c', POPT_ARG_INT, &cComponent, 0, "Connected component in object", NULL},
  { "prune-depth", 'p', POPT_ARG_INT, &pruneDepth, 0, "Prune depth", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "edges", 'e', POPT_ARG_NONE, &drawEdges, 0, "Draw edges", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  LengthFactory lFactory;

  poptContext context = poptGetContext("meshPruner", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh pruner", "Prune input using a algorithm that build new triangles using part of the edges.");
    hMsg << "Input: mesh to cut (available format: VRML)";
    hMsg << "Output: display cut mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
  }

  if (pruneDepth < 0)
    pruneDepth = 0;

  cout << "Building mesh..." << endl;
  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename_input, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }


  cout << "Checking for topological properties...";
  std::deque<MeshPart> cc = mesh.getConnectedComponents();
  if (((int)cc.size() <= cComponent) || (cComponent < 0)) {
    cerr << "Error: connected component #" << cComponent << " unknown. Surface has " << cc.size() << " connected components."<< endl;
    return 1;
  }
  cout << " OK" << endl;

  cout << "Selecting connected component..." << endl;
  Mesh meshCropped(cc[cComponent]);

  cout << "Computing pruned graph..." << endl;
  MeshPrune prune(meshCropped, pruneDepth);

  try {

    prune.pruneMesh();

    if (display != 0)
      Display3D::displayMesh(256, 256, "3D Visualisation", meshCropped, drawEdges != 0, true);

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      meshCropped.save(filename_output, objectName);
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
