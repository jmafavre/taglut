/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "CImgUse.h"
#include "Mesh.h"
#include "Display3D.h"
#include "MeshPart.h"
#include "Point3D.h"

#include <unistd.h>
#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  objectName      = NULL;
static int    display         = 0;
static int    help            = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display object after curvature computation", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("Global curvature of mesh", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("meshGlobalCurvature", "Computing for the given mesh a global measure of the curvature.");
    hMsg << "Input: mesh to use (available format: VRML, PLY, ...)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }


  try {
    /* build mesh */
    Mesh mesh(filename_input, objectName);


    /* process mesh */
    cout << "Processing..." << endl;
    long double result = 0.0;
    VertexID nbEdges = 0;
    long double areaTriangles = 0.0;

    for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
      for(std::deque<VertexID>::const_iterator n = (*p).getNeighbours().begin(); n != (*p).getNeighbours().end(); ++n)
	if ((*p).getId() < *n) {
	  try {
	    const Triangle & t1 = mesh.findTriangle((*p).getId(), *n);
	    const Triangle & t2 = mesh.findOtherTriangle((*p).getId(), *n, t1);
	    ++nbEdges;
	    areaTriangles += mesh.getArea(t1.getId()) + mesh.getArea(t2.getId());
	    result += mesh.computeLocalCurvature((*p).getId(), *n) * (mesh.getArea(t1.getId()) + mesh.getArea(t2.getId()));
	  }
	  catch (Exception e) {
	    // Edge with missing triangle: boundary or single edge.
	  }
	}
    result /= areaTriangles;
    std::cout << "Number of valid edges: " << nbEdges << std::endl;
    std::cout << "Shape estimator: " << result << std::endl;

    if (display != 0)
      Display3D::displayMesh(512, 512, "3D Visualisation", mesh);

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
