/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "MeshList.h"
#include "MeshPart.h"
#include "MTiling.h"

#include "Messages.h"

using namespace Taglut;

static char* filename  = NULL;
static char* objectName = NULL;
static int   help = 0;
static long int point = -1;
static int   tiles = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "point", 'p', POPT_ARG_INT, &point, 0, "Object name", NULL},
  { "tiles", 't', POPT_ARG_NONE, &tiles, 0, "Show tile properties", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("meshInfos", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh infos", "Display topological informations of input mesh.");
    hMsg << "Input: mesh to display (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  try {
    /* load mesh */
    MeshList meshes(filename, objectName);

    /* triangulate if needed */
    std::cout << "Mesh (" << filename << ")" << std::endl;
    unsigned int idM = 0;
    for(MeshList::iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
      if (point < 0) {
	if (idM != 0)
	  std::cout << std::endl;

	std::cout << " Mesh #" << idM << std::endl;
	std::cout << "  Triangle number: " << (*mesh).getNbTriangles() << "; ";
	std::cout << "  Points number: " << (*mesh).getNbPoints() << std::endl;

	std::deque<MeshPart> cc = (*mesh).getConnectedComponents();
	std::cout << "  Connected components number: " << cc.size() << std::endl;

	if (tiles == 0) {
	  unsigned int i = 0;
	  for(std::deque<MeshPart>::iterator it = cc.begin(); it != cc.end(); ++it) {
	    std::cout << std::endl << "  Component #" << i << ": " << std::endl;
	    std::cout << "   Points number: " << (*it).getNbPoints();
	    VertexID badBPoints = (*it).getNbBadBPoints();
	    if (badBPoints != 0)
	      std::cout << " (" << badBPoints << " incoherent boundary points)";
	    std::cout << std::endl;
	    std::cout << "   Triangles number: " << (*it).getNbTriangles();
	    TriangleID badTriangles = (*it).getNbBadTriangles();
	    if (badTriangles != 0)
	      std::cout << " (" << badTriangles << " incoherent triangles)";
	    std::cout << std::endl;
	    std::cout << "   Edges number: " << (*it).getNbEdges();
	    VertexID nbSingleEdges = (*it).getNbSingleEdges();
	    if (nbSingleEdges != 0)
	      std::cout << " (single edges: " << nbSingleEdges << ")";
	    std::cout << std::endl;
	    std::cout << "   Boundaries number: " << (*it).getNbBoundaries();
	    VertexID bPSize = (*it).getBoundaryPoints().size();
	    if (bPSize != 0)
	      std::cout << " (" << bPSize << " points)";
	    std::cout << std::endl;
	    std::cout << "   Genus: " << (*it).getGenus() << std::endl;
	    std::cout << "   Euler caracteristic: " << (*it).getEulerCaracteristic() << std::endl;
	    std::cout << "   2nd Betti number: " << (*it).getBettiNumber() << std::endl;
	    std::cout << "   Box size: " << (*it).getBoxSize() << std::endl;
	    ++i;
	  }
	}
	else {
	  MTiling tiling(*mesh);
	  if (tiling.getNbTiles() > 1) {
	    std::cout << std::endl << "Tiles: " << std::endl;
	    unsigned int nbt = 0;
	    for(std::vector<MTile>::const_iterator t = tiling.getTiles().begin(); t != tiling.getTiles().end(); ++t, ++nbt) {
	      std::cout << " - #" << nbt << ": (genus: " << (*t).getGenus() << ", boundaries: " << (*t).getNbBoundaries() << ", multipoints: " << (*t).getNbMultiPoints() << ")" << std::endl;
	    }
	  }
	}
      }
      else {
	if ((unsigned int) point < (*mesh).getNbPoints())
	  (*mesh).tracePoint(point);
      }
      ++idM;
    }
  }
  catch(Exception e) {
    std::cerr << e << std::endl;
    return 1;
  }

  return 0;
}
