/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <popt.h>
#include "CImgUse.h"
#include <assert.h>
#include <sstream>
#include <limits>


#include "Messages.h"

using namespace std;
using namespace Taglut;

static char* filename  = NULL;
static char* filename2  = NULL;
static double zratio = 1.0;
static int   help = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input image", NULL},
  { "input-bis", 'j', POPT_ARG_STRING, &filename2, 0, "Input image (second)", NULL},
  { "zratio", 'z', POPT_ARG_DOUBLE, &zratio, 0, "Zratio", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};




int main (int argc, const char **argv) {
  poptContext context = poptGetContext("imageViewer", argc, argv, options, 0);

  /* check values */

  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Image viewer", "Display image.");
    hMsg << "Input: image to view";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }


  try {
    /* load image */
    CImg<> img(filename);

    if (filename2 == NULL) { // if only one image
      if (img._depth != 1) {
	CImgDisplay disp(img._width + img._depth, img._height + img._depth, "Visualisation", 1);
	int X = img._width / 2, Y = img._height / 2, Z = img._depth / 2, area = 0;

	while (!disp._is_closed && (disp.key() != cimg::keyESC)) {
	  CImg<unsigned char> visu(disp._width, disp._height, 1);

	  visu = img.get_projections2d(X, Y, Z).normalize(0, 255).resize(disp._width, disp._height, 1, cimg::min((unsigned int)3, img._spectrum));

	  visu.display(disp);

	  if (disp._button != 0) {
	    const int mx = disp._mouse_x, my = disp._mouse_y;
	    if ((mx >= 0) && (my >= 0)) {
	      const int mX = mx * (img._width + (img._depth > 1 ? img._depth : 0)) / disp._width, mY = my * (img._height + (img._depth > 1 ? img._depth : 0)) / disp._height;
	      if ((unsigned int)mX < img._width && (unsigned int)mY < img._height)   { area = 1; X = mX; Y = mY; }
	      if ((unsigned int)mX < img._width && (unsigned int)mY >= img._height)  { area = 2; X = mX; Z = mY - img._height; }
	      if ((unsigned int)mX >= img._width && (unsigned int)mY < img._height)  { area = 3; Y = mY; Z = mX - img._width;  }
	      if ((unsigned int)mX >= img._width && (unsigned int)mY >= img._height) { X = 0; Y = 0; Z = 0; }
	      if ((unsigned int)mX < img._width && (unsigned int)mY < img._height)   { area = 1; X = mX; Y = mY; }
	    }

	    if (disp._is_resized) disp.resize();
	  }
	}
      }
      else {
	CImgDisplay disp(img);

	while(!disp._is_closed && (disp.key() != cimg::keyESC)) {
	  disp.wait(50);
	  img.display(disp);
	}
      }
    }
    else {
      /* load image */
      const unsigned char colorWhite = std::numeric_limits<unsigned char>::max();
      const unsigned char colorBlack = 0;
      unsigned char color;

      CImg<> img2(filename2);
      img.normalize(0, 255);
      img2.normalize(0, 255);

      assert(img2._width == img._width);
      assert(img2._height == img._height);
      assert(img2._depth == img._depth);

      CImgDisplay disp(img._width + img2._width + 15, max(img._height, img2._height) + 20, "Visualisation", 1);
      int x, y, z;
      if (img._depth == 1)
	z = 0;
      else
	z = img._depth / 2;
      x = img._width / 2;
      y = img._height / 2;

      CImg<unsigned char> visu(disp._width,disp._height, 1);
      visu.draw_image(5, 5, img.get_slice(z));
      visu.draw_image(10 + img._width, 5, img2.get_slice(z));



      while (!disp._is_closed && (disp.key() != cimg::keyESC)) {

	if (disp._button != 0) {
	  int mx = disp._mouse_x, my = disp._mouse_y;
	  if ((unsigned int)mx > 5 + img._width)
	    mx -= 10 + img._width;
	  else
	    mx -= 5;

	  if ((mx >= 0) && (my >= 0)) {
	    x = mx;
	    y = my;
	  }
	}

	if (disp._wheel) {
	  z += disp._wheel;
	  if (z <= 0)
	    z = 0;
	  if ((unsigned int) z >= min(img._depth, img2._depth))
	    z = min(img._depth, img2._depth) - 1;
	  disp._wheel = 0;
	}

	// redraw cross
	visu.draw_image(5, 5, img.get_slice(z));
	visu.draw_image(10 + img._width, 5, img2.get_slice(z));

	if (img(x, y, z) > 128)
	  color = colorBlack;
	else
	  color = colorWhite;
	visu.draw_line(5 + x - 2, 5 + y, 5 + x + 2, 5 + y, &color);
	visu.draw_line(5 + x, 5 + y - 2, 5 + x, 5 + y + 2, &color);

	if (img2(x, y, z) > 128)
	  color = colorBlack;
	else
	  color = colorWhite;
	visu.draw_line(10 + img._width + x - 2, 5 + y, 10 + img._width + x + 2, 5 + y, &color);
	visu.draw_line(10 + img._width + x, 5 + y - 2, 10 + img._width + x, 5 + y + 2, &color);

	std::ostringstream str;
	str << "x: " << x << " y: " << y << " z: " << (z * zratio) << "               ";
	visu.draw_text(10, img._width + 10, str.str().c_str(), &colorWhite, &colorBlack);

	visu.display(disp);

      }
    }


  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  poptFreeContext(context);
  return 0;
}


