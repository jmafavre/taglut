#include <algorithm>
#include <list>
#include "MeshCPCNaive.h"
#include "Mesh.h"
#include "RandomGenerator.h"
#include "RandomVertexSelector.h"
#include "MeshMap.h"
#include "MeshManipulator.h"

using namespace Taglut;

MeshCPCNaive::MeshCPCNaive(Mesh & mesh_t,
			   const RandomGenerator & rDistrib_t,
			   const RandomVertexSelector & vSelector_t,
			   const RandomGenerator & wDistrib_t,
			   long unsigned int nbMultipliers_t,
			   long double coef_t) : multipliers(mesh_t.getNbPoints(), std::vector<Multiplier>()),
					       mesh(mesh_t), meshMap(mesh_t),
					       rDistrib(rDistrib_t),
					       wDistrib(wDistrib_t),
					       vSelector(vSelector_t),
					       nbMultipliers(nbMultipliers_t),
					       coef(coef_t) {

  std::cout << " Creating the multipliers..." << std::endl;
  generateMultipliers();
  std::cout << " Computing the texture..." << std::endl;
  computeTexture();

}


void MeshCPCNaive::generateMultipliers() {
  for (long unsigned int i = 0; i < nbMultipliers; ++i) {
    VertexID v = vSelector();
    long double r = rDistrib() / 2; // the radius of the multiplier on the surface is r/2
    long double w = wDistrib();
    multipliers[v].push_back(Multiplier(r, w));
  }
}

void MeshCPCNaive::computeTexture() {
  bool verbose = nbMultipliers > 10;
  MeshManipulator mManip(mesh);
  const long double value = 1.0 / coef;
  for(long int i = 0; (VertexID) i < mesh.getNbPoints(); ++i) {
    meshMap.setValue(i, value);
  }

  VertexID ii = 0;
  const VertexID iimax = multipliers.size();
  for(std::vector<std::vector<Multiplier> >::iterator i = multipliers.begin(); i != multipliers.end(); ++i, ++ii) {
    if (verbose) {
      std::cout << "\r  Processing... " << ii << " / " << iimax;
      std::cout.flush();
    }
    if ((*i).size() != 0) {
      sort((*i).rbegin(), (*i).rend()); // sort using the r value (the first is the largest)
      mManip.computeDijkstraBound(ii, (*i).front().getR());
      std::list<VertexID> open;
      open.push_back(ii);
      assert(mesh.point(ii).getFlag() == 0);
      mesh.point(ii).setFlag(1);
      while(open.size() != 0) {
	VertexID p = open.back();
	open.pop_back();
	double l = mManip.getLength(p);
	for(std::vector<Multiplier>::const_iterator m = (*i).begin(); m != (*i).end(); ++m)
	  if ((*m).getR() >= l) {
	    meshMap[p] *= (*m).getW();
	  }
	  else
	    break;
	for(std::deque<VertexID>::const_iterator nb = mesh.point(p).getNeighbours().begin();
	    nb != mesh.point(p).getNeighbours().end(); ++nb) {
	  Point3D & nbp = mesh.point(*nb);
	  if ((nbp.getFlag() == 0) && (mManip.getLength(*nb) >= 0.0)) {
	    open.push_back(*nb);
	    nbp.setFlag(1);
	  }
	}

      }
    }
  }
  if (verbose)
    std::cout << std::endl;

}
