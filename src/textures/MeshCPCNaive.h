/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_CPC_NAIVE
#define MESH_CPC_NAIVE
#include <vector>
#include "MeshMap.h"


namespace Taglut {

  /**
     For compilation speed, Mesh is not defined here using an include, but with a
     forward definition.
  */
  class Mesh;
  class RandomGenerator;
  class RandomVertexSelector;


  /**
     @class MeshCPCNaive

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-12-08
     @brief Using Compound Poisson Cascades to generate procedural textures on a mesh
  */
  class MeshCPCNaive {
  private:
    /**
       @class Multiplier

       @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
       @date 2008-12-09
       @brief A description of the multipliers used here (on the normal of a vertex)
    */
    class Multiplier {
    private:
      /**
	 "altitude" of the multiplier on the normal of a vertex
      */
      long double r;
      /**
	 value of the multiplier
      */
      long double w;
    public:
      /**
	 Default constructor
      */
      Multiplier(long double r_r, long double w_w) : r(r_r), w(w_w) {}
      /**
	 Accessor of the "altitude" of the multiplier
      */
      inline const long double & getR() const { return r; }
      /**
	 Accessor of the multiplier value
      */
      inline const long double & getW() const { return w; }

      /**
	 sort operator based on the r value
      */
      inline bool operator < (const Multiplier & m) const {
	return r < m.r;
      }

      friend class MeshCPCNaive;
    };

    std::vector<std::vector<Multiplier> > multipliers;

    /**
       Mesh where the texture will be computed
    */
    Mesh & mesh;
    /**
       MeshMap describing the texture
    */
    MeshMap meshMap;

    /**
       The random number generator used for the multipliers' location
    */
    const RandomGenerator & rDistrib;

    /**
       The random number generator used for the multipliers' values (Wi)
    */
    const RandomGenerator & wDistrib;

    /**
       The random vertex generator
    */
    const RandomVertexSelector & vSelector;

    /**
       Generate the multipliers using the random tools
    */
    void generateMultipliers();

    /**
       Compute the texture according to the computed multipliers
    */
    void computeTexture();

    /**
       Number of multipliers on the whole surface
    */
    long unsigned int nbMultipliers;

    /**
       Divider coefficient to compute the final texture. Corresponds to l^H1.
    */
    long double coef;

  public:
    /**
       Constructor defined by a mesh and a probability distribution
    */
    MeshCPCNaive(Mesh & mesh,
		 const RandomGenerator & rDistrib,
		 const RandomVertexSelector & vSelector,
		 const RandomGenerator & wDistrib,
		 long unsigned int nbMultipliers,
		 long double coef);


    /**
       Access to the generated texture (described by the mesh map)
    */
    inline const MeshMap & getTexture() const { return meshMap; }
  };

}


#endif
