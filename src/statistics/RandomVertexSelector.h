/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef RANDOM_VERTEX_SELECTOR
#define RANDOM_VERTEX_SELECTOR
#include <vector>

#include "IDTypes.h"
#include "RandomGenerator.h"

namespace Taglut {
  /*
     For compilation speed, Mesh is not defined here using an include, but with a
     forward definition.
  */
  class Mesh;

  /**
     @class RandomVertexSelector

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-12-08
     @brief A virtual random vertex selector from a mesh
  */
  class RandomVertexSelector {
  public:
    /**
       A constructor using a mesh
    */
    RandomVertexSelector() {}

    /**
       Destructor
    */
    virtual ~RandomVertexSelector() {}

    /**
       The random vertex selector
    */
    virtual VertexID operator() () const = 0;
  };

  /**
     @class RandomVertexSelectorUniform

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-12-08
     @brief A uniform random vertex selector from a mesh
  */
  class RandomVertexSelectorUniform : public RandomVertexSelector {
  private:
    /**
       An uniform random number generator
    */
    RandomGeneratorUniform rGenerator;
  public:
    /**
       A constructor using a mesh
    */
    RandomVertexSelectorUniform(const Mesh & mesh);

    /**
       Destructor
    */
    ~RandomVertexSelectorUniform() {}

    /**
       The random vertex selector
    */
    VertexID operator() () const;
  };

  /**
     @class RandomVertexSelectorUniformSurface

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-12-08
     @brief A uniform random vertex selector from a mesh, using the surface arround each vertex
     to balance the uniform selection.
  */
  class RandomVertexSelectorUniformSurface : public RandomVertexSelector {
  private:
    std::vector<long double> cumulateAreas;
    /**
       An uniform random number generator
    */
    RandomGeneratorUniform rGenerator;
  public:
    /**
       A constructor using a mesh
    */
    RandomVertexSelectorUniformSurface(const Mesh & mesh);

    /**
       Destructor
    */
    ~RandomVertexSelectorUniformSurface() {}

    /**
       The random vertex selector
    */
    VertexID operator() () const;
  };

}

#endif
