#include "Mesh.h"
#include "RandomGenerator.h"
#ifdef USE_GSL
#include <gsl/gsl_rng.h>
#else
#include <stdlib.h>
#endif

using namespace Taglut;


#ifdef USE_GSL
gsl_rng * RandomGeneratorUniform::gBaseRand = NULL;
#else
bool RandomGeneratorUniform::finit = false;
#endif


double RandomGenerator::value() const {

  return (*this)();
}

void RandomGeneratorUniform::init(long double min, long double max) {
#ifdef USE_GSL
  if (gBaseRand == NULL) {
    gBaseRand = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(gBaseRand, time(NULL));
  }
#else
  if (!finit) {
    finit = true;
    srand(time(NULL));
  }
#endif
  umin = min;
  umax = max;
}


RandomGeneratorUniform::RandomGeneratorUniform(double min, double max) {
  init(min, max);
}
RandomGeneratorUniform::RandomGeneratorUniform(long double min, long double max) {
  init(min, max);
}

RandomGeneratorUniform::~RandomGeneratorUniform() {
#ifdef USE_GSL
  if (gBaseRand != NULL) {
    gsl_rng_free(gBaseRand);
    gBaseRand = NULL;
  }
#endif
}

long double RandomGeneratorUniform::operator() () const {
#ifdef USE_GSL
  return (long double)gsl_rng_get(gBaseRand) * (umax - umin) / (gsl_rng_max(gBaseRand) - gsl_rng_min(gBaseRand)) + umin;
#else
  return (long double)rand() * (umax - umin) / RAND_MAX + umin;
#endif
}

RandomGenerator1_z3::RandomGenerator1_z3(long double min_t, long double max_t) : min(min_t), max(max_t), randu((long double)0.0, (max_t * max_t) / (min_t * min_t)) {
}


long double RandomGenerator1_z3::operator() () const {
  /*ri=1./sqrt(1./sqr(rmin)-ui)
    with umax=1./sqr(rmin)-1.
    ui = umax * randuniform()
    where rmax = 1 */
  /*
    rmin = min / max
    rmax = max / max = 1
    the uniform generator has been initializated between 0.0 and (max * max) / (min * min)
  */
  const double rmin = min / max;
  const double _rmin2 = 1 / (rmin * rmin);

  return max / sqrt(_rmin2 - randu());
}

RandomGeneratorNormal::RandomGeneratorNormal() : randu(-1.0, 1.0), other(false) {
}

long double RandomGeneratorNormal::operator() () {
  // using the Box-Muller method
  // see http://en.wikipedia.org/wiki/Box-Muller_transform
  if (other) {
    other = false;
    return othervalue;
  }
  else {
    long double x1;
    long double x2;
    long double w;
    do {
      x1 = 2. * randu() - 1.;
      x2 = 2. * randu() - 1.;
      w = x1 * x1 + x2 * x2;
    } while (w >= 1.);

    w = sqrt((-2. * log(w)) / w);
    othervalue = x1 * w;
    other = true;
    return x2 * w;
  }
}

long double RandomGeneratorNormal::operator() () const {
  // using the regection sampling method
  // see http://en.wikipedia.org/wiki/Rejection_sampling
  // and http://fr.wikipedia.org/wiki/Méthode_de_Box-Muller
  long double x1;
  long double x2;
  long double w;
  do {
    x1 = randu();
    x2 = randu();
    w = x1 * x1 + x2 * x2;
  } while (w > 1.);

  w = sqrt((-2. * log(w)) / w);
  return x2 * w;
}

RandomGeneratorLogNormal::RandomGeneratorLogNormal(long double sigma, long double mu_t) : sqrtsigma(sqrt(sigma)), mu(mu_t),
											  randn() {
}

long double RandomGeneratorLogNormal::operator() () const {
  return exp(sqrtsigma * randn() + mu);
}

RandomGeneratorLogExponential::RandomGeneratorLogExponential(long double temp_t) : temp(temp_t), coef(pow(1. + temp_t, 1. / temp_t)),
										   randu(0.0, 1.0) {
}

long double RandomGeneratorLogExponential::operator() () const {
  return pow(coef * randu(), temp);
}

RandomGeneratorLogPoisson::RandomGeneratorLogPoisson(long double w0_t) : w0(w0_t) {
}

long double RandomGeneratorLogPoisson::operator() () const {
  return w0;
}
