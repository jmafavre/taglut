/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef RANDOM_GENERATOR
#define RANDOM_GENERATOR

#ifndef SWIG
#include <limits>

#ifdef USE_GSL
#include <gsl/gsl_rng.h>
#endif
#endif

namespace Taglut {
  /*
     For compilation speed, Mesh is not defined here using an include, but with a
     forward definition.
  */
  class Mesh;


  /**
     @class RandomGenerator

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief A virtual random number generator
  */
  class RandomGenerator {

  public:
    /**
       default constructor
    */
    RandomGenerator() {}
    /**
       Destructor
    */
    virtual ~RandomGenerator() { }

    /**
       generate a number according to the probability distribution of the current class
    */
#ifndef SWIG
    virtual long double operator() () const = 0;
#endif

    /**
       generate a number according to the probability distribution of the current class
    */
    double value() const;

    /**
       return the minimum value that can be generated
    */
    virtual long double getMin() const = 0;
    /**
       return the maximum value that can be generated
    */
    virtual long double getMax() const = 0;
  };

  /**
     @class RandomGeneratorUniform

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief An uniform random number generator
  */
  class RandomGeneratorUniform : public RandomGenerator {
  protected:
#ifndef SWIG
#ifdef USE_GSL
    /**
       The used generator (from the GSL)
    */
    static gsl_rng * gBaseRand;
#else
    /**
       A flag that describes the initialization of the seed when GSL is not available
     */
    static bool finit;
#endif

    /**
       minimum bound
    */
    long double umin;
    /**
       maximum bound
    */
    long double umax;

    void init(long double min, long double max);
#endif // SWIG
  public:

#ifndef SWIG
    /**
       default constructor that initialisate the uniform random generator
    */
    RandomGeneratorUniform(long double min = 0.0, long double max = 1.0);
#endif

    /**
       default constructor that initialisate the uniform random generator
    */
    RandomGeneratorUniform(double min, double max = 0.);

    /**
       Destructor
    */
    ~RandomGeneratorUniform();

    /**
       generate a number according to the probability distribution of the current class
    */
#ifndef SWIG
    long double operator() () const;
#endif

    /**
       return the minimum value that can be generated
    */
    inline long double getMin() const { return umin; }
    /**
       return the maximum value that can be generated
    */
    inline long double getMax() const { return umax; }

    /**
       set the minimum value that can be generated
    */
    inline void setMin(long double min) { umin = min; }
    /**
       set the maximum value that can be generated
    */
    inline void setMax(long double max) { umax = max; }

  };

  /**
     @class RandomGeneratorNormal

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief A standard normal random number generator
  */
  class RandomGeneratorNormal : public RandomGenerator {
  protected:
    /**
       An uniform random number generator
    */
    RandomGeneratorUniform randu;
    /**
       The method can generate two values at each call (with non const method)
    */
    long double othervalue;
    /**
       a boolean (true if another value is available)
    */
    bool other;
  public:
    /**
       default constructor
    */
    RandomGeneratorNormal();
    /**
       Destructor
    */
    ~RandomGeneratorNormal() {}

#ifndef SWIG
    /**
       generate a number according to the probability distribution of the current class
    */
    long double operator() () const;

    /**
       generate a number according to the probability distribution of the current class
       (two values are generated and the second one is saved for a next calling of the method)
    */
    long double operator() ();
#endif

    /**
       return the minimum value that can be generated
    */
    inline long double getMin() const { return 0.0; }
    /**
       return the maximum value that can be generated
    */
    inline long double getMax() const { return 1.0; }

  };


  /**
     @class RandomGenerator1_z3

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief A random number generator corresponding to the generator used in CPC method.
     The probability distribution is max^3/z^3, and only values contains in [min, max] are
     selected.
  */
  class RandomGenerator1_z3 : public RandomGenerator {
  private:
    /** minimum bound of the generator */
    long double min;
    /** maximum bound of the generator */
    long double max;
    /** a uniform random number generator */
    RandomGeneratorUniform randu;
  public:
    /**
       Constructor defining the min and max boundaries of the random generator
    */
    RandomGenerator1_z3(long double min, long double max);

    /**
       Destructor
    */
    ~RandomGenerator1_z3() {}

    /**
       generate a number according to the probability distribution of the current class
    */
#ifndef SWIG
    double long operator() () const;
#endif

    /**
       return the minimum value that can be generated
    */
    inline long double getMin() const { return min; }
    /**
       return the maximum value that can be generated
    */
    inline long double getMax() const { return max; }
  };


  /**
     @class RandomGeneratorLogNormal

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief An log normal random number generator
  */
  class RandomGeneratorLogNormal : public RandomGenerator {
  protected:
    /** the square root value of the given sigma */
    long double sqrtsigma;
    /** the given mu */
    long double mu;
    /**
       an normal random number generator
    */
    RandomGeneratorNormal randn;
  public:
    /**
       default constructor that initialisate the log normal random generator
    */
    RandomGeneratorLogNormal(long double sigma, long double mu);
    /**
       Destructor
    */
    ~RandomGeneratorLogNormal() { }

    /**
       generate a number according to the probability distribution of the current class
    */
    long double operator() () const;

    /**
       return the minimum value that can be generated
    */
#ifndef SWIG
    inline long double getMin() const { return 0.0; }
#endif

    /**
       return the maximum value that can be generated
    */
    inline long double getMax() const { return std::numeric_limits<long double>::max(); }
  };


  /**
     @class RandomGeneratorLogExponential

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief An log exponential random number generator
  */
  class RandomGeneratorLogExponential : public RandomGenerator {
  protected:
    /** a parameter of the random generator */
    long double temp;
    /** a coefficient computed using temp */
    long double coef;
    /** an uniform random number generator */
    RandomGeneratorUniform randu;
  public:
    /**
       default constructor that initialisate the log exponential random generator
    */
    RandomGeneratorLogExponential(long double temp);
    /**
       Destructor
    */
    ~RandomGeneratorLogExponential() {}

    /**
       generate a number according to the probability distribution of the current class
    */
#ifndef SWIG
    long double operator() () const;
#endif

    /**
       return the minimum value that can be generated
    */
    inline long double getMin() const { return 0.0; }
    /**
       return the maximum value that can be generated
    */
    inline long double getMax() const { return 1.0; }
  };

  /**
     @class RandomGeneratorLogPoisson

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief An log Poisson random number generator
  */
  class RandomGeneratorLogPoisson : public RandomGenerator {
  protected:
    /** a parameter of the random generator */
    long double w0;
    /** an uniform random number generator */
    RandomGeneratorUniform randu;
  public:
    /**
       default constructor that initialisate the log poisson random generator
    */
    RandomGeneratorLogPoisson(long double w0);
    /**
       Destructor
    */
    ~RandomGeneratorLogPoisson() {}

    /**
       generate a number according to the probability distribution of the current class
    */
#ifndef SWIG
    long double operator() () const;
#endif

    /**
       return the minimum value that can be generated
    */
    inline long double getMin() const { return w0; }
    /**
       return the maximum value that can be generated
    */
    inline long double getMax() const { return w0; }
  };


}

#endif
