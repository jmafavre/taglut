#include "RandomVertexSelector.h"
#include "Mesh.h"
using namespace Taglut;

RandomVertexSelectorUniform::RandomVertexSelectorUniform(const Mesh & mesh) : rGenerator(0.0, mesh.getNbPoints()) {

}

VertexID RandomVertexSelectorUniform::operator() () const {
  VertexID r = floor(rGenerator());
  if (r == rGenerator.getMax())
    r = rGenerator.getMax() - 1;
  return r;
}

RandomVertexSelectorUniformSurface::RandomVertexSelectorUniformSurface(const Mesh & mesh) : rGenerator() {
  double cArea = 0.0;
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p) {
    double localArea = 0.0;
    for(std::deque<TriangleID>::const_iterator t = (*p).getTriangles().begin(); t != (*p).getTriangles().end(); ++t)
      localArea += mesh.getArea(*t);
    localArea /= 3;
    cArea += localArea;
    cumulateAreas.push_back(cArea);
  }
  assert(fabs(cArea - mesh.getArea()) < 1e-7);
  rGenerator.setMax(cArea);
}

VertexID RandomVertexSelectorUniformSurface::operator() () const {
  long double r = rGenerator();

  if (r <= cumulateAreas[0])
    return 0;

  // using a binary search method to find the selected vertex
  VertexID start = 0;
  VertexID end = cumulateAreas.size() - 1;

  do {
    VertexID middle = VertexID(start + ceil(((double)end - start) / 2));
    if ((cumulateAreas[middle - 1] < r) && (r <= cumulateAreas[middle]))
      return middle;
    if (r < cumulateAreas[middle]) {
      end = middle;
    }
    else {
      start = middle;
    }
  } while (end >= start);

  return start;
}
