/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "CPMethod.h"
#include "Mapping2D3D.h"
#include "Mesh.h"
#include "Display3D.h"

#include "Messages.h"

using namespace Taglut;

static char*  filename  = NULL;
static int    sizex = 300;
static int    sizey = 300;
static int    threeD = 0;
static int    color = 0;
static char*  imagefilename = NULL;
static int    noCirclePacking = 0;
static double epsilon = 0.000000000001;
static int    maxIter = 40000;
static int    help = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input circle set file", NULL},
  { "x", 'x', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizex, 0, "window size (x)", NULL},
  { "y", 'y', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizey, 0, "window size (y)", NULL},
  { "3d", '3', POPT_ARG_NONE | POPT_ARGFLAG_SHOW_DEFAULT, &threeD, 0, "3d mesh", NULL},
  { "color", 'c', POPT_ARG_NONE | POPT_ARGFLAG_SHOW_DEFAULT, &color, 0, "Use colored primitives", NULL},
  { "texture", 't', POPT_ARG_STRING | POPT_ARGFLAG_SHOW_DEFAULT, &imagefilename, 0, "Image texture", NULL},
  { "no-circle-packing", 'n', POPT_ARG_NONE, &noCirclePacking, 0, "Not use circle packing algorithm after loading to correct round error...", NULL},
  { "epsilon", 'e', POPT_ARG_DOUBLE, &epsilon, 0, "Epsilon (used in circle packing algorithm)", NULL},
  { "maxIter", 'm', POPT_ARG_INT, &maxIter, 0, "Maximum iteration (used in circle packing algorithm)", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {

  poptContext context = poptGetContext("circleSetViewer", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Circle set viewer", "Display a circle set.");
    hMsg << "Input: circle set to display (available format: cset). Optional: image texture.";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((color != 0) && (imagefilename != NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: incompatible options (color, image)." << endl;
    return 1;
  }


  if ((sizex <= 0) || (sizey <= 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: 3d window size must be positive." << endl;
    return 1;
  }

  /* load circleset */
  cout << "Loading circleset..." << endl;
  Mesh mesh;
  Mapping2D3D mapping(mesh);
  CPMethod cset(mapping);

  try {
    cset.load(filename);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }

  if (noCirclePacking == 0) {
    cout << "Circle packing: unfolding..." << endl;
    /* unfold using circle packing */
    int nbIter = cset.processUnfolding(epsilon, maxIter, false);
    cout << " Iteration number: " << nbIter << endl;
  }

  try {
    if (imagefilename == NULL)
      Display3D::displayMapping(sizex,sizey, "3D Visualisation", mapping, threeD != 0, color == 0, false);
    else {
      cout << "Loading texture image..." << endl;
      CImg<unsigned char> imgTexture(imagefilename);

      Display3D::displayMapping(sizex,sizey, "3D Visualisation", mapping, imgTexture, threeD != 0);
    }
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }





  return 0;

}
