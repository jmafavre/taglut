/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MATRIX
#define MATRIX
#include "Coord3D.h"

namespace Taglut {

  /**
     @class Matrix3x3

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-12-21
     @brief 3x3 matrix with operators on Coord3D
  */

  class Matrix3x3 {
  protected:
    /**
       Values of the matrix
    */
    double matrix[3][3];

  public:
    /**
       Default constructor with 3 vectors
    */
    Matrix3x3(const Coord3D & c1, const Coord3D & c2, const Coord3D & c3);

    /**
       Default constructor with 3 vectors
    */
    Matrix3x3(double m00 = 0.0, double m10 = 0.0, double m20 = 0.0,
	      double m01 = 0.0, double m11 = 0.0, double m21 = 0.0,
	      double m02 = 0.0, double m12 = 0.0, double m22 = 0.0);

    /**
       Matricial product
     */
    inline Coord3D operator*(const Coord3D & v) const {
      return Coord3D(matrix[0][0] * v.getX() + matrix[1][0] * v.getY() + matrix[2][0] * v.getZ(),
		     matrix[0][1] * v.getX() + matrix[1][1] * v.getY() + matrix[2][1] * v.getZ(),
		     matrix[0][2] * v.getX() + matrix[1][2] * v.getY() + matrix[2][2] * v.getZ());
    }

    /**
       Matricial product
     */
    inline Matrix3x3 operator-(const Matrix3x3 & m) const {
      return Matrix3x3(matrix[0][0] - m.matrix[0][0], matrix[1][0] - m.matrix[1][0], matrix[2][0] - m.matrix[2][0],
		       matrix[0][1] - m.matrix[0][1], matrix[1][1] - m.matrix[1][1], matrix[2][1] - m.matrix[2][1],
		       matrix[0][2] - m.matrix[0][2], matrix[1][2] - m.matrix[1][2], matrix[2][2] - m.matrix[2][2]);
    }

    /**
       Matricial product
     */
    inline Matrix3x3 operator+(const Matrix3x3 & m) const {
      return Matrix3x3(matrix[0][0] + m.matrix[0][0], matrix[1][0] + m.matrix[1][0], matrix[2][0] + m.matrix[2][0],
		       matrix[0][1] + m.matrix[0][1], matrix[1][1] + m.matrix[1][1], matrix[2][1] + m.matrix[2][1],
		       matrix[0][2] + m.matrix[0][2], matrix[1][2] + m.matrix[1][2], matrix[2][2] + m.matrix[2][2]);
    }


    /**
       Construct a rotation matrix according to the given axis and angle
     */
    static Matrix3x3 getRotationMatrix(const Coord3D & axis, double angle);

    /**
       Transpose operator
    */
    inline Matrix3x3 t() const {
      return Matrix3x3(matrix[0][0], matrix[0][1], matrix[0][2],
		       matrix[1][0], matrix[1][1], matrix[1][2],
		       matrix[2][0], matrix[2][1], matrix[2][2]); 
    }

    /**
       Accessor
    */
    const double & operator() (unsigned int i, unsigned int j) const {
      if ((i >= 3) || (j >= 3))
	throw Exception("operator(2): wrong id.");
      return matrix[i][j];
    }

    /**
       Accessor
    */
    double & operator() (unsigned int i, unsigned int j) {
      if ((i >= 3) || (j >= 3))
	throw Exception("operator(2): wrong id.");
      return matrix[i][j];
    }
  };

  /**
     Stream operator
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Matrix3x3 & p) {
    f << "Matrix[" << p(0, 0) << " " << p(1, 0) << " " << p(2, 0) << "]" << std::endl
      << "      [" << p(0, 1) << " " << p(1, 1) << " " << p(2, 1) << "]" << std::endl
      << "      [" << p(0, 2) << " " << p(1, 2) << " " << p(2, 2) << "]";

    return f;
  }

}

#endif
