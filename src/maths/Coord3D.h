/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef COORD3D
#define COORD3D

#include <math.h>
#include <assert.h>
#include <deque>
#include <limits>



#include "Exception.h"


namespace Taglut {

  template<typename T>
  class Line3DT;

  template<typename T>
  class Plane3DT;

  template<typename T>
  class Triangle3DT;

  /**
     Available 3D directions (for point selection)
     DFront / DBack is the X axis
     DLeft / DRight is the Y axis
     DTop / DBottom is the Z axis
  */
  enum Direction { DNone, DTop, DBottom, DLeft, DRight, DFront, DBack };


  /**
     @class Coord3DT

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-12-21
     @brief Coords of R^3 (defined as template)
  */
  template <typename T>
  class Coord3DT {
  protected:
    /** X location */
    T x;
    /** Y location */
    T y;
    /** Z location */
    T z;

  public:

    /**
       Default constructor
    */
    Coord3DT() {
      x = y = z = 0;
    }

    /**
       Complete constructor
    */
    Coord3DT(T xl, T yl, T zl) : x(xl), y(yl), z(zl) {
    }

    /**
       copy constructor
    */
    Coord3DT(const Coord3DT<T> & c) : x(c.x), y(c.y), z(c.z) {
    }

    /** destructor */
    ~Coord3DT() { }

    /**
       Comparison operator
    */
    inline bool operator==(const Coord3DT<T> & c) const {
      return (x == c.x) && (y == c.y) && (z == c.z);
    }

    /**
       Multiply operator
    */
    inline Coord3DT<T> & operator*= (double a) {
      x *= a;
      y *= a;
      z *= a;

      return *this;
    }

    /**
       Divide operator
    */
    inline Coord3DT<T> & operator/= (double a) {
      if(a == 0)
	throw Exception("Division by zero");
      x /= a;
      y /= a;
      z /= a;

      return *this;
    }

    /**
       Add operator
    */
    inline Coord3DT<T> & operator+= (const Coord3DT<T> & p) {
      x += p.x;
      y += p.y;
      z += p.z;

      return *this;
    }

    /**
       Minus operator
    */
    inline Coord3DT<T> & operator-= (const Coord3DT<T> & p) {
      x -= p.x;
      y -= p.y;
      z -= p.z;

      return *this;
    }

    /**
       Minus operator
    */
    inline Coord3DT<T> operator- () const {
      Coord3DT<T> result;
      result.x = -x;
      result.y = -y;
      result.z = -z;
      return result;
    }

#ifndef SWIG
    /**
       Affectation operator
    */
    inline Coord3DT<T> & operator= (const Coord3DT<T> & p) {
      x = p.x;
      y = p.y;
      z = p.z;

      return *this;
    }
#endif

    /** Accessor */
    inline T & operator() (unsigned int id) {
      if (id == 0) return x;
      if (id == 1) return y;
      if (id == 2) return z;
      throw Exception("Coord3DT::operator()(1): bad id value.");
    }

#ifndef SWIG
    /** Accessor */
    inline T & operator[] (unsigned int id) {
      return (*this)(id);
    }
#endif

    /** Accessor */
    inline const T & operator() (unsigned int id) const {
      if (id == 0) return x;
      if (id == 1) return y;
      if (id == 2) return z;
      throw Exception("Coord3DT::operator()(1): bad id value.");
    }

#ifndef SWIG
    /** Accessor */
    inline const T & operator[] (unsigned int id) const {
      return (*this)(id);
    }
#endif

    /**
       Return x location
    */
    inline T getX() const { return x; }
    /**
       Return y location
    */
    inline T getY() const { return y; }

    /**
       Return z location
    */
    inline T getZ() const { return z; }

    /**
       return sum of coordinates components
    */
    inline T getSum() const { return x + y + z; }

    /**
       Fill coords using (x, y, z) values
    */
    inline void fill(T xl, T yl, T zl) {
      (*this).x = xl;
      (*this).y = yl;
      (*this).z = zl;
    }

    /* **** modifiers */
    /**
       Set x location value
    */
    inline void setX(T v) { x = v; }
    /**
       Set y location value
    */
    inline void setY(T v) { y = v; }
    /**
       Set z location value
    */
    inline void setZ(T v) { z = v; }

    /**
       Set location
    */
    inline void setCoords(T v1, T v2, T v3) {
      x = v1;
      y = v2;
      z = v3;
    }

    /**
       Set location
    */
    inline void setCoords(const Coord3DT<T> & c) {
      x = c.x;
      y = c.y;
      z = c.z;
    }

    /* maths tools */

    /**
       Euclidean distance between current point and p
    */
    inline double distance(const Coord3DT<T> & p) const {
      const double xx = p.x - (*this).x;
      const double yy = p.y - (*this).y;
      const double zz = p.z - (*this).z;
      return sqrt(xx * xx + yy * yy + zz * zz);
    }

    /**
       Euclidean squared distance between current point and p
    */
    inline double distance2(const Coord3DT<T> & p) const {
      const double xx = p.x - (*this).x;
      const double yy = p.y - (*this).y;
      const double zz = p.z - (*this).z;
      return xx * xx + yy * yy + zz * zz;
    }

#ifndef SWIG
    /**
       Euclidean distance betwen current point and (px, py, pz)
    */
    template <typename S>
    inline double distance(S px, S py, S pz) const {
      return sqrt((px - (*this).x) * (px - (*this).x) +
		  (py - (*this).y) * (py - (*this).y) +
		  (pz - (*this).z) * (pz - (*this).z));
    }
#endif

    /** compute the Hausdorff distance between two list of points */
    template <class S>
    static double hausdorffDistance(const S & l1, const S & l2) {
      const double d1 = orientedDistance(l1, l2);
      const double d2 = orientedDistance(l2, l1);
      if (d1 > d2)
	return d1;
      else
	return d2;
    }

    /** compute an oriented distance between two list of points */
    template <class S>
    static double orientedDistance(const S & l1, const S & l2) {
      double d = 0.;
      for(typename S::const_iterator p = l1.begin(); p != l1.end(); ++p) {
	const double dd = (*p).distanceToList(l2);
	if (dd > d)
	  d = dd;
      }
      return d;
    }

    /** compute distance between the current point and a list of points */
    template <class S>
    double distanceToList(const S & l) const {
      double d = std::numeric_limits<double>::max();
      for(typename S::const_iterator p = l.begin(); p != l.end(); ++p) {
	const double dd = distance(*p);
	if (dd < d)
	  d = dd;
      }
      return d;
    }

    /** compute angle defined by ((this, p1), (this, p2) */
    inline double angle(const Coord3DT<T> & p1, const Coord3DT<T> & p2) const {
      Coord3DT<double> n1(p1.x - (*this).x, p1.y - (*this).y, p1.z - (*this).z);
      Coord3DT<double> n2(p2.x - (*this).x, p2.y - (*this).y, p2.z - (*this).z);

      return angleVector(n1, n2);
    }

    /** compute cotangent of the angle defined by ((this, p1), (this, p2) */
    inline double cotanAngle(const Coord3DT<T> & p1, const Coord3DT<T> & p2) const {
      const double a = (*this).distance(p1);
      const double b = (*this).distance(p2);
      const double c = p1.distance(p2);
      const double R = (a * a + b * b - c * c) / (2 * a * b);

      return R / sqrt(1 - R * R);
    }

    /** compute angle defined by the two given vectors */
    static inline double angleVector(const Coord3DT<T> & n1, const Coord3DT<T> & n2) {
      // then compute normal's angles
      const double value = (n1.x * n2.x + n1.y * n2.y + n1.z * n2.z) /
	sqrt((n1.x * n1.x + n1.y * n1.y + n1.z * n1.z) *
	     (n2.x * n2.x + n2.y * n2.y + n2.z * n2.z));

      if (value > 1) return 0.0;
      if (value < -1) return M_PI;
      return acos(value);
    }

    /**
       Return true if the current point is inside box defined by point1, point2
    */
    inline bool isInside(const Coord3DT<T> & point1, const Coord3DT<T> & point2) {
      return(((*this).x >= point1.x) && ((*this).y >= point1.y) && ((*this).z >= point1.z) &&
	     ((*this).x <= point2.x) && ((*this).y <= point2.y) && ((*this).z <= point2.z));
    }

#ifndef SWIG
    /** return true if the current point and the given point has same location */
    inline bool sameLocation(const Coord3DT<T> & point) const {
      return sameLocation(*this, point);
    }
#endif

    /** return true if given point has same location */
    inline static bool sameLocation(const Coord3DT<T> & point1, const Coord3DT<T> & point2) {
      return ((point1.x == point2.x) && (point1.y == point2.y) && (point1.z == point2.z));
    }

    /**
       Return norm of the coord seen as a vector
    */
    inline double norm() const {
      return sqrt(x * x + y * y + z * z);
    }

    /**
       normalize the current vector
    */
    inline Coord3DT<T> & normalize() {
      double value = norm();

      if (value != 0) {
	x /= value;
	y /= value;
	z /= value;
      }

      return *this;
    }

    /**
       get a normalized version of the current vector
    */
    inline Coord3DT<T> getNormalize() const {
      Coord3DT<T> p(*this);
      return p.normalize();
    }


    /**
       normalize the current vector (according to the sum of the components)
    */
    inline Coord3DT<T> & normalize_sum() {
      double value = x + y + z;

      if (value != 0) {
	x /= value;
	y /= value;
	z /= value;
      }

      return *this;
    }

    /**
       Vectorial product
    */
    static Coord3DT<T> vectorialProduct(const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
      return Coord3DT<T>(p1.y * p2.z - p1.z * p2.y,
			 p1.z * p2.x - p1.x * p2.z,
			 p1.x * p2.y - p1.y * p2.x);

    }

    /**
       Scalar product
    */
    static double scalarProduct(const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
      return p1.x * p2.x + p1.y * p2.y + p1.z * p2.z;
    }

    /**
       Compute the area of the triangle defined by 3 3D points
    */
    inline static double computeTriangleArea(const Coord3DT<T> & p1, const Coord3DT<T> & p2, const Coord3DT<T> & p3) {
      return 0.5 * vectorialProduct(p2 - p1, p3 - p1).norm();
    }

    /**
       Output method for python binding
    */
    char * __STR__() {
      static char tmp[1024];
      sprintf(tmp,"Coord3D(%f, %f, %f)", x, y, z);
      return tmp;
    }

    /**
       Compute ortogonal projection of the current point on the axis defined by a point and a vector.
    */
    Coord3DT<T> projectionOnAxis(const Line3DT<T> & line) const {
      const Coord3DT<T> n = line.getVector().getNormalize();
      return line.getPoint() + n * scalarProduct((*this) - line.getPoint(), n);
    }

    /**
       Compute distance to a given line
    */
    inline double distance(const Line3DT<T> & line) const {
      const Coord3DT<T> n = line.getVector().getNormalize();
      const double d1 = line.getPoint().distance(*this);
      const double d2 = scalarProduct((*this) - line.getPoint(), n);
      return sqrt(d1 * d1 - d2 * d2);
    }

    /**
       assuming that the current point is in the edge (p1, p2), compute its coordinates on the edge.
       Between 0 and 1: inside the edge...
    */
    inline double getLocationOnEdge(const Coord3DT<T> & p1, const Coord3DT<T> & p2) const {
      const double d1 = fabs(p1.getX() - p2.getX());
      const double d2 = fabs(p1.getY() - p2.getY());
      const double d3 = fabs(p1.getZ() - p2.getZ());

      if ((d1 > d2) && (d1 > d3))
	return (getX() - p1.getX()) / (p2.getX() - p1.getX());
      else if ((d2 > d1) && (d2 > d3))
	return (getY() - p1.getY()) / (p2.getY() - p1.getY());
      else
	return (getZ() - p1.getZ()) / (p2.getZ() - p1.getZ());
    }

#ifndef SWIG
    struct ltCoord {
      bool operator() (const Coord3DT<T> & c1, const Coord3DT<T> & c2) const {
	return ((c1.getX() < c2.getX()) ||
		((c1.getX() == c2.getX()) && ((c1.getY() < c2.getY()) ||((c1.getY() == c2.getY()) && (c1.getZ() < c2.getZ())))));
      }
    };
#endif

  };

  /**
     Define an alias to Coord3DT with double
  */
  typedef Coord3DT<double> Coord3D;



  /**
     @class Edge3DT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-03-06
     @brief A 3D edge defined by points (standelone version, without Mesh reference)
  */
  template<typename T>
  class Edge3DT {
  private:
    Coord3DT<T> p1;
    Coord3DT<T> p2;
  public:

    /**
       Constructor
    */
    Edge3DT(const Coord3DT<T> & p1l, const Coord3DT<T> & p2l) : p1(p1l), p2(p2l) { }

    /** point accessor */
    Coord3DT<T> & getP1() { return p1; }
    /** point accessor */
    Coord3DT<T> & getP2() { return p2; }

    /** point accessor */
    const Coord3DT<T> & getP1() const { return p1; }
    /** point accessor */
    const Coord3DT<T> & getP2() const { return p2; }

    /** point modifier */
    void setP1(const Coord3DT<T> & p) { p1 = p; }
    /** point modifier */
    void setP2(const Coord3DT<T> & p) { p2 = p; }

    /** return true if the given point is on the edge, with a distance < \p epsilon * (distance between p1 and p2) */
    bool contains(const Coord3DT<T> & p, double epsilon = 1e-5) const;
  };


  /**
     @class Line3DT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2007-12-21
     @brief A 3d line (defined by a point and a vector)
  */
  template<typename T>
  class Line3DT {
  private:
    Coord3DT<T> point;
    Coord3DT<T> vector;

  public:

    /**
       Default constructor
    */
    Line3DT() : point(), vector() { }

    /**
       A line is defined by a point and a vector
    */
    Line3DT(const Coord3DT<T> & point_l, const Coord3DT<T> & vector_l) : point(point_l), vector(vector_l) { }

    /**
       A line is defined by an edge
    */
    Line3DT(const Edge3DT<T> & edge) : point(edge.getP1()), vector(edge.getP2() - edge.getP1()) { }

    /**
       A line is defined by a point and a direction
    */
    Line3DT(const Coord3DT<T> & point_l, const enum Direction & direction) : point(point_l) {
      if (direction == DFront)
	vector = Coord3DT<T> (1, 0, 0);
      else if (direction == DBack)
	vector = Coord3DT<T> (-1, 0, 0);
      else if (direction == DLeft)
	vector = Coord3DT<T> (0, 1, 0);
      else if (direction == DRight)
	vector = Coord3DT<T> (0, -1, 0);
      else if (direction == DTop)
	vector = Coord3DT<T> (0, 0, 1);
      else if (direction == DBottom)
	vector = Coord3DT<T> (0, 0, -1);
      else // default direction
	vector = Coord3DT<T> (1, 0, 0);
    }

    /**
       copy constructor
    */
    Line3DT(const Line3DT<T> & line) : point(line.point), vector(line.vector) { }

    /**
       Point accessor
    */
    const Coord3DT<T> & getPoint() const {
      return point;
    }

    /**
       Vector accessor
    */
    const Coord3DT<T> & getVector() const {
      return vector;
    }

    /**
       Assums that p is a point of the current line. Return the scalar value alpha
       verifying p = alpha * vector + point
    */
    double getCoords(const Coord3DT<T> & p) const {
      if (fabs(vector.getX()) >  1e-6)
	return (p.getX() - point.getX()) / vector.getX();
      if (fabs(vector.getY()) >  1e-6)
	return (p.getY() - point.getY()) / vector.getY();
      if (fabs(vector.getZ()) >  1e-6)
	return (p.getZ() - point.getZ()) / vector.getZ();
      else
	return 0.0;
    }

    /**
       Return intersection between the current line and the given plane
    */
    Coord3DT<T> getIntersection(const Plane3DT<T> & plane) const {
      assert(!isParallel(plane));
      double d = - Coord3DT<T>::scalarProduct(plane.getVector(), plane.getPoint());
      double alpha = - ((plane.getVector() * point).getSum() + d) / Coord3DT<T>::scalarProduct(plane.getVector(), vector);

      return point + alpha * vector;
    }

    /**
       Return true if the current line is parallel to the given plane
    */
    bool isParallel(const Plane3DT<T> & plane) const {
      static double epsilon = 1e-7;
      double angle = Coord3DT<T>().angle(plane.getVector(), vector);

      return (angle < M_PI / 2 + epsilon) && (angle > M_PI / 2 - epsilon);
    }

    /**
       Compute distance to a given point
    */
    double distance(const Coord3DT<T> & point_l) const {
      return point_l.distance(point_l.projectionOnAxis(*this));
    }

    /**
       Compute intersection between two lines, assuming that they are in the same plane.
       It can throws an exception if the two lines are parallel.
       The result is the scalar coordinate in the current line.
     */
    double getIntersectionLocation(const Line3DT<T> & line) const {
      // select the best lines to compute the values
      unsigned char v1 = 0;
      unsigned char v2 = 0;
      double div = 0.;
      for(unsigned char i = 0; i < 3; ++i)
	for(unsigned char j = 0; j < 3; ++j) {
	  const double d2 = fabs(line.getVector()[j] * (line.getVector()[j] * vector[i] - line.getVector()[i] * vector[j]));
	  if (d2 > div) {
	    div = d2;
	    v1 = i;
	    v2 = j;
	  }
	}

      if ((line.getVector()[v2] == 0.) || ((line.getVector()[v2] * vector[v1] - line.getVector()[v1] * vector[v2]) == 0.))
	throw Exception("getIntersection(): the two lines are parallel");

      // compute the scalar value on the current line
      return (line.getVector()[v2] *
	      (line.getPoint()[v1] + line.getVector()[v1] * (point[v2] - line.getPoint()[v2]) / line.getVector()[v2] - point[v1]) /
	      (line.getVector()[v2] * vector[v1] - line.getVector()[v1] * vector[v2]));
    }

    /**
       Compute intersection between two lines, assuming that they are in the same plane.
       It can throws an exception if the two lines are parallel.
     */
    inline Coord3D getIntersection(const Line3DT<T> & line) const {
      return point + vector * getIntersectionLocation(line);
    }

  };


  /**
     @class Triangle3DT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-03-06
     @brief A 3D triangle defined by points (standelone version, without Mesh reference)
  */
  template<typename T>
  class Triangle3DT {
  private:
    Coord3DT<T> p1;
    Coord3DT<T> p2;
    Coord3DT<T> p3;
  public:
    /** default constructor */
    Triangle3DT() { }

    /**
       Constructor
    */
    Triangle3DT(const Coord3DT<T> & p1_l, const Coord3DT<T> & p2_l, const Coord3DT<T> & p3_l) : p1(p1_l), p2(p2_l), p3(p3_l) { }

    /** point accessor */
    Coord3DT<T> & getP1() { return p1; }
    /** point accessor */
    Coord3DT<T> & getP2() { return p2; }
    /** point accessor */
    Coord3DT<T> & getP3() { return p3; }

    /** point accessor */
    const Coord3DT<T> & getP1() const { return p1; }
    /** point accessor */
    const Coord3DT<T> & getP2() const { return p2; }
    /** point accessor */
    const Coord3DT<T> & getP3() const { return p3; }

    /** point modifier */
    void setP1(const Coord3DT<T> & p) { p1 = p; }
    /** point modifier */
    void setP2(const Coord3DT<T> & p) { p2 = p; }
    /** point modifier */
    void setP3(const Coord3DT<T> & p) { p3 = p; }

    /** point accessor */
    Coord3DT<T> & getPoint(unsigned int i) {
      switch (i % 3) {
      case 0: return p1;
      case 1: return p2;
      case 2: default: return p3;
      }
    }

    /** point accessor */
    const Coord3DT<T> & getPoint(unsigned int i) const {
      switch (i % 3) {
      case 0: return p1;
      case 1: return p2;
      case 2: default: return p3;
      }
    }

    /** edge accessor */
    Edge3DT<T> getEdge1() const { return Edge3DT<T>(p1, p2); }

    /** edge accessor */
    Edge3DT<T> getEdge2() const { return Edge3DT<T>(p2, p3); }

    /** edge accessor */
    Edge3DT<T> getEdge3() const { return Edge3DT<T>(p3, p1); }

    /** compute area of the triangle using the vectorial product */
    double area() const {
      return 0.5 * Coord3D::vectorialProduct(p2 - p1, p3 - p1).norm();
    }

    /**
       Return true if the given line intersects the current triangle
    */
    bool intersect(const Line3DT<T> & line) const {
      Plane3DT<T> plane(*this);
      if (plane.isParallel(line))
	return false;
      else
	return isInside(plane.getIntersection(line));
    }


    /**
       Return true if the given edge intersects the current triangle
    */
    bool intersect(const Edge3DT<T> & edge) const {
      Line3DT<T> line(edge);
      Plane3DT<T> plane(*this);
      if (plane.isParallel(line))
	return false;
      else {
	Coord3DT<T> intersection = plane.getIntersection(line);
	if (isInside(intersection)) {
	  double p1Coords = line.getCoords(edge.getP1());
	  double p2Coords = line.getCoords(edge.getP2());
	  double interCoords = line.getCoords(intersection);
	  return (((p1Coords <= interCoords) && (interCoords <= p2Coords)) ||
		  ((p2Coords <= interCoords) && (interCoords <= p1Coords)));
	}
	else
	  return false;
      }
    }

    /**
       Return true if the given point is inside the current triangle
       Assums that the point is on the plane defined by the triangle
    */
    bool isInside(const Coord3DT<T> & coord) const {
      /* see http://www.blackpawn.com/texts/pointinpoly/default.html */
      return ((coord.distance(p1) < 1e-7) || (coord.distance(p2) < 1e-7) || (coord.distance(p3) < 1e-7) ||
	      ((Coord3DT<T>::scalarProduct(Coord3D::vectorialProduct(p2 - p1, coord - p1), Coord3DT<T>::vectorialProduct(coord - p1, p3 - p1)) >= 0) &&
	       (Coord3DT<T>::scalarProduct(Coord3D::vectorialProduct(p1 - p2, coord - p2), Coord3DT<T>::vectorialProduct(coord - p2, p3 - p2)) >= 0) &&
	       (Coord3DT<T>::scalarProduct(Coord3D::vectorialProduct(p1 - p3, coord - p3), Coord3DT<T>::vectorialProduct(coord - p3, p2 - p3)) >= 0)));
    }

    /**
       Return a normal vector to the current triangle
    */
    inline Coord3DT<T> getNormal() const {
      return Coord3DT<T>((p2.getY() - p1.getY()) * (p3.getZ() - p1.getZ()) -
			 (p2.getZ() - p1.getZ()) * (p3.getY() - p1.getY()),
			 (p2.getZ() - p1.getZ()) * (p3.getX() - p1.getX()) -
			 (p2.getX() - p1.getX()) * (p3.getZ() - p1.getZ()),
			 (p2.getX() - p1.getX()) * (p3.getY() - p1.getY()) -
			 (p2.getY() - p1.getY()) * (p3.getX() - p1.getX()));
    }

  };


  /**
     @class Plane3DT

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-12-21
     @brief A 3d plane (defined by a point and the normal vector)
  */
  template<typename T>
  class Plane3DT {
  private:
    Coord3DT<T> point;
    Coord3DT<T> vector;

  public:

    /**
       Default constructor
    */
    Plane3DT() : point(), vector() { }

    /**
       A plane is defined by a point and a vector
    */
    Plane3DT(const Coord3DT<T> & point_l, const Coord3DT<T> & vector_l) : point(point_l), vector(vector_l) { }

    /**
       copy constructor
    */
    Plane3DT(const Plane3DT<T> & plane) : point(plane.point), vector(plane.vector) { }

    /**
       a plane defined by a triangle
    */
    Plane3DT(const Triangle3DT<T> & triangle) : point(triangle.getP1()), vector(triangle.getNormal()) { }

#ifndef SWIG
    /** copy operator */
    Plane3DT & operator=(const Plane3DT<T> & plane) {
      point = plane.point;
      vector = plane.vector;
      return *this;
    }
#endif

    /**
       Point accessor
    */
    const Coord3DT<T> & getPoint() const {
      return point;
    }

    /**
       Vector accessor
    */
    const Coord3DT<T> & getVector() const {
      return vector;
    }

    /** translate the plane using the vector direction and the
	given distance */
    Plane3DT<T> & translate(double d) {
      point += vector.getNormalize() * d;
      return *this;
    }

    /** get a new plane translated from the current one using the vector direction and the
	given distance */
    Plane3DT<T> getTranslate(double d) const {
      Plane3DT<T> result(*this);
      return result.translate(d);
    }

    /**
       Return intersection between the current plane and the given line
    */
    Coord3DT<T> getIntersection(const Line3DT<T> & line) const {
      return line.getIntersection(*this);
    }

    /**
       Return true if the current plane is parallel to the given line
    */
    inline bool isParallel(const Line3DT<T> & line) const {
      return line.isParallel(*this);
    }

    /**
       return true if the two given planes are parallel
     */
    inline bool isParallel(const Plane3DT<T> & plane) const {
      const Coord3D v1(vector.getNormalize());
      const Coord3D v2(plane.vector.getNormalize());
      return (v1 == v2) || (v1 == -v2);
    }

    /**
       Return the distance between the current plane and the given point.
    */
    inline double distance(const Coord3DT<T> & p) const {
      return fabs(Coord3DT<T>::scalarProduct(vector, p - point)) / vector.norm();
    }

    /**
       Return the signed distance between the current plane and the given point (the sign define the side).
    */
    inline double signedDistance(const Coord3DT<T> & p) const {
      return Coord3DT<T>::scalarProduct(vector, p - point) / vector.norm();
    }

    /**
       Get the orthogonal projection of the given point on the plane
     */
    inline const Coord3DT<T> getOrthogonalProjection(const Coord3DT<T> & p) const {
      Line3DT<T> line(p, vector);
      return line.getIntersection(*this);
    }

    /**
       return true if the two given points are in the same side of the plane
    */
    inline bool sameSide(const Coord3DT<T> & p1, const Coord3DT<T> & p2) const {
      return Coord3DT<T>::scalarProduct(vector, p1 - getOrthogonalProjection(p1)) * Coord3DT<T>::scalarProduct(vector, p2 - getOrthogonalProjection(p2)) >= 0;
    }

    /**
       return true if the current plane contains the given point
     */
    inline bool contains(const Coord3DT<T> & p) const {
      Coord3DT<T> pp = p - point;
      return pp.getX() * vector.getX() + pp.getY() * vector.getY() + pp.getZ() * vector.getZ() == 0.;
    }

    /** return true if the current face is contained in the plane */
    inline bool contains(const Triangle3DT<T> & t) const {
      return contains(t.getP1()) && contains(t.getP2()) && contains(t.getP3());
    }

    /**
       return true if the given triangle intersect the current plane
    */
    inline bool intersect(const Triangle3DT<T> & p) const {
      return (contains(p.getP1()) || contains(p.getP2()) || contains(p.getP3()) ||
	      !sameSide(p.getP1(), p.getP2()) || !sameSide(p.getP1(), p.getP3()) || !sameSide(p.getP3(), p.getP2()));
    }

    /** if the two planes are not parallel, it return the intersection line */
    Line3DT<T> getIntersection(const Plane3DT<T> & p) const {
      if (isParallel(p))
	throw Exception("getIntersection(1): parallel planes");
      const Coord3DT<T> v(Coord3DT<T>::vectorialProduct(vector, p.vector));
      const Coord3DT<T> v2(Coord3DT<T>::vectorialProduct(vector, v));
      const Coord3DT<T> pt = p.getIntersection(Line3DT<T>(point, v2));
      return Line3DT<T>(pt, v);
    }

    Coord3DT<T> getIntersection(const Plane3DT<T> & p1, const Plane3DT<T> & p2) const {
      const Line3DT<T> l = p1.getIntersection(p2);
      if (isParallel(l))
	throw Exception("getIntersection(2): parallel planes");
      return getIntersection(l);
    }
  };



  /**
     @class Box3DT

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-12-21
     @brief A 3D box defined by points
  */
  template<typename T>
  class Box3DT {
  protected:
    /** Min X coord */
    T minX;
    /** Max X coord */
    T maxX;
    /** Min Y coord */
    T minY;
    /** Max Y coord */
    T maxY;
    /** Min Z coord */
    T minZ;
    /** Max Z coord */
    T maxZ;

  public:
    /**
       Default constructor
    */
    Box3DT() : minX(0), maxX(0), minY(0), maxY(0), minZ(0), maxZ(0) { }

    /**
       Copy constructor
    */
    template <typename S>
    Box3DT(const Box3DT<S> & box) : minX(box.minX), maxX(box.maxX), minY(box.minY), maxY(box.maxY), minZ(box.minZ), maxZ(box.maxZ) { }

    /**
       Constructor with default values
    */
    Box3DT(T minx, T maxx, T miny, T maxy, T minz, T maxz) : minX(minx), maxX(maxx), minY(miny), maxY(maxy), minZ(minz), maxZ(maxz) { }

    /**
       Build a bounding box for the given points
    */
    Box3DT(const std::deque<Coord3D> & points) {
      if (points.size() == 0)
	minX = maxX = minY = maxY = minZ = maxZ = 0;
      else {
	/* initialization */
	minX = maxX = points.front().getX();
	minY = maxY = points.front().getY();
	minZ = maxZ = points.front().getZ();

	/* adding all the other points */
	for(std::deque<Coord3D>::const_iterator p = points.begin(); p != points.end(); ++p)
	  addPoint(*p);
      }
    }


    /**
       Build a bounding box of a triangle
    */
    Box3DT(const Triangle3DT<T> & triangle) {
      /* initialization */
      minX = maxX = triangle.getP1().getX();
      minY = maxY = triangle.getP1().getY();
      minZ = maxZ = triangle.getP1().getZ();

      /* adding all the other points */
      addPoint(triangle.getP2());
      addPoint(triangle.getP3());

    }

    /** constructor with two points */
    Box3DT(const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
      if (p1.getX() < p2.getX()) {
	minX = p1.getX();
	maxX = p2.getX();
      }
      else {
	maxX = p1.getX();
	minX = p2.getX();
      }
      if (p1.getY() < p2.getY()) {
	minY = p1.getY();
	maxY = p2.getY();
      }
      else {
	maxY = p1.getY();
	minY = p2.getY();
      }
      if (p1.getZ() < p2.getZ()) {
	minZ = p1.getZ();
	maxZ = p2.getZ();
      }
      else {
	maxZ = p1.getZ();
	minZ = p2.getZ();
      }
    }

    /** add a point inside the current box: resize-it if needed*/
    void addPoint(const Coord3DT<T> & p) {
      if (minX > p.getX()) minX = p.getX();
      if (maxX < p.getX()) maxX = p.getX();
      if (minY > p.getY()) minY = p.getY();
      if (maxY < p.getY()) maxY = p.getY();
      if (minZ > p.getZ()) minZ = p.getZ();
      if (maxZ < p.getZ()) maxZ = p.getZ();
    }

    /**
       return true if the given point is inside the 3D box
    */
    bool contains(const Coord3DT<T> & p, double e = 1e-7) const {
      return ((minX - e <= p.getX()) && (maxX + e >= p.getX()) &&
	      (minY - e <= p.getY()) && (maxY + e >= p.getY()) &&
	      (minZ - e <= p.getZ()) && (maxZ + e >= p.getZ()));
    }

    /**
       check the intersection between the current box and a triangle
    */
    bool intersect(const Triangle3DT<T> & triangle) const {
      Box3DT<T> box(triangle);

      // first check intersection of the bounding box and the current box
      if (contains(box))
	return true;
      if (!intersect(box))
	return false;

      // then for each edge, compute intersection with the current box
      if (intersect(triangle.getEdge1()) ||
	  intersect(triangle.getEdge2()) ||
	  intersect(triangle.getEdge3()))
	return true;

      // then check each of the diagonal edges
      return (triangle.intersect(Edge3DT<T>(Coord3DT<T>(minX, minY, minZ), Coord3DT<T>(maxX, maxY, maxZ))) ||
	      triangle.intersect(Edge3DT<T>(Coord3DT<T>(minX, maxY, minZ), Coord3DT<T>(maxX, minY, maxZ))) ||
	      triangle.intersect(Edge3DT<T>(Coord3DT<T>(minX, minY, maxZ), Coord3DT<T>(maxX, maxY, minZ))) ||
	      triangle.intersect(Edge3DT<T>(Coord3DT<T>(minX, maxY, maxZ), Coord3DT<T>(maxX, minY, minZ))));
    }

    /**
       check the intersection between the current box and the given \p box
    */
    bool intersect(const Box3DT<T> & box) const {
      return !((minX > box.getMaxX()) || (maxX < box.getMinX()) ||
	       (minY > box.getMaxY()) || (maxY < box.getMinY()) ||
	       (minZ > box.getMaxZ()) || (maxZ < box.getMinZ()));
    }

    /**
       check the intersection between the current box and the given \p edge
    */
    bool intersect(const Edge3DT<T> & edge) const {
      for(unsigned int i = 0; i < 6; ++i)
	if (intersectFace(i, edge))
	  return true;
      return false;
    }

    /**
       check the intersection between the face \p i of the current box and the given \p edge
    */
    bool intersectFace(unsigned int i, const Edge3DT<T> & edge) const {
      Line3DT<T> line(edge);
      Coord3DT<T> point;
      Coord3DT<T> vector;
      if (i < 3)
	point = Coord3DT<T>(minX, minY, minZ);
      else
	point = Coord3DT<T>(maxX, maxY, maxZ);

      if (i % 3 == 0)
	vector = Coord3DT<T>(1, 0, 0);
      else if (i % 3 == 1)
	vector = Coord3DT<T>(0, 1, 0);
      else // i % 3 == 2
	vector = Coord3DT<T>(0, 0, 1);

      Plane3DT<T> plane (point, vector);

      if (line.isParallel(plane))
	return false;
      else {
	Coord3DT<T> intersection = line.getIntersection(plane);
	if (i % 3 == 0)
	  return ((minY <= intersection.getY()) && (intersection.getY() <= maxY) &&
		  (minZ <= intersection.getZ()) && (intersection.getZ() <= maxZ));
	else if (i % 3 == 1)
	  return ((minX <= intersection.getX()) && (intersection.getX() <= maxX) &&
		  (minZ <= intersection.getZ()) && (intersection.getZ() <= maxZ));
	else // i % 3 == 2
	  return ((minY <= intersection.getY()) && (intersection.getY() <= maxY) &&
		  (minX <= intersection.getX()) && (intersection.getX() <= maxX));
      }
    }

    /**
       Return true if the current box contains the given \p box
    */
    bool contains(const Box3DT<T> & box) const {
      return ((minX <= box.getMinX()) && (maxX >= box.getMaxX()) &&
	      (minY <= box.getMinY()) && (maxY >= box.getMaxY()) &&
	      (minZ <= box.getMinZ()) && (maxZ >= box.getMaxZ()));
    }

    /** return true if the given plane intersects the current box */
    bool intersect(const Plane3DT<T> & plane) const {
      Coord3DT<T> p111(minX, minY, minZ);
      Coord3DT<T> p112(minX, minY, maxZ);
      Coord3DT<T> p121(minX, maxY, minZ);
      Coord3DT<T> p122(minX, maxY, maxZ);
      Coord3DT<T> p211(maxX, minY, minZ);
      Coord3DT<T> p212(maxX, minY, maxZ);
      Coord3DT<T> p221(maxX, maxY, minZ);
      Coord3DT<T> p222(maxX, maxY, maxZ);

      return (!plane.sameSide(p111, p112) ||
	      !plane.sameSide(p111, p112) ||
	      !plane.sameSide(p122, p112) ||
	      !plane.sameSide(p122, p121) ||
	      !plane.sameSide(p211, p212) ||
	      !plane.sameSide(p211, p212) ||
	      !plane.sameSide(p222, p212) ||
	      !plane.sameSide(p222, p221) ||
	      !plane.sameSide(p111, p211) ||
	      !plane.sameSide(p112, p212) ||
	      !plane.sameSide(p121, p221) ||
	      !plane.sameSide(p122, p222));
    }

    /** min X accessor */
    inline T getMinX() const { return minX; }
    /** max X accessor */
    inline T getMaxX() const { return maxX; }
    /** min Y accessor */
    inline T getMinY() const { return minY; }
    /** max Y accessor */
    inline T getMaxY() const { return maxY; }
    /** min Z accessor */
    inline T getMinZ() const { return minZ; }
    /** max Z accessor */
    inline T getMaxZ() const { return maxZ; }

    /** size X accessor */
    inline T getSizeX() const { return maxX - minX; }
    /** size Y accessor */
    inline T getSizeY() const { return maxY - minY; }
    /** size Z accessor */
    inline T getSizeZ() const { return maxZ - minZ; }

    /**
       return the radius of the box, i.e. the distance between the center and a corner point
    */
    inline T getRadius() const {
      const double x = getSizeX() / 2;
      const double y = getSizeY() / 2;
      const double z = getSizeZ() / 2;
      return sqrt(x * x + y * y + z * z);
    }

    /**
       Divide operator
    */
    Box3DT<T> operator /= (double ratio) {
      minX /= ratio;
      maxX /= ratio;
      minY /= ratio;
      maxY /= ratio;
      minZ /= ratio;
      maxZ /= ratio;

      return *this;
    }

    /**
       Multiply operator
    */
    Box3DT<T> operator *= (double ratio) {
      minX *= ratio;
      maxX *= ratio;
      minY *= ratio;
      maxY *= ratio;
      minZ *= ratio;
      maxZ *= ratio;

      return *this;
    }

    /**
       Plus operator
    */
    Box3DT<T> operator += (const Coord3DT<T> & coord) {
      minX += coord.getX();
      maxX += coord.getX();
      minY += coord.getY();
      maxY += coord.getY();
      minZ += coord.getZ();
      maxZ += coord.getZ();

      return *this;
    }

    /**
       Minus operator
    */
    Box3DT<T> operator -= (const Coord3DT<T> & coord) {
      minX -= coord.getX();
      maxX -= coord.getX();
      minY -= coord.getY();
      maxY -= coord.getY();
      minZ -= coord.getZ();
      maxZ -= coord.getZ();

      return *this;
    }

    /**
       Round to nearest integer coords. The result box includes the current
    */
    Box3DT<int> largeRound() const {
      return Box3DT<int>((int)floor(minX), (int)ceil(maxX),
			 (int)floor(minY), (int)ceil(maxY),
			 (int)floor(minZ), (int)ceil(maxZ));
    }


    /**
       return the middle point of the box
    */
    inline Coord3DT<T> getMiddle() const {
      return Coord3DT<T>(minX + (maxX - minX) / 2, minY + (maxY - minY) / 2, minZ + (maxZ - minZ) / 2);
    }
  };

  /**
     + operator for points
  */
  template <typename T>
  Coord3DT<T> operator + (const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
    return Coord3DT<T>(p1.getX() + p2.getX(), p1.getY() + p2.getY(), p1.getZ() + p2.getZ());
  }

  /**
     + operator for boxes
  */
  template <typename T>
  Box3DT<T> operator + (const Box3DT<T> & b, const Coord3DT<T> & p) {
    return Box3DT<T>(b.getMinX() + p.getX(), b.getMaxX() + p.getX(),
		     b.getMinY() + p.getY(), b.getMaxY() + p.getY(),
		     b.getMinZ() + p.getZ(), b.getMaxZ() + p.getZ());
  }

  /**
     / operator by a double
  */
  template <typename T>
  Coord3DT<T> operator / (const Coord3DT<T> & p, double a) {
    if(a == 0)
      throw Exception("Division by zero");
    return Coord3DT<T>(p.getX() / a, p.getY() / a, p.getZ() / a);
  }

  /**
   * operator by a double
   */
  template <typename T>
  Box3DT<T> operator * (const Box3DT<T> & b, double a) {
    return Box3DT<T>(b.getMinX() * a, b.getMaxX() * a,
		     b.getMinY() * a, b.getMaxY() * a,
		     b.getMinZ() * a, b.getMaxZ() * a);
  }

  /**
   * operator by a double
   */
  template <typename T>
  Coord3DT<T> operator * (const Coord3DT<T> & p1, double a) {
    return Coord3DT<T>(p1.getX() * a, p1.getY() * a, p1.getZ() * a);
  }

  /**
   * operator by a double
   */
  template <typename T>
  Coord3DT<T> operator * (double a, const Coord3DT<T> & p1) {
    return Coord3DT<T>(p1.getX() * a, p1.getY() * a, p1.getZ() * a);
  }

  /**
   * operator p1^T * p2
   */
  template <typename T>
  Coord3DT<T> operator * (const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
    return Coord3DT<T>(p1.getX() * p2.getX(), p1.getY() * p2.getY(), p1.getZ() * p2.getZ());
  }

  /**
     / operator term by term
  */
  template <typename T>
  Coord3DT<T> operator / (const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
    return Coord3DT<T>(p1.getX() / p2.getX(), p1.getY() / p2.getY(), p1.getZ() / p2.getZ());
  }


  /**
     - operator for points
  */
  template <typename T>
  Coord3DT<T> operator - (const Coord3DT<T> & p1, const Coord3DT<T> & p2) {
    return Coord3DT<T>(p1.getX() - p2.getX(), p1.getY() - p2.getY(), p1.getZ() - p2.getZ());
  }

  /**
     Stream operator
  */
  template <typename T, typename S, typename U>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Coord3DT<U> & p) {
    f << "Coord3D(" << p.getX() << ", " << p.getY() << ", " << p.getZ() << ")";
    return f;
  }

  /**
     Stream operator
  */
  template <typename T, typename S, typename U>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Box3DT<U> & p) {
    f << "Box((" << p.getMinX() << ", " << p.getMaxX() << "), (" << p.getMinY() << ", " << p.getMaxY() << "), (" << p.getMinZ() << ", " << p.getMaxZ() << "))";
    return f;
  }

  template <typename T>
  bool Edge3DT<T>::contains(const Coord3DT<T> & p, double epsilon) const {
    Box3DT<T> b(p1, p2);
    if (!b.contains(p, 0.))
      return false;
    Line3DT<T> l(*this);
    return l.distance(p) < epsilon * p1.distance(p2);
  }

  /**
     Define an alias to Edge3DT with double
  */
  typedef Edge3DT<double> Edge3D;
  /**
     Define an alias to Triangle3DT with double
  */
  typedef Triangle3DT<double> Triangle3D;
  /**
     Define an alias to Line3DT with double
  */
  typedef Line3DT<double> Line3D;
  /**
     Define an alias to Plane3DT with double
  */
  typedef Plane3DT<double> Plane3D;
  /**
     Define an alias to Box3DT with double
  */
  typedef Box3DT<double> Box3D;

}
#endif
