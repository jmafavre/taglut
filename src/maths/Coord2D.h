/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef COORD2D
#define COORD2D

#include<vector>
#include "Coord3D.h"

namespace Taglut {


  /**
     @class Coord2DT

     @author Jean-Marie Favreau (CNRS / Univ. Blaise Pascal)
     @date 2008-11-11
     @brief 2D coordinates
  */
  template <typename T>
  class Coord2DT {
  protected:
    /**
       2d location (x value)
    */
    T x2d;
    /**
       2d location (y value)
    */
    T y2d;

  public:

    /**
       Default constructor
    */
    inline Coord2DT() {
      x2d = y2d = 0;
    }

    /**
       Constructor using location
    */
    inline Coord2DT(const T & x, const T & y) {
      x2d = x;
      y2d = y;
    }

    /**
       Constructor using Coord
    */
    inline Coord2DT(const Coord2DT<T> & c) {
      x2d = c.x2d;
      y2d = c.y2d;
    }

    /** destructor */
    ~Coord2DT() { }

    /**
       Set 2d location
    */
    inline const Coord2DT<T> & set2DCoords(const T & x2d_l, const T & y2d_l) {
      (*this).x2d = x2d_l;
      (*this).y2d = y2d_l;
      return *this;
    }

    /**
       Set 2D location
    */
    inline const Coord2DT<T> & set2DCoords(const Coord2DT<T> & c) {
      x2d = c.x2d;
      y2d = c.y2d;
      return *this;
    }

    /**
       return 2d location
    */
    inline const std::pair<T, T> get2DCoords() const {
      return std::pair<T, T>(x2d, y2d);
    }

    /**
       Get x value (2d location)
    */
    inline const T get2DX() const { return x2d; }
    /**
       Get y value (2d location)
    */
    inline const T get2DY() const { return y2d; }

#ifndef SWIG
    /** random access operator */
    T & operator[](unsigned int i) {
      if (i == 0)
	return x2d;
      else
	return y2d;
    }

    /** random access operator */
    const T & operator[](unsigned int i) const {
      if (i == 0)
	return x2d;
      else
	return y2d;
    }
#endif

    /**
       Set x value (2d location)
    */
    inline void set2DX(const T & v) { x2d = v; }
    /**
       Set y value (2d location)
    */
    inline void set2DY(const T & v) { y2d = v; }

    /**
       Multiply operator
    */
    inline Coord2DT<T> & operator*= (double a) {
      x2d *= a;
      y2d *= a;
      return *this;
    }

    /**
       Multiply operator
    */
    inline Coord2DT<T> operator* (double a) const {
      return Coord2DT<T>(x2d * a, y2d * a);
    }


    /**
       Divide operator
    */
    inline Coord2DT<T> & operator/= (double a) {
      if(a == 0)
	throw Exception("Division by zero");
      x2d /= a;
      y2d /= a;

      return *this;
    }

    /**
       Add operator
    */
    inline Coord2DT<T> & operator+= (const Coord2DT<T> & p) {
      x2d += p.x2d;
      y2d += p.y2d;

      return *this;
    }

#ifndef SWIG
    /**
       Affectation operator
    */
    inline Coord2DT<T> & operator= (const Coord2DT<T> & p) {
      x2d = p.x2d;
      y2d = p.y2d;

      return *this;
    }
#endif

    /** inverse the coordinates */
    inline Coord2DT<T> & inverseXY() {
      const T tmp = x2d;
      x2d = y2d;
      y2d = tmp;
      return *this;
    }

    /** compute angle defined by ((this, p1), (this, p2) */
    inline double angle2D(const Coord2DT<T> & p1, const Coord2DT<T> & p2) const {
      std::pair<double, double> n1;
      n1.first = p1.get2DX() - (*this).get2DX();
      n1.second = p1.get2DY() - (*this).get2DY();
      std::pair<double, double> n2;
      n2.first = p2.get2DX() - (*this).get2DX();
      n2.second = p2.get2DY() - (*this).get2DY();

      // then compute angle
      double value = (n1.first * n2.first + n1.second * n2.second) /
	sqrt((n1.first * n1.first + n1.second * n1.second) *
	     (n2.first * n2.first + n2.second * n2.second));
      double sign = (n1.first * n2.second - n1.second * n2.first);

      if (value > 1) value = 1;
      if (value < -1) value = -1;
      if (sign < 0)
	return 2 * M_PI - acos(value);
      else
	return acos(value);
    }


    /**
       2D Euclidean distance between current point and p
    */
    inline double distance2D(const Coord2DT<T> & p) const {
      return sqrt((x2d - p.get2DX()) * (x2d - p.get2DX()) +
		  (y2d - p.get2DY()) * (y2d - p.get2DY()));
    }

    /**
       Return norm of the coord seen as a vector
    */
    inline double norm2D() const {
      return sqrt(x2d * x2d + y2d * y2d);
    }

    /**
       normalize the current vector
    */
    inline Coord2DT<T> & normalize() {
      double value = norm2D();

      if (value != 0) {
	x2d /= value;
	y2d /= value;
      }

      return *this;
    }

    /**
       normalize the current vector
    */
    inline Coord2DT<T> getNormalize() const {
      Coord2DT<T> norm(*this);

      return norm.normalize();
    }

    /**
       normalize the current vector (according to the sum of the components)
    */
    inline Coord2DT<T> & normalize_sum() {
      double value = x2d + y2d;

      if (value != 0) {
	x2d /= value;
	y2d /= value;
      }

      return *this;
    }

#ifndef SWIG
    /**
       2D Euclidean distance between current point and p
    */
    template <typename V>
    inline double distance2D(V px, V py) const {
      return sqrt((x2d - px) * (x2d - px) +
		  (y2d - py) * (y2d - py));
    }
#endif


    /**
       Compute the area of the triangle defined by 3 2D points
    */
    inline static double compute2DTriangleArea(const Coord2DT<T> & p1, const Coord2DT<T> & p2, const Coord2DT<T> & p3) {
      return 0.5 * fabs((p2.get2DX() - p1.get2DX()) * (p3.get2DY() - p1.get2DY()) - (p3.get2DX() - p1.get2DX()) * (p2.get2DY() - p1.get2DY()));
    }


#ifndef SWIG
    /** return true if the current point and the given point has same location */
    inline bool same2DLocation(const Coord2DT<T> & point) const {
      return same2DLocation(*this, point);
    }
#endif

    /** return true if given point has same location */
    inline static bool same2DLocation(const Coord2DT<T> & point1, const Coord2DT<T> & point2) {
      return (point1.x2d == point2.x2d) && (point1.y2d == point2.y2d);
    }

    /**
       Compute rotation of the current point arround the given point
    */
    inline Coord2DT<T> & rotate(double angle, const Coord2DT<T> & center) {
      // compute rotation
      double x = center.get2DX() + (x2d - center.get2DX()) * cos(angle) - (y2d - center.get2DY()) * sin(angle);
      double y = center.get2DY() +  (x2d - center.get2DX()) * sin(angle) + (y2d - center.get2DY()) * cos(angle);


      x2d = x;
      y2d = y;

      return *this;
    }

    /**
       angle of the vector (in [0; 2 * PI])
    */
    inline double angle2D() const {
      Coord2DT<T> norm = getNormalize();

      double angle = acos(norm.get2DX());
      if (norm.get2DY() < 0)
	angle = 2 * M_PI - angle;

      return angle;
    }


    /**
       Vectorial product
    */
    static Coord3DT<T> vectorialProduct(const Coord2DT<T> & p1, const Coord2DT<T> & p2) {
      return Coord3DT<T>(0, 0, p1.get2DX() * p2.get2DY() - p1.get2DY() * p2.get2DX());
    }

    /**
       Scalar product
    */
    static double scalarProduct(const Coord2DT<T> & p1, const Coord2DT<T> & p2) {
      return p1.get2DX() * p2.get2DX() + p1.get2DY() * p2.get2DY();
    }

    static Coord2DT<T> getIsoBarycenter(const std::vector<Coord2DT<T> > & points) {
      if (points.size() == 0)
	throw Exception("getIsoBarycenter(1): empty list");
      Coord2DT<T> result;
      for(typename std::vector<Coord2DT<T> >::const_iterator p = points.begin(); p != points.end(); ++p)
	result += *p;
      result /= points.size();
      return result;
    }
  };


  /**
     @class Line2DT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-09-08
     @brief A 2D line defined by a point and a vector
  */
  template<typename T>
  class Line2DT {
  private:
    Coord2DT<T> point;
    Coord2DT<T> vector;
  public:
    /** constructor */
    Line2DT(const Coord2DT<T> & point_l, const Coord2DT<T> & vector_l) : point(point_l), vector(vector_l) {
    }

    /**
       Point accessor
    */
    const Coord2DT<T> & getPoint() const {
      return point;
    }

    /**
       Vector accessor
    */
    const Coord2DT<T> & getVector() const {
      return vector;
    }

    /** return the location of the crossing between the current line and a given line, defined in the scalar coordinate system
	defined by the vector and the point that describe this line */
    double getLocation(const Line2DT<T> & line) const {
      // select the best lines to compute the values
      unsigned char v1 = 0;
      unsigned char v2 = 0;
      double div = 0.;
      for(unsigned char i = 0; i < 2; ++i)
	for(unsigned char j = 0; j < 2; ++j) {
	  const double d2 = fabs(line.getVector()[j] * (line.getVector()[j] * vector[i] - line.getVector()[i] * vector[j]));
	  if (d2 > div) {
	    div = d2;
	    v1 = i;
	    v2 = j;
	  }
	}

      if ((line.getVector()[v2] == 0.) || ((line.getVector()[v2] * vector[v1] - line.getVector()[v1] * vector[v2]) == 0.))
	throw Exception("getIntersection(): the two lines are parallel");

      // compute the scalar value on the current line
      return (line.getVector()[v2] *
	      (line.getPoint()[v1] + line.getVector()[v1] * (point[v2] - line.getPoint()[v2]) / line.getVector()[v2] - point[v1]) /
	      (line.getVector()[v2] * vector[v1] - line.getVector()[v1] * vector[v2]));
    }

    /** return the location of a point defined by its locaion */
    inline Coord2DT<T> getPointFromLocation(double location) const {
      return point + vector * location;
    }

    /** return the location of the crossing between the current line and a given line */
    inline Coord2DT<T> getIntersection(const Line2DT<T> & line) const {
      return getPointFromLocation(getLocation(line));
    }
  };

  /**
     @class Edge2DT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-09-08
     @brief A 2D edge defined by points (standelone version, without Mesh reference)
  */
  template<typename T>
  class Edge2DT {
  private:
    Coord2DT<T> p1;
    Coord2DT<T> p2;
  public:

    /**
       Constructor
    */
    Edge2DT(const Coord2DT<T> & p1_l, const Coord2DT<T> & p2_l) : p1(p1_l), p2(p2_l) { }

    /** point accessor */
    Coord2DT<T> & getP1() { return p1; }
    /** point accessor */
    Coord2DT<T> & getP2() { return p2; }

    /** point accessor */
    const Coord2DT<T> & getP1() const { return p1; }
    /** point accessor */
    const Coord2DT<T> & getP2() const { return p2; }

    /** point modifier */
    void setP1(const Coord2DT<T> & p) { p1 = p; }
    /** point modifier */
    void setP2(const Coord2DT<T> & p) { p2 = p; }


    /**
       Return true if the current edge intersects the given one.
       see http://www.exaflop.org/docs/cgafaq/cga1.html
       *this = AB
       edge = CD
       (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
       r = -----------------------------  (eqn 1)
       (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
       (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
       s = -----------------------------  (eqn 2)
       (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
    */
    bool intersect(const Edge2DT<T> & edge) const {
      const Coord2DT<T> & pA = p1;
      const Coord2DT<T> & pB = p2;
      const Coord2DT<T> & pC = edge.getP1();
      const Coord2DT<T> & pD = edge.getP2();
      double r = (((pA.get2DY() - pC.get2DY()) * (pD.get2DX() - pC.get2DX()) -
		   (pA.get2DX() - pC.get2DX()) * (pD.get2DY() - pC.get2DY())) /
		  ((pB.get2DX() - pA.get2DX()) * (pD.get2DY() - pC.get2DY()) -
		   (pB.get2DY() - pA.get2DY()) * (pD.get2DX() - pC.get2DX())));

      double s = (((pA.get2DY() - pC.get2DY()) * (pB.get2DX() - pA.get2DX()) -
		   (pA.get2DX() - pC.get2DX()) * (pB.get2DY() - pA.get2DY())) /
		  ((pB.get2DX() - pA.get2DX()) * (pD.get2DY() - pC.get2DY()) -
		   (pB.get2DY() - pA.get2DY()) * (pD.get2DX() - pC.get2DX())));

      return (0 <= r) && (r <= 1) && (0 <= s) && (s <= 1);
    }
  };

  /**
     @class Triangle3DT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-03-06
     @brief A 2D triangle defined by points (standelone version, without Mesh reference)
  */
  template<typename T>
  class Triangle2DT {
  private:
    Coord2DT<T> p1;
    Coord2DT<T> p2;
    Coord2DT<T> p3;
  public:
    /**
       Constructor
    */
    Triangle2DT(const Coord2DT<T> & p1_l, const Coord2DT<T> & p2_l, const Coord2DT<T> & p3_l) : p1(p1_l), p2(p2_l), p3(p3_l) { }

    /** point accessor */
    Coord2DT<T> & getP1() { return p1; }
    /** point accessor */
    Coord2DT<T> & getP2() { return p2; }
    /** point accessor */
    Coord2DT<T> & getP3() { return p3; }

    /** point accessor */
    const Coord2DT<T> & getP1() const { return p1; }
    /** point accessor */
    const Coord2DT<T> & getP2() const { return p2; }
    /** point accessor */
    const Coord2DT<T> & getP3() const { return p3; }

    /** point modifier */
    void setP1(const Coord2DT<T> & p) { p1 = p; }
    /** point modifier */
    void setP2(const Coord2DT<T> & p) { p2 = p; }
    /** point modifier */
    void setP3(const Coord2DT<T> & p) { p3 = p; }

    /** point accessor */
    Coord2DT<T> & getPoint(unsigned int i) {
      switch (i % 3) {
      case 0: return p1;
      case 1: return p2;
      case 2: default: return p3;
      }
    }

    /** point accessor */
    const Coord2DT<T> & getPoint(unsigned int i) const {
      switch (i % 3) {
      case 0: return p1;
      case 1: return p2;
      case 2: default: return p3;
      }
    }

    /** edge accessor */
    Edge2DT<T> getEdge1() const { return Edge2DT<T>(p1, p2); }

    /** edge accessor */
    Edge2DT<T> getEdge2() const { return Edge2DT<T>(p2, p3); }

    /** edge accessor */
    Edge2DT<T> getEdge3() const { return Edge2DT<T>(p3, p1); }


    /**
       Return true if the given edge intersects the current triangle
    */
    bool intersect(const Edge2DT<T> & edge) const {
      return (isInside(edge.getP1()) ||
	      edge.intersect(Edge2DT<T>(p1, p2)) ||
	      edge.intersect(Edge2DT<T>(p1, p3)) ||
	      edge.intersect(Edge2DT<T>(p3, p2)));
    }

    /**
       Return true if the given point is inside the current triangle
       Assums that the point is on the plane defined by the triangle
    */
    bool isInside(const Coord2DT<T> & coord) const {
      /* see http://www.blackpawn.com/texts/pointinpoly/default.html */
      return ((coord.distance2D(p1) < 1e-7) || (coord.distance2D(p2) < 1e-7) || (coord.distance2D(p3) < 1e-7) ||
	      ((Coord3DT<T>::scalarProduct(Coord2DT<T>::vectorialProduct(p2 - p1, coord - p1), Coord2DT<T>::vectorialProduct(coord - p1, p3 - p1)) >= 0) &&
	       (Coord3DT<T>::scalarProduct(Coord2DT<T>::vectorialProduct(p1 - p2, coord - p2), Coord2DT<T>::vectorialProduct(coord - p2, p3 - p2)) >= 0) &&
	       (Coord3DT<T>::scalarProduct(Coord2DT<T>::vectorialProduct(p1 - p3, coord - p3), Coord2DT<T>::vectorialProduct(coord - p3, p2 - p3)) >= 0)));
    }


  };


  /**
     @class Box2DT

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2008-09-08
     @brief A 2D box defined by points
  */
  template<typename T>
  class Box2DT {
  protected:
    /** Min X coord */
    T minX;
    /** Max X coord */
    T maxX;
    /** Min Y coord */
    T minY;
    /** Max Y coord */
    T maxY;

  public:
    /**
       Default constructor
    */
    Box2DT() : minX(0), maxX(0), minY(0), maxY(0) { }

    /**
       Copy constructor
    */
    template <typename S>
    Box2DT(const Box2DT<S> & box) : minX(box.minX), maxX(box.maxX), minY(box.minY), maxY(box.maxY) { }

    /**
       Constructor with default values
    */
    Box2DT(T minx, T maxx, T miny, T maxy) : minX(minx), maxX(maxx), minY(miny), maxY(maxy) { }

    /**
       Constructor as the convex hull of the two given points
    */
    Box2DT(const Coord2DT<T> & p1, const Coord2DT<T> & p2) : minX(p1.get2DX()), maxX(p1.get2DX()),
						     minY(p1.get2DY()), maxY(p1.get2DY()) {
      addPoint(p2);
    }

    /**
       Build a bounding box of a triangle
    */
    Box2DT(const Triangle2DT<T> & triangle) {
      /* initialization */
      minX = maxX = triangle.getP1().get2DX();
      minY = maxY = triangle.getP1().get2DY();

      /* adding all the other points */
      addPoint(triangle.getP2());
      addPoint(triangle.getP3());

    }


    /** add a point inside the current box: resize-it if needed*/
    Box2DT<T> & addPoint(const Coord2DT<T> & p) {
      if (minX > p.get2DX()) minX = p.get2DX();
      if (maxX < p.get2DX()) maxX = p.get2DX();
      if (minY > p.get2DY()) minY = p.get2DY();
      if (maxY < p.get2DY()) maxY = p.get2DY();
      return *this;
    }

    /**
       return true if the given point is inside the 2D box
    */
    bool contains(const Coord2DT<T> & p, double e = 1e-7) const {
      return ((minX - e <= p.get2DX()) && (maxX + e >= p.get2DX()) &&
	      (minY - e <= p.get2DY()) && (maxY + e >= p.get2DY()));
    }

    /**
       return true if the given point is in the border of the 2D box
    */
    bool inBorder(const Coord2DT<T> & p, double e = 1e-7) const {
      return ((fabs(minX - p.get2DX()) <= e) || (fabs(maxX - p.get2DX()) <= e) ||
	      (fabs(minY - p.get2DY()) <= e) || (fabs(maxY - p.get2DY()) <= e));
    }

    /**
       check the intersection between the current box and a triangle
    */
    bool intersect(const Triangle2DT<T> & triangle) const {
      Box2DT<T> box(triangle);

      // first check intersection of the bounding box and the current box
      if (contains(box))
	return true;
      if (!intersect(box))
	return false;

      // then for each edge, compute intersection with the current box
      if (intersect(triangle.getEdge1()) ||
	  intersect(triangle.getEdge2()) ||
	  intersect(triangle.getEdge3()))
	return true;

      return false;
    }

    /**
       check the intersection between the current box and the given \p box
    */
    bool intersect(const Box2DT<T> & box) const {
      return !((minX > box.getMaxX()) || (maxX < box.getMinX()) ||
	       (minY > box.getMaxY()) || (maxY < box.getMinY()));
    }

    /**
       check the intersection between the current box and the given \p edge
    */
    bool intersect(const Edge2DT<T> & edge) const {
      Coord2DT<T> c1(minX, minY);
      Coord2DT<T> c2(minX, maxY);
      Coord2DT<T> c3(maxX, minY);
      Coord2DT<T> c4(maxX, maxY);
      return (contains(edge.getP1()) ||
	      contains(edge.getP2()) ||
	      edge.intersect(Edge2DT<T>(c1, c2)) ||
	      edge.intersect(Edge2DT<T>(c1, c3)) ||
	      edge.intersect(Edge2DT<T>(c2, c4)) ||
	      edge.intersect(Edge2DT<T>(c3, c4)));
    }


    /**
       Return true if the current box contains the given \p box
    */
    bool contains(const Box2DT<T> & box) const {
      return ((minX <= box.getMinX()) && (maxX >= box.getMaxX()) &&
	      (minY <= box.getMinY()) && (maxY >= box.getMaxY()));
    }


    /** min X accessor */
    T getMinX() const { return minX; }
    /** max X accessor */
    T getMaxX() const { return maxX; }
    /** min Y accessor */
    T getMinY() const { return minY; }
    /** max Y accessor */
    T getMaxY() const { return maxY; }

    /** size X accessor */
    T getSizeX() const { return maxX - minX; }
    /** size Y accessor */
    T getSizeY() const { return maxY - minY; }


    /**
       Divide operator
    */
    Box2DT<T> operator /= (double ratio) {
      minX /= ratio;
      maxX /= ratio;
      minY /= ratio;
      maxY /= ratio;

      return *this;
    }

    /**
       Multiply operator
    */
    Box2DT<T> operator *= (double ratio) {
      minX *= ratio;
      maxX *= ratio;
      minY *= ratio;
      maxY *= ratio;

      return *this;
    }

    /**
       Plus operator
    */
    Box2DT<T> operator += (const Coord3DT<T> & coord) {
      minX += coord.getX();
      maxX += coord.getX();
      minY += coord.getY();
      maxY += coord.getY();

      return *this;
    }

    /**
       Minus operator
    */
    Box2DT<T> operator -= (const Coord3DT<T> & coord) {
      minX -= coord.getX();
      maxX -= coord.getX();
      minY -= coord.getY();
      maxY -= coord.getY();

      return *this;
    }

    /**
       Round to nearest integer coords. The result box includes the current
    */
    Box2DT<int> largeRound() const {
      return Box2DT<int>((int)floor(minX), (int)ceil(maxX),
			 (int)floor(minY), (int)ceil(maxY));
    }

  };

  /** this class describe a circle using a center and a radius */
  template <class T>
  class Circle2DT {
  private:
    Coord2DT<T> center;
    T radius;

    static Coord2DT<T> getProjection(T xp, T yp, T xr, T yr, T xc, T yc, T radius, bool inside) {
      const double a = (yr - yp) / (xr - xp);
      const double b = yp - a * xp;
      const double A = 1 + a * a;
      const double B = 2 * a * b - 2 * xc - 2 * a * yc;
      const double C = xc * xc + b * b - 2 * b * yc + yc * yc - radius * radius;
      const double delta = B * B - 4 * A * C;
      if (delta > 0.) {
	const double x1 = (-B + sqrt(delta)) / (2 * A);
	const double x2 = (-B - sqrt(delta)) / (2 * A);
	assert(fabs(x1 - xc) <= radius);
	assert(fabs(x2 - xc) <= radius);
	if (inside) {
	  if (fabs(x1 - xp) < fabs(x1 - xr))
	    return Coord2DT<T>(x1, a * x1 + b);
	  else
	    return Coord2DT<T>(x2, a * x2 + b);
	}
	else {
	  if (fabs(x1 - xp) < fabs(x2 - xp))
	    return Coord2DT<T>(x1, a * x1 + b);
	  else
	    return Coord2DT<T>(x2, a * x2 + b);
	}
      }
      else if (delta == 0.) {
	const double x = -B / (2 * A);
	assert(fabs(x - xc) <= radius);
	assert(fabs((x - xc) * (x - xc) + (a * x + b - yc) * (a * x + b - yc) - radius * radius) <= 1e-3);
	return Coord2DT<T>(x, a * x + b);
      }
      else { // (delta < 0.)
	throw Exception("getProjection(2): no solution");
      }
    }

  public:
    /** constructor */
    Circle2DT(const Coord2DT<T> & center_, T radius_) : center(center_), radius(radius_) { }
    /** copy constructor */
    Circle2DT(const Circle2DT<T> & c) : center(c.center), radius(c.radius) { }

    /** return true if the given point is inside the circle */
    inline bool isInside(const Coord2DT<T> & point) const {
      return point.distance2D(center) <= radius;
    }

    /** return true if the given point is on the border of the circle */
    inline bool onBorder(const Coord2DT<T> & point) const {
      return point.distance2D(center) == radius;
    }

    /** return a point that corresponds to the projection of \p point on
	the current circle. The initial point \p point is between the reference point \p ref
	and the projected point. */
    Coord2DT<T> getProjection(const Coord2DT<T> & point, const Coord2DT<T> & ref) const {
      if (onBorder(ref))
	return ref;
      if (onBorder(point))
	return point;
      if (point.same2DLocation(ref))
	throw Exception("getProjection(2): cannot project a point located on the reference point");
      const bool inside = isInside(point);
      if (inside != isInside(ref))
	throw Exception("getProjection(2): the given point and reference point are not in the same side");
      if ((point.distance2D(center) > radius) && (point.distance2D(center) > ref.distance2D(center)))
	throw Exception("getProjection(2): cannot project a point located on the bad side of point");
      const double xp = point.get2DX();
      const double yp = point.get2DY();
      const double xr = ref.get2DX();
      const double yr = ref.get2DY();
      const double xc = center.get2DX();
      const double yc = center.get2DY();
      if (fabs(xp - xr) > fabs(yp - yr)) // choose the best axis (in order to minimize errors)
	return getProjection(xp, yp, xr, yr, xc, yc, radius, inside);
      else
	return getProjection(yp, xp, yr, xr, yc, xc, radius, inside).inverseXY();
    }
  };

  /**
     Define an alias to Coord2DT with double
  */
  typedef Coord2DT<double> Coord2D;
  /**
     Define an alias to Box2DT with double
  */
  typedef Box2DT<double> Box2D;
  /**
     Define an alias to Triangle2DT with double
  */
  typedef Triangle2DT<double> Triangle2D;
  /**
     Define an alias to Edge2DT with double
  */
  typedef Edge2DT<double> Edge2D;
  /**
     Define an alias to Line2DT with double
  */
  typedef Line2DT<double> Line2D;
  /**
     Define an alias to Circle2DT with double
  */
  typedef Circle2DT<double> Circle2D;


  /**
     + operator for points
  */
  template <typename T>
  Coord2DT<T> operator + (const Coord2DT<T> & p1, const Coord2DT<T> & p2) {
    return Coord2DT<T>(p1.get2DX() + p2.get2DX(), p1.get2DY() + p2.get2DY());
  }

  /**
     / operator by a double
  */
  template <typename T>
  Coord2DT<T> operator / (const Coord2DT<T> & p, double a) {
    if(a == 0)
      throw Exception("Division by zero");
    return Coord3DT<T>(p.get2DX() / a, p.get2DY() / a);
  }

  /**
     - operator for points
  */
  template <typename T>
  Coord2DT<T> operator - (const Coord2DT<T> & p1, const Coord2DT<T> & p2) {
    return Coord2DT<T>(p1.get2DX() - p2.get2DX(), p1.get2DY() - p2.get2DY());
  }

  /**
     Stream operator
  */
  template <typename T, typename S, typename U>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Coord2DT<U> & p) {
    f << "Coord2D(" << p.get2DX() << ", " << p.get2DY() << ")";
    return f;
  }


}

#endif
