/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Matrix.h"

using namespace Taglut;


Matrix3x3::Matrix3x3(const Coord3D & c1, const Coord3D & c2, const Coord3D & c3) {
  matrix[0][0] = c1.getX();  matrix[1][0] = c1.getX(); matrix[2][0] = c1.getX();
  matrix[0][1] = c1.getY();  matrix[1][1] = c2.getY(); matrix[2][1] = c2.getY();
  matrix[0][2] = c1.getZ();  matrix[1][2] = c3.getZ(); matrix[2][2] = c3.getZ();  
}

Matrix3x3::Matrix3x3(double m00, double m10, double m20,
		     double m01, double m11, double m21,
		     double m02, double m12, double m22) {
  matrix[0][0] = m00;  matrix[1][0] = m10; matrix[2][0] = m20;
  matrix[0][1] = m01;  matrix[1][1] = m11; matrix[2][1] = m21;
  matrix[0][2] = m02;  matrix[1][2] = m12; matrix[2][2] = m22;
}


Matrix3x3 Matrix3x3::getRotationMatrix(const Coord3D & axis, double angle) {
  const double cangle = cos(angle);
  const double onecangle = 1 - cangle;
  const double sangle = sin(angle);
  return Matrix3x3(cangle + onecangle * axis.getX() * axis.getX(),
		   onecangle * axis.getX() * axis.getY() - sangle * axis.getZ(),
		   onecangle * axis.getX() * axis.getZ() + sangle * axis.getY(),

		   onecangle * axis.getX() * axis.getY() + sangle * axis.getZ(),
		   cangle + onecangle * axis.getY() * axis.getY(),
		   onecangle * axis.getY() * axis.getZ() - sangle * axis.getX(),

		   onecangle * axis.getX() * axis.getZ() - sangle * axis.getY(),
		   onecangle * axis.getY() * axis.getZ() + sangle * axis.getX(),
		   cangle + onecangle * axis.getZ() * axis.getZ());
}
