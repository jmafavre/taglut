/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "CPMethod.h"
#include "Mapping2D3D.h"
#include "ABFMethod.h"
#include "Exception.h"
#include "FileTools.h"
#include "MeshPart.h"
#include "MeshCut.h"
#include "Display3D.h"
#include "MultiscaleUnfolding.h"
#include "Length.h"

#include "Messages.h"

using namespace std;
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  objectName = NULL;
static char*  filename_output  = NULL;
static double epsilon = 1e-8;
static int    maxIter = 40000;
static int    randomValue = 0;
static int    overlaps = 0;
static int    border = 0;
static int    borderTriangles = 0;
static int    xsize = 256;
static int    ysize = 256;
static double gridStep = -1;
static int    display = 0;
static int    cComponent = -1;
static int    help = 0;
static char*  length = NULL;
static double ratio = -1;
static int    postTreatment = 0;
static int    postTreatmentIt = 20000;
static int    variant = 0;
static int    dynamicDistance = 0;
static int    dynamic = 0;
static char*  method  = NULL;
static char*  logFile = NULL;
static int    mscaleDepth = 0;
static char*  mscaleMethod = NULL;


struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "connected-component", 'c', POPT_ARG_INT, &cComponent, 0, "Connected component in object", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output file (image, cst, ums)", NULL},
  { "unfolding-method", 'u', POPT_ARG_STRING, &method, 0, "Unfolding method (available: \"circle-packing\", \"abf\")", NULL},
  { "log-file", '\0', POPT_ARG_STRING, &logFile, 0, "Log file (with informations on radii, angles, etc)", NULL},
  { "length", 'l', POPT_ARG_STRING, &length, 0, "Select length method used to cut surface (or \"list\" for list them)", NULL},
  { "ratio-length", 'a', POPT_ARG_DOUBLE, &ratio, 0, "Ratio of euclidean length that can be modified by other length methods. 0 <= ratio <= 1", NULL},
  { "epsilon", 'e', POPT_ARG_DOUBLE, &epsilon, 0, "Epsilon (used in unfolding algorithms)", NULL},
  { "maxIter", 'm', POPT_ARG_INT, &maxIter, 0, "Maximum iteration (used in unfolding algorithms)", NULL},
  { "variant", 'v', POPT_ARG_INT, &variant, 0, "Circle packing variant (0: default with fixed boundaries, 1: dynamic boundaries)", NULL},
  { "post-treatment", 'p', POPT_ARG_INT, &postTreatment, 0, "Post treatment to use after unfolding (-1 for help)", NULL},
  { "iteration-post-treatment", 'j', POPT_ARG_INT, &postTreatmentIt, 0, "Maximum iteration for post treatment", NULL},
  { "multiscale-depth", '\0', POPT_ARG_INT, &mscaleDepth, 0, "Multiscale depth", NULL},
  { "multiscale-method", '\0', POPT_ARG_STRING, &mscaleMethod, 0, "Multiscale method (available: \"readjust\" and \"interpolate\")", NULL},
  { "random", 'r', POPT_ARG_NONE, &randomValue, 0, "Use random color on output image.", NULL},
  { "overlaps", '\0', POPT_ARG_NONE, &overlaps, 0, "Use an overlap drawing method on output image.", NULL},
  { "grid", 'g', POPT_ARG_DOUBLE, &gridStep, 0, "Use black and white grid to color output image (used if != -1).", NULL},
  { "border", 'b', POPT_ARG_NONE, &border, 0, "Draw border.", NULL},
  { "triangle-border", 't', POPT_ARG_NONE, &borderTriangles, 0, "Draw border for each triangle.", NULL},
  { "xsize", 'x', POPT_ARG_INT, &xsize, 0, "Output image size (x size).", NULL},
  { "ysize", 'y', POPT_ARG_INT, &ysize, 0, "Output image size (y size).", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image.", NULL},
  { "dynamic-distance", '2', POPT_ARG_NONE, &dynamicDistance, 0, "Display instead of saving image, using dynamic display of the distance.", NULL},
  { "dynamic", 0, POPT_ARG_NONE, &dynamic, 0, "Display dynamic view (2d + 3d window).", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  LengthFactory lFactory;

  poptContext context = poptGetContext("meshUnfolder", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh unfolder", "Unfold a given selected component of the input mesh using unfoldign algorithms (available: circle packing and ABF). If needed, mesh is first cut to be homeomorphic to a disc.");
    hMsg << "Input: mesh to unfold (available format: VRML)";
    hMsg << "Output: display unfolded mesh or save it (available format: VRML, cset, image)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if ((length != NULL) && (strcmp(length, "list") == 0)) {
    cout << lFactory << endl;
    return 0;
  }

  if (postTreatment == -1) {
    cout << Mapping2D3D::helpAdjustMapping() << endl;
    return 0;
  }

  if ((overlaps != 0) && (randomValue != 0)) {
    cout << "Error: cannot use both overlaps and random method." << endl;
    return -1;
  }

  lFactory.setLength(length);
  if (!lFactory.existsLength()) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: length unknown (try -l list)." << endl;
    return 1;
  }

  std::string logFileReal = "";
  if (logFile != NULL)
    logFileReal = logFile;

  unsigned int idMethod = 0; // default: circle packing
  if (method == NULL) {
    std::cout << "Selecting Circle Packing as unfolding algorithm." << std::endl;
    idMethod = 0;
  }
  else if (strcmp(method, "circle-packing") == 0)
    idMethod = 0;
  else if (strcmp(method, "abf") == 0) {
    idMethod = 1;
    if (maxIter > 10000) {
      cout << "Maximum iteration value is higher than 10000. Assums that is the default value, set to 30." << endl;
      maxIter = 30;
    }
  }
  else {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Unknown unfolding method." << endl;
    return 1;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0) && (dynamicDistance == 0) && (dynamic == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select a display mode." << endl;
    return 1;
  }

  if (maxIter <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: maximum iteration not positive." << endl;
    return 1;
  }

  if (epsilon <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: epsilon not positive." << endl;
    return 1;
  }

  if ((display != 0) && (filename_output != NULL)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: incompatible options (display, image output)." << endl;
    return 1;
  }

  if ((xsize <= 0) || (ysize <= 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Output size not positive." << endl;
    return 1;
  }


  UnfoldingMethod * uMethod = NULL;

  try {
    /* load mesh */
    cout << "Loading mesh..." << endl;

    cout << "Building mesh..." << endl;
    /* build mesh then circle set */
    Mesh mesh(filename_input, objectName);

    CImgList<unsigned char> grid;


    cout << "Selecting connected component..." << endl;
    std::deque<MeshPart> cc = mesh.getConnectedComponents();
    if ((int)cc.size() == 1)
      cComponent = 0;
    else
      if (((int)cc.size() <= cComponent) || (cComponent < 0)) {
	cout << "Warning: connected component #" << cComponent << " unknown. Surface has " << cc.size() << " connected components."<< endl;
	cout << "  Selecting connected component by size" << std::endl;
	cComponent = 0;
	unsigned int nb = cc[0].getNbPoints();
	unsigned int i = 0;
	for(std::deque<MeshPart>::iterator m = cc.begin(); m != cc.end(); ++m) {
	  if ((*m).getNbPoints() > nb) {
	    cComponent = i;
	    nb = (*m).getNbPoints();
	  }
	  ++i;
	}
      }
    Mesh meshCropped(cc[cComponent]);

    if (ratio >= 0)
      lFactory.setRatio(ratio);
    MeshCut meshCutter(meshCropped, lFactory.getLength(meshCropped));
    try {
      cout << "Cutting surface... " << endl;
      meshCutter.cutMeshUsingMinimalCut();
    }
    catch(Exception e) {
      cerr << endl << "Error: " << e << endl;
      return -1;
    }

    unsigned int sizeCut = meshCutter.getSize();
    if (sizeCut != 0)
      cout << " Cut size: " << sizeCut << ". OK" << endl;
    else
      cout << " OK" << endl;

    Mapping2D3D mapping(meshCropped);

    if (idMethod == 0) {
      cout << "Circle packing: unfolding..." << endl;
      CPMethod * cset = new CPMethod(mapping, (variant == 0));
      uMethod = cset;
    }
    else if (idMethod == 1) {
      cout << "ABF: unfolding..." << endl;
      ABFMethod * abf = new ABFMethod(mapping);
      uMethod = abf;
    }
    else {
      cerr << "Unknown method." << endl;
      return -1;
    }

    bool readjust = (mscaleMethod == NULL) || (string(mscaleMethod) == "readjust");
    MultiscaleUnfolding mSUnfolding(readjust, epsilon, maxIter, true, logFileReal);
    int nbIter = mSUnfolding.processUnfolding(*uMethod, mscaleDepth);

    if (nbIter < 0)
      throw Exception("Abording.");

    std::cout << "nbPoints: " << meshCropped.getNbPoints() << std::endl;
    std::cout << "nbTriangles: " << meshCropped.getNbTriangles() << std::endl;
    int nbNeighbours = 0;
    for(Mesh::const_point_iterator i = meshCropped.point_begin(); i != meshCropped.point_end(); ++i)
      nbNeighbours += (*i).getNbNeighbours();
    std::cout << "nbNeighbours: " << nbNeighbours << std::endl;

    IndentManager iManager;
    mapping.adjustMapping(postTreatment, postTreatmentIt, epsilon, iManager);

    if ((filename_output != NULL) && mapping.isSaveFormat(filename_output)) {
      cout << "Export unfolding mapping..." << endl;
      mapping.save(filename_output);
    }
    else if ((filename_output != NULL) && (idMethod == 0) && ((*((CPMethod *)uMethod)).isSaveFormat(filename_output))) {
      cout << "Saving circle set..." << endl;
      (*((CPMethod *)uMethod)).save(filename_output);
    }
    else if ((filename_output != NULL) && mesh.isSaveFormat(filename_output)) {
      cout << "Saving mesh..." << endl;
      Mesh mesh2(mapping);
      mesh2.save(filename_output);
    }
    else {
      if (filename_output != NULL) {
	CImg<unsigned char> result(xsize, ysize);
	result.fill(cimg::type<unsigned char>::min());

	if (randomValue)
	  mapping.toImageRandom(result);
	else if (overlaps) {
	  std::cout << "Computing the overlaps" << std::endl;
	  mapping.toImageOverlap(result);
	}

	if (gridStep != -1) {
	  meshCropped.computeGridValues(grid, gridStep);
	  mapping.toImage(result, grid);
	}

	if (border != 0)
	  mapping.toImageDrawBorder(result, (unsigned char)0);

	if (borderTriangles != 0)
	  mapping.toImageDrawBorderTriangles(result, (unsigned char)0);


	/* save result image */
	cout << "Saving image..." << endl;
	result.save(filename_output);
      }
      if (dynamicDistance != 0)
	Display3D::displayMappingDynamicDistance(xsize, ysize, "3D Visualisation", mapping);
      else
      if (dynamic != 0)
	Display3D::displayMappingDynamicMap(xsize, ysize, "Visualisation", mapping);
      else
	if (display != 0) {
	  if (overlaps != 0) {
	    CImg<unsigned char> result(xsize, ysize);
	    result.fill(cimg::type<unsigned char>::min());
	    mapping.toImageOverlap(result);
	    CImgDisplay disp(result);
	    while (!disp._is_closed) {
	      disp.wait();
	      if (disp.key() == cimg::keyESC) {
		break;
	      }
	    }
	  }
	  else
	    Display3D::displayMapping(xsize, ysize, "3D Visualisation", mapping, false, borderTriangles != 0, border != 0);
	}

    }
  }
  catch(Exception e) {
    cerr << e << "Abort." << endl;
    if (uMethod != NULL)
      delete uMethod;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    if (uMethod != NULL)
      delete uMethod;
    return 3;
  }

  if (uMethod != NULL)
    delete uMethod;
  return 0;
}
