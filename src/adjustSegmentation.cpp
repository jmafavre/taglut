/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <popt.h>
#include "CImgUse.h"
#include <assert.h>
#include <sstream>
#include <limits>


#include "Messages.h"

using namespace std;
using namespace Taglut;

static char* filename_t  = NULL;
static char* filename_output  = NULL;
static double zratio = 1.0;
static int non_labeled = 0;
static int help = 0;
static int offset = 5;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_t, 0, "Input image", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output image", NULL},
  { "non-labeled", 'n', POPT_ARG_INT, &non_labeled, 0, "Non labeled value", NULL},
  { "zratio", 'z', POPT_ARG_DOUBLE, &zratio, 0, "Zratio", NULL},
  { "offset", 0, POPT_ARG_INT, &offset, 0, "Offset of the \"dilatation\"", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};




int main (int argc, const char **argv) {
  poptContext context = poptGetContext("adjustSegmentation", argc, argv, options, 0);

  /* check values */

  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Adjust segmentation", "Adjust the segmentation by modifying the non labeled values according to the shortest labeled point.");
    hMsg << "Input: image to view";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_t == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if (filename_output == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }




  try {
    /* load image */
    CImg<> img(filename_t);
    CImg<> imgres(img);
    imgres.fill(0);

    cimg_forXYZ(img, x, y, z) {
      if (img(x, y, z) != non_labeled) {
        std::cout << "\rinit, z: " << z << ", y: " << y << ", x: " << x;
        std::cout.flush();
        imgres.draw_rectangle(x - offset, y - offset, z - offset, 0, x + offset, y + offset, z + offset, 0, (float)1.0);
      }
    }
    std::cout << "next" << std::endl;
    double zratio2 = zratio * zratio;

    cimg_forXYZ(img, x, y, z) {
	if ((x == 0) && (y % 5 == 0)) {
	  std::cout << "\rstep, z: " << z << ", y: " << y;
	  std::cout.flush();
	}
      if (imgres(x, y, z) == 1) {
	double distance = sqrt(x * x + y * y + z * z * zratio2);
	float value = img(x, y, z);
	bool found = false;
	for(int x2 = x - offset; x2 <= x + offset; ++x2) {
	  for(int y2 = y - offset; y2 <= y + offset; ++y2)
	    for(int z2 = z - offset; z2 <= z + offset; ++z2)
	      if ((x2 >= 0) && (y2 >= 0) && (z2 >= 0) &&
		  (x2 < (int)img._width) && (y2 < (int)img._height) && (z2 < (int)img._depth)) {
		double d2 = sqrt(x2 * x2 + y2 * y2 + z2 * z2 * zratio2);
		if (((!found) || (d2 < distance)) && (img(x2, y2, z2) != non_labeled)) {
		  found = true;
		  distance = d2;
		  value = img(x2, y2, z2);
		}
	      }
        if ((x2 > x) && (x2 - x >  distance))
	  break;
        }
	imgres(x, y, z) = value;
      }
    }

    imgres.save(filename_output);

  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  poptFreeContext(context);
  return 0;
}


