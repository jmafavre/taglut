/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "Display3D.h"

#include "Messages.h"

using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static double centerx = -1;
static double centery = -1;
static double centerz = -1;
static double radius = 20;
static int    autoR = 0;
static int    display = 0;
static int    noSC = 0;
static int    help = 0;


struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "centerx", 'x', POPT_ARG_DOUBLE, &centerx, 0, "Disc's center (x coords)", NULL},
  { "centery", 'y', POPT_ARG_DOUBLE, &centery, 0, "Disc's center (y coords)", NULL},
  { "centerz", 'z', POPT_ARG_DOUBLE, &centerz, 0, "Disc's center (z coords)", NULL},
  { "radius", 'r', POPT_ARG_DOUBLE, &radius, 0, "Disc radius", NULL},
  { "auto", 'a', POPT_ARG_NONE, &autoR, 0, "Choose maximal radius", NULL},
  { "keep-all", 'k', POPT_ARG_NONE, &noSC, 0, "Keep all points inside disc (no simple cut)", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("maskCropDisc", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh crop disc", "Crop mesh using a circle defined by center and radius. Default center location: (0, 0, 0). Default radius: 20.");
    hMsg << "Input: mesh to display (available format: VRML)";
    hMsg << "Output: display mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {

    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
  }


  if (radius <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify positive radius." << endl;
    return 1;
  }


  CImgList<double> points;
  CImgList<VertexID> primitives;
  Point3D center(centerx, centery, centerz);
  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename_input, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }


  MeshPart * part;
  if (autoR)
    /* crop mesh */
    part = new MeshPart(mesh, center);
  else
    /* crop mesh using given radius */
    part = new MeshPart(mesh, center, radius);

  /* build resulting mesh */
  if (noSC)
    (*part).cropMesh();
  else
    (*part).cropSimpleCut();

  /* save mesh */
  if (filename_output != NULL)
    try {
      mesh.save(filename_output, objectName);
    } catch (Exception e) {
      cerr << e << endl;
      return -1;
    }


  if (display != 0)
    Display3D::displayMesh(256, 256, "3D Visualisation (crop disc)", mesh, false, true);

  /* clear memory */
  delete part;


  return 0;
}
