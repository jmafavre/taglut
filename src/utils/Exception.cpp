/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Exception.h"

using namespace Taglut;

Exception::Exception():msg() {}

Exception::Exception(const std::string & msg_l) : msg(msg_l) {}

Exception::Exception(const char * msg_l) : msg(msg_l) {}

std::string Exception::getMessage() const { return msg; }


ExceptionTopology::ExceptionTopology() : Exception() {}

ExceptionTopology::ExceptionTopology(const std::string & msg_l) : Exception(msg_l) {}

FatalException::FatalException() : Exception() {}

FatalException::FatalException(const std::string & msg_l) : Exception(msg_l) {}
