/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdlib.h>
#include "StringManipulation.h"

using namespace Taglut;

bool StringManipulation::isUnsignedInt(const std::string & str) {
  int loc = str.find_first_not_of("0123456789");

  return (loc < 0) || (loc >= (int)(str.size()));
}

bool StringManipulation::isSignedInt(const std::string & str) {
  int loc = str.find_first_not_of("0123456789+-");

  if ((loc >= 0) && (loc < (int)(str.size())))
    return false;

  // check for number of "+" and "-"
  if ((str[0] == '+') || (str[0] == '-')) {
    int locpm = str.find_last_of("+-");
    if (locpm != 0)
      return false;
  }

  return true;
}

bool StringManipulation::isUnsignedFloat(const std::string & str) {
  int loc = str.find_first_not_of("0123456789.");

  if ((loc >= 0) && (loc < (int)(str.size())))
    return false;

  int firstloc = str.find_first_of('.');
  int lastloc = str.find_last_of('.');
  if ((firstloc < 0) || (firstloc >= (int)(str.size())))
    return true;
  else return firstloc == lastloc;
}

bool StringManipulation::isSignedFloat(const std::string & str) {
  int loc = str.find_first_not_of("+-0123456789.e");

  if ((loc >= 0) && (loc < (int)(str.size())))
    return false;


  // check for number of "e"
  int firstloc = str.find_first_of('e');
  int lastloc = str.find_last_of('e');
  int nbSign = 1;
  if ((firstloc >= 0) && (firstloc < (int)(str.size()))) {
    if (firstloc != lastloc)
      return false;
    else
      nbSign = 2;
  }

  // check for number of "."
  firstloc = str.find_first_of('.');
  lastloc = str.find_last_of('.');
  if ((firstloc >= 0) && (firstloc < (int)(str.size())) && (firstloc != lastloc))
    return false;


  // check for number of "+" and "-"
  if ((str[0] == '+') || (str[0] == '-')) {
    int locpm = str.find_last_of("+-");
    if ((locpm != 0) && (nbSign == 1))
      return false;
  }

  return true;
}


bool StringManipulation::isUnsignedPercent(const std::string & str) {
  if ((str.size() == 0) || (str[str.size() - 1] != '%'))
    return false;

  int loc = str.find_first_not_of("0123456789.");

  if(loc != (int)(str.size()) - 1)
    return false;

  int firstloc = str.find_first_of('.');
  int lastloc = str.find_last_of('.');
  if ((firstloc < 0) || (firstloc >= (int)(str.size())))
    return true;
  else return firstloc == lastloc;
}

bool StringManipulation::isSignedPercent(const std::string & str) {
  if ((str.size() == 0) || (str[str.size() - 1] != '%'))
    return false;

  int loc = str.find_first_not_of("+-0123456789.");

  if(loc != (int)(str.size()) - 1)
    return false;

  // check for number of "."
  int firstloc = str.find_first_of('.');
  int lastloc = str.find_last_of('.');
  if ((firstloc >= 0) && (firstloc < (int)(str.size())))
    return firstloc == lastloc;

  // check for number of "+" and "-"
  if ((str[0] == '+') || (str[0] == '-')) {
    int locpm = str.find_last_of("+-");
    if (locpm != 0)
      return false;
  }

  return true;
}


double StringManipulation::getPercent(const std::string & str) {
  return atof(str.substr(0, str.size() - 1).c_str()) / 100;
}

int StringManipulation::getInt(const std::string & str) {
  return atoi(str.c_str());
}


VertexID StringManipulation::getVertexID(const std::string & str) {
  VertexID result = 0;
  for(unsigned int i = 0; i < str.size(); ++i) {
    result *= 10;
    result += str[i] - '0';
  }

  return result;
}


double StringManipulation::getFloat(const std::string & str) {
  return atof(str.c_str());
}


bool StringManipulation::isFunction(const std::string & str, int nbArgs) {
  int nbRealArgs = getNbArgsFunction(str);

  return (nbRealArgs != -1) && ((nbArgs < 0) || (nbArgs == nbRealArgs));
}

bool StringManipulation::isFunction(const std::string & str, const std::string & name, int nbArgs) {
  int nbRealArgs = getNbArgsFunction(str);

  return (nbRealArgs != -1) && (getFunctionName(str) == name) && ((nbArgs < 0) || (nbArgs == nbRealArgs));
}


int StringManipulation::getNbArgsFunction(const std::string & str) {
  int locFirst1 = str.find_first_of('(');
  int locLast1 = str.find_last_of('(');
  int locFirst2 = str.find_first_of(')');
  int locLast2 = str.find_last_of(')');

  if ((locFirst1 != locLast1) || (locFirst2 != locLast2) ||
      (locFirst1 < 0) || (locFirst1 >= (int)str.size()) ||
      (locFirst2 < 0) || (locFirst2 >= (int)str.size()) ||
      (locFirst1 > locFirst2) || (locFirst1 == 0) || (locFirst2 != (int)str.size() - 1))
    return -1;

  // compute parameters
  std::deque<std::string> parameters = getParameters(str);
  // check for non empty parameters
  for(std::deque<std::string>::const_iterator s = parameters.begin(); s != parameters.end(); ++s)
    if ((*s) == "")
      return -1;
  return parameters.size();
}

std::string StringManipulation::getFunctionName(const std::string & str) {
  unsigned int loc = str.find_first_of('(');

  return str.substr(0, loc);
}

std::deque<std::string> StringManipulation::getParameters(const std::string & str) {
  unsigned int locfirst = str.find_first_of('(');
  unsigned int loclast = str.find_first_of(')');
  std::string parameters = str.substr(locfirst + 1, loclast - locfirst - 1);

  return splitString(parameters, ',', true);
}

std::deque<std::string> StringManipulation::splitString(const std::string & str, char separator, bool stripSpaces) {
  std::deque<std::string> result;
  std::string strReal = str;

  while(strReal.size() != 0) {
    unsigned int pos = strReal.find(separator);
    if (pos >= strReal.size()) {
      if (stripSpaces)
	result.push_back(strip(strReal));
      else
	result.push_back(strReal);

      strReal = "";
      break;
    }
    else {
      if (stripSpaces)
	result.push_back(strip(strReal.substr(0, pos)));
      else
	result.push_back(strReal.substr(0, pos));
      strReal = strReal.substr(pos + 1, strReal.size() - pos);
    }
  }

  return result;
}

std::string StringManipulation::strip(const std::string & str) {
  std::string::size_type const first = str.find_first_not_of(" \t");
  if (first == std::string::npos)
    return "";
  else
    return str.substr(first, str.find_last_not_of(" \t") - first + 1);
}

std::string StringManipulation::removeCR(const std::string & str) {
  std::string::size_type const first = str.find_first_of("\r");
  if (first == std::string::npos)
    return str;
  else
    return str.substr(0, first);
}
