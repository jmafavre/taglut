/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef FILE_MANIPULATOR
#define FILE_MANIPULATOR

#include <vector>
#include <map>
#include <string>
#include <fstream>

namespace Taglut {
  /**
     @class FileManipulator

     @author Jean-Marie Favreau
     @brief An abstract class that describe a loading and saving interface
  */
  class FileManipulator {
  protected:

    /** list of the load formats */
    std::vector<std::string> loadFormats;
    /** list of the save formats */
    std::vector<std::string> saveFormats;
    /** booleans describing the ascii property of the formats */
    std::map<std::string, bool> asciiFormat;

    /** adding a load/save format */
    void addFormat(const std::string & format, bool ascii = true);

    /** adding a load format */
    void addLoadFormat(const std::string & format, bool ascii = true);

    /** adding a save format */
    void addSaveFormat(const std::string & format, bool ascii = true);

    /** adding all the formats */
    virtual void addFormats() = 0;


    /**
       VRML saving tool
    */
    static std::string getHeaderVRML();

    /**
       VRML saving tool
    */
    static std::string getMiddleVRML();

    /**
       VRML saving tool
    */
    static std::string getFooterVRML();

    /**
       VRML saving tool
    */
    static std::string getTransformationsVRML(const std::string & name, unsigned int nbMeshes = 1);

    /**
       VRML loading tool
    */
    bool checkFormatVRML(std::ifstream & infile) const;

    /**
       VRML loading tool
    */
    bool skipBlockVRML(std::ifstream & infile) const;

  public:

    /** Constructor */
    FileManipulator();

    /** Copy constructor */
    FileManipulator(const FileManipulator & fm);

    /** Constructor using a file */
    FileManipulator(const std::string & fileName, const std::string & objectName = "");

    /** Destructor */
    virtual ~FileManipulator();


    /** Loading a file (with the wanted object) */
    void load(const std::string & fileName, const std::string & objectName = "");

#ifndef SWIG
    /** Loading a file (with the wanted object). "char *" version */
    void load(const char * fileName, const char * objectName = NULL);
#endif

    /** Loading a file (with the wanted object), with an alternate format given by \p altFormat */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string & objectName = "") = 0;

    /** Return true if the given file's format is a known format for loading */
    bool isLoadFormat(const std::string & fileName) const;



    /** Saving a file (with the wanted object) */
    void save(const std::string & fileName, const std::string & objectName = "") const;

#ifndef SWIG
    /** Saving a file (with the wanted object). "char *" version */
    void save(const char * fileName, const char * objectName = NULL) const;
#endif

    /** Saving a file (with the wanted object), with an alternate format given by \p altFormat */
    virtual void saveAltFormat(const std::string & fileName, const std::string & altFormat, const std::string & objectName = "") const = 0;

    /** Return true if the given file's format is a known format for saving */
    bool isSaveFormat(const std::string & fileName) const;


    /** Return true if the given file's format is an ascii format */
    bool isAsciiFormat(const std::string & fileName) const;

  };
}
#endif
