/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "FileExceptions.h"
using namespace Taglut;

ExceptionFile::ExceptionFile(const std::string & msg_l) : Exception(msg_l) { }

ExceptionFileNotFound::ExceptionFileNotFound() : ExceptionFile(std::string("File not found.")) { }

ExceptionUnknownFormat::ExceptionUnknownFormat() : ExceptionFile(std::string("Unknown format.")) { }

ExceptionDataNotFoundInFile::ExceptionDataNotFoundInFile() : ExceptionFile(std::string("Data not found in file.")) { }

ExceptionErrorDuringReadingFile::ExceptionErrorDuringReadingFile() : ExceptionFile(std::string("Error during reading file.")) { }

ExceptionEmptyData::ExceptionEmptyData() : ExceptionFile(std::string("Empty data.")) { }

ExceptionCannotOpenOutputFile::ExceptionCannotOpenOutputFile() : ExceptionFile(std::string("Cannot open output file.")) { }

ExceptionDuringWritingFile::ExceptionDuringWritingFile() : ExceptionFile(std::string("Error during writing file.")) { }

ExceptionXMLReaderNotAvailable::ExceptionXMLReaderNotAvailable() : ExceptionFile(std::string("XML reader not available.")) { }

ExceptionXMLReaderNotAvailable::ExceptionXMLReaderNotAvailable(const std::string & msg_l) : ExceptionFile("XML reader not available. " + msg_l) { }
