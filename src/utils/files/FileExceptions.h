/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef FILE_EXCEPTIONS
#define FILE_EXCEPTIONS

#include "Exception.h"

namespace Taglut {
  /**
     @class ExceptionFile

     @author Jean-Marie Favreau
     @brief Exception generated when IO error occurs
  */
  class ExceptionFile : public Exception {
  public:
    /** default constructor */
    ExceptionFile(const std::string & msg);
  };

  /**
     @class ExceptionFileNotFound

     @author Jean-Marie Favreau
     @brief Exception generated when an input file is not found
  */
  class ExceptionFileNotFound : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionFileNotFound();
  };

  /**
     @class ExceptionUnknownFormat

     @author Jean-Marie Favreau
     @brief Exception generated when an extension is unknown
  */
  class ExceptionUnknownFormat : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionUnknownFormat();
  };

  /**
     @class ExceptionDataNotFoundInFile

     @author Jean-Marie Favreau
     @brief Exception generated when data needed is not found in an input file
  */
  class ExceptionDataNotFoundInFile : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionDataNotFoundInFile();
  };

  /**
     @class ExceptionErrorDuringReadingFile

     @author Jean-Marie Favreau
     @brief Exception generated when error occurs during reading file
  */
  class ExceptionErrorDuringReadingFile : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionErrorDuringReadingFile();
  };

  /**
     @class ExceptionEmptyData

     @author Jean-Marie Favreau
     @brief Exception generated when data is empty
  */
  class ExceptionEmptyData : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionEmptyData();
  };

  /**
     @class ExceptionCannotOpenOutputFile

     @author Jean-Marie Favreau
     @brief Exception generated when output file cannot be opened
  */
  class ExceptionCannotOpenOutputFile : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionCannotOpenOutputFile();
  };

  /**
     @class ExceptionDuringWritingFile

     @author Jean-Marie Favreau
     @brief Exception generated when an error occurs during writing file
  */
  class ExceptionDuringWritingFile : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionDuringWritingFile();
  };

  /**
     @class ExceptionXMLReaderNotAvailable

     @author Jean-Marie Favreau
     @brief Exception generated when XML reader is not available
  */
  class ExceptionXMLReaderNotAvailable : public ExceptionFile {
  public:
    /** default constructor */
    ExceptionXMLReaderNotAvailable();

    /** constructor with string parameter */
    ExceptionXMLReaderNotAvailable(const std::string & msg);
  };
}

#endif
