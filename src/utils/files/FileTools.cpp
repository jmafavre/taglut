/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "FileTools.h"
#include "Exception.h"
#include "StringManipulation.h"
#include <sstream>
#include <assert.h>
#include <math.h>
#include <fstream>



using namespace std;
using namespace Taglut;

std::string FileTools::concat(const std::string & file1, const std::string & file2) {
  if (file1 == "")
    return file2;
  else if (file2 == "")
    return file1;
  else
    return file1 + SEPARATOR + file2;
}


std::string FileTools::parentdir(const std::string & fileName) {
  return path(fileName);
}

std::string FileTools::path(const std::string & fileName) {
  int lastSep = fileName.find_last_of(SEPARATOR);
  if ((lastSep >= (int)fileName.size()) || (lastSep < 0))
    return "";
  else
    return fileName.substr(0, lastSep);
}

std::string FileTools::basename(const std::string & fileName) {
  int lastDot = fileName.find_last_of(".");
  int lastSep = fileName.find_last_of(SEPARATOR);
  return fileName.substr(lastSep + 1, lastDot - lastSep - 1);
}

std::string FileTools::skipExtension(const std::string & fileName) {
  int lastDot = fileName.find_last_of(".");
  return fileName.substr(0, lastDot);
}

std::string FileTools::extension(const std::string & fileName) {
  int lastDot = fileName.find_last_of(".");
  return fileName.substr(lastDot + 1);
}

bool FileTools::isAbsoluteName(const std::string & fileName) {
#ifdef WIN32
  return (fileName[1] == ':');
#else
  return (fileName[0] == '/');
#endif
}

std::string FileTools::realPath(const std::string & fileName) {
  deque<string> splitFileName = splitPath(fileName);
  deque<string> splitFileNameClean;
  std::string result;
  bool first = true;

  for(deque<string>::const_iterator str = splitFileName.begin(); str != splitFileName.end(); ++str)
    if (*str != ".") {
      if (*str == "..")
	if ((splitFileNameClean.size() == 0) || (splitFileNameClean.back() == ".."))
	  splitFileNameClean.push_back(*str);
	else
	  splitFileNameClean.pop_back();
      else
	if ((*str != "") || (str == splitFileName.begin()))
	  splitFileNameClean.push_back(*str);
    }

  for(deque<string>::const_iterator str = splitFileNameClean.begin(); str != splitFileNameClean.end(); ++str) {
    if (!first)
      result += SEPARATOR;
    result += *str;
    first = false;
  }

  return result;
}

std::deque<std::string> FileTools::splitPath(const std::string & fileName) {
  return StringManipulation::splitString(fileName, SEPARATOR);
}



std::string FileTools::computeFileName(const std::string & file, unsigned int nb, unsigned int id) {
  assert(id < nb);

  if (nb == 1)
    return file;
  unsigned int nbChar = (unsigned int) log10(nb - 1);

  // build start of string
  std::string result = skipExtension(file) + "-";

  if (id == 0) {
    for(unsigned int i = 0; i < nbChar + 1; ++i)
      result += "0";
  }
  else {
    std::ostringstream str;
    str << id;

    if (log10(nb - 1) != log10(id))
      for(unsigned int i = 0; i < nbChar - log10(id); ++i)
	result += "0";
    result += str.str();
  }

  return result + "." + extension(file);
}

std::string FileTools::getAvailableFileName(const std::string & prefix, const std::string & suffix) {
  unsigned int nb = 0;
  std::ostringstream str;
  bool found = false;
  do {
    str.str("");
    str << prefix;
    if (nb != 0)
      str << "-" << nb;
    str << "." << suffix;

    ++nb;
    std::ifstream fileS(str.str().c_str(), std::ios::in | std::ios::binary);
    found = !(fileS.is_open());
  } while (!found);

  return str.str();
}
