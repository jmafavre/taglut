/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef FILE_TOOLS_H
#define FILE_TOOLS_H

#include <string>
#include <deque>


/**
   If OS is Microsoft Windows, separator is \, else separator is /
 */
#if defined(_WIN32) || defined(__WIN32__)
#define SEPARATOR '\\'
#else
#define SEPARATOR '/'
#endif

namespace Taglut {
  /**
     @class FileTools
     @brief Virtual class with simple functions
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
  */
  class FileTools {
  public:

    /**
       @return parent directory
    */
    static std::string parentdir(const std::string & fileName);

    /**
       @return path
    */
    static std::string path(const std::string & fileName);

    /**
       @return basename (without path and extension)
    */
    static std::string basename(const std::string & fileName);

    /**
       @return file without extension
    */
    static std::string skipExtension(const std::string & fileName);

    /**
       @return extension
    */
    static std::string extension(const std::string & fileName);

    /**
       @return true if filename is absolute one, false otherwise
    */
    static bool isAbsoluteName(const std::string & fileName);

    /**
       @return realpath (remove "./", "../")
    */
    static std::string realPath(const std::string & fileName);

    /**
       Split path according to SEPARATOR char
    */
    static std::deque<std::string> splitPath(const std::string & fileName);

    /**
       @return concatenate path
    */
    static std::string concat(const std::string & file1, const std::string & file2);


    /**
       Given a filename and the number of files that will be computed, compute
       filename of the id.
       @param file input filename
       @param nb number of files
       @param id current id
       @return computed filename
    */
    static std::string computeFileName(const std::string & file, unsigned int nb, unsigned int id);

    /**
       Given a prefix and a suffix, compute an available file name adding characters if needed
       at the end of the prefix.
       @param prefix prefix
       @param suffix suffix
       @return computed filename
    */
    static std::string getAvailableFileName(const std::string & prefix, const std::string & suffix);

  };
}

#endif

