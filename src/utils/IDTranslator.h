/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <j-marie.favreau@u-clermont1.fr>
 *                    ISIT / Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "IDTypes.h"
#include "PointOnEdge.h"
#include "Mesh.h"

#ifndef ID_TRANSLATOR
#define ID_TRANSLATOR

namespace Taglut {
  class PLPath;
  class MeshPart;
  class Mesh;

#ifndef SWIG
  /**
     @class IDTranslator
     @brief a class that store translation for ids (local to global and global to local)

  */
  class IDTranslator {
  private:
    /** local-to-global ids */
    std::vector<VertexID> l2gIDs;
    /** global-to-local ids */
    std::map<VertexID, VertexID> g2lIDs;

  public:
    /** constructor using a MeshPart */
    IDTranslator(const MeshPart & mPart);

    /** translate from local to global */
    inline PointOnEdge l2g(const PointOnEdge & v, const Mesh & mesh) const {
      PointOnEdge result(v);
      result.setFirstVertex(l2gIDs[result.getFirstVertex()]);
      result.setSecondVertex(l2gIDs[result.getSecondVertex()]);
      result.adjustLocation(mesh.point(result.getFirstVertex()), mesh.point(result.getSecondVertex()));
      return result;
    }

    /** translate from local to global */
    inline VertexID l2g(VertexID v) const {
      return l2gIDs[v];
    }

    /** translate from global to local */
    inline VertexID g2l(VertexID v) const {
      std::map<VertexID, VertexID>::const_iterator l = g2lIDs.find(v);
      if (l == g2lIDs.end())
	throw Exception("g2l(1): cannot find id");
      else
	return (*l).second;
    }

    /** translate from local to global */
    template <typename T>
    inline T l2gVector(const T & vector) const {
      T result;
      for(class T::const_iterator p = vector.begin(); p != vector.end(); ++p)
	result.push_back(l2g(*p));
      return result;
    }

    /** translate from global to local */
    template <typename T>
    inline T g2lVector(const T & vector) const {
      T result;
      for(class T::const_iterator p = vector.begin(); p != vector.end(); ++p)
	result.push_back(g2l(*p));
      return result;
    }

    /** translate from the local to global, given the global mesh */
    PLPath l2gPLPath(const PLPath & vector, const Mesh & mesh) const;

  };
}
#endif
#endif
