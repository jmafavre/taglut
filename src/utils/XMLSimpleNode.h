/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef XML_SIMPLE_NODE
#define XML_SIMPLE_NODE

#include <libxml/xmlreader.h>
#include <deque>
#include <string>


namespace Taglut {
  /**
     @class XMLSimpleNode

     @author Jean-Marie Favreau
     @brief XML Node (without children)
  */
  class XMLSimpleNode : public std::deque<std::pair<std::string, std::string> > {
  private:
    std::string name;
    static const std::string emptyString;
  public:
    /**
       Default constructor
    */
    XMLSimpleNode();

    /**
       Copy constructor
    */
    XMLSimpleNode(const XMLSimpleNode & node);

#ifdef LIBXML_READER_ENABLED
    /**
       Main constructor (using xml reader to load current xml node)
    */
    XMLSimpleNode(xmlTextReaderPtr & reader);
#endif

    /**
       Add a new attribute
    */
    void addAttribute(const std::string & key, const std::string & value);

    /**
       Given an attribute key, return value if exists, otherwise return empty string
    */
    const std::string & getAttribute(const std::string & key) const;
  };
}

#endif
