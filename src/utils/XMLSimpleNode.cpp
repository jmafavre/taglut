/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "XMLSimpleNode.h"

using namespace Taglut;

const std::string XMLSimpleNode::emptyString = "";

XMLSimpleNode::XMLSimpleNode() : name("") {
}

XMLSimpleNode::XMLSimpleNode(const XMLSimpleNode & node) : std::deque<std::pair<std::string, std::string> >(node), name(node.name) {
}

#ifdef LIBXML_READER_ENABLED
XMLSimpleNode::XMLSimpleNode(xmlTextReaderPtr & reader) {
  // loading name
  name = (const char *) xmlTextReaderConstName(reader);

  // then loading all the attributes
  if (xmlTextReaderMoveToFirstAttribute(reader) > 0)
    do {
      addAttribute((char *) xmlTextReaderConstLocalName(reader), (char *) xmlTextReaderConstValue(reader));
    } while (xmlTextReaderMoveToNextAttribute(reader) > 0);

}
#endif

void XMLSimpleNode::addAttribute(const std::string & key, const std::string & value) {
  push_front(std::pair<std::string, std::string>(key, value));
}


const std::string & XMLSimpleNode::getAttribute(const std::string & key) const {
  for(std::deque<std::pair<std::string, std::string> >::const_iterator attribute = begin(); attribute != end(); ++attribute)
    if ((*attribute).first == key)
      return (*attribute).second;

  return emptyString;
}
