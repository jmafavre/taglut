/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESSAGES
#define MESSAGES


#include <deque>
#include <string>
#include <sstream>

namespace Taglut {
  /**
     @class Message
     A class used to produce shell messages (80 caracters)
  */
  class Message : public std::deque<std::string> {

  protected:
    /**
       Given a string, build another string with 80 caracters splits
    */
    static std::string messageCut80(const std::string message);

  public:

    /**
       Conversion to string
    */
    std::string toString() const;

    /**
       add a new line
    */
    Message & operator<<(const std::string & msg);

  };


  /**
     Stream output
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Message & m) {
    f << m.toString();
    return f;
  }


  /**
     Help message for binaries
  */
  class HelpMessage : public Message {
  public:
    /**
       Default constructor
    */
    HelpMessage(const std::string & bName, const std::string & helpMessage);

  };
}

#endif
