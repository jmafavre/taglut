/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "SurfaceToolbox.h"
using namespace Taglut;

double SurfaceToolbox::surface(const Coord3D & p1, const Coord3D & p2, const Coord3D & p3) {
  Coord3D n1 = p1 - p3;
  Coord3D n2 = p2 - p3;
  return fabs(n1.getX() * n2.getX() + n1.getY() * n2.getY() + n1.getZ() * n2.getZ());
}

double SurfaceToolbox::surface(const Coord2D & p1, const Coord2D & p2, const Coord2D & p3) {
  Coord2D n1 = p1 - p3;
  Coord2D n2 = p2 - p3;
  return fabs(n1.get2DX() * n2.get2DX() + n1.get2DY() * n2.get2DY());
}
