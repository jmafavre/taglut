/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <fstream>
#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include "MeshBExpr.h"
#include "FileTools.h"

using namespace Taglut;

const unsigned int MeshBExpr::OPERATION_SIMPLE = 0;
const unsigned int MeshBExpr::OPERATION_PARENTHESIS = 1;
const unsigned int MeshBExpr::OPERATION_NEGATION = 2;
const unsigned int MeshBExpr::OPERATION_AND = 3;
const unsigned int MeshBExpr::OPERATION_OR = 4;
const unsigned int MeshBExpr::OPERATION_ERROR = 5;


MeshBExpr::MeshBExpr(const std::string & rx, MeshList & meshes_t, const std::string & filenameScript_t) : meshes(meshes_t),
													  filenameScript(filenameScript_t) {
  lmR = rmR = NULL;
  boolexp = clearSpaces(rx);

  buildBoolExprStructure(boolexp);
}

MeshBExpr::~MeshBExpr() {
  if (lmR != NULL)
    delete lmR;
  if (rmR != NULL)
    delete rmR;
}

std::string MeshBExpr::clearSpaces(const std::string & rx) {
  unsigned int firstChar = rx.find_first_not_of(" \t");
  unsigned int lastChar = rx.find_last_not_of(" \t");

  if (firstChar > lastChar)
    return "";
  else
    return rx.substr(firstChar, lastChar - firstChar + 1);
}

void MeshBExpr::buildBoolExprStructure(const std::string & boolexp_t) {
  unsigned int location = 0;
  unsigned int parNb = 0;
  unsigned int size = boolexp_t.size();
  if (size == 0) {
    operation = OPERATION_ERROR;
    return;
  }

  while(location != size) {
    if (boolexp_t[location] == '(') {
      ++parNb;
    }
    else if (boolexp_t[location] == ')')
      if (parNb == 0) { // bad parenthesis order
	operation = OPERATION_ERROR;
	return;
      }
      else
	--parNb;
    else if ((parNb == 0) && (boolexp_t.substr(location).find("and") == 0)) {
      operation = OPERATION_AND;
      lmR = new MeshBExpr(boolexp_t.substr(0, location), meshes, filenameScript);
      rmR = new MeshBExpr(boolexp_t.substr(location + 3), meshes, filenameScript);
      return;
    }
    else if ((parNb == 0) && (boolexp_t.substr(location).find("or") == 0)) {
      operation = OPERATION_OR;
      lmR = new MeshBExpr(boolexp_t.substr(0, location), meshes, filenameScript);
      rmR = new MeshBExpr(boolexp_t.substr(location + 2), meshes, filenameScript);

      return;
    }
    ++location;
  }

  if (parNb != 0) { // bad parenthesis number
    operation = OPERATION_ERROR;
    return;
  }
  else if ((boolexp_t[0] == '(') && (boolexp_t[size - 1] == ')')) {
    operation = OPERATION_PARENTHESIS;
    lmR = new MeshBExpr(boolexp_t.substr(1, size - 2), meshes, filenameScript);
  }
  else if (boolexp_t[0] == '!') {
    operation = OPERATION_NEGATION;
    lmR = new MeshBExpr(boolexp_t.substr(1), meshes, filenameScript);
  }
  else
    operation = OPERATION_SIMPLE;

}

bool MeshBExpr::eval() {
  unsigned int v;
  const unsigned int size = boolexp.size();
  const unsigned int last = size - 1;

  if (operation == OPERATION_ERROR) // error
    throw Exception("Bad regular expression (\"" + boolexp + "\")");
  else if (operation == OPERATION_OR) {
    assert((lmR != NULL) && (rmR != NULL));
    return (*lmR).eval() || (*rmR).eval();
  }
  else if (operation == OPERATION_AND) {
    assert((lmR != NULL) && (rmR != NULL));
    return (*lmR).eval() && (*rmR).eval();
  }
  else if (operation == OPERATION_NEGATION) {
    assert(lmR != NULL);
    return !(*lmR).eval();
  }
  else if (operation == OPERATION_PARENTHESIS) {
    assert(lmR != NULL);
    return (*lmR).eval();
  }
  else { // operation = OPERATION_SIMPLE
    if (boolexp == "")
      throw Exception(std::string("Bad boolean expression (empty)"));
    else if (boolexp == "true")
      return true;
    else if (boolexp == "false")
      return false;
    else {
      unsigned int opPos;

      // check for binary operations
      opPos = boolexp.find("==");
      if (opPos < size)	return evalUInt(boolexp.substr(0, opPos - 1)) == evalUInt(boolexp.substr(opPos + 2));
      opPos = boolexp.find("!=");
      if (opPos < size) return evalUInt(boolexp.substr(0, opPos - 1)) != evalUInt(boolexp.substr(opPos + 2));
      opPos = boolexp.find(">=");
      if (opPos < size) return evalUInt(boolexp.substr(0, opPos - 1)) >= evalUInt(boolexp.substr(opPos + 2));
      opPos = boolexp.find("<=");
      if (opPos < size) return evalUInt(boolexp.substr(0, opPos - 1)) <= evalUInt(boolexp.substr(opPos + 2));
      opPos = boolexp.find(">");
      if (opPos < size) return evalUInt(boolexp.substr(0, opPos - 1)) > evalUInt(boolexp.substr(opPos + 1));
      opPos = boolexp.find("<");
      if (opPos < size) return evalUInt(boolexp.substr(0, opPos - 1)) < evalUInt(boolexp.substr(opPos + 1));

      // check boolean methods
      v = boolexp.find("fileExists");
      if (v == 0) {
	std::string file = boolexp.substr(v + 1);
	unsigned int fP = file.find_first_of("(");
	if (boolexp[last] != ')')
	  throw Exception("Bad expression (\"" + boolexp + "\")");
	file = file.substr(fP + 1, size - fP - 3);

	if (!FileTools::isAbsoluteName(file))
	  file = FileTools::realPath(FileTools::path(filenameScript) + SEPARATOR + file);

	std::ifstream fileS(file.c_str(), std::ios::in | std::ios::binary);
	return fileS.is_open();
      }

      if (boolexp == "mesh.isEmpty()")
	return meshes.isEmpty();
    }

    throw Exception("Unknown expression (\"" + boolexp + "\")");
  }
}


unsigned int MeshBExpr::evalUInt(const std::string & expr) {
  std::string e = clearSpaces(expr);

  char *endptr;
  const char *estr = e.c_str();

  double value = strtod(estr, &endptr);
  if (endptr != estr)
    return (unsigned int) value;


  if (expr == "mesh.getNbCC()")
    return meshes.getNbCC();
  if (expr == "mesh.getNbPoints()")
    return meshes.getNbPoints();
  if (expr == "mesh.getNbTriangles()")
    return meshes.getNbTriangles();
  if (expr == "mesh.getNbPointFlagValues()")
    return meshes.getNbPointFlagValues();

  throw Exception("Unknown uint expression (\"" + expr + "\")");
}
