/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_BEXPR
#define MESH_BEXPR

#include "MeshList.h"
#include "Exception.h"

namespace Taglut {
  /**
     @class MeshBExpr
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @brief Mesh regular expression evaluator
  */
  class MeshBExpr {
  private:
    /** operation value (simple) */
    static const unsigned int OPERATION_SIMPLE;
    /** operation value (parenthesis) */
    static const unsigned int OPERATION_PARENTHESIS;
    /** operation value (negation) */
    static const unsigned int OPERATION_NEGATION;
    /** operation value (and) */
    static const unsigned int OPERATION_AND;
    /** operation value (or) */
    static const unsigned int OPERATION_OR;
    /** operation value (error) */
    static const unsigned int OPERATION_ERROR;

    /** operation */
    unsigned int operation;
    /** left boolexp */
    MeshBExpr * lmR;
    /** right boolexp (may be empty) */
    MeshBExpr * rmR;
    /** boolexp string */
    std::string boolexp;
    /** mesh */
    MeshList & meshes;
    /** Script filename */
    const std::string & filenameScript;

    /** remove spaces at begining and end of string */
    static std::string clearSpaces(const std::string & rx);

    /** build boolexp structure according to the associated string */
    void buildBoolExprStructure(const std::string & boolexp);

    /** evaluation (return unsigned int value) */
    unsigned int evalUInt(const std::string & expr);

  public:

    /**
       Default constructor
    */
    MeshBExpr(const std::string & rx, MeshList & meshes, const std::string & filenameScript);

    /**
       Destructor
    */
    ~MeshBExpr();

    /**
       Boolexp evaluation
    */
    bool eval();

  };
}

#endif
