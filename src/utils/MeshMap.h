/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_MAP
#define MESH_MAP

#include <vector>
#include <list>

#include "IDTypes.h"
#include "Coord3D.h"
#include "Point3D.h"
#include "FileManipulator.h"
#include "IDTranslator.h"


namespace Taglut {
  class Mesh;
  class MeshManipulator;
  class MeshPart;
  class PLPath;
  class PointOnEdge;


  /**
     @class MeshMapT

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-02-24
     @brief Mesh map: a double value is associated for each point of the mesh
  */
  template <typename T>
  class MeshMapT : public FileManipulator {
  protected:
    /**
       Mesh that is the support of the current map
    */
    Mesh * mesh;

    /**
       Array of values associated for each point of the mesh
    */
    T * values;

    /**
       Number of values (should be equal to the number of points on the mesh)
    */
    unsigned int nbValues;

    /**
       Adding the supported saving and loading formats
    */
    inline void addFormats() {
      addFormat("txt");
      addFormat("usr");
      addFormat("hdr");
      addFormat("dcm");
      addFormat("ply");
      addSaveFormat("vtk");
    }

    /**
       For the given point, compute in its neighbourhood the number of values' changes (up or down)
       according to the value of p (that may be given in the second parameter
    */
    unsigned int getNeighbourArity(const Point3D & p, T * curValue = NULL) const;

  public:

    /**
       Empty constructor
    */
    MeshMapT();

    /**
       Default constructor
    */
    MeshMapT(Mesh & mesh, T value = 1);

    /**
       Constructor using the angle value according to an axis. \p angle0 corresponds to the non-continuity of the values
    */
    MeshMapT(Mesh & mesh, const Direction & d, double angle0);

    /**
       Constructor using the axis location as value
    */
    MeshMapT(Mesh & mesh, const Direction & d);

    /**
       Create a meshmap on the mesh from a part of an original mesh, and the corresponding meshmap
       @param mesh The submesh
       @param meshPart The description of the inclusion (with the mapping of vertices)
       @param refMeshMap The reference MeshMapT where the values will be taken
    */
    MeshMapT(Mesh & mesh, const MeshPart & meshPart, const MeshMapT<T> & refMeshMap);

    /**
       build a function defined by the plane (one side with negative values, one side with
       positive values, and value defined by distance to the plane)
    */
    MeshMapT(Mesh & mesh, const Plane3D & plane);


    /**
       Copy constructor
    */
    MeshMapT(const MeshMapT<T> & mMap);

    /**
       Load values from a file, without connecting it to a mesh
    */
    MeshMapT(const std::string & filename);

    /**
       Load values from a file
    */
    MeshMapT(Mesh & mesh, const std::string & filename);

#ifndef SWIG
    /**
       Load values from a file
    */
    MeshMapT(Mesh & mesh, const char * filename);
#endif

    /** "copy" constructor using a new mesh */
    MeshMapT(const MeshMapT<T> & mMap, Mesh & mesh);

    /**
       Destructor
    */
    ~MeshMapT() {
      if (values != NULL)
	delete [] values;
    }

    /**
       Load Mesh from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Save Mesh to file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;

    /**
       Save values to file (ascii format)
    */
    void saveTXT(const std::string & fileName) const;

    /**
       Save values (and mesh) to file (vtk ascii format)
    */
    void saveVTK(const std::string & fileName) const;

    /**
       Load values from a file (ascii format)
    */
    void loadTXT(const std::string & fileName);

    /**
       Load MeshMap from file (volumic image, like analyze or dicom). Assuming that a mesh has been associated to
       the current mesh map, the values of the meshmap are computed using the coordinates of the vertices on the image
    */
    void loadFromVolumicImage(const std::string & fileName, double ratio = 1.);

    /**
     Save the current meshmap (and mesh) as a ply file
    */
    void savePLY(const std::string & fileName) const;

    /**
      load the PLY file on the current structure, assuming that it have an intensity field.
      If the instance contains a mesh, the PLY description of the mesh overwrite the current mesh.
     */
    void loadPLY(const std::string & fileName);

    /**
       Set the mesh that will be manipulated
    */
    inline const Mesh & getMesh() const { return *mesh; }

    /** return true if the current structure contains a mesh */
    inline bool hasMesh() const { return mesh != NULL; }

    /**
       Set the mesh that will be manipulated
    */
    void setMesh(Mesh & mesh);

    /**
       Set the mesh that will be manipulated, with an initial value
    */
    void setMesh(Mesh & mesh, T value);

    /**
       Update the mesh by adding the possible missing points
       \see addMissingValuesUsingMean
    */
    void updateMesh(Mesh & mesh);

    /**
       return the number of values in the current mesh map
    */
    inline VertexID getNbValues() const {
      return nbValues;
    }

    /**
       Accessor for the map value associated to a given point defined by \p id
    */
    inline const T & getValue(VertexID id) const {
      if (values == NULL)
	throw Exception("getValue(1): values not defined.");
      if (id >= nbValues)
	throw Exception("getValue(1): id out of bounds.");
      return values[id];
    }

    /**
       set the values according to the flags
    */
    void setValuesFromFlags();

    /**
       set the values according to lengths
    */
    void setValuesFromLengths(const MeshManipulator & mManip);

#ifndef SWIG
    /**
       Accessor for the map value associated to a given point defined by \p id
    */
    inline const T & operator[] (VertexID id) const {
      if (values == NULL)
	throw Exception("operator[](1): values not defined.");
      if (id >= nbValues)
	throw Exception("operator[](1): id out of bounds.");
      return values[id];
    }

    /**
       Accessor for the map value associated to a given point defined by \p id.
    */
    inline T & operator[] (VertexID id) {
      if (values == NULL)
	throw Exception("operator[](1): values not defined.");
      if (id >= nbValues)
	throw Exception("operator[](1): id out of bounds.");
      return values[id];
    }
#endif

    /**
       Modifier for the map value associated to a given point defined by \p id
    */
    void setValue(VertexID id, T value);

    /**
       init all the values using the given one
    */
    void initValues(T value);

    /**
       Given a center defined by its point's id \p id, an initial value (\p epsilon),
       and a scale value (\p alpha), define value for each point of the mesh
    */
    void setValuesFromCenter(VertexID id, double alpha = 1.0, double epsilon = 1e-1);

    /**
       Given a center defined by a direction \p direction, an initial value (\p epsilon),
       and a scale value (\p alpha), define value for each point of the mesh
    */
    void setValuesFromDirection(const Direction & direction, double alpha = 1.0, double epsilon = 1e-1);

    /**
       Given a direction \p direction, it generates a scalar function corresponding
       to the coordinate of points along this axis
    */
    void setValuesFromAxis(const Direction & direction);

#ifndef SWIG
    /** assuming that \p input is a meshmap on a subpart of the mesh manipulated by the current
	meshmap, it exploits the corresponding \p translator to set values on the current meshmap
	from \p input
     */
    void setValues(const MeshMapT<T> & input, const IDTranslator & translator);

#endif

    /**
       For each point on the boundary with a value < threshold,
       add to the value the given one
     */
    void addOnBoundaries(T value, T threshold = 0.);

    /**
       return the maximum value of the mapping
    */
    T getMaxValue() const;

    /**
       return the minimum value of the mapping
    */
    T getMinValue() const;

    /**
       Assuming that the values along the given path have a single extremum (except first and last point),
       it returns the corresponding value.
     */
    T getExtremum(const std::vector<VertexID> & path) const;

    /**
       Using a Gaussian filter to smooth the values
    */
    void smoothValues(unsigned int nbiter = 1);

    /** return an epsilon value smallest than the smallest difference on an edge (except flat edges) */
    double getEpsilonOnEdge() const;

    /**
       Remove flat edges by introducing small values
    */
    void removeFlatEdges();

    /**
       Apply a log function filter to modify the values
    */
    void applyLog();

    /** add new points at the end of the current meshmap.
	the contained reference to the mesh is reseted.
     */
    void addValues(const std::vector<double> & newValues);

    /**
       If points as been added to the mesh, and assuming that the insertion has been done at the end
       of the list, update the values of this MeshMapT by adding the missing values and estimate it using
       the mean of its neighbours in the original mesh.
    */
    void addMissingValuesUsingMean();

    /**
       return the value using a linear approximation along the edge
    */
    T getValue(const PointOnEdge & point) const;

    /**
       Given a point on an edge, compute the associated connected component of the level set on the function
       defined by the current mesh map.
    */
    PLPath computeLevelSetCC(const PointOnEdge & point) const;

    /**
       Given a point on an edge, compute the associated connected component of the level set on the function
       defined by the current mesh map, walking in the direct way (using triangle ordering)
    */
    PLPath computeDirectLevelSetCC(const PointOnEdge & point) const;

    /**
       Given a value, compute the corresponding level sets. If \p direct = true,
       paths are oriented using the direct ordering (i.e. paths are oriented boundaries of the region defined by "< value").
    */
    std::vector<PLPath> computeLevelSet(double value, bool direct = false) const;

    /**
       Given an edge (i.e. 2 points) and a scalar value, return the corresponding point on edge, or
       throws an exception if the value is not between the values of p1 and p2
     */
    PointOnEdge getPointOnEdgeFromValueAndEdge(VertexID p1, VertexID p2, T value) const;

    /**
       given a path (that is probably an integral line) and a value, compute a point close to the path with
       the given value. This point is on the path if possible, or in an edge that is connected to the
       path with one triangle.
     */
    PointOnEdge getPointOnEdgeFromValueAndPath(const PLPath & path, double value) const;

    /**
       given a path and a value, compute the first point on the path with the given value.
     */
    PointOnEdge getPointOnEdgeFromValueAndPath(const std::vector<VertexID> & path, double value) const;

    /**
       given a path (that is probably an integral line) and a value, compute the connected
       component of level set associated to the given value that cross the given path
     */
    PLPath computeLevelSetFromValueAndPath(const PLPath & path, double value) const;

    /**
       Given a point on an edge, an adjacent triangle and the level set value associated to the point,
       compute the next edge point on the triangle that corresponds to the level set.
    */
    PointOnEdge getNextPointOnLevelSet(const PointOnEdge & point, TriangleID t, double lsvalue) const;

    /**
       Given a point (that is a vertex) and the level set value associated to the point,
       compute a possible next edge point that corresponds to the level set.
    */
    PointOnEdge getPossibleNextPointOnLevelSet(const PointOnEdge & point, double lsvalue) const;

    /**
       Given a point (that is a vertex), the level set value associated to the point, and a previously found point,
       compute the next edge point that corresponds to the level set.
    */
    PointOnEdge getOtherPossibleNextPointOnLevelSet(const PointOnEdge & point, double lsvalue, const PointOnEdge & pointfirst) const;

    /**
       return true if the given points has the same value
    */
    inline bool isFlatEdge(VertexID p1, VertexID p2) const {
      return values[p1] == values[p2];
    }

    /**
       Return the saddle points (and the corresponding complexity of the saddle) of the scalar function described by the mesh map.
    */
    std::vector<std::pair<VertexID, unsigned int> > getSaddlePoints() const;

    /**
       return the number of local extrema
    */
    inline unsigned int getNbLocalExtrema(bool removeBoundaryPoints = false) const {
      return getLocalExtrema(removeBoundaryPoints).size();
    }

    /**
       Return local extrema
    */
    std::vector<VertexID> getLocalExtrema(bool removeBoundaryPoints = false) const;

    /** return true if the given point is a local minimum */
    inline bool isLocalMinimum(VertexID point) const {
      return isLocalExtremum(point, false);
    }

    /** return true if the given point is a local maximum */
    inline bool isLocalMaximum(VertexID point) const {
      return isLocalExtremum(point, true);
    }

    /** return true if the given point is a local extremum of the given direction. */
    bool isLocalExtremum(VertexID point, bool direction) const;

    /** return true if the given point is a local minimum. Non strict version. */
    inline bool isLocalMinimumNonStrict(VertexID point) const {
      return isLocalExtremumNonStrict(point, false);
    }

    /** return true if the given point is a local maximum. Non strict version. */
    inline bool isLocalMaximumNonStrict(VertexID point) const {
      return isLocalExtremumNonStrict(point, true);
    }

    /** return true if the given point is a local extremum of the given direction.
	Non strict version. */
    bool isLocalExtremumNonStrict(VertexID point, bool direction) const;

    /**
       Return the number of flat edges
    */
    VertexID getNbFlatEdges() const;

    /**
       return true if the given point is a saddle point
    */
    bool isSaddlePoint(VertexID p) const;


    /**
       return a map that contains for each point the absolute difference between the value
       of the current mesh map and the given value
    */
    MeshMapT<T> distanceMap(T value) const;

    /**
       return the minimum (non zero) difference on edge
     */
    T getMinimumDifferenceOnEdge() const;

    /**
       return the area of the surface contained between the two values
     */
    double getArea(T minValue, T maxValue) const;

    /**
       return the area of the surface in one side of the value. If \p sup = true, the superior side is choosen.
     */
    double getArea(T value, bool sup) const;

#ifndef SWIG
    /** init the meshmap using a given list */
    template <class L>
    MeshMapT<T> & operator=(const L & list);
#endif
  };

  /** a MeshMap with doubles */
  typedef MeshMapT<double> MeshMap;

  /** a MeshMap with unsigned int */
  typedef MeshMapT<unsigned int> MeshMapUInt;
}
#define MESH_MAP_DEFINED

#include "Mesh.h"
#include "MeshManipulator.h"
#include "MeshPart.h"
#include "FileExceptions.h"
#include "PointOnEdge.h"
#include "PLPath.h"
#include "PLY.h"

namespace Taglut {

  template <typename T>
  MeshMapT<T>::MeshMapT() {
    addFormats();
    mesh = NULL;
    values = NULL;
    nbValues = 0;
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, T value) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    T * v = values;
    for(unsigned int i = 0; i < nbValues; ++i, ++v)
      *v = value;
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, const Direction & d) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    setValuesFromAxis(d);
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, const Direction & d, double angle0) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    T * v = values;
    for(Mesh::const_point_iterator p = mesh_l.point_begin(); p != mesh_l.point_end(); ++p, ++v) {
      double ax;
      bool oside;
      if (d == DTop) {
	ax = (*p).getX() / (sqrt((*p).getX() * (*p).getX() + (*p).getY() * (*p).getY()));
	oside = (*p).getY() < 0;
      }
      else if (d == DBottom) {
	ax = (*p).getX() / (sqrt((*p).getX() * (*p).getX() + (*p).getY() * (*p).getY()));
	oside = (*p).getY() > 0;
      }
      else if (d == DLeft) {
	ax = (*p).getY() / (sqrt((*p).getZ() * (*p).getZ() + (*p).getY() * (*p).getY()));
	oside = (*p).getZ() < 0;
      }
      else if (d == DRight) {
	ax = (*p).getY() / (sqrt((*p).getZ() * (*p).getZ() + (*p).getY() * (*p).getY()));
	oside = (*p).getZ() > 0;
      }
      else if (d == DFront) {
	ax = (*p).getZ() / (sqrt((*p).getZ() * (*p).getZ() + (*p).getX() * (*p).getX()));
	oside = (*p).getX() < 0;
      }
      else if (d == DBottom) {
	ax = (*p).getZ() / (sqrt((*p).getZ() * (*p).getZ() + (*p).getX() * (*p).getX()));
	oside = (*p).getX() > 0;
      }
      float angle = acos(ax);
      assert(angle >= 0.);
      assert(angle <= 2 * M_PI);
      if (oside)
	angle = 2 * M_PI - angle;
      if (angle > angle0)
	angle -= 2 * M_PI;
      *v = angle;
    }
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, const Plane3D & plane) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    T * v = values;

    for(Mesh::const_point_iterator p = mesh_l.point_begin(); p != mesh_l.point_end(); ++p, ++v)
      *v = plane.signedDistance(*p);
  }


  template <typename T>
  MeshMapT<T>::MeshMapT(const MeshMapT<T> & mMap, Mesh & mesh_l) : FileManipulator(mMap) {
    addFormats();
    (*this).mesh = &mesh_l;
    if ((mMap.values != NULL) && (nbValues != 0)) {
      nbValues = mMap.nbValues;
      values = new T[nbValues];
      T * v = values;
      T * oV = mMap.values;
      for(unsigned int i = 0; i < nbValues; ++i, ++v, ++oV)
	*v = *oV;
    }
    else {
      nbValues = 0;
      values = NULL;
    }
    addMissingValuesUsingMean();
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(const MeshMapT<T> & mMap) : FileManipulator(mMap) {
    addFormats();
    if (mMap.mesh != NULL)
      mesh = mMap.mesh;
    nbValues = mMap.nbValues;
    if ((mMap.values != NULL) && (nbValues != 0)) {
      values = new T[nbValues];
      assert(values != NULL);
      T * v = values;
      T * oV = mMap.values;
      for(unsigned int i = 0; i < nbValues; ++i, ++v, ++oV) {
	*v = *oV;
      }
    }
    else {
      nbValues = 0;
      values = NULL;
    }
  }


  template <typename T>
  MeshMapT<T>::MeshMapT(const std::string & filename) {
    addFormats();
    (*this).mesh = NULL;
    nbValues = 0;
    values = NULL;
    load(filename);
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, const std::string & filename) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    load(filename);
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, const char * filename) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    load(filename);
  }

  template <typename T>
  MeshMapT<T>::MeshMapT(Mesh & mesh_l, const MeshPart & meshPart, const MeshMapT<T> & refMeshMapT) {
    addFormats();
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    if (mesh_l.getNbPoints() != meshPart.getNbPoints())
      throw Exception("MeshMapT(): wrong number of points in the meshpart.");

    const VertexID maxID = meshPart.getMesh().getNbPoints();
    if (maxID != refMeshMapT.getNbValues())
      throw Exception("MeshMapT(): wrong number of points in the original mesh.");

    T * v = values;
    VertexID mapID = 0;
    T * vs = refMeshMapT.values;
    for(VertexID i = 0; i != maxID; ++i, ++vs) {
      if (meshPart.hasPoint(i)) {
        *v = *vs;
        ++v;
        ++mapID;
        if (mapID == nbValues)
          break;
      }
    }
  }


  template <typename T>
  void MeshMapT<T>::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string &) {
    if ((altFormat == "txt") || (altFormat == "usr"))
      loadTXT(fileName);
    else if ((altFormat == "dcm") || (altFormat == "hdr"))
      loadFromVolumicImage(fileName);
    else
      throw Exception("Unknown format");
  }

  template <typename T>
  void MeshMapT<T>::saveAltFormat(const std::string & fileName, const std::string & format, const std::string &) const {
    if ((format == "txt") || (format == "usr"))
      saveTXT(fileName);
    else if (format == "vtk")
      saveVTK(fileName);
    else
      throw Exception("Unknown format");
  }

  template <typename T>
  void MeshMapT<T>::saveTXT(const std::string & fileName) const {
    std::ofstream outfile(fileName.c_str(), std::ios::out);

    if (!outfile.is_open())
      throw ExceptionCannotOpenOutputFile();

    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      outfile << *v << std::endl;

    outfile.close();
  }

  template <typename T>
  void MeshMapT<T>::saveVTK(const std::string & fileName) const {
    std::ofstream outfile(fileName.c_str(), std::ios::out);

    if (!outfile.is_open())
      throw ExceptionCannotOpenOutputFile();

    // save mesh
    outfile << "# vtk DataFile Version 3.0" << std::endl;

    outfile << "vtk output" << std::endl << "ASCII" << std::endl << "DATASET POLYDATA" << std::endl;
    outfile << "POINTS " << (*mesh).getNbPoints() << " float" << std::endl;

    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
      outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;

    outfile << "POLYGONS " << (*mesh).getNbTriangles() << " " << ((*mesh).getNbTriangles() * 4) << std::endl;

    for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
      outfile << "3 " << (*t).getP1() << " " << (*t).getP2() << " " << (*t).getP3() << std::endl;

    // save values
    outfile << "POINT_DATA " << nbValues << std::endl;
    outfile << "SCALARS values float" << std::endl;
    outfile << "LOOKUP_TABLE default" << std::endl;
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      outfile << *v << std::endl;

    outfile.close();
  }

  template <typename T>
  void MeshMapT<T>::loadTXT(const std::string & fileName) {
    std::ifstream infile(fileName.c_str());

    if (!infile.is_open())
      throw ExceptionFileNotFound();

    if (values != NULL)
      delete [] values;

    if (mesh != NULL) {
      nbValues = (*mesh).getNbPoints();
      values = new T[nbValues];

      VertexID cpt = 0;
      T * v = values;
      while(!infile.eof()) {
	infile >> *v;
	++v;
	++cpt;
	if (cpt == nbValues)
	  break;
      }
      if (cpt != nbValues)
	throw ExceptionDataNotFoundInFile();
    }
    else {
      // load file
      std::vector<T> lv;
      T v;
	infile >> v;
      while(!infile.eof()) {
	lv.push_back(v);
	infile >> v;
      }

      // copy it on the structure
      nbValues = lv.size();
      values = new T[nbValues];
      T * vv = values;
      for(typename std::vector<T>::const_iterator lvv = lv.begin(); lvv != lv.end(); ++lvv, ++vv)
	*vv = *lvv;

    }

    infile.close();
  }


  template <typename T>
  void MeshMapT<T>::loadFromVolumicImage(const std::string & fileName, double ratio) {
    if (mesh == NULL) {
      throw Exception("loadFromVolumicImage(2): the mesh has to be defined to load data from a volumic image.");
    }
    if (values != NULL)
      delete [] values;
    nbValues = (*mesh).getNbPoints();
    values = new T[nbValues];

    const CImg<double> img(fileName.c_str());

    T *v = values;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++v, ++p) {
      unsigned int x = floor((*p).getX());
      unsigned int y = floor((*p).getY());
      unsigned int z = floor((*p).getZ() / ratio);
      if (x != (*p).getX()) {
	*v = (img(x, y, z) + img(x + 1, y, z)) / 2;
      }
      else if (y != (*p).getY()) {
	*v = (img(x, y, z) + img(x, y + 1, z)) / 2;
      }
      else {
	assert(z != (*p).getZ());
	*v = (img(x, y, z) + img(x, y, z + 1)) / 2;
      }
    }

  }

  template <typename T>
  void MeshMapT<T>::setMesh(Mesh & mesh_l, T value) {
    if (values != NULL)
      delete [] values;
    (*this).mesh = &mesh_l;
    nbValues = mesh_l.getNbPoints();
    values = new T[nbValues];
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      *v = value;
  }

  template <typename T>
  void MeshMapT<T>::setMesh(Mesh & mesh_l) {
    (*this).mesh = &mesh_l;
  }


  template <typename T>
  void MeshMapT<T>::setValue(VertexID id, T value) {
    if (values == NULL)
      throw Exception("setValue(2): values not defined.");
    if (id >= nbValues)
      throw Exception("setValue(2): id out of bounds.");
    values[id] = value;
  }

  template <typename T>
  void MeshMapT<T>::initValues(T value) {
    if (values == NULL)
      throw Exception("initValues(): values not defined.");
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      *v = value;
  }

  template <typename T>
  void MeshMapT<T>::setValuesFromFlags() {
    if (mesh == NULL)
      throw Exception("setValuesFromFlags(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("setValuesFromFlags(): values not defined.");
    VertexID i = 0;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++i)
      values[i] = (T)((*p).getFlag());
  }

  template <typename T>
  void MeshMapT<T>::smoothValues(unsigned int nbiter) {
    if (mesh == NULL)
      throw Exception("smoothValues(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("smoothValues(): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("smoothValues(): mesh has been modified (bad number of values).");
    T tmp[nbValues];
    for(unsigned int it = 0; it != nbiter; ++it) {
      VertexID i = 0;
      for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++i) {
	unsigned int nbNb = 1;
	tmp[i] = values[i];
	for (std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb, ++nbNb)
	  tmp[i] += values[*nb];
	tmp[i] /= nbNb;
      }
      T * tmp_p = tmp;
      T * values_p = values;
      for(VertexID ii = 0; ii != nbValues; ++ii, ++values_p, ++tmp_p) {
	*values_p = *tmp_p;
      }
    }
  }

  template <typename T>
  double MeshMapT<T>::getEpsilonOnEdge() const {

    if (mesh == NULL)
      throw Exception("getEpsilonOnEdge(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("getEpsilonOnEdge(): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("getEpsilonOnEdge(): mesh has been modified (bad number of values).");

    // first compute the smallest difference between points
    double value = std::numeric_limits<double>::max();
    VertexID i = 0;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++i) {
      const double v1 = values[i];
      for (std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb) {
	const double diff = fabs(values[*nb] - v1);
	if ((diff != 0.) && (diff < value))
	  value = diff;
      }
    }
    // then modify the values using this small value
    value /= nbValues + 1;
    assert(value != 0.);
    return value;
  }

  template <typename T>
  void MeshMapT<T>::removeFlatEdges() {
    double value = getEpsilonOnEdge();

    double * cValue = values;
    for(VertexID ii = 0; ii != nbValues; ++ii, ++cValue) {
#ifndef NDEBUG
      const double previous = *cValue;
#endif
      (*cValue) += ii * value;
#ifndef NDEBUG
      assert((ii == 0) || (*cValue != previous));
#endif
    }

  }


  template <typename T>
  void MeshMapT<T>::setValuesFromLengths(const MeshManipulator & mManip) {
    if (nbValues != mManip.getMesh().getNbPoints())
      throw Exception("setValuesFromLengths(1): error (bad number of values in MeshManipulator ).");
    for(VertexID i = 0; i < nbValues; ++i)
      values[i] = (T)mManip.getLength(i);

  }


  template <typename T>
  void MeshMapT<T>::setValuesFromCenter(VertexID id, double alpha, double epsilon) {
    if (mesh == NULL)
      throw Exception("setValuesFromCenter(3): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("setValuesFromCenter(3): values not defined.");
    if (id >= nbValues)
      throw Exception("setValuesFromCenter(3): id out of bounds.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("setValuesFromCenter(3): mesh has been modified (bad number of values).");
    MeshManipulator mManip(*mesh);

    mManip.computeDijkstra(id);
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      *v = alpha * mManip.getLength(i) + epsilon;
  }

  template <typename T>
  void MeshMapT<T>::setValuesFromDirection(const Direction & direction, double alpha, double epsilon) {
    if (mesh == NULL)
      throw Exception("setValuesFromDirection(3): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("setValuesFromDirection(3): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("setValuesFromDirection(3): mesh has been modified (bad number of values).");

    if (direction == DNone)
      return;

    MeshManipulator mManip(*mesh);


    Line3D line((*mesh).getIsobarycenter(), direction);
    unsigned int id = (*mesh).getLastIntersection(line);

    if (id == (*mesh).getNbPoints())
      std::cout << "Warning: cannot find points on the axis." << std::endl;
    else
      setValuesFromCenter(id, alpha, epsilon);
  }


  template <typename T>
  void MeshMapT<T>::setValuesFromAxis(const Direction & direction) {
    if (mesh == NULL)
      throw Exception("setValuesFromDirection(3): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("setValuesFromDirection(3): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("setValuesFromDirection(3): mesh has been modified (bad number of values).");

    if (direction == DNone)
      return;

    T * v = values;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++v) {
      if (direction == DTop)
	(*v) = (*p).getZ();
      else if (direction == DBottom)
	(*v) = - (*p).getZ();
      else if (direction == DLeft)
	(*v) = (*p).getY();
      else if (direction == DRight)
	(*v) = - (*p).getY();
      else if (direction == DFront)
	(*v) = (*p).getX();
      else if (direction == DBack)
	(*v) = - (*p).getX();
    }
  }

  template <typename T>
  T MeshMapT<T>::getMaxValue() const {
    T result = std::numeric_limits<T>::min();
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      if (*v > result)
	result = *v;
    return result;
  }

  template <typename T>
  T MeshMapT<T>::getMinValue() const {
    T result = std::numeric_limits<double>::max();
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      if (*v < result)
	result = *v;
    return result;
  }

  template <typename T>
  T MeshMapT<T>::getExtremum(const std::vector<VertexID> & path) const {
    if (path.size() == 0)
      return 0.;
    else if (path.size() < 3) {
      return path.front();
    }
    bool up = (*this)[path.front()] < (*this)[path[1]];

    std::vector<VertexID>::const_iterator predp = path.begin();
    for(std::vector<VertexID>::const_iterator p = path.begin() + 1; p != path.end(); ++p, ++predp) {
      if ((up && ((*this)[*predp] > (*this)[*p])) ||
	  ((!up) && ((*this)[*predp] < (*this)[*p])))
	return (*this)[*predp];
    }
    throw Exception("getExtremum(1): Uniform path");
  }

  template <typename T>
  void MeshMapT<T>::applyLog() {
    T * v = values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      *v = log(*v);
  }


  template <typename T>
  void MeshMapT<T>::addValues(const std::vector<double> & newValues) {
    VertexID nbValuesOld = nbValues;
    T * valuesOld = values;
    nbValues += newValues.size();
    values = new T[nbValues];
    T * v = values;
    T * vold = valuesOld;
    // first copy the values
    for(VertexID i = 0; i < nbValuesOld; ++i, ++v, ++vold)
      *v = *vold;
    delete[] valuesOld;

    for(std::vector<double>::const_iterator nv = newValues.begin(); nv != newValues.end(); ++nv, ++v)
      *v = *nv;
    mesh = NULL;
  }

  template <typename T>
  void MeshMapT<T>::addMissingValuesUsingMean() {
    if (mesh == NULL)
      throw Exception("addMissingValuesUsingMean(): mesh not defined in MeshMap");
    if ((*mesh).getNbPoints() == nbValues)
      return;

    VertexID nbValuesOld = nbValues;
    T * valuesOld = values;
    nbValues = (*mesh).getNbPoints();
    values = new T[nbValues];
    T * v = values;
    T * vold = valuesOld;
    // first copy the values
    for(VertexID i = 0; i < nbValuesOld; ++i, ++v, ++vold)
      *v = *vold;

    // then update the new one using the mean of its neighbours
    for(VertexID i = nbValuesOld; i < nbValues; ++i, ++v) {
      T r = 0.0;
      unsigned int nbNbOld = 0;
      for(std::deque<VertexID>::const_iterator nb = (*mesh).point(i).getNeighbours().begin(); nb != (*mesh).point(i).getNeighbours().end(); ++nb)
	if (*nb < nbValuesOld) {
	  ++nbNbOld;
	  r += valuesOld[*nb];
	}
      if (nbNbOld != 0)
	r /= nbNbOld;
      *v = r;
    }
    delete [] valuesOld;
  }

  template <typename T>
  void MeshMapT<T>::updateMesh(Mesh & mesh_l) {
    (*this).mesh = &mesh_l;
    addMissingValuesUsingMean();
  }

  template <typename T>
  PLPath MeshMapT<T>::computeDirectLevelSetCC(const PointOnEdge & point) const {
    PLPath result = computeLevelSetCC(point);
    const T lsvalue = point.getValueFromPLFunction(values[point.getFirstVertex()], values[point.getSecondVertex()]);

    if (result.size() >= 1) {
      const PointOnEdge & p1 = result.front();
      const PointOnEdge & p2 = result[1];
      const Triangle & t = (*mesh).findTriangle(p1, p2);

      // compute for each corner of the triangle its location wrt the level set
      char tvalues[3];
      for(unsigned char i = 0; i != 3; ++i)
	tvalues[i] = (lsvalue == values[t.getVertexId(i)] ? 0 :
		      lsvalue > values[t.getVertexId(i)] ? 1 : -1);

      // one of the corners of the triangle is alone
      unsigned char iSingle;
      bool found = false;
      for(iSingle = 0; iSingle != 3; ++iSingle) {
	if ((tvalues[iSingle] != 0) &&
	    (tvalues[iSingle] != tvalues[(iSingle + 1) % 3]) &&
	    (tvalues[iSingle] != tvalues[(iSingle + 2) % 3])) {
	  found = true;
	  break;
	}
      }
      assert(found);
      const VertexID v1 = p1.isVertex() ? p1.getClosestVertex() : p1.getOtherVertex(t.getVertexId(iSingle));
      const VertexID v2 = p2.isVertex() ? p2.getClosestVertex() : p2.getOtherVertex(t.getVertexId(iSingle));
      const bool direct = tvalues[iSingle] < 0 ? t.sameOrder(t.getVertexId(iSingle), v1, v2) : t.sameOrder(v1, t.getVertexId(iSingle), v2);
      if (!direct)
	result.reversePath();
      return result;
    }
    else
      return result;
  }

  template <typename T>
  PLPath MeshMapT<T>::computeLevelSetCC(const PointOnEdge & point) const {
    if (mesh == NULL)
      throw Exception("computeLevelSetCC(1): mesh not defined in MeshMap");
    PLPath path(*mesh);
    path.addPointAtEnd(point);
    const T lsvalue = point.getValueFromPLFunction(values[point.getFirstVertex()], values[point.getSecondVertex()]);
    bool not_closed = true;
    bool firstSide = true;
    while(not_closed) {
      assert(fabs(getValue(path.back()) - getValue(point)) < 1e-3);
      bool reverse = false;
      if (path.back().isVertex()) {
	if (path.size() == 1) {
	  PointOnEdge nextp = getPossibleNextPointOnLevelSet(path.back(), lsvalue);
	  path.addPointAtEnd(nextp);
	}
	else {
	  PointOnEdge nextp;
	  try {
	    nextp = getPossibleNextPointOnLevelSet(path.back(), lsvalue);

	    const PointOnEdge predp = path[path.size() - 2];
	    if (nextp == predp) {
	      nextp = getOtherPossibleNextPointOnLevelSet(path.back(), lsvalue, predp);
	    }
	  }
	  catch (Exception e) {
	    if (firstSide) {
	      firstSide = false;
	      path.reversePath();
	      reverse = true;
	    }
	    else {
	      not_closed = false;
	      continue;
	    }
	  }
	  if (!reverse) {
	    path.addPointAtEnd(nextp);
	    if (path.front().inSameEdge(path.back()) && (path.size() > 2)) {
	      not_closed = false;
	      if (path.front().distance(path.back()) != 0.) // if the connexion is not perfect, add a missing edge
		path.back() = path.front();
	    }
	  }
	}
      }
      else if (isFlatEdge(path.back().getFirstVertex(), path.back().getSecondVertex())) {
	assert(path.size() == 1);
	path.addPointAtEnd((*mesh).point(path.back().getFirstVertex()));
      }
      else {
	// TODO: if it's the first edge, check the two sides, and select the farest one
	TriangleID t = (*mesh).findTriangle(path.back().getFirstVertex(), path.back().getSecondVertex()).getId();
	if (path.size() != 1) {
	  TriangleID t2 = (*mesh).findTriangle(path.back(), path[path.size() - 2]).getId();
	  if (t == t2) {
	    try {
	      t = (*mesh).findOtherTriangle(path.back().getFirstVertex(), path.back().getSecondVertex(), t).getId();
	    }
	    catch (Exception e) {
	      if (firstSide) {
		firstSide = false;
		path.reversePath();
		reverse = true;
	      }
	      else
		not_closed = false;
	    }
	  }
	}
	else {
	  // look to the other triangle...
	  try {
	    TriangleID t2 = (*mesh).findOtherTriangle(path.back().getFirstVertex(), path.back().getSecondVertex(), t).getId();
	    const PointOnEdge nextp1 = getNextPointOnLevelSet(path.back(), t, lsvalue);
	    const PointOnEdge nextp2 = getNextPointOnLevelSet(path.back(), t2, lsvalue);
	    if ((nextp1.inSameEdge(path.back())) || ((nextp1.distance(path.back()) < nextp2.distance(path.back())) && (!nextp2.inSameEdge(path.back()))))
	      t = t2;

	  }
	  catch (Exception e) {
	  }
	}

	if (not_closed && !reverse) {
	  const PointOnEdge nextp = getNextPointOnLevelSet(path.back(), t, lsvalue);
	  assert(nextp.inSameEdge(path.back()) ||
		 (t == (*mesh).findTriangle(path.back(), nextp).getId()));
	  path.addPointAtEnd(nextp);
	  if (path.front().inSameEdge(path.back()) && (path.size() > 2)) {
	    not_closed = false;
	    if (path.front().distance(path.back()) != 0.) // if the connexion is not perfect, add a missing edge
	      path.back() = path.front();
	  }
	}
      }
    }

    return path;
  }

  template <typename T>
  PointOnEdge MeshMapT<T>::getPossibleNextPointOnLevelSet(const PointOnEdge & point, double lsvalue) const {
    if (mesh == NULL)
      throw Exception("getPossibleNextPointOnLevelSet(2): mesh not defined in MeshMap");
    assert(point.isVertex());
    const Point3D & p = (*mesh).point(point.getClosestVertex());
    VertexID prednb = p.getNeighbours().back();
    for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin(); nb != p.getNeighbours().end(); ++nb) {
      if ((*mesh).point(*nb).hasNeighbour(prednb)) {
	T valuenb = values[*nb];
	T valueprednb = values[prednb];
	if ((valuenb != valueprednb) &&
	    (((valuenb >= lsvalue) && (valueprednb <= lsvalue)) ||
	     ((valuenb <= lsvalue) && (valueprednb >= lsvalue)))) {
	  T minvalue = valuenb < valueprednb ? valuenb : valueprednb;
	  T maxvalue = valuenb > valueprednb ? valuenb : valueprednb;
	  PointOnEdge result(valuenb < valueprednb ? (*mesh).point(*nb) : (*mesh).point(prednb),
			     valuenb < valueprednb ? (*mesh).point(prednb) : (*mesh).point(*nb),
			     (lsvalue - minvalue) / (maxvalue - minvalue));
	  if (result.isVertex())
	    return PointOnEdge((*mesh).point(result.getClosestVertex()));
	  else
	    return result;
	}
	else if ((valuenb == valueprednb) && (valuenb == lsvalue))
	  return PointOnEdge((*mesh).point(*nb), (*mesh).point(prednb), 0.5);
      }
      prednb = *nb;
    }
    throw Exception("getPossibleNextPointOnLevelSet(2): unable to find next point.");
  }

  template <typename T>
  PointOnEdge MeshMapT<T>::getOtherPossibleNextPointOnLevelSet(const PointOnEdge & point, double lsvalue, const PointOnEdge & pointfirst) const {
    if (mesh == NULL)
      throw Exception("getOtherPossibleNextPointOnLevelSet(3): mesh not defined in MeshMap");
    assert(point.isVertex());
    const Point3D & p = (*mesh).point(point.getClosestVertex());
    VertexID prednb = p.getNeighbours().back();
    for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin(); nb != p.getNeighbours().end(); ++nb) {
      if ((*mesh).point(*nb).hasNeighbour(prednb)) {
	if (!pointfirst.inEdge(std::pair<VertexID, VertexID>(*nb, prednb))) {
	  T valuenb = values[*nb];
	  T valueprednb = values[prednb];
	  if ((valuenb != valueprednb) &&
	      (((valuenb >= lsvalue) && (valueprednb <= lsvalue)) ||
	       (((valuenb <= lsvalue) && (valueprednb >= lsvalue))))) {
	    T minvalue = valuenb < valueprednb ? valuenb : valueprednb;
	    T maxvalue = valuenb > valueprednb ? valuenb : valueprednb;
	    PointOnEdge result(valuenb < valueprednb ? (*mesh).point(*nb) : (*mesh).point(prednb),
			       valuenb < valueprednb ? (*mesh).point(prednb) : (*mesh).point(*nb),
			       (lsvalue - minvalue) / (maxvalue - minvalue));

	    if (result.isVertex())
	      return PointOnEdge((*mesh).point(result.getClosestVertex()));
	    else
	      return result;
	  }
	  else if ((valuenb == valueprednb) && (valuenb == lsvalue)) {
	    return PointOnEdge((*mesh).point(*nb), (*mesh).point(prednb), 0.5);
	  }
	}
      }
      prednb = *nb;
    }
    throw Exception("getOtherPossibleNextPointOnLevelSet(2): unable to find next point.");
  }

  template <typename T>
  PointOnEdge MeshMapT<T>::getPointOnEdgeFromValueAndEdge(VertexID p1, VertexID p2, T value) const {
    const T v1 = values[p1];
    const T v2 = values[p2];
    // reorder the values
    if (v1 > v2)
      return getPointOnEdgeFromValueAndEdge(p2, p1, value);
    // throw exception if out of bound
    if (((v1 > value) && v2 > value) ||
	((v2 < value) && v2 < value))
      throw Exception("getPointOnEdgeFromValueAndEdge(3): out of bounds");
    assert(v1 <= v2);
    assert(value <= v2);
    assert(v1 <= value);
    // if the two points have the same value... Choose the middle.
    if (v1 == v2)
      return PointOnEdge((*mesh).point(p1), (*mesh).point(p2), 0.5);
    else // otherwise, adjust the location according to the values
      return PointOnEdge((*mesh).point(p1), (*mesh).point(p2), (value - v1) / (v2 - v1));
  }


  template <typename T>
  PLPath MeshMapT<T>::computeLevelSetFromValueAndPath(const PLPath & path, double value) const {
    return computeLevelSetCC(getPointOnEdgeFromValueAndPath(path, value));
  }


  template <typename T>
  PointOnEdge MeshMapT<T>::getPointOnEdgeFromValueAndPath(const std::vector<VertexID> & path, double value) const {
    if (path.size() == 0)
      throw Exception("getPointOnEdgeFromValueAndPath(2): empty path");
    if (path.size() == 1) {
      if (getValue(path.front()) == value)
	return (*mesh).point(path.front());
      else
	throw Exception("getPointOnEdgeFromValueAndPath(2): cannot find the required value along the path.");
    }
    std::vector<VertexID>::const_iterator predp = path.begin();
    double predValue = (*this)[*predp];
    for(std::vector<VertexID>::const_iterator p = path.begin() + 1; p != path.end(); ++p, ++predp) {
      const double valueP = (*this)[*p];
      if (predValue == value)
	return (*mesh).point(*predp);
      else if (valueP == value)
	return (*mesh).point(*p);
      else if (((predValue <= value) && (value <= valueP)) ||
	       ((predValue >= value) && (value >= valueP))) {
	return PointOnEdge((*mesh).point(*predp), (*mesh).point(*p), (value - predValue) / (valueP - predValue));
      }
      predValue = valueP;
    }

    throw Exception("getPointOnEdgeFromValueAndPath(2): cannot find the required value along the path.");
  }

  template <typename T>
  PointOnEdge MeshMapT<T>::getPointOnEdgeFromValueAndPath(const PLPath & path, double value) const {
    if (path.size() == 0)
      throw Exception("getPointOnEdgeFromValueAndPath(2): empty path");
    if (path.size() == 1) {
      if (getValue(path.front()) == value)
	return path.front();
      else
	throw Exception("getPointOnEdgeFromValueAndPath(2): cannot find the required value along the path.");
    }
    PLPath::const_iterator predp = path.begin();
    double predValue = getValue(*predp);
    for(PLPath::const_iterator p = path.begin() + 1; p != path.end(); ++p, ++predp) {
      const double valueP = getValue(*p);
      if (predValue == value)
	return *predp;
      else if (valueP == value)
	return *p;
      else if (((predValue <= value) && (value <= valueP)) ||
	       ((predValue >= value) && (value >= valueP))) {
	if ((*predp).inSameEdge(*p)) {
	  std::pair<VertexID, VertexID> edge = (*predp).getEdge(*p);
	  return PointOnEdge((*mesh).point(edge.first), (*mesh).point(edge.second), (value - values[edge.first]) / (values[edge.second] - values[edge.first]));
	}
	else {
	  const Triangle & t = (*mesh).findTriangle(*predp, *p);
	  for(unsigned int i = 0; i < 3; ++i) {
	    const std::pair<VertexID, VertexID> edge = t.getEdge(i);
	    const double v1 = (*this)[edge.first];
	    const double v2 = (*this)[edge.second];
	    if (((v1 <= value) && (value <= v2)) ||
	       ((v1 >= value) && (value >= v2))) {
	      return PointOnEdge((*mesh).point(edge.first), (*mesh).point(edge.second), (value - v1) / (v2 - v1));
	    }
	  }
	  throw Exception("getPointOnEdgeFromValueAndPath(2): cannot find the required value along the path.");
	}
      }
      predValue = valueP;
    }

    throw Exception("getPointOnEdgeFromValueAndPath(2): cannot find the required value along the path.");
  }


  template <typename T>
  PointOnEdge MeshMapT<T>::getNextPointOnLevelSet(const PointOnEdge & point, TriangleID t, double lsvalue) const {
    if (mesh == NULL)
      throw Exception("getNextPointOnLevelSet(2): mesh not defined in MeshMap");
    const Triangle & triangle = (*mesh).triangle(t);
    VertexID p1 = point.getFirstVertex();
    assert(triangle.hasPoint(p1));
    VertexID p2 = point.getSecondVertex();
    assert(triangle.hasPoint(p2));
    VertexID p3 = triangle.getOtherPoint(p1, p2);

    // reorder p1 and p2: values[p1] <= values[p2]
    if (values[p1] > values[p2]) {
      VertexID ptmp = p1;
      p1 = p2;
      p2 = ptmp;
    }

    if (lsvalue == values[p3]) {
      return (*mesh).point(p3);
    }
    if (lsvalue < values[p3]) {
      // the new point is between p1 and p3
      if (values[p1] < values[p3])
	return PointOnEdge((*mesh).point(p1), (*mesh).point(p3), (lsvalue - values[p1]) / (values[p3] - values[p1]));
      else
	return PointOnEdge((*mesh).point(p3), (*mesh).point(p1), (lsvalue - values[p3]) / (values[p1] - values[p3]));
    }
    else {
      // the new point is between p2 and p3
      if (values[p3] < values[p2])
	return PointOnEdge((*mesh).point(p3), (*mesh).point(p2), (lsvalue - values[p3]) / (values[p2] - values[p3]));
      else
	return PointOnEdge((*mesh).point(p2), (*mesh).point(p3), (lsvalue - values[p2]) / (values[p3] - values[p2]));
    }
  }

  template <typename T>
  std::vector<PLPath> MeshMapT<T>::computeLevelSet(double value, bool direct) const {
    if (mesh == NULL)
      throw Exception("computeLevelSet(1): mesh not defined in MeshMap");
    std::vector<PLPath> result;

    (*mesh).setTriangleFlag(0);
    (*mesh).setPointFlag(0);
    // for all the triangles
    for(Mesh::triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
      if ((*t).getFlag() == 0) {
	VertexID inf = (*mesh).getNbPoints();
	VertexID sup = (*mesh).getNbPoints();
	for(unsigned int i = 0; i < 3; ++i) {
	  T v = values[(*t).getVertexId(i)];
	  if (v <= value) inf = (*t).getVertexId(i);
	  if (v >= value) sup = (*t).getVertexId(i);
	}
	// a valid edge is found
	if ((inf != (*mesh).getNbPoints()) && (sup != (*mesh).getNbPoints())) {
	  PointOnEdge firstPoint((*mesh).point(inf), (*mesh).point(sup),
				 values[sup] == values[inf] ? 0. : (value - values[inf]) / (values[sup] - values[inf]));
	  if (!firstPoint.isVertex() || ((*mesh).point(firstPoint.getClosestVertex()).getFlag() == 0)) {
	    assert((*mesh).getBox().contains(firstPoint));
	    PLPath path;
	    if (direct)
	      path = computeDirectLevelSetCC(firstPoint);
	    else
	      path = computeLevelSetCC(firstPoint);
	    (*mesh).setTriangleAndPointFlag(path, 1);
	    result.push_back(path);
	  }
	}
      }

    return result;
  }

  template <typename T>
  bool MeshMapT<T>::isSaddlePoint(VertexID p) const {
    return getNeighbourArity((*mesh).point(p)) > 1;
  }

  template <typename T>
  std::vector<std::pair<VertexID, unsigned int> > MeshMapT<T>::getSaddlePoints() const {
    if (mesh == NULL)
      throw Exception("getSaddlePoints(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("getSaddlePoints(): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("getSaddlePoints(): mesh has been modified (bad number of values).");

    std::vector<std::pair<VertexID, unsigned int> > result;

    T * v = values;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++v)
      if ((*p).getNbNeighbours() != 0) {
	unsigned int arity = getNeighbourArity(*p);

	if (arity > 1)
	  result.push_back(std::pair<VertexID, unsigned int>((*p).getId(), arity));
      }

    return result;
  }

  template <typename T>
  unsigned int MeshMapT<T>::getNeighbourArity(const Point3D & p, T * curValue) const {
    unsigned int arity = 0;
    if (curValue == NULL)
      curValue = values + p.getId();
    VertexID prevnb = p.getNeighbours().back();
    for(std::deque<VertexID>::const_iterator nb = p.getNeighbours().begin(); nb != p.getNeighbours().end(); prevnb = *nb, ++nb)
      if ((values[*nb] > *curValue) && (values[prevnb] < *curValue))
	++arity;
    return arity;
  }

  template <typename T>
  bool MeshMapT<T>::isLocalExtremumNonStrict(VertexID pid, bool direction) const {
    if (mesh == NULL)
      throw Exception("isLocalExtremumNonStrict(2): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("isLocalExtremumNonStrict(2): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("isLocalExtremumNonStrict(2): mesh has been modified (bad number of values).");

    const Point3D & point  = (*mesh).point(pid);
    const double value = getValue(pid);

    bool min = values[point.getNeighbours().front()] >= value;
    if (min == direction)
      return false;

    for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb)
      if ((min && (values[*nb] < value)) ||
	  ((!min) && (values[*nb] > value))) {
	return false;
      }
    return true;
  }

  template <typename T>
  bool MeshMapT<T>::isLocalExtremum(VertexID pid, bool direction) const {
    if (mesh == NULL)
      throw Exception("isLocalExtremum(2): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("isLocalExtremum(2): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("isLocalExtremum(2): mesh has been modified (bad number of values).");

    const Point3D & point  = (*mesh).point(pid);
    const double value = getValue(pid);
    bool min = values[point.getNeighbours().front()] > value;
    if (min == direction)
      return false;

    for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb)
      if ((min && (values[*nb] <= value)) ||
	  ((!min) && (values[*nb] >= value))) {
	return false;
      }
    return true;
  }


  template <typename T>
  void MeshMapT<T>::addOnBoundaries(T value, T threshold) {
    if (mesh == NULL)
      throw Exception("getLocalExtrema(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("getLocalExtrema(): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("getLocalExtrema(): mesh has been modified (bad number of values).");

    std::vector<VertexID> result;

    T * v = values;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++v) {
      if ((*p).getIsBoundary() && (*v > threshold))
	*v += value;
    }

  }

  template <typename T>
  std::vector<VertexID> MeshMapT<T>::getLocalExtrema(bool removeBoundaryPoints) const {
    if (mesh == NULL)
      throw Exception("getLocalExtrema(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("getLocalExtrema(): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("getLocalExtrema(): mesh has been modified (bad number of values).");

    std::vector<VertexID> result;

    T * v = values;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++v)
      if ((*p).getNbNeighbours() != 0) {
	bool isExtrema = true;
	bool min = values[(*p).getNeighbours().front()] > *v;

	for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	  if ((min && (values[*nb] <= *v)) ||
	      ((!min) && (values[*nb] >= *v))) {
		isExtrema = false;
		break;
	      }
	if (isExtrema && ((!removeBoundaryPoints) || (!(*p).getIsBoundary())))
	  result.push_back((*p).getId());
      }

    return result;
  }

  template <typename T>
  VertexID MeshMapT<T>::getNbFlatEdges() const {
    if (mesh == NULL)
      throw Exception("getNbFlatEdges(): mesh not defined in MeshMap");
    if (values == NULL)
      throw Exception("getNbFlatEdges(): values not defined.");
    if ((*mesh).getNbPoints() != nbValues)
      throw Exception("getNbFlatEdges(): mesh has been modified (bad number of values).");

    VertexID result = 0;

    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p)
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	if ((*nb > (*p).getId()) && (values[*nb] == values[(*p).getId()]))
	  ++result;

    return result;
  }


  template <typename T>
  T MeshMapT<T>::getValue(const PointOnEdge & point) const {
    const T & v = values[point.getFirstVertex()];
    return v + point.getLocation() * (values[point.getSecondVertex()] - v);
  }

  template <typename T>
  MeshMapT<T> MeshMapT<T>::distanceMap(T value) const {
    MeshMapT<T> result(*this);
    assert(result.getNbValues() == nbValues);
    T * v = result.values;
    for(VertexID i = 0; i < nbValues; ++i, ++v)
      *v = fabs(*v - value);
    return result;
  }

  template <typename T>
  T MeshMapT<T>::getMinimumDifferenceOnEdge() const {
    T result = std::numeric_limits<T>::max();

    T * v = values;
    for(Mesh::const_point_iterator p = (*mesh).point_begin(); p != (*mesh).point_end(); ++p, ++v)
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb) {
	const T diff = fabs(*v - values[*nb]);
	if ((diff != 0) && (diff < result))
	  result = diff;
      }
    return result;
  }

  template <typename T>
  double MeshMapT<T>::getArea(T minValue, T maxValue) const {
    return getArea(minValue, true) + getArea(maxValue, false) - (*mesh).getArea();
  }

  template <typename T>
  double MeshMapT<T>::getArea(T value, bool sup) const {
    double result = 0.;
    double direction = sup ? -1 : 1;

    for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t) {
      const bool p1Inside = (value - values[(*t).getP1()]) * direction > 0;
      const bool p2Inside = (value - values[(*t).getP2()]) * direction > 0;
      const bool p3Inside = (value - values[(*t).getP3()]) * direction > 0;
      const unsigned int nbInside = (unsigned int)p1Inside + (unsigned int)p2Inside + (unsigned int)p3Inside;
      if (nbInside == 3)
	result += (*mesh).getArea((*t).getId());
      else if (nbInside == 2) {
	Coord3D c1, c2, c3;
	if (!p1Inside)
	  c1 = (*mesh).point((*t).getP1());
	else
	  if (!p2Inside)
	    c1 = getPointOnEdgeFromValueAndEdge((*t).getP1(), (*t).getP2(), value);
	  else // !p3Inside
	    c1 = getPointOnEdgeFromValueAndEdge((*t).getP1(), (*t).getP3(), value);
	if (!p2Inside)
	  c2 = (*mesh).point((*t).getP2());
	else
	  if (!p1Inside)
	    c2 = getPointOnEdgeFromValueAndEdge((*t).getP2(), (*t).getP1(), value);
	  else // !p3Inside
	    c2 = getPointOnEdgeFromValueAndEdge((*t).getP2(), (*t).getP3(), value);
	if (!p3Inside)
	  c3 = (*mesh).point((*t).getP3());
	else
	  if (!p1Inside)
	    c3 = getPointOnEdgeFromValueAndEdge((*t).getP3(), (*t).getP1(), value);
	  else // !p2Inside
	    c3 = getPointOnEdgeFromValueAndEdge((*t).getP3(), (*t).getP2(), value);

	const Triangle3D tt(c1, c2, c3);
	result += (*mesh).getArea((*t).getId()) - tt.area();
      }
      else if (nbInside == 1) {
	Coord3D c1, c2, c3;
	if (p1Inside)
	  c1 = (*mesh).point((*t).getP1());
	else
	  if (p2Inside)
	    c1 = getPointOnEdgeFromValueAndEdge((*t).getP1(), (*t).getP2(), value);
	  else // p3Inside
	    c1 = getPointOnEdgeFromValueAndEdge((*t).getP1(), (*t).getP3(), value);
	if (p2Inside)
	  c2 = (*mesh).point((*t).getP2());
	else
	  if (p1Inside)
	    c2 = getPointOnEdgeFromValueAndEdge((*t).getP2(), (*t).getP1(), value);
	  else // p3Inside
	    c2 = getPointOnEdgeFromValueAndEdge((*t).getP2(), (*t).getP3(), value);
	if (p3Inside)
	  c3 = (*mesh).point((*t).getP3());
	else
	  if (p1Inside)
	    c3 = getPointOnEdgeFromValueAndEdge((*t).getP3(), (*t).getP1(), value);
	  else // p2Inside
	    c3 = getPointOnEdgeFromValueAndEdge((*t).getP3(), (*t).getP2(), value);

	const Triangle3D tt(c1, c2, c3);
	result += tt.area();
      }
    }

    return result;
  }

  template <typename T>
  void MeshMapT<T>::setValues(const MeshMapT<T> & input, const IDTranslator & translator) {
    const VertexID maxInput = input.getNbValues();

    for(VertexID i = 0; i != maxInput; ++i) {
      setValue(translator.l2g(i), input.getValue(i));
    }
  }

  template <typename T>
  void MeshMapT<T>::savePLY(const std::string & fileName) const {
      PLYSaver plysaver;
      plysaver.savePLY(*this, fileName);
  }

  template <typename T>
  void MeshMapT<T>::loadPLY(const std::string & fileName) {
      PLYLoader plyLoader(fileName);
      if (mesh != NULL)
          plyLoader.loadMeshFromPLY(*mesh);
      plyLoader.loadMeshMapFromPLY(*this);
  }

  template <typename T>
  template <class L>
  MeshMapT<T> & MeshMapT<T>::operator=(const L & list) {
      if (values != NULL)
          delete[] values;
      nbValues = list.size();
      values = new T[nbValues];
      T * vv = values;
      for(typename L::const_iterator v = list.begin(); v != list.end(); ++v, ++vv)
          *vv = *v;

      return *this;
  }

}



#endif
