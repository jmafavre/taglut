/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshVoxelizer.h"
#include "Mesh.h"

using namespace Taglut;

unsigned char MeshVoxelizer::inside = 1;
unsigned char MeshVoxelizer::outside = 3;
unsigned char MeshVoxelizer::surface = 2;
unsigned char MeshVoxelizer::unknown = 0;


MeshVoxelizer::MeshVoxelizer(const Mesh & mesh_t, unsigned int maxSize) {
  (*this).mesh = &mesh_t;
  computeVoxelization(maxSize);
  isComputedIsoBarycenter = false;
  areComputedAxes = false;
}


MeshVoxelizer::~MeshVoxelizer() {
  if (voxelization != NULL)
    delete voxelization;
}

void MeshVoxelizer::computeVoxelization(unsigned int maxSize) {
  assert(maxSize > 2);

  Box3D box = (*mesh).getBox();

  double diffx = box.getSizeX();
  double diffy = box.getSizeY();
  double diffz = box.getSizeZ();


  if ((diffx >= diffy) && (diffx >= diffz))
    voxSize = diffx / (maxSize - 4);
  else if ((diffy >= diffx) && (diffy >= diffz))
    voxSize = diffy / (maxSize - 4);
  else // ((diffz >= diffx) && (diffz >= diffy))
    voxSize = diffz / (maxSize - 4);

  firstPoint = Coord3D(box.getMinX() - 2 * voxSize, box.getMinY() - voxSize, box.getMinZ() - 2 * voxSize);


  voxelization = new CImg<unsigned char>((unsigned int) (ceil(diffx / voxSize)) + 4, (unsigned int) (ceil(diffy / voxSize)) + 4, (unsigned int) (ceil(diffz / voxSize)) + 4);

  (*voxelization).fill(unknown);

  // first compute the surface's voxels
  drawTriangles();

  // then fill the first connected component
  fillConnectedComponent(0, 0, 0, outside);

  // Then fill all the others connected components according to the inside/outside status
  cimg_forXYZ(*voxelization, i, j, k)
    if ((*voxelization)(i, j, k) == unknown) {
      unsigned char value;
      if ((*mesh).isInside(convertVoxelToReal(Coord3D(i, j, k))))
	value = inside;
      else
	value = outside;
      fillConnectedComponent(i, j, k, value);
    }
}


void MeshVoxelizer::fillConnectedComponent(unsigned int x, unsigned int y, unsigned int z, unsigned char value) {
  if ((*voxelization)(x, y, z) == unknown) {
    (*voxelization)(x, y, z) = value;
    if (x != 0)
      fillConnectedComponent(x - 1, y, z, value);
    if (x != (*voxelization)._width - 1)
      fillConnectedComponent(x + 1, y, z, value);
    if (y != 0)
      fillConnectedComponent(x, y - 1, z, value);
    if (y != (*voxelization)._height - 1)
      fillConnectedComponent(x, y + 1, z, value);
    if (z != 0)
      fillConnectedComponent(x, y, z - 1, value);
    if (z != (*voxelization)._depth - 1)
      fillConnectedComponent(x, y, z + 1, value);
  }
}

void MeshVoxelizer::drawTriangles() {
  for(Mesh::const_triangle_iterator t = (*mesh).triangle_begin(); t != (*mesh).triangle_end(); ++t)
    drawTriangle((*mesh).point((*t).getP1()), (*mesh).point((*t).getP2()), (*mesh).point((*t).getP3()));
}

void MeshVoxelizer::drawTriangle(const Coord3D & p1, const Coord3D & p2, const Coord3D & p3) {
  Triangle3D triangle(p1, p2, p3);

  Box3D box(triangle);

  Box3DT<int> voxels = getIntersectedVoxels(box);

  for(int i = voxels.getMinX(); i <= voxels.getMaxX(); ++i)
    for(int j = voxels.getMinY(); j <= voxels.getMaxY(); ++j)
      for(int k = voxels.getMinZ(); k <= voxels.getMaxZ(); ++k) {
	Box3D voxel = buildVoxelBox(i, j, k);
	if (voxel.intersect(triangle))
	  (*voxelization)(i, j, k) = surface;
      }
}

Box3DT<int> MeshVoxelizer::getIntersectedVoxels(const Box3D & box) {
  // compute box in voxel system
  Box3D boxVoxel(box);
  boxVoxel -= firstPoint;
  boxVoxel /= voxSize;

  // then round the values enlarging the box to integer values
  return boxVoxel.largeRound();
}

Box3D MeshVoxelizer::buildVoxelBox(unsigned int i, unsigned int j, unsigned int k) {
  return Box3D(i, i + 1, j, j + 1, k, k + 1) * voxSize + firstPoint;
}

void MeshVoxelizer::computeIsoBarycenter() {
  unsigned int nbPoints = 0;
  isoBarycenter.setX(0.0);
  isoBarycenter.setY(0.0);
  isoBarycenter.setZ(0.0);

  // for each voxel, check its "inside" flag
  cimg_forXYZ(*voxelization, i, j, k)
    if (((*voxelization)(i, j, k) == inside) || ((*voxelization)(i, j, k) == surface)) {
      isoBarycenter += convertVoxelToReal(Coord3D(i, j, k));
      ++nbPoints;
    }

  if (nbPoints != 0)
    isoBarycenter /= nbPoints;

  isComputedIsoBarycenter = true;
}

void MeshVoxelizer::computeAxes() {
  computeIsoBarycenter();
  unsigned int nbPoints = 0;
  CImg<double> matrix(3, 3);
  CImg<double> val, vec;
  double value;
  // first compute number of points inside the volume
  cimg_forXYZ(*voxelization, i, j, k)
    if ((*voxelization)(i, j, k) == inside)
      ++nbPoints;

  // then build the matrix
  for(unsigned int i = 0; i < 3; ++i)
    for(unsigned int j = 0; j <= i; ++j) {
      value = 0.0;
      cimg_forXYZ(*voxelization, x, y, z)
	if (((*voxelization)(x, y, z) == inside) || ((*voxelization)(x, y, z) == surface)) {
	  Coord3D point = convertVoxelToReal(Coord3D(x, y, z)) - isoBarycenter;
	  value += point[i] * point[j] / nbPoints;
	}

      matrix(i, j) = value;
      if (i != j) {
	matrix(j, i) = value;
      }
    }

  // then inverse it
  matrix.symmetric_eigen(val, vec);

  // and build the resulting list
  for(unsigned int i = 0; i < 3; ++i)
    axes.push_back(Coord3D(vec(i, 0), vec(i, 1), vec(i, 2)));

  areComputedAxes = true;
}

void MeshVoxelizer::addBorder(UCharCImg & img, unsigned int margin, unsigned char value) {
  if (margin == 0)
    return;

  UCharCImg imgResult(img.width() + 2 * margin, img.height() + 2 * margin, img.depth() + 2 * margin);
  imgResult.fill(value);
  imgResult.draw_image(margin, margin, margin, img);

  img = imgResult;
}

const UCharCImg & MeshVoxelizer::getVoxelization() const {
  if (voxelization == NULL)
    throw Exception("getVoxelization(): voxelization has not been computed.");

  return *voxelization;
}

UCharCImg MeshVoxelizer::getVoxelization(int size, unsigned int margin) const {
  if (voxelization == NULL)
    throw Exception("getVoxelization(): voxelization has not been computed.");

  UCharCImg img = (*voxelization);

  addBorder(img, margin, outside);

  if (size > 0) {
    double ratio = ((double) size) / (*voxelization)._width;
    if ((*voxelization)._width < (*voxelization)._height)
      ratio = ((double) size) / (*voxelization)._height;
    if (((double) size) / (*voxelization)._depth > ratio)
      ratio = ((double) size) / (*voxelization)._depth;
    img = img.get_resize((int)((*voxelization)._width * ratio), (int)((*voxelization)._height * ratio), (int)((*voxelization)._depth * ratio), 1);
  }


  return img;
}

UCharCImg MeshVoxelizer::getBinaryVoxelization(int size, unsigned int margin) const {
  if (voxelization == NULL)
    throw Exception("getVoxelization(): voxelization has not been computed.");

  UCharCImg result = getVoxelization(size, margin);

  cimg_forXYZ(result, i, j, k) {
    if (result(i, j, k) == outside)
      result(i, j, k) = 0;
    else
      result(i, j, k) = 1;
  }

  return result;
}


const Coord3D & MeshVoxelizer::getVolumicIsoBarycenter() {
  if (voxelization == NULL)
    throw Exception("getVolumicIsoBarycenter(): voxelization has not been computed.");
  if (!isComputedIsoBarycenter)
    computeIsoBarycenter();
  return isoBarycenter;
}

const std::deque<Coord3D> & MeshVoxelizer::getVolumicPCAAxes() {
  if (voxelization == NULL)
    throw Exception("getVolumicPCAAxes(): voxelization has not been computed.");
  if (!areComputedAxes)
    computeAxes();
  return axes;
}

void MeshVoxelizer::display(unsigned int size, unsigned int margin, bool binary) const {
  CImg<unsigned char> img;
  if (binary)
    img = getBinaryVoxelization(size, margin);
  else
    img = getVoxelization(size, margin);

  CImgDisplay disp( img._width + img._depth, img._height + img._depth, "Visualisation");
  int X = img._width / 2, Y = img._height / 2, Z = img._depth / 2;

  while (!disp._is_closed) {
    CImg<unsigned char> visu(disp._width, disp._height, 1, 3, 0);

    visu = img.get_projections2d(X, Y, Z).normalize(0, 255).resize(disp._width, disp._height, 1, cimg::min(3, (int)img._spectrum));

    visu.display(disp);

    if (disp._button != 0) {
      const int mx = disp._mouse_x, my = disp._mouse_y;
      if ((mx >= 0) && (my >= 0)) {
	const int mX = mx*(img._width + (img._depth > 1 ? img._depth : 0)) / disp._width, mY = my * (img._height + (img._depth > 1 ? img._depth : 0)) / disp._height;
	if (((unsigned int)mX < img._width) && ((unsigned int)mY < img._height))   { X=mX; Y = mY; }
	if (((unsigned int)mX < img._width) && ((unsigned int)mY >= img._height))  { X=mX; Z = mY - img._height; }
	if (((unsigned int)mX >= img._width) && ((unsigned int)mY < img._height))  { Y=mY; Z = mX - img._width;  }
	if (((unsigned int)mX >= img._width) && ((unsigned int)mY >= img._height)) { X = 0; Y = 0; Z = 0; }
	if (((unsigned int)mX < img._width) && ((unsigned int)mY < img._height))   { X = mX; Y = mY; }
      }

      if (disp._is_resized) disp.resize();
    }

  }


}
