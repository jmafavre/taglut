/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SCRIPT_INTERPRETER
#define SCRIPT_INTERPRETER

#include <iostream>
#include <string>
#include <libxml/xmlreader.h>

#include "MeshList.h"
#include "Mapping2D3DList.h"

namespace Taglut {
  class XMLSimpleNode;
  class IndentManager;
  class MeshPathes;

  /**
     @class ScriptInterpreter

     @author Jean-Marie Favreau
     @brief Script interpreter (xml format)
  */
  class ScriptInterpreter {

    /** script filename */
    const std::string script;

    /** xml schema filename */
    const std::string fxmlschema;

    /** true if xml validation is needed */
    bool validation;

    /** manipulated mesh */
    MeshList meshes;

    /** manipulated mappings */
    Mapping2D3DList mappings;

    /** instruction id */
    unsigned int idInstruction;

    /** io manager */
    Taglut::IndentManager indentManager;

#ifdef LIBXML_READER_ENABLED
    /** xml reader */
    xmlTextReaderPtr reader;

    int getMeshId();
    void jumpNextNode();
    void jumpNextNode(xmlTextReaderPtr & reader);
    std::deque<std::string> readOptions(std::string attribute = "options");
    std::deque<std::string> computeOptions(std::string sOptions);
    int breakMsg();
    int comment();
    int outputMeshPathes(MeshPathes & mCut, const std::deque<XMLSimpleNode> & outputNodes, unsigned int idM);
    int outputClustering(int meshId);
    int printMessage();
    int selectMesh();
    int duplicateMesh();
    int cropMesh();
    int cleanMesh();
    int patchMesh();
    int cutMesh();
    int pruneMesh();
    int uncutMesh();
    int pointManipulationMesh();
    int flipTriangleMesh();
    int addPointMesh();
    int unfoldMesh();
    int output() ;
    int inputMesh();
    int reorientMesh();
    int adjustUnfolding();
    int adjustCutting();
    bool checkTest(const std::string & test);
    void skipBlock(const std::string & nodeName);
    int convertInstruction();
    int switchInstruction();
    int mainReader(const std::string & instrName);
    void loadChildren(std::deque<XMLSimpleNode> & outputNodes, const std::string & name);
    int waitInstruction();
    int sleepInstruction();
    int execWithCache();
    int timeInstr();
    int registration();

#endif

  public:

    /** default constructor */
    ScriptInterpreter(const std::string & script, bool validation, const Mesh & mesh, const std::string & fxmlschema);

    /** file name of the xml schema */
    static std::string SchemaFileName;

    /** run the script */
    unsigned int runScript();

    /** return help message */
    static std::string helpScript();

    /** return a merged of the meshes manipulated by the script interpreter */
    Mesh getMesh() const;
  };
}

#endif
