/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef STRING_MANIPULATION
#define STRING_MANIPULATION

#include <string>
#include <sstream>
#include <deque>
#include "IDTypes.h"

namespace Taglut {
  /**
     @class StringManipulation

     @author Jean-Marie Favreau (CNRS / Univ. Blaise Pascal)
     @date 2008-11-11
     @brief String manipulation
  */
  class StringManipulation {
  public:
    /*  CHECK VALUES' VALIDITY  */


    /**
       Return true if the given parameter is a non signed float
    */
    static bool isUnsignedFloat(const std::string & str);

    /**
       Return true if the given parameter is a signed float
    */
    static bool isSignedFloat(const std::string & str);

    /**
       Return true if the given parameter is a non signed integer
    */
    static bool isUnsignedInt(const std::string & str);

    /**
       Return true if the given parameter is a signed integer
    */
    static bool isSignedInt(const std::string & str);

    /**
       Return true if the given parameter is a non signed percentage
    */
    static bool isUnsignedPercent(const std::string & str);

    /**
       Return true if the given parameter is a signed percentage
    */
    static bool isSignedPercent(const std::string & str);


    /**
       Return true if the given parameter is a call of a function with number of
       arguments given in parameters. If nbArgs = -1, the number of params is not
       checked
    */
    static bool isFunction(const std::string & str, int nbArgs = -1);

    /**
       Return true if the given parameter is a call of a function with the given name and
       with number of arguments given in parameters. If nbArgs = -1, the number of params
       is not checked
    */
    static bool isFunction(const std::string & str, const std::string & name, int nbArgs = -1);

    /**
       Return the number of parameters of the function call given in parameters, or "-1"
       if it is not a correct call of function
    */
    static int getNbArgsFunction(const std::string & str);


    /*  GET VALUES  */

    /**
       Return the percent value
    */
    static double getPercent(const std::string & str);

    /**
       Return the int value
    */
    static int getInt(const std::string & str);

    /**
       Return the vertex id value
    */
    static VertexID getVertexID(const std::string & str);

    /**
       Return the float value
    */
    static double getFloat(const std::string & str);


    /**
       Return name of the function used in the call given in parameter
    */
    static std::string getFunctionName(const std::string & str);

    /**
       Return list of parameters used in the call of function given in parameter
    */
    static std::deque<std::string> getParameters(const std::string & str);

    /**
       Split a string using the separator given in parameter
    */
    static std::deque<std::string> splitString(const std::string & str, char separator, bool stripSpaces = false);

    /**
       Strip the given string (removing spaces at the begin and the end)
    */
    static std::string strip(const std::string & str);

    /**
       Remove a possible CR caracter at the end of the given string
    */
    static std::string removeCR(const std::string & str);

    /** convert every kind of variable into string */
    template <class T>
    static std::string toString(const T & val) {
      std::stringstream stream;
      stream << val;
      return stream.str();
    }
  };
}

#endif
