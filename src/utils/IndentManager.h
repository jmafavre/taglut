/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef INDENT_MANAGER
#define INDENT_MANAGER

#include <iostream>

namespace Taglut {
  /**
     @class IndentManager

     @author Jean-Marie Favreau
     @brief Adding white spaces before lines according to the indent depth
  */
  class IndentManager {
  private:
    unsigned int depth;

  public:

    /**
       Default constructor
    */
    IndentManager();

    /**
       Copy constructor
    */
    IndentManager(const IndentManager & m);

    /**
       Increase depth
    */
    inline void incDepth() { ++depth; }


    /**
       Decrease depth
    */
    inline void decrDepth() { if (depth != 0) --depth; }

    /**
       Get indent characters
    */
    std::string getIndent() const;

  };


  /**
     Stream operator
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Taglut::IndentManager & iManager) {
    f << iManager.getIndent();

    return f;
  }
}

#endif
