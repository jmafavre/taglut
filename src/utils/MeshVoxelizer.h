/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_VOXELIZER
#define MESH_VOXELIZER

#include <deque>
#include "CImgUse.h"
#include "Coord3D.h"

namespace Taglut {
  class Mesh;

  /**
     @class MeshVoxelizer
     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @brief Voxelizer tool
  */
  class MeshVoxelizer {
  private:
    const Mesh * mesh;

    CImg<unsigned char> * voxelization;

    Coord3D isoBarycenter;

    bool isComputedIsoBarycenter;

    std::deque<Coord3D> axes;

    bool areComputedAxes;

    double voxSize;

    Coord3D firstPoint;

    void computeVoxelization(unsigned int maxSize);

    void computeIsoBarycenter();

    void computeAxes();

    void drawTriangles();

    void drawTriangle(const Coord3D & p1, const Coord3D & p2, const Coord3D & p3);

    Box3DT<int> getIntersectedVoxels(const Box3D & box);

    Box3D buildVoxelBox(unsigned int i, unsigned int j, unsigned int k);

    void fillConnectedComponent(unsigned int x, unsigned int y, unsigned int z, unsigned char value);

    static void addBorder(UCharCImg & img, unsigned int margin, unsigned char value);
  public:

    /** voxel status (inside) */
    static unsigned char inside;
    /** voxel status (outside) */
    static unsigned char outside;
    /** voxel status (surface) */
    static unsigned char surface;
    /** voxel status (unknown) */
    static unsigned char unknown;

    /**
       Default constructor
    */
    MeshVoxelizer(const Mesh & mesh, unsigned int maxSize);

    /**
       Destructor
    */
    ~MeshVoxelizer();

    /**
       Get voxelization of the manipulated mesh
    */
    const UCharCImg & getVoxelization() const;

    /**
       Get voxelization of the manipulated mesh (resizing it using size as maximum size)
    */
    UCharCImg getVoxelization(int size, unsigned int margin = 0) const;

    /**
       Get a binary voxelization of the manipulated mesh (resizing it using size as maximum size if size > 0)
    */
    UCharCImg getBinaryVoxelization(int size = -1, unsigned int margin = 0) const;

    /**
       Get the volumic iso-barycenter of the manipulated mesh
    */
    const Coord3D & getVolumicIsoBarycenter();

    /**
       Get the volumic PCA axes of the manipulated mesh
    */
    const std::deque<Coord3D> & getVolumicPCAAxes();

    /**
       Convert a coord from the voxel system to the real system
    */
    inline Coord3D convertVoxelToReal(const Coord3D & coord) const {
      return coord * voxSize + firstPoint;
    }

    /**
       Convert a coord from the real system to the voxel system
    */
    inline Coord3D convertRealToVoxel(const Coord3D & coord) const {
      return (coord - firstPoint) / voxSize;
    }

    /**
       Display the current voxel set
    */
    void display(unsigned int size, unsigned int margin = 0, bool binary = false) const;
  };
}

#endif

