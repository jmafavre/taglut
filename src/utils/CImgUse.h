/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
    \mainpage Topological And Geometrical Library - a Useful Toolkit

    \section intro_sec Introduction

*/


/**
   \file CImgUse.h
   \brief Contain all include and define need to use CImg
   \author Jean-Marie Favreau
*/

#ifndef USE_X
/** If no X detected, set display type to false in CImg */
#define cimg_display 0
#endif

#ifndef CIMGUSE_H
#define CIMGUSE_H

/**
   Adding fsf reader
 */
#ifndef SWIG
#define cimg_plugin <io_fsf.h>
#endif

#include <CImg.h>
using namespace cimg_library;
/**
   Define an alias
*/
#define UCharCImg CImg<unsigned char>

#endif
