/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "ColorChooser.h"

using namespace Taglut;

unsigned char ColorChooser::computeValue(int value) {
  int newvalue = value % 1536;

  if (newvalue < 0)
    return 0;
  else if (newvalue < 256)
    return newvalue;
  else if (newvalue < 768)
    return 255;
  else if (newvalue < 1024)
    return 1023 - newvalue;
  else
    return 0;
}

CImg<unsigned char> ColorChooser::getColorFromId(unsigned int id, unsigned int nbColors) {
  CImg<unsigned char> result(1, 1, 1, 3);
  int scaledValue = (int) (((double) id * 1536) / (nbColors - 1)); /* 1536 = 256 * 6 */

  result(0, 0, 0, 0) = computeValue(scaledValue + 1024);
  result(0, 0, 0, 1) = computeValue(scaledValue + 512);
  result(0, 0, 0, 2) = computeValue(scaledValue);

  return result;
}
