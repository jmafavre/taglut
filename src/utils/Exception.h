/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef EXCEPTION
#define EXCEPTION

#ifndef SWIG
#include <iostream>
#include <string>
#endif


namespace Taglut {
  /**
     @class Exception
     @author Jean-marie Favreau
     @brief Simple Exception class with string message.
  */
  class Exception {
#ifdef SWIG
      %pythoncode { __metaclass__ = BaseException }
#endif
  private:
    std::string msg;

  public:
    /**
       Default constructor with empty message
    */
    Exception();
    /**
       Constructor with message (string)
    */
    Exception(const std::string & msg);


#ifndef SWIG
    /**
       Constructor with message (char *)
    */
    Exception(const char * msg);
#endif

    /**
       Return exception's message
    */
    std::string getMessage() const;
  };

  /**
     A string representation of the given exception
   */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, Exception & p) {
    f << "Exception: " << p.getMessage() << std::endl;

    return f;
  }

  /**
     @class ExceptionTopology
     @author Jean-Marie Favreau
     @brief Topological exception
  */
  class ExceptionTopology : public Exception {
  public:
    /**
       Default constructor
    */
    ExceptionTopology();

    /**
       Constructor with message
    */
    ExceptionTopology(const std::string & msg);
  };


  /**
     @class FatalException
     @author Jean-Marie Favreau
     @brief Fatal exception
  */
  class FatalException : public Exception {
  public:
    /**
       Default constructor
    */
    FatalException();

    /**
       Constructor with message
    */
    FatalException(const std::string & msg);

  };
}

#endif
