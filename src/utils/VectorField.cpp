/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <fstream>

#ifdef USE_GSL
#include <gsl/gsl_linalg.h>
#endif


#include "VectorField.h"
#include "FileExceptions.h"
#include "Coord3D.h"
#include "Mesh.h"

using namespace Taglut;

void VectorField::addFormats() {
  addFormat("txt");
}


void VectorField::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string &) {
  if (altFormat == "txt")
    loadTXT(fileName);
  else
    throw Exception("Unknown format");
}

void VectorField::saveAltFormat(const std::string & fileName, const std::string & format, const std::string &) const {
  if (format == "txt")
    saveTXT(fileName);
  else
    throw Exception("Unknown format");
}

void VectorField::saveTXT(const std::string & fileName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  for(std::vector<Coord3D>::const_iterator c = begin(); c != end(); ++c)
    outfile << (*c).getX() << " " << (*c).getY() << " " << (*c).getZ() << std::endl;

  outfile.close();
}

void VectorField::loadTXT(const std::string & fileName) {
  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (size() != 0)
    clear();

  double x, y, z;
  while(!infile.eof()) {
    infile >> x >> y >> z;
    push_back(Coord3D(x, y, z));
  }

  infile.close();
}



void VectorField::computeFromScalarFunction(const MeshMap & mMap, const Mesh & mesh) {
#ifdef USE_GSL
  clear();
  reserve(mesh.getNbTriangles());


  double a[9];
  gsl_matrix_view m = gsl_matrix_view_array (a, 3, 3);
  double b[3];
  gsl_vector_view v = gsl_vector_view_array(b, 3);
  b[2] = 0.;
  gsl_vector * x = gsl_vector_alloc(3);
  int s;
  gsl_permutation * p = gsl_permutation_alloc(3);
  int r;

  for(Mesh::const_triangle_iterator t = mesh.triangle_begin(); t != mesh.triangle_end(); ++t) {
    const Point3D & pi = mesh.point((*t).getP1());
    const Point3D & pj = mesh.point((*t).getP2());
    const Point3D & pk = mesh.point((*t).getP3());
    const Coord3D & n = mesh.computeTriangleNormal((*t).getId()).normalize();
    a[0] = pj.getX() - pi.getX();
    a[1] = pj.getY() - pi.getY();
    a[2] = pj.getZ() - pi.getZ();
    a[3] = pk.getX() - pj.getX();
    a[4] = pk.getY() - pj.getY();
    a[5] = pk.getZ() - pj.getZ();
    a[6] = n.getX();
    a[7] = n.getY();
    a[8] = n.getZ();

    b[0] = mMap[pj.getId()] - mMap[pi.getId()];
    b[1] = mMap[pk.getId()] - mMap[pj.getId()];

    r = gsl_linalg_LU_decomp(&m.matrix, p, &s);
    if (r != 0)
      throw Exception("computeFromScalarFunction(2): error during LU decomposition");
    r = gsl_linalg_LU_solve(&m.matrix, p, &v.vector, x);
    if (r != 0)
      throw Exception("computeFromScalarFunction(2): error during LU solving");

    push_back(Coord3D(gsl_vector_get(x, 0), gsl_vector_get(x, 1), gsl_vector_get(x, 2)));
  }

  gsl_permutation_free(p);
  gsl_vector_free(x);

#else
  throw Exception("computeFromScalarFunction(2): the software has been compiled without linear solver. Please compile using GSL.");
#endif
}

PointOnEdge VectorField::getNextPoint(const Point3D & point, bool up, const Mesh & mesh, const MeshMap & mMap) const {
  PointOnEdge result;
  double distance = mMap[point.getId()];
  bool emptyresult = true;
  Coord3D p;

  double tmpdist = 0.;
  for(std::deque<TriangleID>::const_iterator t = point.getTriangles().begin(); t !=point.getTriangles().end(); ++t) {
    const Point3D & pFirst = mesh.point(mesh.triangle(*t).getOtherPoint(point.getId()));
    const Point3D & pSecond = mesh.point(mesh.triangle(*t).getOtherPoint(point.getId(), pFirst.getId()));
    const Line3D lineFromVector(point, (*this)[*t]);
    bool found = true;
    try {
      p = lineFromVector.getIntersection(Line3D(pFirst, pSecond - pFirst));
    }
    catch (Exception e) {
      found = false;
    }

    if (found) {
      double location = p.getLocationOnEdge(pFirst, pSecond);
      if ((location >= 0.) && (location <= 1.)) {
	PointOnEdge r(pFirst, pSecond, location);
	const double ratio = (mMap.getValue(r) - distance) / point.distance(r);
	if ((up && (ratio > tmpdist)) || ((!up) && (ratio < tmpdist))) {
	  if (!point.getIsBoundary() && mesh.isBoundaryEdge(r)) {
	    tmpdist = ratio;
	    result = r;
	    emptyresult = false;
	  }
	}
      }
    }
  }

  // try the vertices
  for(std::deque<VertexID>::const_iterator nb = point.getNeighbours().begin(); nb != point.getNeighbours().end(); ++nb) {
    const double ratio = (mMap[*nb] - distance) / point.distance(mesh.point(*nb));
    if ((up && ((ratio > tmpdist) && ((!point.getIsBoundary() || !mesh.point(*nb).getIsBoundary())))) ||
	 ((!up) && ((ratio < tmpdist) && ((!point.getIsBoundary() || !mesh.point(*nb).getIsBoundary()))))) {
      tmpdist = ratio;
      result = PointOnEdge(mesh.point(*nb));
      emptyresult = false;
    }
  }

  if (emptyresult) {
    // last try: an edge that is not a boundary edge
    for(std::deque<TriangleID>::const_iterator t = point.getTriangles().begin(); t !=point.getTriangles().end(); ++t) {
      const Point3D & pFirst = mesh.point(mesh.triangle(*t).getOtherPoint(point.getId()));
      const Point3D & pSecond = mesh.point(mesh.triangle(*t).getOtherPoint(point.getId(), pFirst.getId()));
      if (!mesh.isBoundaryEdge(pFirst.getId(), pSecond.getId())) {
	result = PointOnEdge(pFirst, pSecond, 0.5);
	emptyresult = false;
	break;
      }
    }

    if (emptyresult)
      throw Exception("getNextPoint(4): no intersection found in the given direction.");
  }

  return result;
}

PointOnEdge VectorField::getNextPoint(const PointOnEdge & point, const Triangle & t, bool up, const Mesh & mesh, const MeshMap & mMap) const {
  PointOnEdge result;
  const double value = mMap.getValue(point);

  const Point3D & pFirst = mesh.point(point.getFirstVertex());
  const Point3D & pSecond = mesh.point(point.getSecondVertex());
  const Point3D & pOther = mesh.point(t.getOtherPoint(point.getFirstVertex(), point.getSecondVertex()));


  const Line3D lineFromVector(point, (*this)[t.getId()]);

  Coord3D p;
  bool intersection = true;
  double location = -1.;

  // compute the first intersection
  try {
    p = lineFromVector.getIntersection(Line3D(pFirst, pOther - pFirst));
  }
  catch (Exception e) {
    intersection = false;
  }

  if (intersection) {
    location = p.getLocationOnEdge(pFirst, pOther);
    if ((location < 0.) || (location > 1.)) {
      intersection = false;
    }
    else {
      result = PointOnEdge(pFirst, pOther, location);
      double vr = mMap.getValue(result);
      if ((up && (vr < value)) || (!up && (vr > value))) {
	intersection = false;
      }
    }
  }

  if (!intersection) {
    // compute the second one
    intersection = true;
    try {
      p = lineFromVector.getIntersection(Line3D(pSecond, pOther - pSecond));
    }
    catch (Exception e) {
      intersection = false;
    }

    if (intersection) {
      location = p.getLocationOnEdge(pSecond, pOther);
      if ((location < 0.) || (location > 1.)) {
	intersection = false;
      }
      else {
	result = PointOnEdge(pSecond, pOther, location);
	double vr = mMap.getValue(result);
	if ((up && (vr < value)) || (!up && (vr > value))) {
	  intersection = false;
	}
      }
    }

  }

  if (!intersection) {
    // in case of flat triangle, we select the non boundary edge
    if ((mMap.getValue(pFirst.getId()) == mMap.getValue(pOther.getId())) &&
	(mMap.getValue(pSecond.getId()) == mMap.getValue(pOther.getId()))) {
      if (!mesh.isBoundaryEdge(pFirst.getId(), pOther.getId()))
	return PointOnEdge(pFirst, pOther, 0.5);
      if (!mesh.isBoundaryEdge(pSecond.getId(), pOther.getId()))
	return PointOnEdge(pSecond, pOther, 0.5);
    }

    // finally, try the vertices
    const double ratioFirst = (mMap[point.getFirstVertex()] - value) / pFirst.distance(point);
    const double ratioSecond = (mMap[point.getSecondVertex()] - value) / pSecond.distance(point);
    if (up && (ratioFirst >= 0) && (ratioFirst >= ratioSecond)) return pFirst;
    if (up && (ratioSecond >= 0) && (ratioSecond >= ratioFirst)) return pSecond;
    if ((!up) && (ratioSecond <= 0) && (ratioSecond <= ratioFirst)) return pSecond;
    if ((!up) && (ratioFirst <= 0) && (ratioFirst <= ratioSecond)) return pFirst;


    throw Exception("getNextPoint(5): no intersection found");
  }



  return result;
}

