/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Messages.h"
#include "Exception.h"

using namespace Taglut;

std::string Message::messageCut80(const std::string message) {
  bool first = true;
  bool itemize = (message.size() >= 2) && (message[0] == '-') && (message[1] == ' ');
  unsigned int lLength;
  std::ostringstream str;
  std::string messageCut = message;
  std::string buf;

  while (messageCut.size() != 0) {
    if (itemize && !first) {
      lLength = 78;
      str << "  ";
    } else
      lLength = 80;

    buf = messageCut.substr(0, lLength);
    int p = buf.find_last_of(" \t\n");
    if ((p < 0) || (buf.size() != lLength))
      messageCut = messageCut.substr(buf.size());
    else {
      buf = buf.substr(0, p);
      messageCut = messageCut.substr(buf.size() + 1);
    }
    if (first)
      first = false;
    else
      if ((message[0] == ' ') || (message[0] == '\t'))
	str << message[0];
    str << buf << std::endl;
  }

  return str.str();
}


std::string Message::toString() const {
  std::ostringstream str;
  for(Message::const_iterator i = (*this).begin(); i != (*this).end(); ++i)
    str << messageCut80(*i);

  return str.str();
}

Message & Message::operator<<(const std::string & msg) {
  (*this).push_back(msg);
  return *this;
}

HelpMessage::HelpMessage(const std::string & bName, const std::string & helpMessage) {
  push_back(bName +  " - (" + __DATE__ + " " + __TIME__ + ")");
  push_back("Author: Jean-Marie Favreau (CNRS / Univ. Blaise Pascal) - 2006-2010");
  push_back("Description: ");
  push_back(" " + helpMessage);
}
