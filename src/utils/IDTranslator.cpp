/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <j-marie.favreau@u-clermont1.fr>
 *                    ISIT / Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "IDTranslator.h"
#include "MeshMap.h"

using namespace Taglut;



IDTranslator::IDTranslator(const MeshPart & mPart) {
  VertexID idGlobal = 0;
  VertexID idLocal = 0;
  for(std::deque<bool>::const_iterator mb = mPart.getPointMembership().begin(); mb != mPart.getPointMembership().end(); ++mb, ++idGlobal)
    if (*mb) {
      l2gIDs.push_back(idGlobal);
      g2lIDs[idGlobal] = idLocal;
      ++idLocal;
  }
}

PLPath IDTranslator::l2gPLPath(const PLPath & vector, const Mesh & mesh) const {
  PLPath result(mesh);
  for(PLPath::const_iterator p = vector.begin(); p != vector.end(); ++p)
    result.push_back(l2g(*p, mesh));
  return result;
}

