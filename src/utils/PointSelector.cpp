/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <vector>
#include <algorithm>

#include "PointSelector.h"
#include "Messages.h"
#include "StringManipulation.h"

using namespace Taglut;

PointSelector::PointSelector(const std::string & method_t, const std::string & optionsString) : validityChecked(false), method(method_t) {
  splitOptions(optionsString);
  selected = NULL;
  validityChecked = false;
}

PointSelector::~PointSelector() {
  if (selected != NULL)
    delete [] selected;
}


bool PointSelector::isValidSelector() {
  validityChecked = true;

  // the method should be one of the defined methods
  if ((method != "all") && (method != "id") && (method != "random") && (method != "all no-border") && (method != "random no-border")
       && (method != "all only-border") && (method != "random only-border"))
    return false;

  // the "all" method has no option
  if (((method == "all") || (method == "all no-border")) && (options.size() != 0))
    return false;


  // the "id" method should have id parameters
  if (method == "id") {
    if (options.size() == 0)
      return false;
    for(std::deque<std::string>::const_iterator o = options.begin();
	o != options.end(); ++o) {
      if (!StringManipulation::isUnsignedInt(*o))
	return false;
      idSelected.push_back(StringManipulation::getInt(*o));
    }
  }

  // the "random" method has only one option (a number or a pourcentage)
  if ((method == "random") || (method == "random no-border") || (method == "random only-border")) {
    if (options.size() != 1)
      return false;
    const std::string & option = options[0];
    if (option[option.size() -1] == '%') {
      if (!StringManipulation::isUnsignedPercent(option))
	return false;
      pValue = StringManipulation::getPercent(option);
      if (pValue > 1.0)
	return false;
    }
    else {
      if (!StringManipulation::isUnsignedInt(option))
	return false;
      nbSelected = StringManipulation::getInt(option);
    }
  }

  return true;
}

void PointSelector::splitOptions(const std::string & optionsString) {
  std::string sOptions = optionsString;

  while(sOptions.size() != 0) {
    unsigned int i = sOptions.find(",");
    if (i >= sOptions.size()) {
      options.push_back(sOptions);
      sOptions = "";
    }
    else {
      std::string option = sOptions.substr(0, i);
      unsigned int k = option.rfind(" ");
      if (k < option.size()) {
	option.erase(k);
      }
      options.push_back(option);
      sOptions.erase(0, i + 1);
      unsigned int j = 0;
      while(sOptions[j] == ' ') ++j;
      if (j != 0)
	sOptions.erase(0, j);
    }
  }
}

void PointSelector::setMesh(const Mesh & mesh) {
  if ((!validityChecked) && !isValidSelector())
    throw Exception("setMesh(1): selection method is not valid.");

  if (selected != NULL)
    delete [] selected;

  nbPoints = mesh.getNbPoints();
  selected = new bool[nbPoints];

  bool * current = selected;
  if (method == "all")
    for(VertexID i = 0; i < nbPoints; ++i, ++current)
      *current = true;
  else if (method == "all no-border")
    for(VertexID i = 0; i < nbPoints; ++i, ++current)
      *current = !mesh.point(i).getIsBoundary();
  else if (method == "all only-border")
    for(VertexID i = 0; i < nbPoints; ++i, ++current)
      *current = mesh.point(i).getIsBoundary();
  else {
    // initialization
    for(VertexID i = 0; i < nbPoints; ++i, ++current)
      *current = false;

    if (method == "id") {
      for(std::deque<VertexID>::const_iterator p = idSelected.begin(); p != idSelected.end(); ++p)
	if (*p < nbPoints)
	  selected[*p] = true;
    }
    else if ((method == "random") || (method == "random no-border") || (method == "random only-border")) {
      bool simpleRandom = method == "random";
      bool noBorderRandom = method == "random no-border";

      // first reinit nbselected if needed according to the percent value
      if (StringManipulation::isUnsignedPercent(options[0])) {
	if (simpleRandom)
	  nbSelected = (VertexID) (pValue * mesh.getNbPoints());
	else if (noBorderRandom)
	  nbSelected = (VertexID) (pValue * (mesh.getNbPoints() - mesh.getNbBoundaryPoints()));
	else
	  nbSelected = (VertexID) (pValue * mesh.getNbBoundaryPoints());
      }

      // initialisation of possible points
      std::vector<VertexID> listOfPossiblePoints;

      // create list
      for(VertexID i = 0; i < nbPoints; ++i)
	if (simpleRandom || (noBorderRandom && !mesh.point(i).getIsBoundary()) || (mesh.point(i).getIsBoundary()))
	  listOfPossiblePoints.push_back(i);

      // randomly shuffle elements
      random_shuffle(listOfPossiblePoints.begin(), listOfPossiblePoints.end());

      // each selected points are computed
      VertexID i = 0;
      for(std::vector<VertexID>::const_iterator it = listOfPossiblePoints.begin(); it != listOfPossiblePoints.end(); ++it, ++i) {
	if (i >= nbSelected)
	  break;
	selected[*it] = true;
      }
    }

  }
}

bool PointSelector::isSelected(const Point3D & point) const {
  assert(point.getId() < nbPoints);

  if (!validityChecked)
    throw Exception("isSelected(1): validity of the selection method has not been checked, and mesh has not been set.");

  return selected[point.getId()];
}



std::string PointSelector::helpSelectionMethods() {
  Message msg;

  msg << "Points are selected using the \"selection\" attribute: ";
  msg << "- \"all\": all the points of mesh are selected.";
  msg << "- \"all no-border\": all the points of mesh not in boundary are selected.";
  msg << "- \"all only-border\": all the points of mesh in boundary are selected.";
  msg << "- \"id\": points are selected by id (using \"selection-options\").";
  msg << "- \"random\": a random method is used (using \"selection-options\").";
  msg << "- \"random no-border\": a random method is used (using \"selection-options\") to select no boundary points.";
  msg << "- \"random only-border\": a random method is used (using \"selection-options\") to select boundary points.";

  return msg.toString();
}

