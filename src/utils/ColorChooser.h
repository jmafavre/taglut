/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef COLOR_CHOOSER
#define COLOR_CHOOSER

#include "CImgUse.h"

namespace Taglut {
  /**
     @class ColorChooser
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-06-07
     @brief Define color values for a set of different points
  */

  class ColorChooser {
  private:

    /**
       compute a [0, 256[ value given a [0, 256*6[ value using
       a picewise linear rule.
    */
    static unsigned char computeValue(int value);

  public:
    /** given a color id and a number of colors, compute CImg<> color */
    static CImg<unsigned char> getColorFromId(unsigned int id, unsigned int nbColors);

  };
}

#endif
