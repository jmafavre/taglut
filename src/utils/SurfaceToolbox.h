/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SURFACE_TOOLBOX
#define SURFACE_TOOLBOX

#include "Coord3D.h"
#include "Coord2D.h"

namespace Taglut {
  /**
     @class SurfaceToolbox

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-05-31
     @brief toolbox with surface methods
  */
  class SurfaceToolbox {

  public:

    /** compute surface of triangle (p1, p2, p3) */
    static double surface(const Coord3D & p1, const Coord3D & p2, const Coord3D & p3);

    /** compute surface of triangle (p1, p2, p3) */
    static double surface(const Coord2D & p1, const Coord2D & p2, const Coord2D & p3);

  };
}

#endif
