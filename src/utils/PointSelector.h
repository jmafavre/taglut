/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_SELECTOR
#define POINT_SELECTOR

#include <string>
#include <deque>
#include "Point3D.h"
#include "Mesh.h"

namespace Taglut {
  /**
     @class PointSelector

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2008-01-22
     @brief Point selector (using random methods and others)
  */
  class PointSelector {
  private:
    /** validity has been checked */
    bool validityChecked;

    /** method used to select point */
    const std::string method;

    /** options used during point selection */
    std::deque<std::string> options;

    /** selected flags */
    bool * selected;

    /** number of points */
    VertexID nbPoints;

    /** id of selected points if method = "id" */
    std::deque<VertexID> idSelected;

    /** number of selected points if method = "random" */
    VertexID nbSelected;

    /** percent value if method = "random" */
    double pValue;

    /**
       split options given in parameters (using "," as separator), then
       init options
    */
    void splitOptions(const std::string & optionsString);

  public:


    /**
       Constructor using a string description of method and options for
       points' selection
    */
    PointSelector(const std::string & method, const std::string & optionsString);

    /**
       Destructor
    */
    ~PointSelector();

    /**
       Set mesh that will be manipulated
    */
    void setMesh(const Mesh & mesh);

    /**
       Return true if the point is seleted. Internal data are initialisatied.
    */
    bool isSelected(const Point3D & point) const;

    /**
       Return a string that describe available selection methods
    */
    static std::string helpSelectionMethods();

    /**
       Return true if the current point selector is a valid
       selector (checking method and options)
    */
    bool isValidSelector();

  };
}

#endif
