/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VECTOR_FIELD
#define VECTOR_FIELD


#include <vector>
#include "MeshMap.h"
#include "Coord3D.h"
#include "FileManipulator.h"
#include "Mesh.h"
#include "PLPath.h"

namespace Taglut {
  class Mesh;

  /**
     @class VectorField
     A vector field in a mesh, defined for each triangle
   */
  class VectorField : public std::vector<Coord3D>, public FileManipulator {

    /**
       Adding the supported saving and loading formats
    */
    void addFormats();

  public:
    /** default constructor */
    VectorField() {
      addFormats();
    }

    /**
       Constructor that generate the vector field using the scalar function
       defined by the given \p MeshMap on the given \p Mesh
    */
    VectorField(const MeshMap & mMap, const Mesh & mesh) {
      addFormats();
      computeFromScalarFunction(mMap, mesh);
    }

    /**
       copy constructor
    */
    VectorField(const VectorField & vf) : std::vector<Coord3D>(vf), FileManipulator() {
      addFormats();
    }

    ~VectorField() {

    }

    /**
       generate the vector field using the scalar function
       defined by the given \p MeshMap on the given \p Mesh
    */
    void computeFromScalarFunction(const MeshMap & mMap, const Mesh & mesh);

    /**
       Load from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Save to file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;

    /**
       Save vectors to file (ascii format)
    */
    void saveTXT(const std::string & fileName) const;

    /**
       Load vectors from a file (ascii format)
    */
    void loadTXT(const std::string & fileName);


    /**
       Given a point on an edge, and a corresponding triangle, compute the
       next PointOnEdge according to the vector field, or throws an exception
       if there is no other intersection between the edges of the triangle and the line.
     */
    PointOnEdge getNextPoint(const PointOnEdge & point, const Triangle & t, bool up, const Mesh & mesh, const MeshMap & mMap) const;

    /**
       Given a vertex, and a direction (up or down), returns the
       next PointOnEdge according to the vector field, or throws an exception
       if there is no intersection in this direction.
     */
    PointOnEdge getNextPoint(const Point3D & point, bool up, const Mesh & mesh, const MeshMap & mMap) const;

  };

}

#endif

