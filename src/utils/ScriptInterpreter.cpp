/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "ScriptInterpreter.h"

#include <string>
#include <time.h>

#include "CPMethod.h"
#include "Exception.h"
#include "FileTools.h"
#include "MeshPart.h"
#include "MeshCut.h"
#include "MeshUncut.h"
#include "MeshPatches.h"
#include "MeshPrune.h"
#include "Display3D.h"
#include "DDisplay.h"
#include "MeshBExpr.h"
#include "Messages.h"
#include "XMLSimpleNode.h"
#include "Electrodes.h"
#include "MeshRectPatcher.h"
#include "ABFMethod.h"
#include "PointSelector.h"
#include "PointModifier.h"
#include "StringManipulation.h"
#include "MappingRegistration.h"
#include "MappingComparator.h"
#include "MeshComparator.h"
#include "MultiscaleUnfolding.h"
#include "Length.h"

#include "XMLSimpleNode.h"
#include "IndentManager.h"
#include "MeshPathes.h"

using namespace std;
using namespace Taglut;

std::string ScriptInterpreter::SchemaFileName = "ScriptSchema.xsd";


ScriptInterpreter::ScriptInterpreter(const std::string & script, bool validation, const Mesh & mesh, const std::string & fxmlschema) :
  script(script), fxmlschema(fxmlschema), validation(validation),
  meshes(), mappings(meshes) {
  if (mesh.getNbPoints() != 0)
    meshes.push_back(mesh);
}

unsigned int ScriptInterpreter::runScript() {
#ifdef LIBXML_READER_ENABLED
  idInstruction = 0;

  /* find xml schema */
  string xmlSchema;
  if (fxmlschema != "")
    xmlSchema = fxmlschema;
  else {
    string dirxmlSchema = FileTools::realPath(FileTools::path(script));
    xmlSchema = FileTools::concat(dirxmlSchema, "ScriptSchema.xsd");
    while ((dirxmlSchema != "") && (access(xmlSchema.c_str(), R_OK) != 0)) {
      dirxmlSchema = FileTools::parentdir(dirxmlSchema);
      xmlSchema = FileTools::concat(dirxmlSchema, "ScriptSchema.xsd");
    }
  }

  /* init reader */
  reader = xmlReaderForFile(script.c_str(), NULL, 0);

  if (reader != NULL) {
    // first, xml validation
#if defined(LIBXML_SCHEMAS_ENABLED) && LIBXML_VERSION > 20620
    if (validation) {
      if (access(xmlSchema.c_str(), R_OK) == 0) {
	int rValidation = xmlTextReaderSchemaValidate(reader, xmlSchema.c_str());
	if (rValidation != 0)
	  cout << indentManager << "Warning: validation cannot be activated (using " << xmlSchema << ")" << endl;
	else
	  cout << indentManager << "XML validation activated." << endl;
      }
      else {
	cerr << "Error: XML-Schema \"" << xmlSchema << "\" cannot be found." << endl;
	return 1;
      }
    }
    else
      cout << indentManager << "Skip XML validation" << endl;
#else
    cout << indentManager << "XML validation disabled." << endl;
#endif

    // first skip root item
    jumpNextNode();
    assert(string("script") == (const char *) xmlTextReaderConstName(reader));
    jumpNextNode();

    // and run each instruction
    idInstruction = 1;
    unsigned int result = mainReader("script");

    xmlFreeTextReader(reader);

    return result;
  }
  else
    throw Exception(std::string("XML script not found."));
#else
  throw Exception(std::string("XML reader not available."));
#endif
}


#ifdef LIBXML_READER_ENABLED


void ScriptInterpreter::jumpNextNode() {
  jumpNextNode(reader);
}

void ScriptInterpreter::jumpNextNode(xmlTextReaderPtr & reader) {
  int ret;
  do {
    ret = xmlTextReaderRead(reader);
    if (ret != 1) throw Exception(std::string("Error during reading xml file."));
  } while((xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) &&
	  (xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT));
}

std::deque<std::string> ScriptInterpreter::readOptions(string attribute) {
  const char * options = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)attribute.c_str());
  if (options == NULL)
    return deque<string>();

  string sOptions(options);
  return computeOptions(sOptions);
}

std::deque<std::string> ScriptInterpreter::computeOptions(string sOptions) {
  std::deque<std::string> result;
  while(sOptions.size() != 0) {
    int i = sOptions.find(",");
    int p = sOptions.find("(");
    if ((p >= 0) && (p < (int)sOptions.size()) && (p < i)) {
      int p2 = sOptions.find(")");
      if ((p2 >= 0) && (p2 < (int)sOptions.size())) {
	// if comma is between "(" and ")", find next comma
	i = sOptions.find(",", p2);
      }
    }

    if ((i >= (int)sOptions.size()) || (i < 0)) {
      result.push_back(sOptions);
      sOptions = "";
    }
    else {
      string option = sOptions.substr(0, i);
      unsigned int k = option.rfind(" ");
      if (k < option.size()) {
	option.erase(k);
      }
      result.push_back(option);
      sOptions.erase(0, i + 1);
      unsigned int j = 0;
      while(sOptions[j] == ' ') ++j;
      if (j != 0)
	sOptions.erase(0, j);
    }
  }

  return result;
}

int ScriptInterpreter::breakMsg() {
  cout << indentManager << "Break (instr. " <<  idInstruction << ")." << endl;
  return 1;
}

int ScriptInterpreter::outputClustering(int meshId) {
  const char * oMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");

  if ((string(oMethod) == "display") || (oMethod == NULL)) {
    unsigned int sizex, sizey;
    const char * cSizex = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizex");
    if (cSizex == NULL) sizex = 512; else sizex = atoi(cSizex);
    const char * cSizey = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizey");
    if (cSizey == NULL) sizey = 512; else sizey = atoi(cSizey);
    deque<string> options = readOptions();
    const char * name = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"name");
    string realName;
    if (name != NULL) realName = name; else realName = "Display";


    CImgList<unsigned char> resultImgs;
    // first compute images
    int id = 0;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++id)
      if ((meshId < 0) || (meshId == id))
	resultImgs.insert((*mapping).clustersToImage(sizex, sizey));

    if (resultImgs._width > 0) {
      // then build displays
      CImgDisplay ** displays;
      displays = new CImgDisplay * [resultImgs._width];
      cimglist_for(resultImgs, i)
	displays[i] = new CImgDisplay(resultImgs[i], "image");
      bool disp = true;

      while(disp) {
	(*displays[0]).wait(50);
	cimglist_for(resultImgs, i) {
	  if ((*displays[i])._is_closed) {
	    disp = false;
	    break;
	  }
	  if ((*displays[i]).key() == cimg::keyESC) {
	    disp = false;
	    break;
	  }
	}
      }

      cimglist_for(resultImgs, i)
	delete(displays[i]);
      delete [] displays;
    }
  } else if (string(oMethod) == "file") {
    const char * filename = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");
    if (filename == NULL) {
      cerr << "Error (output, instr. " <<  idInstruction << "): output file not defined (attribute \"filename\")." << endl;
      return 1;
    }
    unsigned int sizex, sizey;
    const char * cSizex = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizex");
    if (cSizex == NULL) sizex = 512; else sizex = atoi(cSizex);
    const char * cSizey = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizey");
    if (cSizey == NULL) sizey = 512; else sizey = atoi(cSizey);

    string realFilename;
    if (FileTools::isAbsoluteName(filename))
      realFilename = filename;
    else
      realFilename = FileTools::realPath(FileTools::path(script) + SEPARATOR + filename);

    cout << indentManager << "Saving file (" << realFilename << ")..." << endl;

    unsigned int i = 0;
    int id = 0;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping!= mappings.end(); ++mapping, ++id)
      if ((meshId < 0) || (meshId == id)) {
	CImg<unsigned char> resultImg = (*mapping).clustersToImage(sizex, sizey);
	resultImg.save(FileTools::computeFileName(realFilename, mappings.size(), i).c_str());
	++i;
      }

  } else if (string(oMethod) == "infos")
    cout << indentManager << mappings.getClusterInfos();
  else
    cout << indentManager << "Warning (adjust-cutting, output, instr. " << idInstruction << "): \"" << oMethod << "\" is unknown." << endl;

  jumpNextNode();
  return 0;


}


int ScriptInterpreter::outputMeshPathes(MeshPathes & mCut, const std::deque<XMLSimpleNode> & outputNodes, unsigned int idM) {
  if (mCut.size() == 0)
    cout << indentManager << "Number of cut path: 0" << endl;
  else
    for(std::deque<XMLSimpleNode>::const_iterator n = outputNodes.begin(); n != outputNodes.end(); ++n) {
      const string & oMethod = (*n).getAttribute("method");

      if ((oMethod == "display") || (oMethod == "")) {
	unsigned int sizex, sizey;
	const string & cSizex = (*n).getAttribute("sizex");
	if (cSizex == "") sizex = 512; else sizex = atoi(cSizex.c_str());
	const string & cSizey = (*n).getAttribute("sizey");
	if (cSizey == "") sizey = 512; else sizey = atoi(cSizey.c_str());
	deque<string> options = computeOptions((*n).getAttribute("options"));
	const string & name = (*n).getAttribute("name");
	string realName;
	if (name != "") realName = name; else realName = "Display";
	bool meshesb = false;
	bool pathes = false;
	bool opengl = false;
	bool map2D = false;
	for(deque<string>::iterator option = options.begin(); option != options.end(); ++option)
	  if ((*option == "meshes") || (*option == "triangles")) meshesb = true;
	  else if (*option == "pathes") pathes = true;
	  else if (*option == "opengl") opengl = true;
	  else if ((*option == "map2D") || (*option == "2D") || (*option == "dim2D")) map2D = true;
	  else cout << indentManager << " Warning (display, instr. " << idInstruction << "): option \"" << *option << "\" unknown." << endl;
	if ((mappings.size() == 0) && (map2D)) {
	  cerr << indentManager << " Warning: \"map2D\" option is not available because 2d mapping does not exists" << endl;
	  map2D = false;
	}
	if (pathes || map2D) {
	  if (map2D) {
	    Mesh fMesh = mappings[idM];
	    MeshPathes * mCutFlat = mCut.clone(fMesh);
	    Display3D::displayMeshPathes(sizex, sizey, realName, *mCutFlat, meshesb, opengl);
	    delete mCutFlat;
	  }
	  else
	    Display3D::displayMeshPathes(sizex, sizey, realName, mCut, meshesb, opengl);
	}
	else
	  Display3D::displayMesh(sizex, sizey, realName, mCut.cutMesh(), meshesb, true, opengl);
      }
      else if (oMethod == "file") {
	const string & filename = (*n).getAttribute("filename");
	string realFilename;
	if (FileTools::isAbsoluteName(filename))
	  realFilename = filename;
	else
	  realFilename = FileTools::realPath(FileTools::path(script) + SEPARATOR + filename);
	if (mCut.isExportFormat(realFilename)) {
	  cout << indentManager << "Saving file (" << realFilename << ")..." << endl;
	  mCut.exportToFile(realFilename);
	}
	else
	  cout << indentManager << " Warning: \"" << realFilename << "\" is not a known export format." << std::endl;

      } else if (oMethod == "infos") {
	cout << indentManager << " Number of cut path: " << mCut.size() << endl;
	unsigned int i = 0;
	for(MeshCut::iterator c = mCut.begin(); c != mCut.end(); ++c) {
	  if ((*c).front() == (*c).back())
	    cout << indentManager << "  Path #" << i << ": " << ((*c).size() - 1) << " edges (cycle)" << endl;
	  else
	    cout << indentManager << "  Path #" << i << ": " << ((*c).size() - 1) << " edges (path)" << endl;
	  ++i;
	}
      } else
	cout << indentManager << " Warning (cut, output, instr. " << idInstruction << "): \"" << oMethod << "\" is unknown." << endl;
    }

  return 0;
}


int ScriptInterpreter::printMessage() {
  const char * msg = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"msg");
  if (msg == NULL) {
    cerr << "Error (print, instr. " <<  idInstruction << "): message not found (attribute \"msg\")." << endl;
    return 1;
  }
  cout << indentManager << msg << endl;

  jumpNextNode();
  return 0;
}


int ScriptInterpreter::waitInstruction() {
  const char * msg = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"msg");
  if (msg == NULL)
    cout << indentManager << "Press <enter> to continue" << std::endl;
  else
    cout << indentManager << msg << endl;

  fflush(stdin);
  cin.get();

  jumpNextNode();
  return 0;
}

int ScriptInterpreter::sleepInstruction() {
  const char * msg = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"msg");
  const char * time = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"char");
  unsigned int nbs = 5;
  if (time != NULL)
    nbs = atoi(time);

  if (msg == NULL)
    cout << indentManager << "Sleep during " << nbs << " second(s)" << endl;
  else
    cout << indentManager << msg << " (" << nbs << " second(s))" << endl;

  for(unsigned int i = nbs; i > 0; --i) {
    (cout << indentManager << "\r Waiting: " << i).flush();
    sleep(1);
  }

  (cout << "\r").flush();

  jumpNextNode();
  return 0;
}


int ScriptInterpreter::selectMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (select, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  cout << indentManager << "Selecting connected component..." << endl;
  const char * m = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"connected-component");
  const char * x = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointx");
  const char * y = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointy");
  const char * z = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointz");
  if ((c == NULL) && ((x == NULL) || (y == NULL) || (z == NULL)) && (m == NULL)) {
    cerr << "Error  (select, instr. " <<  idInstruction << "): method, connected-component and point not defined (attribute \"method\", \"connected-component\" or \"pointx\", \"pointy\", \"pointz\")."<< endl;
    return 1;
  }
  if ((c != NULL) && ((m == NULL) || (string(m) == "id"))) {
    unsigned int cComponent = atoi(c);
    deque<MeshPart> cc = meshes.getConnectedComponents();
    if (cc.size() <= cComponent) {
      cerr << "Error (select, instr. " << idInstruction << "): connected component #" << cComponent << " unknown. Surface has " << cc.size() << " connected component(s)."<< endl;
      return 1;
    }
    meshes = cc[cComponent];
    mappings.clear();
  }
  else if ((x != NULL) && (y != NULL) && (z != NULL) && ((m == NULL) || (string(m) == "location"))) {
    Point3D point(atof(x), atof(y), atof(z));
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m) {
      MeshPart mPart(*m, point);
      mPart.cropMesh();
    }
    mappings.clear();
  }
  else if (string(m) == "size") {
    meshes = meshes.getLargestCC();
    mappings.clear();
  }
  else {
    cerr << "Error  (select, instr. " <<  idInstruction << "): method, connected-component and point not defined (attribute \"method\", \"connected-component\" or \"pointx\", \"pointy\", \"pointz\")."<< endl;
    return 1;
  }

  jumpNextNode();

  return 0;
}

int ScriptInterpreter::cropMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (crop, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  cout << indentManager << "Cropping mesh..." << endl;

  const char * cMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  if (cMethod == NULL) {
    cerr << "Error (crop, instr. " <<  idInstruction << "): method not defined (attribute \"method\")."<< endl;
    return 1;
  }

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (crop, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << ")."<< endl;
    return -1;
  }


  if (string("ball") == cMethod) {
    const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"radius");
    double radius = (c == NULL? 10 : atof(c));
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointx");
    double centerx = (c == NULL? 10 : atof(c));
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointy");
    double centery = (c == NULL? 10 : atof(c));
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointz");
    double centerz = (c == NULL? 10 : atof(c));
    Point3D center(centerx, centery, centerz);
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshPart mPart(*m, center, radius, false);
	mPart.cropMesh();
      }
    mappings.clear();
  }
  else if (string("box") == cMethod) {
    const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointx");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): pointx not defined (attribute \"pointx\")." << endl; return 1; }
    double centerx = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointy");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): pointy not defined (attribute \"pointy\")." << endl; return 1; }
    double centery = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointz");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): pointz not defined (attribute \"pointz\")." << endl; return 1; }
    double centerz = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizex");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): sizex not defined (attribute \"sizex\")." << endl; return 1; }
    double sizex = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizey");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): sizey not defined (attribute \"sizey\")." << endl; return 1; }
    double sizey = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizez");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): sizez not defined (attribute \"sizez\")." << endl; return 1; }
    double sizez = atof(c);

    Point3D point1(centerx - sizex / 2, centery - sizey / 2, centerz - sizez / 2);
    Point3D point2(centerx + sizex / 2, centery + sizey / 2, centerz + sizez / 2);
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshPart mPart(*m, point1, point2);
	mPart.cropMesh();
      }
    mappings.clear();
  }
  else if ((string("disc") == cMethod) || (string("ball2d") == cMethod)) {
    const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"radius");
    double radius = (c == NULL? 10 : atof(c));
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointx");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): pointx not defined (attribute \"pointx\")." << endl; return 1; }
    double centerx = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointy");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): pointy not defined (attribute \"pointy\")." << endl; return 1; }
    double centery = atof(c);
    c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointz");
    if (c == NULL) { cerr << "Error (crop, instr. " << idInstruction << "): pointz not defined (attribute \"pointz\")." << endl; return 1; }
    double centerz = atof(c);

    Point3D center(centerx, centery, centerz);
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshPart mPart(*m, center, radius);
	if (string("ball2d") == cMethod)
	  mPart.cropMesh();
	else
	  mPart.cropSimpleCut();
      }
    mappings.clear();
  }
  else
    cout << indentManager << " Warning (crop, instr. " << idInstruction << "): unknown method (\"" << cMethod << "\")." << endl;

  jumpNextNode();

  return 0;
}


int ScriptInterpreter::cleanMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (clean, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  cout << indentManager << "Cleaning mesh..." << endl;

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (clean, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }

  const char * cMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");

  if ((cMethod == NULL) || (string("simple") == cMethod)) {
    unsigned int nb = 0;
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshPart mPart(*m);
	nb += mPart.cleanMesh();
      }
    mappings.clear();
    if (nb != 0)
      cout << indentManager << " Info (crop, instr. " << idInstruction << "): " << nb << " point removed." << endl;
  }
  else
    cout << indentManager << " Warning (clean, instr. " << idInstruction << "): unknown method (\"" << cMethod << "\")." << endl;

  jumpNextNode();

  return 0;
}

int ScriptInterpreter::reorientMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (reorient, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (reorient, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }

  cout << indentManager << "Reorienting mesh..." << endl;

  int i = 0;
  for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
    if ((meshId < 0) || (meshId == i))
      (*m).reorient();

  mappings.clear();

  jumpNextNode();

  return 0;
}

int ScriptInterpreter::duplicateMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (duplicate, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (duplicate, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }
  if (meshId == -1)
    meshId = 0;

  cout << indentManager << "Duplicate mesh..." << endl;

  // duplicate mesh
  meshes.push_back(meshes[meshId]);

  mappings.clear();

  jumpNextNode();

  return 0;
}


int ScriptInterpreter::patchMesh() {
  cout << indentManager << "Patching surface... " << endl;

  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (patch, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  LengthFactory lFactory;

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (patch, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }

  // first check number of connected components
  if (meshId < 0) {
    if (meshes.splitCC())
      mappings.clear();
  } else if (meshes[meshId].getNbCC() != 1) {
    cerr << "Error (instr. " << idInstruction << "): mesh #" << meshId << " has more than one connected component." << endl;
    return 1;
  }

  const char * lengthMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"length-method");

  const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"ratio-length");
  double ratioLength = 0.5;
  if (c != NULL)
    ratioLength = atof(c);

  float mLength = 1;
  const char * maxLength = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"max-length");
  if (maxLength != NULL) {
    mLength = atof(maxLength);
    if (mLength <= 0) {
      cerr << "Error (patches, instr. " <<  idInstruction << "): max length must be positive."<< endl;
      return -1;
    }
  }

  const char * filename = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");
  const char * log = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"log");

  lFactory.setLength(lengthMethod);
  if (!lFactory.existsLength()) {
    cerr << "Error (instr. " << idInstruction << "): length method unknown (try --help)." << endl;
    return 1;
  }

  c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"max-patch");
  int maxPatch = -1;
  if (filename != NULL)
    maxPatch = 0;
  if (c != NULL)
    maxPatch = atoi(c);

  c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"optim-sc");

  MeshPathes::OptimEC optimSC = MeshPathes::OECTruncMinMax;
  if (c != NULL) {
    unsigned int optimsc = atoi(c);
    if (optimsc == 0)
      optimSC = MeshPathes::OECnoTrunc;
    else if (optimsc == 1)
      optimSC = MeshPathes::OECTruncMin;
    else if (optimsc == 2)
      optimSC = MeshPathes::OECTruncMax;
    else if (optimsc == 3)
      optimSC = MeshPathes::OECTruncMinMax;
    else if (optimsc == 4)
      optimSC = MeshPathes::OECsimple;
    else if (optimsc == 5)
      optimSC = MeshPathes::OECbyStrip;
    else if (optimsc == 6)
      optimSC = MeshPathes::OECbyStripAndPredCut;
  }

  if (ratioLength >= 0)
    lFactory.setRatio(ratioLength);


  int i = 0;
  for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
    if ((meshId < 0) || (meshId == i)) {
      MeshPatches mPatches(*m, lFactory.getLength(*m), mLength);
      if (log != NULL)
	mPatches.setLogFile(FileTools::computeFileName(log, meshes.size(), i));
      if (filename != NULL)
	mPatches.patchMesh(maxPatch, optimSC, filename, "", indentManager);
      else
	mPatches.patchMesh(maxPatch, optimSC, "", "", indentManager);
    }

  mappings.clear();

  jumpNextNode();
  return 0;

}

int ScriptInterpreter::uncutMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (cut, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  cout << indentManager << "Sticking cuttings... " << endl;
  const char * method = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  unsigned int nbPoints = meshes.getNbPoints();

  if ((method == NULL) || (string(method) == "simple")) {
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m) {
      MeshUncut mUncut(*m);
      *m = mUncut.getUMesh();
    }
  }
  else if (string(method) == "complete") {
    Mesh mergedMesh(meshes);
    MeshUncut mUncut(mergedMesh);
    meshes = mUncut.getUMesh();
  }
  else if (string(method) == "merge")
    meshes = Mesh(meshes);
  else {
    cerr << "Error (uncut, instr. " <<  idInstruction << "): unknown method (\"" << method << "\")."<< endl;
    return -1;
  }

  mappings.clear();

  cout << indentManager << (nbPoints - meshes.getNbPoints()) << " point(s) has been removed." << endl;

  jumpNextNode();
  return 0;

}


void ScriptInterpreter::loadChildren(std::deque<XMLSimpleNode> & outputNodes, const std::string & name) {
  jumpNextNode();
  while(name == (const char *) xmlTextReaderConstName(reader)) {
    outputNodes.push_back(XMLSimpleNode(reader));
    jumpNextNode();
  }
  if (!((string("cut") == (const char *) xmlTextReaderConstName(reader)) && (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)) &&
      !((string("adjust-cutting") == (const char *) xmlTextReaderConstName(reader)) && (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)))
    cout << indentManager << " Warning (*, output, instr. " << idInstruction << "): \"" << (const char *) xmlTextReaderConstName(reader) << "\" unknown instruction." << endl;
}


int ScriptInterpreter::pruneMesh() {
  const char * method = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  const char * depth = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"depth");

  int depthValue = 0;
  if (depth != NULL)
    depthValue = atoi(depth);

  int meshId = getMeshId();

  if (meshId == -2) {
    cerr << "Error (prune, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }

  if ((method == NULL) || (string(method) == "simple-curvature")) {
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshPrune prune(*m, depthValue);
	prune.pruneMesh();
      }
  }
  else {
    cerr << "Error (prune, instr. " <<  idInstruction << "): unknown method (\"" << method << "\")."<< endl;
    return -1;
  }

  // then clear mappings
  mappings.clear();

  jumpNextNode();
  return 0;

}


int ScriptInterpreter::cutMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (cut, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  LengthFactory lFactory;
  bool hasChildren = (xmlTextReaderIsEmptyElement(reader) == 0);

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (cut, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }

  // first check number of connected components
  if (meshId < 0) {
    if (meshes.splitCC())
      mappings.clear();
  } else if (meshes[meshId].getNbCC() != 1) {
    cerr << "Error (instr. " << idInstruction << "): mesh #" << meshId << " has more than one connected component." << endl;
    return 1;
  }

  const char * method = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  const char * log = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"log");
  const char * lengthMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"length-method");
  const char * rL = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"ratio-length");
  const char * filename = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");
  const char * maxCut = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"max-cut");
  const char * osc = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"optim-sc");
  const char * curv = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"min-curvature");
  const char * mergemp = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"merge-multipoints");

  std::deque<XMLSimpleNode> outputNodes;
  if (hasChildren)
    loadChildren(outputNodes, "output");


  int mCut = -1;
  if (maxCut != NULL)
    mCut = atoi(maxCut);

  cout << indentManager << "Cutting surface... " << endl;
  if ((method == NULL) || (string(method) == "simple") || (string(method) == "rectangular") || (string(method) == "quadrangulation")) {

    double ratioLength = 0.5;
    if (rL != NULL)
      ratioLength = atof(rL);

    if (filename != NULL)
      mCut = 0;

    MeshPathes::OptimEC optimSC = MeshPathes::OECTruncMinMax;
    if (osc != NULL) {
      unsigned int optimsc = atoi(osc);
      if (optimsc == 0)
	optimSC = MeshPathes::OECnoTrunc;
      else if (optimsc == 1)
	optimSC = MeshPathes::OECTruncMin;
      else if (optimsc == 2)
	optimSC = MeshPathes::OECTruncMax;
      else if (optimsc == 3)
	optimSC = MeshPathes::OECTruncMinMax;
      else if (optimsc == 4)
	optimSC = MeshPathes::OECsimple;
      else if (optimsc == 5)
	optimSC = MeshPathes::OECbyStrip;
      else if (optimsc == 6)
	optimSC = MeshPathes::OECbyStripAndPredCut;
    }

    MeshCut::MergeMPointsMethod mergeMP = MeshCut::MMPnone;
    if ((method != NULL) && ((string(method) == "rectangular") || (string(method) == "quadrangulation")))
      mergeMP = MeshCut::MMPsimple;

    if (mergemp != NULL) {
      if (string(mergemp) == "none")
	mergeMP = MeshCut::MMPnone;
      else if (string(mergemp) == "simple")
	mergeMP = MeshCut::MMPsimple;
      else if (string(mergemp) == "one-point")
	mergeMP = MeshCut::MMPonePoint;
    }

    lFactory.setLength(lengthMethod);
    if (!lFactory.existsLength()) {
      cerr << "Error (instr. " << idInstruction << "): length method unknown (try --help-xml)." << endl;
      return 1;
    }

    if (ratioLength >= 0)
      lFactory.setRatio(ratioLength);

    if (meshId < 0)
      meshes.splitCC();
    else if (meshes[meshId].getNbCC() != 1) {
	cerr << "Error (instr. " << idInstruction << "): mesh #" << meshId << " has more than one connected component." << endl;
	return 1;
      }

    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshCut meshCutter(*m, lFactory.getLength(*m));
	if (log != NULL)
	  meshCutter.setLogFile(FileTools::computeFileName(log, meshes.size(), i));

	try {
	  if ((method != NULL) && ((string(method) == "rectangular") || (string(method) == "quadrangulation"))) {
	    if (filename != NULL)
	      meshCutter.buildRectangularCut(mCut, MeshCut::CMEricksonPlus, mergeMP, MeshManipulator::UMFast, optimSC, filename, "", indentManager);
	    else
	      meshCutter.buildRectangularCut(mCut, MeshCut::CMEricksonPlus, mergeMP, MeshManipulator::UMFast, optimSC, "", "", indentManager);
	  } else {
	    if (filename != NULL)
	      meshCutter.buildMinimalCut(mCut, MeshCut::CMEricksonPlus, mergeMP, MeshManipulator::UMFast, optimSC, filename, "", indentManager);
	    else
	      meshCutter.buildMinimalCut(mCut, MeshCut::CMEricksonPlus, mergeMP, MeshManipulator::UMFast, optimSC, "", "", indentManager);
	  }

	}
	catch(Exception e) {
	  cerr << endl << "Error (cut, instr. " << idInstruction << "): " << e << endl;
	  return -1;
	}

	if (outputNodes.size() != 0) {
	  int v = outputMeshPathes(meshCutter, outputNodes, i);
	  if (v != 0)
	    return v;
	}

	meshCutter.cutMesh();
      }

  }
  else if (string(method) == "curvature") {
    double alpha = M_PI / 3;
    if (curv != NULL)
      alpha = atof(curv);

    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshCut meshCutter(*m);
	if (log != NULL)
	  meshCutter.setLogFile(FileTools::computeFileName(log, meshes.size(), i));

	meshCutter.buildCutUsingCurvature(alpha, mCut, indentManager);

	if (outputNodes.size() != 0) {
	  int v = outputMeshPathes(meshCutter, outputNodes, i);
	  if (v != 0)
	    return v;
	}

	meshCutter.cutMesh();
      }
  }
  else {
    cerr << "Error (cut, instr. " <<  idInstruction << "): unknown method (attribute \"method\")."<< endl;
  }

  assert(string((const char *) xmlTextReaderConstName(reader)) == "cut");
  jumpNextNode();

  mappings.clear();

  return 0;
}


int ScriptInterpreter::pointManipulationMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): empty mesh." << endl;
    return -1;
  }

  const char * method = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  const char * selection = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"selection");
  const char * methodOptions = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method-options");
  const char * selectionOptions = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"selection-options");


  int meshId = getMeshId();

  if (meshId == -2) {
    cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }


  if (method == NULL) {
    cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): method not defined (attribute \"method\")."<< endl;
    return -1;
  }


  if (string(method) == "remove-nonrealpoints") {
    if ((selection != NULL) && (string(selection) != "all")) {
      cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): the only selection method allowed using \"remove-nonrealpoints\" is \"all\" (was \"" << selection << "\")." << endl;
      return -1;
    }
    cout << indentManager << "Point manipulation: removing non real points..." << endl;

    unsigned int rm = 0;
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i))
	rm += (*m).removeNonRealPoints();
    mappings.clear();
    cout << indentManager << " " << rm << " point removed." << endl;
  }
  else if (string(method) == "setall-realpoints") {
    if ((selection != NULL) && (string(selection) != "all")) {
      cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): the only selection method allowed using \"setall-realpoints\" is \"all\" (was \"" << selection << "\")." << endl;
      return -1;
    }
    cout << indentManager << "Point manipulation: set all points as real points..." << endl;
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i))
	(*m).setAllRealPoints();
    mappings.clear();
  }
  else if (string(method) == "modify-location") {
    PointSelector pSelector(selection == NULL ? "all" : selection, selectionOptions == NULL ? "" : selectionOptions);
    PointModifier pModifier(method, methodOptions == NULL ? "" : methodOptions);

    if (!pSelector.isValidSelector()) {
      cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): the selection method is not valid (selection=\"" << selection << "\", selection-options=\"" << selectionOptions << "\")."<< endl;
      return -1;
    }

    if (!pModifier.isValidModifier()) {
      cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): the modification method is not valid (method=\"" << method << "\", method-options=\"" << methodOptions << "\")."<< endl;
      return -1;
    }

    cout << indentManager << "Point manipulation: modify location..." << endl;
    int i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	pSelector.setMesh(*m);
	pModifier.setMesh(*m);
	for(Mesh::point_iterator p = (*m).point_begin(); p != (*m).point_end(); ++p)
	  if (pSelector.isSelected(*p))
	    pModifier.modify(*p);
      }
  }
  else {
    cerr << "Error (point-manipulation, instr. " <<  idInstruction << "): unknown method (\"" << method << "\")."<< endl;
    return -1;

  }

  jumpNextNode();
  return 0;
}


int ScriptInterpreter::addPointMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error (add-point, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  const char * meshId = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"mesh-id");

  const char * point1 = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"point1");
  const char * point2 = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"point2");

  unsigned int id = 0;
  if (meshId != NULL)
    id = atoi(meshId);

  if (id >= meshes.size()) {
    cerr << "Error (add-point, instr. " <<  idInstruction << "): \"mesh-id\" is out of bound, there is only " << meshes.size() << " meshes."<< endl;
    return -1;
  }

  Mesh & mesh = meshes[id];

  cout << indentManager << "Adding point..." << endl;

  if ((point1 != NULL) && (point2 != NULL)) {
    unsigned int p1 = atoi(point1);
    unsigned int p2 = atoi(point2);
    if (p1 >= mesh.getNbPoints()) {
      cerr << "Error (add-point, instr. " <<  idInstruction << "): \"point1\" is out of bound, there is only " << mesh.getNbPoints() << " points on the mesh."<< endl;
      return -1;
    }
    if (p2 >= mesh.getNbPoints()) {
      cerr << "Error (add-point, instr. " <<  idInstruction << "): \"point2\" is out of bound, there is only " << mesh.getNbPoints() << " points on the mesh."<< endl;
      return -1;
    }
    if (!mesh.point(p1).hasNeighbour(p2)) {
      cerr << "Error (add-point, instr. " <<  idInstruction << "): \"point1\" and \"point2\" are not neighbour points."<< endl;
      return -1;
    }

    mesh.addPointOnEdge(p1, p2);
  }
  else {
    cerr << "Error (add-point, instr. " <<  idInstruction << "): location not defined (attributes \"point1\" and \"point2\")."<< endl;
    return -1;
  }

  mappings.clear();

  jumpNextNode();
  return 0;

}


int ScriptInterpreter::flipTriangleMesh() {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error (flip-triangle, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  const char * meshId = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"mesh-id");

  const char * point1 = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"point1");
  const char * point2 = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"point2");
  const char * triangle1 = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"triangle1");
  const char * triangle2 = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"triangle2");

  unsigned int id = 0;
  if (meshId != NULL)
    id = atoi(meshId);

  if (id >= meshes.size()) {
    cerr << "Error (add-point, instr. " <<  idInstruction << "): \"mesh-id\" is out of bound, there is only " << meshes.size() << " meshes."<< endl;
    return -1;
  }

  Mesh & mesh = meshes[id];

  cout << indentManager << "Flipping triangles..." << endl;

  if ((point1 != NULL) && (point2 != NULL)) {
    unsigned int p1 = atoi(point1);
    unsigned int p2 = atoi(point2);
    if (p1 >= mesh.getNbPoints()) {
      cerr << "Error (flip-triangle, instr. " <<  idInstruction << "): \"point1\" is out of bound, there is only " << mesh.getNbPoints() << " points on the mesh."<< endl;
      return -1;
    }
    if (p2 >= mesh.getNbPoints()) {
      cerr << "Error (flip-triangle, instr. " <<  idInstruction << "): \"point2\" is out of bound, there is only " << mesh.getNbPoints() << " points on the mesh."<< endl;
      return -1;
    }
    if (!mesh.point(p1).hasNeighbour(p2)) {
      cerr << "Error (add-point, instr. " <<  idInstruction << "): \"point1\" and \"point2\" are not neighbour points."<< endl;
      return -1;
    }
    std::pair<unsigned int, unsigned int> resultPoints = mesh.flipEdge(p1, p2);

    std::cout << indentManager << " Points " << p1 << " and " << p2 << " has been flipped. The new edge is (" << resultPoints.first << ", " << resultPoints.second << ")" << std::endl;

  }
  else if ((triangle1 != NULL) && (triangle2 != NULL)) {
    unsigned int t1 = atoi(triangle1);
    unsigned int t2 = atoi(triangle2);
    if (t1 >= mesh.getNbTriangles()) {
      cerr << "Error (flip-triangle, instr. " <<  idInstruction << "): \"triangle1\" is out of bound, there is only " << mesh.getNbTriangles() << " triangles on the mesh."<< endl;
      return -1;
    }
    if (t2 >= mesh.getNbTriangles()) {
      cerr << "Error (flip-triangle, instr. " <<  idInstruction << "): \"triangle2\" is out of bound, there is only " << mesh.getNbTriangles() << " triangles on the mesh."<< endl;
      return -1;
    }

    try {
      std::pair<unsigned int, unsigned int> edge = mesh.triangle(t1).getCommonEdge(mesh.triangle(t2));

      std::pair<unsigned int, unsigned int> resultPoints = mesh.flipEdge(edge.first, edge.second);

      std::cout << indentManager << " Triangles " << t1 << " and " << t2 << " has been flipped. The new edge is (" << resultPoints.first << ", " << resultPoints.second << ")" << std::endl;
    }
    catch (Exception e) {
      cerr << "Error (add-point, instr. " <<  idInstruction << "): \"triangle1\" and \"triangle2\" are not neighbour triangles."<< endl;
      return -1;
    }
  }
  else {
    cerr << "Error (flip-triangle, instr. " <<  idInstruction << "): location not defined (attributes \"point1\" and \"point2\" or \"triangle1\" and \"triangle2\")."<< endl;
    return -1;
  }

  mappings.clear();

  jumpNextNode();
  return 0;
}

int ScriptInterpreter::getMeshId() {
  const char * meshIdS = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"mesh-id");
  unsigned int meshId;
  if (meshIdS != NULL) {
    meshId = atoi(meshIdS);
    if (meshId >= meshes.size())
      return -2;

    return meshId;
  }
  else
    return -1;

}

int ScriptInterpreter::adjustCutting() {
  bool hasChildren = (xmlTextReaderIsEmptyElement(reader) == 0);

  if (mappings.size() == 0) {
    cerr << "Error (adjust-cutting, instr. " <<  idInstruction << "): unfolding needs updating."<< endl;
    return -1;
  }

  cout << indentManager << "Adjust cutting..." << endl;
  const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"ratio-limit");
  const char * m = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  const char * verbose = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"verbose");

  int meshId = getMeshId();

  if (meshId == -2) {
    cerr << "Error (adjust-cutting, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }

  if ((m == NULL) || (string(m) == "cluster")) {
    const char * cm = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"cluster-method");

    if (c != NULL) {
      double limitRatio = atof(c);
      int i = 0;
      for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++i)
	if ((meshId < 0) || (meshId == i))
	  (*mapping).computeClusters(limitRatio, -1, ClusterGenerator::CMratiosimple, 0.2, indentManager);
    }
    else {
      int i = 0;
      for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++i)
	if ((meshId < 0) || (meshId == i))
	  (*mapping).computeClusters(-1, -1, ClusterGenerator::CMratiosimple, 0.2, indentManager);
    }


    if (hasChildren) {
      jumpNextNode();
      while(string("output") == (const char *) xmlTextReaderConstName(reader)) {
	int v = outputClustering(meshId);
	if (v != 0) return v;
      }

      if (!((string("adjust-cutting") == (const char *) xmlTextReaderConstName(reader)) && (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)))
	cout << indentManager << " Warning (adjust-cutting, instr. " << idInstruction << "): \"" << (const char *) xmlTextReaderConstName(reader) << "\" unknown instruction." << endl;

      jumpNextNode();
    }
    else
      jumpNextNode();

    MeshList::iterator mesh = meshes.begin();
    int i = 0;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++i)
      if ((meshId < 0) || (meshId == i)) {
	MeshCut mcut(*mesh);
	if ((cm == NULL) || (string(cm) == "simple"))
	  mcut.cutMeshUsingPoints((*mapping).getExtrema());
	else if (string(cm) == "split")
	  mcut.cutMeshUsingPointsSplitCC((*mapping).getExtrema(), indentManager);
	else {
	  cerr << "Error (adjust-cutting, instr. " <<  idInstruction << "): unknown method."<< endl;
	  return -1;
	}
	++mesh;
	(*mapping).updatePoints(mcut);
      }

  }
  else if (string(m) == "rectangular") {
    std::cout << indentManager << "adjust-cutting using \"rectangular\" method is deprecated. Better use cut instruction with \"rectangular\" method." << std::endl;
    const char * mc = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"max-cut");

    unsigned int maxCut;
    if (mc != NULL)
      maxCut = atoi(mc);
    else
      maxCut = 0;

    // load children if needed
    std::deque<XMLSimpleNode> outputNodes;
    if (xmlTextReaderIsEmptyElement(reader) == 0)
      loadChildren(outputNodes, "output");

    jumpNextNode();

    // for each mapping
    int i = 0;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++i)
      if ((meshId < 0) || (meshId == i)) {
	// first cut
	MeshRectPatcher mRPatcher(*mapping);

	if (c != NULL)
	  mRPatcher.setRatioArea(atof(c));

	if (maxCut != 0)
	  mRPatcher.buildRectangleCut(indentManager, (verbose != NULL) && (1 == atoi(verbose)), maxCut);
	else
	  mRPatcher.buildRectangleCut(indentManager, (verbose != NULL) && (1 == atoi(verbose)));

	// then display
	if (outputNodes.size() != 0) {
	  int v = outputMeshPathes(mRPatcher, outputNodes, i);
	  if (v != 0)
	    return v;
	}

	mRPatcher.cutMesh();
	(*mapping).updatePoints(mRPatcher);
      }
  }

  return 0;
}


int ScriptInterpreter::adjustUnfolding() {
  float epsilonPT;

  if (mappings.size() == 0) {
    cerr << "Error (adjust-unfolding, instr. " <<  idInstruction << "): unfolding needs updating."<< endl;
    return -1;
  }

  const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  if (c == NULL) {
    cerr << "Error (adjust-unfolding, instr. " <<  idInstruction << "): method not defined (attribute \"method\")."<< endl;
    return 1;
  }
  int methodPT = atoi(c);

  c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"epsilon");
  if (c == NULL)
    epsilonPT = 0.0000001;
  else
    epsilonPT = atof(c);

  if (epsilonPT == 0) {
    cerr << "Error (adjust-unfolding, instr. " <<  idInstruction << "): epsilon value cannot be 0."<< endl;
    return 1;
  }

  c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"max-iteration");
  unsigned int maxIterPT;
  if (c == NULL) {
    maxIterPT = 10000;
  }
  else
    maxIterPT = atoi(c);

  int meshId = getMeshId();

  if (meshId == -2) {
    cerr << "Error (adjust-unfolding, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }


  if (methodPT != 0) {
    cout << indentManager << "Adjust unfolding..." << endl;
    int i = 0;
    for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++i)
      if ((meshId < 0) || (meshId == i)) {
	unsigned int nbIt = (*mapping).adjustMapping(methodPT, maxIterPT, epsilonPT, indentManager);
	cout << indentManager << " " << nbIt << " iteration(s)." << endl;
      }
  }
  else {
    cout << indentManager << "No adjust method defined." << endl;
  }

  jumpNextNode();
  return 0;

}


int ScriptInterpreter::unfoldMesh() {

  if (meshes.getNbPoints() == 0) {
    cerr << "Error (unfold, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  // values initialisation
  bool hasChildren = (xmlTextReaderIsEmptyElement(reader) == 0);
  const char * uMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  if (uMethod == NULL)
    cout << indentManager << "Warning (unfold, instr. " <<  idInstruction << "): unfold method not defined, using \"circle-packing\" (attribute \"method\")."<< endl;


  const char * iMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"initialization-method");

  const char * multiDepth = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"multiscale-depth");
  unsigned int mutliscaleDepth = 0;
  if (multiDepth != NULL)
    mutliscaleDepth = atoi(multiDepth);

  const char * multiMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"multiscale-method");
  bool readjustMScale = true;
  if (multiMethod != NULL) {
    if (string(multiMethod) == "readjust")
      readjustMScale = true;
    else if (string(multiMethod) == "interpolate")
      readjustMScale = false;
    else {
      cerr << indentManager << "Error (unfold, instr. " <<  idInstruction << "): multiscale method unknown (attribute \"multiscale-method\")."<< endl;
      return -1;
    }
  }

  const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"epsilon");
  double epsilon;
  if (c == NULL)
    epsilon = 0.00000001;
  else
    epsilon = atof(c);

  if (epsilon == 0) {
    cerr << "Error (unfold, instr. " <<  idInstruction << "): epsilon value cannot be 0."<< endl;
    return 1;
  }

  const char * log = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"log");

  int meshId = getMeshId();

  if (meshId == -2) {
    cerr << "Error (unfold, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }
  const char * variant = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"variant");

  c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"max-iteration");
  unsigned int maxIter;
  if (c == NULL) {
    if ((uMethod != NULL) && (string(uMethod) == "abf"))
      maxIter = 20;
    else
      maxIter = 10000;
  }
  else
    maxIter = atoi(c);

  // first check number of connected components
  if (meshId < 0)
    meshes.splitCC();
  else if (meshes[meshId].getNbCC() != 1) {
    cerr << "Error (instr. " << idInstruction << "): mesh #" << meshId << " has more than one connected component." << endl;
    return 1;
  }

  bool hasToBeCreated = false;
  if (meshId < 0)
    mappings.clear();
  else {
    hasToBeCreated = (mappings.size() == 0);
  }

  // then check topology
  int i = 0;
  for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
    if ((meshId < 0) || (meshId == i)) {
      MeshPart mPart(*m, (VertexID) 0);
      if ((mPart.getNbBoundaries() != 1) || (mPart.getBettiNumber() != 0)) {
	cout << indentManager << " Warning (unfold, instr. " << idInstruction << "): bad topology. Cut first." << endl;
	MeshCut meshCutter(*m);

	meshCutter.cutMeshUsingMinimalCut();
      }
    }

  if ((uMethod == NULL) || (string(uMethod) == "circle-packing") || (string(uMethod) == "cp") || (string(uMethod) == "cp-db")
      || (string(uMethod) == "circle-packing dynamic-borders")) {
    bool randomInit = (iMethod != NULL) && ((string(iMethod) == "random") || (StringManipulation::isFunction(iMethod, "random", 1)));

    if ((string(uMethod) == "cp-db") || (string(uMethod) == "circle-packing dynamic-borders"))
      cout << indentManager << "Unfolding: circle packing with dynamic borders";
    else
      cout << indentManager << "Unfolding: circle packing";
    if (randomInit) {
      cout << " (random initialization)";
    }
    cout << "..." << endl;

    std::deque<CPMethod> csetl;

    i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	bool fixed = !(((uMethod != NULL) && ((string(uMethod) == "circle-packing dynamic-borders") || (string(uMethod) == "co-db"))) || ((variant != NULL) && (string(variant) == "with-borders")));



	if ((meshId < 0) || hasToBeCreated)
	  mappings.push_back(*m);
	else
	  mappings[i] = *m;

	csetl.push_back(CPMethod(mappings[i], fixed));

	string realLogFile = "";
	if (log != NULL) {
	  realLogFile = FileTools::computeFileName(string(log), meshes.size(), i);
	}

	/* unfold using circle packing */
	if (randomInit) {
	  if (StringManipulation::isFunction(iMethod)) {
	    string param = StringManipulation::getParameters(iMethod).front();
	    if (!StringManipulation::isUnsignedFloat(param)) {
	      cerr << "Error (instr. " << idInstruction << "): random parameter for unfolding initialization mignt be a positive float value." << endl;
	      return 1;
	    }
	    double maxRandom = StringManipulation::getFloat(param);
	    double realMaxRandom = csetl.back().initDataRandom(maxRandom);
	    cout << indentManager << " Random initialization (effective max: " << realMaxRandom << "; choosen max: " << maxRandom << ")" << endl;
	  }
	  else
	    csetl.back().initDataRandom();
	}
	MultiscaleUnfolding mSUnfolding(readjustMScale, epsilon, maxIter, !randomInit, realLogFile, indentManager);
	int nbIter = mSUnfolding.processUnfolding(csetl.back(), mutliscaleDepth);

	if (mutliscaleDepth != 0)
	  cout << indentManager << " Global iteration number: " << nbIter << endl;

      }
      else if(hasToBeCreated)
	mappings.push_back(Mapping2D3D(*m));

    if (log != NULL)
      cout << indentManager << " Logging unfolding values (" << log << ")" << endl;

    if (hasChildren) {
      jumpNextNode();
      if(string("output") != (const char *) xmlTextReaderConstName(reader)) {
	cerr << indentManager << " Error (unfold, instr. " <<  idInstruction << "): unknown subinstructon." << endl;
	return 1;
      }
      const char * oMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
      const char * filename = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");
      if ((filename == NULL) || ((oMethod != NULL) && (string(oMethod) != "file"))) {
	cout << indentManager << " Warning (unfold, output, instr. " <<  idInstruction << "): output method is not \"file\", ignored." << endl;
      }
      else {
	cout << indentManager << " Saving circle set (" << filename << ")..." << std::endl;
	CPMethod::saveCST(filename, csetl);
      }

      jumpNextNode();

      if (!((string("unfold") == (const char *) xmlTextReaderConstName(reader)) && (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)))
	cout << indentManager << " Warning (unfold, instr. " << idInstruction << "): \"" << (const char *) xmlTextReaderConstName(reader) << "\" unknown instruction." << endl;

    }

    jumpNextNode();
  }
  else if (string(uMethod) == "abf") {
    cout << indentManager << "Unfolding: ABF..." << endl;

    i = 0;
    for(MeshList::iterator m = meshes.begin(); m != meshes.end(); ++m, ++i)
      if ((meshId < 0) || (meshId == i)) {
	if ((meshId < 0) || hasToBeCreated)
	  mappings.push_back(*m);
	else
	  mappings[i] = *m;


	ABFMethod abf(mappings[i]);

	/* unfold using ABF */
	string realLogFile = "";
	if (log != NULL) {
	  realLogFile = FileTools::computeFileName(string(log), meshes.size(), i);
	}

	MultiscaleUnfolding mSUnfolding(readjustMScale, epsilon, maxIter, realLogFile, indentManager);
	int nbIter = mSUnfolding.processUnfolding(abf, mutliscaleDepth);
	if (nbIter < 0) {
	  cerr << indentManager << "Abording." << std::endl;
	  return -1;
	}
	else
	  if (mutliscaleDepth != 0)
	    cout << indentManager << " Global iteration number: " << nbIter << endl;

      }
      else if(hasToBeCreated)
	mappings.push_back(Mapping2D3D(*m));

    if (log != NULL)
      cout << indentManager << "  Logging unfolding values (" << log << ")" << endl;

    if (hasChildren) {
      jumpNextNode();
      if(string("output") != (const char *) xmlTextReaderConstName(reader)) {
	cerr << indentManager << " Error (unfold, instr. " <<  idInstruction << "): unknown subinstructon." << endl;
	return 1;
      }

      cout << indentManager << " Warning (unfold, instr. " << idInstruction << "): skip subinstruction." << endl;
      jumpNextNode();

      if (!((string("unfold") == (const char *) xmlTextReaderConstName(reader)) && (xmlTextReaderNodeType(reader) == XML_READER_TYPE_END_ELEMENT)))
	cout << indentManager << " Warning (unfold, instr. " << idInstruction << "): \"" << (const char *) xmlTextReaderConstName(reader) << "\" unknown instruction." << endl;
    }

    jumpNextNode();
  }
  else {
    cout << indentManager << " Warning (unfold, instr. " << idInstruction << "): \"" << uMethod << "\" is unknown." << endl;
    if(hasChildren) {
      while(string("unfold") != (const char *) xmlTextReaderConstName(reader))
	jumpNextNode();
    }
    jumpNextNode();
  }


  return 0;
}




int ScriptInterpreter::output()  {
  if (meshes.getNbPoints() == 0) {
    cerr << "Error  (output, instr. " <<  idInstruction << "): empty mesh."<< endl;
    return -1;
  }

  const char * oMethod = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"method");
  const char * filename = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");


  int meshId = getMeshId();

  if (meshId == -2) {
    cerr << "Error (output, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }


  float zratio = 1;
  const char * zr = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"zratio");
  if (zr != NULL)
    zratio = atof(zr);

  if (((oMethod == NULL) && (filename != NULL)) || ((oMethod != NULL) && (string(oMethod) == "file"))) {
    if (filename == NULL) {
      cerr << "Error (output, instr. " <<  idInstruction << "): filename not found (attribute \"filename\")."<< endl;
      return 1;
    }

    string realFilename;
    if (FileTools::isAbsoluteName(filename))
      realFilename = filename;
    else
      realFilename = FileTools::realPath(FileTools::path(script) + SEPARATOR + filename);

    // if file format is a mesh format
    if (meshes.isSaveFormat(realFilename)) {
      cout << indentManager << "Saving mesh (" << realFilename << ")..." << endl;
      if (meshId < 0)
	meshes.save(realFilename);
      else
	meshes[meshId].save(realFilename);
    }
    else if (mappings.isSaveFormat(filename)) {
      cout << indentManager << "Saving mapping (" << realFilename<< ")..." << endl;

      if (mappings.size() == 0) {
	cerr << "Error: mapping is out of date." << endl;
	return 1;
      }
      if (meshId < 0)
	mappings.save(realFilename);
      else
	mappings[meshId].save(realFilename);
    }
    else if (mappings.isSaveFormat(filename)) {
      cout << indentManager << "Exporting mapping (" << realFilename<< ")..." << endl;

      if (mappings.size() == 0) {
	cerr << "Error: mapping is out of date." << endl;
	return 1;
      }
      if (meshId < 0)
	mappings.save(realFilename);
      else
	mappings[meshId].save(realFilename);
    }
    else if ((FileTools::extension(filename) == "jpg") || (FileTools::extension(filename) == "png"))  { // else: it is an image
      unsigned int sizex, sizey;
      const char * cSizex = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizex");
      if (cSizex == NULL) sizex = 512; else sizex = atoi(cSizex);
      const char * cSizey = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizey");
      if (cSizey == NULL) sizey = 512; else sizey = atoi(cSizey);

      bool random = false;
      bool border = false;
      bool meshesb = false;
      bool texture = false;
      bool dElectrodes = false;

      if (mappings.size() == 0) {
	cout << " Saving file:" << endl;
	cerr << "Error: mapping is out of date." << endl;
	return 1;
      }

      deque<string> options = readOptions();
      for(deque<string>::iterator option = options.begin(); option != options.end(); ++option)
	if (*option == "random") random = true;
	else if (*option == "border") border = true;
	else if ((*option == "meshes") || (*option == "triangles")) meshesb = true;
	else if (*option == "texture") texture = true;
	else if (*option == "distance-electrodes") dElectrodes = true;
	else cout << indentManager << "Saving file (instr. " << idInstruction << "): option \"" << *option << "\" unknown." << endl;
      if (!(random && border && meshesb && texture))
	meshesb = true;

      int i = 0;
      for(Mapping2D3DList::iterator mapping = mappings.begin(); mapping != mappings.end(); ++mapping, ++i)
	if ((meshId < 0) || (meshId == i)) {
	  CImg<unsigned char> result(sizex, sizey);
	  result.fill(std::numeric_limits<unsigned char>::max());

	  if (random)
	    (*mapping).toImageRandom(result);

	  if (meshesb)
	    (*mapping).toImageDrawBorderTriangles(result, (unsigned char)0);

	  if (border)
	    (*mapping).toImageDrawBorder(result, (unsigned char)0);

	  if (texture) {
	    const char * nameTexture = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"texture");
	    if (nameTexture == NULL) {
	      cerr << "Error (output, instr. " <<  idInstruction << "): texture filename not found (attribute \"texture\")."<< endl;
	      return 1;
	    }
	    string realNameTexture;
	    if (FileTools::isAbsoluteName(nameTexture))
	      realNameTexture = nameTexture;
	    else
	      realNameTexture = FileTools::realPath(FileTools::path(script) + SEPARATOR + nameTexture);

	    if ((FileTools::extension(realNameTexture) == "dcm") || (FileTools::extension(realNameTexture) == "fsf") || (FileTools::extension(realNameTexture) == "hdr")) {
	      cout << indentManager << " Loading texture (" << realNameTexture << ")..." << endl;
	      CImg<unsigned char> img(realNameTexture.c_str());
	      (*mapping).toImageDrawTexture(result, img, zratio);
	    }
	    else {
	      cerr << "Error (output, instr. " <<  idInstruction << "): unknown texture file format (attribute \"texture\"): \"dcm\" is needed."<< endl;
	      return 1;
	    }
	  }
	  else if (dElectrodes) {
	    const char * nameData = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"input-data");
	    if (nameData == NULL) {
	      cerr << "Error (output, instr. " <<  idInstruction << "): input-data filename not found (attribute \"input-data\")."<< endl;
	      return 1;
	    }
	    string realNameData;
	    if (FileTools::isAbsoluteName(nameData))
	      realNameData = nameData;
	    else
	      realNameData = FileTools::realPath(FileTools::path(script) + SEPARATOR + nameData);

	    if (FileTools::extension(realNameData) == "txt") {
	      cout << indentManager << " Loading electrodes' data (" << realNameData << ")..." << endl;
	      ElectrodeList eLocation(realNameData.c_str(), zratio);

	      unsigned int j = 0;
	      for(ElectrodeList::const_iterator e = eLocation.begin(); e != eLocation.end(); ++e) {
		(*mapping).toImageDrawDistanceMap(result, *e);

		string filenameComplete = FileTools::computeFileName(FileTools::computeFileName(realFilename, mappings.size(), i), eLocation.size(), j);
		cout << indentManager << "Saving image (" << filenameComplete << ")..." << endl;
		result.save(filenameComplete.c_str());
		++j;
	      }
	    }
	    else {
	      cerr << "Error (output, instr. " <<  idInstruction << "): unknown texture file format (attribute \"texture\"): \"dcm\" is needed."<< endl;
	      return 1;
	    }

	  }

	  if (!dElectrodes) { // if images has not been saved (electrodes case)
	    string filenameComplete = FileTools::computeFileName(realFilename, mappings.size(), i);
	    cout << indentManager << "Saving image (" << filenameComplete << ")..." << endl;
	    result.save(filenameComplete.c_str());
	  }
	}

    }
    else // else: unknown format
      cout << indentManager << " Warning (output, file, instr. " << idInstruction << "): unknown format." << endl;

  }
  else if ((oMethod == NULL) || (string(oMethod) == "display")) {
    unsigned int sizex, sizey;
    const char * cSizex = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizex");
    if (cSizex == NULL) sizex = 512; else sizex = atoi(cSizex);
    const char * cSizey = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizey");
    if (cSizey == NULL) sizey = 512; else sizey = atoi(cSizey);
    deque<string> options = readOptions();
    const char * name = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"name");
    string realName;
    if (name != NULL) realName = name; else realName = "Display";
    bool texture = false;
    bool meshesb = false;
    bool border = false;
    bool map2D = false;
    bool dynamic_med = false;
    bool ratio = false;
    bool opengl = false;
    bool dynamic_distance = false;
    bool dynamic = false;
    for(deque<string>::iterator option = options.begin(); option != options.end(); ++option)
      if (*option == "texture") texture = true;
      else if (*option == "border") border = true;
      else if ((*option == "meshes") || (*option == "triangles")) meshesb = true;
      else if ((*option == "map2D") || (*option == "2D") || (*option == "dim2D")) map2D = true;
      else if (*option == "ratio") ratio = true;
      else if (*option == "dynamic") dynamic = true;
      else if (*option == "dynamic-med") dynamic_med = true;
      else if (*option == "dynamic-distance") dynamic_distance = true;
      else if (*option == "opengl") opengl = true;
      else cout << indentManager << " Warning (output, display ,instr. " << idInstruction << "): option \"" << *option << "\" unknown." << endl;


    // if dynamic_med, loading filename
    string realFilename;
    if (dynamic_med) {
      const char * filename = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");
      if (filename == NULL) {
	cerr << "Error (output, instr. " <<  idInstruction << "): filename not found (attribute \"filename\")."<< endl;
	return 1;
      }

      if (FileTools::isAbsoluteName(filename))
	realFilename = filename;
      else
	realFilename = FileTools::realPath(FileTools::path(script) + SEPARATOR + filename);
    }

    const char * nameTexture = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"texture");
    if (texture || (nameTexture != NULL)) { // dynamic med or not
      if (mappings.size() == 0) {
	cerr << "Error: mapping is out of date." << endl;
	return 1;
      }


      if (nameTexture == NULL) {
	cerr << "Error (output, instr. " <<  idInstruction << "): texture filename not found (attribute \"texture\")."<< endl;
	return 1;
      }
      string realNameTexture;
      if (FileTools::isAbsoluteName(nameTexture))
	realNameTexture = nameTexture;
      else
	realNameTexture = FileTools::realPath(FileTools::path(script) + SEPARATOR + nameTexture);

      if ((mappings.size() != 1) && (meshId < 0)) {
	cerr << " Warning: only first mapping will be used (" << mappings.size() << " available)." << endl;
	meshId = 0;
      }
      else
	if (mappings.size() == 1)
	  meshId = 0;

      if ((FileTools::extension(realNameTexture) == "dcm") || (FileTools::extension(realNameTexture) == "fsf") || (FileTools::extension(realNameTexture) == "hdr")) {
	cout << indentManager << "Display mapping." << endl;
	cout << indentManager << " Loading texture (" << realNameTexture << ")..." << endl;
	CImg<unsigned char> imgTexture(realNameTexture.c_str());

	if (dynamic_med) {
	  cout << indentManager << " Loading image (" << realFilename << ")..." << endl;
	  CImg<unsigned char> img(realFilename.c_str());
	  DDisplay dd(realName, mappings[meshId], imgTexture, img, zratio);
	  dd.display();

	}
	else {
	  cout << indentManager << " Warning: dynamic display not available with textures." << endl;
	  Display3D::displayMapping(sizex, sizey, realName, mappings.front(), imgTexture, !map2D, opengl, zratio);
	}
      }
      else {
	cerr << "Error (output, instr. " <<  idInstruction << "): unknown texture file format (attribute \"texture\"): \"dcm\" is needed."<< endl;
	return 1;
      }
    }
    else if (dynamic_med) {
      const char * nameData = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"input-data");
      if (nameData == NULL) {
	cerr << "Error (output, instr. " <<  idInstruction << "): input-data or texture filename not found (attribute \"input-data\" or \"texture\")."<< endl;
	return 1;
      }
      string realNameData;
      if (FileTools::isAbsoluteName(nameData))
	realNameData = nameData;
      else
	realNameData = FileTools::realPath(FileTools::path(script) + SEPARATOR + nameData);

      if ((mappings.size() != 1) && (meshId < 0)) {
	cerr << " Warning: only first mapping will be used (" << mappings.size() << " available)." << endl;
	meshId = 0;
      }
      else if (mappings.size() == 1)
	meshId = 0;

      cout << indentManager << "Display using medical interface." << endl;
      cout << indentManager << " Loading image (" << realFilename << ")..." << endl;
      CImg<unsigned char> img(realFilename.c_str());

      cout << indentManager << " Loading electrodes' data (" << realNameData << ")..." << endl;
      ElectrodeList eLocation(realNameData.c_str(), zratio);

      DDisplay dd(realName, mappings[meshId], eLocation, img, zratio);

      dd.display();
    }
    else if (dynamic_distance) {
      if (mappings.size() == 0) {
	cerr << "Error: mapping is out of date." << endl;
	return 1;
      }
      if (mappings.size() != 1)
	cerr << " Warning: only first mapping will be used (" << mappings.size() << " available)." << endl;

      Display3D::displayMappingDynamicDistance(sizex, sizey, realName, mappings.front());
    }
    else
      if (map2D || dynamic) {
	if (mappings.size() == 0) {
	  cerr << "Error: mapping is out of date." << endl;
	  return 1;
	}
	if (dynamic)
	  if (meshId < 0)
	    Display3D::displayMappingListDynamicMap(sizex, sizey, realName, mappings);
	  else
	    Display3D::displayMappingDynamicMap(sizex, sizey, realName, mappings[meshId]);
	else
	  if (meshId < 0)
	    Display3D::displayMappingList(sizex, sizey, realName, mappings, false, meshesb, border, ratio, opengl);
	  else
	    Display3D::displayMapping(sizex, sizey, realName, mappings[meshId], false, meshesb, border, ratio, opengl);
      }
      else
	if (meshId < 0)
	  Display3D::displayMeshList(sizex, sizey, realName, meshes, meshesb, border, opengl);
	else
	  Display3D::displayMesh(sizex, sizey, realName, meshes[meshId], meshesb, border, opengl);

  }
  else if (string(oMethod) == "infos") {
    cout << indentManager << "Display infos: " << endl;
    bool points = false, triangles = false, boundaries = false, topology = false, size = false, size2D = false;
    deque<string> options = readOptions();
    for(deque<string>::iterator option = options.begin(); option != options.end(); ++option)
      if (*option == "points") points = true;
      else if (*option == "triangles") triangles = true;
      else if (*option == "boundaries") boundaries = true;
      else if (*option == "size") size = true;
      else if (*option == "size2D") size2D = true;
      else if (*option == "topology") topology = true;
      else if (*option == "all") points = triangles = boundaries = topology = true;
      else cout << indentManager << " Warning (output, infos, instr. " << idInstruction << "): option \"" << *option << "\" unknown." << endl;
    if (!(points || triangles || boundaries || topology || size || size2D))
      points = triangles = boundaries = topology = size = size2D = true;

    cout << indentManager << " Number of meshes: " << meshes.size() << endl;
    int i = 0;
    for(MeshList::iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh, ++i)
      if ((meshId < 0) || (meshId == i)) {
	deque<MeshPart> cc = (*mesh).getConnectedComponents();
	cout << indentManager << "  Connected components number: " << cc.size() << endl;

	if (size2D && (mappings.size() != 0))
	  cout << indentManager << "   Mapping size: " << mappings[i].getBox2DSize() << endl;

	unsigned int i = 0;
	for(deque<MeshPart>::iterator it = cc.begin(); it != cc.end(); ++it) {
	  cout << indentManager << "  Component #" << i << ": " << endl;
	  if (points) {
	    cout << indentManager << "   Points number: " << (*it).getNbPoints();
	    unsigned int nbNRPoints = (*it).getNonRealPoints().size();
	    if (nbNRPoints != 0)
	      cout << " (" << nbNRPoints << " nonreal points)";
	    cout << endl;
	  }
	  if (triangles)
	    cout << indentManager << "   Triangles number: " << (*it).getNbTriangles() << endl;
	  if (size)
	    cout << indentManager << "   Mesh size: " << (*it).getBoxSize() << endl;
	  if (boundaries)
	    cout << indentManager << "   Boundaries number: " << (*it).getNbBoundaries() << endl;
	  if (topology) {
	    cout << indentManager << "   2nd Betti number: " << (*it).getBettiNumber() << endl;
	  }
	}
    }
  }
  else if (string(oMethod) == "comparison") {
    deque<string> options = readOptions();

    if (options.size() == 0) {
      cerr << "Error (output, instr. " <<  idInstruction << "): options for comparison not defined."<< endl;
      return -1;
    }

    if ((options.size() == 1) && StringManipulation::isFunction(options[0], 2) &&
	(MappingComparator::isKnownMethod(StringManipulation::getFunctionName(options[0])) ||
	 MeshComparator::isKnownMethod(StringManipulation::getFunctionName(options[0])))) {
      std::deque<std::string> oParameters = StringManipulation::getParameters(options[0]);
      std::string functionName = StringManipulation::getFunctionName(options[0]);

      if ((!StringManipulation::isUnsignedInt(oParameters[0])) || (!StringManipulation::isUnsignedInt(oParameters[1]))) {
	cerr << "Error (output, instr. " <<  idInstruction << "): parameters of the \"" << functionName << "\" option should be integers."<< endl;
	return -1;
      }
      unsigned int id0 = StringManipulation::getInt(oParameters[0]);
      unsigned int id1 = StringManipulation::getInt(oParameters[1]);

      if (MappingComparator::isKnownMethod(functionName)) {
	if ((id0 >= mappings.size()) || (id0 >= mappings.size())) {
	  cerr << "Error (output, instr. " <<  idInstruction << "): parameters of the \"" << functionName << "\" option should be smaller than the number of meshes."<< endl;
	  return -1;
	}

	MappingComparator mComparator(mappings[id0], mappings[id1]);
	cout << indentManager << "Computing \"" << functionName << "\" between mapping #" << id0 << " and #" << id1 << ": " << mComparator.useMethod(functionName) << ", " << mappings[id1].getBox2DSize() << std::endl;
      }
      else {
	MeshComparator mComparator(meshes[id0], meshes[id1]);
	cout << indentManager << "Computing \"" << functionName << "\" between mesh #" << id0 << " and #" << id1 << ": " << mComparator.useMethod(functionName) << ", " << meshes[id1].getBoxSize() << std::endl;
      }

    }
    else {
      cerr << "Error (output, instr. " <<  idInstruction << "): unknown options for mapping comparison."<< endl;
      return -1;
    }

  }
  else
    cout << indentManager << " Warning (output, instr. " << idInstruction << "): \"" << oMethod << "\" is unknown." << endl;
  jumpNextNode();

  return 0;
}

int ScriptInterpreter::registration() {

  int meshId = getMeshId();
  if (meshId == -2) {
    cerr << "Error (registration, instr. " <<  idInstruction << "): wrong \"mesh-id\" value (should be smaller than " << meshes.size() << "."<< endl;
    return -1;
  }
  else if (meshId == -1) {
    cerr << "Error (registration, instr. " <<  idInstruction << "): \"mesh-id\" should be defined."<< endl;
    return -1;
  }

  const char * ref = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"reference");
  if (ref == NULL) {
    cerr << "Error (registration, instr. " <<  idInstruction << "): \"reference\" should be defined."<< endl;
    return -1;
  }
  int refId = StringManipulation::getInt(string(ref));
  if ((refId < 0) || (refId >= (int)meshes.size())) {
    cerr << "Error (registration, instr. " <<  idInstruction << "): \"reference\" should be defined as a valid id value."<< endl;
    return -1;
  }

  if (mappings.size() != meshes.size()) {
    cout << "Warning (registration, instr. " <<  idInstruction << "): registration cannot be applied (mappings are out of date)."<< endl;
    return 0;
  }

  MappingRegistration mReg(mappings[meshId], mappings[refId]);
  mReg.apply(indentManager);

  jumpNextNode();

  return 0;

}


int ScriptInterpreter::execWithCache() {
  const char * cache = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"cache");

  if (cache == NULL) {
    cerr << " Error (exec, instr. " << idInstruction << "): \"cache\" attribute not defined (filename of the cache)." << endl;
    return 1;
  }

  // build real filename
  string filename;
  if (FileTools::isAbsoluteName(cache))
    filename = cache;
  else
    filename = FileTools::realPath(FileTools::path(script) + SEPARATOR + cache);

  // goto next instruction
  jumpNextNode();

  std::ifstream fileS(filename.c_str(), std::ios::in | std::ios::binary);
  bool exists = fileS.is_open();
  if (exists) {
    // load cache
    if (meshes.isLoadFormat(filename)) { // mesh file
      cout << indentManager << "Loading cache (" << filename << ")..." << endl;
      try {
	meshes.load(filename);
      }
      catch (Exception e) {
	cerr << e << endl;
	return -1;
      }

      // then clear mapping
      mappings.clear();
    }
    else if (mappings.isLoadFormat(filename)) {
      cout << indentManager << "Loading from file (" << filename << ")..." << endl;
      mappings.clear();
      try {
	// load mapping and update mesh
	mappings.load(filename.c_str());
      } catch (Exception e) {
	cerr << "Error (input, instr. " << idInstruction << "): " << e << "." << endl;
	return -1;
      }

    }
    else {
      cerr << " Error (exec, instr. " << idInstruction << "): \"" << filename << "\" format is unknown." << endl;
      return 1;
    }

    // then skip script
    skipBlock("exec");
    jumpNextNode();
  }
  else {
    cout << indentManager << "Building cache (" << filename << ")..." << endl;
    indentManager.incDepth();
    int v = mainReader("exec");
    if (v != 0)
      return v;
    indentManager.decrDepth();

    // saving file
    if (meshes.isSaveFormat(filename)) {
      cout << indentManager << "Saving cache (" << filename << ")..." << endl;
      meshes.save(filename);
    }
    else if (mappings.isSaveFormat(filename)) {
      cout << indentManager << "Saving cache (" << filename << ")..." << endl;
      if (mappings.size() == 0) {
	cerr << "Error: mapping is out of date." << endl;
	return 1;
      }
      mappings.save(filename);
    }
    else
      cout << indentManager << " Warning (exec, instr. " << idInstruction << "): \"" << filename << "\" format is unknown." << endl;

  }

  return 0;
}

int ScriptInterpreter::timeInstr() {
  clock_t start = clock();
  jumpNextNode();

  cout << indentManager << "Starting time instruction..." << endl;
  indentManager.incDepth();
  int v = mainReader("time");
  if (v != 0)
    return v;
  indentManager.decrDepth();

  clock_t stop = clock();

  cout << indentManager << "End of time instruction: " << ((double)(stop - start)) / (CLOCKS_PER_SEC) << "s" << endl;

  return 0;
}


int ScriptInterpreter::inputMesh() {
  CImg<> points;
  CImgList<VertexID> primitives;
  float zratio = 1;

  const char * fn_input = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"filename");
  const char * format_input = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"format");
  const char * object_input = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"objectname");
  const char * px = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointx");
  const char * py = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointy");
  const char * pz = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"pointz");
  const char * sx = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizex");
  const char * sy = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizey");
  const char * sz = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"sizez");
  const char * zr = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"zratio");

  if (zr != NULL)
    zratio = atof(zr);
  if (zratio == 0) {
    cerr << "Error (input, instr. " << idInstruction << "): zratio != 0." << endl;
    return 1;
  }

  if (fn_input == NULL) {
    cerr << "Error (input, instr. " << idInstruction << "): bad input file." << endl;
    return 1;
  }

  // build real filename
  string filename;
  if (FileTools::isAbsoluteName(fn_input))
    filename = fn_input;
  else
    filename = FileTools::realPath(FileTools::path(script) + SEPARATOR + fn_input);

  // build format definition
  string formatToCheck;
  if (format_input != NULL)
    formatToCheck = format_input;
  else
    formatToCheck = FileTools::extension(filename);

  // then load data
  if (meshes.isLoadFormat(formatToCheck)) { // mesh file
    cout << indentManager << "Loading mesh (" << filename << ")..." << endl;
    try {
      if (object_input != NULL) {
	meshes.clear();
	meshes.push_front(Mesh());
	meshes.front().loadAltFormat(filename, formatToCheck, (object_input == NULL)? "" : object_input);
      }
      else
	meshes.loadAltFormat(filename, formatToCheck);
    }
    catch (Exception e) {
      cerr << e << endl;
      return -1;
    }


    // crop mesh if needed
    if ((px != NULL) && (py != NULL) && (pz != NULL) &&
      (sx != NULL) && (sy != NULL) && (sz != NULL)) {
      double sizex2 = atof(sx) / 2;
      double sizey2 = atof(sy) / 2;
      double sizez2 = atof(sz) / 2;
      Point3D point1(atof(px) - sizex2, atof(py) - sizey2, atof(pz) - sizez2);
      Point3D point2(atof(px) + sizex2, atof(py) + sizey2, atof(pz) + sizez2);
      for(MeshList::iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
	MeshPart mPart(*mesh, point1, point2);
	mPart.cropMesh();
      }
    }

    // then clear mapping
    mappings.clear();
  }
  else if (mappings.isLoadFormat(formatToCheck)) {
    cout << indentManager << "Loading from file (" << filename << ")..." << endl;
    mappings.clear();
    try {
      // load mapping and update mesh
      mappings.load(filename.c_str());
    } catch (Exception e) {
      cerr << "Error (input, instr. " << idInstruction << "): " << e << "." << endl;
      return -1;
    }

  }
  else if ((FileTools::extension(formatToCheck) == "dcm") || (FileTools::extension(formatToCheck) == "fsf")) { // dicom file
    float cubesize = 2;
    const char * c = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"cubesize");
    if (c != NULL)
      cubesize = atof(c);

    cout << indentManager << "Loading image (" << filename << ")..." << endl;

    CImg<> img(filename.c_str());

    deque<string> morphomath = readOptions("morphomath");
    for (deque<string>::iterator i = morphomath.begin(); i != morphomath.end(); ++i) {
      unsigned int size;
      if (((*i).find("erode(") == 0) && ((*i)[(*i).size() - 1] == ')')) {
	*i = (*i).substr(6, (*i).size() - 7);
	size = atoi((*i).c_str());
	cout << indentManager << " Morphomath: erode (" << size << ")..." << endl;
	img.erode(size);
      }
      else if (((*i).find("dilate(") == 0) && ((*i)[(*i).size() - 1] == ')')) {
	*i = (*i).substr(7, (*i).size() - 8);
	size = atoi((*i).c_str());
	cout << indentManager << " Morphomath: dilage (" << size << ")..." << endl;
	img.dilate(size);
      }
      else if (((*i).find("open(") == 0) && ((*i)[(*i).size() - 1] == ')')) {
	*i = (*i).substr(5, (*i).size() - 6);
	size = atoi((*i).c_str());
	cout << indentManager << " Morphomath: open (" << size << ")..." << endl;
	img.erode(size).dilate(size);
      }
      else if (((*i).find("close(") == 0) && ((*i)[(*i).size() - 1] == ')')) {
	*i = (*i).substr(6, (*i).size() - 7);
	size = atoi((*i).c_str());
	cout << indentManager << " Morphomath: close (" << size << ")..." << endl;
	img.dilate(size).erode(size);
      }
      else
	cout << indentManager << " Warning (input, morphomath, instr. " << idInstruction << "): \"" << *i << "\" is unknown." << endl;
    }

    if ((px != NULL) && (py != NULL) && (pz != NULL) &&
	(sx != NULL) && (sy != NULL) && (sz != NULL)) {
       const CImg<>::_functor3d_float func(img);
       double sizex2 = atof(sx) / 2;
       double sizey2 = atof(sy) / 2;
       double sizez2 = atof(sz) / 2;
       points = CImg<>::isosurface3d(primitives, func, 0.45, atof(px) - sizex2, atof(py) - sizey2, (atof(pz) - sizez2) / zratio,
				     atof(px) + sizex2, atof(py) + sizey2, (atof(pz) + sizez2) / zratio,
				     cubesize, cubesize, cubesize);
       cimg_forX(points, i)
	 points(i, 2) *= zratio;
    }
    else {
      if ((px != NULL) || (py != NULL) || (pz != NULL) ||
	  (sx != NULL) || (sy != NULL) || (sz != NULL)) {
	cerr << "Error (input, instr. " << idInstruction << "): all parameters have to be defined to load only part of file (\"pointx\", \"pointy\", \"pointz\", \"sizex\", \"sizey\", \"sizez\")." << endl;
      }
      points = img.get_isosurface3d(primitives, 0.45, img._width / cubesize, img._height / cubesize, img._depth / cubesize);
       cimg_forX(points, i)
	 points(i, 2) *= zratio;
    }

    cout << indentManager << "Building mesh..." << endl;
    Mesh::triangulateCImgMesh(primitives);
    meshes.push_front(Mesh());
    meshes.front().buildMesh(points, primitives);
    mappings.clear();
  }
  else {
    cerr << " Warning (input, instr. " << idInstruction << "): unknown format." << endl;
    jumpNextNode();
    return 0;
  }

  jumpNextNode();
  return 0;
}


bool ScriptInterpreter::checkTest(const string & test) {
  MeshBExpr mBExpr(test, meshes, script);

  try {
    return mBExpr.eval();
  }
  catch (Exception e) {
    cerr << "Error (switch, instr. " << idInstruction << "): " << e;
    throw Exception(std::string("Bad boolean expression"));
  }
}

void ScriptInterpreter::skipBlock(const string & nodeName) {
  while((nodeName != (const char *) xmlTextReaderConstName(reader)) ||
	(xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT)) {
    if (nodeName == (const char *) xmlTextReaderConstName(reader)) {
      jumpNextNode();
      skipBlock(nodeName);
    }
    jumpNextNode();
  }

}


int ScriptInterpreter::convertInstruction() {
  cout << indentManager << "Convert unfolded mesh to default mesh." << endl;
  if (mappings.size() == 0)
    cerr << " Warning: unfolded mesh is out of date." << endl;

  meshes = mappings;
  mappings.clear();

  jumpNextNode();
  return 0;
}


int ScriptInterpreter::comment() {
  jumpNextNode();
  skipBlock("comment");
  jumpNextNode();
  return 0;
}

int ScriptInterpreter::switchInstruction() {
  jumpNextNode();

  // find instruction to run
  while(string("switch") != (const char *) xmlTextReaderConstName(reader)) {
    const char * test = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"test");
    if (string("case") == (const char *) xmlTextReaderConstName(reader)) {
      if (test == NULL) {
	cerr << "Error (switch, instr. " << idInstruction << "): Error in case, attribute \"test\" not defined." << endl;
	return -1;
      }

      // jump to instructions inside otherwise block
      jumpNextNode();

      // using main reader loop
      if (checkTest(test)) {
	cout << indentManager << "Switch instruction: case \"" << string(test) << "\"." << endl;
	indentManager.incDepth();
	int v = mainReader("case");
	indentManager.decrDepth();
	if (v != 0)
	  return v;

	// only one block is executed
	break;
      }
      else
	skipBlock("case");

      // jump to next instruction
      jumpNextNode();

    }
    else if (string("otherwise") == (const char *) xmlTextReaderConstName(reader)) {
      cout << indentManager << "Switch instruction: otherwise." << endl;

      // jump to instructions inside otherwise block
      jumpNextNode();

      // using main reader loop
      indentManager.incDepth();
      int v = mainReader("otherwise");
      indentManager.decrDepth();

      if (v != 0)
	return v;

      // only one block is executed
      break;
    }
    else {
      cout << indentManager << "Unknown instruction #"<< idInstruction << " in switch structure: " <<
	(const char *) xmlTextReaderConstName(reader) << endl;
      throw Exception(std::string("Error during reading xml file."));
    }
  }

  // then skip others
  skipBlock("switch");

  jumpNextNode();
  return 0;
}


int ScriptInterpreter::mainReader(const std::string & instrName) {

  while((instrName != (const char *) xmlTextReaderConstName(reader)) || (xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT)) {
    int v = 0;
    if (string("input") == (const char *) xmlTextReaderConstName(reader))
      v = inputMesh();
    else if (string("time") == (const char *) xmlTextReaderConstName(reader))
      v = timeInstr();
    else if (string("exec") == (const char *) xmlTextReaderConstName(reader))
      v = execWithCache();
    else if (string("select") == (const char *) xmlTextReaderConstName(reader))
      v = selectMesh();
    else if (string("cut") == (const char *) xmlTextReaderConstName(reader))
      v = cutMesh();
    else if (string("prune") == (const char *) xmlTextReaderConstName(reader))
      v = pruneMesh();
    else if (string("unfold") == (const char *) xmlTextReaderConstName(reader))
      v = unfoldMesh();
    else if (string("adjust-unfolding") == (const char *) xmlTextReaderConstName(reader))
      v = adjustUnfolding();
    else if (string("adjust-cutting") == (const char *) xmlTextReaderConstName(reader))
      v = adjustCutting();
    else if (string("output") == (const char *) xmlTextReaderConstName(reader))
      v = output();
    else if (string("reorient") == (const char *) xmlTextReaderConstName(reader))
      v = reorientMesh();
    else if (string("crop") == (const char *) xmlTextReaderConstName(reader))
      v = cropMesh();
    else if (string("print") == (const char *) xmlTextReaderConstName(reader))
      v = printMessage();
    else if (string("clean") == (const char *) xmlTextReaderConstName(reader))
      v = cleanMesh();
    else if (string("patch") == (const char *) xmlTextReaderConstName(reader))
      v = patchMesh();
    else if (string("point-manipulation") == (const char *) xmlTextReaderConstName(reader))
      v = pointManipulationMesh();
    else if (string("add-point") == (const char *) xmlTextReaderConstName(reader))
      v = addPointMesh();
    else if (string("flip-triangle") == (const char *) xmlTextReaderConstName(reader))
      v = flipTriangleMesh();
    else if (string("break") == (const char *) xmlTextReaderConstName(reader))
      v = breakMsg();
    else if (string("uncut") == (const char *) xmlTextReaderConstName(reader))
      v = uncutMesh();
    else if (string("switch") == (const char *) xmlTextReaderConstName(reader))
      v = switchInstruction();
    else if (string("convert") == (const char *) xmlTextReaderConstName(reader))
      v = convertInstruction();
    else if (string("wait") == (const char *) xmlTextReaderConstName(reader))
      v = waitInstruction();
    else if (string("sleep") == (const char *) xmlTextReaderConstName(reader))
      v = sleepInstruction();
    else if (string("duplicate") == (const char *) xmlTextReaderConstName(reader))
      v = duplicateMesh();
    else if (string("comment") == (const char *) xmlTextReaderConstName(reader))
      v = comment();
    else if (string("registration") == (const char *) xmlTextReaderConstName(reader))
      v = registration();
    else {
      cout << indentManager << "Unknown instruction #"<< idInstruction << ": " << (const char *) xmlTextReaderConstName(reader) << endl;
      throw Exception(std::string("Error during reading xml file."));
    }
    if (v != 0)
      return v;
    ++idInstruction;
  }

  try {
    jumpNextNode();
  }
  catch(Exception e) {
    cout << indentManager << "End of script." << endl;
  }

  return 0;
}

#endif


std::string ScriptInterpreter::helpScript() {

  Message msg;
  msg << "A large part of the following instructions have an \"mesh-id\" parameter to only apply the method on the given mesh.";
  msg << "";
  msg << "[[ input ]]";
  msg << "Available input formats:";
  msg << "- mesh file;";
  msg << "- dicom file.";
  msg << "- fsf file.";
  msg << " ";
  msg << "Available crop method during input:";
  msg << "- Crop according to a box (attributes: \"pointx\", \"pointy\", \"pointz\", \"sizex\", \"sizey\", \"sizez\").";
  msg << "- Morphomath tool (atribute \"morphomath\"). Available methods: dilate, erode, open, close. Methods are used given radius in parameter, separated by comma. Example: \"open(2), close(3)\".";
  msg << "- \"zratio\" attribute define voxel ratio on dicom and fsf data.";
  msg << " ";
  msg << "[[ cut ]]";
  msg << "Available methods: ";
  msg << "- \"simple\" (default value): cut mesh to obtain a mesh homeomorphic to a disc.";
  msg << "- \"curvature\": cut mesh using curvature.";
  msg << "- \"rectangular\" (or \"quadrangulation\"): cut mesh to have rectangular patches.";
  msg << " ";
  cout << msg;
  msg.clear();
  cout << LengthFactory();
  cout << " " << endl;
  msg << "Available cut attributes: ";
  msg << "- \"optim-sc\": optimization of base point order. Available values: 0 (none), 1 (naive cycle length estimation), 2 (cycle length estimation using strips).";
  msg << "- \"max-cut\": cut method can be stop before a defined number of cut.";
  msg << "- \"merge-multipoints\": method used to merge multipoints (available values: \"none\", \"simple\", \"one-point\").";
  msg << "- \"min-curvature\": minimum value for curvature.";
  msg << "- \"ratio-length\": ratio parameter for length method.";
  msg << "- \"log\": filename used to log pathes.";
  msg << "- \"filename\": filename used to load pathes.";
  msg << " ";
  msg << "Available output methods during cut: ";
  msg << "- \"display\": display cut using colors for pathes (available options: \"triangles\" (alias: \"meshes\"), \"pathes\").";
  msg << "- \"infos\": write in default output some informations about cut pathes.";
  msg << " ";
  msg << "[[ patch ]]";
  cout << msg;
  msg.clear();
  cout << LengthFactory();
  cout << " " << endl;
  msg << "Available options: ";
  msg << "- \"max-length\": only pathes shorter than maxLength value are patched.";
  msg << "- \"optim-sc\": optimization of base point order. Available values: 0 (none), 1 (naive cycle length estimation), 2 ().";
  msg << "- \"max-patch\": patch method can be stop before a defined number of patches.";
  msg << "- \"ratio-length\": ratio parameter for length method.";
  msg << "- \"log\": filename for logging pathes.";
  msg << "- \"filename\": filename used to load pathes.";
  msg << " ";
  msg << "Note: the center of patches are non real points.";
  msg << " ";
  msg << "[[ adjust-cutting ]]";
  msg << "Adjust cutting using unfolding";
  msg << "Available attributes:";
  msg << "- \"method\": method used to cut the surface using the unfolding information. Available values: \"cluster\", \"rectangular\".";
  msg << "- \"cluster-method\": specific submethod used by the cluster method. Available values: \"simple\", \"split\".";
  msg << "- \"ratio-limit\": when the method is \"cluster\", ratio used to remove clusters with means smaller than this value. When the method is \"rectangular\", this ratio is the ratio of area between polygon and its hull.";
  msg << "An output instruction can be used in the \"adjust-cutting\" instruction.";
  msg << " ";
  msg << "[[ crop ]]";
  msg << "Available crop methods: ";
  msg << "- \"box\": crop using a 3d box defined by \"pointx\", \"pointy\", \"pointz\", \"sizex\", \"sizey\", \"sizez\".";
  msg << "- \"ball\": crop using a ball defined by \"pointx\", \"pointy\", \"pointz\", \"radius\" (euclidean distance).";
  msg << "- \"disc\": crop using a disc on surface defined by \"pointx\", \"pointy\", \"pointz\", \"radius\" (geodesic distance). Resulting mesh is homeomorphic to a disc.";
  msg << "- \"ball2d\": crop using a surfacic ball defined by \"pointx\", \"pointy\", \"pointz\", \"radius\" (geodesic distance).";
  msg << " ";
  msg << "[[ select ]]";
  msg << "Available methods:";
  msg << "- \"id\": select connected component using id.";
  msg << "- \"location\": select nearest connected component to a given point (attributes \"pointx\", \"pointy\" and \"pointz\").";
  msg << "- \"size\": select connected component using number of points.";
  msg << " ";
  msg << "[[ prune ]]";
  msg << "Available methods:";
  msg << "- \"simple-curvature\": remove points using simple curvature method (options: \"depth\": depth of the prune).";
  msg << " ";
  msg << "[[ unfold ]]";
  msg << "Available unfold methods: ";
  msg << "- \"circle-packing\": unfold mesh using circle packing method. Alias: \"cp\".";
  msg << "- \"circle-packing dynamic-borders\": unfold mesh using circle packing with dynamic borders method. Alias: \"cp-db\".";
  msg << "- \"abf\": unfold mesh using ABF method.";
  msg << "Other options: ";
  msg << "- \"variant\": select a variant of unfolding method (available variants for \"circle-packing\": \"with-borders\", same as \"circle-packing dynamic-borders\")";
  msg << "- \"epsilon\": allowed error value. default: 0.00000001.";
  msg << "- \"max-iteration\": maximum iteration allowed. Default: 20 for ABF, and 10000 for Circle Packing (\"max-iteration\" = 0 means no limit).";
  msg << "- \"log\": filename used to log unfolding informations.";
  msg << "- \"multiscale-depth\": depth for multiscale process (default: 0).";
  msg << "- \"multiscale-method\": method for multiscale process (available: \"readjust\" and \"interpolate\").";
  msg << "- \"initialization-method\": method for data initialization (available: \"normal\" and \"random\").";
  msg << " " ;
  msg << "Available output methods during cut: ";
  msg << "- \"file\": when using circle packing algorithm, a \".cst\" can be generated.";
  msg << " ";
  msg << "Note: circle set can also be loaded from file using attribute \"filename\".";
  msg << " ";
  msg << "[[ adjust-unfolding ]]";
  cout << msg;
  msg.clear();
  cout << Mapping2D3D::helpAdjustMapping();
  msg << " ";
  msg << "[[ uncut ]]";
  msg << "Uncut meshes merging points at same location. Available methods (\"method\" attribute): \"simple\" (each mesh is uncut), \"merge\" (meshes are merged), \"complete\" (all meshes are merged then uncut).";
  msg << " ";
  msg << "[[ point-manipulation ]]";
  msg << "Available methods of point manipulation (\"method\" attribute): ";
  msg << "- \"remove-nonrealpoints\": removing non real points.";
  msg << "- \"setall-realpoints\": set all points as real points.";
  msg << "- \"modify-location\": modify location according to the \"method-options\".";
  cout << msg;
  msg.clear();
  cout << PointSelector::helpSelectionMethods();
  cout << " ";
  cout << PointModifier::helpModificationMethods();
  msg << "[[ output ]]";
  msg << "Available output methods:";
  msg << "- \"infos\": write in default output some informations about mesh. Available options: \"points\", \"triangles\", \"boundaries\", \"topology\", \"all\" (default: \"all\").";
  msg << "- \"display\": 3d mesh or unfolded mesh view. Available options: \"meshes\", \"border\", \"opengl\", \"ratio\", \"dynamic\", \"dynamic-distance\", \"dynamic-med\", \"map2D\" (same as \"dim2D\" and \"2D\").";
  msg << "- \"file\": save mesh in file (available formats: wrl, cst, ums).";
  msg << " ";
  msg << "[[ switch ]]";
  msg << "Each \"case\" instruction needs a \"test\" attribute with a boolean expression.";
  msg << "Available operators and functions in boolean expressions:";
  msg << "- Boolean operators (\"and\", \"or\", \"!\")";
  msg << "- Functions on files (\"fileExists()\")";
  msg << "- Functions on meshes (\"mesh.getNbCC()\", \"mesh.getNbCC()\", \"mesh.getNbPoints()\", \"mesh.getNbTriangles()\", \"mesh.getNbPointFlagValue()\", \"mesh.isEmpty()\")";
  msg << "- Comparison operators for integer values.";
  msg << "In a switch structure, only the first \"case\" instruction with \"test\" evaluated as true is executed. If none \"case\" is evaluated as true, \"otherwise\" instruction is executed.";
  msg << " ";
  msg << "[[ convert ]]";
  msg << "Convert unfolded mesh to current mesh.";
  msg << " ";
  msg << "[[ exec ]]";
  msg << "Exec with cache (filename given using the parameter \"cache\").";
  msg << " ";
  msg << "[[ flip-triangle ]]";
  msg << "Flip a triangle as in delaunay triangulation.";
  msg << "Available options:";
  msg << "- \"triangle1\" and \"triangle2\". The edge between the two given triangles will be inverted.";
  msg << "- \"point1\" and \"point2\". The edge between the two given points will be removed, and its dual will be added.";
  msg << " ";
  msg << "[[ add-point ]]";
  msg << "Add a point in the mesh.";
  msg << "Available options:";
  msg << "- \"point1\" and \"point2\". The point will be added on the edge between the two given points. Triangles are also added to preserve the surface coherence";
  msg << " ";
  msg << "[[ duplicate ]]";
  msg << "Duplicate a mesh adding the mesh selected by id at the end of the mesh list.";




  return msg.toString();
}

Mesh ScriptInterpreter::getMesh() const {
  return meshes.getMesh();
}
