/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SCENE_DESCRIPTOR
#define SCENE_DESCRIPTOR

#include <string>
#include <vector>
#include <assert.h>
#include <iostream>

#ifndef SWIG
#include "XMLSimpleNode.h"
#endif

namespace Taglut {

  /** a class that describe the global structures, i.e. cavities and inclusions */
  class SceneDescriptor {
  protected:
    bool origin;
    unsigned int genus;
    std::vector<SceneDescriptor *> children;

#ifdef LIBXML_READER_ENABLED
    void jumpNextNode(xmlTextReaderPtr & reader);

    void load(xmlTextReaderPtr & reader, const std::string & name);
#endif

    void writeSubScene(std::ofstream & outfile, const std::string & space) const;

  public:

    /** constructor (origin)  */
    SceneDescriptor() : origin(true), genus(0) { }

    /** constructor (origin)  */
    SceneDescriptor(const std::string & xmlDescription) : origin(true), genus(0) {
      load(xmlDescription);
      assert(isOrigin());
    }

    /** load from an xml description */
    void load(const std::string & xmlDescription);

    /** constructor */
    SceneDescriptor(unsigned int genus_) : origin(false), genus(genus_) { }

    /** constructor */
    SceneDescriptor(bool origin_, unsigned int genus_) : origin(origin_), genus(genus_) { }

    /** copy constructor */
    SceneDescriptor(const SceneDescriptor & sd) : origin(sd.origin), genus(sd.genus) {
      unsigned int i = 0;
      for(std::vector<SceneDescriptor *>::const_iterator c = sd.begin(); c != sd.end(); ++c, ++i) {
	assert(i < sd.children.size());
	assert(sd.getNbChildren() != 0);
	assert(!(**c).isOrigin());
	children.push_back(new SceneDescriptor(**c));
      }
    }

#ifndef SWIG
    SceneDescriptor & operator=(const SceneDescriptor & sd) {
      origin = sd.origin;
      genus = sd.genus;
      children.clear();
      for(std::vector<SceneDescriptor *>::const_iterator c = sd.begin(); c != sd.end(); ++c)
	children.push_back(new SceneDescriptor(**c));
      return *this;
    }
#endif

    /** iterator on children */
    inline std::vector<SceneDescriptor *>::iterator begin() { return children.begin(); }

    /** iterator on children */
    inline std::vector<SceneDescriptor *>::iterator end() { return children.end(); }

    /** iterator on children */
    inline std::vector<SceneDescriptor *>::const_iterator begin() const { return children.begin(); }

    /** iterator on children */
    inline std::vector<SceneDescriptor *>::const_iterator end() const { return children.end(); }

    /** destructor */
    virtual ~SceneDescriptor() {
      clear();
    }

    /** save the current object in the given file */
    const SceneDescriptor & save(const std::string & filename) const;

    /** add a child in the current structure */
    virtual SceneDescriptor & addSDChild(unsigned int genus_) {
      SceneDescriptor * newSD = new SceneDescriptor(genus_);
      children.push_back(newSD);
      return *newSD;
    }

    /** add a child in the current structure */
    virtual SceneDescriptor & addSDChild(const SceneDescriptor & l) {
      assert(!l.isOrigin());
      SceneDescriptor * newSD = new SceneDescriptor(l);
      children.push_back(newSD);
      return *newSD;
    }

    inline std::vector<SceneDescriptor *>::iterator removeChild(const std::vector<SceneDescriptor *>::iterator & it) {
      delete (*it);
      return children.erase(it);
    }

    /** accessor */
    unsigned int getNbChildren() const { return children.size(); }

    /** modify the current object adding a number to its genus */
    inline SceneDescriptor & addGenus(unsigned int genus_) {
      assert(!isOrigin());
      genus += genus_;
      return *this;
    }

    /** set the current object giving its genus */
    inline SceneDescriptor & setGenus(unsigned int genus_) {
      origin = false;
      genus = genus_;
      return *this;
    }

    /** accessor */
    inline bool isOrigin() const { return origin; }
    /** accessor */
    inline unsigned int getGenus() const { return genus; }

    /** reset the current structure assuming that it corresponds to the ambient space */
    inline SceneDescriptor & clear(bool freememory = true) {
      if (freememory)
	for(std::vector<SceneDescriptor *>::const_iterator c = begin(); c != end(); ++c)
	  delete (*c);
      children.clear();
      return *this;
    }

    /** return true if the current layer do not contains another one */
    inline bool isEmpty() const {
      return children.size() == 0;
    }

    /** return the number of contained layers */
    unsigned int size() const;

#ifndef SWIG
    /** return true if the current scene is equivalent to the given one */
    inline bool operator==(const SceneDescriptor & sd) const {
      return similar(sd);
    }
#endif

    /** return true if the current scene is equivalent to the given one */
    bool similar(const SceneDescriptor & sd) const;

    /** return true if the current scene is equivalent to the given one */
    inline bool similar(const std::string & xmlDesc) const {
      SceneDescriptor sd(xmlDesc);
      return similar(sd);
    }

    /** return the cumulated genus, by adding genus of all the structure */
    unsigned int getCumulatedGenus() const;

    /** return the higher genus for an object of the current structure */
    unsigned int getMaxGenus() const;

    /** return the maximal depth of the child hierarchy */
    unsigned int maxDepth() const;

    /** return true if the current scene descriptor is unreachable from the given
	state (scene description with or without merges). A false result does not
	corresponds to a reachable configuration (the current method is an estimation).
    */
    bool isUnreachableFrom(const SceneDescriptor & sd) const;

    /** return the number of elementary steps to obtain the given scene descriptor
	from the current one. */
    inline int getNbElementarySteps(const SceneDescriptor & sd) const {
      return (sd.getMaxGenus() - getMaxGenus()) + (size() - sd.size());
    }

    /** return the cumulated number by depth. result[i] corresponds to the number of
	objects at depth >= i (result[0] corresponds to size()) */
    std::vector<unsigned int> getCumulatedSize() const;
  };


}

#endif
