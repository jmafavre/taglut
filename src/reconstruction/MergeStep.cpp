/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>
#include <sstream>
#include <limits>

#include "Exception.h"
#include "MergeStep.h"
#include "MeshBySlices.h"

using namespace Taglut;


double MergeStepCostPolygonDistance::getValue(const MergeStep & ms) {
  const std::vector<unsigned int> & polygons = ms.getPolygons();
  double result = 0.;

  for(std::vector<unsigned int>::const_iterator p = polygons.begin();
      p != polygons.end(); ++p)
    for(std::vector<unsigned int>::const_iterator pp = p + 1;
	pp != polygons.end(); ++pp)
      result += (*mbs).hausdorffDistance(ms.getCellX(), ms.getCellY(), ms.getCellZ(),
					 *p, *pp);
  return result;
}

std::vector<MergeStep> MergeStep::getSubMergeSteps() const {
  std::vector<MergeStep> result;
  if (idPaths.size() < 2)
    throw Exception("getSubMergeSteps(): bad number of points");
  if (idPaths.size() == 2) {
    result.push_back(*this);
    return result;
  }

  for(unsigned int i = 0; i != ccList.size(); ++i) {
    std::vector<unsigned int> ccListNew(ccList);
    ccListNew.erase(ccListNew.begin() + i);
    std::vector<unsigned int> idPathsNew(idPaths);
    idPathsNew.erase(idPathsNew.begin() + i);
    result.push_back(MergeStep(cellx, celly, cellz, ccListNew, idPathsNew));
    std::vector<MergeStep> sub = result.back().getSubMergeSteps();
    result.insert(result.end(), sub.begin(), sub.end());
  }

  return result;
}

bool MergeStep::removeOne(unsigned int id) {
  std::vector<unsigned int>::iterator idp = idPaths.begin();
  for(std::vector<unsigned int>::iterator cc = ccList.begin();
      cc != ccList.end(); ++cc, ++idp)
    if ((*cc) == id) {
      ccList.erase(cc);
      idPaths.erase(idp);
      return true;
    }
  return false;
}

unsigned int MergeStep::removeAllButOne(unsigned int id) {
  unsigned int result = 0;
  bool found = false;

  std::vector<unsigned int>::iterator idp = idPaths.begin();
  for(std::vector<unsigned int>::iterator cc = ccList.begin();
      cc != ccList.end();)
    if ((*cc) == id) {
      if (!found) {
	++cc; ++idp;
	found = true;
      }
      else {
	cc = ccList.erase(cc);
	idp = idPaths.erase(idp);
	++result;
      }
    }
    else {
      ++cc; ++idp;
    }

  return result;
}

unsigned int MergeStep::removeAll(unsigned int id) {
  unsigned int result = 0;

  std::vector<unsigned int>::iterator idp = idPaths.begin();
  for(std::vector<unsigned int>::iterator cc = ccList.begin();
      cc != ccList.end();)
    if ((*cc) == id) {
      cc = ccList.erase(cc);
      idp = idPaths.erase(idp);
      ++result;
    }
    else {
      ++cc; ++idp;
    }

  return result;
}
