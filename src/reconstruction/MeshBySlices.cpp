/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>
#include <sstream>
#include <limits>

#include "MeshBySlices.h"
#include "Exception.h"

using namespace Taglut;

MeshBySlices::MeshBySlices(double epsilon, double epsilonAngle, double epsilonPlane) : error(epsilon),
										       errorAngles(epsilonAngle),
										       errorPlanes(epsilonPlane) { }

MeshBySlices::MeshBySlices(const std::string & filename,
			   double epsilon, double epsilonAngle, double epsilonPlane) : error(epsilon),
										       errorAngles(epsilonAngle),
										       errorPlanes(epsilonPlane)  {
  std::vector<Path3D> polygons = Path3D::loadPolygons(filename);
  addPaths(polygons);
}

MeshBySlices::MeshBySlices(const std::vector<Path3DPlanar> & polygons,
			   double epsilon, double epsilonAngle, double epsilonPlane) : error(epsilon),
										       errorAngles(epsilonAngle),
										       errorPlanes(epsilonPlane)  {
  addPaths(polygons);
}

MeshBySlices::MeshBySlices(const std::vector<Path3D> & polygons,
			   double epsilon, double epsilonAngle, double epsilonPlane) : error(epsilon),
										       errorAngles(epsilonAngle),
										       errorPlanes(epsilonPlane)  {
  addPaths(polygons);
}

MeshBySlices::MeshBySlices(const MeshBySlices & mbs) {
  error = mbs.error;
  errorAngles = mbs.errorAngles;
  errorPlanes = mbs.errorPlanes;
  for(unsigned char i = 0; i != 3; ++i) {
    slices[i] = mbs.slices[i];
    directions[i] = mbs.directions[i];
  }
}

void MeshBySlices::clearData() {
  connectedComponents.clear();
  intersliceCells.clear();
  nbEdges.clear();
  nbCircularEdges.clear();
  layers.clear();
  inverseDirectory[0].clear();
  inverseDirectory[1].clear();
  inverseDirectory[2].clear();
}

MeshBySlices & MeshBySlices::addPath(const Path3DPlanar & path) {
  clearData();

  Coord3D dirPath = path.getVector().getNormalize();
  unsigned char d = 0;
  // find the direction
  for(; d != 4; ++d) {
    // the 3 directions has been used
    if (d == 3)
      throw Exception("addPath(1): cannot find the corresponding direction. Abort.");
    // a new direction is found
    if (directions[d].norm() == 0.) {
      directions[d] = dirPath;
      break;
    }
    // it corresponds to an existing direction
    if ((dirPath.distance(directions[d]) < errorAngles) ||
	(dirPath.distance(-(directions[d])) < errorAngles))
      break;
  }
  assert(d < 3);

  // get the main direction
  const Coord3D origin;
  Line3D line(origin, directions[d]);
  const double intersection = line.getCoords(path.getIntersection(line));

  // find the good slice
  std::vector<MultiPath3DPlanar>::iterator sls = slices[d].begin();
  for(; sls != slices[d].end(); ++sls) {
    assert((*sls).nbPaths() != 0);
    assert(((*sls).getVector().getNormalize().distance(dirPath) < errorAngles) ||
	   ((-(*sls).getVector().getNormalize()).distance(dirPath) < errorAngles));
    const double i2 = line.getCoords((*sls).getIntersection(line));
    if (fabs(i2 - intersection) < error) {
      // the slice has been found
      (*sls).addPath(path);
      return *this;
    }
    else {
      assert(i2 != intersection);
      if (i2 > intersection)
	break;
    }
  }
  // we have to create a new slide
  MultiPath3DPlanar slice;
  slice.addPath(path);
  slices[d].insert(sls, slice);

  return *this;
}


std::string MeshBySlices::toString() const {
  std::ostringstream str;
  for(unsigned char i = 0; i != 3; ++i)
    if (directions[i].norm() != 0.) {
      str << "Direction #" << (unsigned int)i << ": " << directions[i] << std::endl;
      str << "  * Number of planes: " << slices[i].size() << std::endl;
      if (slices[i].size() != 0) {
	std::vector<MultiPath3DPlanar>::const_iterator preds = slices[i].begin();
	str << "  * Polygons by plane: (";
	double d = 0.;
	for(std::vector<MultiPath3DPlanar>::const_iterator s = slices[i].begin();
	    s != slices[i].end(); preds = s, ++s) {
	  assert((*s).nbPaths() != 0);
	  if (preds != s) {
	    const double dd = (*s).distance((*preds).getPoint());
	    d += dd;
	    str << ")-[" << dd << "]-(";
	  }
	  str << (*s).nbPaths();
	}
	str << ")" << std::endl;
	if (slices[i].size() > 1) {
	  d /= (slices[i].size() - 1);
	  str << "  * Mean space between slices: " << d << std::endl;
	}
      }
    }
  return str.str();
}


std::vector<Path3D> MeshBySlices::getPaths() const {
  std::vector<Path3D> result;
  for(unsigned char i = 0; i != 3; ++i)
    for(std::vector<MultiPath3DPlanar>::const_iterator mp = slices[i].begin();
	mp != slices[i].end(); ++mp) {
      std::vector<Path3D> r = (*mp).getPaths();
      result.insert(result.end(), r.begin(), r.end());
    }

  return result;
}

MeshBySlices::VCPoint MeshBySlices::addIntersectionPoints(const PathDescriptor & pdesc) {
  VCPoint result;
  if (size(pdesc) < 2)
    return result;

  MultiPath3DPlanar & myPlane = getPlane(pdesc);
  Path2D::iterator p1 = begin(pdesc);

  unsigned int idP1 = 0;
  for(Path2D::iterator p2 = begin(pdesc) + 1; p2 != end(pdesc); ++p2, ++p1, ++idP1) {
    Coord3D p3D(myPlane.get3DCoords(*p2));
    Coord3D predp3D(myPlane.get3DCoords(*p1));
    if (p3D.distance(predp3D) > error) {
      std::vector<std::pair<Coord3D, unsigned char> > crossings;
      for(unsigned int i = 0; i != 3; ++i)
	if (i != pdesc.getDirection()) {
	  unsigned int cpt = 0;
	  for(std::vector<MultiPath3DPlanar>::iterator mp = slices[i].begin(); mp != slices[i].end(); ++mp, ++cpt) {
	    if ((*mp).distance(predp3D) < errorPlanes) {
	      result.push_back(CrossingPoint(*this, pdesc, idP1, i));
	    }
	    else if (!((*mp).distance(p3D) < errorPlanes))
	      if (!(*mp).sameSide(p3D, predp3D)) {
		crossings.push_back(std::pair<Coord3D, unsigned char>((*mp).getIntersection(Line3D(p3D, predp3D - p3D)), i));
	      }
	  }
	}
      if (crossings.size() != 0) {
	assert(crossings.size() < 3);
	assert((crossings.size() != 2) || (crossings.front().second != crossings[1].second) || !(crossings.front().first == crossings[1].first));
	if ((crossings.size() == 2) && (crossings.front().first.distance(crossings[1].first) < error)) {
	  Coord3D npc = (crossings.front().first + crossings[1].first) / 2;
	  crossings.clear();
	  crossings.push_back(std::pair<Coord3D, unsigned char>(npc, pdesc.getDirection()));
	}

	if ((crossings.size() == 2) && (crossings.front().first.distance(predp3D) > crossings[1].first.distance(predp3D)))
	  std::reverse(crossings.begin(), crossings.end());

	// add the first one
	p1 = myPlane.insertPoint(pdesc.getPath(), p2, crossings.front().first);
	++idP1;
	assert(p1 != end(pdesc));
	p2 = p1 + 1;
	result.push_back(CrossingPoint(*this, pdesc, idP1, crossings.front().second));

	if (crossings.size() == 2) {
	  p1 = myPlane.insertPoint(pdesc.getPath(), p2, crossings[1].first);
	  assert(p1 != end(pdesc));
	  ++idP1;
	  p2 = p1 + 1;
	  result.push_back(CrossingPoint(*this, pdesc, idP1, crossings[1].second));
	}
      }
    }
  }


  return result;
}

struct sortFirst {
  bool operator()(const std::pair<unsigned int, Coord3D> & p1, const std::pair<unsigned int, Coord3D> & p2) const {
    return p1.first < p2.first;
  }
};

std::vector<std::pair<unsigned int, Coord3D> > MeshBySlices::getIntersectionPoints(const PathDescriptor & pdesc) {
  std::vector<std::pair<unsigned int, Coord3D> > result;
  for(VVPathDescriptor::const_iterator cc = connectedComponents.begin(); cc != connectedComponents.end(); ++cc)
    for(VCPoint::const_iterator c = (*cc).second.begin(); c != (*cc).second.end(); ++c)
      if ((*c).contains(pdesc))
	result.push_back(std::pair<unsigned int, Coord3D>((*c).getPoint(pdesc), (*c).getCoord()));

  std::sort(result.begin(), result.end(), sortFirst());
  return result;
}


std::string MeshBySlices::CrossingPoint::toString() const {
  std::ostringstream str;
  str << "CrossingPoint(" << getCoord() << ", " << points.size() << " points, directions: (" << cDirections[0] << ", "
      << cDirections[1] << ", " << cDirections[2] << "))";
  return str.str();
}

bool MeshBySlices::CrossingPoint::contains(const MeshBySlices::PathDescriptor & pdesc) const {
  for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p)
    if ((*p).first == pdesc)
      return true;
  return false;
}

unsigned int MeshBySlices::CrossingPoint::getPoint(const MeshBySlices::PathDescriptor & pdesc) const {
  for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p)
    if ((*p).first == pdesc)
      return (*p).second;
  throw Exception("getPoint(1): this crossing do not corresponds to the given path");
}



MeshBySlices::PathDescriptor::PathDescriptor(const MeshBySlices & mbs) : meshBS(&mbs), directionid(3), planeid(0), pathid(0) {}
MeshBySlices::PathDescriptor::PathDescriptor(const MeshBySlices & mbs,
					     unsigned char direction,
					     unsigned int plane,
					     unsigned int path) : meshBS(&mbs), directionid(direction),
								  planeid(plane),
								  pathid(path) {
  assert(directionid < 3);
}

MeshBySlices::PathDescriptor::PathDescriptor(const PathDescriptor & pdesc) : meshBS(pdesc.meshBS),
									     directionid(pdesc.directionid),
									     planeid(pdesc.planeid),
									     pathid(pdesc.pathid) {

}

MeshBySlices::PathDescriptor MeshBySlices::PathDescriptor::firstPath(const MeshBySlices & mbs) {
  return mbs.begin();
}


MeshBySlices::PathDescriptor & MeshBySlices::PathDescriptor::operator++() {
  if (directionid == 3)
    return *this;

  if (pathid + 1 < (*meshBS).slices[directionid][planeid].nbPaths())
    ++pathid;
  else {
    pathid = 0;
    if (planeid + 1 < (*meshBS).slices[directionid].size()) {
      ++planeid;
    }
    else {
      planeid = 0;
      ++directionid;
      while ((directionid != 3) && ((*meshBS).directions[directionid] == Coord3D()))
	++directionid;
    }
  }

  return *this;
}

MeshBySlices::PathDescriptor MeshBySlices::begin() const {
  if ((directions[0].norm() == 0.) || (slices[0].size() == 0) || (slices[0].front().nbPaths() == 0))
    return PathDescriptor(*this);
  else
    return PathDescriptor(*this, 0, 0, 0);
}

MeshBySlices::PathDescriptor MeshBySlices::end() const {
  return PathDescriptor(*this);
}

Path3D MeshBySlices::getPath(const MeshBySlices::PathDescriptor & pdesc) const {
  assert(isValid(pdesc));
  Path3D result = slices[pdesc.getDirection()][pdesc.getPlane()].getPath(pdesc.getPath());
  assert(slices[pdesc.getDirection()][pdesc.getPlane()].distance(result.front()) < errorPlanes);
  assert(slices[pdesc.getDirection()][pdesc.getPlane()].distance(result.back()) < errorPlanes);
  return result;
}

Coord3D MeshBySlices::getPoint(const PathDescriptor & pdesc, unsigned int pointid) const {
  assert(pdesc.getDirection() < 3);
  assert(slices[pdesc.getDirection()].size() > pdesc.getPlane());
  const MultiPath3DPlanar & mpath = slices[pdesc.getDirection()][pdesc.getPlane()];
  assert(mpath.nbPaths() > pdesc.getPath());
  return mpath.get3DPoint(pdesc.getPath(), pointid);
}


void MeshBySlices::merge(MeshBySlices::VCPoint & result, const VCPoint & newCoords, double epsilon) {
  VCPoint::const_iterator ee = newCoords.end();
  for(VCPoint::const_iterator p = newCoords.begin(); p != ee; ++p) {
    bool merged = false;
    for(VCPoint::iterator pp = result.begin(); pp != result.end(); ++pp)
      if ((*pp).softMerge(*p, epsilon)) {
	merged = true;
	break;
      }
    if (!merged) {
      result.push_back(*p);
    }
  }
}

bool MeshBySlices::isValid(const MeshBySlices::PathDescriptor & pdesc) const {
  return ((pdesc.getDirection() < 3) &&
	  (directions[pdesc.getDirection()].norm() != 0.) &&
	  (slices[pdesc.getDirection()].size() > pdesc.getPlane()) &&
	  (slices[pdesc.getDirection()][pdesc.getPlane()].nbPaths() > pdesc.getPath()));
}

Path2D::iterator MeshBySlices::begin(const MeshBySlices::PathDescriptor & pdesc) {
  assert(isValid(pdesc));
  return slices[pdesc.getDirection()][pdesc.getPlane()].begin(pdesc.getPath());
}

Path2D::iterator MeshBySlices::end(const MeshBySlices::PathDescriptor & pdesc) {
  assert(isValid(pdesc));
  return slices[pdesc.getDirection()][pdesc.getPlane()].end(pdesc.getPath());
}

Path2D::const_iterator MeshBySlices::begin(const MeshBySlices::PathDescriptor & pdesc) const {
  assert(isValid(pdesc));
  return slices[pdesc.getDirection()][pdesc.getPlane()].begin(pdesc.getPath());
}

Path2D::const_iterator MeshBySlices::end(const MeshBySlices::PathDescriptor & pdesc) const {
  assert(isValid(pdesc));
  return slices[pdesc.getDirection()][pdesc.getPlane()].end(pdesc.getPath());
}

unsigned int MeshBySlices::size(const MeshBySlices::PathDescriptor & pdesc) const {
  assert(isValid(pdesc));
  return slices[pdesc.getDirection()][pdesc.getPlane()].size(pdesc.getPath());
}

bool MeshBySlices::hasCommonPoint(const MeshBySlices::VCPoint & cs1, const MeshBySlices::VCPoint & cs2, double epsilon) {
  for(VCPoint::const_iterator c1 = cs1.begin(); c1 != cs1.end(); ++c1)
    for(VCPoint::const_iterator c2 = cs2.begin(); c2 != cs2.end(); ++c2)
      if ((*c1).isCompatible(*c2, epsilon))
	return true;
  return false;
}

// TODO: improve the method using the following algorithm:
//  - add intersection points on paths, and get the corresponding CrossingPoints
//  - store the Crossing points on the corresponding lines
//  - merge them using their ordering along the line
// Remark: specific configuration with CrossingPoints on more than one line
MeshBySlices & MeshBySlices::computeCC() {
  // reset data structure
  connectedComponents.clear();

  // add paths on the first direction, and missing points (corresponding to crossings)
  PathDescriptor pdesc = begin();
  for(; pdesc.getDirection() != 1; ++pdesc) {
    connectedComponents.push_back(std::pair<VPathDescriptor, VCPoint>(VPathDescriptor(), addIntersectionPoints(pdesc)));
    connectedComponents.back().first.push_back(pdesc);
  }

  // then for each other path, merge the already existing connected components, or create a new one if required
  const PathDescriptor e(end());
  for(; pdesc != e; ++pdesc) {
    const Path3D p = getPath(pdesc);
    VCPoint newVCPoint = addIntersectionPoints(pdesc);
    std::vector<VVPathDescriptor::iterator> intersected;
    // find the corresponding connected components
    VVPathDescriptor::iterator ee = connectedComponents.end();
    for(VVPathDescriptor::iterator cc = connectedComponents.begin(); cc != ee; ++cc) {
      if (hasCommonPoint(newVCPoint, (*cc).second, error)) {
	intersected.push_back(cc);
      }
    }

    // create the new structure
    VPathDescriptor newVPath;
    newVPath.push_back(pdesc);

    if (intersected.size() != 0) {
      // merge the existing connected components on the new created structure
      std::vector<VVPathDescriptor::iterator>::const_iterator e = intersected.end();
      for(std::vector<VVPathDescriptor::iterator>::const_iterator icc = intersected.begin(); icc != e; ++icc) {
	newVPath.insert(newVPath.end(), (**icc).first.begin(), (**icc).first.end());
	merge(newVCPoint, (**icc).second, error);
      }

      // remove the old ones (from the last to the first, in order to prevent wrong memory access)
      std::reverse(intersected.begin(), intersected.end());
      std::vector<VVPathDescriptor::iterator>::const_iterator e2 = intersected.end();
      for(std::vector<VVPathDescriptor::iterator>::const_iterator icc = intersected.begin(); icc != e2; ++icc)
	connectedComponents.erase(*icc);
    }
    // then add the created structure in the cc list
    connectedComponents.push_back(std::pair<VPathDescriptor, VCPoint>(newVPath, newVCPoint));
  }
  // TODO: merge crossing points at plane intersection

  return *this;
}


Coord3D MeshBySlices::getIntersection(unsigned int i,
				      unsigned int j,
				      unsigned int k) const {
  if ((directions[0].norm() * directions[1].norm() * directions[2].norm() == 0.) ||
      (slices[0].size() <= i) || (slices[1].size() <= j) || (slices[2].size() <= k))
    throw Exception("getIntersection(3): coordinates out of bound");
  return slices[0][i].getIntersection(slices[1][j], slices[2][k]);
}

Coord3D MeshBySlices::getSliceSpace(unsigned int d, unsigned int s) const {
  if ((directions[d].norm() == 0.) ||
      (slices[d].size() <= s + 1))
    throw Exception("getSliceSpace(2): coordinates out of bound");
  return directions[d] * slices[d][s + 1].distance(slices[d][s].getPoint());
}



int MeshBySlices::getSliceIdBefore(unsigned char direction, const Coord3D & point) const {
  int result = -1;
  for(std::vector<MultiPath3DPlanar>::const_iterator s = slices[direction].begin(); s != slices[direction].end(); ++s)
    if ((point - (*s).getOrthogonalProjection(point)).getNormalize().distance(directions[direction]) < .5)
      ++result;
    else
      break;
  return result;
}

MeshBySlices::CellProperties & MeshBySlices::getCell(const Coord3D & point,
						     const PathDescriptor & pdesc,
						     bool before) {
  int refs[3];
  for(unsigned char i = 0; i != 3; ++i) {
    if (i == pdesc.getDirection()) {
      refs[i] = before ? pdesc.getPlane() - 1 : pdesc.getPlane();
    }
    else {
      refs[i] = getSliceIdBefore(i, point);
    }
  }
  return getCell(refs[0], refs[1], refs[2]);
}

bool MeshBySlices::isValidCell(int i, int j, int k) const {
  return ((i >= -1) && (j >= -1) && (k >= -1) && (i + 1 < (int)intersliceCells.size()) &&
	  (j + 1 < (int)intersliceCells[i + 1].size()) && (k + 1 < (int)intersliceCells[i + 1][j + 1].size()));
}

MeshBySlices::CellProperties & MeshBySlices::getCell(int i, int j, int k) {
  assert(isValidCell(i, j, k));
  return intersliceCells[i + 1][j + 1][k + 1];
}

const MeshBySlices::CellProperties & MeshBySlices::getCell(int i, int j, int k) const {
  assert(isValidCell(i, j, k));
  return intersliceCells[i + 1][j + 1][k + 1];
}

Coord3D MeshBySlices::getCellPoint(int i, int j, int k) const {
  if (directions[2].norm() != 0.) {
    assert(directions[0].norm() != 0.);
    assert(directions[1].norm() != 0.);
    Coord3D point = getIntersection(i == -1 ? 0 : i,
				    j == -1 ? 0 : j,
				    k == -1 ? 0 : k);
    if (i == -1)
      point -= getSliceSpace(0, 0) / 2;
    else
      if (i == (int)slices[0].size() - 1)
	point += getSliceSpace(0, i - 1) / 2;
      else
	point += getSliceSpace(0, i) / 2;
    if (j == -1)
      point -= getSliceSpace(1, 0) / 2;
    else
      if (j == (int)slices[1].size() - 1)
	point += getSliceSpace(1, j - 1) / 2;
      else
	point += getSliceSpace(1, j) / 2;
    if (k == -1)
      point -= getSliceSpace(2, 0) / 2;
    else
      if (k == (int)slices[2].size() - 1)
	point += getSliceSpace(2, k - 1) / 2;
      else
	point += getSliceSpace(2, k) / 2;

    return point;
  }
  else {
    if (directions[1].norm() == 0.) {
      throw Exception("getCellPoint(3): not yet implemented");
    }
    assert(directions[0].norm() != 0.);
    assert(directions[1].norm() != 0.);
    Line3D intersection = slices[0][i < 0? 0 : i].getIntersection(slices[1][j < 0? 0 : j]);
    Coord3D point = getIsoBarycenter().projectionOnAxis(intersection);
    if (i == -1)
      point -= getSliceSpace(0, 0) / 2;
    else
      if (i == (int)slices[0].size() - 1)
	point += getSliceSpace(0, i - 1) / 2;
      else
	point += getSliceSpace(0, i) / 2;
    if (j == -1)
      point -= getSliceSpace(1, 0) / 2;
    else
      if (j == (int)slices[1].size() - 1)
	point += getSliceSpace(1, j - 1) / 2;
      else
	point += getSliceSpace(1, j) / 2;
    return point;
  }
}

MeshBySlices & MeshBySlices::computeIntersliceProperties() {
  if (connectedComponents.size() == 0)
    computeCC();

  if (intersliceCells.size() != 0)
    intersliceCells.clear();

  nbEdges.clear();
  nbCircularEdges.clear();
  nbEdges.insert(nbEdges.begin(), connectedComponents.size(), 0);
  nbCircularEdges.insert(nbCircularEdges.begin(), connectedComponents.size(), 0);
  assert(nbEdges.size() == connectedComponents.size());
  assert(nbCircularEdges.size() == connectedComponents.size());

  // create cells
  for(int i = -1; i != (int)slices[0].size(); ++i) {
    intersliceCells.push_back(std::vector<std::vector<CellProperties> >());
    for(int j = -1; j != (int)slices[1].size(); ++j) {
      intersliceCells.back().push_back(std::vector<CellProperties>());
      for(int k = -1; k != (int)slices[2].size(); ++k) {
	Coord3D point = getCellPoint(i, j, k);
	intersliceCells.back().back().push_back(CellProperties(point, i, j, k));
      }
      assert(intersliceCells.back().back().size() == slices[2].size() + 1);
    }
    assert(intersliceCells.back().size() == slices[1].size() + 1);
  }
  assert(intersliceCells.size() == slices[0].size() + 1);
  // add data in each cell using paths
  PathDescriptor e(end());
  for(PathDescriptor pdesc = begin(); pdesc != e; ++pdesc) {
    const unsigned int pId = getCCId(pdesc);
    const Path3D path = getPath(pdesc);

    std::vector<std::pair<unsigned int, Coord3D> > crossings = getIntersectionPoints(pdesc);

    if (crossings.size() == 0) {
      // if the full path is in a single "square", all the path will be added to the cell
      getCell(path.front(), pdesc, true).addPath(pdesc, path[0], path[0], 0, path.size() - 1, pId);
      getCell(path.front(), pdesc, false).addPath(pdesc, path[0], path[0], 0, path.size() - 1, pId);
      ++nbEdges[pId];
      ++nbCircularEdges[pId];
    }
    else {
      std::vector<std::pair<unsigned int, Coord3D> >::const_iterator predc = crossings.begin() + (crossings.size() - 1);
      for(std::vector<std::pair<unsigned int, Coord3D> >::const_iterator c = crossings.begin(); c != crossings.end(); ++c) {
	assert((*predc).first < path.size());
	assert((*c).first < path.size());
	Coord3D middle;
	if (((*predc).first + 1 == (*c).first) || (((*c).first == 0) && ((*predc).first == path.size() - 2)))
	  middle = (path[(*predc).first] + path[(*c).first]) / 2;
	else {
	  unsigned int mid = (*predc).first + 1;
	  if (mid == path.size())
	    mid = 0;
	  assert(mid < path.size());
	  middle = path[mid];
	}

	assert(!((*predc).second == (*c).second));
	getCell(middle, pdesc, true).addPath(pdesc, (*predc).second, (*c).second, (*predc).first, (*c).first, pId);
	getCell(middle, pdesc, false).addPath(pdesc, (*predc).second, (*c).second, (*predc).first, (*c).first, pId);
	++nbEdges[pId];

	predc = c;
      }
    }
  }

  // set inside/outside flags
  CellIterator cend(end_cell());
  for(CellIterator c = begin_cell(); c != cend; ++c) {
    if (!(*c).isBorder()) {
      MultiPath3DPlanar p;
      if ((*c).getSliceX() == -1)
	p = slices[0][0];
      else
	p = slices[0][(*c).getSliceX()];
      Coord3D point(p.getOrthogonalProjection((*c).getLocation()));
      assert(point.distance((*c).getLocation()) <
	     getSliceSpace(0, (*c).getSliceX() >= 0 ? ((*c).getSliceX() >= (int)slices[0].size() - 1 ?
						       (*c).getSliceX() - 1 : (*c).getSliceX()) : 0).norm());
      if (p.isInsideObject(point))
	(*c).setInside();
      else
	(*c).setOutside();
    }
    else
      (*c).computePolygons();
  }

  return *this;
}

UCharCImg MeshBySlices::getIntersliceImage() {
  if (intersliceCells.size() == 0)
    computeIntersliceProperties();

  CImg<unsigned char> result(intersliceCells.size(),
			     intersliceCells.front().size(),
			     intersliceCells.front().front().size());

  result.fill(0);
  for(std::vector<std::vector<std::vector<CellProperties> > >::const_iterator ppp = intersliceCells.begin(); ppp != intersliceCells.end(); ++ppp) {
    for(std::vector<std::vector<CellProperties> >::const_iterator pp = (*ppp).begin(); pp != (*ppp).end(); ++pp) {
      for(std::vector<CellProperties>::const_iterator p = (*pp).begin(); p != (*pp).end(); ++p) {
	const unsigned int i = (*p).getSliceX() + 1;
	const unsigned int j = (*p).getSliceY() + 1;
	const unsigned int k = (*p).getSliceZ() + 1;
	if ((*p).isBorder())
	  result(i, j, k) = 1;
	else if ((*p).isInside())
	  result(i, j, k) = 2;
      }
    }
  }

  return result;
}


std::string MeshBySlices::SubPathDescriptor::toString() const {
  std::ostringstream str;
  str << "[" << cbegin << "-" << cend << "]";
  return str.str();
}


MeshBySlices::CellProperties::CellProperties() { }
MeshBySlices::CellProperties::CellProperties(const Coord3D & l, int x, int y, int z) : location(l) {
  predSlices[0] = x;
  predSlices[1] = y;
  predSlices[2] = z;
  inside = false;
}
MeshBySlices::CellProperties::CellProperties(const CellProperties & cp) : location(cp.location), borders(cp.borders) {
  predSlices[0] = cp.predSlices[0];
  predSlices[1] = cp.predSlices[1];
  predSlices[2] = cp.predSlices[2];
  inside = cp.inside;
}

MeshBySlices::CellProperties &
MeshBySlices::CellProperties::addPath(const PathDescriptor & pdesc,
				      const Coord3D & cbegin, const Coord3D & cend,
				      unsigned int begin, unsigned int end,
				      unsigned int idCC) {
  borders.push_back(SubPathDescriptor(pdesc, cbegin, cend, begin, end, idCC));
  return *this;
}


MeshBySlices::CellProperties & MeshBySlices::CellProperties::computePolygons() {
  for(std::vector<SubPathDescriptor>::const_iterator b = borders.begin(); b != borders.end(); ++b) {
    std::vector<std::vector<std::vector<SubPathDescriptor> >::iterator> connected;
    for(std::vector<std::vector<SubPathDescriptor> >::iterator p = polygons.begin(); p != polygons.end();  ++p) {
      for(std::vector<SubPathDescriptor>::const_iterator sp = (*p).begin(); sp != (*p).end(); ++sp)
	if ((*sp).isConnected(*b)) {
	  connected.push_back(p);
	  break;
	}
    }
    if (connected.size() == 0) {
      polygons.push_back(std::vector<SubPathDescriptor>());
      polygons.back().push_back(*b);
    }
    else {
      assert(connected.size() < 3);
      if (connected.size() == 1)
	// add the new subpart in the existing part
	(*(connected.front())).push_back(*b);
      else { // (connected.size() == 2)
	// merge the two existing parts, and add the new subpart
	(*(connected.front())).insert((*(connected.front())).end(), (*connected[1]).begin(), (*connected[1]).end());
	(*(connected.front())).push_back(*b);
	polygons.erase(connected[1]);
      }
    }
  }
  return *this;
}

unsigned int MeshBySlices::CellProperties::getNbPolygons() const {
  return polygons.size();
}

unsigned int MeshBySlices::CellProperties::getNbPolygons(unsigned int id) const {
  unsigned int result = 0;

  for(std::vector<std::vector<SubPathDescriptor> >::const_iterator p = polygons.begin(); p != polygons.end(); ++p) {
    if (((*p).size() != 0) && ((*p).front().getId() == id)) {
      ++result;
    }
  }

  return result;
}

unsigned int MeshBySlices::CellProperties::getCC(unsigned int id) const {
  if (id >= polygons.size())
    throw Exception("getCC(1): wrong polygon id");
  return polygons[id].front().getId();
}


MeshBySlices::CellIterator::CellIterator(std::vector<std::vector<std::vector<MeshBySlices::CellProperties> > > &
					 iCell) : ii(&iCell),
						  cit((*ii).begin()),
						  ccit((*ii).front().begin()),
						  cccit((*ii).front().front().begin()) {

}

MeshBySlices::CellIterator::CellIterator(std::vector<std::vector<std::vector<MeshBySlices::CellProperties> > > & iCell,
					 const std::vector<std::vector<std::vector<MeshBySlices::CellProperties> > >::iterator & cit_,
					 const std::vector<std::vector<MeshBySlices::CellProperties> >::iterator & ccit_,
					 const std::vector<MeshBySlices::CellProperties>::iterator & cccit_) : ii(&iCell),
													       cit(cit_),
													       ccit(ccit_),
													       cccit(cccit_) {

}

MeshBySlices::CellIterator::CellIterator(const MeshBySlices::CellIterator & cp) : ii(cp.ii), cit(cp.cit), ccit(cp.ccit), cccit(cp.cccit) {

}

MeshBySlices::CellIteratorConst::CellIteratorConst(const std::vector<std::vector<std::vector<MeshBySlices::CellProperties> > > &
						   iCell) : ii(&iCell),
							    cit((*ii).begin()),
							    ccit((*ii).front().begin()),
							    cccit((*ii).front().front().begin()) {

}

MeshBySlices::CellIteratorConst::CellIteratorConst(const std::vector<std::vector<std::vector<CellProperties> > > & iCell,
						   const std::vector<std::vector<std::vector<CellProperties> > >::const_iterator & cit_,
						   const std::vector<std::vector<CellProperties> >::const_iterator & ccit_,
						   const std::vector<CellProperties>::const_iterator & cccit_) : ii(&iCell),
														 cit(cit_),
														 ccit(ccit_),
														 cccit(cccit_) {

}

MeshBySlices::CellIteratorConst::CellIteratorConst(const CellIteratorConst & cp) : ii(cp.ii), cit(cp.cit), ccit(cp.ccit), cccit(cp.cccit) {

}

MeshBySlices::CellIteratorConst::CellIteratorConst(const CellIterator & cp) : ii(cp.ii), cit(cp.cit), ccit(cp.ccit), cccit(cp.cccit) {

}

unsigned int MeshBySlices::getNbPolygons() {
  if (intersliceCells.size() == 0)
    computeIntersliceProperties();

  unsigned int result = 0;

  CellIteratorConst cend(end_cell());
  for(CellIteratorConst c = begin_cell(); c != cend; ++c) {
    result += (*c).getNbPolygons();
  }

  return result;
}

unsigned int MeshBySlices::getNbPolygons(unsigned int id) {
  if (intersliceCells.size() == 0)
    computeIntersliceProperties();

  unsigned int result = 0;

  CellIteratorConst cend(end_cell());
  for(CellIteratorConst c = begin_cell(); c != cend; ++c) {
    result += (*c).getNbPolygons(id);
  }

  return result;
}


unsigned int MeshBySlices::getNbEdges() {
  if (nbEdges.size() == 0)
    computeIntersliceProperties();

  unsigned int result = 0;
  for(std::vector<unsigned int>::const_iterator nbe = nbEdges.begin(); nbe != nbEdges.end(); ++nbe)
    result += *nbe;
  return result;
}

unsigned int MeshBySlices::getNbEdges(unsigned int id) {
  if (nbEdges.size() == 0)
    computeIntersliceProperties();

  if (id >= nbEdges.size())
    return 0;
  else
    return nbEdges[id];
}

unsigned int MeshBySlices::getNbCircularEdges(unsigned int id) {
  if (nbCircularEdges.size() == 0)
    computeIntersliceProperties();

  if (id >= nbCircularEdges.size())
    return 0;
  else
    return nbCircularEdges[id];
}


unsigned int MeshBySlices::getNbPoints(unsigned int id) {
  if (connectedComponents.size() == 0)
    computeCC();

  if (id >= connectedComponents.size())
    return 0;
  else
    return connectedComponents[id].second.size();
}

unsigned int MeshBySlices::getNbPoints() {
  if (connectedComponents.size() == 0)
    computeCC();

  unsigned int result = 0;
  for(VVPathDescriptor::const_iterator vp = connectedComponents.begin(); vp != connectedComponents.end(); ++vp)
    result += (*vp).second.size();
  return result;
}


Box3D MeshBySlices::getBoundedBox(unsigned int id) {
  if (connectedComponents.size() == 0)
    computeCC();

  Coord3D p1(getPoint(connectedComponents[id].first.front(), 0));
  Coord3D p2(getPoint(connectedComponents[id].first.front(), 1));
  Box3D result(p1, p2);

  for(VPathDescriptor::const_iterator vp = connectedComponents[id].first.begin(); vp != connectedComponents[id].first.end(); ++vp) {
    Path3D path(getPath(*vp));
    for(Path3D::const_iterator p = path.begin(); p != path.end(); ++p)
      result.addPoint(*p);
  }

  return result;
}

std::string MeshBySlices::toStringCC() {
  std::ostringstream str;
  str << "Number of connected components: " << getNbCC() << std::endl;
  unsigned int i = 0;
  for(VVPathDescriptor::const_iterator vp = connectedComponents.begin();
      vp != connectedComponents.end(); ++vp, ++i) {

    str << "Connected component #" << i << std::endl;
    str << "  * Number of paths: " << (*vp).first.size() << std::endl;
    str << "  * Number of points: "<< getNbPoints(i) << ", edges: " << getNbEdges(i) << " (";
    str << "including " << getNbCircularEdges(i) << " circular), polygons: " << getNbPolygons(i) << std::endl;
    str << "  * Euler caracteristic: " << getEulerCaracteristic(i) << std::endl;
    str << "  * Genus: " << getGenus(i) << std::endl;

    unsigned int nbCellsNotSingle = 0;
    CellIteratorConst cend(end_cell());
    for(CellIteratorConst c = begin_cell(); c != cend; ++c)
      if ((*c).getNbPolygons(i) > 1)
	nbCellsNotSingle += 1;
    str << "  * Number of cells with more than one polygon: " << nbCellsNotSingle << std::endl;

    str << "  * " << getBoundedBox(i) << std::endl;
  }
  return str.str();
}



MeshBySlices & MeshBySlices::computeCCInverseDirectory() {
  if (connectedComponents.size() == 0)
    computeCC();

  inverseDirectory[0].clear();
  inverseDirectory[1].clear();
  inverseDirectory[2].clear();

  for(unsigned char i = 0; i != 3; ++i) {
    inverseDirectory[i].insert(inverseDirectory[i].begin(), slices[i].size(),
			       std::vector<unsigned int>());

    const unsigned int nbCC = getNbCC();
    std::vector<std::vector<unsigned int> >::iterator j = inverseDirectory[i].begin();
    for(std::vector<MultiPath3DPlanar>::const_iterator mp = slices[i].begin();
	mp != slices[i].end(); ++mp, ++j)
      (*j).insert((*j).begin(), (*mp).nbPaths(), nbCC);
  }

  unsigned int idCC = 0;
  for(VVPathDescriptor::const_iterator cc = connectedComponents.begin();
      cc != connectedComponents.end(); ++cc, ++idCC)
    for(VPathDescriptor::const_iterator vp = (*cc).first.begin();
	vp != (*cc).first.end(); ++vp) {
      inverseDirectory[(*vp).getDirection()][(*vp).getPlane()][(*vp).getPath()] = idCC;
    }

  return *this;
}

unsigned int MeshBySlices::getCCId(const PathDescriptor & pdesc) {
  if ((inverseDirectory[0].size() == 0) &&
      (inverseDirectory[1].size() == 0) &&
      (inverseDirectory[2].size() == 0))
    computeCCInverseDirectory();

  return inverseDirectory[pdesc.getDirection()][pdesc.getPlane()][pdesc.getPath()];
}

Layer MeshBySlices::getInclusions(const MultiPath3DPlanar & mplane,
				  unsigned int direction,
				  unsigned int plane) {

  Layer inclusions = mplane.getInclusions();

  assert(inclusions.size() == mplane.nbPaths());

  for(LayerIterator l = inclusions.begin(); l != inclusions.end(); ++l) {
    PathDescriptor pdesc(*this, direction, plane, (*l).getId());
    assert(isValid(pdesc));
    (*l).setId(getCCId(pdesc));
  }

  // after this reconstruction, several brothers of children may became the same object
  // TODO: do not only remove similar layers, but move children as brothers...
  inclusions.mergeSimilarLayers();

  return inclusions;
}

MeshBySlices & MeshBySlices::computeLayerStructure() {
  if (connectedComponents.size() == 0)
    computeCC();

  layers.clear();

  for(unsigned char i = 0; i != 3; ++i) {
    unsigned int j = 0;
    for(std::vector<MultiPath3DPlanar>::const_iterator mp = slices[i].begin();
	mp != slices[i].end(); ++mp, ++j) {
      layers.merge(getInclusions(*mp, i, j));
      if (layers.size() == connectedComponents.size())
	return *this;
    }
  }

  return *this;
}

void MeshBySlices::toStringCavity(std::ostringstream & str, const Layer & layer,
		    const std::string & space) {
  if (layer.getId() != -1) {
    str << space << "Layer #" << layer.getId();
    if (layer.isCavity())
      str << " (cavity)";
    str << ", genus: " << getGenus(layer.getId());
    str << ", number of polygons: " << getNbPolygons(layer.getId()) << std::endl;
  }
  for(std::vector<Layer>::const_iterator c = layer.children_begin();
      c != layer.children_end(); ++c) {
    toStringCavity(str, *c, space + " ");
  }
}

std::string MeshBySlices::toStringCavities() {
  if (layers.isEmpty())
    computeLayerStructure();
  if (intersliceCells.size() == 0)
    computeIntersliceProperties();

  std::ostringstream str;

  str << "Cavities (" << layers.size() << ")" << std::endl;
  toStringCavity(str, layers, "");

  return str.str();
}


SceneDescriptor MeshBySlices::getSubSceneDescription(const Layer & layer) {
  SceneDescriptor result;
  if (!layer.isAmbientSpace())
    result.setGenus(getGenus(layer.getId()));

  assert(layer.isAmbientSpace() == result.isOrigin());

  for(std::vector<Layer>::const_iterator c = layer.children_begin();
      c != layer.children_end(); ++c)
    result.addSDChild(getSubSceneDescription(*c));

  return result;
}

SceneDescriptor MeshBySlices::getSceneDescription() {
  if (layers.isEmpty())
    computeLayerStructure();

  return getSubSceneDescription(layers);
}

MergeDescriptor MeshBySlices::getInitialSubMergeDescription(const Layer & layer) {
  MergeDescriptor result;
  if (!layer.isAmbientSpace()) {
    result.addObject(layer.getId(), getGenus(layer.getId()));
  }

  assert(layer.isAmbientSpace() == result.isOrigin());

  for(std::vector<Layer>::const_iterator c = layer.children_begin();
      c != layer.children_end(); ++c)
    result.addMDChild(getInitialSubMergeDescription(*c));

  return result;
}

MergeDescriptor MeshBySlices::getInitialMergeDescription() {
  if (layers.isEmpty())
    computeLayerStructure();

  return getInitialSubMergeDescription(layers);
}

Coord3D MeshBySlices::getIsoBarycenter() const {
  Coord3D result;
  unsigned int nbPoints = 0;


  PathDescriptor e(end());
  for(PathDescriptor pdesc = begin(); pdesc != e; ++pdesc) {
    Path3D path(getPath(pdesc));
    for(Path3D::const_iterator p = path.begin(); p != path.end(); ++p, ++nbPoints)
      result += *p;
  }

  if (nbPoints != 0)
    result /= nbPoints;

  return result;
}


std::vector<MergeStep> MeshBySlices::getPossibleMerges() {
  std::vector<MergeStep> result;
  if (intersliceCells.size() == 0)
    computeIntersliceProperties();

  CellIterator cend(end_cell());
  for(CellIteratorConst c = begin_cell(); c != cend; ++c)
    if ((*c).getNbPolygons() > 1) {
      std::vector<unsigned int> idspolygon;
      std::vector<unsigned int> idscc;
      for(unsigned int i = 0; i != (*c).getNbPolygons(); ++i) {
	idspolygon.push_back(i);
	idscc.push_back((*c).getCC(i));
      }
      result.push_back(MergeStep((*c).getSliceX(), (*c).getSliceY(), (*c).getSliceZ(),
				 idscc, idspolygon));
    }

  return result;
}


void MeshBySlices::initMergeTmpData(const std::vector<MergeStep> & pMerges,
				     MergeStepCost & msc) {
  t_msc = & msc;
  t_pMerges = pMerges;
  assert(t_pSubMerges.size() == 0);
  for(std::vector<MergeStep>::const_iterator pm =  pMerges.begin(); pm != pMerges.end(); ++pm)
    t_pSubMerges.push_back((*pm).getSubMergeSteps());
}

void MeshBySlices::clearMergeTmpData() {
  t_msc = NULL;
  t_pMerges.clear();
  t_pSubMerges.clear();
}

MergeDescriptor MeshBySlices::getMergeDescription(const SceneDescriptor & sd, MergeStepCost & msc) {
  SceneDescriptor sdthis = getSceneDescription();
  if (sd.isUnreachableFrom(sdthis))
    throw Exception("getSceneDescription(2): the wanted scene is not reachable");

  MergeDescriptor mdesc = getInitialMergeDescription();

  std::vector<MergeStep> pMerges = getPossibleMerges();
  const unsigned int nbEM = MergeStep::getNbElementarySteps(pMerges);
  const unsigned int nbEMsd = sdthis.getNbElementarySteps(sd);

  if (nbEM < nbEMsd) {
    throw Exception("getMergeDescription(2): the number of possible merges is too small");
  }
  if (nbEM == nbEMsd) {
    for(std::vector<MergeStep>::const_iterator ms = pMerges.begin();
	ms != pMerges.end(); ++ms)
      mdesc.merge(*ms);
    return mdesc;
  }
  else {
    initMergeTmpData(pMerges, msc);
    std::pair<MergeDescriptor, double> res = getBestMergeDescription(mdesc, 0., sd);
    clearMergeTmpData();
    if (res.second < 0.)
      throw Exception("getMergeDescription(2): the wanted scene is a possible ancestor, but not reachable");
    return res.first;
  }
}




std::pair<MergeDescriptor, double>
MeshBySlices::getBestMergeDescription(const MergeDescriptor & currentMDesc,
				      double currentCost,
				      const SceneDescriptor & target,
				      double oldbest,
				      unsigned int start) {
  int nbMissingSteps = currentMDesc.getNbElementarySteps(target);

  // not reachable
  if (nbMissingSteps < 0)
    return std::pair<MergeDescriptor, double>(currentMDesc, -1);

  // a better solution has been found before
  if (currentCost > oldbest)
    return std::pair<MergeDescriptor, double>(currentMDesc, -1);
  // we found a good solution
  if (currentMDesc.similar(target))
    return std::pair<MergeDescriptor, double>(currentMDesc, currentCost);
  // the configuration we reach is not valid
  if (target.isUnreachableFrom(currentMDesc))
    return std::pair<MergeDescriptor, double>(currentMDesc, -1);

  // check for possible configurations starting from the given one
  double resultCost = oldbest;
  MergeDescriptor result;
  bool found = false;

  std::vector<std::vector<MergeStep> >::const_iterator mmss = t_pSubMerges.begin() + start;
  unsigned int sstart = start;
  for(std::vector<MergeStep>::const_iterator ms = t_pMerges.begin() + start;
      ms != t_pMerges.end(); ++ms, ++mmss, ++sstart) {
    for(std::vector<MergeStep>::const_iterator mms = (*mmss).begin();
	mms != (*mmss).end(); ++mms)
      if ((int)(*mms).getNbElementarySteps() <= nbMissingSteps) {
	const double cost = currentCost + (*t_msc)(*mms);
	if (cost < resultCost) {
	  MergeDescriptor newMDesc(currentMDesc);
	  newMDesc.merge(*mms);
	  if (!target.isUnreachableFrom(newMDesc)) {
	    std::pair<MergeDescriptor, double> r = getBestMergeDescription(newMDesc, cost,
									   target, resultCost, sstart + 1);
	    if (r.second >= 0.) {
	      if (r.second < resultCost) {
		resultCost = r.second;
		found = true;
		result = r.first;
	      }
	    }
	  }
	}
      }
  }

  if (found) {
    return std::pair<MergeDescriptor, double>(result, resultCost);
  }
  else
    return std::pair<MergeDescriptor, double>(currentMDesc, -1);

}


double MeshBySlices::hausdorffDistance(int x, int y, int z,
				       unsigned int id1, unsigned int id2) const {
  assert(intersliceCells.size() != 0);
  const CellProperties & cell = getCell(x, y, z);
  const std::vector<SubPathDescriptor> & p1 = cell.getPolygon(id1);
  const std::vector<SubPathDescriptor> & p2 = cell.getPolygon(id2);
  const std::vector<Coord3D> l1 = getPoints(p1);
  const std::vector<Coord3D> l2 = getPoints(p2);
  return Coord3D::hausdorffDistance(l1, l2);
}


std::vector<Coord3D> MeshBySlices::getPoints(const std::vector<SubPathDescriptor> & polygon) const {
  std::vector<Coord3D> result;
  for(std::vector<SubPathDescriptor>::const_iterator sp = polygon.begin();
      sp != polygon.end(); ++sp) {
    const unsigned int tsize = size((*sp).getPathDescriptor());
    for(unsigned int i = (*sp).getBegin(); i != (*sp).getEnd();) {
      result.push_back(getPoint((*sp).getPathDescriptor(), i));
      ++i;
      if (i == tsize)
	i = 0;
    }
  }
  return result;
}
