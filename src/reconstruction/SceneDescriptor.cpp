/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <iostream>
#include <fstream>

#include "SceneDescriptor.h"
#include "FileExceptions.h"

#include "XMLSimpleNode.h"

using namespace Taglut;


unsigned int SceneDescriptor::size() const {
  unsigned int result = children.size();

  for(std::vector<SceneDescriptor *>::const_iterator c = children.begin();
      c != children.end(); ++c)
    result += (**c).size();
  return result;

}


void SceneDescriptor::writeSubScene(std::ofstream & outfile, const std::string & space) const {
  outfile << space << "<object genus=\"" << getGenus() << "\"";
  if (children.size() == 0)
    outfile << "/>" << std::endl;
  else {
    outfile << ">" << std::endl;
    for(std::vector<SceneDescriptor *>::const_iterator sd = children.begin(); sd != children.end(); ++sd)
      (**sd).writeSubScene(outfile, space + " ");
    outfile << space << "</object>" << std::endl;
  }
}

const SceneDescriptor & SceneDescriptor::save(const std::string & filename) const {
  assert(isOrigin());
  std::ofstream outfile(filename.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;

  outfile << "<scene>" << std::endl;
  for(std::vector<SceneDescriptor *>::const_iterator sd = begin(); sd != end(); ++sd)
    (**sd).writeSubScene(outfile, " ");

  outfile << "</scene>" << std::endl;

  outfile.close();

  return *this;
}

#ifdef LIBXML_READER_ENABLED
void SceneDescriptor::jumpNextNode(xmlTextReaderPtr & reader) {
  int ret;
  do {
    ret = xmlTextReaderRead(reader);
    if (ret != 1) throw Exception(std::string("Error during reading xml file."));
  } while((xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) &&
	  (xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT));
}

void SceneDescriptor::load(xmlTextReaderPtr & reader, const std::string & name) {
  // set genus
  if (name == "object") {
    const char * g = (const char *)xmlTextReaderGetAttribute(reader, (const xmlChar *)"genus");
    if (g != NULL) {
      genus = atoi(g);
      origin = false;
    }
    else
      throw ExceptionErrorDuringReadingFile();
  }

  if (!xmlTextReaderIsEmptyElement(reader)) {
    jumpNextNode(reader);
    while(xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT) {
      assert(xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT);
      assert(std::string("object") == (const char *) xmlTextReaderConstName(reader));
      children.push_back(new SceneDescriptor());
      (*(children.back())).load(reader, "object");
      jumpNextNode(reader);
    }
    assert(name == (const char *) xmlTextReaderConstName(reader));
  }
}
#endif


void SceneDescriptor::load(const std::string & xmlDescription) {
#ifdef LIBXML_READER_ENABLED
  xmlTextReaderPtr reader;
  if ((xmlDescription.size() >= 6) && ((xmlDescription.substr(0, 6) == "<?xml ") ||
				       (xmlDescription.substr(0, 6) == "<scene")))
    reader = xmlReaderForMemory(xmlDescription.c_str(),
				xmlDescription.size(), NULL, NULL, 0);
  else
    reader = xmlReaderForFile(xmlDescription.c_str(), NULL, 0);

  if (reader != NULL) {
    jumpNextNode(reader);
    assert(std::string("scene") == (const char *) xmlTextReaderConstName(reader));
    clear();
    load(reader, "scene");
    assert(std::string("scene") == (const char *) xmlTextReaderConstName(reader));
    xmlFreeTextReader(reader);
    assert(isOrigin());
  }
  else
    throw Exception("XML script not found.");

#else
    throw Exception("XML reader not defined");
#endif
}

unsigned int SceneDescriptor::getCumulatedGenus() const {
  unsigned int result = 0;
  if (!isOrigin())
    result += genus;
  for(std::vector<SceneDescriptor *>::const_iterator c = begin(); c != end(); ++c)
    result += (**c).getCumulatedGenus();
  return result;
}


unsigned int SceneDescriptor::maxDepth() const {
  unsigned int maxrchild = 0;
  for(std::vector<SceneDescriptor *>::const_iterator c = begin(); c != end(); ++c) {
    const unsigned int dc = (**c).maxDepth();
    if (dc > maxrchild)
      maxrchild = dc;
  }
  return (isOrigin() ? 0 : 1) + maxrchild;
}


unsigned int SceneDescriptor::getMaxGenus() const {
  unsigned int mG = isOrigin() ? 0 : genus;
  for(std::vector<SceneDescriptor *>::const_iterator c = begin(); c != end(); ++c) {
    const unsigned int mgC = (**c).getMaxGenus();
    if (mgC > mG)
      mG = mgC;
  }
  return mG;
}


std::vector<unsigned int> SceneDescriptor::getCumulatedSize() const {
  std::vector<unsigned int> rchildren;
  for(std::vector<SceneDescriptor *>::const_iterator c = begin();
      c != children.end(); ++c) {
    std::vector<unsigned int> cc = (**c).getCumulatedSize();
    std::vector<unsigned int>::iterator r = rchildren.begin();
    bool end = (r == rchildren.end());
    for(std::vector<unsigned int>::const_iterator ncc = cc.begin(); ncc != cc.end(); ++ncc) {
      if (end)
	rchildren.push_back(*ncc);
      else {
	*r += *ncc;
	++r;
	end = (r == rchildren.end());
      }
    }
  }
  std::vector<unsigned int> result;
  result.push_back(children.size());
  if (result.back() != 0)
    result.back() += rchildren.front();
  result.insert(result.end(), rchildren.begin(), rchildren.end());
  return result;
}

bool SceneDescriptor::similar(const SceneDescriptor & sd) const {
  if (origin != sd.isOrigin())
    return false;

  if (!origin)
    if (genus != sd.getGenus())
      return false;

  if (getNbChildren() != sd.getNbChildren())
    return false;

  std::vector<bool> identified(getNbChildren(), false);
  for(std::vector<SceneDescriptor *>::const_iterator c = begin(); c != end(); ++c) {

    bool found = false;
    std::vector<bool>::iterator i = identified.begin();
    for(typename std::vector<SceneDescriptor *>::const_iterator csd = sd.begin(); csd != sd.end(); ++csd, ++i) {
      if ((!(*i)) && ((**c).similar(**csd))) {
	found = true;
	(*i) = true;
	break;
      }
    }
    if (!found)
      return false;
  }

  return true;
}

bool SceneDescriptor::isUnreachableFrom(const SceneDescriptor & sd) const {
  if (size() > sd.size())
    return true;
  if (maxDepth() > sd.maxDepth())
    return true;
  if (getCumulatedGenus() < sd.getCumulatedGenus())
    return true;
  if (getMaxGenus() < sd.getMaxGenus())
    return true;

  std::vector<unsigned int> c = getCumulatedSize();
  std::vector<unsigned int> csd = sd.getCumulatedSize();

  std::vector<unsigned int>::const_iterator csdid = csd.begin();
  for(std::vector<unsigned int>::const_iterator cid = c.begin(); cid != c.end(); ++cid, ++csdid) {
    assert(csdid != csd.end());
    if (*cid > *csdid)
      return true;
  }

  return false;
}
