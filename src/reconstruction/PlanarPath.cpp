/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include <algorithm>

#include "PlanarPath.h"

using namespace Taglut;

PlanarMapping::PlanarMapping() : Plane3D(Coord3D(), Coord3D()) { }

PlanarMapping::PlanarMapping(const Coord3D & point,
			     const Coord3D & vector) : Plane3D(point, vector) {
  setAxis();
}

PlanarMapping::PlanarMapping(const Plane3D & plane) : Plane3D(plane) {
  setAxis();
}

Coord2D PlanarMapping::get2DCoords(const Coord3D & coords) const {
  assert(axisx.norm() != 0.);
  assert(axisy.norm() != 0.);
  return Coord2D(Coord3D::scalarProduct(axisx,
					getOrthogonalProjection(coords) - getPoint()),
		 Coord3D::scalarProduct(axisy,
					getOrthogonalProjection(coords) - getPoint()));
}

Coord3D PlanarMapping::get3DCoords(const Coord2D & coords) const {
  return getPoint() + axisx * coords.get2DX() + axisy * coords.get2DY();
}

Path3D PlanarMapping::convert(const Path2D & coords) const {
  std::vector<Coord3D> result;

  for(Path2D::const_iterator c = coords.begin(); c != coords.end(); ++c)
    result.push_back(get3DCoords(*c));
  return result;
}



Path2D PlanarMapping::convert(const Path3D & coords) const {
  std::vector<Coord2D> result;

  for(Path3D::const_iterator c = coords.begin(); c != coords.end(); ++c) {
    result.push_back(get2DCoords(*c));
  }
  return result;
}

void PlanarMapping::setAxis() {
  // compute two orthogonal vector in the plane (local coordinate system)
  // using 3 non-alignated points, then by projecting them in the plane
  // and by selecting the best one to build the first vector.
  double d = 0.;
  axisx = Coord3D(0., 0., 0.);
  for(unsigned char i = 0; i != 3; ++i) {
    Coord3D pt(i == 0 ? 1. : 0,
	       i == 1 ? 1. : 0,
	       i == 2 ? 1. : 0);
    pt = getOrthogonalProjection(pt);
    const double dd = pt.distance(getPoint());
    if (dd > d) {
      d = dd;
      axisx = (pt - getPoint()).normalize();
    }
  }
  assert((axisx.norm() - 1.) < 1e-7);
  axisy = Coord3D::vectorialProduct(getVector(), axisx).normalize();
  assert((axisy.norm() - 1.) < 1e-7);
}


PlanarMapping & PlanarMapping::operator=(const PlanarMapping & pm) {
  Plane3D::operator=(pm);
  axisx = pm.axisx;
  axisy = pm.axisy;
  return *this;
}


Path3DPlanar::Path3DPlanar() {

}

Path3DPlanar::Path3DPlanar(const Path3D & p) : PlanarMapping(p.getPlanarEstimation()), Path2D(PlanarMapping::convert(p)) {
  if (!p.isPlanar()) {
    std::cout << "Warning: non planar path" << std::endl;
  }

}


Path3DPlanar::Path3DPlanar(const Path3DPlanar & p) : PlanarMapping(p), Path2D(p) {

}


MultiPath3DPlanar::MultiPath3DPlanar() {
}

MultiPath3DPlanar::MultiPath3DPlanar(const MultiPath3DPlanar & mp) : PlanarMapping(mp),
								     std::vector<Path2D>(mp) {

}


MultiPath3DPlanar & MultiPath3DPlanar::addPath(const Path3DPlanar & path) {
  if (nbPaths() == 0) {
    PlanarMapping::operator=(path);
    push_back(path);
  }
  else
    push_back(convert(path.getPath3D()));
  return *this;
}

bool MultiPath3DPlanar::isInside(unsigned int id1, unsigned int id2) const {
  assert(id1 != id2);
  assert(id1 < nbPaths());
  assert(id2 < nbPaths());
  return ((*this)[id1].isInside((*this)[id2].front()));
}

bool MultiPath3DPlanar::isInsideObject(const Coord2D & coord) const {
  unsigned int nbInclusions = 0;
  for(MultiPath3DPlanar::const_iterator p = std::vector<Path2D>::begin(); p != std::vector<Path2D>::end(); ++p)
    if ((*p).isInside(coord)) {
      ++nbInclusions;
    }
  return nbInclusions % 2 == 1;
}


std::vector<Path3D> MultiPath3DPlanar::getPaths() const {
  std::vector<Path3D> result;
  for(MultiPath3DPlanar::const_iterator p = std::vector<Path2D>::begin(); p != std::vector<Path2D>::end(); ++p)
    result.push_back(convert(*p));
  return result;
}

Coord3D MultiPath3DPlanar::get3DPoint(unsigned int pathid, unsigned int pointid) const {
  assert(pathid < nbPaths());
  assert(pointid < (*this)[pathid].size());
  return get3DCoords((*this)[pathid][pointid]);
}


Path3D MultiPath3DPlanar::getPath(unsigned int id) const {
  if (id >= nbPaths())
    throw Exception("getPath(1): id out of bound");
  return convert((*this)[id]);
}

void MultiPath3DPlanar::insert(Layer & layer, unsigned int id) const {
  std::vector<std::vector<Layer>::iterator> contained;

  // if the new path is inside an existing one
  for(std::vector<Layer>::iterator c = layer.children_begin();
      c != layer.children_end(); ++c)
    if (isInside((*c).getId(), id)) {
      insert((*c), id);
      return;
    }

  // else, check for the old ones who are inside the new one
  for(std::vector<Layer>::iterator c = layer.children_begin();
      c != layer.children_end(); ++c)
    if (isInside(id, (*c).getId())) {
      contained.push_back(c);
    }
  // create the new layer
  Layer l(true, id);
  if (contained.size() != 0) {
    // add inside the new layer the contained paths
    for(std::vector<std::vector<Layer>::iterator>::const_iterator cc = contained.begin();
	cc != contained.end(); ++cc) {
      l.addChild(**cc);
    }
    // then remove them from the structure
    std::reverse(contained.begin(), contained.end());
    for(std::vector<std::vector<Layer>::iterator>::const_iterator cc = contained.begin();
	cc != contained.end(); ++cc)
      layer.removeChildren(*cc);
  }
  // finally, add the new l inside the structure
  layer.addChild(l);
}

Layer MultiPath3DPlanar::getInclusions() const {
  Layer result;

  const unsigned int nb = nbPaths();
  if (nb != 0) {
    result.addChild(0);
    assert(result.size() == 1);
    for(unsigned int i = 1; i != nb; ++i) {
      insert(result, i);
      assert(result.size() == i + 1);
    }
  }
  assert(result.size() == nbPaths());
  return result;
}
