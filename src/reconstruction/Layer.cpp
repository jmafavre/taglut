/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include "Layer.h"
#include <string>
#include <iostream>

using namespace Taglut;

unsigned int Layer::size() const {
  unsigned int result = children.size();

  for(std::vector<Layer>::const_iterator c = children.begin();
      c != children.end(); ++c)
    result += (*c).size();
  return result;
}

Layer & Layer::merge(const Layer & layer) {
  assert(layer.id == id);
  assert(layer.cavity == cavity);
  for(std::vector<Layer>::const_iterator l = layer.children.begin();
      l != layer.children.end(); ++l) {
    bool inserted = false;
    for(std::vector<Layer>::iterator ll = children.begin();
	ll != children.end(); ++ll)
      if ((*l).id == (*ll).id) {
	inserted = true;
	(*ll).merge(*l);
      }
    if (!inserted)
      children.push_back(*l);
  }
  return *this;
}

LayerIterator Layer::begin() {
  return LayerIterator(*this);
}

LayerIterator Layer::end() {
  return LayerIterator(*this, false);
}

LayerIterator::LayerIterator(Layer & layer, bool begin) {
  if (begin) {
    current = layer.children.begin();
  }
  else {
    current = layer.children.end();
  }
}

LayerIterator::LayerIterator(const LayerIterator & li) : current(li.current),
							 ancestors(li.ancestors) {
}

LayerIterator & LayerIterator::operator=(const LayerIterator & li) {
  current = li.current;
  ancestors = li.ancestors;
  return *this;
}

LayerIterator & LayerIterator::operator++() {

  if ((*current).children.size() != 0) {
    ancestors.push_back(current);
    current = (*current).children.begin();
  }
  else
    ++current;

  while((ancestors.size() != 0) && (current == (*(ancestors.back())).children.end())) {
    current = ancestors.back() + 1;
    ancestors.pop_back();
  }

  return *this;
}

bool LayerIterator::operator!=(const LayerIterator & li) const {
  return ((ancestors.size() != li.ancestors.size()) ||
	  (current != li.current));
}

Layer & LayerIterator::operator*() {
  return *current;
}


Layer & Layer::addChild(const Layer & l) {
  assert(!l.isAmbientSpace());
  children.push_back(l);
  return children.back().setCavity(!cavity);
}

Layer & Layer::setCavity(bool c) {
  cavity = c;
  for(std::vector<Layer>::iterator cc = children.begin();
      cc != children.end(); ++cc)
    (*cc).setCavity(!c);
  return *this;
}

Layer & Layer::mergeSimilarLayers() {

  // call the method iteratively
  for(std::vector<Layer>::iterator cc = children.begin();
      cc != children.end(); ++cc)
    (*cc).mergeSimilarLayers();

  // merge similar children
  for(std::vector<Layer>::iterator c = children.begin();
      c != children.end(); ++c) {
    std::vector<Layer> similarChildren;
    for(std::vector<Layer>::iterator cc = c + 1;
	cc != children.end(); )
      if ((*c).getId() == (*cc).getId()) {
	similarChildren.insert(similarChildren.begin(), (*cc).children.begin(), (*cc).children.end());
	cc = children.erase(cc);
      }
      else
	++cc;
    (*c).addChildren(similarChildren);
  }

  // merge with similar children
  std::vector<Layer> newChildren;
  for(std::vector<Layer>::iterator c = children.begin();
      c != children.end();)
    if ((*c).getId() == id) {
      newChildren.insert(newChildren.end(), (*c).children.begin(), (*c).children.end());
      c = children.erase(c);
    }
    else
      ++c;
  addChildren(newChildren);


  return *this;
}
