/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MERGE_DESCRIPTOR
#define MERGE_DESCRIPTOR

#include <string>
#include <vector>
#include <assert.h>

#include "SceneDescriptor.h"
#include "MergeStep.h"

namespace Taglut {


  /** a class that describe a merging state of structures */
  class MergeDescriptor : public SceneDescriptor {
  protected:
    //! list of the objects (id and genus) taking part of this object
    std::vector<std::pair<unsigned int, unsigned int> > objects;
    //! list of merging steps
    std::vector<MergeStep> mSteps;

    /** return the number of self connection during the given
	merging step */
    unsigned int getNbConnections(const MergeStep & ms) const;

    /** return true if the current structure has to be connected
	during the given merging step */
    bool connected(const MergeStep & ms) const;

    /** merge the given structure inside the given one */
    MergeDescriptor & merge(const MergeDescriptor & md);

    /** return true if the given merge step was applied previously
	on this structure */
    bool wasApplied(const MergeStep & mstep) const;

    /** add the given merge step, only if it wasn't added before */
    inline MergeDescriptor & addMergeStep(const MergeStep & mstep) {
      if (!wasApplied(mstep)) {
	mSteps.push_back(mstep);
      }
      return *this;
    }

    /** internal conversion for children */
    inline MergeDescriptor & toMD(const std::vector<SceneDescriptor *>::iterator & it) {
#ifndef NDEBUG
      MergeDescriptor * md = NULL;
      md = dynamic_cast<MergeDescriptor *>(*it);
      assert(md != NULL);
#endif
      return *static_cast<MergeDescriptor *>(*it);
    }

    /** internal conversion for children */
    inline const MergeDescriptor & toMD(const std::vector<SceneDescriptor *>::const_iterator & it) const {
#ifndef NDEBUG
      MergeDescriptor * md = NULL;
      md = dynamic_cast<MergeDescriptor *>(*it);
      assert(md != NULL);
#endif
      return *static_cast<MergeDescriptor *>(*it);
    }

    void mergeInternal(MergeStep & op);

    bool removeFromMS(MergeStep & ms) const;

    unsigned int removeAllButOneFromMS(MergeStep & ms) const;

    inline std::vector<SceneDescriptor *> & getChildren() { return children; }

  public:
    /** constructor (origin)  */
    MergeDescriptor() : SceneDescriptor() { }

    /** constructor */
    MergeDescriptor(const SceneDescriptor & sd) : SceneDescriptor(sd.isOrigin(), sd.getGenus()) {
      for(std::vector<SceneDescriptor *>::const_iterator c = sd.begin(); c != sd.end(); ++c)
	children.push_back(new MergeDescriptor (toMD(c)));
    }

    /** copy constructor */
    MergeDescriptor(const MergeDescriptor & sd) : SceneDescriptor(sd.origin, sd.genus),
						  objects(sd.objects), mSteps(sd.mSteps) {
      for(std::vector<SceneDescriptor *>::const_iterator c = sd.begin(); c != sd.end(); ++c)
	children.push_back(new MergeDescriptor (toMD(c)));
    }

    virtual ~MergeDescriptor() {
      clear();
    }

#ifndef SWIG
    /** copy operator */
    MergeDescriptor & operator=(const MergeDescriptor & md) {
      objects.clear();
      objects.insert(objects.begin(), md.objects.begin(), md.objects.end());
      mSteps.clear();
      mSteps.insert(mSteps.begin(), md.mSteps.begin(), md.mSteps.end());
      origin = md.origin;
      genus = md.genus;
      children.clear();
      for(std::vector<SceneDescriptor *>::const_iterator c = md.begin(); c != md.end(); ++c)
	children.push_back(new MergeDescriptor (toMD(c)));
      return *this;
    }
#endif

    virtual SceneDescriptor & addSDChild(unsigned int genus_) {
      children.push_back(new MergeDescriptor(genus_));
      return *(children.back());
    }

    /** add a child in the current structure */
    virtual SceneDescriptor & addSDChild(const SceneDescriptor & l) {
      children.push_back(new MergeDescriptor(l));
      return *(children.back());
    }

    /** add an object in the current structure */
    virtual MergeDescriptor & addObject(unsigned int id, unsigned int genus) {
      if (origin)
	origin = false;
      genus += genus;
      objects.push_back(std::pair<unsigned int, unsigned int>(id, genus));
      return *this;
    }
    /** add a child in the current structure */
    virtual MergeDescriptor & addMDChild(unsigned int genus_) {
      MergeDescriptor * md = new MergeDescriptor(genus_);
      children.push_back(md);
      return *md;
    }

    /** add a child in the current structure */
    virtual MergeDescriptor & addMDChild(const MergeDescriptor & m) {
      assert(!m.isOrigin());
      MergeDescriptor * md = new MergeDescriptor(m);
      children.push_back(md);
      return *md;
    }

    /** apply the merging step given in parameter */
    MergeDescriptor & merge(const MergeStep & op);

    /** return a string description of the current merge descriptor */
    std::string toString(const std::string & prefix = "") const;

  };
}
#endif
