/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH_BY_SLICES
#define MESH_BY_SLICES

#include <vector>
#include <set>

#include "CImgUse.h"
#include "Path3D.h"
#include "PlanarPath.h"
#include "Coord3D.h"
#include "Layer.h"
#include "SceneDescriptor.h"
#include "MergeDescriptor.h"

namespace Taglut {

  /** a class containing a slice-by-slice description of a mesh, for reconstruction */
  class MeshBySlices {
  private:
    /** polygons grouped slices by slices, and direction by direction */
    std::vector<MultiPath3DPlanar> slices[3];
    /** direction of the 3 slices sets */
    Coord3D directions[3];

    /** epsilon value used to estimate similarities between slices (distances between points) */
    double error;

    /** epsilon value used to estimate similarities between slices (angles) */
    double errorAngles;

    /** epsilon value used to estimate similarities between slices (distances between point and plane) */
    double errorPlanes;


    /** a class that describe paths in the structure. Can be used as an interator on the structure */
    class PathDescriptor {
    private:
      /** the current object */
      const MeshBySlices * meshBS;
      /** direction ID */
      unsigned char directionid;
      /** plane ID in the direction */
      unsigned int planeid;
      /** path ID in the plane */
      unsigned int pathid;

    public:
      /** create an invalid descriptor (outside of the structure) */
      PathDescriptor(const MeshBySlices & mbs);
      /** constructor */
      PathDescriptor(const MeshBySlices & mbs, unsigned char direction, unsigned int plane, unsigned int path);
      /** copy constructor */
      PathDescriptor(const PathDescriptor & pdesc);
      /** accessor */
      inline unsigned char getDirection() const { return directionid; }
      /** accessor */
      inline unsigned int getPlane() const { return planeid; }
      /** accessor */
      inline unsigned int getPath() const { return pathid; }
      /** increment operator */
      PathDescriptor & operator++();
      /** get the first path descriptor */
      static PathDescriptor firstPath(const MeshBySlices & mbs);

      /** return true if the two paths are in a parallel plane */
      inline bool isParallel(const PathDescriptor & pdesc) const {
	return directionid == pdesc.directionid;
      }
      /** return true if the two paths are in the same plane */
      inline bool samePlane(const PathDescriptor & pdesc) const {
	return isParallel(pdesc) && (planeid == pdesc.planeid);
      }
      /** comparison operator */
      inline bool operator==(const PathDescriptor & pdesc) const {
	return !operator!=(pdesc);
      }
      /** comparison operator */
      inline bool operator!=(const PathDescriptor & pdesc) const {
	return (directionid != pdesc.directionid) || (planeid != pdesc.planeid) || (pathid != pdesc.pathid);
      }

      /** return true if the current descriptor is well defined */
      inline bool isValid() const {
	return directionid != 3;
      }
    };

    /** return true if the given path descriptor is valid, i.e. if the described path exists in the current structure */
    bool isValid(const PathDescriptor & pdesc) const;

    class CrossingPoint {
    private:
      /** the current object */
      const MeshBySlices * meshBS;
      std::vector<std::pair<PathDescriptor, unsigned int> > points;
      Coord3D center;

      bool cDirections[3];

      CrossingPoint & updateCenter() {
	center = Coord3D();
	for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p)
	  center += (*meshBS).getPoint((*p).first, (*p).second);
	if (points.size() != 0)
	  center /= points.size();
	return *this;
      }
    public:
      /** default constructor */
      CrossingPoint(const MeshBySlices & mbs) : meshBS(&mbs) {
	cDirections[0] = cDirections[1] = cDirections[2] = false;
      }

      /** constructor with a point */
      CrossingPoint(const MeshBySlices & mbs,
		    const PathDescriptor & pdesc, unsigned int id,
		    unsigned char cDir) : meshBS(&mbs) {
	cDirections[0] = cDirections[1] = cDirections[2] = false;
	addPoint(pdesc, id, cDir);
      }

      /** copy constructor */
      CrossingPoint(const CrossingPoint & cp) : meshBS(cp.meshBS), points(cp.points),
						center(cp.center) {
	cDirections[0] = cp.cDirections[0];
	cDirections[1] = cp.cDirections[1];
	cDirections[2] = cp.cDirections[2];
      }
      /** get coordinates of the crossing point */
      inline Coord3D getCoord() const {
	return center;
      }
      /** add a point in this crossing. The point is described by a path
	  descriptor, an id in the path. \p cDir corresponds to the other direction.
	  If cDir == pdesc.getDirection(), the crossing corresponds to the two others directions.  */
      CrossingPoint & addPoint(const PathDescriptor & pdesc, unsigned int id,
			       unsigned char cDir) {
#ifndef NDEBUG
	for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p) {
	  assert((*p).first != pdesc);
	  assert((*p).first.getDirection() != pdesc.getDirection());
	}
#endif
	points.push_back(std::pair<PathDescriptor, unsigned int>(pdesc, id));
	if (cDir == pdesc.getDirection()) {
	  cDirections[0] = cDirections[1] = cDirections[2] = true;
	}
	else {
	  cDirections[pdesc.getDirection()] = true;
	  cDirections[cDir] = true;
	}
	return updateCenter();
      }

      CrossingPoint & addDirection(unsigned char c) {
	assert(c < 3);
	cDirections[c] = true;
	return *this;
      }
      /** return the maximum distance between a point of this structure and its isobarycenter */
      double getError() const {
	const Coord3D center = getCoord();
	double result = 0.;
	for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p) {
	  const double d = (*meshBS).getPoint((*p).first, (*p).second).distance(center);
	  if (d > result)
	    result = d;
	}
	return result;
      }
      /** return true if all the contained directions have a corresponding path */
      bool valid() const {
	bool contained[3] = { false, false, false};
	for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p)
	  contained[(*p).first.getDirection()] = true;
	return ((contained[0] == cDirections[0]) &&
		(contained[1] == cDirections[1]) &&
		(contained[2] == cDirections[2]));
      }

      /** return true if the two cPoints are compatible according to the known directions */
      bool isCompatible(const CrossingPoint & cPoint, double error) const {
	return ((cDirections[0] == cPoint.cDirections[0]) &&
		(cDirections[1] == cPoint.cDirections[1]) &&
		(cDirections[2] == cPoint.cDirections[2]) &&
		(center.distance(cPoint.center) < error));
      }

      /** merge the two crossing points if and only if they are compatible */
      bool softMerge(const CrossingPoint & cPoint, double error) {
	if (isCompatible(cPoint, error)) {
	  merge(cPoint);
	  return true;
	}
	else return false;
      }

      /** merge the two crossing points */
      CrossingPoint & merge(const CrossingPoint & cPoint) {
#ifndef NDEBUG
	for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator p = points.begin(); p != points.end(); ++p)
	  for(std::vector<std::pair<PathDescriptor, unsigned int> >::const_iterator pp = cPoint.points.begin(); pp != cPoint.points.end(); ++pp) {
	    assert((*p).first != (*pp).first);
	    assert((*p).first.getDirection() != (*pp).first.getDirection());
	  }
#endif
	for(unsigned char i = 0; i < 3; ++i)
	  cDirections[i] = cDirections[i] || cPoint.cDirections[i];
	points.insert(points.end(), cPoint.points.begin(), cPoint.points.end());
	return updateCenter();
      }

      /** return true if the current crossing point corresponds to the given path */
      bool contains(const PathDescriptor & pdesc) const;

      /** assuming that the current crossing point corresponds to the given path, it return the id of
	  the crossing point on this path */
      unsigned int getPoint(const PathDescriptor & pdesc) const;

      /** get a string description of the current object */
      std::string toString() const;
    };

    /** a set of coordinates */
    typedef std::vector<CrossingPoint> VCPoint;

    friend class PathDescriptor;
    friend class CrossingPoint;

    typedef std::vector<PathDescriptor> VPathDescriptor;

    typedef std::vector<std::pair<VPathDescriptor, VCPoint> > VVPathDescriptor;

    /** this structure describe the connected components of the current object */
    VVPathDescriptor connectedComponents;

    /** get the first path of the structure */
    PathDescriptor begin() const;

    /** get the end of the structure */
    PathDescriptor end() const;

    /** return an iterator on the given path */
    Path2D::iterator begin(const PathDescriptor & pdesc);

    /** return an iterator on the given path */
    Path2D::iterator end(const PathDescriptor & pdesc);

    /** return an iterator on the given path */
    Path2D::const_iterator begin(const PathDescriptor & pdesc) const;

    /** return an iterator on the given path */
    Path2D::const_iterator end(const PathDescriptor & pdesc) const;

    /** return the size on the given path */
    unsigned int size(const PathDescriptor & pdesc) const;

    /** number of edges per connected component */
    std::vector<unsigned int> nbEdges;

    /** number of circular edges per connected component */
    std::vector<unsigned int> nbCircularEdges;

    /** a class to describe a subpart of a path contained int the cell */
    class SubPathDescriptor {
    private:
      PathDescriptor pdesc;
      Coord3D cbegin;
      Coord3D cend;
      unsigned int begin;
      unsigned int end;
      unsigned int id;
    public:
      /** constructor */
      SubPathDescriptor(const PathDescriptor & pdesc_,
			Coord3D cbegin_,
			Coord3D cend_,
			unsigned int begin_,
			unsigned int end_,
			unsigned int id_) : pdesc(pdesc_),
					    cbegin(cbegin_),
					    cend(cend_),
					    begin(begin_),
					    end(end_),
					    id(id_) { }
      /** copy constructor */
      SubPathDescriptor(const SubPathDescriptor & spd) : pdesc(spd.pdesc),
							 cbegin(spd.cbegin),
							 cend(spd.cend),
							 begin(spd.begin),
							 end(spd.end),
							 id(spd.id) {
      }

      /** accessor */
      inline unsigned int getBegin() const { return begin; }
      /** accessor */
      inline unsigned int getEnd() const { return end; }

      /** accessor */
      const PathDescriptor & getPathDescriptor() const { return pdesc; }

      /** return true if the current subpath and the given one are connected */
      inline bool isConnected(const SubPathDescriptor & subpath) const {
	return (id == subpath.id) && ((cbegin == subpath.cbegin) || (cbegin == subpath.cend) ||
				      (cend == subpath.cbegin) || (cend == subpath.cend));
      }

      /** accessor */
      unsigned int getId() const { return id; }

      /** get a string description of the current object */
      std::string toString() const;
    };

    /** a class that describe properties of the cells (interslices "cubes") */
    class CellProperties {
    private:
      /** coordinates of the cell's center */
      Coord3D location;
      /** index of the slices before the cell */
      int predSlices[3];
      /** inside flag */
      bool inside;
      /** a set of border subparts */
      std::vector<SubPathDescriptor> borders;
      /** a set of polygons */
      std::vector<std::vector<SubPathDescriptor> > polygons;
    public:
      /** default constructor */
      CellProperties();
      /** constructor using a location and corresponding slices */
      CellProperties(const Coord3D & l, int x, int y, int z);
      /** copy constructor */
      CellProperties(const CellProperties & cp);

      /** accessor */
      inline const Coord3D & getLocation() const { return location; }
      /** accessor */
      inline int getSliceX() const { return predSlices[0]; }
      /** accessor */
      inline int getSliceY() const { return predSlices[1]; }
      /** accessor */
      inline int getSliceZ() const { return predSlices[2]; }

      inline int getSlice(unsigned char sid) const {
	if (sid == 0)
	  return getSliceX();
	else if (sid == 1)
	  return getSliceY();
	else if (sid == 2)
	  return getSliceZ();
	else
	  throw Exception("getSlice(1): id out of bound");
      }

      /** modifier */
      inline void setInside() { inside = true; }
      /** modifier */
      inline void setOutside() { inside = false; }
      /** return true if the current cell is described by the plane where is located the given path */
      inline bool hasPlane(const PathDescriptor & pdesc) const {
	return ((getSlice(pdesc.getDirection()) == (int)pdesc.getPlane()) ||
		(getSlice(pdesc.getDirection()) + 1 == (int)pdesc.getPlane()));
      }
      /** accessor */
      inline bool isInside() const { return inside; }
      /** accessor */
      inline bool isOutside() const { return !inside; }
      /** accessor */
      inline bool isBorder() const { return borders.size() != 0; }

      /** add a path in the cell */
      CellProperties & addPath(const PathDescriptor & pdesc,
			       const Coord3D & rbegin, const Coord3D & rend,
			       unsigned int begin, unsigned int end,
			       unsigned int idCC);

      /** merge the subpaths in polygons */
      CellProperties & computePolygons();

      /** return number of polygons contained in this cell */
      unsigned int getNbPolygons() const;

      /** return number of polygons of the given CC contained in this cell */
      unsigned int getNbPolygons(unsigned int id) const;

      /** return the connected component from which the given polygon is part of */
      unsigned int getCC(unsigned int id) const;

      /** return the wanted polygon */
      const std::vector<SubPathDescriptor> & getPolygon(unsigned int id) const {
	assert(id < polygons.size());
	return polygons[id];
      }
    };

    friend class CellProperties;

    /** interslice cells with the corresponding properties */
    std::vector<std::vector<std::vector<CellProperties> > > intersliceCells;

#ifndef SWIG

    class CellIteratorConst;

    /** an iterator throw the cells  */
    class CellIterator {
    private:
      std::vector<std::vector<std::vector<CellProperties> > > * ii;
      std::vector<std::vector<std::vector<CellProperties> > >::iterator cit;
      std::vector<std::vector<CellProperties> >::iterator ccit;
      std::vector<CellProperties>::iterator cccit;
    public:
      /** default constructor */
      CellIterator(std::vector<std::vector<std::vector<CellProperties> > > & iCell);
      /** constructor using iterator */
      CellIterator(std::vector<std::vector<std::vector<CellProperties> > > & iCell,
		   const std::vector<std::vector<std::vector<CellProperties> > >::iterator & cit_,
		   const std::vector<std::vector<CellProperties> >::iterator & ccit_,
		   const std::vector<CellProperties>::iterator & cccit_);
      /** copy constructor */
      CellIterator(const CellIterator & cp);
      /** increment operator*/
      inline CellIterator & operator++() {
	if (cccit == (*ccit).end())
	  return *this;
	++cccit;
	if (cccit == (*ccit).end()) {
	  ++ccit;
	  if (ccit == (*cit).end()) {
	    ++cit;
	    if (cit != (*ii).end()) {
	      ccit = (*cit).begin();
	    }
	  }
	  if ((cit != (*ii).end()) && (ccit != (*cit).end())) {
	    cccit = (*ccit).begin();
	  }
	}
	return *this;
      }
      /** comparison operator */
      inline bool operator==(const CellIterator & ci) const {
	return (cit == ci.cit) && (ccit == ci.ccit) && (cccit == ci.cccit);
      }
      /** comparison operator */
      inline bool operator!=(const CellIterator & ci) const {
	return (cit != ci.cit) || (ccit != ci.ccit) || (cccit != ci.cccit);
      }
      /** accessor operator */
      inline CellProperties & operator*() const {
	return *cccit;
      }

      friend class CellIteratorConst;
    };

    /** get an iterator on the cells of this structure */
    inline CellIterator begin_cell() {
      return CellIterator(intersliceCells);
    }

    /** get an iterator on the cells of this structure */
    inline CellIterator end_cell() {
      return CellIterator(intersliceCells, intersliceCells.end(), intersliceCells.back().end(), intersliceCells.back().back().end());
    }


    /** an iterator throw the cells  */
    class CellIteratorConst {
    private:
      const std::vector<std::vector<std::vector<CellProperties> > > * ii;
      std::vector<std::vector<std::vector<CellProperties> > >::const_iterator cit;
      std::vector<std::vector<CellProperties> >::const_iterator ccit;
      std::vector<CellProperties>::const_iterator cccit;
    public:
      /** default constructor */
      CellIteratorConst(const std::vector<std::vector<std::vector<CellProperties> > > & iCell);
      /** constructor using iterator */
      CellIteratorConst(const std::vector<std::vector<std::vector<CellProperties> > > & iCell,
		   const std::vector<std::vector<std::vector<CellProperties> > >::const_iterator & cit_,
		   const std::vector<std::vector<CellProperties> >::const_iterator & ccit_,
		   const std::vector<CellProperties>::const_iterator & cccit_);
      /** copy constructor */
      CellIteratorConst(const CellIteratorConst & cp);
      /** constructor using a non-const iterator */
      CellIteratorConst(const CellIterator & cp);
      /** increment operator*/
      inline CellIteratorConst & operator++() {
	if (cccit == (*ccit).end())
	  return *this;
	++cccit;
	if (cccit == (*ccit).end()) {
	  ++ccit;
	  if (ccit == (*cit).end()) {
	    ++cit;
	    if (cit != (*ii).end())
	      ccit = (*cit).begin();
	  }
	  if ((cit != (*ii).end()) && (ccit != (*cit).end())) {
	    cccit = (*ccit).begin();
	  }
	}
	return *this;
      }
      /** comparison operator */
      inline bool operator==(const CellIteratorConst & ci) const {
	return (cit == ci.cit) && (ccit == ci.ccit) && (cccit == ci.cccit);
      }
      /** comparison operator */
      inline bool operator!=(const CellIteratorConst & ci) const {
	return (cit != ci.cit) || (ccit != ci.ccit) || (cccit != ci.cccit);
      }
      /** accessor operator */
      inline const CellProperties & operator*() const {
	return *cccit;
      }

    };

    /** get an iterator on the cells of this structure */
    inline CellIteratorConst const_begin_cell() const {
      return CellIteratorConst(intersliceCells);
    }

    /** get an iterator on the cells of this structure */
    inline CellIteratorConst const_end_cell() const {
      return CellIteratorConst(intersliceCells, intersliceCells.end(), intersliceCells.back().end(), intersliceCells.back().back().end());
    }

#endif // SWIG

    /** return the multipath structure that contains the path described by the given descriptor */
    inline const MultiPath3DPlanar & getPlane(const PathDescriptor & pdesc) const {
      if (!pdesc.isValid())
	throw Exception("getPlane(1): bad descriptor");
      return slices[pdesc.getDirection()][pdesc.getPlane()];
    }

    /** return the multipath structure that contains the path described by the given descriptor */
    inline MultiPath3DPlanar & getPlane(const PathDescriptor & pdesc) {
      if (!pdesc.isValid())
	throw Exception("getPlane(1): bad descriptor");
      return slices[pdesc.getDirection()][pdesc.getPlane()];
    }

    /** get the path corresponding to the given description */
    Path3D getPath(const PathDescriptor & pdesc) const;

    /** return the point in the given path, at the given location */
    Coord3D getPoint(const PathDescriptor & pdesc, unsigned int pointid) const;

    static bool hasCommonPoint(const VCPoint & cs1, const VCPoint & cs2, double epsilon);

    /** add in the first VCPoint the points that are not similar to the existing one, using epsilon as similarity distance */
    static void merge(VCPoint & result, const VCPoint & newCoords, double epsilon);

    /** get the point located at the intersection between the given planes,
	in each direction. */
    Coord3D getIntersection(unsigned int i,
			    unsigned int j,
			    unsigned int k) const;

    /** return the space between the s-st plane in the d-st direction, and
	the next one, as a vector (it gives a direction to walk with). */
    Coord3D getSliceSpace(unsigned int d, unsigned int s) const;

    /** return a point inside the given cell */
    Coord3D getCellPoint(int i, int j, int k) const;

    /** given a direction and a 3D point, it returns the slice's id located on the direction,
	and just before */
    int getSliceIdBefore(unsigned char direction, const Coord3D & point) const;

    /** return true if the point given in paramter (and part of the given
	path descriptor) is contained in another plane ofthe current structure */
    bool inOtherPlane(const Coord3D & point, const PathDescriptor & pdesc) const;

    /** return the cell corresponding to the point given in parameter (that
	is part of the given path descriptor), and in the \p before side of the
	plane. */
    CellProperties & getCell(const Coord3D & point,
			     const PathDescriptor & pdesc,
			     bool before);

    /** return the cell corresponding to the given slices (i, j and k may be -1 if it corresponds
	to a cell before the slices) */
    CellProperties & getCell(int i, int j, int k);

    /** return the cell corresponding to the given slices (i, j and k may be -1 if it corresponds
	to a cell before the slices) */
    const CellProperties & getCell(int i, int j, int k) const;

    /** return true if the given IDs correspond to a real cell. return false if the cell structure is not
	yet created. */
    bool isValidCell(int i, int j, int k) const;

    /** clear the precomputed data */
    void clearData();

    /** add points that corresponds to intersections with the other planes, and return the corresponding crossings */
    VCPoint addIntersectionPoints(const PathDescriptor & pdesc);

    /** assuming that the structure has been updated, it returns the list of points
	in the path that corresponds to the crossing with another plane */
    std::vector<std::pair<unsigned int, Coord3D> > getIntersectionPoints(const PathDescriptor & pdesc);

    /** connected components described with the layer point of view */
    Layer layers;

    /** compute the layer structure of the current object */
    MeshBySlices & computeLayerStructure();

    /** return the inclusions of paths using id of CC. */
    Layer getInclusions(const MultiPath3DPlanar & mplane,
			unsigned int direction,
			unsigned int plane);

    /** computes the inverse directory */
    MeshBySlices & computeCCInverseDirectory();

    /** return the CC corresponding to the given path descriptor */
    unsigned int getCCId(const PathDescriptor & pdesc);

    /** an inverse directory that gives the CC of a given path descriptor */
    std::vector<std::vector<unsigned int> > inverseDirectory[3];

    void toStringCavity(std::ostringstream & str, const Layer & layer,
			const std::string & space);

    /** return a scene descriptor (or subpart) corresponding to the given layer */
    SceneDescriptor getSubSceneDescription(const Layer & layer);

    /** return a merge descriptor (or subpart) corresponding to the given layer */
    MergeDescriptor getInitialSubMergeDescription(const Layer & layer);

    /** return a list of possible merges */
    std::vector<MergeStep> getPossibleMerges();

    /**
       return the best merge description and the corresponding cost,
       starting from the given description, and applying several merging steps given
       in parameter, but not previously used (\p seen). Negative costs corresponds to
       empty or not valid solutions.
       Do not return solutions with values > oldbest.
     */
    std::pair<MergeDescriptor, double>
    getBestMergeDescription(const MergeDescriptor & currentMDesc,
			    double currentCost,
			    const SceneDescriptor & target,
			    double oldbest = std::numeric_limits<double>::max(),
			    unsigned int start = 0);

    /** init tmp data */
    void initMergeTmpData(const std::vector<MergeStep> & pMerges,
			   MergeStepCost & msc);
    /** clear tmp data */
    void clearMergeTmpData();
    /** tmp data */
    MergeStepCost * t_msc;
    std::vector<MergeStep> t_pMerges;
    std::vector<std::vector<MergeStep> > t_pSubMerges;

    /** return list of points in a polygon */
    std::vector<Coord3D> getPoints(const std::vector<SubPathDescriptor> & polygon) const;
  public:
    /** default constructor */
    MeshBySlices(double epsilon = 1e-4, double epsilonAngle = 1e-2, double epsilonPlane = 1e-6);

    /** constructor loading the polygons from a file, then creating the corresponding structure */
    MeshBySlices(const std::string & filename, double epsilon = 1e-4, double epsilonAngle = 1e-2, double epsilonPlane = 1e-6);

    /** build the current structure using a set of polygons */
    MeshBySlices(const std::vector<Path3DPlanar> & polygons, double epsilon = 1e-4, double epsilonAngle = 1e-2, double epsilonPlane = 1e-6);

    /** build the current structure using a set of polygons */
    MeshBySlices(const std::vector<Path3D> & polygons, double epsilon = 1e-4, double epsilonAngle = 1e-2, double epsilonPlane = 1e-6);

    /** copy constructorr */
    MeshBySlices(const MeshBySlices & mbs);

    /** add a path in the structure */
    MeshBySlices & addPath(const Path3DPlanar & path);

    /** add a path in the structure */
    inline MeshBySlices & addPath(const Path3D & path) {
      return addPath(Path3DPlanar(path));
    }

    /** add a set of paths in the current structure */
    inline MeshBySlices & addPaths(const std::vector<Path3DPlanar> & paths) {
      for(std::vector<Path3DPlanar>::const_iterator p = paths.begin(); p != paths.end(); ++p)
	addPath(*p);
      return *this;
    }

    /** add a set of paths in the current structure */
    inline MeshBySlices & addPaths(const std::vector<Path3D> & paths) {
      for(std::vector<Path3D>::const_iterator p = paths.begin(); p != paths.end(); ++p)
	addPath(*p);
      return *this;
    }

    /** Group paths by connected components */
    MeshBySlices & computeCC();

    /** return the number of connected components */
    inline unsigned int getNbCC() {
      if (connectedComponents.size() == 0)
	computeCC();
      return connectedComponents.size();
    }

    /** compute for each interslice "cube" the properties (inside/outside/border, paths, ...) */
    MeshBySlices & computeIntersliceProperties();

    /** create an image using the interslice properties (2: inside, 0: outside, 1: border) */
    UCharCImg getIntersliceImage();

    /** return the list of paths */
    std::vector<Path3D> getPaths() const;

    /** return a synthetic description of the current structure */
    std::string toString() const;

    /** return a synthetic description of the connected components in the
	current structure */
    std::string toStringCC();

    /** return a synthetic description of the inclusions */
    std::string toStringCavities();

    /** return the bounded box of the given CC */
    Box3D getBoundedBox(unsigned int id);

    /** return the number of polygons in cells */
    unsigned int getNbPolygons();

    /** return the number of polygons of the given CC in cells */
    unsigned int getNbPolygons(unsigned int id);

    /** return the number of edges of the structure (vertices are in the intersection of the planes) */
    unsigned int getNbEdges();

    /** return the number of edges of the structure (vertices are in the intersection of the planes), of the given CC */
    unsigned int getNbEdges(unsigned int id);

    /** return the number of circular edges of the structure (vertices are in the intersection of the planes), of the given CC */
    unsigned int getNbCircularEdges(unsigned int id);

    /** return the number of vertices of the structure (vertices are in the intersection of the planes along the paths), of the given CC */
    unsigned int getNbPoints(unsigned int id);

    /** return the number of vertices of the structure (vertices are in the intersection of the planes along the paths) */
    unsigned int getNbPoints();

    /** return the Euler caracteristic of the given CC */
    inline int getEulerCaracteristic(unsigned int id) {
      return (int)getNbPoints(id) +getNbCircularEdges(id) - getNbEdges(id) + getNbPolygons(id);
    }

    /** return the genus of the given CC, assuming that it's without boundary */
    inline unsigned int getGenus(unsigned int id) {
      int ec = getEulerCaracteristic(id);
      //assert(ec % 2 == 0);
      return (2 - ec) / 2;
    }

    /** return a description of the scene (genus of objects, and inclusion structure) */
    SceneDescriptor getSceneDescription();

    /** return a description of the scene (genus and ids of objects, and inclusion structure),
	that can be modified by merging steps */
    MergeDescriptor getInitialMergeDescription();

    /** return location of the isobarycenter */
    Coord3D getIsoBarycenter() const;

    /** return a description of the merged that have to be performed in order to obtain
     a scene description as given in parameter */
    MergeDescriptor getMergeDescription(const SceneDescriptor & sd, MergeStepCost & msc);

    /** return a description of the merged that have to be performed in order to obtain
     a scene description as given in parameter */
    inline MergeDescriptor getMergeDescription(const std::string & sd, MergeStepCost & msc) {
      return getMergeDescription(SceneDescriptor(sd), msc);
    }

    /** return true if the given scene descriptor is a possible
	merged scene. \see SceneDescriptor::isPossibleAncestor */
    inline bool isPossibleMergedScene(const SceneDescriptor & sd) {
      return !sd.isUnreachableFrom(getSceneDescription());
    }

    /** return true if the given scene descriptor is a possible
	merged scene. \see SceneDescriptor::isPossibleAncestor */
    inline bool isPossibleMergedScene(const std::string & sd) {
      return !SceneDescriptor(sd).isUnreachableFrom(getSceneDescription());
    }

    /** assuming that the polygon structure has been computed,
	it returns the Hausdorff distance between the two polygons id1 and id2
	in the cell (x, y, z) */
    double hausdorffDistance(int x, int y, int z, unsigned int id1, unsigned int id2) const;

  };

}

#endif
