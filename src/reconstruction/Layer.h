/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LAYER
#define LAYER

#include <vector>
#include <assert.h>

namespace Taglut {

  class LayerIterator;

  /** a class that describe the global structures, i.e. cavities and inclusions */
  class Layer {
  private:
    int id;
    std::vector<Layer> children;
    bool cavity;

    /** modifier */
    Layer & setCavity(bool c);
  public:
    /** constructor. id = -1 means the ambient space.
	\param c A boolean indicating if the current layer is a cavity
    */
    Layer(bool c = true, int id_ = -1) : id(id_), cavity(c) { }

    /** copy constructor */
    Layer(const Layer & layer) : id(layer.id), children(layer.children), cavity(layer.cavity) {
    }

    /** add a child in the current structure */
    inline Layer & addChild(unsigned int id_) {
      children.push_back(Layer(!cavity, (int)id_));
      return children.back();
    }

    /** add a child in the current structure */
    Layer & addChild(const Layer & l);

    /** add children in the current structure */
    inline Layer & addChildren(const std::vector<Layer> & ls) {
      for(std::vector<Layer>::const_iterator l = ls.begin(); l != ls.end(); ++l)
	addChild(*l);
      return *this;
    }

    inline Layer & removeChildren(const std::vector<Layer>::iterator & it) {
      children.erase(it);
      return *this;
    }

    /** accessor */
    inline const std::vector<Layer> & getChildren() const { return children; }
    /** accessor */
    inline bool isCavity() const { return cavity; }
    /** accessor */
    inline bool isAmbientSpace() const { return id == -1; }
    /** accessor */
    inline int getId() const { return id; }

    /** modifier */
    inline Layer & setId(int i) { id = i; return *this; }

    /** reset the current structure assuming that it corresponds to the ambient space */
    inline Layer & clear() {
      assert(id == -1);
      children.clear();
      return *this;
    }

    /** return true if the current layer do not contains another one */
    inline bool isEmpty() const {
      return children.size() == 0;
    }

    /** merge the two structures */
    Layer & merge(const Layer & layer);

    /** merge children with same id */
    Layer & mergeSimilarLayers();

    /** return the number of contained layers */
    unsigned int size() const;

    friend class LayerIterator;

    /** an iterator (DFS) */
    LayerIterator begin();

    /** an iterator (DFS) */
    LayerIterator end();

    /** an iterator on children */
    inline std::vector<Layer>::const_iterator children_begin() const {
      return children.begin();
    }

    /** an iterator on children */
    inline std::vector<Layer>::const_iterator children_end() const {
      return children.end();
    }

    /** an iterator on children */
    inline std::vector<Layer>::iterator children_begin() {
      return children.begin();
    }

    /** an iterator on children */
    inline std::vector<Layer>::iterator children_end() {
      return children.end();
    }
  };

  class LayerIterator {
  private:
    std::vector<Layer>::iterator current;
    std::vector<std::vector<Layer>::iterator> ancestors;
  public:
    /** constructor */
    LayerIterator(Layer & layer, bool begin = true);

    /** copy constructor */
    LayerIterator(const LayerIterator & li);

    /** copy operator */
    LayerIterator & operator=(const LayerIterator & li);

    /** incrementation */
    LayerIterator & operator++();

    /**comparison operator */
    bool operator!=(const LayerIterator & li) const;

    /** get the layer corresponding to the current iterator */
    Layer & operator*();
  };
}

#endif
