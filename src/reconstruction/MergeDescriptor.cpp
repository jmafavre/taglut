/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>
#include <sstream>
#include <limits>

#include "MergeDescriptor.h"

using namespace Taglut;


void MergeDescriptor::mergeInternal(MergeStep & op) {
  // apply the operator on the children
  for(std::vector<SceneDescriptor *>::iterator sd = begin();
      sd != end(); ++sd) {
    if (op.getNbElementarySteps() == 0)
      break;
    toMD(sd).mergeInternal(op);
 }

  // check for child/grandchild connection
  bool found = true;
  while(found) {
    found = false;
    for(std::vector<SceneDescriptor *>::iterator sd = begin();
	sd != end(); ++sd) {
      if (op.getNbElementarySteps() == 0)
	break;
      if (toMD(sd).connected(op)) {
	// check for an involved grandchild
	for(std::vector<SceneDescriptor *>::iterator gsd = (**sd).begin();
	    gsd != (**sd).end(); ++gsd) {
	  if (toMD(gsd).connected(op)) {
	    found = true;
	    children.insert(children.end(), (**gsd).begin(), (**gsd).end());
	    assert(toMD(sd).connected(op));
	    assert(toMD(gsd).connected(op));
	    (**sd).addGenus((**gsd).getGenus());
	    (**gsd).clear(false);
	    (**sd).removeChild(gsd);
	    toMD(sd).addMergeStep(op);
	    toMD(sd).removeFromMS(op);
	    break;
	  }
	}
	if (found)
	  break;
      }
    }
  }

  // check for child/child connection
  for(std::vector<SceneDescriptor *>::iterator sd = begin();
      sd != end(); ++sd) {
    if (op.getNbElementarySteps() == 0)
      break;
    if (toMD(sd).connected(op)) {
      for(std::vector<SceneDescriptor *>::iterator sd2 = sd + 1;
	  sd2 != end();)
	if (toMD(sd).connected(op)) {
	  toMD(sd).getChildren().insert((**sd).end(), (**sd2).begin(), (**sd2).end());
	  (**sd2).clear(false);
	  (**sd).addGenus((**sd2).getGenus());
	  sd2 = removeChild(sd2);
	}
	else
	  ++sd2;
    }
  }

  // at the end, apply self connexions
  unsigned int removed = removeAllButOneFromMS(op);
  if (removed >= 1) {
    genus += removed;
    addMergeStep(op);
  }

}

MergeDescriptor & MergeDescriptor::merge(const MergeStep & op) {
  MergeStep mstep(op);
  mergeInternal(mstep);
  return *this;
}


bool MergeDescriptor::removeFromMS(MergeStep & ms) const {

  for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator o = objects.begin();
      o != objects.end(); ++o)
    if (ms.removeOne((*o).first))
      return true;

  return false;
}

unsigned int MergeDescriptor::removeAllButOneFromMS(MergeStep & ms) const {
  unsigned int result = 0;
  for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator o = objects.begin();
      o != objects.end(); ++o)
    if (result == 0)
      result += ms.removeAllButOne((*o).first);
    else
      result += ms.removeAll((*o).first);

  return result;
}

unsigned int MergeDescriptor::getNbConnections(const MergeStep & ms) const {
  unsigned int result = 0;

  for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator o = objects.begin();
      o != objects.end(); ++o)
    result += ms.getNbPolygonsFromCC((*o).first);

  return result;
}

bool MergeDescriptor::connected(const MergeStep & ms) const {
  for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator o = objects.begin();
      o != objects.end(); ++o)
    if (ms.isInvolved((*o).first))
      return true;
  return false;
}


MergeDescriptor & MergeDescriptor::merge(const MergeDescriptor & md) {
  assert(!origin);
  assert(!md.origin);
  genus += md.genus;

  objects.insert(objects.end(), md.objects.begin(), md.objects.end());

  for(std::vector<SceneDescriptor *>::const_iterator c = md.begin(); c != md.end(); ++c)
    children.push_back(new MergeDescriptor (toMD(c)));

  return *this;
}

bool MergeDescriptor::wasApplied(const MergeStep & mstep) const {
  for(std::vector<MergeStep>::const_iterator m = mSteps.begin();
      m != mSteps.end(); ++m)
    if (*m == mstep)
      return true;
  return false;
}

bool MergeStep::operator==(const MergeStep & mstep) const {
  assert(ccList.size() == idPaths.size());
  assert(mstep.ccList.size() == mstep.idPaths.size());
  if (cellx != mstep.cellx)
    return false;
  if (celly != mstep.celly)
    return false;
  if (cellz != mstep.cellz)
    return false;
  if (ccList.size() != mstep.ccList.size())
    return false;

  // finally, we only check the paths, we don't need to check the cclist,
  // because it's linked
  std::vector<unsigned int>::const_iterator j = mstep.idPaths.begin();
  for(std::vector<unsigned int>::const_iterator i = idPaths.begin();
      i != idPaths.end(); ++i, ++j)
    if (*i != *j)
      return false;
  return true;
}

std::string MergeDescriptor::toString(const std::string & prefix) const {
  std::ostringstream str;
  if (isOrigin()) {
    str << prefix << "Scene" << std::endl;
  }
  else {
    str << prefix << "Object genus: " << genus << std::endl;
    str << prefix << " Elements: ";
    for(std::vector<std::pair<unsigned int, unsigned int> >::const_iterator o = objects.begin();
	o != objects.end(); ++o) {
      if (o != objects.begin())
	str << ", ";
      str << (*o).first;
    }
    str << std::endl;
  }

  for(std::vector<SceneDescriptor *>::const_iterator c = begin(); c != end(); ++c) {
    str << toMD(c).toString(prefix + " ");
  }

  return str.str();
}


