/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef PLANAR_PATH
#define PLANAR_PATH

#include <vector>
#include "Path3D.h"
#include "Path2D.h"
#include "Layer.h"

namespace Taglut {

  /** a class that describe a 2D coordinate system in a plane */
  class PlanarMapping : public Plane3D {
  private:
    /** Axis X (onto the plane) */
    Coord3D axisx;
    /** Axis Y (onto the plane) */
    Coord3D axisy;

    /** create the axes */
    void setAxis();
  public:
    /** default constructor (the mapping is not defined */
    PlanarMapping();

    /** constructor using a point and a vector */
    PlanarMapping(const Coord3D & point, const Coord3D & vector);

    /** constructor using a plane */
    PlanarMapping(const Plane3D & plane);

#ifndef SWIG
    /** copy operator */
    PlanarMapping & operator=(const PlanarMapping & pm);
#endif

    /** return true if the current mapping has been defined */
    inline bool isDefined() const {
      return axisx.norm() != 0.;
    }

    /** conversion tool */
    Coord2D get2DCoords(const Coord3D & coords) const;

    /** conversion tool */
    Coord3D get3DCoords(const Coord2D & coords) const;

    /** conversion tool */
    Path3D convert(const Path2D & coords) const;

    /** conversion tool */
    Path2D convert(const Path3D & coords) const;
  };


  /** a class that contains a 2D path in a plane */
  class Path3DPlanar : public PlanarMapping, public Path2D {
  public:
    /** default constructor (empty path) */
    Path3DPlanar();
    /** given a 3D path, use PCA to detect the best plane, then compute projections the points in the plane to compute the 2D coordinates */
    Path3DPlanar(const Path3D & p);
    /** copy constructor */
    Path3DPlanar(const Path3DPlanar & p);

    /** conversion tool */
    inline static std::vector<Path3DPlanar> convert(const std::vector<Path3D> & paths) {
      std::vector<Path3DPlanar> result;
      for(std::vector<Path3D>::const_iterator p = paths.begin(); p != paths.end(); ++p)
	result.push_back(*p);
      return result;
    }

    /** conversion tool */
    inline Path3D getPath3D() const {
      return PlanarMapping::convert(*this);
    }
  };

  /** a class that contains several 2D paths in a plane */
  class MultiPath3DPlanar : public PlanarMapping, private std::vector<Path2D> {
  private:
    void insert(Layer & layer, unsigned int id) const;

    /** return true if path #id2 is inside path #id1 */
    bool isInside(unsigned int id1, unsigned int id2) const;

  public:
    /** default constructor (empty stucture) */
    MultiPath3DPlanar();

    /** copy constructor */
    MultiPath3DPlanar(const MultiPath3DPlanar & mp);

    /** add a path in this structure. The first one create the plane,
	the others are added according to the specific 2D coordinates system */
    MultiPath3DPlanar & addPath(const Path3DPlanar & path);

    /** accessor */
    inline unsigned int nbPaths() const  {
      return std::vector<Path2D>::size();
    }

    /** return true if the given 2D point is inside the object wich
	borders are described by the polygons in the current structure. */
    bool isInsideObject(const Coord2D & coord) const;

    /** return true if the given 2D point is inside the object wich
	borders are described by the polygons in the current structure. */
    inline bool isInsideObject(const Coord3D & coord) const {
      return isInsideObject(get2DCoords(coord));
    }

    /** return the list of 3D paths */
    std::vector<Path3D> getPaths() const;

    /** return the 3D path described by the given id (can generate an exception if out of bound) */
    Path3D getPath(unsigned int id) const;

    /** return the 3D point contained by the path and at the given location */
    Coord3D get3DPoint(unsigned int pathid, unsigned int pointid) const;

    /** return the 3D point corresponding to the given iterator on a path */
    Coord3D get3DPoint(const Path2D::iterator & it) const {
      return get3DCoords(*it);
    }

    /** return the 3D point corresponding to the given iterator on a path */
    Coord3D get3DPoint(const Path2D::const_iterator & it) const {
      return get3DCoords(*it);
    }

    /** insert a point at the given location */
    inline Path2D::iterator insertPoint(unsigned int pathid, Path2D::iterator point, const Coord3D & cPoint) {
      Coord2D p(get2DCoords(cPoint));
      return (*this)[pathid].insertPoint(point, p);
    }

    /** iterator on a path */
    Path2D::const_iterator begin(unsigned int pathid) const {
      assert(pathid < nbPaths());
      return (*this)[pathid].begin();
    }

    /** iterator on a path */
    Path2D::const_iterator end(unsigned int pathid) const {
      assert(pathid < nbPaths());
      return (*this)[pathid].end();
    }

    /** iterator on a path */
    Path2D::iterator begin(unsigned int pathid) {
      assert(pathid < nbPaths());
      return (*this)[pathid].begin();
    }

    /** iterator on a path */
    Path2D::iterator end(unsigned int pathid) {
      assert(pathid < nbPaths());
      return (*this)[pathid].end();
    }

    /** return the number of points of the given path */
    unsigned int size(unsigned int pathid) const {
      assert(pathid < nbPaths());
      return (*this)[pathid].size();
    }

    /** return the layer structure of paths (value corresponds to the
	id of the path in the structure, -1 means ambient space).  */
    Layer getInclusions() const;
  };

}

#endif
