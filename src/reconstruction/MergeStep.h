/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MERGE_STEP
#define MERGE_STEP

#include <vector>
#include <map>
#include <assert.h>

namespace Taglut {
  /** this class describe a merging step by describing the location of the merge (the cell coordinates),
      and the involved CC (that can be present more than one time) */
  class MergeStep {
  protected:
    int cellx;
    int celly;
    int cellz;

    std::vector<unsigned int> ccList;

    std::vector<unsigned int> idPaths;
    public:
    /** constructor */
    MergeStep(int cellx_, int celly_, int cellz_,
	      unsigned int idCC1, unsigned int idCC2,
	      unsigned int idPath1, unsigned int idPath2) : cellx(cellx_), celly(celly_), cellz(cellz_) {
      ccList.push_back(idCC1);
      ccList.push_back(idCC2);
      idPaths.push_back(idPath1);
      idPaths.push_back(idPath2);
    }
    /** copy constructor */
    MergeStep(const MergeStep & ms) : cellx(ms.cellx), celly(ms.celly), cellz(ms.cellz),
				      ccList(ms.ccList), idPaths(ms.idPaths) {
    }
    /** constructor using a liste of cc */
    MergeStep(int cellx_, int celly_, int cellz_,
	      const std::vector<unsigned int> & CClist,
	      const std::vector<unsigned int> & idpaths) : cellx(cellx_), celly(celly_), cellz(cellz_),
							  ccList(CClist), idPaths(idpaths) {
      assert(CClist.size() == idpaths.size());
    }

    /** accessor */
    inline const std::vector<unsigned int> & getPolygons() const {
      return idPaths;
    }

    /** accessor */
    inline int getCellX() const { return cellx; }
    /** accessor */
    inline int getCellY() const { return celly; }
    /** accessor */
    inline int getCellZ() const { return cellz; }

#ifndef SWIG
    /** return true if the current step and the given one are equal */
    bool operator==(const MergeStep & mstep) const;
#endif

    /** return the number of elementary steps contained in the current merge step */
    unsigned int getNbElementarySteps() const {
      assert(ccList.size() == idPaths.size());
      return ccList.size() - 1;
    }

    /** return the number of elementary steps described by the given merge steps */
    static unsigned int getNbElementarySteps(const std::vector<MergeStep> & msteps) {
      unsigned int result = 0;
      for(std::vector<MergeStep>::const_iterator m = msteps.begin();
	  m != msteps.end(); ++m)
	result += (*m).getNbElementarySteps();
      return result;
    }

    /** return the number of polygons that will be involved in this
	merge, that are part of the given id */
    unsigned int getNbPolygonsFromCC(unsigned int id) const {
      unsigned int result = 0;
      for(std::vector<unsigned int>::const_iterator cc = ccList.begin();
	  cc != ccList.end(); ++cc)
	if ((*cc) == id)
	  ++result;
      return result;
    }

    /** return true if the given id is involved in the current merge step */
    bool isInvolved(unsigned int id) const {
      for(std::vector<unsigned int>::const_iterator cc = ccList.begin();
	  cc != ccList.end(); ++cc)
	if ((*cc) == id)
	  return true;
      return false;
    }

    /** remove one polygon part of the given id. Return true if a removal has been applied */
    bool removeOne(unsigned int id);

    /** remove all but one polygons part of the given id. Return the number of removals that has been applied */
    unsigned int removeAllButOne(unsigned int id);

    /** remove all the polygons part of the given id. Return the number of removals that has been applied */
    unsigned int removeAll(unsigned int id);

    /** an ordering to manipulate std::map<MergeStep> */
    bool operator<(const MergeStep & ms) const {
      if (cellx < ms.cellx)
	return true;
      else if (cellx > ms.cellx)
	return false;
      assert(cellx == ms.cellx);
      if (celly < ms.celly)
	return true;
      else if (celly > ms.celly)
	return false;
      assert(celly == ms.celly);
      if (cellz < ms.cellz)
	return true;
      else if (cellz > ms.cellz)
	return false;
      assert(cellz == ms.cellz);

      {
	if (ccList.size() < ms.ccList.size())
	  return true;
	else if (ccList.size() > ms.ccList.size())
	  return false;
	assert(ccList.size() == ms.ccList.size());
	std::vector<unsigned int>::const_iterator msc = ms.ccList.begin();
	for(std::vector<unsigned int>::const_iterator c = ccList.begin();
	    c != ccList.end(); ++c, ++msc)
	  if (*c < *msc)
	    return true;
	  else if (*c > *msc)
	    return false;
      }

      {
	if (idPaths.size() < ms.idPaths.size())
	  return true;
	else if (idPaths.size() > ms.idPaths.size())
	  return false;
	assert(idPaths.size() == ms.idPaths.size());
	std::vector<unsigned int>::const_iterator msc = ms.idPaths.begin();
	for(std::vector<unsigned int>::const_iterator c = idPaths.begin();
	    c != idPaths.end(); ++c, ++msc)
	  if (*c < *msc)
	    return true;
	  else if (*c > *msc)
	    return false;
	return false;
      }
    }

    /** return the list of merge step that are subpart of the current one */
    std::vector<MergeStep> getSubMergeSteps() const;

  };


  /** A class that describe a cost system for MergStep */
  class MergeStepCost {
  private:
    std::map<MergeStep, double> cost;
  protected:
    virtual double getValue(const MergeStep & ms) = 0;
  public:
    MergeStepCost() { }

    virtual ~MergeStepCost() { }

    /**an operator that return the cost corresponding to the given merge step */
    double operator()(const MergeStep & ms) {
      std::map<MergeStep, double>::const_iterator v = cost.find(ms);
      if (v == cost.end()) {
	double vv = getValue(ms);
	cost[ms] = vv;
	return vv;
      }
      else
	return (*v).second;
    }
  };

  class MeshBySlices;

  /** a class describing a cost using Hausdorff distances (in R^3)
      between polygons */
  class MergeStepCostPolygonDistance : public MergeStepCost {
  private:
    const MeshBySlices * mbs;
  protected:
    virtual double getValue(const MergeStep & ms);
  public:
    MergeStepCostPolygonDistance(const MeshBySlices & mbs_) : mbs(&mbs_) { }

  };

}

#endif
