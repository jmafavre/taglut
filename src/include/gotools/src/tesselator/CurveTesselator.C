//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#include "GoTools/tesselator/CurveTesselator.h"

namespace Go
{


//===========================================================================
CurveTesselator::~CurveTesselator()
//===========================================================================
{
}

//===========================================================================
void CurveTesselator::tesselate()
//===========================================================================
{
    Point pt(3);
    int n = mesh_->numVertices();
    for (int i = 0; i < n; ++i) {
	double rt = double(i)/double(n-1);
	double ta = curve_.startparam();
	double tb = curve_.endparam();

	// Ensure a finite extension in case of an unbounded curve
// 	double limit = 1.0e3;
// 	ta = std::max(ta, -1.0*limit);
// 	tb = std::min(tb, 1.0*limit);

	double t = ta*(1.0 - rt) + rt*tb;
	curve_.point(pt, t);
	mesh_->vertexArray()[i*3] = pt[0];
	mesh_->vertexArray()[i*3 + 1] = pt[1];
	mesh_->vertexArray()[i*3 + 2] = pt[2];
    }
}

} // namespace Go

