//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _SOLVEBCG_H
#define _SOLVEBCG_H


#include "GoTools/creators/SolveCG.h"

namespace Go
{

class SolveBCG : public SolveCG
{

public:

  SolveBCG(int conv_type, bool symm);

  // Destructor.
  virtual ~SolveBCG();

  // Prepare for preconditioning.
  virtual void precond(double relaxfac);

  // Solve the equation system.
  virtual int solve(double *ex, double *eb, int nn);


private:

  double omega_;        // Relaxation parameter.
  int conv_type_; // We operate with 4 different types of convergence estimates.
  bool symm_;

  void precondRILU(double relaxfac); // @@sbr Not sure RILU is what we will be using...

  // Helper funtion, return largest element in array (i.e. the sup-norm).
  double max_abs_element(double* x, int nn);

};

} // end namespace Go


#endif // _SOLVEBCG_H

