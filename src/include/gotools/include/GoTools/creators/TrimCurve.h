//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _TRIMCURVE_
#define _TRIMCURVE_

#include <boost/smart_ptr.hpp>

#include "GoTools/utils/Point.h"
#include "GoTools/creators/EvalCurveSet.h"
#include "GoTools/geometry/SplineCurve.h"
#include "GoTools/geometry/SplineSurface.h"


namespace Go
{

  class CurveOnSurface;
/// This class represents the curve obtained by projecting a 
/// given 3D curve onto a given part of a given 3D surface.

class TrimCurve : public EvalCurveSet
{
public:

    /// Constructor, 
  TrimCurve(CurveOnSurface* bd_crv);

  TrimCurve(CurveOnSurface* bd_crv, double start, double end);

  TrimCurve(Point startpt, Point endpt, CurveOnSurface* bd_crv);

    /// virtual destructor ensures safe inheritance
    virtual ~TrimCurve();
    
    // Inherited from EvalCurveSet
    std::vector<Point> eval( double t);

    // Inherited from EvalCurveSet
    virtual void eval(double t, int n, std::vector<std::vector<Point> >& der);

    // Inherited from EvalCurveSet
    virtual double start();

    // Inherited from EvalCurveSet
    virtual double end();

    /// Inherited from EvalCurveSet::dim().  
    virtual int dim();

    /// Inherited from EvalCurveSet::approximationOK().  For this class, the specified tolerances
    /// are not used; the internally stored 'epsgeo' value is used as tolerance (this value was
    /// specified in the constructor).
    /// \param par the parameter at which to check the curve
    /// \param approxpos the position we want to check whether or not the curve
    ///                  approximates for parameter 'par'.
    /// \param tol1 unused
    /// \param tol2 unused
    /// \return 'true' if the curve approximates the point at the parameter, 'false'
    ///         otherwise.
    virtual bool approximationOK(double par, const std::vector<Point>& approxpos,
				 double tol1, double tol2); 

    /// The number of curves in the curve set.
    /// \return the number of curves in the curve set.
    virtual int nmbCvs();

private:
    CurveOnSurface* sfcv_;
    Point startpt_;
    Point endpt_;
    double start_;
    double end_;

    void evaluate(double t, int n, std::vector<Point>& result);
};


}

#endif //_TRIMCURVE_
