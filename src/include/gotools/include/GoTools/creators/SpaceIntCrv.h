//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _SPACEINTCRV_
#define _SPACEINTCRV_

#include "GoTools/creators/EvalCurve.h"
#include "GoTools/geometry/CurveOnSurface.h"
#include "GoTools/utils/Point.h"

namespace Go
{
  /// This class represents an intersection curve approximated by two
  /// curve on surface instances
  class SpaceIntCrv : public EvalCurve
  {
  public:
    /// Constructor
    SpaceIntCrv(boost::shared_ptr<ParamCurve> init_crv, int pardir,
		std::vector<boost::shared_ptr<CurveOnSurface> >& sfcv1, 
		std::vector<double> start1, 
		std::vector<double> end1,
		std::vector<boost::shared_ptr<CurveOnSurface> >& sfcv2,
		std::vector<double> start2, std::vector<double> end2,
		std::vector<bool> opposite, bool same_orient);

    /// Destructor.
    virtual ~SpaceIntCrv();

    /// Evaluate the curves.
    /// \param t parameter in which to evaluate.
    /// \return the evaluated point for the curve.
    virtual Point eval(double t) const;

    /// Evaluate the curve derivatives.
    /// \param t parameter in which to evaluate.
    /// \param n number of derivatives to compute.
    /// \param der the evaluated points up to the n'th derivative for the curve.
    virtual void eval(double t, int n, Point der[]) const; // n = order of diff

    /// Start parameter of domain.
    /// \return start parameter of the spline space.
    virtual double start() const;

    /// End parameter of domain.
    /// \return end parameter of the spline space.
    virtual double end() const;

    /// The geometric dimension of the spline curves.
    virtual int dim() const;

    /// Whether the approximation is within tolerances in input parameter.
    /// \param par parameter in which to evaluate.
    /// \param approxpos whether the input point are within tolerance from the
    ///                  evaluated points (as given by eval()).
    /// \param tol1 tolerance used to decide approximation accuracy.
    /// \param tol2 tolerance used to decide approximation accuracy.
    /// \return whether the approximation is within tolerances in input parameter.
    virtual bool approximationOK(double par, Point approxpos,
				 double tol1, double tol2) const;

  private:
    boost::shared_ptr<ParamCurve> init_crv_;
    std::vector<boost::shared_ptr<CurveOnSurface> > sfcv1_;
    std::vector<boost::shared_ptr<CurveOnSurface> > sfcv2_;
    std::vector<double> start1_, end1_, start2_, end2_;
    std::vector<double> segment_;
    std::vector<bool> opposite_;
    bool same_orient_;

    void evaluate(double t, int n, Point result[]) const;
  };

} // namespace Go

#endif //_SPACEINTCRV_
