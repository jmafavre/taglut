//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _SOLVECGCO_H_
#define _SOLVECGCO_H_


#include "GoTools/creators/SolveCG.h"


//   -----------------------------------------------------------------------
//      Interface file for class SolveCGCO
//   -----------------------------------------------------------------------
//
//       Solve the equation system Ax=b where A is a symmetric
//       positive definite matrix using Conjugate Gradient Method.
//       Assuming A describes a constrained optimization problem, i.e.
//           (B^TB C^T)
//       A = (        ),
//           (C     0 )
//       We create a block preconditioner using the CG preconditioner for the
//       upper left block, and a (currently) diagonal preconditioner to the
//       lower right.
//
//   -----------------------------------------------------------------------
//    Written by: Vibeke Skytt                            09.99
//    Based on  : PrCGCO.h written by Mike Floater
//   -----------------------------------------------------------------------

namespace Go
{

class SolveCGCO : public SolveCG
{

public:

  // Constructor.
  // m: size of original system.
  // n: number of constraints.
  SolveCGCO(int m, int n);

  // Prepare for preconditioning.
  virtual void precondRILU(double relaxfac);

  // Destructor.
  virtual ~SolveCGCO();


private:


  int m_;
  int n_;

};

} // end namespace Go

#endif // _SOLVECGCO_H_
