//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef PARAMETRICSURFACETESSELATOR_H
#define PARAMETRICSURFACETESSELATOR_H

#include "GoTools/tesselator/Tesselator.h"
#include "GoTools/tesselator/RegularMesh.h"
#include "GoTools/geometry/ParamSurface.h"
#include "GoTools/tesselator/GenericTriMesh.h"
#include <boost/shared_ptr.hpp>
#include "GoTools/utils/config.h"

namespace Go
{

/** ParametricSurfaceTesselator: create a mesh for a possibly trimmed surface with a suitable
    triangulation.
*/

class GO_API ParametricSurfaceTesselator : public Tesselator
{
public:
    ParametricSurfaceTesselator(const ParamSurface& surf)
	: surf_(surf), m_(20), n_(20)
    {
	// Use of texture coordinates in mesh_ seems to be buggy -
	// commenting out for now. @jbt
// 	mesh_ = boost::shared_ptr<GenericTriMesh>(new GenericTriMesh(0,0,true,true));
	mesh_ = boost::shared_ptr<GenericTriMesh>(new GenericTriMesh(0,0,true,false));
    }

    virtual ~ParametricSurfaceTesselator();

    virtual void tesselate();

    boost::shared_ptr<GenericTriMesh> getMesh()
    {
	return mesh_;
    }

    void changeRes(int n, int m);

    void getRes(int& n, int& m)
    {
	m = m_;
	n = n_;
    }

private:
    const ParamSurface& surf_;
    boost::shared_ptr<GenericTriMesh> mesh_;
    int m_;
    int n_;

};

} // namespace Go




#endif //  PARAMETRICSURFACETESSELATOR_H

