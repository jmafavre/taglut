//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef CURVETESSELATOR_H
#define CURVETESSELATOR_H

#include "GoTools/tesselator/Tesselator.h"
#include "GoTools/tesselator/LineStrip.h"
#include "GoTools/geometry/ParamCurve.h"

/** Documentation ...
 */

namespace Go
{

class CurveTesselator : public Tesselator
{
public:
    CurveTesselator(const ParamCurve& curve)
	: curve_(curve) 
	{
	    mesh_ = boost::shared_ptr<LineStrip>(new LineStrip(500));
	}

    virtual ~CurveTesselator();
  
    void tesselate();

    boost::shared_ptr<LineStrip> getMesh()
    {
	return mesh_;
    }

    //
    // 010430: I'm not sure if we're going to need these functions, an
    //         alternative is to trigger these actions when asking for
    //         pointers to the discretizations, which will be done by
    //         the 'painter' when that one is requested to redraw the scene...
    //         (jon)
    //

    void changeRes(int n)
    {
	mesh_->resize(n);
	tesselate();
    }
    void getRes(int& n)
    {
	n = mesh_->numVertices();
    }

private:
    const ParamCurve& curve_;
    boost::shared_ptr<LineStrip> mesh_;
};

} // namespace Go

#endif // CURVETESSELATOR_H

