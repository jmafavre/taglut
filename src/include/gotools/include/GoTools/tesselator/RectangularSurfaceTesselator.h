//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef RECTANGULARSURFACETESSELATOR_H
#define RECTANGULARSURFACETESSELATOR_H

#include "GoTools/tesselator/Tesselator.h"
#include "GoTools/tesselator/RegularMesh.h"
#include "GoTools/tesselator/LineStrip.h"
#include "GoTools/geometry/ParamSurface.h"
#include "GoTools/utils/config.h"

namespace Go
{

/** RectangularSurfaceTesselator: create a mesh for a boundary trimmed parametric
    surface with a suitable
    triangulation.
*/

class GO_API RectangularSurfaceTesselator : public Tesselator
{
public:
    RectangularSurfaceTesselator(const ParamSurface& surf,
				   int ures = 100,
				   int vres = 100,
				   bool iso = true,
				   int uiso = 15,
				   int viso = 15,
				   int isores = 300)
	: surf_(surf),
	isolines_(iso), uiso_(uiso), viso_(viso), isores_(isores)
    {
	mesh_ = boost::shared_ptr<RegularMesh>(new RegularMesh(ures, vres, true, true));
    }

    virtual ~RectangularSurfaceTesselator();
  
    virtual void tesselate();

    boost::shared_ptr<RegularMesh> getMesh()
    {
	return mesh_;
    }

    std::vector<LineStrip>& getIsolineStrips()
    {
	return isolinestrips_;
    }

    //
    // 010430: I'm not sure if we're going to need these functions, an
    //         alternative is to trigger these actions when asking for
    //         pointers to the discretizations, which will be done by
    //         the 'painter' when that one is requested to redraw the scene...
    //         (jon)
    //

    void changeRes(int m, int n)
    {
	mesh_->resize(m, n);
	tesselateSurface();
    }
    void getRes(int& m, int& n)
    {
	n = mesh_->numStrips() + 1;
	m = mesh_->numVertices()/n;
    }

    void changeIsolines(bool isolines)
    {
	isolines_ = isolines;
	if (isolines_) {
	    tesselateIsolines();
	}
    }

    void getIsolines(bool& isolines)
    {
	isolines = isolines_;
    }

    void changeIsolineNumRes(int m, int n, int res)
    {
	uiso_ = m;
	viso_ = n;
	isores_ = res;
	tesselateIsolines();
    }
    void getIsolineNumRes(int& m, int& n, int& res)
    {
	n = uiso_;
	m = viso_;
	res = isores_;
    }

private:
    void tesselateSurface();
    void tesselateIsolines();

    const ParamSurface& surf_;
    boost::shared_ptr<RegularMesh> mesh_;
    std::vector<LineStrip> isolinestrips_;
    bool isolines_;
    int uiso_;
    int viso_;
    int isores_;
};

} // namespace Go


#endif // RECTANGULARSURFACETESSELATOR_H

