//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef SPLINE2MESH_H_INCLUDED
#define SPLINE2MESH_H_INCLUDED






#include "GoTools/utils/Array.h"

#include <vector>


#include "GoTools/tesselator/2dpoly_for_s2m.h"
#include "GoTools/geometry/SplineSurface.h"
#include "GoTools/geometry/SplineCurve.h"






namespace Go
{
  
  // 081206: A version for more than one curve.
  void make_trimmed_mesh(boost::shared_ptr<SplineSurface> srf, 
			 std::vector<boost::shared_ptr<SplineCurve> >& crv_set,
			 std::vector< Vector3D > &vert,
			 std::vector< Vector2D > &vert_p,
			 std::vector< int > &bd,
			 std::vector< Vector3D > &norm,
			 std::vector<int> &mesh,
			 std::vector< Vector3D > &trim_curve,
			 std::vector< Vector3D > &trim_curve_p,
			 const int dn, const int dm,
			 double bd_res_ratio);

} // namespace Go

#endif
