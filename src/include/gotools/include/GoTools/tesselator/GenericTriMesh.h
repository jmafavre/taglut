//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef GENERICTRIMESH_H
#define GENERICTRIMESH_H


#include "GoTools/tesselator/GeneralMesh.h"
#include <vector>
/* #ifdef _MSC_VER */
/* #include <windows.h> */
/* #endif */
/* #include <GL/gl.h> */

namespace Go
{

/** Based on RegularMesh, 
 * designed to store data for GL visualizing (regular triangles,
 * not triangle strips).
 */
class GO_API GenericTriMesh : public GeneralMesh
{
public:
    GenericTriMesh(int num_vert = 0, int num_tri = 0,
		   bool use_normals = true,
		   bool use_texcoords = false);

    ~GenericTriMesh();

    bool useNormals()
    { return use_norm_; }

    bool useTexCoords() 
    { return use_texc_; }

    /// The total number of triangles
    virtual int numTriangles()
    { return triangles_.size()/3; }

    virtual int numVertices()
    { return vert_.size()/3; }

    void resize(int num_vert, int num_tri);

    virtual double* vertexArray();
    virtual double* paramArray();
    virtual int atBoundary(int idx);
    int* boundaryArray();
    double* normalArray();
    double* texcoordArray();
    virtual unsigned int* triangleIndexArray();

    virtual GenericTriMesh* asGenericTriMesh();

private:
    bool use_norm_;
    bool use_texc_;
  
    std::vector<double> vert_;
    std::vector<double> param_;
    std::vector<int> bd_;
    std::vector<double> norm_;
    std::vector<double> texc_;
    std::vector<unsigned int> triangles_;
  
};

} // namespace Go



#endif // GENERICTRIMESH_H

