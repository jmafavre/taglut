//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _LINECLOUDTESSELATOR_H
#define _LINECLOUDTESSELATOR_H


#include "GoTools/tesselator/Tesselator.h"
#include "GoTools/geometry/LineCloud.h"

/** Documentation ...
    etc
 */

namespace Go
{

class LineCloudTesselator : public Tesselator
{
public:
    LineCloudTesselator(const Go::LineCloud& lc)
	: orig_cloud_(lc), render_cloud_(lc), scale_(1.0)
    {}
    virtual ~LineCloudTesselator();
  
    void tesselate();

    const Go::LineCloud& getRenderCloud()
    {
	return render_cloud_;
    }

    void setScale(double scale)
    {
	if (scale != scale_) {
	    scale = scale_;
	    tesselate();
	}
    }
    double getScale()
    {
	return scale_;
    }

private:
    const Go::LineCloud& orig_cloud_;
    Go::LineCloud render_cloud_;
    double scale_;
};

} // namespace Go



#endif // _LINECLOUDTESSELATOR_H

