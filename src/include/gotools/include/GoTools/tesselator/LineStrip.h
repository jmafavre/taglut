//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _LINESTRIP_H
#define _LINESTRIP_H

#include "GoTools/tesselator/GeneralMesh.h"
#include <vector>


namespace Go
{

/** LineStrip: Structure for storing values for a line strip.
 */

class GO_API LineStrip : public GeneralMesh
{
public:
    virtual ~LineStrip();
    LineStrip(int n = 200);

    void resize(int n);


    virtual int numVertices()
    { return vert_.size()/3; }

    virtual double* vertexArray() { return &vert_[0]; }
    virtual double* paramArray() { return &param_[0]; }
     virtual int atBoundary(int idx);
    unsigned int* stripArray() { return &strip_[0]; }
    virtual unsigned int* triangleIndexArray() { return &strip_[0]; }

    virtual LineStrip* asLineStrip();

private:
    std::vector<double> vert_;
    std::vector<double> param_;
    std::vector<unsigned int> strip_;
};



} // namespace Go





#endif // end of #ifdef _LINESTRIP_H_
