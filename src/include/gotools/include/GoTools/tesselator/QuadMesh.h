//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _QUADMESH_H
#define _QUADMESH_H

    /** Brief description. 
     *  Detailed description.
     */

#include "GoTools/tesselator/GeneralMesh.h"
#include <vector>

/** Based on gvGenericTriMesh,
 *  designed to store data for GL visualizing.
 */
namespace Go
{

class QuadMesh : public GeneralMesh
{
public:
    QuadMesh(int num_vert = 0, int num_quads = 0,
	       bool use_normals = true,
	       bool use_texcoords = false)
	: use_norm_(use_normals),
	  use_texc_(use_texcoords)
    {
	resize(num_vert, num_quads);
    }

    bool useNormals()
    { return use_norm_; }

    bool useTexCoords() 
    { return use_texc_; }

    /// The total number of quads
    int numQuads()
    { return quads_.size()/4; }

    virtual int numTriangles()
    { return quads_.size()/4; }

    virtual int numVertices()
    { return vert_.size()/3; }

    void resize(int num_vert, int num_quads)
    {
	vert_.resize(num_vert*3);
	if (use_norm_)
	    norm_.resize(num_vert*3);
	if (use_texc_)
	    texc_.resize(num_vert*2);
	quads_.resize(num_quads*4);
    }

    virtual double* vertexArray() { return &vert_[0]; }
    double* normalArray() { return &norm_[0]; }
    double* texcoordArray() { return &texc_[0]; }
    unsigned int* quadIndexArray() { return &quads_[0]; }
    virtual unsigned int* triangleIndexArray() { return &quads_[0]; }
    virtual double* paramArray() {return 0;}
    virtual int atBoundary(int idx) {return 0;}


private:
    bool use_norm_;
    bool use_texc_;
  
    std::vector<double> vert_;
    std::vector<double> norm_;
    std::vector<double> texc_;
    std::vector<unsigned int> quads_;
  
};

} // namespace Go



#endif // _GVQUADMESH_H

