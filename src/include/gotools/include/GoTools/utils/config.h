//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _CONFIG_H
#define _CONFIG_H

#ifdef __BORLANDC__
# define GO_API __declspec(package)
#elif defined(MICROSOFT) || defined(_MSC_VER)
# if defined(__DLL__) || defined(_DLL)
#  define GO_API __declspec(dllexport)
# else
#  define GO_API __declspec(dllimport)
# endif // __DLL__
#else
# define GO_API
#endif // __BORLANDC__

// The following pragma is not optimal, but it's a workaround to
// getting rid of warning C4251 in Visual Studio
#ifdef _MSC_VER
#pragma warning( disable: 4251 )
#endif // _MSC_VER

#endif // _CONFIG_H
