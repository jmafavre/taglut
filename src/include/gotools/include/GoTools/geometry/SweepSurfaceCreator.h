//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef __SWEEPSURFACECREATOR_H
#define __SWEEPSURFACECREATOR_H

#include "GoTools/geometry/SplineSurface.h"
#include "GoTools/geometry/SplineCurve.h"
#include "GoTools/utils/Point.h"

namespace Go
{

  class SweepSurfaceCreator
  {

  public:

    // Destructor
    virtual ~SweepSurfaceCreator() { }

    /// Create a linearly swept surface as a tensor product of two curves. If \f$P\f$ is the point to
    /// be swept, and the curves have B-spline coefficients \f$C_i\f$ and \f$D_j\f$, then the surface has
    /// B-spline coefficients \f$C_i+D_j-P\f$. If the point lies on the first curve,
    /// the first curve will be swept along the second curve. If the point lies on the second curve,
    /// the second curve will be swept along the first curve.
    /// \param curv1 the first curve
    /// \param curv2 the second curve
    /// \param pt the point to be swept
    /// \return a pointer to the swept surface
    static SplineSurface* linearSweptSurface(const SplineCurve &curv1,
					     const SplineCurve &curv2,
					     const Point &pt);

    /// Create a surface as the rotation of a curve around an axis
    /// \param curve the curve
    /// \param angle the rotation angle (in radians). If less than \f$2\pi\f$, it is truncated down to \f$2\pi\f$.
    ///              If more than \f$-2\pi\f$, it is truncated up to \f$-2\pi\f$.
    /// \param pt a point on the rotation axis
    /// \param axis a vector describing the direction of the rotation axis
    /// \return a pointer to the rotated surface
    static SplineSurface* rotationalSweptSurface(const SplineCurve &curve,
						 double angle,
						 const Point &pt,
						 const Point &axis);


  };    // Class SweepSurfaceCreator


} // namespace Go


#endif    // #ifndef __SWEEPSURFACECREATOR_H
