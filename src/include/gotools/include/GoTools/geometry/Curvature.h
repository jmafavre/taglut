//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef __CURVATURE_H
#define __CURVATURE_H


#include "GoTools/utils/Point.h"
#include "GoTools/geometry/SplineCurve.h"
#include <vector>





namespace Go
{

  // Get the points on a SplineCurve with a given curvature radius
  // Result is stored in a vector as parameter values
  void curvatureRadiusPoints(const SplineCurve& curve,
			     double curveRad,
			     std::vector<double>& pos);

  // Get the minimal curvature radius, and the parameter value of the point
  // with the minimal curvature radius
  void minimalCurvatureRadius(const SplineCurve& curve,
			      double& mincurv,
			      double& pos);

} // namespace Go



#endif    // #ifndef __CURVATURE_H

