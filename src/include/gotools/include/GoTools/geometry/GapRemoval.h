//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _GAPREMOVAL_H
#define _GAPREMOVAL_H

#include "GoTools/geometry/SplineCurve.h"
#include "GoTools/geometry/SplineSurface.h"
#include "GoTools/geometry/CurveOnSurface.h"
#include "GoTools/geometry/BsplineBasis.h"
#include "GoTools/geometry/BoundedSurface.h"
#include "GoTools/creators/ConstraintDefinitions.h"

namespace Go {

  // Removal of gaps between two adjacent surfaces of various types

namespace GapRemoval
{

  // Both surfaces are non-trimmed spline surfaces and the
  // common boundary are described by CurveOnSurface entities
  void
  removeGapSpline(boost::shared_ptr<SplineSurface>& srf1, 
		  boost::shared_ptr<CurveOnSurface>& bd_cv1,
		  double start1, double end1,
		  boost::shared_ptr<SplineSurface>& srf2, 
		  boost::shared_ptr<CurveOnSurface>& bd_cv2,
		  double start2, double end2, Point vertex1, Point vertex2,
		  double epsge, bool *same_orientation = NULL);

  // Both surfaces are trimmed of some type and the
  // common boundary are described by CurveOnSurface entities
  double
  removeGapTrim(boost::shared_ptr<CurveOnSurface>& bd_cv1,
		double start1, double end1,
		boost::shared_ptr<CurveOnSurface>& bd_cv2,
		double start2, double end2, Point vertex1, Point vertex2,
		double epsge);

  bool
  removeGapSplineTrim(boost::shared_ptr<SplineSurface>& srf1, 
		      std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv1,
		      std::vector<double> start1, 
		      std::vector<double> end1,
		      std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv2,
		      std::vector<double> start2, 
		      std::vector<double> end2, Point vertex1, 
		      Point vertex2, double epsge);

  void modifySplineSf(boost::shared_ptr<ParamSurface>& srf1, 
		      std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv1,
		      std::vector<double> start1, std::vector<double> end1,
		      boost::shared_ptr<ParamSurface>& srf2, 
		      std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv2,
		      std::vector<double> start2, std::vector<double> end2, 
		      double epsge);

  void modifySplines(boost::shared_ptr<ParamSurface>& srf1, 
		     std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv1,
		     std::vector<double>& start1, std::vector<double>& end1,
		     boost::shared_ptr<ParamSurface>& srf2, 
		     std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv2,
		     std::vector<double>& start2, std::vector<double>& end2, 
		     std::vector<Point>& vertex, double epsge);
//
  void
    removeGapSpline2(std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv1,
		     std::vector<double>& start1, std::vector<double>& end1,
		     std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cv2,
		     std::vector<double>& start2, std::vector<double>& end2, 
		     std::vector<Point>& vertex, double epsge);

  std::vector<boost::shared_ptr<sideConstraintSet> > 
    getCoefConstraints(boost::shared_ptr<SplineCurve>& crv1, int idx1,
		       boost::shared_ptr<SplineCurve>& crv2, int idx2, 
		       double tol);

  boost::shared_ptr<SplineCurve>
    replaceCurvePiece(boost::shared_ptr<SplineCurve> crv,
		      boost::shared_ptr<SplineCurve> sub_crv,
		      double par1, int cont1, 
		      double par2, int cont2);

  boost::shared_ptr<SplineSurface> 
    getSplineAndBd(boost::shared_ptr<ParamSurface> psurf,
		   std::vector<boost::shared_ptr<CurveOnSurface> >& bd_crvs);

  void 
    getBoundarySamples(std::vector<boost::shared_ptr<CurveOnSurface> >& all_bd,
		       std::vector<boost::shared_ptr<CurveOnSurface> >& bd_cvs,
		       std::vector<double>& pts, std::vector<double>& pars,
		       std::vector<int>& bd_idx, double epsge);

  bool modifyAtVertex(boost::shared_ptr<SplineSurface> srf,
		      Point face_param, Point vertex,
		      double epsge);

void 
  checkBoundaryDist(boost::shared_ptr<CurveOnSurface> bd1,
		    boost::shared_ptr<CurveOnSurface> bd2,
		    double start1, double end1,
		    double start2, double end2,
		    int nmb_sample, double& mdist1,
		    double& mdist2);

} // of namespace CreatorsUtils.

}; // end namespace Go



#endif // _GAPREMOVAL_H
