//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _POINT_ON_CURVE_H
#define _POINT_ON_CURVE_H

#include "GoTools/utils/Point.h"
#include <boost/smart_ptr.hpp>
#include "GoTools/geometry/ParamCurve.h"

namespace Go
{

/** Represents a point on a curve. Lean functionality for the time being, but should
    be extended to cover for instance curvature computations  */

class PointOnCurve
    {
    public:

      PointOnCurve();  // Empty constructor

	/// Constructor taking a curve and a parameter value
	PointOnCurve(boost::shared_ptr<ParamCurve> curve, double par);

	/// Constructor taking a curve and a point
	PointOnCurve(boost::shared_ptr<ParamCurve> curve, Point pnt);

	~PointOnCurve();

	/// Evaluate
	/// If an input point is given, this is the point that will be returned even if it
	/// does not lie exactly on the curve
	Point getPos() const;

	boost::shared_ptr<ParamCurve> getCurve() const
	    {
		return crv_;
	    }

	double getPar() const
	    {
		return par_;
	    }

	/// The curve is evaluated including der derivatives
	void evaluate(int der, std::vector<Point>& deriv) const;

	/// Add limiting information to the current curve
	void setParInterval(double start, double end);

    private:
	Point point_;  // The point
	double par_; // Parameter value on the curve corresponding to this point
	double t1_, t2_;  // End parameters of current parameter interval
	boost::shared_ptr<ParamCurve> crv_;  // The curve

};


} // namespace Go

#endif // _POINT_ON_CURVE_H

