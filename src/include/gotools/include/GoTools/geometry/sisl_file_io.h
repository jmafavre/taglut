//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _SISL_FILE_IO_H
#define _SISL_FILE_IO_H


#include <boost/smart_ptr.hpp>
#include <vector>

#include <fstream>

#ifdef __BORLANDC__
#include <cstdio>
#define STD_FILE std::FILE
#else
#define STD_FILE FILE
#endif

struct SISLObject;

void read_non_comment(STD_FILE* fp, char* string);

void file_to_obj(STD_FILE* fp, SISLObject** wo, int* jstat);

void curve_to_file(STD_FILE *f, struct SISLCurve *c1);

void surface_to_file(STD_FILE *f, struct SISLSurf *surf);

int get_next_surface(STD_FILE *fp, SISLSurf **qc);

int get_sisl_surfaces(STD_FILE* fp,
		      std::vector<boost::shared_ptr<SISLSurf> >& sisl_sfs);


#endif // _SISL_FILE_IO_H

