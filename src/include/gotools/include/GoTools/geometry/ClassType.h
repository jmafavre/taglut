//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _CLASSTYPE_H
#define _CLASSTYPE_H

namespace Go
{

    /** All concrete classes that inherit GeomObject should
     *  have a matching enum in ClassType. It is necessary to
     *  have a central location for the type identification numbers,
     *  so that two classes in different modules (for example) does
     *  not try to use the same number. At the same time, it is
     *  something we'd like to avoid, because it forces you to
     *  update this file every time you add a new GeomObject-
     *  inheriting class.
     */
    enum ClassType
    {
	Class_Unknown = 0,

	Class_SplineCurve = 100,
	Class_CurveOnSurface = 110,
	Class_Line = 120,
	Class_Circle = 130,
	Class_Ellipse = 140,
	Class_BoundedCurve = 150,
	Class_Hyperbola = 160,
	Class_Parabola = 170,

	Class_SplineSurface = 200,
	Class_BoundedSurface = 210,
	Class_SurfaceOnVolume = 211,
	Class_GoBaryPolSurface = 220,
	Class_GoHBSplineParamSurface = 230,
	Class_CompositeSurface = 240,
	Class_Plane = 250,
	Class_Cylinder = 260,
	Class_Sphere = 270,
	Class_Cone = 280,
	Class_Torus = 290,
	Class_SurfaceOfRevolution = 291,
	Class_Disc = 292,
	Class_LRSplineSurface = 293,

	Class_Go3dsObject = 300,
	Class_GoHeTriang = 310,
	Class_GoSdTriang = 320,
	Class_GoQuadMesh = 330,
	Class_GoHybridMesh = 340,
	Class_ParamTriang = 350,
	Class_GoVrmlGeometry = 360,

	Class_PointCloud = 400,
	Class_LineCloud = 410,

	Class_GoTriangleSets = 500,
	Class_RectGrid = 510,

	Class_SplineVolume = 700,
	Class_BoundedVolume = 710,
	Class_Parallelepiped = 720,
	Class_SphereVolume = 721,
	Class_CylinderVolume = 722,
	Class_ConeVolume = 723,
	Class_TorusVolume = 724
    };

}


#endif // _CLASSTYPE_H

