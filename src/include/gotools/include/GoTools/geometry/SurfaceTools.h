//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _SURFACETOOLS_H
#define _SURFACETOOLS_H

#include "GoTools/geometry/SplineSurface.h"
#include "GoTools/geometry/SplineCurve.h"
#include "GoTools/geometry/CurveOnSurface.h"
#include "GoTools/geometry/CurveLoop.h"


// Free functions operating on parametric surfaces

namespace Go
{

  // Fetch the outer boundary loop of a parametric surface
  CurveLoop outerBoundarySfLoop(boost::shared_ptr<ParamSurface> surf,
				double degenerate_epsilon);

  // Fetch all boundary loops of a parametric surface
  std::vector<CurveLoop> allBoundarySfLoops(boost::shared_ptr<ParamSurface> surf,
					    double degenerate_epsilon); 

  // Fetch all boundary loops of a parametric surface including degenerate
  // curves
  std::vector<CurveLoop> 
    absolutelyAllBoundarySfLoops(boost::shared_ptr<ParamSurface> surf,
				 double degenerate_epsilon); 
  // Iterate to an optimal corner where a number of surfaces meet
  // Positions on elementary surfaces are emphasized
  void 
    iterateCornerPos(Point& vertex, 
		     std::vector<std::pair<boost::shared_ptr<ParamSurface>, Point> > sfs,
		     double tol);

  bool cornerToCornerSfs(boost::shared_ptr<ParamSurface> sf1,
			 boost::shared_ptr<CurveOnSurface> sf_cv1,
			 boost::shared_ptr<ParamSurface> sf2,
			 boost::shared_ptr<CurveOnSurface> sf_cv2,
			 double tol);

  bool getSfAdjacencyInfo(boost::shared_ptr<ParamSurface> sf1,
			  boost::shared_ptr<CurveOnSurface> sf_cv1,
			  boost::shared_ptr<ParamSurface> sf2,
			  boost::shared_ptr<CurveOnSurface> sf_cv2,
			  double tol,
			  int& bd1, int& bd2, bool& same_orient);

  bool getCorrCoefEnum(boost::shared_ptr<SplineSurface> sf1,
		       boost::shared_ptr<SplineSurface> sf2,
		       int bd1, int bd2, bool same_orient,
		       std::vector<std::pair<int,int> >& enumeration);

  bool getCoefEnumeration(boost::shared_ptr<SplineSurface> sf, int bd,
			  std::vector<int>& enumeration);

} // namespace Go





#endif // _SURFACETOOLS_H

