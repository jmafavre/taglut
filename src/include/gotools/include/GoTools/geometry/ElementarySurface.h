//===========================================================================
// GoTools Core - SINTEF Geometry Tools Core library, version 2.0.1
//
// Copyright (C) 2000-2007, 2010 SINTEF ICT, Applied Mathematics, Norway.
//
// This program is free software; you can redistribute it and/or          
// modify it under the terms of the GNU General Public License            
// as published by the Free Software Foundation version 2 of the License. 
//
// This program is distributed in the hope that it will be useful,        
// but WITHOUT ANY WARRANTY; without even the implied warranty of         
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
// GNU General Public License for more details.                           
//
// You should have received a copy of the GNU General Public License      
// along with this program; if not, write to the Free Software            
// Foundation, Inc.,                                                      
// 59 Temple Place - Suite 330,                                           
// Boston, MA  02111-1307, USA.                                           
//
// Contact information: E-mail: tor.dokken@sintef.no                      
// SINTEF ICT, Department of Applied Mathematics,                         
// P.O. Box 124 Blindern,                                                 
// 0314 Oslo, Norway.                                                     
//
// Other licenses are also available for this software, notably licenses
// for:
// - Building commercial software.                                        
// - Building software whose source code you wish to keep private.        
//===========================================================================

#ifndef _ELEMENTARYSURFACE_H
#define _ELEMENTARYSURFACE_H



#include "GoTools/geometry/ParamSurface.h"


namespace Go
{


class SplineSurface;


/// \brief ElementarySurface is a base class for elementary surfaces
/// like planes and cylinders. Such surfaces have natural
/// parametrizations and ElementartSurface is therefore a subclass of
/// ParamSurface. These surfaces are non-self-intersecting.
class ElementarySurface : public ParamSurface
{
public:
    /// Virtual destructor, enables safe inheritance.
    virtual ~ElementarySurface();

    // --- Functions inherited from GeomObject ---
    virtual ElementarySurface* clone() const = 0;

    // --- Functions inherited from ParamSurface ---

    RectDomain containingDomain() const;

    virtual Point closestInDomain(double u, double v) const;

    /// Check if a parameter pair lies inside the domain of this surface
    virtual bool inDomain(double u, double v) const;

    virtual SplineSurface* geometrySurface() const = 0;

    virtual double area(double tol) const;
};


} // namespace Go



#endif // _ELEMENTARYSURFACE_H

