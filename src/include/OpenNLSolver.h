/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-GE CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef OPENNL_SOLVER
#define OPENNL_SOLVER

#include "ONL_opennl.h"
#include "IDTypes.h"
#include "Exception.h"

namespace Taglut {


  class OpenNLSolver {
  private:
    NLContext context;
    bool openWriting;
    bool openSystem;
    bool openMatrix;
    bool isSolved;
    unsigned int size;

  public:
    /** default constructor */
    OpenNLSolver(unsigned int size_t);

    /** destructor */
    ~OpenNLSolver();

    /** accessor */
    inline unsigned int getSize() const { return size; }

    /** set is symmetric */
    void setIsSymmetric(bool value = true) const;

    /** set a matrix value */
    void addMatrixValue(VertexID i, VertexID j, float value);

    /** add a vector value */
    void addVectorValue(VertexID i, float value);

    /** set a vector value */
    void setVectorValue(VertexID i, float value);

    /** return the variable part of the solution */
    void setVariable(VertexID id, float value);

    float getVariable(VertexID id) const;

    /** solver */
    bool solve();
  };
}

#endif
