/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-GE CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "OpenNLSolver.h"

using namespace Taglut;

OpenNLSolver::OpenNLSolver(unsigned int size_t) : size(size_t) {
  context = nlNewContext();
  nlSolverParameteri(NL_NB_VARIABLES, size_t);
  openWriting = false;
  openSystem = false;
  openMatrix = true;
  isSolved = false;
}
/** destructor */
OpenNLSolver::~OpenNLSolver() {
  nlDeleteContext(context);
}

/** set is symmetric */
void OpenNLSolver::setIsSymmetric(bool value) const {
  nlMakeCurrent(context);
  nlSolverParameteri(NL_SYMMETRIC, value);
}

/** set a matrix value */
void OpenNLSolver::addMatrixValue(VertexID i, VertexID j, float value) {
  nlMakeCurrent(context);
  if (!openMatrix)
    throw Exception("setMatrixValue(3): cannot set matrix element.");
  if (!openSystem) {
    nlBegin(NL_SYSTEM);
    openSystem = true;
  }
  if (!openWriting) {
    nlBegin(NL_MATRIX);
    openWriting = true;
  }
  nlMatrixAdd(i, j, value);
}

/** add a vector value */
void OpenNLSolver::addVectorValue(VertexID i, float value) {
  nlMakeCurrent(context);

  if (!openSystem) {
    nlBegin(NL_SYSTEM);
    openSystem = true;
  }
  if (!openWriting) {
    nlBegin(NL_MATRIX);
    openWriting = true;
  }
  nlRightHandSideAdd(i, value);
}

/** set a vector value */
void OpenNLSolver::setVectorValue(VertexID i, float value) {
  nlMakeCurrent(context);

  if (!openSystem) {
    nlBegin(NL_SYSTEM);
    openSystem = true;
  }
  if (!openWriting) {
    nlBegin(NL_MATRIX);
    openWriting = true;
  }
  nlRightHandSideAdd(i, value);
}

/** return the variable part of the solution */
void OpenNLSolver::setVariable(VertexID id, float value) {
  nlMakeCurrent(context);

  if (!openSystem) {
    nlBegin(NL_SYSTEM);
    openSystem = true;
  }

  nlSetVariable(id, value);
}

/** solver */
bool OpenNLSolver::solve() {
  nlMakeCurrent(context);

  if (openWriting) {
    nlEnd(NL_MATRIX);
    openWriting = false;
  }
  if (openSystem) {
    nlEnd(NL_SYSTEM);
    openSystem = false;
  }
  openMatrix = false;

  isSolved = nlSolveAdvanced(NULL, NL_TRUE) == NL_TRUE;
  return isSolved;
}

/** return the variable part of the solution */
float OpenNLSolver::getVariable(VertexID id) const {
  if (!isSolved)
    throw Exception("getVariable(1): system not yet solved");
  nlMakeCurrent(context);
  return nlGetVariable(id);
}
