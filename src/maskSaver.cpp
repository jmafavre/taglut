/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <popt.h>
#include "CImgUse.h"

#include "Messages.h"
#include "Exception.h"
#include "Mesh.h"

using namespace std;
using namespace Taglut;

static char*  filename  = NULL;
static char*  filename3d  = NULL;
static char*  oName  = NULL;
static double resolution = 1.0;
static double zratio = 1.0;
static int    help = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input image", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename3d, 0, "Output 3d file", NULL},
  { "name", 'n', POPT_ARG_STRING, &oName, 0, "Object name", NULL},
  { "res", 'r', POPT_ARG_DOUBLE | POPT_ARGFLAG_SHOW_DEFAULT, &resolution, 0, "Resolution used by marching cube", NULL},
  { "zratio", 'z', POPT_ARG_DOUBLE | POPT_ARGFLAG_SHOW_DEFAULT, &zratio, 0, "Zratio of the image", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};




int main (int argc, const char **argv) {
  poptContext context = poptGetContext("maskSaver", argc, argv, options, 0);

  /* check values */

  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mask saver", "Given a binary mask (dicom file), build corresponding mesh using marching cubes algorithm and save it.");
    hMsg << "Input: 3d image (available format: dicom)";
    hMsg << "Output: save resulting mesh (available format: VRML)";
    cout << hMsg << endl;
    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if (filename3d == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }

  if (resolution <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: resolution value <= 0." << endl;
    return 1;
  }


  /* load image */
  CImg<> img(filename);


  /* build 3d mesh */
  CImg<> points;
  CImgList<long unsigned int> faces;
  points = img.get_isosurface3d(faces, 1, (int)img._width / resolution, (int)img._height / resolution, (int)img._depth / resolution);
  cimg_forX(points, p) {
    points(p, 2) *= zratio;
  }
  Mesh mesh(points, faces);

  /* save to file */

  try {
    mesh.save(filename3d, oName);

  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  poptFreeContext(context);
  return 0;
}


