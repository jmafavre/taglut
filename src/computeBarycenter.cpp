/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <popt.h>
#include "CImgUse.h"

#include "Messages.h"

using namespace std;
using namespace Taglut;

static char*  filename  = NULL;
static int    help = 0;
static double zratio = 1.0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input image", NULL},
  { "zratio", 'z', POPT_ARG_DOUBLE, &zratio, 0, "Zratio", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};




int main (int argc, const char **argv) {
  poptContext context = poptGetContext("computeBarycenter", argc, argv, options, 0);

  /* check values */

  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Image viewer", "Display image.");
    hMsg << "Input: image to view";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }



  try {
    /* load image */
    CImg<> img(filename);
    double xSum = 0;
    double ySum = 0;
    double zSum = 0;
    unsigned int nb = 0;

    // check voxels
    cimg_forXYZ(img, x, y, z)
      if (img(x, y , z) != 0) {
	++nb;
	xSum += x;
	ySum += y;
	zSum += z;
      }

    if (nb != 0) {
      xSum /= nb;
      ySum /= nb;
      zSum /= nb;
    }

    zSum *= zratio;

    std::cout << "x: " << xSum << " y: " << ySum << " z: " << zSum << std::endl;

  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  poptFreeContext(context);
  return 0;
}
