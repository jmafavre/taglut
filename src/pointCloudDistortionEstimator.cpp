/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "Point3D.h"
#include "PointCloud.h"
#include "PointCloudShapeEstimator.h"

#include <unistd.h>
#include "Messages.h"
#include "CImgUse.h"
#include "Display3D.h"
#include "DisplayGL.h"
#include "StringManipulation.h"

using namespace Taglut;

static char*  filename_input          = NULL;
static char*  filename_output         = NULL;
static int    help                    = 0;
static int    display                 = 0;
static int    sphere_resolution       = 2;
static double surface_ratio           = .2;
static int    use_real_size           = 0;
static double maximum_size            = std::numeric_limits<double>::max();
static int    no_draw_normal          = 0;
static int    draw_only_bd_edges      = 0;
static int    draw_edges              = 0;
static double normal_size             = 5;
static double maxAngle                = 3 * M_PI / 8;
static int    normal_method           = 1;
static double pruneRatio              = 0.0;
static int    boundary_estimation     = 0;
static int    use_boundary_flag       = 0;
static int    use_boundary_distortion = 0;
static double angle_error             = M_PI / 6;
static int    nb_points_on_edges      = 0;
static int    no_use_octree           = 0;
static int    radius_method           = 0;
static int    neighbour_number        = 8;
static double radius_value            = 1.;
static double octree_depth            = 5;
static double octree_max_nb           = 18;
static double threshold_mean          = std::numeric_limits<double>::max();
static int    use_threshold           = 0;
static char*  unwantedpoints          = NULL;
static int    draw_normal_with_axes   = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input file", NULL},
  { "prune-ratio", 'p', POPT_ARG_DOUBLE, &pruneRatio, 0, "Prune ratio, between 0 and 1. Default: 0.", NULL},
  { "sphere-resolution", 'r', POPT_ARG_INT, &sphere_resolution, 0, "Sphere resolution (number of subdivision steps from the icosahedron). Default: 2.", NULL},
  { "radius-method", 'm', POPT_ARG_INT, &radius_method, 0, "Radius computation method. 0: estimation; 1: by neighbour number; 2: by value. Default: 2.", NULL},
  { "radius-value", 0, POPT_ARG_DOUBLE, &radius_value, 0, "Radius value (only for radius computation by value). It will be multiplied by the smallest distance between points on cloud.", NULL},
  { "neighbour-number", 0, POPT_ARG_INT, &neighbour_number, 0, "Neighbour number (only for radius computation by neighbours).", NULL},
  { "surface-ratio", 0, POPT_ARG_DOUBLE, &surface_ratio, 0, "Surface ratio of the sphere used to compute the normal direction, when a global ratio method is used. Default: 0.2.", NULL},
  { "max-angle", 0, POPT_ARG_DOUBLE, &maxAngle, 0, "Maximum angle value for radius computation. Value between 0 and pi/2. Default: 3pi/8 ~= 1.18.", NULL},
  { "octree-depth", 0, POPT_ARG_INT, &octree_depth, 0, "Maximum depth of the octree structure.", NULL},
  { "octree-max-nb", 0, POPT_ARG_INT, &octree_max_nb, 0, "Maximum number of points in octree leaves.", NULL},
  { "no-use-octree", 0, POPT_ARG_NONE, &no_use_octree, 0, "Do not use an octree structure to speedup the normal computation.", NULL},
  { "normal-method", 0, POPT_ARG_INT, &normal_method, 0, "Normal computation method. Available values: 0 (using main connected component), 1 (using a PCA approach). Default: 1.", NULL},
  { "nb-points-on-edges", 0, POPT_ARG_INT, &nb_points_on_edges, 0, "Number of points in the circle edges. Default: 0.", NULL},
  { "boundary-estimation", 'b', POPT_ARG_NONE, &boundary_estimation, 0, "Estimation of the boundary distortion.", NULL},
  { "angle-error", 'a', POPT_ARG_DOUBLE, &angle_error, 0, "Minimum angle value for boundary detection. Default: pi/6 ~= 0.52.", NULL},
  { "threshold_mean", 0, POPT_ARG_DOUBLE, &threshold_mean, 0, "Threshold value used during the global estimator computation (points with radius higher than this value * minimum distance in the point cloud will be ignored). Default: 5.", NULL},
  { "unwanted-points", 0, POPT_ARG_STRING, &unwantedpoints, 0, "A list of points (separated by comma) that will not been used for global distortion computation.", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output PLY file that will contains the normal estimations for every point", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display object after curvature computation", NULL},
  { "draw-normal-with-axes", 'd', POPT_ARG_NONE, &draw_normal_with_axes, 0, "Draw normal using axes.", NULL},
  { "use-real-size", 0, POPT_ARG_NONE, &use_real_size, 0, "Use the real size of the balls for the display mode.", NULL},
  { "maximum-size", 0, POPT_ARG_DOUBLE, &maximum_size, 0, "Use the given value as maximum size of the balls for the display mode.", NULL},
  { "no-draw-normal", 'n', POPT_ARG_NONE, &no_draw_normal, 0, "Do not draw the computed normals.", NULL},
  { "draw-only-bd-edges", 0, POPT_ARG_NONE, &draw_only_bd_edges, 0, "Draw only the edges corresponding to neighbourhoods on the boundary.", NULL},
  { "draw-edges", 'e', POPT_ARG_NONE, &draw_edges, 0, "Draw the edges corresponding to neighbourhoods.", NULL},
  { "normal-size", 0, POPT_ARG_DOUBLE, &normal_size, 0, "Size of the normals according to the associated spheres. Default: 5.", NULL},
  { "use-boundary-flag", 0, POPT_ARG_NONE, &use_boundary_flag, 0, "Use boundary flag as color rather than local distortion.", NULL},
  { "use-boundary-distortion", 0, POPT_ARG_NONE, &use_boundary_distortion, 0, "Use boundary distortion as color rather than local distortion.", NULL},
  { "use-threshold", 0, POPT_ARG_NONE, &use_threshold, 0, "Use threshold to hide non used points for the mean computation.", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("Global curvature of point cloud", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (sphere_resolution < 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: sphere resolution should be a positive integer." << endl;
    return 1;
  }


  if ((surface_ratio <= 0.) || (surface_ratio >= 1.)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the surface ratio have to be betwen 0.0 and 1.0." << endl;
    return 1;
  }

  if ((maxAngle <= 0.) || (maxAngle > M_PI / 2)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: the maximum angle value have to be betwen 0.0 and pi / 2." << endl;
    return 1;
  }

  if ((use_boundary_flag == 1) && (use_boundary_distortion == 1))
    cout << "Warning: more than one coloration flag has been selected. Using boundary flag." << endl;

  if (sphere_resolution > 6) {
    cout << "Warning: sphere resolution > 6 will produce a big sphere" << endl;
  }

  if (octree_max_nb <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: bad maximum number of points in octree leaves." << endl;
    return 1;
  }

  if (octree_depth <= 0) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: bad maximum octree depth." << endl;
    return 1;
  }

  if ((radius_method < 0) || (radius_method > 2)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: bad radius computation method." << endl;
    return 1;
  }

  if ((normal_method < 0) || (normal_method >= 2)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: bad normal computation method." << endl;
    return 1;
  }
  enum PointCloudShapeEstimator::NormalMethod nMethod = PointCloudShapeEstimator::NMMainCC;
  if (normal_method == 1)
    nMethod = PointCloudShapeEstimator::NMPCA;
  if ((boundary_estimation == 1) || (use_boundary_flag == 1) || (use_boundary_distortion == 1)) {
    if (normal_method == 1)
      nMethod = PointCloudShapeEstimator::NMPCAandMainCC;
    else
      nMethod = PointCloudShapeEstimator::NMMainCCandPCA;
  }

  if (help != 0) {
    HelpMessage hMsg("pointCloudShapeEstimator", "Computing for the given point cloud various shape measures.");
    hMsg << "Input: point cloud to use. Format: \"x1 y1 z1 ... xn yn zn\". Separators can be space, tabulation, new line. Lines starting with \"#\" are ignored. 3D mesh format can also be used (PLY, VRML, etc.).";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }


  try {
    cout << "Loading point cloud..." << std::endl;
    PointCloud cloud(filename_input);

    if (cloud.size() == 0) {
      std::cout << "The cloud point is empty. Abort." << std::endl;
      return 1;
    }
    cout << " " << cloud.size() << " points has been loaded" << std::endl;

    if (pruneRatio > 0) {
      cout << "Using a random prune method (ratio: " << pruneRatio << ")" << endl;
      cout << " " << (cloud.size() - cloud.prune(pruneRatio).size()) << " points has been removed" << endl;
    }


    cout << "Normal estimator initialization..." << std::endl;
    PointCloudShapeEstimator pcEstimator(cloud, sphere_resolution, no_use_octree == 0, nMethod, octree_max_nb, octree_depth);
    pcEstimator.setNbPointsOnShpereEdges((unsigned int) nb_points_on_edges);
    pcEstimator.setAngleErrorOnBoundary(angle_error);
    if (radius_method == 0)
      pcEstimator.radiusByEstimation(surface_ratio, maxAngle);
    else if (radius_method == 1)
      pcEstimator.radiusByNbNeighbours(neighbour_number);
    else // if (radius_method == 2)
      pcEstimator.radiusByValue(radius_value);

    /* process mesh */
    cout << "Processing normal estimation..." << endl;
    if (unwantedpoints != NULL) {
      std::deque<std::string> params = StringManipulation::splitString(std::string(unwantedpoints), ',');
      for(PointCloud::iterator p = cloud.begin(); p != cloud.end(); ++p)
	(*p).setFlag(1);
      for(std::deque<std::string>::const_iterator param = params.begin(); param != params.end(); ++param)
	if (StringManipulation::isUnsignedInt(*param)) {
	  VertexID p = StringManipulation::getInt(*param);
	  if (p < cloud.size())
	    cloud[p].setFlag(0);
	  else
	    std::cout << "Warning: " << p << " is out of box" << std::endl;
	}
	else
	  std::cout << "Warning: " << *param << " is not a valid index value." << std::endl;
      cout << "Global shape estimator: " << pcEstimator.getMeanLocalDistortionFlags() << std::endl;
    }
    else
      cout << "Global shape estimator: " << pcEstimator.getMeanLocalDistortionThreshold(threshold_mean) << std::endl;
    cout << "Local shape estimator: " << std::endl;
    cout << " Minimum: " << pcEstimator.getMinLocalDistortion() << std::endl;
    cout << " Maximum: " << pcEstimator.getMaxLocalDistortion() << std::endl;
    cout << " Mean: " << pcEstimator.getMeanLocalDistortion() << std::endl;
    cout << " Median: " << pcEstimator.getMedianLocalDistortion() << std::endl;
    if ((boundary_estimation == 1) || (use_boundary_flag == 1) || (use_boundary_distortion == 1) || (draw_only_bd_edges == 1)) {
      cout << "Boundary shape estimator: " << pcEstimator.computeBoundaryShapeEstimator() << std::endl;
      cout << " Minimum: " << pcEstimator.getMinLocalBoundaryDistortion() << std::endl;
      cout << " Maximum: " << pcEstimator.getMaxLocalBoundaryDistortion() << std::endl;
    }

    if (filename_output != NULL) {
      std::cout << "Saving normals (" << filename_output << ")" << std::endl;
      cloud.save(filename_output);
    }

    enum DisplayPointCLoudGL::SphereColor color =  DisplayPointCLoudGL::SCDistortion;
    if (use_boundary_flag == 1)
      color =  DisplayPointCLoudGL::SCBoundary;
    else if (use_boundary_distortion == 1)
      color =  DisplayPointCLoudGL::SCBoundaryDistortion;

    if (display != 0) {
      std::cout << "Visualization" << std::endl;
      const std::string title = "3D visualization";
      DisplayPointCLoudGL d(512, 512, title, pcEstimator, true, !draw_normal_with_axes, color, use_real_size == 1, draw_edges == 1, draw_only_bd_edges == 1,
			    draw_normal_with_axes && (no_draw_normal == 0), normal_size,
			    unwantedpoints != NULL,
			    use_threshold == 1, threshold_mean,
			    maximum_size);
      d.disp();
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
