/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "MeshPart.h"
#include "MeshPathes.h"
#include "MeshCut.h"
#include "Display3D.h"
#include "Length.h"
#include "NLoop.h"

#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    display = 0;
static int    cComponent = 0;
static int    help = 0;
static int    drawEdges = 0;
static int    drawPathes = 0;
static int    mergePoints = 0;
static int    cMethod = 0;
static int    maxCut = -1;
static int    method = 0;
static char*  length = NULL;
static double ratio = -1;
static int    unstickMethod = 0;
static int    optimMethod = 3;
static int    logMesh = 0;
static char * logMeshPrefix = NULL;
static int    simulatedAnnealing = 0;
static double simTempInit = 20.;
static int    simStepSize = 50;
static double simTempCoeff = 0.9;
static double simAcceptThreshold = 0.05;
static double simTempMin = 1e-3;
static int    simAMethod = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML or PLY file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "connected-component", 'j', POPT_ARG_INT, &cComponent, 0, "Connected component in object", NULL},
  { "method", 't', POPT_ARG_INT, &method, 0, "Method used during cut (0: simple; 1: quadrangulation, 2: by cylinders).", NULL},
  { "cutting-method", 'c', POPT_ARG_INT, &cMethod, 0, "Method used during the first cut step (0: Erickson; 1: Erickson+, 2: old).", NULL},
  { "merge-multipoints", 'm', POPT_ARG_INT, &mergePoints, 0, "Method used to merge multipoints (0: none, 1: simple, 2: one point).", NULL},
  { "maximum-cut", 'x', POPT_ARG_INT, &maxCut, 0, "Maximum cutting iteration.", NULL},
  { "pathes", 'p', POPT_ARG_NONE, &drawPathes, 0, "Draw pathes with different colors.", NULL},
  { "edges", 'e', POPT_ARG_NONE, &drawEdges, 0, "Draw edges", NULL},
  { "length", 'l', POPT_ARG_STRING, &length, 0, "Select length method used to cut surface (or \"list\" for list them)", NULL},
  { "ratio-length", 'a', POPT_ARG_DOUBLE, &ratio, 0, "Ratio of euclidean length that can be modified by other length methods. 0 <= ratio <= 1", NULL},
  { "optim-method", 0, POPT_ARG_INT, &optimMethod, 0, "Optimization method (0: no trunc, 1: trunc minimum, 2: trunc maximum, 3: trunc minimum and maximum, 4: simple, 5: by strip, 6: by strip and pred cut; 1000: approximation)", NULL},
  { "unstick-method", 0, POPT_ARG_INT, &unstickMethod, 0, "Unsticking method (0: none, 1: fast, 2: full)", NULL},
  { "simulated-annealing", 's', POPT_ARG_NONE, &simulatedAnnealing, 0, "Adjust the location using simulated annealing", NULL},
  { "temp-init", 0, POPT_ARG_DOUBLE, &simTempInit, 0, "Initial temperature (simulated annealing)", NULL},
  { "step-size", 0, POPT_ARG_INT, &simStepSize, 0, "Step size between two temperature modifications (simulated annealing)", NULL},
  { "temp-coeff", 0, POPT_ARG_DOUBLE, &simTempCoeff, 0, "Coefficient of the geometric progression (simulated annealing)", NULL},
  { "accept-threshold", 0, POPT_ARG_DOUBLE, &simAcceptThreshold, 0, "Value for acceptation (simulated annealing)", NULL},
  { "temp-min", 0, POPT_ARG_DOUBLE, &simTempMin, 0, "Minimal temperature value (simulated annealing)", NULL},
  { "sa-method", 0, POPT_ARG_INT, &simAMethod, 0, "Selection method for the next NLoop (simulated annealing). 0: simple, 1: by middle.", NULL},
  { "log-mesh", 0, POPT_ARG_NONE, &logMesh, 0, "Save intermediate meshes after each cutting step.", NULL},
  { "log-mesh-prefix", 0, POPT_ARG_STRING, &logMeshPrefix, 0, "Prefix of the log mesh.", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  LengthFactory lFactory;

  poptContext context = poptGetContext("meshCutter", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh cutter", "Cut input a selected component of the input mesh with minimal method.");
    hMsg << "Input: mesh to cut (available format: VRML)";
    hMsg << "Output: display cut mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if ((length != NULL) && (strcmp(length, "list") == 0)) {
    cout << lFactory << endl;
    return 0;
  }

  lFactory.setLength(length);
  if (!lFactory.existsLength()) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: length unknown (try -l list)." << endl;
    return 1;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
  }

  std::string logMeshPrefixString;
  if (logMeshPrefix != NULL)
    logMeshPrefixString = logMeshPrefix;
  if ((logMeshPrefixString == "") and (logMesh != 0))
    logMeshPrefixString = "/tmp/log";

  MeshCut::MergeMPointsMethod mmp = MeshCut::MMPnone;
  MeshCut::CuttingMethod cm = MeshCut::CMEricksonPlus;
  MeshManipulator::UnstickMethod um = MeshManipulator::UMNone;
  MeshPathes::OptimEC oec = MeshPathes::OECTruncMinMax;
  SimulatedAnnealingNLoop::SAMethod sam = SimulatedAnnealingNLoop::SASimple;

  if (simAMethod == 1)
    sam = SimulatedAnnealingNLoop::SAByMiddle;
  else if (simAMethod != 0)
    std::cout << "Unknown selection method for the next NLoop in simulated annealing" << std::endl;

  if (mergePoints == 1)
    mmp = MeshCut::MMPsimple;
  else if (mergePoints == 2)
    mmp = MeshCut::MMPonePoint;

  if (cMethod == 1)
    cm = MeshCut::CMEricksonPlus;
  else if (cMethod == 2)
    cm = MeshCut::CMOld;

  if (unstickMethod == 0)
    um = MeshManipulator::UMNone;
  else if (unstickMethod == 1)
    um = MeshManipulator::UMFast;
  else if (unstickMethod == 2)
    um = MeshManipulator::UMPreserveMesh;
  else
    std::cout << "Unknown unsticking method, using a cutting method without unsticking." << std::endl;

  if (optimMethod == 0)
    oec = MeshPathes::OECnoTrunc;
  else if (optimMethod == 1)
    oec = MeshPathes::OECTruncMin;
  else if (optimMethod == 2)
    oec = MeshPathes::OECTruncMax;
  else if (optimMethod == 3)
    oec = MeshPathes::OECTruncMinMax;
  else if (optimMethod == 4)
    oec = MeshPathes::OECsimple;
  else if (optimMethod == 5)
    oec = MeshPathes::OECbyStrip;
  else if (optimMethod == 6)
    oec = MeshPathes::OECbyStripAndPredCut;
  else if (optimMethod == 1000)
    oec = MeshPathes::OECApproximation;
  else
    std::cout << "Unknown optimization method, using a non-optimized algorithm." << std::endl;

  if ((method == 1) && (um == MeshManipulator::UMNone)) {
    std::cout << "Warning: an unsticking method is required for the quadrangulation. Selecting the method that preserve the mesh." << std::endl;
    um = MeshManipulator::UMPreserveMesh;
  }

  if ((mergePoints != 0) && (um == MeshManipulator::UMNone)) {
    std::cout << "Warning: an unsticking method is required for the cutting with merging method. Selecting the method that preserve the mesh." << std::endl;
    um = MeshManipulator::UMPreserveMesh;
  }


  cout << "Building mesh..." << endl;
  Mesh mesh;

  /* load mesh */
  try {
    mesh.load(filename_input, objectName);
  } catch (Exception e) {
    cerr << e << endl;
    return -1;
  }




  cout << "Checking for topological properties...";
  std::deque<MeshPart> cc = mesh.getConnectedComponents();
  if (((int)cc.size() <= cComponent) || (cComponent < 0)) {
    cerr << "Error: connected component #" << cComponent << " unknown. Surface has " << cc.size() << " connected components."<< endl;
    return 1;
  }
  cout << " OK" << endl;

  cout << "Selecting connected component..." << endl;
  mesh = cc[cComponent];

  cout << "Computing graph cut..." << endl;
  if (ratio >= 0)
    lFactory.setRatio(ratio);
  MeshCut meshCut(mesh, lFactory.getLength(mesh));

  try {
    cout << " Number of points before cut: " << mesh.getNbPoints() << endl;
    if (method == 2)
      meshCut.buildCylinderCutUsingNLoops(NLoopEvaluatorLength(), simTempInit, simStepSize, simTempCoeff, simAcceptThreshold, simTempMin, sam);
    else if (method == 1)
      meshCut.buildRectangularCut(maxCut, cm, mmp, um, oec, "", logMeshPrefixString);
    else
      meshCut.buildMinimalCut(maxCut, cm, mmp, um, oec, "", logMeshPrefixString);
    cout << " Number of points after cut: " << meshCut.getCNbPoints() << endl;

    if (display != 0) {
      if (drawPathes != 0)
	Display3D::displayMeshPathes(256, 256, "3D Visualisation (crop disc)", meshCut, drawEdges != 0);
      else
	Display3D::displayMesh(256, 256, "3D Visualisation (crop disc)", meshCut.cutMesh(), drawEdges != 0, true);
    }

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      meshCut.cutMesh();
      mesh.save(filename_output, objectName);
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
