/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "CImgUse.h"
#include "Mesh.h"
#include "Display3D.h"
#include "MeshPart.h"
#include "Point3D.h"

#include <unistd.h>
#include "Messages.h"
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  objectName = NULL;
static int    display = 0;
static int    help = 0;
static int    edges = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML file", NULL},
  { "name", 'n', POPT_ARG_STRING, &objectName, 0, "Object name", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output VRML file", NULL},
  { "display", 'd', POPT_ARG_NONE, &display, 0, "Display instead of saving image", NULL},
  { "edges", 'e', POPT_ARG_NONE, &edges, 0, "display edges", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("Mesh simplifier", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mesh simplifier", "Simplify a mesh.");
    hMsg << "Input: mesh to cut (available format: VRML, PLY, ...)";
    hMsg << "Output: display cut mesh or save it (available format: VRML)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((filename_output == NULL) && (display == 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file or select display mode." << endl;
    return 1;
    }


  try {
    /* build mesh */
    Mesh mesh(filename_input, objectName);


    /* process mesh */
    cout << "Processing..." << endl;
    /* mettre votre code ici */

    /* Critere en utilisant les normales et les variances*/

    int nberr = 0;
    int nbr_iter = 0;
    int nbr_pts_suppr = 0;
    int nbr_pts_init = mesh.getNbPoints();
    int taille_liste ;
    int nbr_voi_cour1;
    int seed;
    int erreur, neigh;
    VertexID idcour1, idcour2, idvoisin_cour1;
    VertexID idv11, idv12;
    float moyX, moyY, moyZ, varX, varY, varZ;
    Coord3D nt1, nt2, nt;
    Triangle t1, t2;
    int choix;
    int nbr_iter_unmaj = 0;

    /*    cout << " " << endl;
    cout << " Forcez le calcul de la variance,etc... ? oui = 1 , non = 0 " << endl;
    cout << " \t Choisir non en cas d'incertitude " << endl;
    cout << " " << endl;
    cin >> choix;*/
    choix = 1;

    /* Choix de la m�thode pour obtenir la graine */

    /* ------------------------------------- */
    /* Valeur de la graine :                 */
    /* N = New software ; O = Old software   */
    /* 1 = forc�        ; 0 = pas forc�      */
    /* ------------------------------------- */
    /* 100: O0 ok                            */
    /* 100: O1 crash 600eme                  */
    /* ------------------------------------- */
    /* 5281:  O0 ok                          */
    /* 5281:  O1 crash 600eme                */
    /* ------------------------------------- */

    /* Valeur de la graine fixe */
   seed = 100;

    /* Valeur de la graine al�atoire*/
   // seed = getpid();

    /* Valeur de la graine entr�e */
    //    cout <<  "Entrez la valeur de la graine: " << endl;
    //cin >> seed;

    srand48(100);

    while( nbr_pts_suppr < 0.8*nbr_pts_init && nbr_iter < 10000 && nbr_iter_unmaj < 1000)
    {
	taille_liste = mesh.getNbPoints();
	nbr_iter_unmaj ++;

	/* choix al�atoire d'un point */
	idcour1 = (VertexID)floor(drand48()*(taille_liste)) ;
	Point3D & cour1 = mesh.point(idcour1);
	nbr_voi_cour1 = cour1.getNbNeighbours();
	
	/* affichage de certaines propri�t�s de ce point */
	cout << "cour1 est un point de bord : " << cour1.getIsBoundary() << endl;
	cout << "identifiant de cour1 : " << idcour1 << endl;
	cout << "nbr de voisin de cour1 : " << nbr_voi_cour1 << endl;
	cout << " " << endl;

	/* calcul d'un bool�en indiquant si le point est triangul� � 360� */
	/* erreur = 0 si point oui */
	/* erreur = 1 sinon */
	erreur = 0;
	for (int i = 0; i < nbr_voi_cour1 ; i++)
	{
		neigh = 0;
		for (unsigned int j = 0; j < mesh.point(cour1.getNeighbour(i)).getNbNeighbours() ; j++)
			if (cour1.getNeighbour((i+1)%nbr_voi_cour1) == mesh.point(cour1.getNeighbour(i)).getNeighbour(j)) neigh = 1;
		if (!neigh)
		{
			erreur = 1;
			cout << "Erreur !!" << endl;
			cout << " " << endl;
			cout << "Le point " << cour1.getNeighbour((i+1)%nbr_voi_cour1) << " n'est pas voisin du point " << cour1.getNeighbour(i) << endl;
			cout << " " << endl;
		}
	}

	if (erreur == 1) nberr++;

	if (choix == 1)
		erreur = 0;

	if (erreur == 0)
	{
		
		/* initialisation des normes, des moyennes et des carres */
		nt1 = Coord3D(0,0,0);
		nt2 = Coord3D(0,0,0);
		varX = 0.0;
		moyX = 0.0;
		varY = 0.0;
		moyY = 0.0;
		varZ = 0.0;
		moyZ = 0.0;
	

		/* Calcul de la moyenne E(X) */
		for (int i = 0; i < nbr_voi_cour1 ; i++)
		{
			/* Les voisins sont ordonn�s => cour1, v11 et v12 forment un triangle (si aucun bords) */
			idv11 = cour1.getNeighbour(i);
			idv12 = cour1.getNeighbour((i+1)%nbr_voi_cour1);

			/* calcul de la normale � ce triangle */
			t1 = mesh.findTriangle(idcour1, idv11, idv12);
			nt = mesh.computeTriangleNormal(t1.getId()).normalize();
		
			float nx = nt.getX();
			float ny = nt.getY();
			float nz = nt.getZ();

			/*
			cout << "\tnorme en X du triangle Num " << i+1 << " : " << nx << endl;
			cout << "\tnorme en Y du triangle Num " << i+1 << " : " << ny << endl;
			cout << "\tnorme en Z du triangle Num " << i+1 << " : " << nz << endl;
			cout << " " << endl;
			*/
	
			/* mise � jour de la moyenne */
			nt1 = nt1 + nt.normalize();
			moyX = moyX +  nx ;
			moyY = moyY +  ny ;
			moyZ = moyZ +  nz ;
		}
	
		moyX = moyX / nbr_voi_cour1;
		moyY = moyY / nbr_voi_cour1;
		moyZ = moyZ / nbr_voi_cour1;
	
		/* Calcul de la variance: E(X-E(x))� */
		for (int i = 0; i < nbr_voi_cour1 ; i++)
		{
			idv11 = cour1.getNeighbour(i);
			idv12 = cour1.getNeighbour((i+1)%nbr_voi_cour1);
		
			/* calcul de la normale � ce triangle */
			t1 = mesh.findTriangle(idcour1, idv11, idv12);
			nt = mesh.computeTriangleNormal(t1.getId()).normalize();
		
			float nx = nt.getX();
			float ny = nt.getY();
			float nz = nt.getZ();
		
			varX = varX+ (nx-moyX)*(nx-moyX) ;
			varY = varY+ (ny-moyY)*(ny-moyY) ;
			varZ = varZ+ (nz-moyZ)*(nz-moyZ) ;
			
		}
	
		varX = varX / nbr_voi_cour1 ;
		varY = varY / nbr_voi_cour1 ;
		varZ = varZ / nbr_voi_cour1 ;

		/*
		cout << "variance en X de cour1:    " << varX << endl;
		cout << "variance en Y de cour1:    " << varY << endl;
		cout << "variance en Z de cour1:    " << varZ << endl;
		*/
		
		if ( varX < 0.05 && varY < 0.05 && varZ < 0.05)
		{
			/* choix al�atoire d'un voisin de cour1 */
			idcour2 = cour1.getNeighbour((VertexID)floor(drand48()*(cour1.getNbNeighbours()))) ;
			Point3D & cour2 = mesh.point(idcour2);
			
			cout << "identifiant du courant2 choisi: " << idcour2 << endl;
			cout << "nbr de voisins de cour2: " << cour2.getNbNeighbours() << endl;
	
			Mesh mesh2 = mesh;
			try {
			  mesh.mergePointsOnFirst(idcour2, idcour1);
			}
			catch (Exception e) {
			  mesh2.save("/tmp/test.wrl");
			  throw Exception(e);
			}
			cout << "un de moins !" << endl;
			nbr_pts_suppr++;
			nbr_iter_unmaj = 0;
	
			/*
			for (int i = 0; i < cour2.getNbNeighbours() ; i++)
			{
			
				idv11 = cour2.getNeighbour(i);
				idv12 = cour2.getNeighbour((i+1)%cour2.getNbNeighbours());
				
				
				cout << "\t \t identifiant du voisin " << i << " : " << idv11 << endl;
				
				for (int ii = 0; ii < mesh.point(idv11).getNbNeighbours(); ii++)
				{
					cout << "\t identifiant du voisin " << ii << " du voisin "<< i << " : " << mesh.point(idv11).getNeighbour(ii) << endl;
				}
		
				cout << " " << endl;
			}*/
			
			/*
			
			for (int j = 0; j < cour2.getNbNeighbours(); j++)
			{
				idv21 = cour2.getNeighbour(j);// triangle idcour2, idv21, idv22
				idv22 = cour2.getNeighbour((j+1)%cour2.getNbNeighbours());
				t2 = mesh.findTriangle(idcour2, idv21, idv22);
				nt2 = nt2 + mesh.computeTriangleNormal(t2.getId()).normalize();
				
			}
			nt2.normalize();
		
			if (cour1.scalarProduct(nt1,nt2) > 0.95)
			{ 
				cout << "un de moins !" << endl;
				VertexID id = mesh.mergePointsOnMiddle(idcour1, idcour2);
				nbr_pts_suppr++;
			}*/
		}
	}
	else
	{
		//Affichage des voisins de chacun des voisins de cour1 
		for (int i = 0; i < nbr_voi_cour1 ; i++)
		{
			idvoisin_cour1 = cour1.getNeighbour(i);
			cout << "\t identifiant du voisin " << i << " de cour1 : " << idvoisin_cour1 << endl;
			
			for (unsigned int j = 0; j < mesh.point(idvoisin_cour1).getNbNeighbours(); j++)
			{
				cout << "\t\t identifiant du voisin " << j << " du voisin "<< i << " de cour1: " << mesh.point(idvoisin_cour1).getNeighbour(j) << endl;
			}
	
			cout << " " << endl;
		}
	}

	nbr_iter++;
	cout << "nb d'iteration: " << nbr_iter << endl;
	cout << "nb d'erreur: " << nberr << endl;
	cout << "valeur de la graine : " << seed << endl;
	cout << "---------------------------------------------- " << endl;
    } // fin while
    
    cout << "nb de points initial : " << nbr_pts_init << endl;
    cout << "nb de points simplifies : " << nbr_pts_suppr << endl;	
    /* output: display and/or save mesh */
    if (display != 0)
      Display3D::displayMesh(256, 256, "3D Visualisation", mesh, edges != 0, true);

    if (filename_output != NULL) {
      cout << "Saving file..." << endl;
      if (objectName != NULL)
	mesh.save(string(filename_output), string(objectName));
      else
	mesh.save(string(filename_output));
    }

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
