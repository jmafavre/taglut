#include "PLY.h"
#include "Mesh.h"
#include "Mapping2D3D.h"
#include "MeshMap.h"
#include "StringManipulation.h"

using namespace Taglut;

void PLY::reset() {
    properties.clear();
    vertices.clear();
    triangles.clear();
}

PLYLoader::PLYLoader() {
    addFormats();
}

void PLYLoader::addFormats() {
    addLoadFormat("ply");
}


PLYLoader::PLYLoader(const std::string & filename, const std::string &) {
    addFormats();
    load(filename);
}

void PLYLoader::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string &) {
  if (altFormat == "ply")
    loadPLY(fileName);
  else
    throw Exception("Unknown format");
}

void PLYLoader::loadPLY(const std::string & fileName) {
    std::ifstream infile(fileName.c_str());

    if (!infile.is_open())
        throw Exception("Cannot open file");

    reset();

    std::string line, secondLine;
    getline(infile, line);
    getline(infile, secondLine);
    if (!(((line == "ply") || (line == "ply\r")) &&
        ((secondLine == "format ascii 1.0") || (secondLine == "format ascii 1.0\r"))))
       throw Exception("Bad header");

    unsigned long int nbPt = 0;
    unsigned long int nbPrimitives = 0;
    std::vector<std::map<std::string, std::vector<double> >::iterator> props;

    // read the header
    getline(infile, line);
    unsigned int active = 0; // 1: vertex, 2: faces
    while((line != "end_header") && (line != "end_header\r") && !infile.eof()) {
        std::deque<std::string> words = StringManipulation::splitString(line, ' ', true);
        if (words.size() != 0) {
            if (words.front() == "element") {
                if (words.size() == 3) {
                    if (words[1] == "vertex") {
                        active = 1;
                        nbPt = StringManipulation::getInt(words[2]);
                    }
                    else if (words[1] == "face") {
                        active = 2;
                        nbPrimitives = StringManipulation::getInt(words[2]);
                    }
                }
            }
            else if (words.front() == "property") {
                if (words.size() > 2) {
                    if (words[1] == "list") {
                        if (active == 2)  {
                            if ((words.size() < 5) ||
                                    ((words[4] != "vertex_indices") &&
                                     (words[4] != "vertex_index")))
                                throw Exception("Bad list for face description");
                        }
                        else
                            throw Exception("List associated to a vertex");
                    }
                    else if ((words[1] == "float") ||
                             (words[1] == "double")) {
                        if (active == 1) {
                            if ((words[2] != "x") && (words[2] != "y") && (words[2] != "z")) {
                                properties[words[2]] = std::vector<double>();
                                props.push_back(properties.find(words[2]));
                            }
                        }
                    }
                }
            }
          }
        getline(infile, line);
    }

    // read vertices
    for(unsigned long int v = 0; v != nbPt; ++v)  {
        double x, y, z;
        infile >> x >> y >> z;
        vertices.push_back(Coord3D(x, y, z));
        for(std::vector<std::map<std::string, std::vector<double> >::iterator>::iterator p = props.begin(); p != props.end(); ++p) {
            double v;
            infile >> v;
            (**p).second.push_back(v);
        }
    }

    // read faces
        for(unsigned long int v = 0; v != nbPrimitives; ++v)  {
            unsigned int n;
            unsigned long int a, b, c, d;
            infile >> n;
            if (n >= 3) {
                infile >> a >> b >> c;
                triangles.push_back(Triangle(a, b, c));
                if (n == 4) {
                    infile >> d;
                    triangles.push_back(Triangle(d, a, c));
                }
                else if (n > 4)
                    std::cout << "Warning: cannot handle a too complicated face" << std::endl;
            }
            else
                std::cout << "Warning: cannot handle a too basic face" << std::endl;
        }
}

void PLYLoader::loadMeshFromPLY(Mesh & mesh) const {
    mesh.buildMesh(vertices, triangles);
}

void PLYLoader::loadMapping2D3DFromPLY(Mapping2D3D & mapping) const {
    std::map<std::string, std::vector<double> >::const_iterator u = properties.find("s");
    std::map<std::string, std::vector<double> >::const_iterator v = properties.find("t");
    if ((u != properties.end()) && (v != properties.end())) {
        const std::vector<double> & ul = (*u).second;
        const std::vector<double> & vl = (*v).second;
        if((mapping.getMesh().getNbPoints() == ul.size()) && (mapping.getMesh().getNbPoints() == vl.size())) {
            std::list<Point2D> p2D;
            std::vector<double>::const_iterator vi = vl.begin();
            for(std::vector<double>::const_iterator ui = ul.begin(); ui != ul.end(); ++ui, ++vi)
                p2D.push_back(Point2D(*ui, *vi));
            mapping.setPoints(p2D);
        }
        else
            std::cerr << "Cannot read scalar values from file: wrong number of values" << std::endl;
    }
}

void PLYLoader::loadMeshMapFromPLY(MeshMap & mmap) const {
    std::map<std::string, std::vector<double> >::const_iterator scalars = properties.find("intensity");
    if (scalars != properties.end()) {
      const std::vector<double> & sl = (*scalars).second;
      if(mmap.getMesh().getNbPoints() == sl.size()) {
        mmap = sl;
      }
      else
          std::cerr << "Cannot read scalar values from file: wrong number of values" << std::endl;
    }
}

/** PLY saver */

PLYSaver::PLYSaver() {

}

void PLYSaver::addFormats() {
    addSaveFormat("ply");
}

void PLYSaver::initDataFromMesh(const Mesh & mesh) {
    triangles.clear();
    vertices.clear();

    vertices.insert(vertices.begin(), mesh.getPoints().begin(), mesh.getPoints().end());
    triangles.insert(triangles.begin(), mesh.getTriangles().begin(), mesh.getTriangles().end());
}

void PLYSaver::initDataFromMapping(const Mapping2D3D & mapping) {
    properties["s"] = std::vector<double>();
    properties["t"] = std::vector<double>();
    for(Mapping2D3D::const_iterator p = mapping.begin(); p != mapping.end(); ++p) {
        properties["s"].push_back((*p).get2DX());
        properties["t"].push_back((*p).get2DY());
    }
}

void PLYSaver::initDataFromMap(const MeshMap & mmap) {
    properties["intensity"] = std::vector<double>();
    for(VertexID id = 0; id != mmap.getNbValues(); ++id)
        properties["intensity"].push_back(mmap.getValue(id));
}

void PLYSaver::savePLY(const std::string & fileName) const {
  if (vertices.empty())
    return;
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
      throw ExceptionCannotOpenOutputFile();
  outfile << "ply" << std::endl;
  outfile << "format ascii 1.0" << std::endl;
  outfile << "comment   Generated by PLYSaver" << std::endl;

  outfile << "element vertex " << vertices.size() << std::endl;
  outfile << "property float x" << std::endl;
  outfile << "property float y" << std::endl;
  outfile << "property float z" << std::endl;
  std::vector<std::vector<double>::const_iterator> itProps;
  for(std::map<std::string, std::vector<double> >::const_iterator p = properties.begin(); p != properties.end(); ++p) {
      outfile << "property double " << (*p).first << std::endl;
      assert((*p).second.size() == vertices.size());
      itProps.push_back((*p).second.begin());
  }

  outfile << "element face " << triangles.size() << std::endl;
  outfile << "property list uchar int vertex_indices" << std::endl;
  outfile << "end_header" << std::endl;

  for(std::vector<Coord3D>::const_iterator p = vertices.begin(); p != vertices.end(); ++p) {
      outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ();
      for(std::vector<std::vector<double>::const_iterator>::iterator pr = itProps.begin(); pr != itProps.end(); ++pr) {
          outfile << " " << **pr;
          ++(*pr);
      }
      outfile << std::endl;
  }

  for(std::vector<Triangle>::const_iterator t = triangles.begin(); t != triangles.end(); ++t)
      outfile << "3 " << (*t).getP1() << " " << (*t).getP2() << " " << (*t).getP3() << std::endl;

  outfile.close();
}

void PLYSaver::saveAltFormat(const std::string & fileName, const std::string &,
                             const std::string &) const {
    savePLY(fileName);
}

void PLYSaver::savePLY(const Mesh & mesh, const std::string & fileName) {
    initDataFromMesh(mesh);
    savePLY(fileName);
}

void PLYSaver::savePLY(const MeshMap & mmap, const std::string & fileName) {
    reset();
    initDataFromMesh(mmap.getMesh());
    initDataFromMap(mmap);
    savePLY(fileName);
}

void PLYSaver::savePLY(const Mapping2D3D & mapping, const std::string & fileName) {
    reset();
    initDataFromMapping(mapping);
    initDataFromMesh(mapping.getMesh());
    savePLY(fileName);
}

void PLYSaver::savePLY(const Mapping2D3D & mapping, const MeshMap & mmap, const std::string & fileName) {
    reset();
    initDataFromMapping(mapping);
    initDataFromMesh(mmap.getMesh());
    initDataFromMap(mmap);
    savePLY(fileName);
}

