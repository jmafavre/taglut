/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "NLoopFactory.h"
#include "VectorField.h"
#include "PLPath.h"
#include "NLoop.h"
#include "PreNLoop.h"

using namespace Taglut;


class byProximityLSV {
private:
  const PreNLoop & pnl;

public:
  byProximityLSV(const PreNLoop & p) : pnl(p) { }

  bool operator()(const std::vector<PreNLoop>::iterator & pn1,
		 const std::vector<PreNLoop>::iterator & pn2) const {
    const double d1 = fabs(pnl.getLevelSetValue() - (*pn1).getLevelSetValue());
    const double d2 = fabs(pnl.getLevelSetValue() - (*pn2).getLevelSetValue());
    return d1 > d2;
  }

};

NLoopFactory::NLoopFactory() : splitForNLoop(true),
			       merge(true),
			       adjustByAngles(false),
			       epsilonForMerge(0.),
			       epsilonForSnap(0.) {

}

Pre3Loop NLoopFactory::getPre3LoopFromSaddlePoint(const Point3D & point,
						  const Mesh & mesh,
						  const MeshMap & mMap, const VectorField & vf) {
  return Pre3Loop(point, mesh, mMap, vf);
}




std::vector<PreNLoop> NLoopFactory::getPreNLoopsFromSaddlePoints(const Mesh & mesh,
								 const MeshMap & mMap, const VectorField & vf) {
  std::vector<PreNLoop> result;

  // first build the pre-n-loops
  std::vector<std::pair<VertexID, unsigned int> > sps = mMap.getSaddlePoints();
  for(std::vector<std::pair<VertexID, unsigned int> >::const_iterator sp = sps.begin(); sp != sps.end(); ++sp) {
    if ((*sp).second == 2) {
      Pre3Loop pre3loop;
      try {
	pre3loop = getPre3LoopFromSaddlePoint(mesh.point((*sp).first), mesh, mMap, vf);
	if ((pre3loop.getFirstIntegralLine().length() == 0.) ||
	    (pre3loop.getSecondIntegralLine().length() == 0.))
	  continue;
	assert(pre3loop.getFirstIntegralLine().length() != 0.);
	assert(pre3loop.getSecondIntegralLine().length() != 0.);
      }
      catch(Exception e) {
	continue;
      }
      if ((merge || splitForNLoop) && (result.size() != 0)) {
	bool inserted = false;
	std::vector<PreNLoop>::iterator pnlInserted;
	for(std::vector<PreNLoop>::iterator pnl = result.begin(); pnl != result.end(); ++pnl) {
 	  assert(pre3loop.getFirstIntegralLine().length() != 0.);
	  assert(pre3loop.getSecondIntegralLine().length() != 0.);
	  if ((*pnl).intersects(pre3loop, mMap)) {
	    if (!inserted) {
	      try {
		(*pnl).addPre3Loop(pre3loop);
		inserted = true;
		pnlInserted = pnl;
		assert((*pnl).isValidLevelSet(mMap));
	      }
	      catch(...) {
		inserted = false;
	      }
	    }
	    else {
	      try {
		(*pnlInserted).addPre3Loops((*pnl).getPre3Loops());
		assert((*pnlInserted).isValidLevelSet(mMap));
		(*pnl).clear();
	      }
	      catch(...) {

	      }
	    }
	  }
	}
	if (!inserted) {
	  result.push_back(PreNLoop(pre3loop));
	}
      }
      else {
	result.push_back(PreNLoop(pre3loop));
      }
    }
    else {
      std::cout << "Warning: a non regular saddle point has been detected. Cannot build an n-loop on it." << std::endl;
    }
  }

  if (splitForNLoop) {
    std::vector<PreNLoop> result2;
    for(std::vector<PreNLoop>::iterator pnl = result.begin(); pnl != result.end(); ++pnl)
      if (!(*pnl).empty()) {
	if ((*pnl).isValidLevelSet(mMap) && (*pnl).isWellOrdered(mMap)) {
	  result2.push_back(*pnl);
	}
	else {
	  std::vector<PreNLoop> sp = (*pnl).split(mMap);
	  result2.insert(result2.end(), sp.begin(), sp.end());
	}
      }
    result = result2;
  }
  else {
    std::vector<PreNLoop> result2;
    for(std::vector<PreNLoop>::iterator pnl = result.begin(); pnl != result.end(); ++pnl)
      if (!(*pnl).empty())
	result2.push_back(*pnl);
    result = result2;
  }


  if (epsilonForMerge > 0.) {
    std::vector<std::pair<PreNLoop::Signature, PreNLoop::Signature> > forbidden;
    bool merged = true;
    // while possible
    while (merged) {
      merged = false;
      //  find the best compatible couple of n-loop
      std::vector<PreNLoop>::iterator couple[2] = { result.end(), result.end() };
      double distance = std::numeric_limits<double>::max();
      for(std::vector<PreNLoop>::iterator pnl = result.begin(); pnl != result.end(); ++pnl) {
	for(std::vector<PreNLoop>::iterator pnl2 = pnl + 1; pnl2 != result.end(); ++pnl2) {
	  const double newDistance = fabs((*pnl).getLevelSetValue() - (*pnl2).getLevelSetValue());
	  if ((newDistance < distance) &&
	      (epsilonForMerge > newDistance) &&
	      ((*pnl).isCompatible(*pnl2, mMap)) &&
	      PreNLoop::isUnknownPair(*pnl, *pnl2, forbidden)) {
	    couple[0] = pnl;
	    couple[1] = pnl2;
	    distance = newDistance;
	    merged = true;
	    break;
	  }
	}
      }

      //  merge it
      if (merged) {
	const unsigned int nb = (*(couple[0])).getNbPre3Loop();
	(*(couple[0])).addPreNLoop(*(couple[1]), mMap);
	assert((*(couple[0])).getNbPre3Loop() >= nb);
	if ((*(couple[0])).getNbPre3Loop() != nb) {
	  result.erase(couple[1]);
	}
	else {
	  merge = false;
	  forbidden.push_back(std::pair<PreNLoop::Signature, PreNLoop::Signature>((*(couple[0])).getSignature(),
										  (*(couple[1])).getSignature()));
	}
	assert((*(couple[0])).isValidLevelSet(mMap));
      }

    }

  }
  return result;
}


std::vector<NLoop> NLoopFactory::getNLoopsFromSaddlePoints(Mesh & mesh,
							   const MeshMap & mMap, const VectorField & vf) {
  // first compute the pre-n-loops using the scalar function
  std::vector<PreNLoop> preNLoops = getPreNLoopsFromSaddlePoints(mesh, mMap, vf);
  std::vector<NLoop> result;

  for(std::vector<PreNLoop>::const_iterator preNLoop = preNLoops.begin(); preNLoop != preNLoops.end(); ++preNLoop) {
    result.push_back((*preNLoop).getNLoop(mesh, mMap, vf, adjustByAngles));
  }
  return result;
}

std::vector<PLNLoop> NLoopFactory::getPLNLoopsFromSaddlePoints(Mesh & mesh,
							       const MeshMap & mMap, const VectorField & vf) {
  // first compute the pre-n-loops using the scalar function
  std::vector<PreNLoop> preNLoops = getPreNLoopsFromSaddlePoints(mesh, mMap, vf);
  std::vector<PLNLoop> result;

  for(std::vector<PreNLoop>::const_iterator preNLoop = preNLoops.begin(); preNLoop != preNLoops.end(); ++preNLoop) {
    result.push_back((*preNLoop).getPLNLoop(mesh, mMap, vf, adjustByAngles));
    if (result.back().getNbPaths() == 0)
      result.pop_back();
  }
  return result;
}

std::vector<PLPath> NLoopFactory::getNLoopsAsPLPathFromSaddlePoints(Mesh & mesh, const MeshMap & mMap,
								    const VectorField & vf) {
  std::vector<PLNLoop> nloops = getPLNLoopsFromSaddlePoints(mesh, mMap, vf);
  std::vector<PLPath> result;

  for(std::vector<PLNLoop>::const_iterator nloop = nloops.begin(); nloop != nloops.end(); ++nloop) {
    const std::vector<PLPath> & paths = (*nloop).getPaths();
    result.insert(result.end(), paths.begin(), paths.end());
  }

  if (epsilonForSnap != 0.)
    return PLPath::fitToVertices(result, epsilonForSnap);
  else
    return result;
}

