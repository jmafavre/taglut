/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMap.h"

#ifndef NLOOP_FACTORY_H
#define NLOOP_FACTORY_H

#include <vector>
#include "IDTypes.h"
#include "Exception.h"
#include "Mesh.h"

namespace Taglut {
  class PLPath;
  class VectorField;
  class NLoop;
  class PreNLoop;
  class Pre3Loop;
  class PLNLoop;

  /**
     @class NLoopFactory
     a NLoop factory
  */
  class NLoopFactory {
  private:
    bool splitForNLoop;
    bool merge;
    bool adjustByAngles;
    double epsilonForMerge;
    double epsilonForSnap;

  public:

    NLoopFactory();

    /** modifier
	If true (and in case of merge = true), split the pre-n-loops after the merging step
	in order to produce only well-ordered n-loops
    */
    inline NLoopFactory & setSplitForNLoop(bool v = true) {
      splitForNLoop = v;
      return *this;
    }

    /** modifier
	If true, merge the intersected 3-loops.
 */
    inline NLoopFactory & setMerge(bool v = true) {
      merge = v;
      return *this;
    }

    /** modifier */
    inline NLoopFactory & setAdjustByAngles(bool v = true) {
      adjustByAngles = v;
      return *this;
    }

    /** modifier */
    inline NLoopFactory & setEpsilonForMerge(double v = 0.) {
      epsilonForMerge = v;
      return *this;
    }

    /** modifier */
    inline NLoopFactory & setSnapToVertices(double epsilon = 0.1) {
      epsilonForSnap = epsilon;
      return *this;
    }

    /**
       Build a pre-NLoop from a given saddle point using integral lines
       and level sets from the \p MeshMap (and the corresponding \p VectorField)
       on the given mesh. Return both the paths of the pre-loop, and a boolean
       that describes the orientation of the saddle point.
    */
    Pre3Loop getPre3LoopFromSaddlePoint(const Point3D & point,
					const Mesh & mesh,
					const MeshMap & mMap, const VectorField & vf);

    /**
       Build the pre-n-loops from the saddle points computed on the scalar function
       described by the mesh map.
       The results in PreNLoop formats may
       be a non n-loop because the intersection points between the level set and the integral lines
       depends of the arity of the structure.
    */
    std::vector<PreNLoop> getPreNLoopsFromSaddlePoints(const Mesh & mesh,
						       const MeshMap & mMap,
						       const VectorField & vf);


    /**
       Build the n-loops from the saddle points computed on the scalar function
       described by the mesh map, using \p getPreNLoopsFromSaddlePoints
    */
    std::vector<NLoop> getNLoopsFromSaddlePoints(Mesh & mesh, const MeshMap & mMap, const VectorField & vf);

    /**
       Build the n-loops from the saddle points computed on the scalar function
       described by the mesh map, using \p getPreNLoopsFromSaddlePoints
    */
    std::vector<PLNLoop> getPLNLoopsFromSaddlePoints(Mesh & mesh, const MeshMap & mMap, const VectorField & vf);

    /**
       Build the n-loops from the saddle points computed on the scalar function
       described by the mesh map, using \p getPreNLoopsFromSaddlePoints
       return the result as a vector of PLPath
    */
    std::vector<PLPath> getNLoopsAsPLPathFromSaddlePoints(Mesh & mesh, const MeshMap & mMap, const VectorField & vf);
  };

}

#endif
