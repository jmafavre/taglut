/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Vertex.h"
#include "Triangle.h"

using namespace Taglut;

Vertex::Vertex() : Node() {
  isBoundary = false;
}

Vertex::Vertex(const Vertex & v) : Node(v), triangles(v.triangles) {
  isBoundary = v.isBoundary;
}

Vertex & Vertex::operator=(const Vertex & v) {
  *((Node*)this) = v;
  isBoundary = v.getIsBoundary();
  triangles = v.getTriangles();
  return *this;
}


void Vertex::clearNeighboursAndTriangles() {
  neighbours.clear();
  triangles.clear();
}


void Vertex::incIds(VertexID oPoints, TriangleID oTriangles) {
  id += oPoints;

  for(std::deque<VertexID>::iterator i = neighbours.begin(); i != neighbours.end(); ++i)
    *i += oPoints;

  for(std::deque<TriangleID>::iterator i = triangles.begin(); i != triangles.end(); ++i)
    *i += oTriangles;
}


bool Vertex::addNeighbour(VertexID neighbour) {
  if (!hasNeighbour(neighbour)) {
    neighbours.push_back(neighbour);
    return true;
  }
  else
    return false;
}



bool Vertex::addNeighbour(const Vertex & neighbour) {
  return addNeighbour(neighbour.getId());
}


void Vertex::addNeighbourBetweenPoints(VertexID neighbour, VertexID idN1, VertexID idN2) {
  if (neighbours.size() < 2)
    throw Exception(std::string("Error: cannot insert point between points (less than 2 points)."));
  for(std::deque<VertexID>::iterator i = neighbours.begin(); i != neighbours.end(); ++i)
    if ((*i == idN1) || (*i == idN2)) {
      if (i == neighbours.begin()) {
	++i;
	if (!((*i == idN1) || (*i == idN2))) {
	  if ((neighbours.front() == idN1) || (neighbours.front() == idN2)) {
	    neighbours.push_back(neighbour);
	    return;
	  }
	  else
	    throw Exception(std::string("Error: cannot insert point between non successive points"));
	}
      }
      else
	++i;
      if (i == neighbours.end())
	neighbours.push_back(neighbour); // add point at end of list
      else
	neighbours.insert(i, neighbour); // add point between idN1 and idN2
      return;
    }
}


void Vertex::replaceTriangle(TriangleID oldTriangle, TriangleID newTriangle, bool expt) {
  for(std::deque<TriangleID>::iterator i = triangles.begin(); i != triangles.end(); ++i)
    if (*i == oldTriangle) {
      *i = newTriangle;
      return;
    }

  if (expt) {
    std::cerr << "Replace triangle " << oldTriangle << " by " << newTriangle << " in " << getId() << ": failed." << std::endl;
    throw Exception(std::string("Point not found. Cannot be replaced."));
  }
}




void Vertex::removeTriangle(TriangleID t, bool expt) {
  for(std::deque<TriangleID>::iterator i = triangles.begin(); i != triangles.end(); ++i)
    if (*i == t) {
      triangles.erase(i);
      return;
    }

  if (expt)
    throw Exception(std::string("Triangle not found. Cannot be removed."));
}





bool Vertex::hasTriangle(TriangleID t) const {
  if (triangles.size() == 0)
    return false;
  for(std::deque<TriangleID>::const_iterator i = triangles.begin(); i != triangles.end(); ++i)
    if ((*i) == t)
      return true;
  return false;
}


void Vertex::addTriangle(const VertexID idP1, const VertexID idP2,
			 const TriangleID idT) {
  triangles.push_back(idT);
  addNeighbour(idP1);
  addNeighbour(idP2);
}

void Vertex::addTriangle(const Triangle & t) {
  triangles.push_back(t.getId());
  for(unsigned int i = 0; i < 3; ++i)
    if (t.getVertexId(i) != id)
      addNeighbour(t.getVertexId(i));
}


void Vertex::addTriangle(const TriangleID idT) {
  triangles.push_back(idT);
}


VertexID Vertex::getNeighbourOtherSide(VertexID & v) const {
  std::deque<VertexID>::const_iterator current = neighbours.begin();
  while(*current != v) {
    current = nextNeighbourIt(current);
    if (current == neighbours.begin())
      throw Exception("getNeighbourOtherSide(1): given point not in the neighbourhood");
  }
  for(unsigned int i = 0; i < getNbNeighbours() / 2; ++i)
    current = nextNeighbourIt(current);

  return *current;
}



VertexID Vertex::findNextNeighbour(const VertexID idN1, const VertexID idN2) const {
  std::deque<VertexID>::const_iterator last = neighbours.begin();
  for(std::deque<VertexID>::const_iterator i = neighbours.begin(); i != neighbours.end(); ++i) {
    if (*i == idN1) {
      if (++i == neighbours.end()) i = neighbours.begin();
      if (*i != idN2) {
	if (neighbours.back() == idN2)
	  return *(++(neighbours.rbegin()));
	else
	  throw Exception(std::string("findNextNeighbour: points are not successive."));
      }
      if (++i == neighbours.end()) i = neighbours.begin();
      return *i;
    }
    if (*i == idN2) {
      if (i == neighbours.begin()) {
	if (++i == neighbours.end()) i = neighbours.begin();
	if (*i != idN1) {
	  if (neighbours.back() != idN1)
	    throw Exception(std::string("findNextNeighbour: points are not successive."));
	  return *i;
	}
	else
	  return neighbours.back();
      }
      else
	return *last;
    }
    last = i;
  }

  throw Exception(std::string("findNextNeighbour: points not found."));
  return 0;
}


void Vertex::inverseNeighbourList() {
  std::deque<VertexID> neighboursBis;
  for(std::deque<VertexID>::const_iterator j = neighbours.begin(); j != neighbours.end(); ++j)
    neighboursBis.push_front(*j);
  neighbours = neighboursBis;
}



bool Vertex::orderNeighbours(const std::vector<Triangle> & trianglesList, bool warning) {
  std::deque<VertexID> sortedNeighbours;
  bool brk = false;
  bool invert = false;

  /* order neighbours allows to compute more easily angles.
     Direct or indirect order is not important */

  if (neighbours.size() <= 1)
    return true;

  sortedNeighbours.push_back(neighbours.back());


  bool * seenTriangles = new bool[triangles.size()];
  for(TriangleID i = 0; i < triangles.size(); ++i)
    seenTriangles[i] = false;


  while(neighbours.size() != sortedNeighbours.size()) {
    /* find sortedNeighbours.back() neighbour in neighbours */
    VertexID c = sortedNeighbours.back();
    brk = false;
    unsigned int it = 0;
    for(std::deque<TriangleID>::const_iterator i = triangles.begin(); i != triangles.end(); ++i) {
      if (trianglesList[*i].hasPoint(c) && (!seenTriangles[it])) {
	assert(*i == trianglesList[*i].getId());
	seenTriangles[it] = true;
	sortedNeighbours.push_back(trianglesList[*i].getOtherPoint(c, getId()));
	brk = true;
	break;
      }
      ++it;
    }
    if (!brk) {
      if (invert) {
	if (warning) {
	  std::cout << "Warning: neighbours cannot be ordered (" << getId() << ")" << std::endl;
	  std::cout <<   " neighbours: ";
	  for(std::deque<VertexID>::const_iterator i = getNeighbours().begin(); i != getNeighbours().end(); ++i)
	    std::cout << " " << *i;
	  std::cout <<  std::endl << " triangles: ";
	  for(std::deque<VertexID>::const_iterator i = getTriangles().begin(); i != getTriangles().end(); ++i)
	    std::cout << " " << *i;
	  std::cout <<  std::endl;
	}
	return false; // neighbours couldn't be ordered
      }
      else {
	invert = true;
	std::deque<VertexID> sortedNeighboursBis;
	for(std::deque<VertexID>::const_iterator j = sortedNeighbours.begin(); j != sortedNeighbours.end(); ++j)
	  sortedNeighboursBis.push_front(*j);
	sortedNeighbours = sortedNeighboursBis;
      }
    }
  }


  delete [] seenTriangles;
  neighbours = sortedNeighbours;
  return true;
}


VertexID Vertex::getPointSameSide(VertexID p1, VertexID p2, VertexID p3) const {
  VertexID lastP = neighbours.back();
  std::deque<VertexID>::const_iterator nb = neighbours.begin();

  // first find p1
  while(*nb != p1) {
    lastP = *nb;
    nb = nextNeighbourIt(nb);
  }

  bool order = (lastP == p3);
  if (!order) {
    nb = nextNeighbourIt(nb);
    if (*nb != p3) {
      std::cout << "on trouve " << *nb << " au lieu de "<< p3 << " dans getPointSameSide(" << p1 << ", " << p2 << ", " << p3 << ") sur " << *this << std::endl;
      throw Exception("Points are not successive.");
    }
  }

  while(*nb != p2) {
    lastP = *nb;
    nb = nextNeighbourIt(nb);
    if (*nb == p1)
      throw Exception("Given points are not neighbours of the current one.");
  }

  if (order)
    return *nextNeighbourIt(nb);
  else
    return lastP;
}


bool Vertex::neighboursValidOrder(VertexID v1, VertexID v2) const {
  for(std::deque<VertexID>::const_iterator i = getNeighbours().begin(); i != getNeighbours().end(); ++i)
    if (*i == v2) {
      if (i == getNeighbours().begin()) {
	// (a): look for the last one: is it v1? (we are working on a cyclic neighbourhood)
	return (getNeighbours().back() == v1);
      }
      else
	return false;
    }
    else if (*i == v1) {
      const std::deque<VertexID>::const_iterator nexti = i + 1;
      if (i == getNeighbours().end())
	return false; // this configuration has been checked in (a)
      else
	return (*nexti == v2);
    }

  return false;
}

