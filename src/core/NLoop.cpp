/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "NLoop.h"
#include "MeshManipulator.h"

using namespace Taglut;

NLoop::NLoop() : basePoint(0) {

}

NLoop::NLoop(VertexID basePoint_t) : basePoint(basePoint_t) {

}

NLoop::NLoop(const NLoop & nloop) : basePoint(nloop.basePoint), paths(nloop.paths) {

}

NLoop::~NLoop() {

}


VertexID NLoop::getLastPoint() const {
  if ((paths.size() == 0) || (paths.front().size() == 0))
    throw Exception("Empty n-loop");
  VertexID result = paths.front().front();

  for(std::vector<std::vector<VertexID> >::const_iterator p = paths.begin(); p != paths.end(); ++p)
    if ((*p).front() != result)
      throw Exception("Incoherent n-loop");

  return result;
}

void NLoop::addPath(const std::vector<VertexID> & path) {
  if (path.size() == 0)
    return;

  if (paths.size() == 0)
    basePoint = path.back();

  if (basePoint != path.back())
    throw Exception("The given path is not defined with the n-loop's base-point.");

  if (paths.size() != 0)
    if (paths.front().front() != path.front())
      throw Exception("The given path is not defined with the n-loop's last point.");
  paths.push_back(path);
}

void NLoop::clear() {
  paths.clear();
}


bool NLoop::isInsideEdge(VertexID v1, VertexID v2) const {
  for(std::vector<std::vector<VertexID> >::const_iterator path = paths.begin(); path != paths.end(); ++path)
      for(std::vector<VertexID>::const_iterator p = (*path).begin(); p != (*path).end(); ++p)
	if ((*p == v1) || (*p == v2)) {
	  std::vector<VertexID>::const_iterator next = p + 1;
	  if (next == (*path).end()) // edge (v1, v2) is not contained on the current path
	    break;
	  else if ((*next == v1) || (*next == v2))
	    return true; // edge (v1, v2) is contained on the current path
	  else
	    break; // edge (v1, v2) is not contained on the current path
	}

  return false;
}

NLoop PLNLoop::convert() const {
  NLoop result;
  // TODO
  return result;
}


NLoopEvaluator::~NLoopEvaluator() {

}



