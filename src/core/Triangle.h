/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TRIANGLE
#define TRIANGLE

#include <assert.h>
#include <deque>
#include <limits>

#include "IDTypes.h"
#include "Vertex.h"
#include "Exception.h"


namespace Taglut {

  class MultiTriangle;


  /**
     @class VTriangle
     @brief Virtual 3D Triangle class
  */
  class VTriangle {
  protected:
    /**
       id of the current triangle
    */
    TriangleID idT;
  public:

    /** default constructor */
    inline VTriangle() { idT = 0; }

    /** constructor with data */
    inline VTriangle(TriangleID idT_l) : idT(idT_l) { }

    /** copy constructor */
    inline VTriangle(const VTriangle & t) : idT(t.idT) { }


    /** destructor */
    inline virtual ~VTriangle() {}

    /**
       return first point's id
    */
    virtual VertexID getP1() const = 0;

    /**
       return second point's id
    */

    virtual VertexID getP2() const = 0;

    /**
       return third point's id
    */
    virtual VertexID getP3() const = 0;

    /* modifiers  */
    /**
       set first point's id
    */
    virtual void setP1(VertexID p) = 0;
    /**
       set second point's id
    */
    virtual void setP2(VertexID p) = 0;
    /**
       set third point's id
    */
    virtual void setP3(VertexID p) = 0;

    /** return the position of the given vertex in the triangle, or 3 if not contained */
    inline unsigned int getVertexPositionById(VertexID p) const {
      if (p == getP1())
	return 0;
      else if (p == getP2())
	return 1;
      else if (p == getP3())
	return 2;
      else return 3;
    }

    /**
       get vertex id
    */
    inline VertexID getVertexId(unsigned int nb) const {
      if (nb == 0) return getP1();
      if (nb == 1) return getP2();
      if (nb == 2) return getP3();
      return 0;
    }

    /**
       set vertex id
    */
    inline void setVertexId(unsigned int nb, VertexID value) {
      if (nb == 0) setP1(value);
      else if (nb == 1) setP2(value);
      else if (nb == 2) setP3(value);
    }


    /**
       Given one point id, return another id contained in current triangle
    */
    VertexID getOtherPoint(VertexID p) const;


    /**
       Given two point id, return other id contained in current triangle
    */
    VertexID getOtherPoint(VertexID p1, VertexID p2) const;


    /* **** Id functions: set and get id **** */
    /**
       Set id value
    */
    inline void setId(TriangleID i) { idT = i; }

    /**
       Increase id value
    */
    inline void incId(int v = 1) { idT += v; }

    /**
       Get id value
    */
    inline TriangleID getId() const { return idT; }

    /**
       Return true if Point v is one of the 3 vertex of current triangle
    */
    inline bool hasPoint(const Vertex & v) const { return ((v.getId() == getP1()) || (v.getId() == getP2()) || (v.getId() == getP3())); }

    /**
       Return true if id v is one of the 3 vertices' id of current triangle
    */
    inline bool hasPoint(VertexID v) const { return ((v == getP1()) || (v == getP2()) || (v == getP3())); }

    /**
       Return the common edge both in current triangle and in t.
       If not exists, throw an exception
    */
    std::pair<VertexID, VertexID> getCommonEdge(const VTriangle & t) const;

    /**
       return true if point has the given edge
    */
    inline bool hasEdge(VertexID v1, VertexID v2) const {
      return hasPoint(v1) && hasPoint(v2);
    }

    /**
       return true if two points of the triangles are identical
    */
    inline bool isEdge() const {
      return (getP1() == getP2()) || (getP1() == getP3()) || (getP3() == getP2());
    }

    /** return the number of similar points shared by the current triangle and the given one */
    inline unsigned char getNbSimilarPoints(const VTriangle & t) {
      return ((hasPoint(t.getP1()) ? 1 : 0) +
	      (hasPoint(t.getP2()) ? 1 : 0) +
	      (hasPoint(t.getP3()) ? 1 : 0));
    }

  };

  /**
     @class Triangle
     @brief 3D Triangle class, contains 3 ordered point ids
  */
  class Triangle : public VTriangle {
  private:
    VertexID p1;
    VertexID p2;
    VertexID p3;
    int tFlag;

  public:
    /**
       Default constructor
    */
    Triangle();

    /**
       Copy constructor
    */
    Triangle(const Triangle & t);

    /**
       Copy constructor
    */
    Triangle(const MultiTriangle & t);

    /**
       Build triangle given points' id and triangle's id
    */
    Triangle(VertexID p1, VertexID p2, VertexID p3, TriangleID idT = 0);

#ifndef SWIG
    /**
       Set operator
    */
    Triangle& operator=(const Triangle & t);

    /**
       Set operator
    */
    Triangle & operator=(const MultiTriangle & t);

#endif

    /* accessors  */
    /**
       return first point's id
    */
    inline VertexID getP1() const { return p1; }
    /**
       return second point's id
    */
    inline VertexID getP2() const { return p2; }
    /**
       return third point's id
    */
    inline VertexID getP3() const { return p3; }

    /* modifiers  */
    /**
       set first point's id
    */
    inline void setP1(VertexID p) { p1= p; }
    /**
       set second point's id
    */
    inline void setP2(VertexID p) { p2 = p; }
    /**
       set third point's id
    */
    inline void setP3(VertexID p) { p3 = p; }


    /**
       Comparison operator (return true if points' id are the same
    */
    inline bool operator== (const Triangle &v) const { return (p1 == v.p1) && (p2 == v.p2) && (p3 == v.p3); }

    /**
       Return true if triangle has same orientation than param vertices
    */
    bool sameOrder(const Vertex & v1, const Vertex & v2, const Vertex & v3) const;

    /**
       Return true if triangle has same orientation than param vertices
    */
    bool sameOrder(VertexID v1, VertexID v2, VertexID v3) const;

    /**
       Return true if triangle has same orientation than param vertices
    */
    bool sameOrder(VertexID v1, VertexID v2) const;

    /**
       Return true if triangle have same points' id
    */
    bool isEqual(VertexID idP1, VertexID idP2, VertexID idP3) const;

    /**
       Return true if triangle have same points' id
    */
    inline bool isEqual(const Triangle & v) const {
      return isEqual(v.getP1(), v.getP2(), v.getP3());
    }

    /**
       Increase ids using given offsets
    */
    void incIds(int oPoints, int oTriangles);


    /* **** Flag functions: set and get values **** */
    /**
       Set flag value
    */
    inline void setFlag(int f) { tFlag = f; }
    /**
       Get flag value
    */
    inline int getFlag() const { return tFlag; }


    /**
       Given first and second id, reorder points
    */
    void reorder(VertexID idP1, VertexID idP2);


    /**
       Set triangle's points
    */
    void setPoints(VertexID pp1, VertexID pp2, VertexID pp3);

    /**
       Replace oldP by newP in triangle
    */
    void replacePoint(VertexID oldP, VertexID newP);

    /**
       Given an edge id, return corresponding points
    */
    inline std::pair<VertexID, VertexID> getEdge(unsigned int idE) const {
      assert(idE < 3);
      if (idE == 0)
	return std::pair<VertexID, VertexID>(p1, p2);
      else
	if (idE == 1)
	  return std::pair<VertexID, VertexID>(p2, p3);
	else // idE == 2
	  return std::pair<VertexID, VertexID>(p3, p1);
    }

    /**
       Given an id on the triangle, return the next one
    */
    inline unsigned int getNextVertexId(VertexID id) const {
      if (id == p1)
	return p2;
      else if (id == p2)
	return p3;
      else if (id == p3)
	return p1;
      else
	throw Exception("getNextVertexId(1): cannot find point on triangle.");
    }

  };


  /**
     @class MultiEdge
     @brief Edge of a MultiTriangle
  */
  class MultiEdge {
  private:
    /** id of the first point  */
    VertexID idFirst;
    /** id of the second point  */
    VertexID idLast;
    /** id of the middle point (if exists)  */
    VertexID idMiddle;

    /** true if edge has middle point*/
    bool hasMiddle;

  public:

    /**
       Default constructor
    */
    MultiEdge();

    /**
       Default constructor of edge of length 1
    */
    MultiEdge(VertexID idFirst, VertexID idLast);

    /**
       Default constructor of edge of length 2
    */
    MultiEdge(VertexID idFirst, VertexID idLast, VertexID idMiddle);

    /**
       Copy constructor
    */
    MultiEdge(const MultiEdge & mEdge);

#ifndef SWIG
    /**
       Set operator
    */
    inline MultiEdge & operator=(const MultiEdge & mEdge) {
      idFirst = mEdge.idFirst;
      idLast = mEdge.idLast;
      idMiddle = mEdge.idMiddle;
      hasMiddle = mEdge.hasMiddle;

      return * this;
    }
#endif

    /**
       Comparison operator
    */
    inline bool operator==(const MultiEdge & mEdge) const {
      return (idFirst == mEdge.idFirst) && (idLast == mEdge.idLast) && (hasMiddle == mEdge.hasMiddle) && (idMiddle == idMiddle);
    }

    /** id modifier */
    inline void setId(VertexID id1, VertexID id2) { idFirst = id1; idLast = id2; }

    /** idFirst modifier */
    inline void setFirstId(VertexID id) { idFirst = id; }

    /** idLast modifier */
    inline void setLastId(VertexID id) { idLast = id; }

    /** idMiddle modifier */
    inline void setMiddleId(VertexID id) { hasMiddle = true; idMiddle = id; }

    /** idFirst accessor */
    inline VertexID getFirstId() const { return idFirst; }

    /** idSecond accessor */
    inline VertexID getSecondId() const { if (hasMiddle) return idMiddle; else return idLast; }

    /** idThird accessor */
    inline VertexID getThirdId() const { if (hasMiddle) return idLast; else throw Exception("This edge only has two points."); }

    /** idLast accessor */
    inline VertexID getLastId() const { return idLast; }

    /** Next to last accessor */
    inline VertexID getNextToLastId() const { if (hasMiddle) return idMiddle; else return idFirst; }

    /** idMiddle accessor */
    inline VertexID getMiddleId() const { return idMiddle; }

    /** hasMiddle accessor */
    inline bool hasMiddlePoint() const { return hasMiddle; }

    /** Return true if edge has third point */
    inline bool hasThirdPoint() const { return hasMiddle; }

    /** Return next point id given the first or the last point */
    inline VertexID getNextId(VertexID id) const { if (id == idFirst) return getSecondId(); else return getNextToLastId(); }

    /** Return other id (not middle one) */
    inline VertexID getOtherId(VertexID id) const { if (id != idFirst) return idFirst; else return idLast; }

    /**
       Return true if the edge has minimal edge given in parameter
    */
    inline bool hasMinimalEdge(VertexID idP1, VertexID idP2) const {
      return((((idP1 == idFirst) && (idP2 == getSecondId())) ||
	      ((idP2 == idFirst) && (idP1 == getSecondId()))) ||
	     (hasMiddle && (((idP1 == idLast) && (idP2 == getSecondId())) ||
			    ((idP2 == idLast) && (idP1 == getSecondId())))));
    }

    /**
       Return length of the edge (number of points)
    */
    inline unsigned int size() const { return (hasMiddle ? 3 : 2); }

    /**
       Adding a point as last. The old last point became the middle one
    */
    inline void addAsLast(VertexID idP) {
      assert(!hasMiddle);
      setMiddleId(idLast);
      setLastId(idP);
    }

    /**
       Adding a point as first. The old first point became the middle one
    */
    inline void addAsFirst(VertexID idP) {
      assert(!hasMiddle);
      setMiddleId(idFirst);
      setFirstId(idP);
    }

    /**
       return the number of boundary edges
    */
    inline unsigned int getNbBEdges() const { return (hasMiddle?2:1); }

    /**
       Return true if i is a point of the current edge
    */
    inline bool hasPoint(VertexID i) const { return (i == idFirst) || (i == idLast) || (hasMiddle && (idMiddle == i)); }

    /**
       Return true if i is a point of the current edge, but not middle
    */
    inline bool hasPointNotMiddle(VertexID i) const { return (i == idFirst) || (i == idLast); }

    /**
       Return edge with order given by (i, j)
    */
    inline MultiEdge getOrdered(VertexID i, VertexID j) const {
      if ((idFirst == i) && (idLast == j))
	return *this;
      else
	return getInverted();
    }

    /**
       Return edge with order given by i first
    */
    inline MultiEdge getOrderedFirst(VertexID i) const {
      if (idFirst == i)
	return *this;
      else
	return getInverted();
    }

    /**
       Return edge with order given by i last
    */
    inline MultiEdge getOrderedLast(VertexID i) const {
      if (idLast == i)
	return *this;
      else
	return getInverted();
    }

    /**
       Return inverted edge
    */
    inline MultiEdge getInverted() const {
      if (hasMiddle)
	return MultiEdge(idLast, idFirst, idMiddle);
      else
	return MultiEdge(idLast, idFirst);
    }

  };



  /**
     @class MultiTriangle
     @brief Triangle used during prune step (multiple small triangles are contains into the large triangle)
  */
  class MultiTriangle : public VTriangle {
  private:
    /**
       Points in boundary
    */
    MultiEdge edges[3];

    /**
       forbidden triangles
    */
    int forbiddenTriangles[3];

    /**
       List of triangles inside the current MultiTriangle
    */
    std::deque<TriangleID> triangles;

  public:
    /**
       Default constructor
    */
    MultiTriangle();

    /**
       Constructor using edges
    */
    MultiTriangle(const MultiEdge & e1, const MultiEdge & e2, const MultiEdge & e3);

    /**
       Copy constructor
    */
    MultiTriangle(const MultiTriangle & t);

    /**
       Build triangle given points' id and triangle's id
    */
    MultiTriangle(VertexID p1, VertexID p2, VertexID p3);

    /**
       Add a point on the boundary
    */
    inline void addBPoint(unsigned int boundaryId, VertexID p) { assert(boundaryId < 3); edges[boundaryId].setMiddleId(p); }


    /**
       Add a point on the boundary defined by id1, id2
    */
    void addBPoint(VertexID id1, VertexID id2, VertexID p);

    /**
       Given a boundary id (between 0 and 2), return the corresponding
       edge
    */
    inline const MultiEdge & getEdge(unsigned int bId) const { assert(bId < 3); return edges[bId]; }


    /**
       Given a boundary id (between 0 and 2), return the corresponding oriented
       edge
    */
    inline MultiEdge getOrientedEdge(unsigned int bId) const {
      assert(bId < 3);
      if (bId == 0)
	return edges[0];
      else if (bId == 1)
	return edges[2];
      else // (bId == 2)
	return edges[1].getInverted();
    }

    /**
       Given a boundary defined by its vertices, return the corresponding
       edge
    */
    inline const MultiEdge & getEdge(VertexID idP1, VertexID idP2) const {
      for(unsigned int i = 0; i < 3; ++i)
	if (edges[i].hasPointNotMiddle(idP1) && edges[i].hasPointNotMiddle(idP2))
	  return edges[i];
      throw Exception("MultiEdge cannot be found in triangle.");
    }

    /**
       Given a boundary id (between 0 and 2), return the corresponding
       edge
    */
    inline MultiEdge & getEdge(unsigned int bId) { assert(bId < 3); return edges[bId]; }

    /**
       Return true if the triangle has minimal edge given in parameter
    */
    inline bool hasMinimalEdge(VertexID idP1, VertexID idP2) const { return edges[0].hasMinimalEdge(idP1, idP2) || edges[1].hasMinimalEdge(idP1, idP2) || edges[2].hasMinimalEdge(idP1, idP2); }

    /**
       Return the multiedge containing the given small edge
    */
    inline const MultiEdge & getEdgeContaining(VertexID idP1, VertexID idP2) const {
      if (edges[0].hasMinimalEdge(idP1, idP2))
	return edges[0];
      else if (edges[1].hasMinimalEdge(idP1, idP2))
	return edges[1];
      else if (edges[2].hasMinimalEdge(idP1, idP2))
	return edges[2];
      else
	throw Exception();
    }

    /**
       Return the multiedge containing the given small edge
    */
    inline std::deque<MultiEdge> getEdgesContaining(VertexID idP1, VertexID idP2) const {
      std::deque<MultiEdge> result;
      for(unsigned int i = 0; i < 3; ++i)
	if (edges[i].hasMinimalEdge(idP1, idP2))
	  result.push_back(edges[i]);

      return result;
    }

    /** Return point id */
    inline VertexID getP1() const { return edges[0].getFirstId(); }

    /** Return point id */
    inline VertexID getP2() const { return edges[0].getLastId(); }

    /** Return point id  */
    inline VertexID getP3() const { return edges[1].getLastId(); }

    /** Set point id  */
    inline void setP1(VertexID p1) { edges[0].setFirstId(p1); edges[1].setFirstId(p1); }

    /** Set point id  */
    inline void setP2(VertexID p2) { edges[0].setLastId(p2); edges[2].setFirstId(p2); }

    /** Set point id  */
    inline void setP3(VertexID p3) { edges[1].setLastId(p3); edges[2].setLastId(p3); }

    /** add a forbidden triangle to the MultiTriangle according to an edge
	@param eId edge id
	@param idT triangle id
    */
    inline void addForbiddenTriangle(unsigned int eId, TriangleID idT_l) {
      assert(eId < 3);
      //    assert(idT <= INT_MAX);
      forbiddenTriangles[eId] = idT_l;
    }

    /**
       return true if there is a forbidden triangle associated to the edge eId
    */
    inline bool hasForbiddenTriangle(unsigned int eId) const { assert(eId < 3); return forbiddenTriangles[eId] >= 0; }

    /**
       return the forbidden triangle associated to the edge eId
    */
    inline TriangleID getForbiddenTriangle(unsigned int eId) const { assert(eId < 3); return forbiddenTriangles[eId]; }

    /**
       return the number of boundary edges
    */
    inline unsigned int getNbBEdges() const { return  edges[0].getNbBEdges() + edges[1].getNbBEdges() + edges[2].getNbBEdges(); }

    /**
       return the number of open boundary edges
    */
    inline unsigned int getNbOpenBEdges() const {
      unsigned int result = edges[0].getNbBEdges() + edges[1].getNbBEdges() + edges[2].getNbBEdges();
      if (edges[0].hasMiddlePoint() && (edges[1].hasPoint(edges[0].getMiddleId()) || edges[2].hasPoint(edges[0].getMiddleId())))
	result -= 2;
      if (edges[1].hasMiddlePoint() && (edges[2].hasPoint(edges[1].getMiddleId())))
	result -= 2;
      return result;
    }

    /**
       return true if the triangle is empty
    */
    inline bool isEmpty() const { return (getP1() == getP2()) && (getP2() == getP3()); }


    /**
       return true if the triangle is a star, ie if the edges had the same point as middle
    */
    inline bool isStar() const { return edges[0].hasMiddlePoint() && edges[1].hasMiddlePoint() && edges[2].hasMiddlePoint() &&
	(edges[0].getMiddleId() == edges[1].getMiddleId()) && (edges[0].getMiddleId() == edges[2].getMiddleId()); }

    /**
       return true if the triangle is squashed, ie if two edges had the same point as middle
    */
    inline bool isSquashed() const { return ((edges[0].hasMiddlePoint() && edges[1].hasMiddlePoint() &&
					      (edges[0].getMiddleId() == edges[1].getMiddleId())) ||
					     (edges[2].hasMiddlePoint() && edges[1].hasMiddlePoint() &&
					      (edges[2].getMiddleId() == edges[1].getMiddleId())) ||
					     (edges[0].hasMiddlePoint() && edges[2].hasMiddlePoint() &&
					      (edges[0].getMiddleId() == edges[2].getMiddleId())));
    }

    /**
       set list of triangles inside the current MultiTriangle
    */
    inline void setTriangles(const std::deque<TriangleID> & triangles_l) {
      (*this).triangles = triangles_l;
    }

    /**
       add a triangle inside the current MultiTriangle
    */
    inline void addTriangle(TriangleID idtr) {
      triangles.push_back(idtr);
    }

    /**
       get triangles inside the current triangle
    */
    inline const std::deque<TriangleID> & getTriangles() const {
      return triangles;
    }

    /**
       Clear the list of triangles contains by this MultiTriangle
    */
    inline void clearTriangles()  {
      return triangles.clear();
    }

    /**
       Return true if the triangle has \p mEdge as edge (in normal or inverted order)
    */
    inline bool hasEdge(const MultiEdge & mEdge) const {
      MultiEdge mEdgeInvert = mEdge.getInverted();
      return (mEdge == edges[0]) || (mEdgeInvert == edges[0]) ||
	(mEdge == edges[1]) || (mEdgeInvert == edges[1]) ||
	(mEdge == edges[2]) || (mEdgeInvert == edges[2]);
    }

    /**
       Invert order of the triangle
    */
    void invertOrder();

  };



  /**
     Stream operator for Triangle
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Triangle & t) {
    f << "Triangle(" << t.getP1() << ", "
      << t.getP2() << ", " << t.getP3() << "; id: " << t.getId() << "; flag: " << t.getFlag() << ")";

    return f;
  }


  /**
     Stream operator for MultiEdge
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const MultiEdge & e) {
    if (e.hasMiddlePoint())
      f << "Edge(" << e.getFirstId() << ", "
	<< e.getSecondId() << ", " << e.getLastId() << ")";
    else
      f << "Edge(" << e.getFirstId() << ", "
	<< e.getLastId() << ")";
    return f;
  }

  /**
     Stream operator for MultiTriangle
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const MultiTriangle & t) {
    f << "MultiTriangle(id: " << t.getId() << ", "
      << t.getEdge(0) << ", " << t.getEdge(1) << ", " << t.getEdge(2) << ")";

    return f;
  }
}


#endif
