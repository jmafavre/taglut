/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef NODE
#define NODE

#ifndef SWIG
#include <deque>
#include <assert.h>
#endif

#include "IDTypes.h"
#include "Exception.h"

namespace Taglut {
  /**
     @class Node

     @author Jean-Marie Favreau (LIMOS/Blaise Pascal Univ.)
     @date 2007-05-31
     @brief Node of a graph
  */
  class Node {
  protected:
    /**
       Flag value (used by algorithms)
    */
    int flag;

    /**
       Node's Id
    */
    NodeID id;

    /** Neighbours list*/
    std::deque<NodeID> neighbours;

  public:


    /**
       Default constructor
    */
    Node(NodeID id = 0, int flag = 0);

    /**
       Copy constructor
    */
    Node(const Node & v);

#ifndef SWIG
    /**
       Set operator
    */
    Node & operator=(const Node & v);
#endif

    /**
       Return flag value
    */
    inline int getFlag() const { return flag; }


    /**
       Return neighbours list
    */
    inline const std::deque<NodeID> & getNeighbours() const { return neighbours; }
    /**
       Return neighbours list
    */
    inline std::deque<NodeID> & getNeighbours() { return neighbours; }

    /**
       Return neighbour #id
    */
    inline NodeID  getNeighbour(unsigned int id_l) const { return  neighbours[id_l]; }

    /**
       Clear neighbours list
    */
    inline void clearNeighbours() { neighbours.clear(); }

    /**
       Return number of neighbours
    */
    inline NodeID  getNbNeighbours() const { return  neighbours.size(); }


    /**
       return id
    */
    inline NodeID getId() const { return id; }


    /* **** modifiers */
    /**
       Set flag value
    */
    inline void setFlag(int v) { flag = v; }

    /**
       Increase flag value
    */
    inline void incFlag(int v = 1) { flag += v; }

    /**
       Set id value
    */
    inline void setId(NodeID id_l) { (*this).id = id_l; }

    /**
       Increase id value
    */
    inline void incId(int v = 1) { id += v; }

    /**
       Add a neighbour to current point. A point can be more that one time the neighbour of another point
    */
    void addNeighbour(NodeID neighbour);

    /**
       Add a neighbour to current point. A point can be more that one time the neighbour of another point
    */
    void addNeighbour(const Node & neighbour);

    /**
       Replace the neighbour \p oldNeighbour by the new one (\p newNeighbour). Only one
       instance is replaced.
    */
    void replaceNeighbour(NodeID oldNeighbour, NodeID newNeighbour, bool expt = false);


    /**
       remove neighbour. Only one instance is removed.
    */
    void removeNeighbour(NodeID nb, bool expt = false);

    /**
       Return true if p is current point's neighbour
    */
    bool hasNeighbour(const Node & p) const;

    /**
       Return true if c is current point's neighbour
    */
    bool hasNeighbour(NodeID c) const;

    /**
       Return true if the current point his an himself neighbour
    */
    bool hasLoop() const;
  };
}

#endif
