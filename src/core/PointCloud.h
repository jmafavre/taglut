/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_CLOUD
#define POINT_CLOUD

#include <iostream>
#include <assert.h>
#include <vector>

#include "IDTypes.h"
#include "Coord3D.h"
#include "Coord2D.h"
#include "Point3D.h"
#include "FileManipulator.h"
#include "Mesh.h"

namespace Taglut {

  /**
     @class PointCloud
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @brief Point cloud structure
  */
  class PointCloud : public FileManipulator {
  private:
    /** Points of the cloud */
    std::vector<Point3D> points;

    /** Normals associated to the point cloud (may be empty) */
    std::vector<Coord3D> normals;

    bool checkFormatPLY(std::ifstream & file) const;

    bool loadPLY(std::ifstream & file);

    /** Loading a xyz file  */
    void loadXYZ(const std::string & fileName);

    /** Loading a xyzn file  */
    void loadXYZN(const std::string & fileName);

    /** Loading a PLY file  */
    void loadPLY(const std::string & fileName);

    /** Saving a xyz file  */
    void saveXYZ(const std::string & fileName) const;

    /** Saving a xyzn file  */
    void saveXYZN(const std::string & fileName) const;

    /** Saving a PLY file  */
    void savePLY(const std::string & fileName) const;

    /** Adding the supported formats */
    void addFormats();

  public:

    /** Loading method from FileManipulator */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & objectName = "");

    /** Saving method from FileManipulator */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName = "") const;

    /**
       Point iterator
    */
    typedef std::vector<Point3D>::iterator iterator;

    /**
       Point iterator (const)
    */
    typedef std::vector<Point3D>::const_iterator const_iterator;

    /**
       Copy constructor
    */
    PointCloud(const PointCloud & pCloud);

    /**
       Constructor from a set of points
    */
    PointCloud(const std::vector<Coord3D> & points);

    /**
       constructor from a mesh
    */
    PointCloud(const Mesh & mesh);

    /**
       Loading from a file
    */
    PointCloud(const std::string & fileName, const std::string & objectName);

    /**
       Default constructor
    */
    PointCloud();

    /**
       Destructor
    */
    ~PointCloud();

#ifndef SWIG
    /**
       Creating a PointCloud from a file
    */
    PointCloud(const char * filename);

    /**
       Creating a PointCloud from a mesh
    */
    PointCloud & operator= (const Mesh & mesh);

    /**
       Creating a PointCloud from a list of points
    */
    PointCloud & operator= (const std::vector<Coord3D> & points);
 #endif

    inline PointCloud & addPoint(const Coord3D & point) {
      points.push_back(Point3D(point.getX(), point.getY(), point.getZ(), points.size() - 1));
      return *this;
    }

    /**
       Return an iterator to the first point
    */
    inline iterator begin() { return points.begin(); }

    /**
       Return an iterator after the last point
    */
    inline iterator end() { return points.end(); }

    /**
       Return an iterator to the first point
    */
    inline const_iterator begin() const { return points.begin(); }

    /**
       Return an iterator after the last point
    */
    inline  const_iterator end() const { return points.end(); }

#ifndef SWIG
    /**
       Random access operator (const)
    */
    inline const Point3D & operator[] (VertexID id) const {
      return points[id];
    }

    /**
       Random access operator
    */
    inline Point3D & operator[] (VertexID id) {
      return points[id];
    }
#endif

    /**
       Return the number of points of the cloud
    */
    inline VertexID size() const { return points.size(); }

    /**
       Remove all the points of the current cloud
    */
    inline void clear()  { points.clear(); }

    /**
       Translation of the current point cloud
    */
    inline PointCloud & operator+=(const Coord3D & coord) {
      for(iterator p = begin(); p != end(); ++p)
	(*p) += coord;
      return *this;
    }

    /**
       Resizing of the current point cloud
    */
    inline PointCloud & operator*=(double ratio) {
      for(iterator p = begin(); p != end(); ++p)
	(*p) *= ratio;
      return *this;
    }

    /**
       Return the smallest distance between two points of the cloud
    */
    double getSmallestDistance() const;

    /**
       Create a isosphere of radius \p radius, cendered on \p center, using a recursive subdivision approach from an initial icosahedron (CImg method).
    */
    static PointCloud createSphere(double radius, unsigned int resolution = 2, const Coord3D & center = Coord3D(0.0, 0.0, 0.0));

    /**
       Prune the current point cloud by randomly removing \p ratio of the points.
       @param ratio Ratio of points to be removed
       @return The point cloud after removing the points
     */
    PointCloud & prune(double ratio);


    /**
       Set the surface normal associated to the point defined by the given id
     */
    void setNormal(VertexID id, const Coord3D & normal);

    /**
       Return true if the cloud has normals
    */
    inline bool hasNormals() const {
      return normals.size() == points.size();
    }

    /**
       accessor for the surface normal associated to the point defined by the given id
     */
    inline const Coord3D & getNormal(VertexID id) const {
      if (normals.size() < id)
	throw Exception("getNormal(1): normals not available");
      else
	return normals[id];
    }

    /**
       accessor for the surface normal associated to the point defined by the given id
     */
    Coord3D & getNormal(VertexID id) {
      if (normals.size() < id)
	throw Exception("getNormal(1): normals not available");
      else
	return normals[id];
    }

    /**
       accessor to the list of normals
    */
    inline const std::vector<Coord3D> & getNormals() const {
      return normals;
    }

    /**
       accessor to the list of normals
    */
    inline std::vector<Coord3D> & getNormals() {
      return normals;
    }

    /**
       Init the normals of the current point cloud
    */
    inline void initNormals() {
      normals.clear();
      normals.insert(normals.begin(), points.size(), Coord3D(0.0, 0.0, 0.0));
    }

    /**
       return the Hausdorff distance between the current point cloud and the given one
    */
    double getHausdorffDistance(const PointCloud & pCloud) const;

    /**
       return the ratio of points of \p pCloud that are points of the current one (using epsilon as error value for the location)
    */
    double getSimilarity(const PointCloud & pCloud, double epsilon = 0.) const;
  };

}

#endif
