/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LENGTH
#define LENGTH

#include "MeshMap.h"

namespace Taglut {
  class Mesh;
  class Mapping2D3D;
  class MeshManipulator;
  class WeightedEdges;

  /**
     @class LengthEdge
     Class used to measure edge length
  */
  class LengthEdge {
  protected:
    /**
       Mesh used to compute length
    */
    const Mesh & mesh;
  public:

    /**
       Default constructor
    */
    LengthEdge(const Mesh & mesh);
    virtual ~LengthEdge();

    /**
       Return length between p1 and p2
    */
    virtual double operator()(const VertexID p1, const VertexID p2) const = 0;

    /**
       Return mesh used by length computation
    */
    const Mesh & getMesh() const;

    /**
       Return the possibly MeshMap used by length computation
    */
    virtual const MeshMap * getMeshMap() const { return NULL; }

    /**
       Clone length object
    */
    virtual LengthEdge * clone() const = 0;

    /**
       Clone length object using a new mesh
    */
    virtual LengthEdge * clone(const Mesh & m) const = 0;

    /**
       Clone length object using a new MeshMap
    */
    virtual LengthEdge * cloneMeshMap(const MeshMap & meshMap) const;

    /**
       Return true if the current class need a MeshMap
    */
    virtual bool needMeshMap() const { return false; }

  };


  /**
     @class LengthEdgeMergeBoundary
     Merge boundary points (all edges inside boundary has a void length).
  */
  class LengthEdgeMergeBoundary : public LengthEdge {
  private:
    const LengthEdge & lE;
  public:
    /**
       default Constructor
    */
    LengthEdgeMergeBoundary(const LengthEdge & lE);

    /**
       Constructor with new mesh
    */
    LengthEdgeMergeBoundary(const LengthEdge & lE, const Mesh & m);

    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

  };

  /**
     @class LengthEdgeNoBoundary
     Forbid boundary points (all edges inside boundary has a larger length than the edges of the including box).
  */
  class LengthEdgeNoBoundary : public LengthEdge {
  private:
    const double maxDistance;
  public:
    /**
       Constructor with Euclidean distance
    */
    LengthEdgeNoBoundary(const Mesh & m);

    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

  };


  /*************** Length methods *****************/

  /**
     @class LengthEdgeEuclidean
     Euclidean length
  */
  class LengthEdgeEuclidean : public LengthEdge {
  public:

    /**
       Default constructor
    */
    LengthEdgeEuclidean(const Mesh & mesh);
    /**
       Return length between p1 and p2
    */
    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

  };

  /**
     @class LengthEdgeCurvature
     Non Euclidean length (use euclidean length with correction)
  */
  class LengthEdgeCurvature : public LengthEdge {
  private:
    LengthEdgeEuclidean lEdge;

  protected:
    /**
       Function used to modify length according to curvature
    */
    virtual double f(double angle) const = 0;

    /**
       Ratio of length modification
    */
    double ratio;
  public:

    /**
       Default constructor
    */
    LengthEdgeCurvature(const Mesh & mesh, double ratio);

    /**
       Return length between p1 and p2 using f function
    */
    double operator()(const VertexID p1, const VertexID p2) const;


  };

  /**
     @class LengthEdgeLinearCurvature
     Non euclidean length based on euclidean length using curvature (linear correction)
  */
  class LengthEdgeLinearCurvature : public LengthEdgeCurvature {
  public:
    /**
       Default constructor
    */
    LengthEdgeLinearCurvature(const Mesh & mesh);
    /**
       Constructor with mesh and ratio value
    */
    LengthEdgeLinearCurvature(const Mesh & mesh, double ratio);
    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    double f(double angle) const;
  };

  /**
     @class LengthEdgeCosineCurvature
     Non euclidean length based on euclidean length using curvature (cosine correction)
  */
  class LengthEdgeCosineCurvature : public LengthEdgeCurvature {
  public:
    /**
       Default constructor
    */
    LengthEdgeCosineCurvature(const Mesh & mesh);
    /**
       Constructor with mesh and ratio value
    */
    LengthEdgeCosineCurvature(const Mesh & mesh, double ratio);
    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    double f(double angle) const;
  };

  /**
     @class LengthEdgeSineCurvature
     Non euclidean length based on euclidean length using curvature (sine correction)
  */
  class LengthEdgeSineCurvature : public LengthEdgeCurvature {
  public:
    /**
       Default constructor
    */
    LengthEdgeSineCurvature(const Mesh & mesh);
    /**
       Constructor with mesh and ratio value
    */
    LengthEdgeSineCurvature(const Mesh & mesh, double ratio);
    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    double f(double angle) const;
  };

  /**
     @class LengthEdgeInverseCurvature
     Non euclidean length based on euclidean length using curvature (inverse correction)
  */
  class LengthEdgeInverseCurvature : public LengthEdge {
  public:
    /**
       Default constructor
    */
    LengthEdgeInverseCurvature(const Mesh & mesh);

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    double operator()(const VertexID p1, const VertexID p2) const;

    /**
       Function used by length using local curvature (almost inverse function)
    */
    double f(double angle) const;
  };



  /**
     @class LengthEdgeEuclidean2D
     Euclidean length of the 2d representation of the mesh.
  */
  class LengthEdgeEuclidean2D : public LengthEdge {
  private:
    const Mapping2D3D & mapping;
  public:

    /**
       Default constructor
    */
    LengthEdgeEuclidean2D(const Mapping2D3D & mapping);

    /**
       Constructor with an alternative point
    */
    LengthEdgeEuclidean2D(const Mapping2D3D & mapping, const Mesh & mesh);

    /**
       Return length between p1 and p2
    */
    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;
  };

  /**
     @class LengthEdgeMappingRatio
     Non euclidean length based on ratio between the original length and
     the 2d representation of the mesh (mapping).
  */
  class LengthEdgeMappingRatio : public LengthEdge {
  private:
    const Mapping2D3D & mapping;
  public:

    /**
       Default constructor
    */
    LengthEdgeMappingRatio(const Mapping2D3D & mapping);

    /**
       Constructor with an alternative point
    */
    LengthEdgeMappingRatio(const Mapping2D3D & mapping, const Mesh & mesh);


    /**
       Return length between p1 and p2
    */
    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;
  };

  /**
     @class LengthEdgeMappingInverseRatio
     Non euclidean length based on ratio between the 2d representation of the
     mesh (mapping) and the original length.
  */
  class LengthEdgeMappingInverseRatio : public LengthEdge {
  private:
    const Mapping2D3D & mapping;
  public:

    /**
       Default constructor
    */
    LengthEdgeMappingInverseRatio(const Mapping2D3D & mapping);

    /**
       Constructor with an alternative point
    */
    LengthEdgeMappingInverseRatio(const Mapping2D3D & mapping, const Mesh & mesh);

    /**
       Return length between p1 and p2
    */
    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;
  };


  /**
     @class LengthMeshMap
     Non euclidean length distorted using a MeshMap. l = le * m(v1) * m(v2)
  */
  class LengthMeshMap : public LengthEdge {
  protected:
    /** mesh map used to distort the original length */
    const MeshMap & mMap;
  public:


    /**
       Default constructor
    */
    LengthMeshMap(const MeshMap & mM);

    ~LengthMeshMap() { }

    /**
       Return true if the current class need a MeshMap
    */
    virtual bool needMeshMap() const { return true; }

    /**
       Return the meshmap
    */
    const MeshMap * getMeshMap() const { return &mMap; }

    virtual LengthEdge * clone(const Mesh &) const {
      throw Exception("LengthMeshMap::clone(1): error. This kind of length need a MeshMap to be cloned.");
    }

    virtual LengthEdge * clone() const;

    virtual LengthEdge * cloneMeshMap(const MeshMap & mMap) const;

    virtual double operator()(const VertexID p1, const VertexID p2) const;

  };

  /**
     @class LengthMeshMapMean
     Non euclidean length distorted using a MeshMap. l = le * (m(v1) + m(v2)) / 2
  */
  class LengthMeshMapMean : public LengthEdge {
  protected:
    /** mesh map used to distort the original length */
    const MeshMap & mMap;
  public:


    /**
       Default constructor
    */
    LengthMeshMapMean(const MeshMap & mM);

    ~LengthMeshMapMean() { }

    /**
       Return true if the current class need a MeshMap
    */
    virtual bool needMeshMap() const { return true; }

    /**
       Return the meshmap
    */
    const MeshMap * getMeshMap() const { return &mMap; }

    inline LengthEdge * clone(const Mesh &) const {
      throw Exception("LengthMeshMap::clone(1): error. This kind of length need a MeshMap to be cloned.");
    }

    virtual LengthEdge * clone() const;

    virtual LengthEdge * cloneMeshMap(const MeshMap & mMap) const;

    virtual double operator()(const VertexID p1, const VertexID p2) const;

  };

  /**
     @class LengthMeshMapGradient
     Non euclidean length distorted using a MeshMap. l = le / fabs(m(v1) - m(v2)).
     if m(v1) == m(v2), return .5 * mMap.getMinimumDifferenceOnEdge()
  */
  class LengthMeshMapGradient : public LengthEdge {
  protected:
    /** mesh map used to distort the original length */
    const MeshMap & mMap;
    /** epsilon value for flat edges */
    const double epsilon;
  public:


    /**
       Default constructor
    */
    LengthMeshMapGradient(const MeshMap & mM);

    /**
       constructor with an epsilon value
    */
    LengthMeshMapGradient(const MeshMap & mM, double epsilon);

    /** destructor */
    ~LengthMeshMapGradient() { }

    /**
       Return true if the current class need a MeshMap
    */
    virtual bool needMeshMap() const { return true; }

    /**
       Return the meshmap
    */
    const MeshMap * getMeshMap() const { return &mMap; }

    inline LengthEdge * clone(const Mesh &) const {
      throw Exception("LengthMeshMap::clone(1): error. This kind of length need a MeshMap to be cloned.");
    }

    virtual LengthEdge * clone() const;

    virtual LengthEdge * cloneMeshMap(const MeshMap & mMap) const;

    virtual double operator()(const VertexID p1, const VertexID p2) const;

  };

  /**
     @class LengthMeshMapSignedGradient
     Non euclidean length distorted using a MeshMap.
     In the part that is allowed (according to the start value and the direction):
     l = le / fabs(m(v1) - m(v2)).
     if m(v1) == m(v2), return .5 * mMap.getMinimumDifferenceOnEdge()
     In the other part:
     l = a maximal distance

  */
  class LengthMeshMapSignedGradient : public LengthEdge {
  protected:
    /** mesh map used to distort the original length */
    const MeshMap & mMap;
    /** epsilon value for flat edges */
    const double epsilon;
    /** the value that separate the two parts */
    const double startValue;
    /** wanted direction */
    const bool direction;
    /** maximum distance that corresponds to the unwanted direction */
    const double maxDistance;
  public:


    /**
       Default constructor.
    */
    LengthMeshMapSignedGradient(const MeshMap & mM, double startValue, bool direction);

    /**
       constructor with an epsilon value
    */
    LengthMeshMapSignedGradient(const MeshMap & mM, double startValue, bool direction, double epsilon, double maxDistance = -1.);

    /** destructor */
    ~LengthMeshMapSignedGradient() { }

    /**
       Return true if the current class need a MeshMap
    */
    virtual bool needMeshMap() const { return true; }

    /**
       Return the meshmap
    */
    const MeshMap * getMeshMap() const { return &mMap; }

    inline LengthEdge * clone(const Mesh &) const {
      throw Exception("LengthMeshMap::clone(1): error. This kind of length need a MeshMap to be cloned.");
    }

    virtual LengthEdge * clone() const;

    virtual LengthEdge * cloneMeshMap(const MeshMap & mMap) const;

    virtual double operator()(const VertexID p1, const VertexID p2) const;

  };

  /**
     @class LengthLocalFeatureSize
     Non euclidean length distorted using a MeshMap that describe the local feature size,
     and used to estimate the area of the triangles that will be added (using Heron's formula).
  */
  class LengthLocalFeatureSize : public LengthMeshMap {
  public:

    /**
       Default constructor
    */
    LengthLocalFeatureSize(const MeshMap & mM);


    inline LengthEdge * clone(const Mesh &) const {
      throw Exception("LengthMeshMap::clone(1): error. This kind of length need a MeshMap to be cloned.");
    }

    virtual LengthEdge * clone() const;
    virtual LengthEdge * cloneMeshMap(const MeshMap & m) const;

    double operator()(const VertexID p1, const VertexID p2) const;

  };


  /**
     @class LengthOtherDistance
     Non euclidean length using another distance as reference: points with highly different distances
     are describing an edge shorter than an edge described by points with closer distances.
     The other distance is contained by the given MeshManipulator
  */
  class LengthOtherDistance : public LengthEdge {
  private:
    const MeshManipulator & mManip;
  public:

    /**
       Default constructor
    */
    LengthOtherDistance(const MeshManipulator & mManip, const Mesh & mesh);


    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    double operator()(const VertexID p1, const VertexID p2) const;
  };


  /**
     @class LengthIsocurve
     Non euclidean length using another distance as reference for an isocurve computation, modifying the original distance \p lE by a multiplicative approach.
     The other distance is contained by the given MeshMap
  */
  class LengthIsocurve : public LengthEdge {
  private:
    /** other length */
    const MeshMap & mMap;
    /** isocurve value */
    double value;

    const LengthEdge & lE;
  public:

    /**
       Default constructor
    */
    LengthIsocurve(const MeshMap & mMap, double value, const LengthEdge & lE);

    /**
       Constructor with an alternative mesh
    */
    LengthIsocurve(const MeshMap & mMap, double value, const LengthEdge & lE, const Mesh & mesh);

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    double operator()(const VertexID p1, const VertexID p2) const;
  };


  /**
     @class LengthWeightedEdges
     Non euclidean length based on weights defined on the edges
  */
  class LengthWeightedEdges : public LengthEdge {
  private:
    const WeightedEdges & we;
  public:

    /**
       Default constructor
    */
    LengthWeightedEdges(const WeightedEdges & we);

    /**
       Return length between p1 and p2
    */
    double operator()(const VertexID p1, const VertexID p2) const;

    virtual LengthEdge * clone() const;
    virtual LengthEdge * clone(const Mesh & m) const;

    /** accessor */
    inline const WeightedEdges & getWeights() const { return we; }
  };

  /**
     @class LengthFactory
     Given a string from user, create corresponding length function.
  */
  class LengthFactory {
  private:
    std::string length;
    LengthEdge * lEdge;
    double ratio;
  public:

    /**
       Default constructor
    */
    LengthFactory() ;

    /**
       Constructor with length name
    */
    LengthFactory(const std::string & length);

    ~LengthFactory();

    /**
       Set length name
    */
    void setLength(const std::string & length);

    /**
       Set ratio
    */
    void setRatio(double ratio);

    /**
       Return true if it exists a length corresponding to the name contained by LengthFactory
    */
    bool existsLength() const;

    /**
       Return true if it exists the length contained by LengthFactory need a MeshMap
    */
    bool needMeshMap() const;

    /**
       Build length defined by name
    */
    const LengthEdge & getLength(const Mesh & m);

    /**
       Build length defined by name
    */
    const LengthEdge & getLength(const MeshMap & mMap);

    /**
       String conversion
    */
    std::string toString() const;

  };


  /**
     String conversion using operator <<
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const LengthFactory & m) {
    f << m.toString();
    return f;
  }
}

#endif
