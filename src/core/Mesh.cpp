/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <set>
#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "CImgUse.h"
#include "Coord3D.h"
#include "Coord2D.h"
#include "CImgUse.h"
#include "MeshCut.h"
#include "MeshPart.h"
#include "MeshList.h"
#include "Length.h"
#include "Mapping2D3D.h"
#include "FileTools.h"
#include "FileExceptions.h"
#include "SpaceSpliter.h"
#include "StringManipulation.h"
#include "FileManipulator.h"
#include "NLoop.h"
#include "PointOnEdge.h"
#include "PLPath.h"
#include "PLY.h"

using namespace Taglut;

/**
   comparison operator for points using string comparison
*/
namespace Taglut {
  struct ltpt {
    /**
       operator (): comparison operator
    */
    bool operator()(const CImg<double> & p1, const CImg<double> & p2) const
    {
      std::ostringstream str1, str2;
      str1 <<  p1(0) << "," << p1(1) << "," << p1(2);
      str2 <<  p2(0) << "," << p2(1) << "," << p2(2);
      return str1.str() < str2.str();
    }
  };
}


void Mesh::addFormats() {
  addFormat("wrl");
  addFormat("vrml");
  addFormat("obj");
  addFormat("ply");
  addFormat("off");
  addFormat("vtk");
  addLoadFormat("xyz");
  addLoadFormat("asc");
  addLoadFormat("dcm");
  addLoadFormat("hdr");
  addLoadFormat("nii");
}

Mesh::Mesh() {
  addFormats();
  nbPoints = nbTriangles = 0;
}


Mesh::~Mesh() {
}

Mesh::Mesh(const char * filename, const char * objectName) {
  addFormats();
  nbPoints = nbTriangles = 0;
  if (filename != NULL)
    load(filename, objectName);
}

Mesh::Mesh(const std::string & filename, const std::string & objectName) {
  addFormats();
  nbPoints = nbTriangles = 0;
  load(filename, objectName);
}


Mesh::Mesh(const CImgList<double> & pts, CImgList<VertexID> & primitives, bool merge, const IndentManager iManager) {
  addFormats();
  nbPoints = nbTriangles = 0;

  /* triangulate mesh if needed */
  triangulateCImgMesh(primitives);

  /* build mesh */
  if (merge)
    buildMeshMergePoints(pts, primitives, true, iManager);
  else
    buildMesh(pts, primitives);
}

Mesh::Mesh(const CImg<> & image) {
  addFormats();
  nbPoints = nbTriangles = 0;
  createFromVolumicImage(image);
}


Mesh::Mesh(const CImg<> & pts, CImgList<VertexID> & primitives, bool merge, const IndentManager iManager) {
  addFormats();
  nbPoints = nbTriangles = 0;

  /* triangulate mesh if needed */
  triangulateCImgMesh(primitives);

  /* build mesh */
  if (merge)
    buildMeshMergePoints(pts, primitives, true, iManager);
  else
    buildMesh(pts, primitives);
}

Mesh::Mesh(const Mesh & mesh) : FileManipulator(mesh) {
  addFormats();
  nbPoints = nbTriangles = 0;
  *this = mesh;
}


Mesh::Mesh(const std::deque<Mesh> & meshList) {
  addFormats();
  nbPoints = 0;
  for(std::deque<Mesh>::const_iterator m = meshList.begin(); m != meshList.end(); ++m)
    nbPoints += (*m).getNbPoints();

  points.reserve(nbPoints);

  nbTriangles = 0;
  for(std::deque<Mesh>::const_iterator m = meshList.begin(); m != meshList.end(); ++m)
    nbTriangles += (*m).getNbTriangles();

  triangles.reserve(nbTriangles);

  TriangleID oTriangles = 0;
  VertexID oPoints = 0;


  for(std::deque<Mesh>::const_iterator m = meshList.begin(); m != meshList.end(); ++m) {
    for(VertexID i = 0; i < (*m).getNbPoints(); ++i) {
      points.push_back((*m).point(i));
      points.back().incIds(oPoints, oTriangles);
    }

    for(TriangleID i = 0; i < (*m).getNbTriangles(); ++i) {
      triangles.push_back((*m).triangle(i));
      triangles.back().incIds(oPoints, oTriangles);
    }

    oPoints += (*m).getNbPoints();
    oTriangles += (*m).getNbTriangles();
  }

}


Mesh::Mesh(Mapping2D3D & mapping, bool flatMesh) {
  addFormats();
  CImgList<double> uPoints;
  if (flatMesh)
    mapping.computeUnfoldedCImgPoints(uPoints);
  std::vector<Triangle> tri = mapping.getTriangles();

  // build point list
  points.reserve(uPoints._width);
  CImgList<double>::iterator j = uPoints.begin();
  for(Mesh::point_iterator i = mapping.point3D_begin(); i != mapping.point3D_end(); ++i) {
    points.push_back(Point3D(*i));
    if (flatMesh) {
      points.back().setX((*j)(0));
      points.back().setY((*j)(1));
      points.back().setZ((*j)(2));
    }
    ++j;
  }
  nbPoints = points.size();

  // build triangle list
  triangles.reserve(tri.size());
  nbTriangles = 0;
  for(std::vector<Triangle>::const_iterator t = tri.begin(); t != tri.end(); ++t, ++nbTriangles){
    triangles.push_back(*t);
    triangles.back().setId(nbTriangles);
  }

  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag(true);

}

Mesh::Mesh(const MeshPart & meshPart) {
  addFormats();
  nbPoints = nbTriangles = 0;

  *this = meshPart.buildCropMesh();

  assert(meshPart.getNbPoints() == nbPoints);
  assert(meshPart.getNbTriangles() == nbTriangles);

  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag();

}

Mesh::Mesh(MeshCut & meshCut) {
  addFormats();
  nbPoints = nbTriangles = 0;

  MeshCut mCut(meshCut);
  *this = mCut.cutMesh();

  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag();

}

bool Mesh::orderNeighbours(VertexID i, bool warning) {
  return points[i].orderNeighbours(triangles, warning);
}


void Mesh::setPointFlag(int value) {
  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i)
    (*i).setFlag(value);
}

void Mesh::setTriangleFlag(int value) {
  for(std::vector<Triangle>::iterator i = triangles.begin(); i != triangles.end(); ++i)
    (*i).setFlag(value);
}


TriangleID Mesh::getNbTriangles(VertexID idP1, VertexID idP2) const {
  TriangleID result = 0;
  for(std::deque<TriangleID>::const_iterator t = points[idP1].getTriangles().begin();
      t != points[idP1].getTriangles().end(); ++t)
    if (triangles[*t].hasPoint(idP2))
      ++result;

  return result;
}


Mesh & Mesh::operator=(const Mesh & mesh) {
  if (&mesh == this)
    return *this;

  points = mesh.getPoints();
  triangles = mesh.getTriangles();
  nbPoints = mesh.getNbPoints();
  nbTriangles = mesh.getNbTriangles();

  return *this;
}




void Mesh::stripTriangleFlags(int startValue) {
  // first compute min and max values
  std::set<int, ltint<int> > values;
  std::map<int, int, ltint<int> > invertedValues;
  int v = startValue;
  for(std::vector<Triangle>::iterator i = triangles.begin(); i != triangles.end(); ++i)
    values.insert((*i).getFlag());

  for(std::set<int, ltint<int> >::const_iterator i = values.begin(); i != values.end(); ++i)
    invertedValues[*i] = v++;

  for(std::vector<Triangle>::iterator i = triangles.begin(); i != triangles.end(); ++i)
    (*i).setFlag(invertedValues[(*i).getFlag()]);
}


void Mesh::switchTriangleFlags(int f1, int f2) {
  for(std::vector<Triangle>::iterator i = triangles.begin(); i != triangles.end(); ++i) {
    int v = (*i).getFlag();
    if (v == f1)
      (*i).setFlag(f2);
    else if (v == f2)
      (*i).setFlag(f1);
  }

}

void Mesh::setPointFlags(int f1, int f2) {
  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i)
    if((*i).getFlag() == f1)
      (*i).setFlag(f2);
}


/**
   Init order neighbours and init isBoundary flag
*/
void Mesh::initIsBoundary(Point3D & point_t, bool warning) {
  if (point_t.getNbNeighbours() > 1) {
    double ordered = point_t.orderNeighbours(triangles, warning);
    point_t.setIsBoundary((!ordered)
			  || (point_t.getNbNeighbours() != point_t.getNbTriangles()));
  }
  else
    point_t.setIsBoundary(true);

}



/**
   Return true if point is in a black box of a 3d checkerboard
*/
bool Mesh::isOnGrid(const Point3D & value, double size) {
  int dx = (int) (value.getX() / size);
  int dy = (int) (value.getY() / size);
  int dz = (int) (value.getZ() / size);

  return (dx + dy + dz) % 2 == 0;
}


void Mesh::computeGridValues(CImgList<unsigned char>& values, double size) {
  values.clear();

  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i)
    if (isOnGrid(*i, size))
      values.insert(CImg<unsigned char>::vector(255));
  else
      values.insert(CImg<unsigned char>::vector(0));
}


void Mesh::setPointsAndComputeTriangles(const std::deque<Point3D> & oPoints) {
  TriangleID nbETriangles = 0;

  // delete data if needed
  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  // set points' list
  points.reserve(oPoints.size());
  for(std::deque<Point3D>::const_iterator c = oPoints.begin(); c != oPoints.end(); ++c)
    points.push_back(*c);

  // first build points and compute estimated triangle number
  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i) {
    nbETriangles += (*i).getNbNeighbours();
    (*i).setFlag(0);
  }
  nbPoints = points.size();

  nbETriangles /= 3;

  // then build triangles
  triangles.reserve(nbETriangles);
  points[0].setFlag(1);
  std::deque<VertexID> list;
  list.push_front(0);

  while(list.size() != 0) {
    Point3D & current = points[list.back()];
    list.pop_back();
    current.setFlag(2);
    for(VertexID i = 0; i < current.getNbNeighbours(); ++i) {
      Point3D & neighbour = points[current.getNeighbour(i)];
      if (neighbour.getFlag() != 2) {
	// add neighbour to the open points list
	if (neighbour.getFlag() != 1) {
	  neighbour.setFlag(1);
	  list.push_front(neighbour.getId());
	}
	// add triangles if needed
	for(VertexID j = 0; j < neighbour.getNbNeighbours(); ++j) {
	  Point3D & thirdPoint = points[neighbour.getNeighbour(j)];
	  if ((thirdPoint.getFlag() != 2) && thirdPoint.hasNeighbour(current.getId())) {
	    bool hasTriangle = false;
	    for(std::deque<TriangleID>::const_iterator k = current.getTriangles().begin(); k != current.getTriangles().end(); ++k)
	      if (*k < nbTriangles) {
		hasTriangle = true;
		break;
	      }

	    if (!hasTriangle) {
	      // create triangle
	      assert(nbTriangles < nbETriangles);

	      // create triangle
	      triangles.push_back(Triangle(current.getId(), neighbour.getId(), thirdPoint.getId(), nbTriangles));

	      // add it at each point
	      current.addTriangle(triangles.back().getId());
	      neighbour.addTriangle(triangles.back().getId());
	      thirdPoint.addTriangle(triangles.back().getId());
	    }
	  }
	}
      }
    }

  }

  assert(nbETriangles == triangles.size());
  nbTriangles = nbETriangles;

  // reorder triangles
  reorderTriangleVertices();

}

void Mesh::reorderTriangleVertices() {
  if (nbTriangles == 0)
    return;

  // init flags
  setTriangleFlag(0);

  // first triangle order is good
  triangles[0].setFlag(1);
  std::deque<TriangleID> list;
  list.push_front(triangles[0].getId());


  while(list.size() != 0) {
    // get next triangle id in list
    TriangleID i = list.back();
    list.pop_back();
    // for each i's triangle neighbour, set order and add it to list
    try {
      Triangle & t = findOtherTriangle(triangles[i].getP1(), triangles[i].getP2(), triangles[i]);
      if (t.getFlag() == 0) {
	t.reorder(triangles[i].getP2(), triangles[i].getP1());
	t.setFlag(1);
	list.push_front(t.getId());
      }
    } catch(Exception e) { }

    try {
      Triangle & t = findOtherTriangle(triangles[i].getP2(), triangles[i].getP3(), triangles[i]);
      if (t.getFlag() == 0) {
	t.reorder(triangles[i].getP3(), triangles[i].getP2());
	t.setFlag(1);
	list.push_front(t.getId());
      }
    } catch(Exception e) { }

    try {
      Triangle & t = findOtherTriangle(triangles[i].getP3(), triangles[i].getP1(), triangles[i]);
      if (t.getFlag() == 0) {
	t.reorder(triangles[i].getP1(), triangles[i].getP3());
	t.setFlag(1);
	list.push_front(t.getId());
      }
    } catch(Exception e) { }
  }
}

void Mesh::reorient() {
  // first reorder triangles
  reorderTriangleVertices();

  //then for each point reorient neighbours according to triangle order
  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i)
    if ((*i).getNbNeighbours() >= 2) {
      VertexID point1 = (*i).getNeighbour(0);
      VertexID point2 = (*i).getNeighbour(1);
      assert(points[point1].hasNeighbour(point2));
      if (!findTriangle((*i).getId(), point1, point2).sameOrder((*i).getId(), point1, point2))
	(*i).inverseNeighbourList();
    }
}


void Mesh::clear() {
    points.clear();
    triangles.clear();
}


void Mesh::triangulateCImgMesh(CImgList<VertexID> & primitives) {

  TriangleID i = 0;

  CImg<VertexID>t(3);
  while(i < primitives._width) {

    while(primitives[i]._width > 3) {
      t(0) = primitives[i](primitives[i]._width - 1);
      t(1) = primitives[i](0);
      t(2) = primitives[i](1);

      primitives.insert(t);
      primitives[i] = CImg<VertexID>(primitives[i].begin() + 1, primitives[i]._width - 1);
    }
    ++i;
  }

}



Triangle & Mesh::findOtherTriangle(VertexID c1, VertexID c2, const Triangle & t) {
  for(std::deque<TriangleID>::const_iterator i = points[c1].getTriangles().begin(); i != points[c1].getTriangles().end(); ++i)
    if (triangles[*i].hasPoint(c2) && (!(triangles[*i] == t))) {
      assert(*i == triangles[*i].getId());
      return triangles[*i];
    }

  throw Exception(std::string("Mesh: triangle not found (findOtherTriangle(uint, uint, Triangle))"));

}

Triangle & Mesh::findOtherTriangle(VertexID c1, VertexID c2, TriangleID t) {
  for(std::deque<TriangleID>::const_iterator i = points[c1].getTriangles().begin(); i != points[c1].getTriangles().end(); ++i)
    if (triangles[*i].hasPoint(c2) && (*i != t)) {
      assert(*i == triangles[*i].getId());
      return triangles[*i];
    }

  throw Exception(std::string("Mesh: triangle not found (findOtherTriangle(uint, uint, uint))"));

}

const Triangle & Mesh::findOtherTriangle(VertexID c1, VertexID c2, const Triangle & t) const {
  for(std::deque<TriangleID>::const_iterator i = points[c1].getTriangles().begin(); i != points[c1].getTriangles().end(); ++i)
    if (triangles[*i].hasPoint(c2) && (!(triangles[*i] == t))) {
      assert(*i == triangles[*i].getId());
      return triangles[*i];
    }
  throw Exception(std::string("Mesh: triangle not found (findOtherTriangle(uint, uint, Triangle))"));

}

const Triangle & Mesh::findOtherTriangle(VertexID c1, VertexID c2, TriangleID t) const {
  for(std::deque<TriangleID>::const_iterator i = points[c1].getTriangles().begin(); i != points[c1].getTriangles().end(); ++i)
    if (triangles[*i].hasPoint(c2) && (*i != t)) {
      assert(*i == triangles[*i].getId());
      return triangles[*i];
    }

  throw Exception(std::string("Mesh: triangle not found (findOtherTriangle(uint, uint, uint))"));

}

Triangle & Mesh::findTriangle(VertexID c1, VertexID c2) {
  assert(c1 < nbPoints);
  const Point3D & p = points[c1];
  for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t)
    if (triangles[*t].hasPoint(c2)) {
      assert(*t == (triangles[*t]).getId());
      return triangles[*t];
    }

  throw Exception(std::string("Mesh: triangle not found (findTriangle(uint, uint))"));

}

const Triangle & Mesh::findTriangle(VertexID c1, VertexID c2) const {
  assert(c1 < nbPoints);
  const Point3D & p = points[c1];
  for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t)
    if (triangles[*t].hasPoint(c2)) {
      assert(*t == (triangles[*t]).getId());
      return triangles[*t];
    }

  throw Exception(std::string("Mesh: triangle not found (findTriangle(uint, uint))"));

}


const Triangle & Mesh::findTriangle(const Point3D & c1, const Point3D & c2, const Point3D & c3) const {
  assert(c1.getId() < nbPoints);
  for(std::deque<TriangleID>::const_iterator t = c1.getTriangles().begin(); t != c1.getTriangles().end(); ++t)
    if (triangles[*t].hasPoint(c2) && triangles[*t].hasPoint(c3))
      return triangles[*t];

  throw Exception(std::string("Mesh: triangle not found (findTriangle(Point3D, Point3D, Point3D))"));
}

Triangle & Mesh::findTriangle(VertexID c1, VertexID c2, VertexID c3) {
  assert(c1 < nbPoints);
  const Point3D & p = points[c1];
  for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t)
    if (triangles[*t].hasPoint(c2) && triangles[*t].hasPoint(c3))
      return triangles[*t];


  throw Exception(std::string("Mesh: triangle not found (findTriangle(uint, uint, uint))"));
}

const Triangle & Mesh::findTriangle(VertexID c1, VertexID c2, VertexID c3) const {
  assert(c1 < nbPoints);
  const Point3D & p = points[c1];
  for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t)
    if (triangles[*t].hasPoint(c2) && triangles[*t].hasPoint(c3))
      return triangles[*t];

  throw Exception(std::string("Mesh: triangle not found (findTriangle(uint, uint, uint))"));
}



void Mesh::setPointsAndbuildMesh(const std::deque<Point3D> & oPoints, const std::deque<Triangle> & triangles_t) {

  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  // set points' list
  nbPoints = oPoints.size();
  points.reserve(nbPoints);
  for(std::deque<Point3D>::const_iterator c = oPoints.begin(); c != oPoints.end(); ++c)
    points.push_back(*c);

  nbTriangles = triangles_t.size();
  // build triangles' list
  triangles.reserve(nbTriangles);
  for(std::deque<Triangle>::const_iterator c = triangles_t.begin(); c != triangles_t.end(); ++c)
    triangles.push_back(*c);

  // reorder triangles
  reorderTriangleVertices();

  // reorder neighbours
  reorderNeighbours();
}

void Mesh::reorderNeighbours() {
  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i)
    (*i).orderNeighbours((*this).triangles);
}


void Mesh::buildMeshTriangulate(const CImgList<double> & pts, CImgList<VertexID> & primitives, bool warning) {
  triangulateCImgMesh(primitives);

  buildMesh(pts, primitives, warning);

}

void Mesh::buildMeshTriangulate(const CImg<> & pts, CImgList<VertexID> & primitives, bool warning) {
  triangulateCImgMesh(primitives);

  buildMesh(pts, primitives, warning);

}

void Mesh::buildMeshMergePoints(const CImgList<double> & pts, CImgList<VertexID> & primitives, bool warning, const IndentManager iManager) {
  std::map<CImg<double>, VertexID, ltpt> realPts;
  VertexID * ptsId;
  ptsId = new VertexID[pts._width];

  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  // create real point id list (without duplicate points)
  for(VertexID i = 0; i < pts._width; ++i) {
    std::map<CImg<double>, VertexID, ltpt>::const_iterator itrpts = realPts.find(pts[i]);
    if (itrpts == realPts.end()) {
      VertexID nb = realPts.size();
      realPts[pts[i]] = nb;
      ptsId[i] = nb;
    } else
      ptsId[i] = (*itrpts).second;
  }

  // create point list according to id list
  nbPoints = realPts.size();
  points.insert(points.end(), nbPoints, Point3D());
  for(std::map<CImg<double>, VertexID, ltpt>::const_iterator it = realPts.begin();
      it != realPts.end(); ++it) {
    assert((*it).second < nbPoints);
    points[(*it).second].setCoords((*it).first(0), (*it).first(1), (*it).first(2));
    points[(*it).second].setId((*it).second);
  }

  nbTriangles = primitives._width;
  triangles.reserve(nbTriangles);
  cimglist_for(primitives, i) {
    triangles.push_back(Triangle(ptsId[primitives[i](0)], ptsId[primitives[i](1)], ptsId[primitives[i](2)], i));
    assert((triangles.back().getP1() != triangles.back().getP2()) && (triangles.back().getP1() != triangles.back().getP3()) && (triangles.back().getP3() != triangles.back().getP2()));
    points[triangles.back().getP1()].addTriangle(triangles.back().getP2(), triangles.back().getP3(), i);
    points[triangles.back().getP2()].addTriangle(triangles.back().getP1(), triangles.back().getP3(), i);
    points[triangles.back().getP3()].addTriangle(triangles.back().getP1(), triangles.back().getP2(), i);
  }

  delete [] ptsId;

  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag(warning);
  if (pts._width != nbPoints)
    std::cout << iManager << "Points before merge: " << pts._width << ", points after: " << nbPoints << std::endl;
}

void Mesh::buildMeshMergePoints(const CImg<> & pts, CImgList<VertexID> & primitives, bool warning, const IndentManager iManager) {
  std::map<CImg<double>, VertexID, ltpt> realPts;
  VertexID * ptsId;
  ptsId = new VertexID[pts._width];

  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  // create real point id list (without duplicate points)
  CImg<double> p(3);
  for(VertexID i = 0; i < pts._width; ++i) {
    p(0) = pts(i, 0);
    p(1) = pts(i, 1);
    p(2) = pts(i, 2);
    std::map<CImg<double>, VertexID, ltpt>::const_iterator itrpts = realPts.find(p);
    if (itrpts == realPts.end()) {
      VertexID nb = realPts.size();
      realPts[p] = nb;
      ptsId[i] = nb;
    } else
      ptsId[i] = (*itrpts).second;
  }

  // create point list according to id list
  nbPoints = realPts.size();
  points.insert(points.end(), nbPoints, Point3D());
  for(std::map<CImg<double>, VertexID, ltpt>::const_iterator it = realPts.begin();
      it != realPts.end(); ++it) {
    assert((*it).second < nbPoints);
    points[(*it).second].setCoords((*it).first(0), (*it).first(1), (*it).first(2));
    points[(*it).second].setId((*it).second);
  }

  nbTriangles = primitives._width;
  triangles.reserve(nbTriangles);
  cimglist_for(primitives, i) {
    triangles.push_back(Triangle(ptsId[primitives[i](0)], ptsId[primitives[i](1)], ptsId[primitives[i](2)], i));
    assert((triangles.back().getP1() != triangles.back().getP2()) && (triangles.back().getP1() != triangles.back().getP3()) && (triangles.back().getP3() != triangles.back().getP2()));
    points[triangles.back().getP1()].addTriangle(triangles.back().getP2(), triangles.back().getP3(), i);
    points[triangles.back().getP2()].addTriangle(triangles.back().getP1(), triangles.back().getP3(), i);
    points[triangles.back().getP3()].addTriangle(triangles.back().getP1(), triangles.back().getP2(), i);
  }

  delete [] ptsId;

  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag(warning);
  if (pts._width != nbPoints)
    std::cout << iManager << "Points before merge: " << pts._width << ", points after: " << nbPoints << std::endl;
}




void Mesh::buildMesh(const std::vector<Coord3D> & pts,
	       const std::vector<Triangle> & tgles,
	       bool warning) {

  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  /* first build point list using pts */
  nbPoints = pts.size();
  points.reserve(nbPoints);

  VertexID i = 0;
  for(std::vector<Coord3D>::const_iterator p = pts.begin(); p != pts.end(); ++p, ++i)
    points.push_back(Point3D((*p).getX(), (*p).getY(), (*p).getZ(), i));


  /* build triangle list and add neighbours to each Point3D */
  nbTriangles = tgles.size();
  triangles.reserve(nbTriangles);

  TriangleID j = 0;
  for(std::vector<Triangle>::const_iterator t = tgles.begin(); t != tgles.end(); ++t, ++j) {
    triangles.push_back(*t);
    triangles.back().setId(j);

    points[(*t).getP1()].addTriangle((*t).getP2(), (*t).getP3(), j);
    points[(*t).getP2()].addTriangle((*t).getP1(), (*t).getP3(), j);
    points[(*t).getP3()].addTriangle((*t).getP1(), (*t).getP2(), j);
  }


  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag(warning);

}

void Mesh::buildMesh(const CImg<> & pts,
		     const CImgList<VertexID> & primitives,
		     bool warning) {

  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  /* first build point list using pts */
  nbPoints = pts._width;
  points.reserve(nbPoints);

  cimg_forX(pts, i) {
    points.push_back(Point3D(pts(i, 0), pts(i, 1), pts(i, 2), i));
  }


  /* build triangle list and add neighbours to each Point3D */
  nbTriangles = primitives._width;
  triangles.reserve(nbTriangles);

  cimglist_for(primitives, i) {
    const CImg<VertexID>& primitive = primitives[i];
    triangles.push_back(Triangle(primitive(0), primitive(1), primitive(2), i));

    points[primitive(0)].addTriangle(primitive(1), primitive(2), i);
    points[primitive(1)].addTriangle(primitive(0), primitive(2), i);
    points[primitive(2)].addTriangle(primitive(0), primitive(1), i);
  }


  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag(warning);

}

void Mesh::buildMesh(const CImgList<double> & pts,
		     const CImgList<VertexID> & primitives,
		     bool warning) {

  if (nbPoints != 0) points.clear();
  if (nbTriangles != 0) triangles.clear();

  /* first build point list using pts */
  nbPoints = pts._width;
  points.reserve(nbPoints);

  cimglist_for(pts, i) {
    points.push_back(Point3D(pts[i](0), pts[i](1), pts[i](2), i));
  }


  /* build triangle list and add neighbours to each Point3D */
  nbTriangles = primitives._width;
  triangles.reserve(nbTriangles);

  cimglist_for(primitives, i) {
    const CImg<VertexID>& primitive = primitives[i];
    triangles.push_back(Triangle(primitive(0), primitive(1), primitive(2), i));

    points[primitive(0)].addTriangle(primitive(1), primitive(2), i);
    points[primitive(1)].addTriangle(primitive(0), primitive(2), i);
    points[primitive(2)].addTriangle(primitive(0), primitive(1), i);
  }


  // Order neighbours and init isBoundary flag
  initIsBoundaryFlag(warning);

}

void Mesh::initIsBoundaryFlag(bool warning) {
  /* first set isBoundary using neighbour order */
  for(std::vector<Point3D>::iterator c = points.begin(); c != points.end(); ++c)
    initIsBoundary(*c, warning);

  /* if P's neighbours number is 2 and neighbours are boundary points, P is boundary one  */
  for(std::vector<Point3D>::iterator c = points.begin(); c != points.end(); ++c)
    if ((*c).getNbNeighbours() <= 2)
      (*c).setIsBoundary(true);

}

void Mesh::setBoundaryPointFlag(int value) {
  for(std::vector<Point3D>::iterator c = points.begin(); c != points.end(); ++c)
    if ((*c).getIsBoundary())
      (*c).setFlag(value);
}

CImgList<double> Mesh::getCImgPoints() {
  CImgList<double> cImgPoints;
  for(std::vector<Point3D>::const_iterator c = points.begin(); c != points.end(); ++c)
    cImgPoints.insert(CImg<double>::vector((*c).getX(), (*c).getY(), (*c).getZ()));

  return cImgPoints;
}


std::vector<Coord3D> Mesh::getCoordList() const {
  std::vector<Coord3D> stlPoints;

  for(std::vector<Point3D>::const_iterator c = points.begin(); c != points.end(); ++c)
    stlPoints.push_back(*c);

  return stlPoints;
}


CImgList<VertexID> Mesh::getCImgPrimitives() {
  CImgList<VertexID> cImgPrimitives;
  for(std::vector<Triangle>::const_iterator c = triangles.begin(); c != triangles.end(); ++c)
    cImgPrimitives.insert(CImg<double>::vector((*c).getP1(), (*c).getP2(), (*c).getP3()));

  return cImgPrimitives;
}

MeshPart Mesh::getLargestCC() {
  if (nbPoints == 0)
    throw Exception("getLargestCC(): empty mesh");

  std::deque<MeshPart>::const_iterator ccSelected;
  VertexID size = 0;
  std::deque<MeshPart> cc = getConnectedComponents();
  for(std::deque<MeshPart>::const_iterator cci = cc.begin(); cci != cc.end(); ++cci)
    if (size < (*cci).getNbPoints()) {
      size = (*cci).getNbPoints();
      ccSelected = cci;
    }

  return *ccSelected;
}

std::deque<MeshPart> Mesh::getConnectedComponents() {
  std::deque<MeshPart> cc;
  VertexID nbP = 0;
  // flags initialisation
  setPointFlag(0);
  setTriangleFlag(0);

  while (nbP < nbPoints) {
    VertexID idP = 0;
    cc.push_front(MeshPart(*this));

    // find point not seen before
    while(points[idP].getFlag() != 0) {
      ++idP;
      assert(idP < nbPoints);
    }

    // then compute next CC
    computeCC(idP, cc.front());
    nbP += cc.front().getNbPoints();

  }
  return cc;

}


void Mesh::computeCC(VertexID startId, MeshPart & mp) {
  std::deque<VertexID> open;
  open.push_front(startId);

  points[startId].setFlag(1);
  mp.addPoint(startId);

  while(open.size() != 0) {
    VertexID id = open.front();
    open.pop_front();
    std::deque<VertexID> & nb = points[id].getNeighbours();
    for(std::deque<VertexID>::const_iterator itN = nb.begin(); itN != nb.end(); ++itN)
      if (points[*itN].getFlag() == 0) {
	points[*itN].setFlag(1);
	mp.addPoint(*itN);
	open.push_front(*itN);
	std::deque<TriangleID> & tr = points[*itN].getTriangles();
	for(std::deque<TriangleID>::const_iterator itT = tr.begin(); itT != tr.end(); ++itT)
	  if ((points[triangles[*itT].getP1()].getFlag() == 1) &&
	      (points[triangles[*itT].getP2()].getFlag() == 1) &&
	      (points[triangles[*itT].getP3()].getFlag() == 1)) {

	    assert(triangles[*itT].getFlag() == 0);
	    mp.addTriangle(*itT);
	    triangles[*itT].setFlag(1);

	    VertexID idP1 = triangles[*itT].getP1();
	    VertexID idP2;
	    if (idP1 == *itN) {
	      idP1 = triangles[*itT].getP2();
	      idP2 = triangles[*itT].getP3();
	    }
	    else
	      idP2 = triangles[*itT].getP2();
	    if (idP2 == *itN)
	      idP2 = triangles[*itT].getP3();
	  }

      }

  }

}

unsigned int Mesh::buildBorderColor(CImgList<unsigned char> & colors, CImgList<VertexID> & primitives) {
  const int sizeTexture = 16;
  const unsigned char bgColor[3] = {255, 255, 255};
  const unsigned char lineColor[3] = {255, 0, 0};
  CImg<unsigned char> texture(sizeTexture, sizeTexture, 1, 3, 0);
  bool hasBorders = false;

  colors.clear();
  primitives.clear();

  // build triangles
  CImgList<unsigned char> textures;
  for(unsigned int i3 = 0; i3 < 2; ++i3)
    for(unsigned int i2 = 0; i2 < 2; ++i2)
      for(unsigned int i1 = 0; i1 < 2; ++i1) {
	texture.draw_rectangle(0, 0, sizeTexture - 1, sizeTexture - 1, bgColor);
	if(i1 == 1)
	  texture.draw_line(0, 0, 0, sizeTexture - 1, lineColor);
	if(i2 == 1) { // quick and dirty drawing of a diagonal edge
	  texture.draw_line(sizeTexture - 3, 0, 0, sizeTexture - 3, lineColor);
	  texture.draw_line(sizeTexture - 2, 0, 0, sizeTexture - 2, lineColor);
	  texture.draw_line(sizeTexture - 1, 0, 0, sizeTexture - 1, lineColor);
	}
	if(i3 == 1)
	  texture.draw_line(0, 0, sizeTexture - 1, 0, lineColor);
	textures.insert(texture);
      }


  CImg<unsigned int> triangle_t(9);
  triangle_t[3] = 0; triangle_t[4] = 0;
  triangle_t[5] = 0; triangle_t[6] = sizeTexture - 1;
  triangle_t[7] = sizeTexture - 1; triangle_t[8] = 0;

  for(std::vector<Triangle>::const_iterator c = triangles.begin(); c != triangles.end(); ++c) {
    bool isBoundary1 = isBoundaryEdge((*c).getP1(), (*c).getP2());
    bool isBoundary2 = isBoundaryEdge((*c).getP2(), (*c).getP3());
    bool isBoundary3 = isBoundaryEdge((*c).getP3(), (*c).getP1());
    hasBorders = hasBorders || (isBoundary1 || isBoundary2 || isBoundary3);
    triangle_t[0] = (*c).getP1();
    triangle_t[1] = (*c).getP2();
    triangle_t[2] = (*c).getP3();
    unsigned int idTexture = (isBoundary1?1:0) + 2 * (isBoundary2?1:0) + 4 * (isBoundary3?1:0);

    primitives.insert(triangle_t);
    colors.insert(textures[idTexture]);
    for(unsigned int i = 0; i < 16; ++i)
      for(unsigned int j = 0; j < 16; ++j)
	if (colors[colors._width - 1](i, j) != 255)
	  std::cout << "couleur (" << i << ", " << j << "): " << colors[(*c).getId()](i, j) << std::endl;

  }

  if (hasBorders)
    return (lineColor[2] * 256 + lineColor[1]) * 256 + lineColor[0];
  else
    return 0;
}


bool Mesh::isBoundaryTriangle(TriangleID id) const {
  assert(id < getNbTriangles());
  unsigned int nbBE = 0;
  if (isBoundaryEdge(triangles[id].getP1(), triangles[id].getP2()))
    ++nbBE;
  if (isBoundaryEdge(triangles[id].getP1(), triangles[id].getP3()))
    ++nbBE;
  if (isBoundaryEdge(triangles[id].getP3(), triangles[id].getP2()))
    ++nbBE;

  return nbBE >= 2;
}

bool Mesh::isBoundaryEdge(const PointOnEdge & p) const {
  if (p.isVertex())
    return point(p.getClosestVertex()).getIsBoundary();
  else
    return isBoundaryEdge(p.getFirstVertex(), p.getSecondVertex());
}

bool Mesh::isBoundaryEdge(VertexID idP1, VertexID idP2) const {
  assert((idP1 < getNbPoints()) && (idP2 < getNbPoints()));
  const std::deque<TriangleID> & tr = points[idP1].getTriangles();

  if (!points[idP1].getIsBoundary() || !points[idP2].getIsBoundary())
    return false;

  if (points[idP1].hasNeighbour(idP2) && (getNbTriangles(idP1, idP2) == 0))
    return true;

  bool inside = false;

  for(std::deque<TriangleID>::const_iterator i = tr.begin(); i != tr.end(); ++i)
    if (triangles[*i].hasPoint(idP2)) {
      if (inside) // another neighbour triangle has been found inside
	return false;
      else
	inside = true;
    }

  return inside;
}

bool Mesh::isInsideEdge(const std::deque<VertexID> & path, VertexID v1, VertexID v2) {
  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
    if (*p == v1) {
      if (p + 1 == path.end()) {
	if (path.front() == v2)
	  return true;
      }
      else
	if (*(p + 1) == v2)
	  return true;
    }
    else if (*p == v2) {
      if (p + 1 == path.end()) {
	if (path.front() == v1)
	  return true;
      }
      else
	if (*(p + 1) == v1)
	  return true;
    }
  }

  return false;
}

bool Mesh::isNonSeparatingCycle(const std::deque<VertexID> & path) {
  if ((path.size() <= 2) || (path.front() != path.back()))
    return false;

  setPointFlag(0);
  setTriangleFlag(0);

  bool isBoundary = true;
  for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
    if (!points[*p].getIsBoundary())
      isBoundary = false;
    points[*p].setFlag(1);
  }
  // if the cycle is a boundary cycle, it is a separating cycle
  if (isBoundary)
    return false;

  std::vector<TriangleID> open;
  open.push_back(findTriangle(path.front(), path[1]).getId());
  triangles[open.front()].setFlag(1);
  TriangleID nbSeenTriangles = 1;

  while(open.size() != 0) {
    TriangleID next = open.back();
    open.pop_back();

    for(unsigned int i = 0; i < 3; ++i)
      try {
	VertexID v1 = triangles[next].getVertexId(i);
	VertexID v2 = triangles[next].getVertexId((i + 1) % 3);
	if ((points[v1].getFlag() == 0) ||
	    (points[v2].getFlag() == 0) ||
	    !isInsideEdge(path, v1, v2)) {
	  Triangle & ti = findOtherTriangle(v1, v2, next);
	  if (ti.getFlag() == 0) {
	    open.push_back(ti.getId());
	    ti.setFlag(1);
	    ++nbSeenTriangles;
	  }
	}
      }
      catch (Exception e) {}
  }

  return nbSeenTriangles == nbTriangles;
}


bool Mesh::isBoundaryCycle(const std::deque<VertexID> & path) const {
  if (path.size() <= 2)
    return false;

  std::deque<VertexID>::const_iterator lasti = path.begin();

  for(std::deque<VertexID>::const_iterator i = path.begin() + 1; i != path.end(); ++i)
    if (!isBoundaryEdge(*lasti, *i))
      return false;

  return isBoundaryEdge(path.front(), path.back());
}

TriangleID Mesh::addTriangle(const Triangle & triangle_t) {
  triangles.push_back(triangle_t);
  triangles.back().setId(nbTriangles);
  ++nbTriangles;

  return nbTriangles - 1;
}

void Mesh::updateNeighboursAndTriangles() {
  for(std::vector<Triangle>::const_iterator i = triangles.begin(); i != triangles.end(); ++i) {
    points[(*i).getP1()].addTriangle((*i).getP2(), (*i).getP3(), (*i).getId());
    points[(*i).getP2()].addTriangle((*i).getP1(), (*i).getP3(), (*i).getId());
    points[(*i).getP3()].addTriangle((*i).getP1(), (*i).getP2(), (*i).getId());
  }
}

TriangleID Mesh::addTriangleFull(const Triangle & triangle_t) {
  triangles.push_back(triangle_t);
  triangles.back().setId(nbTriangles);

  assert(triangle_t.getP1() < nbPoints);
  assert(triangle_t.getP2() < nbPoints);
  assert(triangle_t.getP3() < nbPoints);

  points[triangle_t.getP1()].addTriangle(triangle_t.getP2(), triangle_t.getP3(), nbTriangles);
  points[triangle_t.getP2()].addTriangle(triangle_t.getP1(), triangle_t.getP3(), nbTriangles);
  points[triangle_t.getP3()].addTriangle(triangle_t.getP1(), triangle_t.getP2(), nbTriangles);

  ++nbTriangles;

  if ((triangle_t.getP1() >= nbPoints) ||
      (triangle_t.getP2() >= nbPoints) ||
      (triangle_t.getP3() >= nbPoints))
    throw Exception("addTriangleFull(1): Adding an incoherent triangle.");


  return nbTriangles - 1;
}



VertexID Mesh::addPoint(const Point3D & aPoint) {
  points.push_back(aPoint);
  points.back().setId(nbPoints);
  ++nbPoints;

  return nbPoints - 1;
}

VertexID Mesh::addPoints(const std::deque<VertexID> & aPoints, bool simple) {
  VertexID result = nbPoints;

  points.reserve(nbPoints + aPoints.size());

  for(std::deque<VertexID>::const_iterator i = aPoints.begin();
      i != aPoints.end(); ++i) {
    points.push_back(points[*i]);
    if (simple)
      points.back().clearNeighboursAndTriangles();
    points.back().setId(nbPoints);
    ++nbPoints;
  }

  return result;

}

unsigned int Mesh::getNbPointFlagValues() const {
  std::set<int, ltint<int> > values;
  for(std::vector<Point3D>::const_iterator c = points.begin(); c != points.end(); ++c)
    values.insert((*c).getFlag());

  return values.size();
}

void Mesh::tracePoint(VertexID id) const {
  std::cout << ":::: trace " << id << "(idReel: " << points[id].getId() << ", isBoundary: " << points[id].getIsBoundary() << ", flag: " << points[id].getFlag() << ")"<< ": "<<  std::endl << " points: ";
  VertexID last = points[id].getNeighbours().front();
  for(std::deque<VertexID>::const_iterator i = points[id].getNeighbours().begin(); i != points[id].getNeighbours().end(); ++i) {
    if (points[*i].hasNeighbour(last))
      std::cout << "->";
    else
      std::cout << "  ";
    std::cout << *i << "[" << points[*i].getFlag() << "]";
    if (points[*i].getIsBoundary())
      std::cout << "(b)";
    last = *i;
  }
  if ((points[id].getNeighbours().size() != 0) && points[points[id].getNeighbours().back()].hasNeighbour(points[id].getNeighbours().front()))
    std::cout << "->";

  std::cout <<  std::endl << " triangles: ";
  for(std::deque<TriangleID>::const_iterator i = points[id].getTriangles().begin(); i != points[id].getTriangles().end(); ++i)
    std::cout << " " << triangles[*i];
  std::cout <<  std::endl;

}


VertexID Mesh::getMiddlePoint(const std::deque<VertexID> & path) const {
  double length = getLength(path) / 2;
  double l = 0.0;

  std::deque<VertexID>::const_iterator last = path.begin();
  for(std::deque<VertexID>::const_iterator i = path.begin(); i != path.end(); ++i)
    if (i != last) {
      l += points[*i].distance(points[*last]);
      if (l >= length)
	return *i;
      ++last;
    }

  assert(false);
  return getNbPoints();
}


VertexID Mesh::getMiddlePoint(const std::vector<VertexID> & path) const {
  double length = getLength(path) / 2;
  double l = 0.0;

  std::vector<VertexID>::const_iterator last = path.begin();
  for(std::vector<VertexID>::const_iterator i = path.begin(); i != path.end(); ++i)
    if (i != last) {
      l += points[*i].distance(points[*last]);
      if (l >= length)
	return *i;
      ++last;
    }

  assert(false);
  return getNbPoints();
}


double Mesh::getLength(const std::vector<VertexID> & path) const {
  double result = 0.0;
  std::vector<VertexID>::const_iterator last = path.begin();
  for(std::vector<VertexID>::const_iterator i = path.begin() + 1; i != path.end(); ++i) {
    result += points[*i].distance(points[*last]);
    ++last;
  }

  return result;
}

double Mesh::getLength(const std::deque<VertexID> & path) const {
  double result = 0.0;
  std::deque<VertexID>::const_iterator last = path.begin();
  for(std::deque<VertexID>::const_iterator i = path.begin() + 1; i != path.end(); ++i) {
    result += points[*i].distance(points[*last]);
    ++last;
  }

  return result;
}

double Mesh::getLength(const NLoop & nloop) const {
  double result = 0.0;
  for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p)
    result += getLength(*p);

  return result;
}

double Mesh::getSqrLength(const NLoop & nloop) const {
  double result = 0.0;
  for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p) {
    double l = getLength(*p);
    result += l * l;
  }

  return result;
}

double Mesh::getSqrtLength(const NLoop & nloop) const {
  double result = 0.0;
  for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p) {
    result += sqrt(getLength(*p));
  }

  return result;
}
double Mesh::getLength(const MultiEdge & e) const {
  if (e.hasMiddlePoint())
    return points[e.getFirstId()].distance(points[e.getMiddleId()]) +
      points[e.getMiddleId()].distance(points[e.getLastId()]);
  else
    return points[e.getFirstId()].distance(points[e.getLastId()]);
}


double Mesh::getLength(const std::deque<VertexID> & path, LengthEdge & lEdge) const {
  LengthEdge * lEdgeLocal = lEdge.clone(*this);
  double result = 0;
  std::deque<VertexID>::const_iterator last = path.begin();
  for(std::deque<VertexID>::const_iterator i = path.begin(); i != path.end(); ++i)
    if (i != last) {
      assert(*i < nbPoints);
      assert(*last < nbPoints);
      result += (*lEdgeLocal)(*i, *last);
      ++last;
    }

  delete(lEdgeLocal);
  return result;

}

Coord3D Mesh::getMeanPoint(const MultiEdge & e) const {
  if (e.hasMiddlePoint())
    return points[e.getMiddleId()];
  else
    return (points[e.getFirstId()] + points[e.getLastId()]) / 2.;
}


Coord3D Mesh::computeTriangleNormal(TriangleID tId) const {
  // first get points
  const Point3D & p1 = points[triangles[tId].getP1()];
  const Point3D & p2 = points[triangles[tId].getP2()];
  const Point3D & p3 = points[triangles[tId].getP3()];
  Coord3D result;

  // then compute (p1, p2)-(p1, p3) normal
  result[0] = (p2.getY() - p1.getY()) * (p3.getZ() - p1.getZ()) -
    (p2.getZ() - p1.getZ()) * (p3.getY() - p1.getY());
  result[1] = (p2.getZ() - p1.getZ()) * (p3.getX() - p1.getX()) -
    (p2.getX() - p1.getX()) * (p3.getZ() - p1.getZ());
  result[2] = (p2.getX() - p1.getX()) * (p3.getY() - p1.getY()) -
    (p2.getY() - p1.getY()) * (p3.getX() - p1.getX());

  return result;
}


Coord3D Mesh::computePointNormal(VertexID pId) const {
  Coord3D normal(0.0, 0.0, 0.0);
  for(std::deque<VertexID>::const_iterator ts = points[pId].getTriangles().begin(); ts != points[pId].getTriangles().end(); ++ts)
    normal += computeTriangleNormal(*ts);

  return normal;
}


std::pair<TriangleID, TriangleID> Mesh::findTriangles(const Point3D & p1, const Point3D & p2) const {
  TriangleID nbT = 0;
  TriangleID tId[2];
  tId[0] = nbTriangles;
  tId[1] = nbTriangles;

  // find triangles
  for(std::deque<TriangleID>::const_iterator t = p1.getTriangles().begin();
      t != p1.getTriangles().end(); ++t) {
    if (triangles[*t].hasPoint(p2))
      tId[nbT++] = *t;
    if (nbT == 2)
      break;
  }

  return std::pair<TriangleID, TriangleID> (tId[0], tId[1]);

}

std::pair<TriangleID, TriangleID> Mesh::findTriangles(VertexID idP1, VertexID idP2) const {
  return findTriangles(points[idP1], points[idP2]);
}

double Mesh::computeAngle(TriangleID idT1, TriangleID idT2) const {
  // first compute the normals
  Coord3D n1 = computeTriangleNormal(idT1).normalize();
  Coord3D n2 = computeTriangleNormal(idT2).normalize();
  // then compute normal's angles
  double value = (n1[0] * n2[0] + n1[1] * n2[1] + n1[2] * n2[2]) /
    sqrt((n1[0] * n1[0] + n1[1] * n1[1] + n1[2] * n1[2]) *
	 (n2[0] * n2[0] + n2[1] * n2[1] + n2[2] * n2[2]));
  if (value > 1) value = 1;
  if (value < -1) value = -1;

  return acos(value);
}

double Mesh::computeAngle(VertexID firstId, VertexID nextId1, VertexID nextId2) const {
  assert(firstId < nbPoints);
  assert(nextId1 < nbPoints);
  assert(nextId2 < nbPoints);
  Coord3D n1 = points[nextId1] - points[firstId];
  Coord3D n2 = points[nextId2] - points[firstId];

  double value = (n1[0] * n2[0] + n1[1] * n2[1] + n1[2] * n2[2]) /
    sqrt((n1[0] * n1[0] + n1[1] * n1[1] + n1[2] * n1[2]) *
	 (n2[0] * n2[0] + n2[1] * n2[1] + n2[2] * n2[2]));
  if (value > 1) value = 1;
  if (value < -1) value = -1;

  return acos(value);

}


double Mesh::computeMeanLocalCurvature(VertexID idP) const {
  VertexID nbNb = 0;
  double curv = 0.0;
  for(std::deque<VertexID>::const_iterator i = points[idP].getNeighbours().begin();
      i != points[idP].getNeighbours().end(); ++i) {
    double lC = computeLocalCurvature(idP, *i);
    if (lC <= 2 * M_PI) {
      curv += lC;
      ++nbNb;
    }
  }

  if (nbNb != 0)
    return curv / nbNb;
  else
    return 42;
}


double Mesh::computeLocalCurvature(VertexID idP1, VertexID idP2) const {
  std::pair<TriangleID, TriangleID> idTs = findTriangles(idP1, idP2);

  // if triangles not found, angle cannot be computed: return bad angle value
  if ((idTs.first == nbTriangles) || (idTs.second == nbTriangles))
    return 42;
  else
    return computeAngle(idTs.first, idTs.second);
}


VertexID Mesh::nbBoundaryEdges(VertexID idP) const {
  VertexID result = 0;

  for(std::deque<VertexID>::const_iterator i = points[idP].getNeighbours().begin(); i != points[idP].getNeighbours().end(); ++i)
    if (isBoundaryEdge(*i, idP))
      ++result;

  return result;

}

void Mesh::buildTexturedTriangleList(const CImg<unsigned char> & image, CImgList<VertexID> & primitives, CImgList<unsigned char> & textures, double zratio) {
  CImg<unsigned int> primitive(9);
  CImg<unsigned char> texture(64, 64, 1, 3);
  unsigned char color[] = {255, 255, 255};
  unsigned char bgColor[] = {0, 0, 0};

  texture.draw_rectangle(0, 0, 63, 63, bgColor);
  primitives.clear();
  textures.clear();

  primitive[3] = 0; primitive[4] = 0;
  primitive[5] = 63; primitive[6] = 0;
  primitive[7] = 0; primitive[8] = 63;

  // check for max texture value
  unsigned int min = 255;
  unsigned int max = 0;
  for(std::vector<Point3D>::const_iterator c = points.begin(); c != points.end(); ++c) {
    if (min > image((unsigned int) (*c).getX(), (unsigned int) (*c).getY(), (unsigned int) ((*c).getZ() / zratio)))
      min = image((unsigned int) (*c).getX(), (unsigned int) (*c).getY(), (unsigned int) ((*c).getZ() / zratio));
    if (max < image((unsigned int) (*c).getX(), (unsigned int) (*c).getY(), (unsigned int) ((*c).getZ() / zratio)))
      max = image((unsigned int) (*c).getX(), (unsigned int) (*c).getY(), (unsigned int) ((*c).getZ() / zratio));
  }

  // build textured triangle list
  for(std::vector<Triangle>::const_iterator t = triangles.begin(); t != triangles.end(); ++t) {
    for(unsigned int i = 0; i < 3; ++i) {
      primitive[i] = (*t).getVertexId(i);
    }
    primitives.insert(primitive);
    texture.draw_triangle(0, 0, 63, 0, 0, 63, color,
			  0.1 + 0.9 * ((float) image((unsigned int) points[(*t).getP1()].getX(), (unsigned int) points[(*t).getP1()].getY(), (unsigned int) (points[(*t).getP1()].getZ() / zratio)) - min) / (max - min + 1),
			  0.1 + 0.9 * ((float) image((unsigned int) points[(*t).getP2()].getX(), (unsigned int) points[(*t).getP2()].getY(), (unsigned int) (points[(*t).getP2()].getZ() / zratio)) - min) / (max - min + 1),
			  0.1 + 0.9 * ((float) image((unsigned int) points[(*t).getP3()].getX(), (unsigned int) points[(*t).getP3()].getY(), (unsigned int) (points[(*t).getP3()].getZ() / zratio)) - min) / (max - min + 1));
    textures.insert(texture);
  }


}




VertexID Mesh::removeNonRealPoints() {
  VertexID result = 0;
  VertexID p = 0;
  while (p < nbPoints)
    if (!points[p].getIsRealPoint()) {
      removePoint(p);
      ++result;
    }
    else
      ++p;

  return result;
}

void Mesh::setAllRealPoints() {
  for(std::vector<Point3D>::iterator c = points.begin(); c != points.end(); ++c)
    (*c).setIsRealPoint(true);
}

void Mesh::removeTriangle(TriangleID triangleId, bool simple) {
  Triangle & t = triangles[triangleId];

  if (!simple) {
    // remove the triangle from the other points
    for(unsigned int i = 0; i < 3; ++i)
      points[t.getVertexId(i)].removeTriangle(triangleId);

    // remove neighbours for each point if it is necessary
    try {
      findOtherTriangle(t.getP1(), t.getP2(), triangleId);
    } catch (Exception e) {
      points[t.getP1()].removeNeighbour(t.getP2());
      points[t.getP2()].removeNeighbour(t.getP1());
    }
    try {
      findOtherTriangle(t.getP3(), t.getP2(), triangleId);
    } catch (Exception e) {
      points[t.getP3()].removeNeighbour(t.getP2());
      points[t.getP2()].removeNeighbour(t.getP3());
    }
    try {
      findOtherTriangle(t.getP1(), t.getP3(), triangleId);
    } catch (Exception e) {
      points[t.getP1()].removeNeighbour(t.getP3());
      points[t.getP3()].removeNeighbour(t.getP1());
    }
  }

  // remove the triangle
  triangles.erase(triangles.begin() + triangleId);

  // update the id of the other triangles
  for(std::vector<Triangle>::iterator i = triangles.begin() + triangleId; i != triangles.end(); ++i)
    (*i).incId(-1);
  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i)
    for(std::deque<TriangleID>::iterator tt = (*i).getTriangles().begin(); tt != (*i).getTriangles().end(); ++tt)
      if (*tt >= triangleId)
	(*tt) -= 1;

  --nbTriangles;
}


void Mesh::removePoint(VertexID pointId, bool setIsBoundaryPoints) {
  Point3D point_l = points[pointId];

  // first update neighbours (remove point and associated triangles, and set isBoundary flag
  for(std::deque<VertexID>::const_iterator n = point_l.getNeighbours().begin(); n != point_l.getNeighbours().end(); ++n) {
    points[*n].removeNeighbour(pointId);
    for(std::deque<TriangleID>::const_iterator t = point_l.getTriangles().begin(); t != point_l.getTriangles().end(); ++t)
      if (points[*n].hasTriangle(*t))
	points[*n].removeTriangle(*t);
    if (setIsBoundaryPoints)
      points[*n].setIsBoundary(true);
  }

  // compute point list
  points.erase(points.begin() + pointId);
  for(std::vector<Point3D>::iterator i = points.begin() + pointId; i != points.end(); ++i) {
    (*i).incId(-1);
    for(std::deque<TriangleID>::const_iterator t = (*i).getTriangles().begin(); t != (*i).getTriangles().end(); ++t)
      triangles[*t].replacePoint((*i).getId() + 1, (*i).getId());
  }

  // then compute triangle list
  std::deque<TriangleID> removed = point_l.getTriangles();
  std::sort(removed.begin(), removed.end());
  for(int r = removed.size() - 1; r >=0 ; --r) {
    triangles.erase(triangles.begin() + removed[r]);
  }

  // reinit triangle ids
  TriangleID tid = 0;
  for(std::vector<Triangle>::iterator t = triangles.begin(); t != triangles.end(); ++t, ++tid)
    (*t).setId(tid);

  for(std::vector<Point3D>::iterator i = points.begin(); i != points.end(); ++i) {
    for(std::deque<VertexID>::iterator n = (*i).getNeighbours().begin(); n != (*i).getNeighbours().end(); ++n)
      if (*n >= pointId)
	(*n) -= 1;
    if (removed.size() != 0)
      for(std::deque<TriangleID>::iterator t = (*i).getTriangles().begin(); t != (*i).getTriangles().end(); ++t) {
	unsigned int nbSup = 0;
	for(std::deque<TriangleID>::const_iterator r = removed.begin(); r < removed.end(); ++r)
	  if (*t < *r)
	    break;
	  else
	    ++nbSup;
	(*t) -= nbSup;
      }
  }


  nbTriangles -= removed.size();
  --nbPoints;

}

std::deque<VertexID> Mesh::getNonRealPoints() const {
 std::deque<VertexID> result;
  for(std::vector<Point3D>::const_iterator c = points.begin(); c != points.end(); ++c)
    if (!(*c).getIsRealPoint())
     result.push_front((*c).getId());

  return result;
}

VertexID Mesh::getBoundaryPoint() const {

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary())
      return (*p).getId();

  throw Exception("getBoundaryPoint(): cannot find a boundary point");
}


std::deque<VertexID> Mesh::getBoundaryPoints() const {

  std::deque<VertexID> result;

  // for each point inside MeshPart, set flag value = 1 if is boundary one
   for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
     if ((*p).getIsBoundary()) {
       assert((*p).getId() < nbPoints);
       result.push_back((*p).getId());
     }

  return result;
}

unsigned int Mesh::getNbBoundaries() {
  unsigned int nbBoundaries = 0;

  // for each point inside MeshPart, set flag value = 1 if is boundary one
   for(Mesh::point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary())
      (*p).setFlag(1);
    else
      (*p).setFlag(0);

  // for each point inside MeshPart
   for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    // if point is in a new boundary
    if ((*p).getFlag() == 1) {
      // compute this boundary (set flag = 2 for each point inside boundary)
      computeBCCUsingPoints((*p).getId(), 1, 2);
      ++nbBoundaries;
    }

  return nbBoundaries;
}

VertexID Mesh::computeBCCUsingPoints(VertexID id, int iFlag, int fFlag) {
  if (points[id].getFlag() != iFlag)
    return 0;

  std::deque<VertexID> & nb = points[id].getNeighbours();
  VertexID result = 0;

  points[id].setFlag(fFlag);

  for(std::deque<VertexID>::const_iterator i = nb.begin(); i != nb.end(); ++i)
    if ((points[*i].getFlag() == iFlag) && (isBoundaryEdge(id, *i)))
      result += computeBCCUsingPoints(*i, iFlag, fFlag);


  return result;
}


VertexID Mesh::getNbBoundaryPoints() const {
  VertexID result = 0;

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary())
      ++result;

  return result;
}

bool Mesh::hasBoundaryPoint() const {

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary())
      return true;

  return false;
}


unsigned int Mesh::getGenus() {
  // TODO assert(isOrientable());
  // TODO nb connected components
  return (2 - getEulerCaracteristic() - getNbBoundaries()) / 2;
}

unsigned int Mesh::getNbEdges() const {
  VertexID nbB = getNbBoundaryPoints();
  if (nbTriangles != 0)
    return (nbTriangles * 3 - nbB) / 2 + nbB;
  else
    return 0;
}

double Mesh::getBoundaryLength() const {
  double result = 0;

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary())
      for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
	if ((*nb < (*p).getId()) && (isBoundaryEdge((*p).getId(), *nb)))
	  result += (*p).distance(points[*nb]);

  return result;
}

int Mesh::getEulerCaracteristic() const {
  return nbPoints - getNbEdges() + nbTriangles;
}

int Mesh::getBettiNumber() {
  return 1 - getEulerCaracteristic() + (hasBoundaryPoint() ? 0 : 1);
}

unsigned int Mesh::getNbCC() {
  std::deque<MeshPart> cc = getConnectedComponents();

  return cc.size();
}


VertexID Mesh::getNextBPointSingle(VertexID p, VertexID oldP) const {
  const Point3D & pt = points[p];
  if (!pt.getIsBoundary())
    throw Exception("getNextBPointSingle(2): Given point is not a boundary point");

  for(std::deque<VertexID>::const_iterator nb = pt.getNeighbours().begin(); nb != pt.getNeighbours().end(); ++nb)
    if (*nb != oldP)
      if (points[*nb].getIsBoundary() && isBoundaryEdge(p, *nb))
      return *nb;

 throw Exception("getNextBPointSingle(2): next boundary point cannot be found.");
}

VertexID Mesh::getNbNextBPoint(VertexID p) const {
  const Point3D & pt = points[p];
  VertexID result = 0;

  if (!pt.getIsBoundary())
    throw Exception("getNextBPoint(1): Given point is not a boundary point");

  for(std::deque<VertexID>::const_iterator nb = pt.getNeighbours().begin(); nb != pt.getNeighbours().end(); ++nb)
    if (points[*nb].getIsBoundary() && isBoundaryEdge(p, *nb))
      ++result;

  return result;
}

VertexID Mesh::getNextBPointSingle(VertexID p) const {
  const Point3D & pt = points[p];

  if (!pt.getIsBoundary())
    throw Exception("getNextBPointSingle(1): Given point is not a boundary point");

  for(std::deque<VertexID>::const_iterator nb = pt.getNeighbours().begin(); nb != pt.getNeighbours().end(); ++nb)
    if (points[*nb].getIsBoundary() && isBoundaryEdge(p, *nb) && (points[*nb].getNbTriangles() == 0))
      return *nb;

  throw Exception("getNextBPointSingle(1): next boundary point cannot be found.");
}



VertexID Mesh::getNextBPoint(VertexID p) const {
  const Point3D & pt = points[p];

  if (!pt.getIsBoundary())
    throw Exception("getNextBPoint(1): Given point is not a boundary point");

  for(std::deque<VertexID>::const_iterator nb = pt.getNeighbours().begin(); nb != pt.getNeighbours().end(); ++nb)
    if (points[*nb].getIsBoundary() && isBoundaryEdge(p, *nb) && (points[*nb].getNbTriangles() != 0))
      return *nb;

  throw Exception("getNextBPoint(1): next boundary point cannot be found.");
}

VertexID Mesh::getNextBPoint(VertexID p, VertexID oldP) const {
  const Point3D & pt = points[p];

  if (!pt.getIsBoundary())
    throw Exception("getNextBPoint(2): Given point is not a boundary point");

  for(std::deque<VertexID>::const_iterator nb = pt.getNeighbours().begin(); nb != pt.getNeighbours().end(); ++nb)
    if (*nb != oldP) {
      if (points[*nb].getIsBoundary() && isBoundaryEdge(p, *nb))
	return *nb;
    }

  throw Exception("getNextBPoint(2): next boundary point cannot be found.");
}

bool Mesh::hasNeighbourSameLocation(VertexID p1, VertexID p2) const {
  if (p1 >= nbPoints)
    throw Exception("hasNeighbourSameLocation(2): p1 out of range");
  if (p2 >= nbPoints)
    throw Exception("hasNeighbourSameLocation(2): p2 out of range");

  const Point3D & pt2 = points[p2];

  for(std::deque<VertexID>::const_iterator nb = points[p1].getNeighbours().begin();
      nb != points[p1].getNeighbours().end(); ++nb)
    if (points[*nb].sameLocation(pt2))
      return true;

  return false;
}

std::pair<VertexID, VertexID> Mesh::flipEdge(VertexID p1, VertexID p2) {
  if (!points[p1].hasNeighbour(p2))
    throw Exception("flipEdge(2): points are not neighours.");

  try {
    Triangle & t1 = findTriangle(p1, p2);
    Triangle & t2 = findOtherTriangle(p1, p2, t1);

    Point3D & point1 = points[p1];
    Point3D & point2 = points[p2];

    Point3D & pointt1 = points[t1.getOtherPoint(p1, p2)];
    Point3D & pointt2 = points[t2.getOtherPoint(p1, p2)];

    // change neighbour info
    point1.removeNeighbour(p2);
    point2.removeNeighbour(p1);
    pointt1.addNeighbour(pointt2.getId());
    pointt2.addNeighbour(pointt1.getId());

    // change triangle info
    t1.replacePoint(p2, pointt2.getId());
    t2.replacePoint(p1, pointt1.getId());

    // remove and add triangle to the points
    point1.removeTriangle(t2.getId());
    point2.removeTriangle(t1.getId());
    pointt1.addTriangle(t2.getId());
    pointt2.addTriangle(t1.getId());

    // reorder points' info
    point1.orderNeighbours(triangles);
    point2.orderNeighbours(triangles);
    pointt1.orderNeighbours(triangles);
    pointt2.orderNeighbours(triangles);

    return std::pair<VertexID, VertexID>(pointt1.getId(), pointt2.getId());
  }
  catch (Exception e) {
    throw Exception("flipEdge(2): there is less than 2 triangles adjacents to (p1, p2).");
  }


}

VertexID Mesh::addPointOnEdge(const PointOnEdge & point) {
  const VertexID result = addPointOnEdge(point.getFirstVertex(), point.getSecondVertex(), point.getLocation());
  points[result].setCoords(point);
  return result;
}

VertexID Mesh::addPointOnEdge(VertexID p1, VertexID p2, double location) {
  if (!points[p1].hasNeighbour(p2))
    throw Exception("addPointOnEdge(3): points are not neighours.");
  if ((location < 0.) || (location > 1.))
    throw Exception("addPointOnEdge(3): bad location value.");

  if (location == 0.)
    return p1;
  if (location == 1.)
    return p2;

  // create a new point
  Point3D point_l((points[p1].getX() * (1 - location) + points[p2].getX() * location),
		  (points[p1].getY() * (1 - location) + points[p2].getY() * location),
		  (points[p1].getZ() * (1 - location) + points[p2].getZ() * location));
  // and add it to the mesh
  VertexID id = addPoint(point_l);
  Point3D & newPoint = points[id];
  assert(newPoint.getId() == id);

  // update neighbour list of p1, p2 and newPoint
  newPoint.addNeighbour(p1);
  newPoint.addNeighbour(p2);
  points[p1].replaceNeighbour(p2, id);
  points[p2].replaceNeighbour(p1, id);


  try {
    assert(newPoint.getId() == id);
    TriangleID t1 = findTriangle(p1, p2).getId();

    try {
      TriangleID t2 = findOtherTriangle(p1, p2, t1).getId();
      // update triangles and other point in t2
      udpateTrianglesAddPointOnEdge(t2, p1, p2, newPoint);
    }
    catch (Exception e) {
      newPoint.setIsBoundary(true);
    }
    // then update triangles and other point in t1
    udpateTrianglesAddPointOnEdge(t1, p1, p2, newPoint);
  }
  catch (Exception e) {
    newPoint.setIsBoundary(true);
  }


  return id;
}

void Mesh::udpateTrianglesAddPointOnEdge(TriangleID idT, VertexID p1, VertexID p2, Point3D & newPoint) {
  Triangle & t = triangles[idT];
  VertexID p3 = t.getOtherPoint(p1, p2);

  // first add neighbours
  points[p3].addNeighbourBetweenPoints(newPoint.getId(), p1, p2);
  newPoint.addNeighbourBetweenPoints(p3, p1, p2);

  // then create a new triangle cutting "t"
  Triangle triangle_l = t;
  triangle_l.replacePoint(p2, newPoint.getId());
  t.replacePoint(p1, newPoint.getId());

  // and add it to the mesh
  TriangleID newTriangle = addTriangle(triangle_l);

  // then add triangles to newPoint and p3
  points[p3].addTriangle(newTriangle);
  newPoint.addTriangle(newTriangle);
  newPoint.addTriangle(idT);

  // then update p1 triangle
  points[p1].replaceTriangle(idT, newTriangle);
}


VertexID Mesh::mergePointsOnMiddle(VertexID p1, VertexID p2) {
  return mergePoints(p1, p2, (points[p1] + points[p2]) / 2);
}


VertexID Mesh::mergePointsOnFirst(VertexID p1, VertexID p2) {
  return mergePoints(p1, p2, Coord3D(points[p1]));
}

VertexID Mesh::mergePoints(VertexID p1, VertexID p2, const Coord3D & newLocation) {
  bool remove = false;
  VertexID newP = (p1 > p2? p1 - 1 : p1);

  TriangleID nbTrianglesInit = nbTriangles;
  Point3D & pt1 = points[p1];
  Point3D & pt2 = points[p2];
  if (!pt1.hasNeighbour(p2))
    throw Exception("mergePoints(3): points are not neighbours.");
  bool bd = pt1.getIsBoundary() || pt2.getIsBoundary();

  // find the one or two triangles that will be removed (with two identical points)
  TriangleID t1, t2;
  t1 = t2 = nbTrianglesInit;
  try {
    t1 = findTriangle(p1, p2).getId();
    t2 = findOtherTriangle(p1, p2, t1).getId();
  }
  catch(Exception e) { }

  // then add missing neibghours and triangles to p1
  VertexID first, last;
  first = last = p1;
  if (t1 != nbTriangles)
    first = triangles[t1].getOtherPoint(p1, p2);
  if (t2 != nbTriangles) {
    if (first == nbTriangles)
      first = triangles[t2].getOtherPoint(p1, p2);
    else
      last = triangles[t2].getOtherPoint(p1, p2);
  }

  VertexID current = first;
  VertexID pred = p1;
  while (current != last) {
    if (current != first) // add neighbour
      if (!pt1.addNeighbour(current))
	remove = true;

    VertexID tmp = current;
    current = pt2.findNextNeighbour(pred, current);
    pred = tmp;

    // add triangle
    if (!isBoundaryEdge(p2, pred)) {
      pt1.addTriangle(findTriangle(current, pred, p2).getId());
    }
  }
  // update p2's neighbours and triangles by replacing p2's id by p1's id
  for(std::deque<VertexID>::const_iterator n = pt2.getNeighbours().begin();
      n != pt2.getNeighbours().end(); ++n)
    if ((*n == p1) || points[*n].hasNeighbour(p1))
      points[*n].removeNeighbour(p2);
    else
      points[*n].replaceNeighbour(p2, p1);


  for(std::deque<TriangleID>::const_iterator t = pt2.getTriangles().begin();
      t != pt2.getTriangles().end(); ++t)
    if ((*t != t1) && (*t != t2))
      triangles[*t].replacePoint(p2, p1);



  // finally remove p2 and the bad triangles
  pt2.clearNeighboursAndTriangles();
  removePoint(p2);
  if (t1 != nbTrianglesInit) {
    removeTriangle(t1);
  }
  if (t2 != nbTrianglesInit) {
    if (t1 < t2)
      removeTriangle(t2 - 1);
    else
      removeTriangle(t2);
  }

  if (remove) {
    Point3D & p = points[newP];
    std::vector<VertexID> rPoints;
    for(std::deque<VertexID>::const_iterator n = p.getNeighbours().begin();
	n != p.getNeighbours().end(); ++n)
      if (((points[*n].getNbTriangles() == 2) && (triangles[points[*n].getTriangles().front()].isEqual(triangles[points[*n].getTriangles()[1]]))) ||
	  ((points[*n].getNbTriangles() == 4) && (points[*n].getNbNeighbours()) == 3)) {
	rPoints.push_back(*n);
      }

    std::sort(rPoints.begin(), rPoints.end());
    for(std::vector<VertexID>::reverse_iterator pp = rPoints.rbegin(); pp != rPoints.rend(); ++pp) {
      if (*pp < newP)
	--newP;
      removePoint(*pp, false);
    }

    for(std::deque<VertexID>::const_iterator n = points[newP].getNeighbours().begin();
	n != points[newP].getNeighbours().end(); ++n)
      initIsBoundary(points[*n]);
  }

  points[newP].orderNeighbours(triangles);
#ifndef NDEBUG

  initIsBoundary(points[newP]);
  if (points[newP].getIsBoundary() != bd)
    throw Exception("mergePoints(): Incoherent boundary property.");
#endif
  points[newP].setIsBoundary(bd);

  // set the new location of the merged point
  points[newP].setCoords(newLocation);

  return newP;
}

void Mesh::removePoints(const std::vector<VertexID> & rPoints) {
  std::vector<VertexID> points_l = rPoints;
  std::sort(points_l.begin(), points_l.end());
  std::reverse(points_l.begin(), points_l.end());
  for(std::vector<VertexID>::const_iterator p = points_l.begin(); p != points_l.end(); ++p)
    removePoint(*p);
}


std::pair<VertexID, VertexID> Mesh::findNextEdge(const std::pair<VertexID, VertexID> & edge, VertexID point_l) const {
  assert(points[edge.first].hasNeighbour(edge.second));
  assert((point_l == edge.first) || (point_l == edge.second));

  const Triangle & t = findTriangle(edge.first, edge.second);

  VertexID idP = t.getOtherPoint(edge.first, edge.second);
  assert(points[idP].hasNeighbour(point_l));

  return std::pair<VertexID, VertexID>(point_l, idP);
}

std::pair<VertexID, VertexID> Mesh::findOtherNextEdge(const std::pair<VertexID, VertexID> & edge, VertexID point_l,
							      const std::pair<VertexID, VertexID> & otherEdge) const {
  assert(points[edge.first].hasNeighbour(edge.second));
  assert((point_l == edge.first) || (point_l == edge.second));

  const Triangle & t = findTriangle(edge.first, edge.second);
  VertexID idP;

  if (t.hasEdge(otherEdge.first, otherEdge.second)) {
    const Triangle & t2 = findOtherTriangle(edge.first, edge.second, t);
    idP = t2.getOtherPoint(edge.first, edge.second);
  }
  else
    idP = t.getOtherPoint(edge.first, edge.second);

  assert(points[idP].hasNeighbour(point_l));

  return std::pair<VertexID, VertexID>(point_l, idP);
}


std::deque<VertexID> Mesh::getNeighboursBetweenPoints(VertexID idPoint, VertexID idPoint1, VertexID idPoint2) const {
  assert(points[idPoint].hasNeighbour(idPoint1) && points[idPoint].hasNeighbour(idPoint2));
  std::deque<VertexID> result;

  std::deque<VertexID>::const_iterator nbpred = points[idPoint].getNeighbours().begin();
  std::deque<VertexID>::const_iterator nb = ++(points[idPoint].getNeighbours().begin());

  // first find idPoint1
  while(*nb != idPoint1) {
    nbpred = nb;
    nb = points[idPoint].nextNeighbourIt(nb);
  }

  // then build result list
  while(*nb != idPoint2) {
    nbpred = nb;
    nb = points[idPoint].nextNeighbourIt(nb);

    if (!points[*nb].hasNeighbour(*nbpred))
      break; // the other way should be the good one

    if (*nb == idPoint2)
      return result; // we found all the neighbours between idPoint1 and idPoint2

    result.push_back(*nb);
  }

  // trying the other way
  result.clear();
  std::deque<VertexID>::const_reverse_iterator nbrpred = points[idPoint].getNeighbours().rbegin();
  std::deque<VertexID>::const_reverse_iterator nbr = ++(points[idPoint].getNeighbours().rbegin());


  // first find idPoint1
  while(*nbr != idPoint1) {
    nbrpred = nbr;
    nbr = points[idPoint].nextNeighbourRIt(nbr);
  }

  // then build result list
  while(*nbr != idPoint2) {
    nbrpred = nbr;
    nbr = points[idPoint].nextNeighbourRIt(nbr);

    if (!points[*nbr].hasNeighbour(*nbrpred))
      break; // list cannot be computed, boundary has been found on the two ways

    if (*nbr == idPoint2)
      return result; // we found all the neighbours between idPoint1 and idPoint2

    result.push_back(*nbr);
  }

  throw Exception("getNeighboursBetweenPoints(3): cannot find a neighbour list between the two given neighbours.");
}

Coord3D Mesh::getIsobarycenter() const {
  Coord3D result(0, 0, 0);

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p) {
    result += *p;
  }

  if (nbPoints != 0)
    result /= nbPoints;

  return result;
}


Coord3D Mesh::getIsobarycenter(const std::deque<VertexID> & pts) const {
  Coord3D result(0, 0, 0);

  for(std::deque<VertexID>::const_iterator p = pts.begin(); p != pts.end(); ++p)
    result += points[*p];

  if (pts.size() != 0)
    result /= pts.size();

  return result;
}

Coord3D Mesh::getIsobarycenter(const VTriangle & t) const {
  Coord3D result(0, 0, 0);

  for(unsigned int i = 0; i < 3; ++i)
    result += points[t.getVertexId(i)];

  result /= 3;

  return result;
}


Coord3D Mesh::getBarycenter(const std::deque<VertexID> & pts, const std::deque<double> & weights) const {
  assert(pts.size() == weights.size());
  Coord3D result(0.0, 0.0, 0.0);
  double wSum = 0.0;

  std::deque<double>::const_iterator w = weights.begin();
  for(std::deque<VertexID>::const_iterator p = pts.begin(); p != pts.end(); ++p, ++w) {
    result += points[*p] * (*w);
    wSum += *w;
  }

  if (wSum != 0)
    result /= wSum;

  return result;
}


double Mesh::getMaxDistanceXYZ() const {
  double minXValue = std::numeric_limits<double>::max();
  double maxXValue = -std::numeric_limits<double>::max();
  double minYValue = std::numeric_limits<double>::max();
  double maxYValue = -std::numeric_limits<double>::max();
  double minZValue = std::numeric_limits<double>::max();
  double maxZValue = -std::numeric_limits<double>::max();

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p) {
    if ((*p).getX() > maxXValue) maxXValue = (*p).getX();
    if ((*p).getX() < minXValue) minXValue = (*p).getX();
    if ((*p).getY() > maxYValue) maxYValue = (*p).getY();
    if ((*p).getY() < minYValue) minYValue = (*p).getY();
    if ((*p).getZ() > maxZValue) maxZValue = (*p).getZ();
    if ((*p).getZ() < minZValue) minZValue = (*p).getZ();
  }

  const double difX = maxXValue - minXValue;
  const double difY = maxYValue - minYValue;
  const double difZ = maxZValue - minZValue;

  if ((difX > difY) && (difX > difZ))
    return difX;
  else if (difY > difZ)
    return difY;
  else
    return difZ;
}

Box3D Mesh::getBox() const {

  double minXValue = std::numeric_limits<double>::max();
  double maxXValue = -std::numeric_limits<double>::max();
  double minYValue = std::numeric_limits<double>::max();
  double maxYValue = -std::numeric_limits<double>::max();
  double minZValue = std::numeric_limits<double>::max();
  double maxZValue = -std::numeric_limits<double>::max();

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p) {
    if ((*p).getX() > maxXValue) maxXValue = (*p).getX();
    if ((*p).getX() < minXValue) minXValue = (*p).getX();
    if ((*p).getY() > maxYValue) maxYValue = (*p).getY();
    if ((*p).getY() < minYValue) minYValue = (*p).getY();
    if ((*p).getZ() > maxZValue) maxZValue = (*p).getZ();
    if ((*p).getZ() < minZValue) minZValue = (*p).getZ();
  }

  return Box3D(minXValue, maxXValue, minYValue, maxYValue, minZValue, maxZValue);
}

std::string Mesh::getBoxSize() const {
  std::ostringstream result;


  Box3D box = getBox();

  double diff = box.getSizeX();
  double diffy = box.getSizeY();
  double diffz = box.getSizeZ();
  if (diffy > diff) diff = diffy;
  if (diffz > diff) diff = diffz;

  result << box << "; max: " << diff;

  return result.str();
}


Coord2D Mesh::computeBarycenterCoords(VertexID idP, VertexID idP1, VertexID idP2) const {
  const double epsilon = 1e-50;
  Coord2D result(0.0, 0.0);
  double d1 = points[idP].distance(points[idP1]);
  double d2 = points[idP].distance(points[idP2]);
  double distance = d1 + d2;

  if (distance >= epsilon) {
    result.set2DX(d2 / distance);
    result.set2DY(d1 / distance);
  }
  else {
    result.set2DX(0.5);
    result.set2DX(.5);
  }

  return result.normalize_sum();
}

Coord3D Mesh::computeBarycenterCoords(VertexID idP, VertexID idP1, VertexID idP2, VertexID idP3) const {
  const double epsilon = 1e-50;
  Coord3D result(0.0, 0.0, 0.0);
  double area = Coord3D::computeTriangleArea(points[idP1], points[idP2], points[idP3]);
  if (area == 0) {
    if (points[idP1].distance(points[idP2]) < epsilon) {
      Coord2D c = computeBarycenterCoords(idP, idP1, idP3);
      result.setX(c.get2DX());
      result.setZ(c.get2DY());
    }
    else if (points[idP1].distance(points[idP3]) < epsilon) {
      Coord2D c = computeBarycenterCoords(idP, idP1, idP2);
      result.setX(c.get2DX());
      result.setY(c.get2DY());
    }
    else if (points[idP3].distance(points[idP2]) < epsilon) {
      Coord2D c = computeBarycenterCoords(idP, idP3, idP1);
      result.setZ(c.get2DX());
      result.setX(c.get2DY());
    }
    else {
      result.setX(1./3);
      result.setY(1./3);
      result.setZ(1./3);
    }
  }
  else {
    result.setX(Coord3D::computeTriangleArea(points[idP], points[idP2], points[idP3]));
    result.setY(Coord3D::computeTriangleArea(points[idP], points[idP1], points[idP3]));
    result.setZ(Coord3D::computeTriangleArea(points[idP], points[idP1], points[idP2]));
  }

  return result.normalize_sum();
}

VertexID Mesh::getNextBPointToward(VertexID point_l, VertexID direction) const {
  VertexID firstNb = getNextBPoint(point_l);
  VertexID secondNb = getNextBPoint(point_l, firstNb);
  std::deque<VertexID> side1;
  std::deque<VertexID> side2;
  side1.push_back(firstNb);
  side2.push_back(secondNb);

  VertexID last1 = point_l;
  VertexID last2 = point_l;
  while((side1.back() != direction) && (side2.back() != direction)) {
    VertexID nextLast1 = side1.back();
    side1.push_back(getNextBPoint(side1.back(), last1));
    VertexID nextLast2 = side2.back();
    side2.push_back(getNextBPoint(side2.back(), last2));
    last1 = nextLast1;
    last2 = nextLast2;
  }
  if (side1.back() == direction)
    return firstNb;
  else
    return secondNb;
}



const Point3D & Mesh::getMiddlePoint() {
  std::list<VertexID> openSet;

  for(Mesh::point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary()) {
      openSet.push_back((*p).getId());
      (*p).setFlag(1);
    }
    else
      (*p).setFlag(0);

  if (openSet.size() == 0)
  return computePseudoCenter();


  VertexID front = nbPoints;
  while(openSet.size() != 0) {
    front = openSet.front();
    openSet.pop_front();

    for(std::deque<VertexID>::const_iterator nb = points[front].getNeighbours().begin();
	nb != points[front].getNeighbours().end(); ++nb)
      if (points[*nb].getFlag() == 0) {
	points[*nb].setFlag(1);
	openSet.push_back(*nb);
      }
  }
  assert(front != nbPoints);

  return points[front];
}


const Point3D & Mesh::computePseudoCenter() const {
  assert(getNbPoints() > 0);
  double minX, maxX, minY, maxY, minZ, maxZ;
  minX = minY = minZ = std::numeric_limits<double>::max();
  maxX = maxY = maxZ = -std::numeric_limits<double>::max();

  // compute min and max coords values
  for (Mesh::const_point_iterator point_l = (*this).point_begin();
       point_l != (*this).point_end(); ++point_l) {
    if ((*point_l).getX() < minX) minX = (*point_l).getX();
    if ((*point_l).getX() > maxX) maxX = (*point_l).getX();
    if ((*point_l).getY() < minY) minY = (*point_l).getY();
    if ((*point_l).getY() > maxY) maxY = (*point_l).getY();
    if ((*point_l).getZ() < minZ) minZ = (*point_l).getZ();
    if ((*point_l).getZ() > maxZ) maxZ = (*point_l).getZ();
  }

  // compute mesh center
  double centerX = (maxX - minX) / 2;
  double centerY = (maxY - minY) / 2;
  double centerZ = (maxZ - minZ) / 2;

  Mesh::const_point_iterator select = (*this).point_begin();
  double sqrDistance = (centerX - (*select).getX()) * (centerX - (*select).getX()) +
                       (centerY - (*select).getY()) * (centerY - (*select).getY()) +
                       (centerZ - (*select).getZ()) * (centerZ - (*select).getZ());
  double sqrDistanceTmp;

  // find point nearest to this center
  for (Mesh::const_point_iterator point_l = (*this).point_begin(); point_l != (*this).point_end(); ++point_l) {
     sqrDistanceTmp = (centerX - (*point_l).getX()) * (centerX - (*point_l).getX()) +
                      (centerY - (*point_l).getY()) * (centerY - (*point_l).getY()) +
                      (centerZ - (*point_l).getZ()) * (centerZ - (*point_l).getZ());
     if (sqrDistance > sqrDistanceTmp) {
       select = point_l;
       sqrDistance = sqrDistanceTmp;
     }
  }

  // return selected id
  return *select;
}


std::deque<Coord3D> Mesh::computePCAAxes() const {
  std::deque<Coord3D> result;
  CImg<double> matrix(3, 3);
  CImg<double> val(3), vec(3,3);
  double value;
  Coord3D isoB = getIsobarycenter();

  for(unsigned int i = 0; i < 3; ++i)
    for(unsigned int j = 0; j <= i; ++j) {
      value = 0.0;
      for(Mesh::const_point_iterator p1 = point_begin(); p1 != point_end(); ++p1) {
	Coord3D p = (*p1) - isoB;
	value += (p[i] * p[j]) / nbPoints;
      }
      matrix(i, j) = value;
      if (i != j)
	matrix(j, i) = value;
    }


  matrix.symmetric_eigen(val, vec);
  for(unsigned int i = 0; i < 3; ++i)
    result.push_back(Coord3D(vec(i, 0), vec(i, 1), vec(i, 2)));

  return result;
}

bool Mesh::isInside(const Coord3D & coord, TriangleID idT) const {
  const Triangle & t = triangles[idT];
  const Coord3D & a = points[t.getP1()];
  const Coord3D & b = points[t.getP2()];
  const Coord3D & c = points[t.getP3()];

  /* see http://www.blackpawn.com/texts/pointinpoly/default.html */
  return ((coord.distance(a) < 1e-7) || (coord.distance(b) < 1e-7) || (coord.distance(c) < 1e-7) ||
	  ((Coord3D::scalarProduct(Coord3D::vectorialProduct(b - a, coord - a), Coord3D::vectorialProduct(coord - a, c - a)) >= 0) &&
	   (Coord3D::scalarProduct(Coord3D::vectorialProduct(a - b, coord - b), Coord3D::vectorialProduct(coord - b, c - b)) >= 0) &&
	   (Coord3D::scalarProduct(Coord3D::vectorialProduct(a - c, coord - c), Coord3D::vectorialProduct(coord - c, b - c)) >= 0)));
}

VertexID Mesh::getLastIntersection(const Line3D & line) const {
  return getFirstLastIntersection(line).first;
}


std::pair<VertexID, VertexID> Mesh::getFirstLastIntersection(const Line3D & line) const {
  VertexID idFirst = nbPoints, idLast = nbPoints;
  double coords1 = 0.0;
  double coords2 = 0.0;


  for(std::vector<Triangle>::const_iterator t = triangles.begin(); t != triangles.end(); ++t) {
    Plane3D plane(points[(*t).getP1()], computeTriangleNormal((*t).getId()));

    if (!line.isParallel(plane)) {
      Coord3D intersection = plane.getIntersection(line);
      if (isInside(intersection, (*t).getId())) {
	VertexID idP = getNbPoints();
	double tempCoord = 0.0;

	double d = std::numeric_limits<double>::max();
	for(unsigned int i = 0; i < 3; ++i) {
	  double d1 = points[(*t).getVertexId(i)].distance(line);
	  if (d1 < d) {
	    idP = (*t).getVertexId(i);
	    d = d1;
	    tempCoord = line.getCoords(points[(*t).getVertexId(i)].projectionOnAxis(line));
	  }
	}

	if ((idFirst == nbPoints) || (tempCoord < coords1)) {
	  coords1 = tempCoord;
	  idFirst = idP;
	}
	if ((idLast == nbPoints) || (tempCoord > coords2)) {
	  coords2 = tempCoord;
	  idLast = idP;
	}
      }
    }
  }

  return std::pair<VertexID, VertexID> (idFirst, idLast);
}

bool Mesh::isInside(const Coord3D & coord) const {
  Line3D line(coord, Coord3D(1, 0, 0));
  VertexID nbPointsBefore = 0;
  VertexID nbPointsAfter = 0;

  for(std::vector<Triangle>::const_iterator t = triangles.begin(); t != triangles.end(); ++t) {
    Plane3D plane(points[(*t).getP1()], computeTriangleNormal((*t).getId()));

    if (!line.isParallel(plane)) {
      Coord3D intersection = plane.getIntersection(line);
      if (isInside(intersection, (*t).getId())) {
	if (line.getCoords(intersection) < 0)
	  ++nbPointsBefore;
	else
	  ++nbPointsAfter;
      }
    }
  }
  return nbPointsBefore % 2 == 1;
}


std::string Mesh::getInfos() {
  std::ostringstream str;

  str << "Mesh info: " << std::endl;
  str << " Number of points: " << getNbPoints() << std::endl;
  str << " Number of triangles: " << getNbTriangles() << std::endl;
  str << " Number of edges: " << getNbEdges() << std::endl;
  str << " Number of connected components: " << getNbCC() << std::endl;
  str << " Number of boundaries: " << getNbBoundaries();
  if (getNbBoundaries() != 0)
    str << " (" << getNbBoundaryPoints() << ")";
  str << std::endl;
  str << " Length of the boundaries: " << getBoundaryLength() << std::endl;

  return str.str();
}

std::string Mesh::getStringFromNLoop(const NLoop & nloop) const {
  std::ostringstream str;
  for(std::vector<std::vector<VertexID> >::const_iterator path = nloop.getPaths().begin(); path != nloop.getPaths().end(); ++path) {
    str << "| " << getStringFromPath(*path) << std::endl;
  }
  return str.str();
}


std::string Mesh::getStringFromPath(const std::vector<VertexID> & path) const {
  std::ostringstream str;

  if (path.size() == 0)
    str << "empty";
  else {
    for(std::vector<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
      str << *p;
      if (*p >= nbPoints)
	std::cout << "(out)";
      else if (points[*p].getIsBoundary())
	str << "(b)";
      if (path.size() != 1) {
	std::vector<VertexID>::const_iterator next = p + 1;
	if (next == path.end())
	  next = path.begin();
	if (points[*p].getIsBoundary() && points[*next].getIsBoundary() && isBoundaryEdge(*p,* next))
	  str << "=>";
	else if (points[*p].hasNeighbour(*next))
	  str << "->";
	else
	  str << " ";
      }
    }

  }
  return str.str();
}


std::string Mesh::getStringFromPath(const std::deque<VertexID> & path) const {
  std::ostringstream str;

  if (path.size() == 0)
    str << "empty";
  else {
    for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
      str << *p;
      if (*p >= nbPoints)
	std::cout << "(out)";
      else if (points[*p].getIsBoundary())
	str << "(b)";
      if (path.size() != 1) {
	std::deque<VertexID>::const_iterator next = p + 1;
	if (next == path.end())
	  next = path.begin();
	if (points[*p].getIsBoundary() && points[*next].getIsBoundary() && isBoundaryEdge(*p,* next))
	  str << "=>";
	else if (points[*p].hasNeighbour(*next))
	  str << "->";
	else
	  str << " ";
      }
    }

  }
  return str.str();
}

std::string Mesh::getStringFromPathWithFlags(const std::deque<VertexID> & path) const {
  std::ostringstream str;

  if (path.size() == 0)
    str << "empty";
  else {
    for(std::deque<VertexID>::const_iterator p = path.begin(); p != path.end(); ++p) {
      str << *p << ":" << points[*p].getFlag();
      if (path.size() != 1) {
	std::deque<VertexID>::const_iterator next = p + 1;
	if (next == path.end())
	  next = path.begin();
	if (points[*p].hasNeighbour(*next))
	  str << "->";
	else
	  str << " ";
      }
    }

  }
  return str.str();
}



void Mesh::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string & object) {
  if ((altFormat == "vrml") || (altFormat == "wrl"))
    loadVRML(fileName, object);
  else if (altFormat == "ply")
    loadPLY(fileName);
  else if (altFormat == "obj")
    loadOBJ(fileName, object);
  else if (altFormat == "xyz")
    loadXYZ(fileName);
  else if (altFormat == "off")
    loadOFF(fileName);
  else if (altFormat == "asc")
    loadASC(fileName, object);
  else if ((altFormat == "hdr") || (altFormat == "dcm") || (altFormat == "nii"))
    loadFromVolumicImage(fileName);
  else if (altFormat == "vtk")
    loadVTK(fileName);
  else
    throw Exception("Unknown format");

}

void Mesh::createFromVolumicImage(const CImg<> & img, double ratio) {
  const double value = (img.min() + img.max()) / 2;

  CImg<> points_l;
  CImgList<long unsigned int> faces;
  points_l = img.get_isosurface3d(faces, value);
  cimg_forX(points_l, p) {
    points_l(p, 2) *= ratio;
  }

  clear();

  /* triangulate mesh if needed */
  triangulateCImgMesh(faces);

  /* build mesh */
  buildMesh(points_l, faces);

}

void Mesh::loadFromVolumicImage(const std::string & fileName, double ratio) {
  const CImg<> img(fileName.c_str());
  createFromVolumicImage(img, ratio);
}

void Mesh::loadVRML(const std::string & fileName, const std::string & object) {
  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (!checkFormatVRML(infile))
    throw ExceptionUnknownFormat();
  if (object != "") {
    if (!findMeshVRML(infile, object))
      throw ExceptionDataNotFoundInFile();

    if (!loadMeshVRML(infile))
      throw ExceptionErrorDuringReadingFile();
  }
  else
    if (!loadNextMeshVRML(infile))
      throw ExceptionErrorDuringReadingFile();

  infile.close();

}

void Mesh::loadASC(const std::string & fileName, const std::string & object) {
  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (object != "")
    if (!findObjectASC(infile, object))
      throw ExceptionDataNotFoundInFile();

  if (!loadASC(infile))
    throw ExceptionErrorDuringReadingFile();

  infile.close();

  initIsBoundaryFlag();
}

void Mesh::loadPLY(const std::string & fileName) {
    PLYLoader plyLoader(fileName);
    plyLoader.loadMeshFromPLY(*this);
}

void Mesh::loadOFF(const std::string & fileName) {
  points.clear();
  triangles.clear();

  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (!checkFormatOFF(infile))
    throw ExceptionUnknownFormat();

  if (!loadOFF(infile))
    throw ExceptionErrorDuringReadingFile();

  infile.close();

  initIsBoundaryFlag();
}

void Mesh::loadVTK(const std::string & fileName) {
  points.clear();
  triangles.clear();

  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (!checkFormatVTK(infile))
    throw ExceptionUnknownFormat();

  if (!loadVTK(infile))
    throw ExceptionErrorDuringReadingFile();

  infile.close();

  initIsBoundaryFlag();
}


void Mesh::loadOBJ(const std::string & fileName, const std::string & objectName) {
  points.clear();
  triangles.clear();

  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (objectName != "")
    if (!findObjectOBJ(infile, objectName))
      throw ExceptionErrorDuringReadingFile();

  if (!loadOBJ(infile))
    throw ExceptionErrorDuringReadingFile();

  infile.close();

  initIsBoundaryFlag();
}

bool Mesh::loadPointsVRML(std::ifstream & infile) {
  double x, y, z;
  std::string buf, bufPoints;
  std::istringstream ins;
  nbPoints = 0;

  infile >> buf; if (buf != "point") return false;
  infile >> buf; if (buf != "[") return false;

  infile >> buf;
  while(buf != "]") {
    bufPoints+= " " + buf; // add x to main buffer
    infile >> buf;
    bufPoints+= " " + buf; // add y to main buffer
    infile >> buf;
    buf = buf.substr(0, buf.size() - 1); // remove ","
    bufPoints+= " " + buf; // add z to main buffer
    ++nbPoints;

    infile >> buf;
    if (infile.eof())
      return false;
  }
  ins.str(bufPoints);

  for(VertexID i = 0; i < nbPoints; ++i) {
    ins >> x >> y >> z;
    points.push_back(Point3D(x, y, z, i));
  }

  return true;
}

bool Mesh::loadTrianglesVRML(std::ifstream & infile) {
  std::string buf;
  infile >> buf; if (buf != "coordIndex") return false;
  infile >> buf; if (buf != "[") return false;

  infile >> buf;
  while(buf != "]") {
    std::string bufPoints = "";
    std::istringstream ins;
    int nbPt = 0;
    // load primitive
    while((buf != "-1,") && (buf != "-1")) {
      unsigned int comma = buf.find(",");
      ++nbPt;
      bufPoints+= " " + buf.substr(0, comma); // add id to main buffer
      buf = buf.substr(comma + 1); // remove ","
      if (buf == "")
	infile >> buf;
      if (infile.eof())
	return false;
    }
    ins.str(bufPoints);

    // only triangles or rectangles are available
    if((nbPt != 3) && (nbPt != 4))
      return false;

    // build primitive
    VertexID x, y, z;
    ins >> x >> y >> z;
    if (nbPt == 3)
      addTriangleFull(Triangle(x, y, z));
    else {
      VertexID t;
      ins >> t;
      addTriangleFull(Triangle(x, y, z));
      addTriangleFull(Triangle(z, t, x));
    }

    // next line
    infile >> buf;
  }

  return true;
}

bool Mesh::findMeshVRML(std::ifstream & infile, std::string objectName) {
  std::string buf;

  /* find definition of "objectName" */
  while(!infile.eof()) {
    infile >> buf;
    if (buf == "DEF") {
      infile >> buf;
      if (buf == objectName)
	return true; /* if found, return true */
    }
  }

  return false; /* if not found in all file, return false */
}

bool Mesh::loadMeshVRML(std::ifstream & infile) {
  std::string buf;
  points.clear();
  triangles.clear();
  nbPoints = 0;
  nbTriangles = 0;

  infile >> buf; if (buf != "Separator") return false;
  infile >> buf; if (buf != "{") return false;
  infile >> buf;
  while(buf != "}") {
    if(buf == "Coordinate3") {
      infile >> buf; // skip "{"
      if (!loadPointsVRML(infile))
	return false;
      do {
	infile >> buf; // skip "}"
      } while(buf != "}");
    }
    else if (buf == "IndexedFaceSet") {
      infile >> buf; // skip "{"
      if (!loadTrianglesVRML(infile))
	return false;
      do {
	infile >> buf; // skip "}"
      } while(buf != "}");
    }
    else if(buf == "USE")
      infile >> buf; // skip name
    else
      if(!skipBlockVRML(infile))
	return false;

    infile >> buf;
    if (infile.eof())
      return false;
  }

  initIsBoundaryFlag();

  return true;
}

bool Mesh::loadNextMeshVRML(std::ifstream & infile, bool skipFirst) {
  bool coords = false;
  bool faces = false;
  points.clear();
  triangles.clear();
  nbPoints = 0;
  nbTriangles = 0;

  // first find next mesh
  std::string buf;

  /* find definition of "objectName" */
  if (!skipFirst)
    while(!infile.eof()) {
      infile >> buf;
      if (buf == "DEF") {
	infile >> buf;
	break;
      }
      if (buf == "Separator") {
	break;
      }
      else if (buf == "}")
	return false;
    }
  else
    infile >> buf;

  // enter into next block
  while(buf != "{") {
    if (infile.eof())
      return false;
    infile >> buf;
  }

  infile >> buf;
  while(buf != "}") {
    if(buf == "Coordinate3") {
      infile >> buf; // skip "{"
      if (!loadPointsVRML(infile))
	return false;
      coords = true;
      do {
	infile >> buf; // skip "}"
      } while(buf != "}");
    }
    else if (buf == "IndexedFaceSet") {
      infile >> buf; // skip "{"
      if (!loadTrianglesVRML(infile))
	return false;
      faces = true;
      do {
	infile >> buf; // skip "}"
      } while(buf != "}");
    }
    else if(buf == "USE")
      infile >> buf; // skip name
    else {
      if ((buf == "Switch") || (buf == "DEF") || (buf == "Separator"))
	infile >> buf; // skip name or "{"
      else if(!skipBlockVRML(infile))
	return false;
    }

    infile >> buf;
    if (infile.eof())
      return false;
  }
  initIsBoundaryFlag();

  if (faces && coords) // if mesh found, all is ok
    return true;
  else if (!faces && !coords) // if no mesh found, find next DEF
    return loadNextMeshVRML(infile);
  else // only faces or coords has been found: wrong data format
    return false;

}



void Mesh::loadFirstMeshVRML(std::ifstream & infile) {
  std::string buf;
  bool coordsOk = false;
  bool facesOk = false;
  points.clear();
  triangles.clear();
  nbPoints = 0;
  nbTriangles = 0;

  infile >> buf;
  while((buf != "Coordinate3") && (buf != "IndexedFaceSet")) {
    infile >> buf;
    if (buf == "DEF") {
      infile >> buf;
      infile >> buf; // skip name
    }
    if (infile.eof()) {
      throw ExceptionErrorDuringReadingFile();
    }
  }

  while(buf != "}") {
    if(buf == "Coordinate3") {
      infile >> buf; // skip "{"
      if (!loadPointsVRML(infile)) {
	throw ExceptionErrorDuringReadingFile();
      }
      do {
	infile >> buf; // skip "}"
      } while(buf != "}");
      coordsOk = true;
    }
    else if (buf == "IndexedFaceSet") {
      infile >> buf; // skip "{"
      if (!loadTrianglesVRML(infile)) {
	throw ExceptionErrorDuringReadingFile();
      }
      do {
	infile >> buf; // skip "}"
      } while(buf != "}");
      facesOk = true;
    }
    else if(buf == "USE")
      infile >> buf; // skip name
    else {

      if(!skipBlockVRML(infile))
	throw ExceptionErrorDuringReadingFile();
    }

    infile >> buf;
    if (infile.eof()) {
      throw ExceptionErrorDuringReadingFile();
    }

    if (facesOk && coordsOk) { // faces and coords has been loaded, return
      initIsBoundaryFlag();
      return;
    }
  }


}

bool Mesh::findObjectASC(std::ifstream & file, const std::string & objectName) {
  std::string buf;

  while(!file.eof()) {
    file >> buf;
    if (buf == "Named") {
      file >> buf;
      if (buf == "object:") {
	file >> buf;
	if (buf == "\"" + objectName + "\"")
	  return true;
      }
    }
  }
  return false;
}

bool Mesh::loadASC(std::ifstream & file) {
  std::string buf;

  while(!file.eof()) {
    file >> buf;
    if (buf == "Vertex") {
      file >> buf;
      if (buf != "list:") {
	// a point
	double coords[3];
	int next = -1;
	getline(file, buf);
	std::deque<std::string> params = StringManipulation::splitString(StringManipulation::removeCR(buf), ' ');
	for(std::deque<std::string>::const_iterator p = params.begin(); p != params.end(); ++p)
	  if (*p != "") {
	    if (next != -1) {
	      if (!StringManipulation::isSignedFloat(*p))
		throw ExceptionErrorDuringReadingFile();
	      coords[next] = StringManipulation::getFloat(*p);
	      next = -1;
	    }
	    else {
	      if ((*p)[0] == 'X')
		next = 0;
	      else if ((*p)[0] == 'Y')
		next = 1;
	      else if ((*p)[0] == 'Z')
		next = 2;
	      else
		next = -1;
	      if ((next != -1) && ((*p).size() != 2)) {
		std::string coord = StringManipulation::splitString(*p, ':')[1];
		if (!StringManipulation::isSignedFloat(coord))
		  throw ExceptionErrorDuringReadingFile();
		coords[next] = StringManipulation::getFloat(coord);
		next = -1;
	      }
	    }
	}
	addPoint(Point3D(coords[0], coords[1], coords[2]));
      }
    }
    else if (buf == "Face") {
      file >> buf;
      if (buf != "list:") {
	// a face
	VertexID pts[3];
	bool next = false;

	getline(file, buf);
	std::deque<std::string> params = StringManipulation::splitString(StringManipulation::removeCR(buf), ' ');

	unsigned int i = 0;
	for(std::deque<std::string>::const_iterator p = params.begin(); p != params.end(); ++p)
	  if (*p != "") {
	    if (next) {
	      if (!StringManipulation::isUnsignedInt(*p))
		throw ExceptionErrorDuringReadingFile();
	      pts[i] = StringManipulation::getVertexID(*p);
	      next = false;
	      ++i;
	      if (i == 3)
		break;
	    }
	    else {
	      if ((*p).size() > 2) {
		std::string id = StringManipulation::splitString(*p, ':')[1];
		if (!StringManipulation::isUnsignedInt(id))
		  throw ExceptionErrorDuringReadingFile();
		pts[i] = StringManipulation::getVertexID(id);
		++i;
		if (i == 3)
		  break;
	      }
	      else
		next = true;
	    }
	  }
	addTriangle(Triangle(pts[0], pts[1], pts[2]));
      }
    }
    else {
      // skip line
      getline(file, buf);
    }

  }


  updateNeighboursAndTriangles();
  return true;
}


bool Mesh::findObjectOBJ(std::ifstream & file, const std::string & objectName) {
  std::string buf;

  while(!file.eof()) {
    file >> buf;
    if (buf == "g") {
      file >> buf;
      if (buf == objectName)
	return true;
    }
  }
  return false;
}

bool Mesh::loadOBJ(std::ifstream & file) {
  std::string buf;
  bool idTr = false;

  while(!file.eof()) {
    file >> buf;
    if (buf == "v") {
      // a point
      double x, y, z;
      file >> x >> y >> z;
      addPoint(Point3D(x, y, z));
    }
    else if (buf == "f") {
      // a face
      VertexID pts[4];
      getline(file, buf);
      std::deque<std::string> params = StringManipulation::splitString(buf, ' ');

      unsigned int i = 0;
      for(std::deque<std::string>::const_iterator p = params.begin(); p != params.end(); ++p)
	if (*p != "") {
	  if (i == 4)
	    throw ExceptionErrorDuringReadingFile();
	  std::string id = StringManipulation::splitString(*p, '/')[0];
	  if (!StringManipulation::isUnsignedInt(id))
	    throw ExceptionErrorDuringReadingFile();
	  pts[i] = StringManipulation::getVertexID(id);
	  ++i;
      }
      addTriangle(Triangle(pts[0], pts[1], pts[2]));
      if (i == 4)
	addTriangle(Triangle(pts[0], pts[2], pts[3]));
    }
    else {
      // skip line
      getline(file, buf);
    }

  }

  for(std::vector<Triangle>::const_iterator t = triangles.begin(); t != triangles.end(); ++t)
    if (((*t).getP1() >= nbPoints) || ((*t).getP2() >= nbPoints) || ((*t).getP3() >= nbPoints)) {
      idTr = true;
      break;
    }
  if (idTr)
    for(std::vector<Triangle>::iterator t = triangles.begin(); t != triangles.end(); ++t) {
      (*t).setP1((*t).getP1() - 1);
      (*t).setP2((*t).getP2() - 1);
      (*t).setP3((*t).getP3() - 1);
    }

  updateNeighboursAndTriangles();
  return true;
}


bool Mesh::checkFormatVTK(std::ifstream & file) {
  std::string buf;
  while(!file.eof()) {
    getline(file, buf);
    if ((buf.size() != 0) && (StringManipulation::strip(buf)[0] != '\n') && (buf[0] != '#'))
      return (StringManipulation::removeCR(buf) == "vtk output");
  }

  return false;
}

bool Mesh::loadVTK(std::ifstream & file) {
  std::string firstLine;
  std::string buf;
  VertexID nbPt = 0;
  TriangleID nbPrimitives = 0;

  // first read the header for points
  while(!file.eof()) {
    if (!getline(file, buf))
      return false;
    if ((buf.size() != 0) && (StringManipulation::strip(buf)[0] != '\n') && (buf[0] != '#')) {
      std::deque<std::string> values = StringManipulation::splitString(StringManipulation::removeCR(buf), ' ');
      if ((values.front() == "POINTS") && (StringManipulation::isUnsignedInt(values[1])) &&
	  (values[2] == "float")) {
	nbPt = StringManipulation::getInt(values[1]);
	break;
      }
    }
  }

  // then read points
  for(VertexID i = 0; i < nbPt; ++i) {
    double x, y , z;
    file >> x >> y >> z;
    points.push_back(Point3D(x, y, z, nbPoints++));
  }

  // read the header for polygons
  while(!file.eof()) {
    if (!getline(file, buf))
      return false;
    if ((buf.size() != 0) && (StringManipulation::strip(buf)[0] != '\n') && (buf[0] != '#')) {
      std::deque<std::string> values = StringManipulation::splitString(StringManipulation::removeCR(buf), ' ');
      if ((values.front() == "POLYGONS") && (StringManipulation::isUnsignedInt(values[1])) &&
	  (StringManipulation::isUnsignedInt(values[2]))) {
	nbPrimitives = StringManipulation::getInt(values[1]);
	break;
      }
    }
  }

  // read polygons
  for(TriangleID i = 0; i < nbPrimitives; ++i) {
    VertexID nbP;
    file >> nbP;

    if((nbP != 3) && (nbP != 4))
      return false;

    VertexID x, y, z;
    file >> x >> y >> z;
    if (nbP == 3) {
      addTriangle(Triangle(x, y, z));
    }
    else {
      unsigned int t;
      file >> t;

      addTriangle(Triangle(x, y, z));
      addTriangle(Triangle(x, z, t));

    }
  }

  updateNeighboursAndTriangles();
  return true;
}

bool Mesh::checkFormatOFF(std::ifstream & file) {
  std::string firstLine;
  getline(file, firstLine);
  return ((firstLine == "OFF") || (firstLine == "OFF\r"));
}


bool Mesh::loadOFF(std::ifstream & file) {
  std::string buf;
  VertexID nbPt = 0;
  TriangleID nbPrimitives = 0;


  /* find definition of vertices in the header */
  while(!file.eof()) {
    getline(file, buf);
    if ((buf.size() != 0) && (StringManipulation::strip(buf)[0] != '\n') && (buf[0] != '#')) {
      std::deque<std::string> values = StringManipulation::splitString(StringManipulation::removeCR(buf), ' ');
      if (values.size() != 3)
	return false;
      if (StringManipulation::isUnsignedInt(values[0]) &&
	  StringManipulation::isUnsignedInt(values[1]) &&
	  StringManipulation::isUnsignedInt(values[2])) {
	nbPt = StringManipulation::getInt(values[0]);
	nbPrimitives = StringManipulation::getInt(values[1]);
      }
      else {
	return false;
      }
      break;
    }
  }

  // then read points
  for(VertexID i = 0; i < nbPt; ++i) {
    double x, y , z;
    file >> x >> y >> z;
    points.push_back(Point3D(x, y, z, nbPoints++));
  }

  // and read faces
  bool start1 = false;
  for(TriangleID i = 0; i < nbPrimitives; ++i) {
    VertexID nbP;
    file >> nbP;

    if((nbP != 3) && (nbP != 4))
      return false;

    VertexID x, y, z;
    file >> x >> y >> z;
    if (nbP == 3) {
      if ((x == nbPoints) ||
	  (y == nbPoints) ||
	  (z == nbPoints))
	start1 = true;

      addTriangle(Triangle(x, y, z));
    }
    else {
      unsigned int t;
      file >> t;
      if ((x == nbPoints) ||
	  (y == nbPoints) ||
	  (z == nbPoints) ||
	  (t == nbPoints))
	start1 = true;

      addTriangle(Triangle(x, y, z));
      addTriangle(Triangle(x, z, t));

    }
  }

  if (start1) {
    for(std::vector<Triangle>::iterator t = triangles.begin(); t != triangles.end(); ++t)
      (*t).incIds(-1, 0);
  }

  updateNeighboursAndTriangles();


  return true;
}


void Mesh::loadXYZ(const std::string & fileName) {
  Point3D point_l;
  Triangle triangle_l;

  points.clear();
  triangles.clear();
  nbTriangles = 0;
  nbPoints = 0;

  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  // first load points
  while(!infile.eof()) {
    double x, y, z;
    infile >> x;
    if (infile.eof())
      break; // last point
    infile >> y;
    if (infile.eof())
      throw ExceptionErrorDuringReadingFile();
    infile >> z;
    if (infile.eof())
      throw ExceptionErrorDuringReadingFile();
    point_l.setX(x);
    point_l.setY(y);
    point_l.setZ(z);
    point_l.setId(nbPoints);
    points.push_back(point_l);
    ++nbPoints;
  }
  infile.close();

  // then build triangles
  SpaceSpliter sspliter(points);
  VertexID firstPoint = sspliter.findFirstX();
  VertexID currentPoint;


  std::cout << " Building triangles [";
  std::cout.flush();
  VertexID ii = 0;
  while(firstPoint < points.size()) {
    VertexID nextPoint = points.size();
    std::cout << ".";
    std::cout.flush();
    currentPoint = sspliter.findFirstSameX(firstPoint);

    VertexID i = 0;
    while(true) {
      ++i;
      assert(currentPoint < points.size());
      VertexID cPointX = sspliter.findNextSameX(currentPoint);
      VertexID cPointY = sspliter.findNextSameY(currentPoint);
      if (cPointY < points.size())
	nextPoint = cPointY;

      if (cPointX < points.size()) {
	if (cPointY < points.size()) {
	  triangle_l.setP1(currentPoint);
	  triangle_l.setP2(cPointX);
	  triangle_l.setP3(cPointY);
	  addTriangleFull(triangle_l);
	  VertexID cPointXY = sspliter.findNextSameX(cPointY);
	  if (cPointXY < points.size()) {
	    triangle_l.setP1(cPointX);
	    triangle_l.setP2(cPointXY);
	    triangle_l.setP3(cPointY);
	    addTriangleFull(triangle_l);
	  }
	}
	currentPoint = cPointX;
      }
      else {
	currentPoint = nextPoint;
	break;
      }
    }

    ii += i;
    firstPoint = currentPoint;
  }
  std::cout << "]" << std::endl;

  initIsBoundaryFlag();
}


void Mesh::saveAltFormat(const std::string & fileName, const std::string & format, const std::string & objectName) const {
  if ((format == "vrml") || (format == "wrl"))
    saveVRML(fileName, objectName);
  else if (format == "obj")
    saveOBJ(fileName, objectName);
  else if (format == "off")
    saveOFF(fileName, objectName);
  else if (format == "ply")
    savePLY(fileName, objectName);
  else if (format == "vtk")
    saveVTK(fileName, objectName);
  else
    throw Exception("Unknown format");
}

void Mesh::savePLY(const std::string & fileName, const std::string &) const {
    PLYSaver plysaver;

    plysaver.savePLY(*this, fileName);
}

void Mesh::saveOFF(const std::string & fileName, const std::string & objectName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "OFF" << std::endl;
  outfile << "#" << std::endl;
  outfile << "#   Generated by saveOFF" << std::endl;
  if (objectName != "")
    outfile << "#    " << objectName << std::endl << std::endl;;
  outfile << "#" << std::endl;

  outfile << nbPoints << " " << nbTriangles << " " << getNbEdges() << std::endl;

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    outfile << " " << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;

  for(Mesh::const_triangle_iterator t = triangle_begin(); t != triangle_end(); ++t)
    outfile << " 3 " << (*t).getP1() << " " << (*t).getP2() << " " << (*t).getP3() << std::endl;

  outfile.close();
}

void Mesh::saveOBJ(const std::string & fileName, const std::string & objectName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "# Generated by saveOBJ" << std::endl << std::endl;
  if (objectName != "")
    outfile << "# " << objectName << std::endl << std::endl;;

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    outfile << "v " << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;

  outfile << std::endl;

  for(Mesh::const_triangle_iterator t = triangle_begin(); t != triangle_end(); ++t)
    outfile << "f " << ((*t).getP1() + 1) << " " << ((*t).getP2() + 1) << " " << ((*t).getP3() + 1) << std::endl;

  outfile.close();
}

void Mesh::saveVTK(const std::string & fileName, const std::string & objectName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "# vtk DataFile Version 3.0" << std::endl;
  if (objectName != "")
    outfile << "# " << objectName << std::endl << std::endl;;


  outfile << "vtk output" << std::endl << "ASCII" << std::endl << "DATASET POLYDATA" << std::endl;
  outfile << "POINTS " << getNbPoints() << " float" << std::endl;

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;

  outfile << "POLYGONS " << getNbTriangles() << " " << (getNbTriangles() * 4) << std::endl;

  for(Mesh::const_triangle_iterator t = triangle_begin(); t != triangle_end(); ++t)
    outfile << "3 " << (*t).getP1() << " " << (*t).getP2() << " " << (*t).getP3() << std::endl;

  outfile.close();
}


void Mesh::saveVRML(const std::string & fileName, const std::string & objectName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  /* head */
  outfile << getHeaderVRML();
  outfile << toStringVRML(objectName);

  /* recenter object */
  outfile << getMiddleVRML();
  outfile << getTransformationsVRML(objectName);
  outfile << getFooterVRML();

  outfile.close();
}

std::string Mesh::toStringVRML(const std::string & objectName, unsigned int id) const {
  std::ostringstream result;

  if ((points.size() == 0) || (triangles.size() == 0))
    throw ExceptionEmptyData();

  result.precision(15);

  result << "\tDEF " << objectName << "_" << id << std::endl;
  result << "\tSeparator {" << std::endl << "\t\tCoordinate3 {" << std::endl << "\t\t\tpoint [" << std::endl;
  for(Mesh::const_point_iterator i = points.begin(); i != points.end(); ++i)
    result << "\t\t\t\t" << (*i).getX() << " " << (*i).getY() << " " << (*i).getZ() << "," << std::endl;

  result << "\t\t\t]" << std::endl << "\t\t}" << std::endl << "\t\tIndexedFaceSet {" << std::endl << "\t\t\tcoordIndex [" << std::endl;

  for(Mesh::const_triangle_iterator i = triangles.begin(); i != triangles.end(); ++i)
    result << "\t\t\t\t" << (*i).getP1() << ", " << (*i).getP2() << ", " << (*i).getP3() << ", -1," << std::endl;

  result << "\t\t\t]" << std::endl << "\t\t}" << std::endl << "\t}" << std::endl;

  return result.str();
}



bool Mesh::inSameBoundary(VertexID idP1, VertexID idP2) const {
  assert((idP1 < getNbPoints()) && (idP2 < getNbPoints()));
  if ((!points[idP1].getIsBoundary()) || (!points[idP2].getIsBoundary()))
    return false;
  if ((idP1 == idP2) || isBoundaryEdge(idP1, idP2))
    return true;

  VertexID pred = idP1;
  VertexID p = getNextBPoint(idP1);

  while((p != idP2) && (p != idP1)) {
    VertexID tmp = p;
    p = getNextBPoint(p, pred);
    pred = tmp;
  }

  if (p == idP1)
    return false;
  else
    return true;
}

std::deque<VertexID> Mesh::findExtrema(double epsilon) {
  std::deque<VertexID> result;
  std::deque<VertexID> extrema1;
  std::deque<VertexID> extrema2;

  // initialization: computation of the first two extrema
  VertexID p1 = 0;
  VertexID p2 = 1;
  double distance = point(p1).distance(point(p2));
  std::cout << " Looking for start points..." << std::endl;

  for(VertexID i = 0; i < getNbPoints() / 2 + 1; ++i) // uint better than iterator
    for(Mesh::const_point_iterator j = point_begin(); j != point_end(); ++j) {
      double d = point(i).distance(*j);
      if (d > distance) {
	p1 = i;
	p2 = (*j).getId();
	distance = d;
      }
    }
  std::cout << " Computing distances from the two selected points ";
  // then compute Dijkstra from each of the two selected points
  MeshManipulator mManip(*this);

  mManip.computeDijkstra(p1);
  // then find extrema
  for(VertexID i = 0; i < nbPoints; ++i)
    if (mManip.isExtremaPoint(i))
      extrema1.push_back(i);
  mManip.computeDijkstra(p2);
  for(VertexID i = 0; i < nbPoints; ++i)
    if (mManip.isExtremaPoint(i))
      extrema2.push_back(i);
  distance = mManip.getLength(p1);

  std::cout << "(" << extrema1.size() << ", " << extrema2.size() << ", distance: " << distance << ")" << std::endl;

  std::cout << " Computing extrema..." << std::endl;
  setPointFlag(0);
  for(std::deque<VertexID>::const_iterator e = extrema1.begin(); e != extrema1.end(); ++e)
    point(*e).setFlag(1);

  for(std::deque<VertexID>::const_iterator e = extrema2.begin(); e != extrema2.end(); ++e)
    point(*e).incFlag(2);

  for(std::deque<VertexID>::const_iterator e = extrema2.begin(); e != extrema2.end(); ++e) {
      mManip.computeDijkstraBound(*e, LengthEdgeEuclidean(*this), 2 * epsilon * distance);
      bool found = false;
      VertexID extremum = getNbPoints();
      for(std::deque<VertexID>::const_iterator e1 = extrema1.begin(); e1 != extrema1.end(); ++e1)
	if ((mManip.getLength(*e1) >= 0.0) && (mManip.getLength(*e1) < 2 * epsilon)) {
	  point(*e1).setFlag(0); // in the neighbourhood, removing it of the potential extrema
	  if (!found) {
	    found = true;
	    extremum = *e1;
	    while(mManip.getLength(extremum) > epsilon) {
	      extremum = mManip.getPredPoint(extremum);
	    }
	  }
	}
      if (extremum != getNbPoints()) {
	bool exNb = false;

	mManip.computeDijkstraBound(extremum, LengthEdgeEuclidean(*this), epsilon * distance);
	for(std::deque<VertexID>::const_iterator ex = result.begin(); ex != result.end(); ++ex)
	  if ((mManip.getLength(*ex) >= 0) && (mManip.getLength(*ex) < epsilon)) {
	    exNb = true;
	    break;
	  }
	if (!exNb) {
	  result.push_back(extremum);
	}
      }
    }

  return result;
}



void Mesh::forEachEdge(EdgeManipulator & em) const {
  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
      if ((*p).getId() < *nb)
	em((*p).getId(), *nb, *this);
}

VertexID Mesh::getNbBadBPoints() const {
  VertexID result = 0;

  for(const_point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getIsBoundary() && ((*p).getNeighbours().size() != 0)) {
      unsigned int nbBNb = 0;
      for(std::deque<VertexID>::const_iterator i = (*p).getNeighbours().begin(); i != (*p).getNeighbours().end(); ++i)
	if (isBoundaryEdge((*p).getId(), *i))
	  ++nbBNb;
      if (nbBNb != 2)
	++result;
    }

  return result;
}

double Mesh::getMeanEdges(VertexID pId) const {
  double result = 0.0;
  unsigned int nbNb = 0;
  const Point3D & p = (*this).point(pId);

  for(std::deque<VertexID>::const_iterator n = p.getNeighbours().begin();
      n != p.getNeighbours().end(); ++n) {
    result += p.distance((*this).point(*n));
    ++nbNb;
  }

  if (nbNb != 0)
    result /= nbNb;

  return result;
}


double Mesh::getLengthSmallestEdge() const {
  double length = point(0).distance(point(point(0).getNeighbours().front()));
  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    for(std::deque<VertexID>::const_iterator nb = (*p).getNeighbours().begin(); nb != (*p).getNeighbours().end(); ++nb)
      if ((*p).getId() < *nb) {
	double l = (*p).distance(point(*nb));
	if (l < length)
	  length = l;
      }
  return length;
}

double Mesh::estimateMaxGeodesicDistance() {
  VertexID p1 = 0;
  VertexID p2 = 1;
  double distance = point(p1).distance(point(p2));

  for(VertexID i = 0; i < getNbPoints() / 2 + 1; ++i) // uint better than iterator
    for(Mesh::const_point_iterator j = point_begin(); j != point_end(); ++j) {
      double d = point(i).distance(*j);
      if (d > distance) {
	p1 = i;
	p2 = (*j).getId();
	distance = d;
      }
    }

  MeshManipulator mManip(*this);

  return mManip.getGeodesicDistance(p1, p2);
}


double Mesh::getArea() const {
  double result = 0.0;
  for(Mesh::const_triangle_iterator t = triangle_begin(); t != triangle_end(); ++t)
    result += Coord3D::computeTriangleArea(point((*t).getP1()), point((*t).getP2()), point((*t).getP3()));

  return result;
}

double Mesh::getArea(TriangleID tid) const {
  assert(tid < nbTriangles);
  const Triangle & t = triangle(tid);
  return Coord3D::computeTriangleArea(point(t.getP1()), point(t.getP2()), point(t.getP3()));
}


Mesh Mesh::createSquare(unsigned int size) {
  Mesh result;

  std::vector<Coord3D> pts;
  std::vector<Triangle> trs;

  for(unsigned int x = 0; x < size; ++x)
    for(unsigned int y = 0; y < size; ++y) {
      VertexID idP = x * size + y;
      pts.push_back(Coord3D((double)x / size, (double)y / size, 0.0));
      if ((x < size - 1) && (y < size - 1)) {
	trs.push_back(Triangle(idP, idP + 1, idP + size + 1));
	trs.push_back(Triangle(idP, idP + size + 1, idP + size));
      }
    }

  result.buildMesh(pts, trs);
  return result;
}

Mesh Mesh::createSphere(double radius, unsigned int resolution, const Coord3D & center) {

  CImgList<VertexID> primitives;
  CImg<double> pts = CImg<double>::sphere3d(primitives, radius, resolution);
  Mesh m(pts, primitives);
  m += center;
  return m;
}

const Triangle & Mesh::findTriangle(const PointOnEdge & p1, const PointOnEdge & p2) const {
  VertexID v1, v2, v3;
  if (p1.isVertex()) {
    if (p1.isVertex() && p2.isVertex()) {
      return findTriangle(p1.getClosestVertex(), p2.getClosestVertex());
    }
    else {
      v1 = p1.getClosestVertex();
      v2 = p2.getFirstVertex();
      v3 = p2.getSecondVertex();
    }
  }
  else if (p2.isVertex())  {
    v1 = p1.getFirstVertex();
    v2 = p1.getSecondVertex();
    v3 = p2.getClosestVertex();
  }
  else {
    v1 = p1.getFirstVertex();
    v2 = p1.getSecondVertex();
    v3 = p2.getFirstVertex();
    if ((v1 == v3) || (v2 == v3))
      v3 = p2.getSecondVertex();
    else
      if ((p2.getSecondVertex() != v1) && (p2.getSecondVertex() != v2))
	throw Exception("findTriangle(2): the given points-on-edge are not on edges connected by a triangle");
  }
  assert(v1 < nbPoints);
  assert(v2 < nbPoints);
  assert(v3 < nbPoints);
  return findTriangle(v1, v2, v3);
}


void Mesh::setTriangleAndPointFlag(const PLPath & path, int flag) {
  PLPath::const_iterator pred = path.begin();
  if ((*pred).isVertex())
    point((*pred).getClosestVertex()).setFlag(flag);
  for(PLPath::const_iterator p = path.begin() + 1; p != path.end(); ++p, ++pred) {
    if ((*p).isVertex())
      point((*p).getClosestVertex()).setFlag(flag);
    Triangle & t = triangles[findTriangle(*p, *pred).getId()];
    t.setFlag(flag);
  }

}


bool Mesh::intersects(const PointOnEdge & u1, const PointOnEdge & u2,
		      const PointOnEdge & v1, const PointOnEdge & v2) const {

  if ((u1 == v1) || (u2 == v1) || (u1 == v2) || (u2 == v2)) {
    return true;
  }

  if (v1.inSameEdge(v2)) {
    std::pair<VertexID, VertexID> e = PointOnEdge::getEdge(v1, v2);
    if (u2.inEdge(e)) {
      if (u1.inEdge(e)) {
	const double pU2 = u2.getLocationOnEdge(e);
	const double pU1 = u1.getLocationOnEdge(e);
	const double pV1 = v1.getLocationOnEdge(e);
	const double pV2 = v2.getLocationOnEdge(e);
	if ((((pV1 >= pU2) && (pU2 >= pV2)) || ((pV1 <= pU2) && (pU2 <= pV2))) || // u2 between v1 and v2
	    (((pV1 >= pU1) && (pU1 >= pV2)) || ((pV1 <= pU1) && (pU1 <= pV2))) || // u1 between v1 and v2
	    (((pU1 >= pV1) && (pV1 >= pU2)) || ((pU1 <= pV1) && (pV1 <= pU2))) || // v1 between u2 and u1
	    (((pU1 >= pV2) && (pV2 >= pU2)) || ((pU1 <= pV2) && (pV2 <= pU2)))) // v2 between u2 and u1
	  return true;
      }
      else {
	const double pU2 = u2.getLocationOnEdge(e);
	const double pV1 = v1.getLocationOnEdge(e);
	const double pV2 = v2.getLocationOnEdge(e);
	if (((pV1 >= pU2) && (pU2 >= pV2)) || ((pV1 <= pU2) && (pU2 <= pV2)))
	  return true;
      }
    }
    else {
      if (u1.inEdge(e)) {
	const double pU1 = u1.getLocationOnEdge(e);
	const double pV1 = v1.getLocationOnEdge(e);
	const double pV2 = v2.getLocationOnEdge(e);
	if (((pV1 >= pU1) && (pU1 >= pV2)) || ((pV1 <= pU1) && (pU1 <= pV2)))
	  return true;
      }
    }
  }
  else {

    // (v1, v2) is not part of an edge: it goes throw the inner part of a triangle

    const Triangle & t = findTriangle(v1, v2);
    if ((!t.hasPoint(u2.getFirstVertex())) || (!t.hasPoint(u2.getSecondVertex()))) { // u2 is on an edge outside of the triangle
      if (!((u2.isVertex()) && (t.hasPoint(u2.getClosestVertex())))) // and u2 is not on a vertex of the triangle
	return false; // we are outside
    }


    if ((!t.hasPoint(u1.getFirstVertex())) || (!t.hasPoint(u1.getSecondVertex()))) { // u1 is on an edge outside of the triangle
      if (!((u1.isVertex()) && (t.hasPoint(u1.getClosestVertex())))) // and u1 is not on a vertex of the triangle
	return false; // we are outside
    }

    // now, we are inside t

    // if (u1, u2) is on an edge
    if (u2.inSameEdge(u1)) {

      std::pair<VertexID, VertexID> e = PointOnEdge::getEdge(u1, u2);
      if (v1.inEdge(e)) {
	if (v1.isVertex()) {
	  if ((v1.getClosestVertex() == e.first) || (v1.getClosestVertex() == e.second))
	    return true;
	}
	else if (u2.inEdge(e) && u1.inEdge(e)) {
	  // v1 is not a vertex
	  const double pV1 = v1.getLocationOnEdge(e);
	  const double pU2 = u2.getLocationOnEdge(e);
	  const double pU1 = u1.getLocationOnEdge(e);
	  if (((pU2 >= pV1) && (pV1 >= pU1)) || ((pU2 <= pV1) && (pV1 <= pU1)))
	    return true;
	}
      }
      else if (v2.inEdge(e)) {
	if (v2.isVertex()) {
	  if ((v2.getClosestVertex() == e.first) || (v2.getClosestVertex() == e.second))
	    return true;
	}
	else if (u2.inEdge(e) && u1.inEdge(e)) {
	  // v2 is not a vertex
	  const double pV2 = v2.getLocationOnEdge(e);
	  const double pU2 = u2.getLocationOnEdge(e);
	  const double pU1 = u1.getLocationOnEdge(e);
	  if (((pU2 >= pV2) && (pV2 >= pU1)) || ((pU2 <= pV2) && (pV2 <= pU1)))
	    return true;
	}
      }
    }
    else {
      // the crossing may occured in the inner part of the triangle
      // get the location of the points along the boundary of the triangle
      double pv1 = v1.getLocationOnTriangle(t);
      double pv2 = v2.getLocationOnTriangle(t);
      double pU2 = u2.getLocationOnTriangle(t);
      double pU1 = u1.getLocationOnTriangle(t);

      if ((pv1 == pU1) || (pv1 == pU2) ||
	  (pv2 == pU1) || (pv2 == pU2))
	return true;
      unsigned int nbBetweenV1andV2 = 0;
      if (pv1 < pv2) {
	if ((pv1 <= pU2) && (pU2 <= pv2)) ++nbBetweenV1andV2;
	if ((pv1 <= pU1) && (pU1 <= pv2)) ++nbBetweenV1andV2;
      }
      else {
	if ((pv2 <= pU2) && (pU2 <= pv1)) ++nbBetweenV1andV2;
	if ((pv2 <= pU1) && (pU1 <= pv1)) ++nbBetweenV1andV2;
      }
      if (nbBetweenV1andV2 == 1) {
	return true;
      }
    }
  }
  return false;

}

std::pair<VertexID, VertexID> Mesh::getClosestVertices(const std::vector<VertexID> & path, double value1, double value2) const {
  std::pair<VertexID, VertexID> result;
  result.first = getClosestVertex(path, value1);
  result.second = getClosestVertex(path, value2);
  if (result.first == result.second) {
    std::cout << getStringFromPath(path) << ", avec " << result.first << " et " << result.second << std::endl;
    // TODO
    throw Exception("getClosestVertices(3): not yet implemented");
  }

  assert((path.size() < 2) || (result.first != result.second));
  return result;
}


PointOnEdge Mesh::getPointOnPath(const std::vector<VertexID> & path, double value) const {
  double l = 0.;
  std::vector<VertexID>::const_iterator pred = path.begin();
  if (value == 0.)
    return point(path.front());

  for(std::vector<VertexID>::const_iterator p = path.begin() + 1; p != path.end(); pred = p, ++p) {
    l += point((*pred)).distance(point(*p));
    if (l == value)
      return point(*p);
    else if (l > value) {
      const double length = point(*pred).distance(point(*p));
      const double partial = length - (l - value);
      return PointOnEdge(point((*pred)), point((*p)), partial / length);
    }
  }

  throw Exception("getPointOnPath(2): value out of range");
}

VertexID Mesh::getClosestVertex(const std::vector<VertexID> & path, double value) const {
  double l = 0.;
  double lastl = 0.;
  std::vector<VertexID>::const_iterator pred = path.begin();
  if (value == 0.)
    return path.front();

  for(std::vector<VertexID>::const_iterator p = path.begin() + 1; p != path.end(); pred = p, ++p) {
    lastl = l;
    l += point((*pred)).distance(point(*p));
    if (l == value)
      return *p;
    else if (l > value) {
      const double middle = (lastl + l) / 2.;
      if (value > middle)
	return *p;
      else
	return *pred;
    }
  }

  throw Exception("getClosestVertex(2): value out of range");

}

VertexID Mesh::getClosestVertex(const PLPath & path, double value) const {
  double l = 0.;
  PLPath::const_iterator pred = path.begin();
  if (value == 0.)
    return path.front().getClosestVertex();

  for(PLPath::const_iterator p = path.begin() + 1; p != path.end(); pred = p, ++p) {
    l += (*pred).distance(*p);
    if (l == value)
      return (*p).getClosestVertex();
    else if (l > value) {
      Coord3D pV = (*pred) + ((*p) - (*pred)) * (l - value);
      double d = pV.distance(point((*p).getFirstVertex()));
      VertexID s = (*p).getFirstVertex();
      double d2 = pV.distance(point((*p).getSecondVertex()));
      if (d2 < d) {
	s = (*p).getSecondVertex();
	d = d2;
      }
      d2 = pV.distance(point((*pred).getFirstVertex()));
      if (d2 < d) {
	s = (*pred).getFirstVertex();
	d = d2;
      }
      d2 = pV.distance(point((*pred).getSecondVertex()));
      if (d2 < d) {
	s = (*pred).getSecondVertex();
	d = d2;
      }
      return s;
    }
  }

  throw Exception("getClosestVertex(2): value out of range");
}


bool Mesh::isSaddlePoint(VertexID id, double epsilon) const {
  const Point3D & p = points[id];
  if (p.getIsBoundary())
    return false;

  double sum = 0.;
  std::deque<VertexID>::const_iterator pred = p.getNeighbours().begin() + (p.getNbNeighbours() - 1);
  for(std::deque<VertexID>::const_iterator i = p.getNeighbours().begin();
      i != p.getNeighbours().end(); pred = i, ++i)
    sum += p.angle(points[*pred], points[*i]);

    return sum > 2 * M_PI - epsilon;
}

bool Mesh::hasTriangleDifferentFlag(VertexID v, int flag) const {
  const Point3D & p = points[v];

  for(std::deque<TriangleID>::const_iterator t = p.getTriangles().begin(); t != p.getTriangles().end(); ++t)
    if (triangles[*t].getFlag() != flag)
      return true;

  return false;
}



std::vector<VertexID> Mesh::computeMultiPoints() const {
  std::vector<VertexID> result;
  std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord> matching = computeMultiPointsByCoords();

  for(std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord>::const_iterator l = matching.begin(); l != matching.end(); ++l)
    result.insert(result.end(), (*l).second.begin(), (*l).second.end());

  return result;
}

std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord> Mesh::computeMultiPointsByCoords() const {
  std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord> result;
  std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord> matching;

  /* compute similarities */
  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p)
    matching[*p].push_back((*p).getId());

  /* get multipoints */
  for(std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord>::const_iterator l = matching.begin(); l != matching.end(); ++l)
    if ((*l).second.size() > 2) {
      result[(*l).first] = (*l).second;
    }
    else if ((*l).second.size() == 2) {
      // Specific configuration
      // TODO: implement it
    }

  return result;
}



std::vector<PLPath> Mesh::getIntersection(const Plane3D & plane,
					  unsigned int nbSlices,
					  double space) {
  assert(getBox().intersect(plane));
  MeshMap mMap(*this, plane);

  if (nbSlices == 0)
    return mMap.computeLevelSet(0.);
  else {
    std::vector<PLPath> result;
    double s = 0.;
    for(unsigned int i = 0; i <= nbSlices; ++i, s += space) {
      std::vector<PLPath> tmp = mMap.computeLevelSet(s);
      for(std::vector<PLPath>::iterator p = tmp.begin(); p != tmp.end(); ++p)
          p->mergeSimilarPoints(1e-8, true);
      if (tmp.size() != 0) {
	assert(getBox().contains(tmp.front().front()));
	assert((plane.distance(tmp.front().front()) - s) < .00001);
	assert((plane.distance(tmp.back().front()) - s) < .00001);
	result.insert(result.end(), tmp.begin(), tmp.end());
      }
    }

    return result;
  }
}

std::vector<PLPath> Mesh::getIntersectionByNumber(const Coord3D & direction,
						  unsigned int nbSlices) {
  const std::pair<double, double> e = getExtrema(direction);
  const double min = e.first;
  const double max = e.second;
  Plane3D plane(getIsobarycenter(), direction);

  double s = (max - min) / (nbSlices + 1);
  return getIntersection(plane.translate(min + .5 * s),
			 nbSlices, s);
}


std::vector<PLPath> Mesh::getIntersectionBySpace(const Coord3D & direction,
						 double space) {
  const std::pair<double, double> e = getExtrema(direction);
  const double min = e.first;
  const double max = e.second;
  Plane3D plane(getIsobarycenter(), direction);

  unsigned int nbSlices = ceil((max - min) / space) - 1;
  assert(nbSlices * space <= max - min);
  double start = min + ((max - min) - nbSlices * space) / 2;
  return getIntersection(plane.getTranslate(start), nbSlices, space);
}


std::pair<double, double> Mesh::getExtrema(const Coord3D & direction) const {

  Plane3D plane(getIsobarycenter(), direction);
  double min = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p) {
    const double d = plane.signedDistance(*p);
    if (d < min) min = d;
    if (d > max) max = d;
  }

  return std::pair<double, double>(min, max);
}


double Mesh::conformalSimilarity(const Mesh & mesh) const {
  assert(getNbPoints() == mesh.getNbPoints());
  assert(getNbTriangles() == mesh.getNbTriangles());
  double result = 0.;

  Mesh::const_point_iterator pm = mesh.point_begin();
  for(Mesh::const_point_iterator p = point_begin(); p != point_end(); ++p, ++pm)
    if (!(*p).getIsBoundary()) {
      std::vector<double> am, a;
      double s = 0.;
      double sm = 0.;
      assert((*pm).getNbNeighbours() == (*p).getNbNeighbours());
      // compute the angles
      std::deque<VertexID>::const_iterator nm = (*pm).getNeighbours().begin();
      std::deque<VertexID>::const_iterator predn = (*p).getNeighbours().begin() + ((*p).getNbNeighbours() - 1);
      for(std::deque<VertexID>::const_iterator n = (*p).getNeighbours().begin();
	  n != (*p).getNeighbours().end(); predn = n, ++n, ++nm) {
	assert(*nm == *n);
	assert(mesh.point(*n).hasNeighbour(*predn));
	const double a1 = (*p).angle(point(*predn), point(*n));
	assert(a1 >= 0.);
	a.push_back(a1);
	s += a1;
	const double a2 = (*pm).angle(mesh.point(*predn), mesh.point(*n));
	assert(a2 >= 0.);
	am.push_back(a2);
	sm += a2;
      }
      // then adjust then using the sum of values in the 1-star
      assert(a.size() == am.size());
      assert(s != 0.);
      assert(sm != 0.);
      std::vector<double>::iterator aam = am.begin();
      for(std::vector<double>::iterator aa = a.begin(); aa != a.end(); ++aa, ++aam) {
	(*aam) /= sm;
	(*aa) /= s;
	result += fabs((*aam) - (*aa));
      }
    }

  return result;
}

std::vector<std::vector<Coord3D> > Mesh::getSqueletonPaths() {
  std::vector<std::vector<Coord3D> > result;

  // first compute point configuration
  for(Mesh::point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getNbTriangles() == 0) {
      const unsigned int nb = (*p).getNbNeighbours();
      if (nb == 0)
	(*p).setFlag(0); // a single point
      else
      (*p).setFlag(nb - 1); // number of corresponding paths
    }
    else
      if ((*p).getIsBoundary() && ((*p).getNbTriangles() != (*p).getNbNeighbours() - 1))
	(*p).setFlag(2); // border of a triangle set, but with an adjacency
      else
	(*p).setFlag(0); // inner point

  // Then build paths
  for(Mesh::point_iterator p = point_begin(); p != point_end(); ++p)
    if ((*p).getFlag() > 0) {
      unsigned char nbStep = ((*p).getFlag() == 1) ? 2 : 1;
      std::vector<Coord3D> path;
      (*p).incFlag(-1);
      path.push_back(*p);
      VertexID current = (*p).getId();
      for(unsigned char i = 0; i != nbStep; ++i) {
	if (i == 1) {
	  std::reverse(path.begin(), path.end());
	  current = (*p).getId();
	}
	bool found = true;
	while(found) {
	  found = false;
	  bool added = false;
	  for(std::deque<VertexID>::const_iterator nb = point(current).getNeighbours().begin();
	      nb != point(current).getNeighbours().end(); ++nb) {
	    assert(*nb < getNbPoints());
	    if (point(*nb).getFlag() >= 1) {
	      current = *nb;
	      if (point(*nb).getFlag() == 1)
		found = true;
	      point(*nb).incFlag(-1);
	      path.push_back(point(*nb));
	      added = true;
	      break;
	    }
	  }
	  if (!added) { // it corresponds to a loop
	    assert(!found);
	    path.push_back(path.front());
	  }
	}
      }

      result.push_back(path);
    }

  return result;
}


std::vector<PointOnEdge> Mesh::getPointOnEdge(const Coord3D & coord, bool onlyOnBorder,
					      double epsilon) const {
  std::vector<PointOnEdge> result;

  for(Mesh::const_point_iterator p1 = point_begin(); p1 != point_end(); ++p1) {
    if (coord.distance(*p1) < epsilon) {
      if (!onlyOnBorder || (*p1).getIsBoundary())
	result.push_back(PointOnEdge(*p1));
    }
    else
      for(std::deque<VertexID>::const_iterator idp2 = (*p1).getNeighbours().begin();
	  idp2 != (*p1).getNeighbours().end(); ++idp2)
	if (*idp2 > (*p1).getId()) {
	  const Point3D & p2 = point(*idp2);
	  if ((coord.distance(p2) > epsilon)
	      && ((!onlyOnBorder) || (isBoundaryEdge((*p1).getId(), p2.getId())))) {
	    if (Edge3D(*p1, p2).contains(coord, epsilon)) {
	      result.push_back(PointOnEdge(*p1, p2, coord));
	    }
	  }
	}
  }

  return result;
}

VertexID Mesh::getNearestPoint(const Point3D & center) const {

  double length_t = point(0).distance(center);
  VertexID id = 0;

  for(Mesh::const_point_iterator i = point_begin(); i != point_end(); ++i) {
    double nl = (*i).distance(center);
    if (nl < length_t) {
      length_t = nl;
      id = (*i).getId();
    }
  }


  return id;
}

