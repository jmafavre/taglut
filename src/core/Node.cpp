/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Node.h"

using namespace Taglut;

Node::Node(NodeID id_t, int flag_t) {
  (*this).flag = flag_t;
  (*this).id = id_t;
}

Node::Node(const Node & node) : neighbours(node.neighbours) {
  flag = node.flag;
  id = node.id;
}

Node & Node::operator=(const Node & n) {
  flag = n.getFlag();
  id = n.getId();
  neighbours = n.getNeighbours();
  return *this;
}

void Node::addNeighbour(NodeID neighbour) {
  neighbours.push_back(neighbour);
}

void Node::addNeighbour(const Node & neighbour) {
  addNeighbour(neighbour.getId());
}


void Node::replaceNeighbour(NodeID oldNeighbour, NodeID newNeighbour, bool expt) {
  for(std::deque<NodeID>::iterator i = neighbours.begin(); i != neighbours.end(); ++i)
    if (*i == oldNeighbour) {
      *i = newNeighbour;
      return;
    }

  if (expt) {
    std::cerr << "Replace neighbour " << oldNeighbour << " by " << newNeighbour << " in " << getId() << ": failed." << std::endl;
    throw Exception(std::string("Point not found. Cannot be replaced."));
  }
}

void Node::removeNeighbour(NodeID nb, bool expt) {
  for(std::deque<NodeID>::iterator i = neighbours.begin(); i != neighbours.end(); ++i)
    if (*i == nb) {
      neighbours.erase(i);
      return;
    }
  if (expt)
    throw Exception(std::string("Point not found. Cannot be removed."));
}

bool Node::hasNeighbour(const Node & p) const {
  return hasNeighbour(p.getId());
}


bool Node::hasNeighbour(NodeID c) const {
  if (neighbours.size() == 0)
    return false;
  for(std::deque<NodeID>::const_iterator i = neighbours.begin(); i != neighbours.end(); ++i)
    if ((*i) == c)
      return true;
  return false;
}

bool Node::hasLoop() const {
  return hasNeighbour(id);
}
