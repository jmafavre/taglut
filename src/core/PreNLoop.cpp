/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <functional>
#include "PLPath.h"
#include "VectorField.h"
#include "NLoop.h"
#include "PreNLoop.h"
#include "GeodesicDistance.h"

using namespace Taglut;

namespace Taglut {
  /**
     @class CompSecond
     A comparator for paths with length
  */
  class CompSecond {
  public:
    /**
       Comparator of the second element of the pair
    */
    bool operator() (const std::pair<unsigned int, double> & b1,
		     const std::pair<unsigned int, double> & b2) const {
      return b1.second < b2.second;
    }

  };

  /**
     @class CompFirstSecond
     A comparator for paths with length and side
  */
  class CompFirstSecond {
  public:
    /**
       Comparator of the second element of first element of the pair
    */
    bool operator() (const std::pair<std::pair<unsigned int, double>, bool> & b1,
		     const std::pair<std::pair<unsigned int, double>, bool> & b2) const {
      return b1.first.second < b2.first.second;
    }

  };

  /**
     @class lastPoint
     A comparator using the last point of a path
  */
  class lastPoint {
  public:
    /**
       Comparator of the second element of the pair
    */
    bool operator() (const std::vector<VertexID> & p1,
		     const std::vector<VertexID> & p2) const {
      return p1.back() < p2.back();
    }

  };


  /**
     @class not_equel
     A binary operator with two forbidden values
  */
  class not_equal {
  private:
    VertexID v1;
    VertexID v2;
  public:
    /** default constructor */
    not_equal(VertexID v1_t, VertexID v2_t) : v1(v1_t), v2(v2_t) {}
    /** binary operator */
    bool operator() (VertexID v) const { return (v != v1) && (v != v2); }
  };

}


Pre3Loop::Pre3Loop() {
}

Pre3Loop::Pre3Loop(const Point3D & point,
		   const Mesh & mesh,
		   const MeshMap & mMap, const VectorField & vf) {

  saddlePoint = point.getId();
  saddlePointValue = mMap.getValue(saddlePoint);

  // build the integral lines associated to the given point
  std::vector<PLPath> integralLinesVector = PLPath::getIntegralLinesFromSaddlePoint(point, mesh, mMap, vf);
  assert(integralLinesVector.size() == 2);
  assert(integralLinesVector.front().front() == integralLinesVector[1].front());
  assert(integralLinesVector.front().size() > 1);
  assert(integralLinesVector[1].size() > 1);
  direction = mMap.getValue(integralLinesVector.front().front()) < mMap.getValue(integralLinesVector.front()[1]);
  integralLines[0] = integralLinesVector.front();
  integralLines[1] = integralLinesVector[1];
  assert(integralLines[0].size() >= 2);
  assert(integralLines[1].size() >= 2);

  PLPath integralLine1(mesh);
  PLPath integralLine2(mesh);

  integralLine1.addPointAtEnd(integralLines[0].front());
  integralLine2.addPointAtEnd(integralLines[0].front());
  double distance1 = 0.0;
  double distance2 = 0.0;

  double lsLength = 0.0;
  double correspondingILValue = 0.0;

  bool endOfPath = false;
  bool split = false;

  PLPath::const_iterator p2 = integralLines[1].begin();
  PLPath::const_iterator predp2 = integralLines[1].begin();
  PLPath::const_iterator predp1 = integralLines[0].begin();
  for(PLPath::const_iterator p1 = integralLines[0].begin() + 1; p1 != integralLines[0].end(); ++p1, ++predp1) {
    // progression of the second side of the path at the same distance
    distance1 += (*p1).distance(*predp1);
    while ((direction && (mMap.getValue(*p1) > mMap.getValue(*p2))) || (!direction && (mMap.getValue(*p1) < mMap.getValue(*p2)))) {
      PLPath::const_iterator nextp2 = p2 + 1;
      if (nextp2 == integralLines[1].end()) {
	endOfPath = true;
	break;
      }
      distance2 += (*p2).distance(*nextp2);
      predp2 = p2;
      p2 = nextp2;
      integralLine2.addPointAtEnd(*p2);
    }
    integralLine1.addPointAtEnd(*p1);

    assert(predp2 + 1 == p2);

    levelSet = mMap.computeLevelSetCC(*p1);

    if (!levelSet.isLoop())
      throw Exception("PreNLoop(): support of surfaces with boundaries not yet implemented.");

    // we are in a split, a saddle point in the other direction... Stop the progression
    if (!levelSet.intersects(*predp2, *p2)) {
      split = true;
      break;
    }

    // computing the length of the level set
    double locallsLength = levelSet.length();
    if (locallsLength > lsLength) {
      // store the highest length
      lsLength = locallsLength;
      correspondingILValue = mMap.getValue(*p1);
    }

    // if the 3-loop is well-balanced, we stop
    if (locallsLength <= 2 * (distance1 + distance2)) {
      break;
    }

  }

  // if the stop of progression occured because of a split or the end of a path
  // then get the better level set seen before
  if (split || endOfPath) {
    if (correspondingILValue != 0.) {
      assert(correspondingILValue != 0.);
      integralLine1.cutPath(correspondingILValue, direction, mMap);
      integralLine2.cutPath(correspondingILValue, direction, mMap);

      levelSet = mMap.computeLevelSetCC(integralLine1.back());
    }
    else {
      double ratio = .5;
      const double step = 0.5;
      bool intersect = false;
      const PointOnEdge & p1 = integralLines[0][1];
      const PointOnEdge & p2 = integralLines[1][1];
      assert(p1.isVertex());
      assert(p2.isVertex());
      const Point3D & pp1 = mesh.point(p1.getClosestVertex());
      const Point3D & pp2 = mesh.point(p2.getClosestVertex());
      while(!intersect) {
	if (ratio < 1e-3)
	  throw Exception("PreNLoop(): cannot generate a valid 3-loop.");
	PointOnEdge poe(point, pp1, ratio);
	levelSet = mMap.computeLevelSetCC(integralLine1.back());
	if (levelSet.intersects(point, pp2)) {
	  intersect = true;
	  levelSetValue = mMap.getValue(levelSet.front());
	}
	else
	  ratio *= step;
      }

    }

  }

  if (levelSet.size() == 0)
    throw Exception("Pre3Loop(): empty level set");

  levelSetValue = mMap.getValue(levelSet.front());
  assert(levelSet.intersects(integralLines[0]));
  assert(levelSet.intersects(integralLines[1]));
}

Pre3Loop::Pre3Loop(const Pre3Loop & pre3loop) : saddlePoint(pre3loop.saddlePoint),
						saddlePointValue(pre3loop.saddlePointValue),
						levelSet(pre3loop.levelSet),
						levelSetValue(pre3loop.levelSetValue),
						direction(pre3loop.direction) {
  integralLines[0] = pre3loop.integralLines[0];
  integralLines[1] = pre3loop.integralLines[1];
}


bool Pre3Loop::intersects(const Pre3Loop & pre3Loop, double epsilon) const {
  const double value = levelSetValue;
  const bool up = direction;
  const double value2 = pre3Loop.levelSetValue;
  const bool up2 = pre3Loop.direction;
  return ((pre3Loop.integralLines[0].intersects(levelSet) &&
	   isIntersectedOrientation(value2, up2, value, up, epsilon)) ||
	  (pre3Loop.levelSet.intersects(integralLines[0]) && isIntersectedOrientation(value, up, value2, up2, epsilon)));
}

std::vector<std::vector<Coord3D> > Pre3Loop::getLines(const MeshMap & mMap, bool cut) const {
  std::vector<std::vector<Coord3D> > result;
  result.push_back(levelSet.getCoordsPath());
  if (cut) {
    result.push_back(integralLines[0].getCoordsCutPath(levelSetValue, direction, mMap));
    result.push_back(integralLines[1].getCoordsCutPath(levelSetValue, direction, mMap));
  }
  else {
    result.push_back(integralLines[0].getCoordsPath());
    result.push_back(integralLines[1].getCoordsPath());
  }

  return result;
}

bool Pre3Loop::isIntersectedOrientation(double value, bool up, double value2, bool up2, double epsilon) {
  if (up != up2)
    return ((up && (value2 <= value + epsilon)) || (!up && (value2 + epsilon >= value)));
  else {
    // remember: the integral line of the first one intersects the level set of the second one
    return ((up && (value2 <= value + epsilon)) || (!up && (value2 + epsilon >= value)));
  }
}

std::string Pre3Loop::toString() const {
  std::ostringstream s;

  s << "Pre3Loop(v:" << getSaddlePointValue() << ", d:" << getDirection() << ", level set: " << getLevelSetValue() <<")";

  return s.str();
}



PreNLoop::PreNLoop() {
}

PreNLoop::PreNLoop(const std::vector<Pre3Loop> & pre3Loops_t, double epsilon) : pre3Loops(pre3Loops_t) {
  updateLevelSetValue(epsilon);
}

PreNLoop::PreNLoop(const Pre3Loop & pre3Loop) {
  pre3Loops.push_back(pre3Loop);
  updateLevelSetValue();
}

PreNLoop::PreNLoop(const PreNLoop & preNLoop) : pre3Loops(preNLoop.pre3Loops), levelSetValue(preNLoop.levelSetValue) {
}

PreNLoop & PreNLoop::addPre3Loop(const Pre3Loop & pre3Loop, double epsilon) {
  pre3Loops.push_back(pre3Loop);
  const double lsv = levelSetValue;
  try {
    updateLevelSetValue(epsilon);
  }
  catch(...) {
    pre3Loops.pop_back();
    levelSetValue = lsv;
  }
  return *this;
}


PreNLoop & PreNLoop::updateLevelSetValue(double epsilon) {
  if (pre3Loops.size() == 0)
    return *this;

  // compute the levelset value of the n-loop
  levelSetValue = 0.;
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    levelSetValue += (*p3l).getLevelSetValue();
  levelSetValue /= pre3Loops.size();

  return updateLevelSetValueUsingSaddlePoint(epsilon);
}

std::pair<double, double> PreNLoop::getMinMaxSaddleValues() const {
  // check for validity according to the saddle points
  double min = -std::numeric_limits<double>::max();
  double max = std::numeric_limits<double>::max();

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
    if ((*p3l).getDirection() && ((*p3l).getSaddlePointValue() > min))
      min = (*p3l).getSaddlePointValue();
    else if ((!((*p3l).getDirection())) && ((*p3l).getSaddlePointValue() < max))
      max = (*p3l).getSaddlePointValue();
  }

  return std::pair<double, double>(min, max);
}

PreNLoop & PreNLoop::updateLevelSetValueUsingSaddlePoint(double epsilon) {
  std::pair<double, double> minmax = getMinMaxSaddleValues();

  const double e2 = epsilon * getMinimumDifferenceValue();
  assert(e2 >= 0.);

  if (minmax.first == minmax.second)
    return updateLevelSetValue(minmax.first, minmax.second);
  else
    return updateLevelSetValue(minmax.first + e2, minmax.second - e2);
}

PreNLoop & PreNLoop::updateLevelSetValue(double min, double max) {
  if (min > max)
    throw Exception("updateLevelSetValue(2): min > max");
  if (min == max) {
    levelSetValue = min;
  }
  if (levelSetValue <= min) {
    levelSetValue = min;
  }
  if (levelSetValue >= max) {
    levelSetValue = max;
  }

  assert(isValidLevelSetValue());
  return *this;
}


Pre3Loop PreNLoop::removeOffCentered3Loop(bool side) {
  // then find the off-centered saddle point
  double distance = -1.;
  std::vector<Pre3Loop>::iterator point;
  for(std::vector<Pre3Loop>::iterator p3l = pre3Loops.begin() + 1; p3l != pre3Loops.end(); ++p3l)
    if ((*p3l).getDirection() == side) {
      double d = fabs(levelSetValue - (*p3l).getSaddlePointValue());
      if (d > distance) {
	distance = d;
	point = p3l;
      }
    }

  if (distance < 0.)
    throw Exception("removeOffCentered3Loop(1): cannot remove non-existing point");
  Pre3Loop result(*point);

  // remove and return the corresponding 3-loop
  pre3Loops.erase(point);
  return result;
}

std::vector<std::vector<Coord3D> > PreNLoop::getLines(const MeshMap & mMap, bool cut) const {
  if (pre3Loops.size() == 0)
    throw Exception("getLines(2): empty n-loop");

  std::vector<std::vector<Coord3D> > result;

  PLPath ls = getLevelSet(mMap);
  result.push_back(ls.getCoordsPath());

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
    if (cut) {
      result.push_back((*p3l).getFirstIntegralLine().getCoordsCutPath(levelSetValue, (*p3l).getDirection(), mMap));
      result.push_back((*p3l).getSecondIntegralLine().getCoordsCutPath(levelSetValue, (*p3l).getDirection(), mMap));
    }
    else {
      result.push_back((*p3l).getFirstIntegralLine().getCoordsPath());
      result.push_back((*p3l).getSecondIntegralLine().getCoordsPath());
    }
  }


  return result;
}

bool PreNLoop::intersectsOtherPre3Loops(const Pre3Loop & pre3Loop, double epsilon) const {
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    if ((*p3l).intersects(pre3Loop, epsilon))
      return true;
  return false;
}

bool PreNLoop::intersects(const Pre3Loop & pre3Loop, const MeshMap & mMap, double epsilon) const {
  if (getNbPre3Loop() == 0)
    return false;

  PLPath ls;
  try {
    ls = getLevelSet(mMap);
  }
  catch (...) {
    return false;
  }
  if (ls.size() < 2)
    return false;

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    if (((pre3Loop.getFirstIntegralLine().intersects(ls) &&
	  Pre3Loop::isIntersectedOrientation(pre3Loop.getLevelSetValue(), pre3Loop.getDirection(), levelSetValue, (*p3l).getDirection(), epsilon)) ||
	 (pre3Loop.getLevelSet().intersects((*p3l).getFirstIntegralLine()) &&
	  Pre3Loop::isIntersectedOrientation(levelSetValue, (*p3l).getDirection(), pre3Loop.getLevelSetValue(), pre3Loop.getDirection(), epsilon))))
      return true;

  return false;
}

bool PreNLoop::intersects(const PreNLoop & preNLoop, const MeshMap & mMap, double epsilon) const {
  if ((getNbPre3Loop() == 0) || (preNLoop.getNbPre3Loop() == 0))
    return false;

  PLPath ls = getLevelSet(mMap);
  PLPath ls2 = preNLoop.getLevelSet(mMap);

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    for(std::vector<Pre3Loop>::const_iterator p3l2 = preNLoop.getPre3Loops().begin(); p3l2 != preNLoop.getPre3Loops().end(); ++p3l2)
      if ((((*p3l2).getFirstIntegralLine().intersects(ls) &&
	    Pre3Loop::isIntersectedOrientation(preNLoop.getLevelSetValue(), (*p3l2).getDirection(), levelSetValue, (*p3l).getDirection(), epsilon)) ||
	   (ls2.intersects((*p3l).getFirstIntegralLine()) &&
	    Pre3Loop::isIntersectedOrientation(levelSetValue, (*p3l).getDirection(), preNLoop.getLevelSetValue(), (*p3l2).getDirection(), epsilon))))
	return true;

  return false;
}


std::vector<std::pair<std::pair<unsigned int, double>, bool> > PreNLoop::getILLocationAndSideInPath(const PLPath & path, bool direction) const {
  std::vector<std::pair<std::pair<unsigned int, double>, bool> > result;
  if (path.size() < 2) // empty path
    return result;
  assert(path.isLoop());

  unsigned int idP3l = 0;
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l, ++idP3l)
    if ((*p3l).getDirection() == direction) {
      assert(path.intersects((*p3l).getFirstIntegralLine()));
      result.push_back(std::pair<std::pair<unsigned int, double>, bool>(std::pair<unsigned int, double>(idP3l, path.getIntersectionValue((*p3l).getFirstIntegralLine())),
									true));
      assert(path.intersects((*p3l).getSecondIntegralLine()));
      result.push_back(std::pair<std::pair<unsigned int, double>, bool>(std::pair<unsigned int, double>(idP3l, path.getIntersectionValue((*p3l).getSecondIntegralLine())), false));
    }

  return result;
}

std::vector<std::pair<unsigned int, double> > PreNLoop::getILLocationInPath(const PLPath & path, bool direction) const {
  std::vector<std::pair<unsigned int, double> > result;
  if (path.size() < 2) // empty path
    return result;
  assert(path.isLoop());

  unsigned int idP3l = 0;
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l, ++idP3l)
    if ((*p3l).getDirection() == direction) {
      assert(path.intersects((*p3l).getFirstIntegralLine()));
      result.push_back(std::pair<unsigned int, double>(idP3l, path.getIntersectionValue((*p3l).getFirstIntegralLine())));
      assert(path.intersects((*p3l).getSecondIntegralLine()));
      result.push_back(std::pair<unsigned int, double>(idP3l, path.getIntersectionValue((*p3l).getSecondIntegralLine())));
    }

  return result;
}

bool PreNLoop::isWellOrderedBySide(const MeshMap & mMap, bool side) const {
  unsigned int nbSameSide = 0;
  if (uniqueDirection() && (getDirection() != side))
    return true;
  assert(isValidLevelSetValue());

  const std::pair<double, double> minmax = getMinMaxSaddleValues();
  const double lsside = (20 * (side ? minmax.first : minmax.second) + levelSetValue) / 21;

  const PLPath ls = mMap.computeLevelSetFromValueAndPath(pre3Loops.front().getFirstIntegralLine(), lsside);
  assert(ls.intersects(pre3Loops.front().getFirstIntegralLine()));

  const double lslength = ls.length();
  assert(ls.isLoop());

  // get and ordered the intersections with integral lines
  std::vector<std::pair<unsigned int, double> > positions = getILLocationInPath(ls, side);

  std::sort(positions.begin(), positions.end(), CompSecond());

  // count the number of "twins"
  std::vector<std::pair<unsigned int, double> >::const_iterator previous = positions.begin() + (positions.size() - 1);
  if ((positions.front().second == 0.) && (positions.back().second == lslength))
    return false;
    //throw Exception("isWellOrderedBySide(2): points are in the same location. Cannot decide (begin/end).");
  for(std::vector<std::pair<unsigned int, double> >::const_iterator pos = positions.begin(); pos != positions.end(); previous = pos, ++pos) {
    if ((*previous).second == (*pos).second)
      return false;
      //throw Exception("isWellOrderedBySide(2): points are in the same location. Cannot decide.");
    else if ((*previous).first == (*pos).first)
      ++nbSameSide;
  }


  return nbSameSide == 2;

}

bool PreNLoop::isWellOrdered(const MeshMap & mMap) const {

  for(unsigned char i = 0; i < 2; ++i) {
    if (!isWellOrderedBySide(mMap, i == 0))
      return false;
  }

  return true;
}

std::vector<std::vector<std::pair<unsigned int, bool> > > PreNLoop::getBasePointSlotsBySide(const MeshMap & mMap, bool side) const {
  std::vector<std::vector<std::pair<unsigned int, bool> > > result;

  if (uniqueDirection() && (getDirection() != side))
    return result;
  assert(isValidLevelSetValue());

  const std::pair<double, double> minmax = getMinMaxSaddleValues();
  const double lsside = ((side ? minmax.first : minmax.second) + levelSetValue) / 2;

  const PLPath ls = mMap.computeLevelSetFromValueAndPath(pre3Loops.front().getFirstIntegralLine(), lsside);
  assert(ls.intersects(pre3Loops.front().getFirstIntegralLine()));

  assert(ls.isLoop());

  // get and ordered the intersections with integral lines
  std::vector<std::pair<std::pair<unsigned int, double>, bool> > positions = getILLocationAndSideInPath(ls, side);

  std::sort(positions.begin(), positions.end(), CompFirstSecond());

  // compute the slots
  std::vector<std::pair<std::pair<unsigned int, double>, bool> >::const_iterator previous = positions.begin(); // the first step creates a new slot
  for(std::vector<std::pair<std::pair<unsigned int, double>, bool> >::const_iterator pos = positions.begin(); pos != positions.end(); previous = pos, ++pos) {
    if ((*previous).first.first == (*pos).first.first) {
      result.push_back(std::vector<std::pair<unsigned int, bool> >());
    }

    result.back().push_back(std::pair<unsigned int, bool>((*pos).first.first, (*pos).second));
  }

  if (result.size() > 0) {
    // do we need to merge (or not) the first and last slot?
    if (positions.front().first.first != positions.back().first.first) {
      result.front().insert(result.front().end(), result.back().begin(), result.back().end());
      result.pop_back();
    }
  }

  assert((result.size() == 2) || (!isWellOrderedBySide(mMap, side)));
  return result;

}

std::pair<double, double> PreNLoop::getBasePointsBySide(bool side, const MeshMap & mmap, const PLPath & ls, double l) const {

  std::vector<std::vector<std::pair<unsigned int, bool> > > basepointsbs = getBasePointSlotsBySide(mmap, side);

  if (basepointsbs.size() == 0)
    throw Exception("getBasePointsBySide(4): empty side");

  if (basepointsbs.size() != 2)
    throw Exception("getBasePointsBySide(4): bad structure");

  std::vector<double> positionsBySide[2];

  for(unsigned char i = 0; i != 2; ++i) {
    for(std::vector<std::pair<unsigned int, bool> >::const_iterator bp = basepointsbs[i].begin(); bp != basepointsbs[i].end(); ++ bp) {
      double location;
      if ((*bp).second) {
	location = ls.getIntersectionValue(pre3Loops[(*bp).first].getFirstIntegralLine());
      }
      else
	location = ls.getIntersectionValue(pre3Loops[(*bp).first].getSecondIntegralLine());
      positionsBySide[i].push_back(location);
    }
  }

  assert(positionsBySide[0].size() == positionsBySide[1].size());


  return std::pair<double, double>(PLPath::meanMod(positionsBySide[0], l, positionsBySide[1].front()),
				   PLPath::meanMod(positionsBySide[1], l, positionsBySide[0].front()));
}

std::pair<double, double> PreNLoop::getBasePoints(const MeshMap & mmap, const PLPath & ls) const {
  assert(getNbPre3Loop() != 0);
  bool twoSides = true;
  std::pair<double, double> result;
  std::pair<double, double> resultByside[2];
  const double l = ls.length();

  try {
    resultByside[0] = getBasePointsBySide(false, mmap, ls, l);
  }
  catch (Exception e) {
    twoSides = false;
  }

  try {
    resultByside[1] = getBasePointsBySide(true, mmap, ls, l);
  }
  catch (Exception e) {
    if (!twoSides)
      throw Exception("getBasePoints(1): empty structure");
    return resultByside[0];
  }

  if (!twoSides)
    return resultByside[1];

  const double m1 = PLPath::distanceMod(resultByside[0].first, resultByside[1].first, l) +
    PLPath::distanceMod(resultByside[0].second, resultByside[1].second, l);
  const double m2 = PLPath::distanceMod(resultByside[0].first, resultByside[1].second, l) +
    PLPath::distanceMod(resultByside[0].second, resultByside[1].first, l);

  if (m1 < m2) {
    result.first = PLPath::middleMod(resultByside[0].first, resultByside[1].first, l);
    result.second = PLPath::middleMod(resultByside[0].second, resultByside[1].second, l);
  }
  else {
    result.first = PLPath::middleMod(resultByside[0].first, resultByside[1].second, l);
    result.second = PLPath::middleMod(resultByside[0].second, resultByside[1].first, l);
  }

  return result;
}

std::vector<std::pair<unsigned int, double> > PreNLoop::buildOrderedExtremaListBySide(const PLPath & levelset, bool side) const {
  // get the list of crossings
  std::vector<std::pair<unsigned int, double> > ill = getILLocationInPath(levelset, side);
  // order it
  std::sort(ill.begin(), ill.end(), CompSecond());

  std::vector<std::pair<unsigned int, double> > result;
  // then look for the beginning of a set
  std::vector<std::pair<unsigned int, double> >::const_iterator ppred = ill.begin() + (ill.size() - 1);
  bool found = false;
  for(std::vector<std::pair<unsigned int, double> >::const_iterator p = ill.begin(); p != ill.end(); ppred = p, ++p) {
    if ((*ppred).first == (*p).first) {
      if (found)
	break;
      else
	found = true;
    }
    if (found) {
      result.push_back(*p);
    }
  }

  assert(2 * result.size() == ill.size());

  return result;
}

std::vector<VertexID> PreNLoop::buildOrderedExtremaList(const Mesh & mesh, const PLPath & levelset, double e1, double e2) const {
  std::vector<std::pair<unsigned int, double> > result1 = buildOrderedExtremaListBySide(levelset, true);
  std::vector<std::pair<unsigned int, double> > result2 = buildOrderedExtremaListBySide(levelset, false);


  if (result1.size() != 0) {
    if (levelset.distance(result1.back().second, e1) >
	levelset.distance(result1.front().second, e1))
      std::reverse(result1.begin(), result1.end());
  }

  if (result2.size() != 0) {
    if (levelset.distance(result2.back().second, e1) <
	levelset.distance(result2.front().second, e1))
      std::reverse(result1.begin(), result1.end());
  }

  std::vector<VertexID> result;
  // add the first side
  for(std::vector<std::pair<unsigned int, double> >::const_iterator r = result1.begin(); r != result1.end(); ++r)
    result.push_back(pre3Loops[(*r).first].getSaddlePoint());
  // add the middle point
  result.push_back(mesh.getClosestVertex(levelset, e1));
  // add the second side
  for(std::vector<std::pair<unsigned int, double> >::const_iterator r = result2.begin(); r != result2.end(); ++r)
    result.push_back(pre3Loops[(*r).first].getSaddlePoint());
  // add the other middle point
  result.push_back(mesh.getClosestVertex(levelset, e2));

  return result;
}


std::vector<std::vector<VertexID> > PreNLoop::computePathsFromSaddlePoints(const MeshMap & mMap, Mesh & mesh, VertexID b1) const {
  MeshManipulator mManip(mesh);
  std::vector<VertexID> sp1;
  std::vector<VertexID> sp2;
  const double b1value = mMap[b1];
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    if (mMap[(*p3l).getSaddlePoint()] < b1value)
      sp1.push_back((*p3l).getSaddlePoint());
    else
      sp2.push_back((*p3l).getSaddlePoint());
  mManip.setLength(LengthMeshMapSignedGradient(mMap, b1value, false));
  std::vector<std::vector<VertexID> > ps1 = mManip.computeShortestPaths(b1, sp1);
  mManip.setLength(LengthMeshMapSignedGradient(mMap, b1value, true));
  std::vector<std::vector<VertexID> > ps2 = mManip.computeShortestPaths(b1, sp2);
  ps1.insert(ps1.end(), ps2.begin(), ps2.end());
  return ps1;
}

NLoop PreNLoop::getNLoop(Mesh & mesh, const MeshMap & mMap, const VectorField & vf, bool adjustByAngles, bool globalAdjustment) const {
  if (!isWellOrdered(mMap))
    throw Exception("getNLoop(): the structure is not well-ordered.");
  NLoop result;

  // get the level set
  PLPath ls = getLevelSet(mMap);
  // compute the two middle points
  std::pair<double, double> midPoints = getBasePoints(mMap, ls);
  assert(midPoints.first != midPoints.second);
  // compute the associated extrema
  std::pair<double, double> extremaLS = ls.getExtremaValuesBetweenPoints(midPoints.first, midPoints.second);

  // compute the base points
  VertexID b1 = mesh.getClosestVertex(ls, midPoints.first);
  VertexID b2 = mesh.getClosestVertex(ls, midPoints.second);
  if (adjustByAngles) {
    // build the extrema list
    std::vector<VertexID> extrema = buildOrderedExtremaList(mesh, ls, extremaLS.first, extremaLS.second);

    std::vector<Coord3D> extremaCoords;
    for(std::vector<VertexID>::const_iterator e = extrema.begin(); e != extrema.end(); ++e)
      extremaCoords.push_back(mesh.point(*e));

    if (globalAdjustment) {
      PLPath iL1 = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.first), mMap.getMesh(), mMap, vf);
      PLPath iL2 = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.second), mMap.getMesh(), mMap, vf);

      b1 = iL1.getMiddleByAngle(extremaCoords).second.getClosestVertex();
      b2 = iL2.getMiddleByAngle(extremaCoords).second.getClosestVertex();
    }
    else {
      PLPath iL1a = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.first), true, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL1a = iL1a.getMiddleByAngle(extremaCoords, true);
      PLPath iL1b = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.first), false, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL1b = iL1b.getMiddleByAngle(extremaCoords, true);
      if (piL1a.first < piL1b.first)
	b1 = piL1a.second.getClosestVertex();
      else
	b1 = piL1b.second.getClosestVertex();

      PLPath iL2a = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.second), true, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL2a = iL2a.getMiddleByAngle(extremaCoords, true);
      PLPath iL2b = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.second), false, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL2b = iL2b.getMiddleByAngle(extremaCoords, true);
      if (piL2a.first < piL2b.first)
	b2 = piL2a.second.getClosestVertex();
      else
	b2 = piL2b.second.getClosestVertex();

    }
  }

  // create the nloop
  MeshManipulator mManip(mesh);

  // build the paths that correspond to the extrema points
  std::vector<VertexID> extremaPoints;
  extremaPoints.push_back(mesh.getClosestVertex(ls, extremaLS.first));
  extremaPoints.push_back(mesh.getClosestVertex(ls, extremaLS.second));
  std::vector<std::vector<VertexID> > paths1 = mManip.computeShortestPaths(b1, extremaPoints);
  std::vector<std::vector<VertexID> > paths2 = mManip.computeShortestPaths(b2, extremaPoints);
  assert(paths1.size() == paths2.size());


  // build the paths that correspond to the saddle points
  std::vector<VertexID> saddlePoints;
  std::vector<std::vector<VertexID> > ps1 = computePathsFromSaddlePoints(mMap, mesh, b1);
  std::vector<std::vector<VertexID> > ps2 = computePathsFromSaddlePoints(mMap, mesh, b2);
  paths1.insert(paths1.end(), ps1.begin(), ps1.end());
  paths2.insert(paths2.end(), ps2.begin(), ps2.end());
  assert(paths1.size() == paths2.size());

  std::sort(paths1.begin(), paths1.end(), lastPoint());
  std::sort(paths2.begin(), paths2.end(), lastPoint());

  std::vector<std::vector<VertexID> >::iterator p1i = paths1.begin();
  for(std::vector<std::vector<VertexID> >::iterator p2i = paths2.begin(); p2i != paths2.end(); ++p1i, ++p2i) {
    std::reverse((*p1i).begin(), (*p1i).end());
    (*p2i).insert((*p2i).end(), (*p1i).begin(), (*p1i).end());
    result.addPath(*p2i);
  }

  return result;
}

PLNLoop PreNLoop::getPLNLoop(Mesh & mesh, const MeshMap & mMap, const VectorField & vf, bool adjustByAngles, bool globalAdjustment) const {
  if (!isWellOrdered(mMap))
    throw Exception("getNLoop(): the structure is not well-ordered.");
  PLNLoop result;

  // get the level set
  PLPath ls = getLevelSet(mMap);
  assert(ls.size() > 1);
  // compute the two middle points
  std::pair<double, double> midPoints;
  try {
    midPoints = getBasePoints(mMap, ls);
  }
  catch (...) {
    return result;
  }
  assert(midPoints.first != midPoints.second);
  // compute the associated extrema
  std::pair<double, double> extremaLS = ls.getExtremaValuesBetweenPoints(midPoints.first, midPoints.second);

  // compute the base points
  VertexID b1 = mesh.getClosestVertex(ls, midPoints.first);
  VertexID b2 = mesh.getClosestVertex(ls, midPoints.second);
  if (adjustByAngles) {
    // build the extrema list
    std::vector<VertexID> extrema = buildOrderedExtremaList(mesh, ls, extremaLS.first, extremaLS.second);

    std::vector<Coord3D> extremaCoords;
    for(std::vector<VertexID>::const_iterator e = extrema.begin(); e != extrema.end(); ++e)
      extremaCoords.push_back(mesh.point(*e));

    if (globalAdjustment) {
      PLPath iL1 = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.first), mMap.getMesh(), mMap, vf);
      PLPath iL2 = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.second), mMap.getMesh(), mMap, vf);

      b1 = iL1.getMiddleByAngle(extremaCoords).second.getClosestVertex();
      b2 = iL2.getMiddleByAngle(extremaCoords).second.getClosestVertex();
    }
    else {
      PLPath iL1a = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.first), true, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL1a = iL1a.getMiddleByAngle(extremaCoords, true);
      PLPath iL1b = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.first), false, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL1b = iL1b.getMiddleByAngle(extremaCoords, true);
      if (piL1a.first < piL1b.first)
	b1 = piL1a.second.getClosestVertex();
      else
	b1 = piL1b.second.getClosestVertex();

      PLPath iL2a = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.second), true, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL2a = iL2a.getMiddleByAngle(extremaCoords, true);
      PLPath iL2b = PLPath::computeIntegralLine(ls.getClosestPointOnEdge(midPoints.second), false, mMap.getMesh(), mMap, vf);
      std::pair<double, PointOnEdge> piL2b = iL2b.getMiddleByAngle(extremaCoords, true);
      if (piL2a.first < piL2b.first)
	b2 = piL2a.second.getClosestVertex();
      else
	b2 = piL2b.second.getClosestVertex();

    }
  }

  // create the nloop
  MeshManipulator mManip(mesh);

  // build the paths that correspond to the extrema points
  std::vector<PointOnEdge> extremaPoints;

  extremaPoints.push_back(ls.getClosestPointOnEdge(extremaLS.first));

  extremaPoints.push_back(ls.getClosestPointOnEdge(extremaLS.second));

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
    extremaPoints.push_back(mesh.point((*p3l).getSaddlePoint()));
  }

  FastMarching fm;
  MeshMap db1(mesh);
  fm.computeDistanceFromPointTargetPOE(db1, mesh, b1, -1., extremaPoints, true);
  VectorField vf1(db1, mesh);
  MeshMap db2(mesh);
  fm.computeDistanceFromPointTargetPOE(db2, mesh, b2, -1., extremaPoints, true);
  VectorField vf2(db2, mesh);


  for(std::vector<PointOnEdge>::const_iterator e = extremaPoints.begin(); e != extremaPoints.end(); ++e) {
    assert(db1.getValue(*e) >= 0.);
    assert(db2.getValue(*e) >= 0.);
    PLPath p1(*e, false, mesh, db1, vf1);
    assert(p1.size() != 0);
    assert(p1.back().distance(mesh.point(b1)) < 1e-5);
    p1.reversePath();
    PLPath p2(*e, false, mesh, db2, vf2);
    assert(p2.size() != 0);
    assert(p2.back().distance(mesh.point(b2)) < 1e-5);
    result.addPath((p1 + p2).removeBranchParts());
    assert(p1.front().getClosestVertex() == b1);
    assert(p2.back().getClosestVertex() == b2);
  }

  return result;
}

PLPath PreNLoop::getLevelSet(const MeshMap & mMap) const {
  assert(!pre3Loops.empty());
  assert(pre3Loops.front().getFirstIntegralLine().length() != 0.);
  return mMap.computeLevelSetFromValueAndPath(pre3Loops.front().getFirstIntegralLine(), levelSetValue);
}



std::vector<std::vector<Pre3Loop> > PreNLoop::clusterPre3Loops(bool side, double epsilon) const {
  std::vector<std::vector<Pre3Loop> > result;

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    if (side == (*p3l).getDirection()) {
      if (result.size() != 0) {
	double v = (*p3l).getSaddlePointValue();
	bool inserted = false;
	for(std::vector<std::vector<Pre3Loop> >::iterator c = result.begin(); c != result.end(); ++c) {
	  for(std::vector<Pre3Loop>::const_iterator a = (*c).begin(); a != (*c).end(); ++a)
	    if (fabs((*a).getSaddlePointValue() - v) < epsilon) {
	      inserted = true;
	      (*c).push_back(*p3l);
	      break;
	    }
	  if (inserted)
	    break;
	}

	if (!inserted) {
	  result.push_back(std::vector<Pre3Loop>());
	  result.back().push_back(*p3l);
	}
      }
      else {
	result.push_back(std::vector<Pre3Loop>());
	result.back().push_back(*p3l);
      }
    }

  return result;
}

std::vector<PreNLoop> PreNLoop::splitByClustering(const MeshMap & mMap, double epsilon) const {
  std::vector<PreNLoop> result;

  for(unsigned char i = 0; i < 2; ++i) {
    const bool side = (i == 0);
    // first cluster the pre-3-loops
    std::vector<std::vector<Pre3Loop> > clusters = clusterPre3Loops(side, .1 * getMaximumDifferenceValue());

    // then merge the intersecting structures in each cluster
    for(std::vector<std::vector<Pre3Loop> >::const_iterator c = clusters.begin(); c != clusters.end(); ++c) {
      std::vector<PreNLoop> cMerge = mergeIntersectingStructures(*c, epsilon);
      result.insert(result.end(), cMerge.begin(), cMerge.end());
    }
  }

  // compute the level sets
  adjustLevelSets(result, mMap, getMinimumDifferenceValue() * .1);


  // then try to merge the pre-n-loops
  std::vector<PreNLoop> result2;
  for(std::vector<PreNLoop>::const_iterator r = result.begin(); r != result.end(); ++r) {
    if (result2.size() != 0) {
      bool intersect = false;
      std::vector<PreNLoop>::iterator r2i;
      for(std::vector<PreNLoop>::iterator r2 = result2.begin(); r2 != result2.end(); ++r2)
	if ((*r).intersects(*r2, mMap, epsilon)) {
	  if (!intersect) {
	    intersect = true;
	    (*r2).addPreNLoop(*r);
	    r2i = r2;
	  }
	  else {
	    (*r2i).addPreNLoop(*r2);
	    (*r2).clear();
	  }
	}
      if (!intersect) {
	result2.push_back(*r);
      }

    }
    else {
      result2.push_back(*r);
    }
  }

  result.clear();
  for(std::vector<PreNLoop>::const_iterator r = result2.begin(); r != result2.end(); ++r)
    if (!(*r).empty())
      result.push_back(*r);


  return result;
}


std::vector<PLPath> PreNLoop::getLevelSetsFromSaddlePoints(const std::vector<PreNLoop> & preNLoops, const MeshMap & mMap, double epsilon) {
  std::vector<PLPath> result;

  for(std::vector<PreNLoop>::const_iterator preNLoop = preNLoops.begin(); preNLoop != preNLoops.end(); ++preNLoop)
    for(std::vector<Pre3Loop>::const_iterator pre3Loop = (*preNLoop).pre3Loops.begin(); pre3Loop != (*preNLoop).pre3Loops.end(); ++pre3Loop) {
      const double lsv = (*pre3Loop).getSaddlePointValue() + ((*pre3Loop).getDirection() ? epsilon : - epsilon);
      result.push_back(mMap.computeLevelSetFromValueAndPath((*pre3Loop).getFirstIntegralLine(), lsv));
    }

  return result;
}

void PreNLoop::adjustLevelSets(std::vector<PreNLoop> & preNLoops, const MeshMap & mMap, double epsilon) {
  const std::vector<PLPath> lss = getLevelSetsFromSaddlePoints(preNLoops, mMap, epsilon);


  // adjust the level sets according to the saddle points
  unsigned int id3Loop = 0;
  for(std::vector<PreNLoop>::iterator preNLoop = preNLoops.begin(); preNLoop != preNLoops.end(); ++preNLoop) {
    double min = -std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::max();
    double minSP = std::numeric_limits<double>::max();
    double maxSP = -std::numeric_limits<double>::max();

    const unsigned int first3Loop = id3Loop;
    const unsigned int last3Loop = id3Loop + (*preNLoop).pre3Loops.size() - 1;
    for(std::vector<Pre3Loop>::const_iterator pre3Loop = (*preNLoop).pre3Loops.begin(); pre3Loop != (*preNLoop).pre3Loops.end(); ++pre3Loop) {
      if (minSP > (*pre3Loop).getSaddlePointValue())
	minSP = (*pre3Loop).getSaddlePointValue();
      if (maxSP < (*pre3Loop).getSaddlePointValue())
	maxSP = (*pre3Loop).getSaddlePointValue();

      ++id3Loop;
      unsigned int idLS = 0;
      for(std::vector<PLPath>::const_iterator ls = lss.begin(); ls != lss.end(); ++ls, ++idLS)
	if ((idLS < first3Loop) || (idLS > last3Loop)) {
	  if ((*ls).intersects((*pre3Loop).getFirstIntegralLine()) ||
	      (*ls).intersects((*pre3Loop).getSecondIntegralLine())) {
	    const double v = mMap.getValue((*ls).front());
	    if (((*pre3Loop).getSaddlePointValue() > v)) {
	      if (min < v + epsilon)
		min = v + epsilon;
	    }
	    else if (max > v - epsilon)
	      max = v - epsilon;
	  }
	}
    }
    (*preNLoop).updateLevelSetValue((min * 2 + minSP) / 3, (max * 2 + maxSP) / 3);
    assert((*preNLoop).isValidLevelSetValue());
  }

}

std::vector<PreNLoop> PreNLoop::splitNaive(const MeshMap & mMap, double epsilon) const {
  std::vector<PreNLoop> result;
  PreNLoop copy(*this);

  for(unsigned char i = 0; i < 2; ++i) {
    const bool side = (i == 0);

    // remove pre-3-loops, starting from the top, to obtain
    // a well-ordered side (quick and dirty strategy)
    PreNLoop part;
    while ((copy.getNbPre3Loop(side) != 0) && (!copy.isWellOrderedBySide(mMap, side))) {
      part.addPre3Loop(copy.removeOffCentered3Loop(side));
    }

    if (part.getNbPre3Loop() != 0) {
      assert(part.getNbPre3Loop() < getNbPre3Loop());
      // split the pre-n-loop described by the removed pre-3-loops
      std::vector<PreNLoop> splitPart = part.split(mMap, epsilon);

      result.insert(result.end(), splitPart.begin(), splitPart.end());
    }
  }

  // add to the result the remains
  result.push_back(copy);

  return result;
}


std::vector<PreNLoop> PreNLoop::mergeIntersectingStructures(const std::vector<Pre3Loop> & pre3Loops, double epsilon) {
  std::vector<PreNLoop> result;
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
    if (result.size() != 0) {
      bool inserted = false;
      std::vector<PreNLoop>::iterator pnlInserted;
      for(std::vector<PreNLoop>::iterator pnl = result.begin(); pnl != result.end(); ++pnl)
	if ((*pnl).intersectsOtherPre3Loops(*p3l, epsilon)) {
	  bool newpre3loop = true;
	  if (!inserted) {
	    newpre3loop = false;
	    const unsigned int nbstruct = (*pnl).getNbPre3Loop();
	    (*pnl).addPre3Loop(*p3l);
	    if (nbstruct != (*pnl).getNbPre3Loop()) {
	      inserted = true;
	      pnlInserted = pnl;
	    }
	    else
	      newpre3loop = true;
	  }
	  if (newpre3loop) {
	    (*pnlInserted).addPre3Loops((*pnl).getPre3Loops());
	    (*pnl).clear();
	  }
	}
      if (!inserted)
	result.push_back(PreNLoop(*p3l));
    }
    else
      result.push_back(PreNLoop(*p3l));
  }

  std::vector<PreNLoop> result2;
  for(std::vector<PreNLoop>::iterator pnl = result.begin(); pnl != result.end(); ++pnl)
    if (!(*pnl).empty())
      result2.push_back(*pnl);

  return result2;
}

std::string PreNLoop::toString() const {
  std::ostringstream s;
  if (pre3Loops.size() == 0)
    return "Empty pre-n-loop";

  s << "PreNLoop(" << pre3Loops.size() << " Pre3Loops:";
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    s << " v:" << (*p3l).getSaddlePointValue() << "(d:" << (*p3l).getDirection() << ")";
  s << ", level set: " << getLevelSetValue() << ")";


  return s.str();
}


PreNLoop & PreNLoop::addPre3Loops(const std::vector<Pre3Loop> & pre3Loops_t, double epsilon) {
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops_t.begin(); p3l != pre3Loops_t.end(); ++p3l)
    addPre3Loop(*p3l, epsilon);
  return *this;
}

PreNLoop & PreNLoop::addPreNLoop(const PreNLoop & preNLoop, double epsilon) {
  for(std::vector<Pre3Loop>::const_iterator p3l = preNLoop.getPre3Loops().begin(); p3l != preNLoop.getPre3Loops().end(); ++p3l)
    pre3Loops.push_back(*p3l);
  levelSetValue = (levelSetValue + preNLoop.getLevelSetValue()) / 2;
  return updateLevelSetValueUsingSaddlePoint(epsilon);
}

PreNLoop & PreNLoop::addPreNLoop(const PreNLoop & preNLoop, const MeshMap & mmap, double epsilon) {
  const double lsvbackup = levelSetValue;
  const unsigned int nbBefore = pre3Loops.size();

  for(std::vector<Pre3Loop>::const_iterator p3l = preNLoop.getPre3Loops().begin(); p3l != preNLoop.getPre3Loops().end(); ++p3l)
    pre3Loops.push_back(*p3l);
  levelSetValue = (levelSetValue + preNLoop.getLevelSetValue()) / 2;
  updateLevelSetValueUsingSaddlePoint(epsilon);

  if (!isValidLevelSet(mmap)) {
    levelSetValue = lsvbackup;
    pre3Loops.erase(pre3Loops.begin() + nbBefore, pre3Loops.end());
  }

  return *this;
}

bool PreNLoop::isValidLevelSetValue() const {

  for(std::vector<Pre3Loop>::const_iterator p3l = getPre3Loops().begin(); p3l != getPre3Loops().end(); ++p3l)
    if (((*p3l).getDirection() && (levelSetValue < (*p3l).getSaddlePointValue())) ||
	(!(*p3l).getDirection() && (levelSetValue > (*p3l).getSaddlePointValue())))
      return false;
  return true;
}

bool PreNLoop::isCompatible(const PreNLoop & pnl, const MeshMap & mMap) const {
  if ((getNbPre3Loop() == 0) || (pnl.getNbPre3Loop() == 0))
    return false;


  // basic tests
  for(std::vector<Pre3Loop>::const_iterator p3l = getPre3Loops().begin(); p3l != getPre3Loops().end(); ++p3l)
    for(std::vector<Pre3Loop>::const_iterator p3l2 = pnl.getPre3Loops().begin(); p3l2 != pnl.getPre3Loops().end(); ++p3l2)
      if ((*p3l).getDirection() != (*p3l2).getDirection()) {
	if (((*p3l).getDirection() && ((*p3l).getSaddlePointValue() > (*p3l2).getSaddlePointValue())) ||
	    (!(*p3l).getDirection() && ((*p3l).getSaddlePointValue() < (*p3l2).getSaddlePointValue()))) {
	  return false;
	}
      }

  PLPath ls = getLevelSet(mMap);
  PLPath ls2 = pnl.getLevelSet(mMap);

  // then check for intersection between level sets and integral lines
  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    for(std::vector<Pre3Loop>::const_iterator p3l2 = pnl.getPre3Loops().begin(); p3l2 != pnl.getPre3Loops().end(); ++p3l2)
      if ((!((*p3l2).getFirstIntegralLine().intersects(ls) && ((*p3l2).getSecondIntegralLine().intersects(ls)))) &&
	  (!(ls2.intersects((*p3l).getFirstIntegralLine()) && (ls2.intersects((*p3l).getSecondIntegralLine())))))
	return false;


  return true;
}

bool PreNLoop::isValidLevelSet(const MeshMap & mMap) const {
  if (!isValidLevelSetValue())
    return false;

  const PLPath ls = mMap.computeLevelSetFromValueAndPath(pre3Loops.front().getFirstIntegralLine(), levelSetValue);
  assert(ls.isLoop());

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
    if (!ls.intersects((*p3l).getFirstIntegralLine()) ||
	!ls.intersects((*p3l).getSecondIntegralLine()))
      return false;

  return true;
}


PreNLoop::Signature::Signature() {
}

PreNLoop::Signature::Signature(const PreNLoop & pnl) {
  for(std::vector<Pre3Loop>::const_iterator p3l = pnl.getPre3Loops().begin();
      p3l != pnl.getPre3Loops().end(); ++p3l)
    saddlePoints.push_back((*p3l).getSaddlePoint());
}

PreNLoop::Signature::Signature(const PreNLoop::Signature & signature) : saddlePoints(signature.saddlePoints) {
}

bool PreNLoop::Signature::operator==(const PreNLoop & pnl) const {
  if (pnl.getNbPre3Loop() != saddlePoints.size())
    return false;

  std::vector<VertexID>::const_iterator sp = saddlePoints.begin();
  for(std::vector<Pre3Loop>::const_iterator p3l = pnl.getPre3Loops().begin();
      p3l != pnl.getPre3Loops().end(); ++p3l, ++sp)
    if ((*p3l).getSaddlePoint() != *sp)
      return false;

  return true;
}

bool PreNLoop::isUnknownPair(const PreNLoop & pnl1, const PreNLoop & pnl2,
			     std::vector<std::pair<PreNLoop::Signature, PreNLoop::Signature> > & signatures) {

  for(std::vector<std::pair<PreNLoop::Signature, PreNLoop::Signature> >::const_iterator signature = signatures.begin(); signature != signatures.end(); ++signature) {
    if ((((*signature).first == pnl1) &&
	 ((*signature).second == pnl2)) ||
	(((*signature).first == pnl2) &&
	 ((*signature).second == pnl1)))
      return false;
  }
  return true;
}


std::vector<std::vector<Coord3D> > PreNLoop::getCoordsPaths() const {
  std::vector<std::vector<Coord3D> > result;
  // TODO PNL

  return result;
}


std::vector<VertexID> PreNLoop::getSaddlePointsBySide(bool side) const {
  std::vector<VertexID> result;

  for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
    if ((*p3l).getDirection() == side)
      result.push_back((*p3l).getSaddlePoint());
  }

  return result;
}


std::vector<PreNLoop> PreNLoop::removeFlatDiscs(const std::vector<PreNLoop> & pnl, Mesh & mesh,
						const MeshMap & mmap,
						double ratio) {
  std::vector<PLPath> isolevels;

  // first build the list of level sets
  for(std::vector<PreNLoop>::const_iterator p = pnl.begin(); p != pnl.end(); ++p)
    isolevels.push_back((*p).getLevelSet(mmap));

  // then remove flat discs
  std::vector<PLPath> ilpreserved = PLPath::removeFlatDiscs(isolevels, mesh, ratio);

  // then build the final set pf n-loops
  std::vector<PreNLoop> result;
  for(std::vector<PreNLoop>::const_iterator p = pnl.begin(); p != pnl.end(); ++p) {
    if ((*p).arity() == 3) {
      const PLPath ls = (*p).getLevelSet(mmap);
      for(std::vector<PLPath>::const_iterator ilp = ilpreserved.begin(); ilp != ilpreserved.end(); ++ilp)
	if ((*ilp).front() == ls.front()) {
	  result.push_back(*p);
	  break;
	}
    }
    else
      result.push_back(*p);
  }

  return result;
}
