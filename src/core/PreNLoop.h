/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2010 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal / IMATI-CNR
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MeshMap.h"

#ifndef PRE_N_LOOP
#define PRE_N_LOOP

#include "Mesh.h"

namespace Taglut {

  class PLPath;
  class VectorField;
  class NLoop;
  class PLNLoop;


  /**
     @class Pre3Loop
     a pre-NLoop (N=3) described by two integral lines from
     a saddle point, and a level set
  */
  class Pre3Loop {
  private:
    /** the saddle point used to compute this pre-3-loop */
    VertexID saddlePoint;
    /** the scalar value associated to the levelset */
    double saddlePointValue;
    /** the level set that draw two paths of the 3-loop */
    PLPath levelSet;
    /** the scalar value associated to the levelset */
    double levelSetValue;
    /** the integral lines are in the direction (or not) of the scalar function */
    bool direction;
    /** the integral lines starging from the saddle point */
    PLPath integralLines[2];

  public:

    /** empty constructor */
    Pre3Loop();

    /** create the Pre3Loop using a saddle point and a scalar function
	described by a MeshMap */
    Pre3Loop(const Point3D & point, const Mesh & mesh, const MeshMap & mMap, const VectorField & vf);

    /** copy constructor */
    Pre3Loop(const Pre3Loop & pre3loop);

    /** destructor */
    ~Pre3Loop() { }


    /** return the paths */
    std::vector<std::vector<Coord3D> > getLines(const MeshMap & mMap, bool cut = true) const;

    /** return true if the current pre-3-loop intersects the given
	pre-3-loop */
    bool intersects(const Pre3Loop & pre3Loop, double epsilon = 0.) const;


    /** accessor */
    inline double getLevelSetValue() const { return levelSetValue; }
    /** accessor */
    inline bool getDirection() const { return direction; }
    /** accessor */
    inline const PLPath & getLevelSet() const {
      assert(levelSet.front().distance(levelSet.back()) < 1e-5);
      assert(levelSet.front().distance(levelSet.back()) == 0.);
      return levelSet;
    }
    /** accessor */
    inline const PLPath & getFirstIntegralLine() const { return integralLines[0]; }
    /** accessor */
    inline const PLPath & getSecondIntegralLine() const { return integralLines[1]; }
    /** accessor */
    inline VertexID getSaddlePoint() const { return saddlePoint; }
    /** accessor */
    inline double getSaddlePointValue() const { return saddlePointValue; }

    /**
       return a string that describe the current pre-3-loop
    */
    std::string toString() const;

    /** assuming that the full integral line of the first n-loop (direction: \p up, value of integral line: \p value) intersects the
	second one (direction: \p up2, value of integral line: \p value2), return true if the orientation implies an intersection.
	\p epsilon corresponds to an allowed error.
    */
    static bool isIntersectedOrientation(double value, bool up, double value2, bool up2, double epsilon);

  };

  class PreNLoop {
  public:
    /** available methods for splitting pre-n-loops */
    enum SplitMethod { SMNaive, SMByClustering };

    /** A signature corresponding to a prenloop (defined by the contained saddle point ids) */
    class Signature {
    private:
      std::vector<VertexID> saddlePoints;
    public:
      /** default constructor */
      Signature();
      /** constructor from a given PreNLoop */
      Signature(const PreNLoop & pnl);
      /** copy constructor */
      Signature(const Signature & signature);

      /** return true if the given prenloop corresponds to the current signature */
      bool operator==(const PreNLoop & pnl) const;
    };

  private:
    /** the pre-3-loops that takes part in this pre-n-loop */
    std::vector<Pre3Loop> pre3Loops;

    /** value of the level set */
    double levelSetValue;

    /**
       compute the level set value as the mean of the level sets
       from the 3-loops, unsticking it from the saddle points using epsilon.
    */
    PreNLoop & updateLevelSetValue(double epsilon = 0.);

    /**
       Split the current PreNLoop to obtain only well-ordered pre-n-loops (naive method)
    */
    std::vector<PreNLoop> splitNaive(const MeshMap & mMap, double epsilon) const;

    /**
       Split the current PreNLoop to obtain only well-ordered pre-n-loops (naive method)
    */
    std::vector<PreNLoop> splitByClustering(const MeshMap & mMap, double epsilon = 0.) const;


    /**
       Compute clusters on the pre-3-loops contained in the current pre-n-loop on the given side,
       by merging the structures if the saddle point value are closer than the given epsilon
    */
    std::vector<std::vector<Pre3Loop> > clusterPre3Loops(bool side, double epsilon) const;

    /**
       given a list of pre-n-loops, computes the integral lines associated to each saddle points of the contained
       pre-3-loops, by moving by an epsilon in the direction of the pre-3-loop
    */
    static std::vector<PLPath> getLevelSetsFromSaddlePoints(const std::vector<PreNLoop> & preNLoops, const MeshMap & mMap, double epsilon);

    /**
       given a set of pre-n-loops, adjust the associated level sets to avoid crossings. The epsilon value
       is used to unstick the temporary level sets from the saddle points
    */
    static void adjustLevelSets(std::vector<PreNLoop> & preNLoops, const MeshMap & mMap, double epsilon);

    /**
       compute the ordered list of the extrema points (saddle points) on the given side. The result corresponds
       to the location of the crossings along the level set.
    */
    std::vector<std::pair<unsigned int, double> > buildOrderedExtremaListBySide(const PLPath & levelset, bool side) const;

    /**
       compute the ordered list of the extrema points (saddle points and extrema points on the level set (e1 and e2))
    */
    std::vector<VertexID> buildOrderedExtremaList(const Mesh & mesh, const PLPath & levelset, double e1, double e2) const;

    /**
       compute the paths that link a given base point \p b1 and the saddle points,
    */
    std::vector<std::vector<VertexID> > computePathsFromSaddlePoints(const MeshMap & mMap, Mesh & mesh, VertexID b1) const;

    /**
       return the minimum saddle point value in the down direction, and the maximum saddle point value
       in the down direction.
     */
    std::pair<double, double> getMinMaxSaddleValues() const;

  public:

    /** empty constructor */
    PreNLoop();

    /** create the PreNLoop by merging the given pre-3-loops */
    PreNLoop(const std::vector<Pre3Loop> & pre3Loops, double epsilon = .1);

    /** create the PreNLoop using the given pre-3-loops */
    PreNLoop(const Pre3Loop & pre3Loop);

    /** copy constructor */
    PreNLoop(const PreNLoop & preNLoop);

    /** destructor */
    ~PreNLoop() { }

    /** add a pre-3-loop in the current structure */
    PreNLoop & addPre3Loop(const Pre3Loop & pre3Loop, double epsilon = .1);

    /** add pre-3-loops in the current structure */
    PreNLoop & addPre3Loops(const std::vector<Pre3Loop> & pre3Loops, double epsilon = .1);

    /** add pre-n-loops in the current structure, adjusting the level sets by computing a mean between the two original one,
	and adjusting it if necessary */
    PreNLoop & addPreNLoop(const PreNLoop & preNLoop, double epsilon = .1);

    /** add pre-n-loops in the current structure, adjusting the level sets by computing a mean between the two original one,
	and adjusting it if necessary. If the final prenloop is not valid, it cancel the merge */
    PreNLoop & addPreNLoop(const PreNLoop & preNLoop, const MeshMap & mmap, double epsilon = .1);

    /** remove the most off-centered pre-3-loop of the structure in the given side */
    Pre3Loop removeOffCentered3Loop(bool side);


    /**
       return the arity of the n-loop that will be constructed using this pre-n-loop
    */
    inline unsigned int arity() const {
      return 2 + pre3Loops.size();
    }

    /**
       return true if there is no pre-3-loop in the current pre-n-loop
    */
    inline bool empty() const {
      return pre3Loops.size() == 0;
    }

    /** remove all the pre-3-loops of the structure */
    inline void clear() {
      pre3Loops.clear();
    }

    /**
       return the number of Pre3Loop
    */
    inline unsigned int getNbPre3Loop() const {
      return pre3Loops.size();
    }

    /**
       return the Pre3Loops
    */
    inline const std::vector<Pre3Loop> & getPre3Loops() const {
      return pre3Loops;
    }

    /**
       return the number of Pre3Loop in the given direction
    */
    inline unsigned int getNbPre3Loop(bool side) const {
      unsigned int result = 0;
      for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
	if (side == (*p3l).getDirection())
	  ++result;
      return result;
    }

    /** return the paths */
    std::vector<std::vector<Coord3D> > getLines(const MeshMap & mMap, bool cut = true) const;

    /** return true if the pre-3-loops contained into the current pre-n-loop intersect the given
	pre-3-loop */
    bool intersectsOtherPre3Loops(const Pre3Loop & pre3Loop, double epsilon) const;

    /** return true if the current pre-n-loop intersect the given
	pre-3-loop */
    bool intersects(const Pre3Loop & pre3Loop, const MeshMap & mMap, double epsilon = 0.) const;

    /** return true if the current pre-n-loop intersect the given
	pre-n-loop */
    bool intersects(const PreNLoop & preNLoop, const MeshMap & mMap, double epsilon = 0.) const;


    /** return true if the integral lines are well-ordered in the given side
	for a merging process of the crossing points to generate a
	NLoop
    */
    bool isWellOrderedBySide(const MeshMap & mMap, bool side) const;

    /** return true if the integral lines are well-ordered
	for a merging process of the crossing points to generate a
	NLoop
    */
    bool isWellOrdered(const MeshMap & mMap) const;

    /**
       return the crossing points between integral lines and the level set, grouped by slot, in a given direction.
       The points are described by the id of the 3-loop, and the side in the 3-loop (false: first, true: second)
     */
    std::vector<std::vector<std::pair<unsigned int, bool> > > getBasePointSlotsBySide(const MeshMap & mmap, bool side) const;

    /**
       internal method used by \m getBasePoints() (approach with a robust method to compute base points)
    */
    std::pair<double, double> getBasePointsBySide(bool side, const MeshMap & mmap, const PLPath & ls, double l) const;

    /**
       assuming that the current pre-n-loop is well ordered, computes
       the two base points as the average of the crossing points on the level sets.
       The computation is made in both sides, then the merging is done
       by looking for the closest points.
       The result is are geodesic coordinates on the level set
    */
    std::pair<double, double> getBasePoints(const MeshMap & mmap, const PLPath & ls) const;

#ifndef SWIG
    /**
       assuming that the current pre-n-loop is well ordered, computes
       the two base points as the average of the crossing points on the level sets.
       The computation is made in both sides, then the merging is done
       by looking for the closest points.
       The result is are geodesic coordinates on the level set
    */
    inline std::pair<std::pair<Coord3D, double>,
		     std::pair<Coord3D, double> > getBasePointsCoords(const MeshMap & mmap, const PLPath & ls) const {
      assert(ls.size() > 1);
      // compute the two middle points
      std::pair<double, double> bp = getBasePoints(mmap, ls);

      return std::pair<std::pair<Coord3D, double>,
		       std::pair<Coord3D, double> >(std::pair<Coord3D, double>(ls.getCoordFromValue(bp.first), bp.first),
						    std::pair<Coord3D, double>(ls.getCoordFromValue(bp.second), bp.second));
    }

#endif

    /**
       return the corresponding NLoop if the current pre-n-loop is
       well ordered. Otherwise, throws an exception.
       if adjustByAngles == true, adjust the location of the base points using angles
       if globalAdjustment == true, adjustment is made using the global optimum on the whole integral line. Otherwise, the local optimum is
       selected.
    */
    NLoop getNLoop(Mesh & mesh, const MeshMap & mMap, const VectorField & vf, bool adjustByAngles, bool globalAdjustment = false) const;

    /**
       return the corresponding NLoop if the current pre-n-loop is
       well ordered. Otherwise, throws an exception.
       if adjustByAngles == true, adjust the location of the base points using angles
       if globalAdjustment == true, adjustment is made using the global optimum on the whole integral line. Otherwise, the local optimum is
       selected.
    */
    PLNLoop getPLNLoop(Mesh & mesh, const MeshMap & mMap, const VectorField & vf, bool adjustByAngles, bool globalAdjustment = false) const;

    /**
       return the level set value
    */
    inline double getLevelSetValue() const {
      return levelSetValue;
    }

    /**
       return the location along the given path of all the integral lines that takes part
       into this pre-n-loop, of direction \p direction
     */
    std::vector<std::pair<unsigned int, double> > getILLocationInPath(const PLPath & path, bool direction) const;

    /**
       return the location along the given path of all the integral lines that takes part
       into this pre-n-loop, of direction \p direction. The boolean associated result corresponds to the side (first or second il) in
       the corresponding 3-loop.
     */
    std::vector<std::pair<std::pair<unsigned int, double>, bool> > getILLocationAndSideInPath(const PLPath & path, bool direction) const;

    /**
       compute the level set associated to the value
    */
    PLPath getLevelSet(const MeshMap & mMap) const;

    /**
       Split the current PreNLoop to obtain only well-ordered pre-n-loops
    */
    inline std::vector<PreNLoop> split(const MeshMap & mMap, double epsilon = 0., enum SplitMethod sMethod = SMByClustering) const {
      if (sMethod == SMNaive)
	return splitNaive(mMap, epsilon);
      else // if (sMethod == SMByClustering)
	return splitByClustering(mMap, epsilon);
    }


    /**
       Given a set of pre-3-loops, build the corresponding pre-n-loops by merging the intersecting structures
    */
    static std::vector<PreNLoop> mergeIntersectingStructures(const std::vector<Pre3Loop> & pre3Loops, double epsilon = 0.);

    /**
       Update the level set value unsticking it from the saddle points using epsilon.
    */
    PreNLoop & updateLevelSetValueUsingSaddlePoint(double epsilon = .1);

    /**
       Update the level set value using the maximum and maximum allowed values. If the current value
       is outside of this area, the new value is computed by selecting the corresponding boundary.
    */
    PreNLoop & updateLevelSetValue(double min, double max);


    /**
       return true if the current pre-n-loop is defined by pre-3-loops all in the same direction
    */
    inline bool uniqueDirection() const {
      if (empty())
	return true;
      const bool d = pre3Loops.front().getDirection();
      for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l)
	if ((*p3l).getDirection() != d)
	  return false;
      return true;
    }

    /**
       assuming that the current pre-n-loop is defined by pre-3-loops all in the same direction,
       return this direction */
    inline bool getDirection() const {
      assert(uniqueDirection());
      if (empty())
	throw Exception("getDirection(): empty pre-n-loop");
      return pre3Loops.front().getDirection();
    }

    /**
       compute the maximum difference value between the level set and the saddle points
    */
    inline double getMaximumDifferenceValue() const {
      double d = 0.;
      for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
	double dd = fabs((*p3l).getSaddlePointValue() - levelSetValue);
	if (dd > d)
	  d = dd;
      }

      return d;
    }


    /**
       compute the minimum difference value between the level set and the saddle points
    */
    inline double getMinimumDifferenceValue() const {
      double d = std::numeric_limits<double>::max();
      for(std::vector<Pre3Loop>::const_iterator p3l = pre3Loops.begin(); p3l != pre3Loops.end(); ++p3l) {
	double dd = fabs((*p3l).getSaddlePointValue() - levelSetValue);
	if ((dd != 0.) && (dd < d))
	  d = dd;
      }

      return d;
    }

    /**
       return a string that describe the current pre-n-loop
    */
    std::string toString() const;

    /** return true if the current level set value is valid wrt the value at saddle points
     */
    bool isValidLevelSetValue() const;

    /** return true if the current level set value is valid wrt the value at saddle points. */
    bool isValidLevelSet(const MeshMap & mMap) const;

    /** return true if the two prenloop are compatible (i.e. if we can merge them) */
    bool isCompatible(const PreNLoop & pnl, const MeshMap & mmap) const;

#ifndef SWIG
    /** return a signature of the current prenloop */
    inline PreNLoop::Signature getSignature() const {
      return Signature(*this);
    }

    /** return true if the two prenloops are not described as a pair in the known signatures */
    static bool isUnknownPair(const PreNLoop & pnl1, const PreNLoop & pnl2,
			      std::vector<std::pair<PreNLoop::Signature, PreNLoop::Signature> > & signatures);
#endif

    /** return a vector of coordinates */
    std::vector<std::vector<Coord3D> > getCoordsPaths() const;

    static inline std::vector<std::vector<Coord3D> > getCoordsPaths(const std::vector<PreNLoop> & pnl) {
      std::vector<std::vector<Coord3D> > result;
      for(std::vector<PreNLoop>::const_iterator p = pnl.begin(); p != pnl.end(); ++p) {
	std::vector<std::vector<Coord3D> > pp = (*p).getCoordsPaths();
	result.insert(result.end(), pp.begin(), pp.end());
      }
      return result;
    }

    /** return tha list of the saddle points by side */
    std::vector<VertexID> getSaddlePointsBySide(bool side) const;

    /** given a set of pre-n-loop in a mesh, the corresponding mesh and a ratio,
	it return a new list of n-loops by removing all the flat discs:
	* for each region in the complement of the isolevels that are part of the n-loops, check the topology
	* if it's a disc, we compute the corresponding area, and the perimeter
	* if area / perimeter is less than the ratio, it corresponds to a flat disc
	* we remove all the 3-loops that corresponds to these discs
	* finally, we remove all the n-loops with n <= 2
	*/
    static std::vector<PreNLoop> removeFlatDiscs(const std::vector<PreNLoop> & pnl, Mesh & mesh,
						 const MeshMap & mmap,
						 double ratio);

  };

}

#endif
