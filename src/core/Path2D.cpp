/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include "Path2D.h"

using namespace Taglut;


Path2D::Path2D() { }

Path2D::Path2D(const std::vector<Coord2D> & p) : std::vector<Coord2D>(p) {
}


Path2D::Path2D(const Path2D & p) : std::vector<Coord2D>(p) {
}

Path2D & Path2D::operator=(const Path2D & path) {
  std::vector<Coord2D>::operator=(path);
  return *this;
}

bool Path2D::isPolygon() const {
  return (size() > 2) && front().same2DLocation(back());
}

bool Path2D::isInside(const Coord2D & coord) const {
  if (!isPolygon()) {
    return false;
  }
  unsigned int nbCrossings = 0;

  Path2D::const_iterator predp = begin();
  for(Path2D::const_iterator p = begin() + 1; p != end(); ++p, ++predp) {
    const Coord2D & pi = *p;
    const Coord2D & pj = *predp;
    const double x = coord.get2DX();
    const double y = coord.get2DY();

    if ((((pi.get2DY() <= y) && (y < pj.get2DY())) || ((pj.get2DY() <= y) && (y < pi.get2DY()))) &&
	(x < (pj.get2DX() - pi.get2DX()) * (y - pi.get2DY()) / (pj.get2DY() - pi.get2DY()) + pi.get2DX()))
      ++nbCrossings;
  }
  return nbCrossings % 2 == 1;
}

double Path2D::length() const {
  if (size() < 2)
    return 0.;

  double result = 0.;
  Path2D::const_iterator pred = begin();
  for(Path2D::const_iterator p = begin() + 1; p != end(); ++p, ++pred)
    result += (*pred).distance2D(*p);

  return result;
}

