/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef PATH_2D
#define PATH_2D

#include <vector>
#include "Coord2D.h"

namespace Taglut {

  /**
     A 2D path (that may be a polygon)/
     @remark similarities with Taglut::Polygon
   */
  class Path2D : public std::vector<Coord2D> {
  public:
    /** default constructor (empty path) */
    Path2D();

    /** build a path given a list of coords */
    Path2D(const std::vector<Coord2D> & p);

    /** copy constructor */
    Path2D(const Path2D & p);

    /** destructor */
    ~Path2D() { }

#ifndef SWIG
    /** copy operator */
    Path2D & operator=(const Path2D & path);
#endif

    /** return true if the current path is a polygon, i.e. if the first point is the last one */
    bool isPolygon() const;

    /** return true if the given point is inside
	the current path (that is a polygon)
    */
    bool isInside(const Coord2D & coord) const;

    /** return the length of the current path */
    double length() const;

    /** insert the point at the given location */
    inline iterator insertPoint(iterator point, const Coord2D & coord) {
      return insert(point, coord);
    }
  };

}

#endif
