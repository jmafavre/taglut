/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MESH
#define MESH

#include <assert.h>
#include <iostream>
#include <map>
#include <vector>

#include "CImgUse.h"
#include "Triangle.h"
#include "Coord3D.h"
#include "Coord2D.h"
#include "Point3D.h"
#include "Exception.h"
#include "IndentManager.h"
#include "FileManipulator.h"
#include "EdgeManipulator.h"

namespace Taglut {

  // cross definition
  class LengthEdge;
  class PointOnEdge;


  /**
     struct used by std::set
  */
  template <typename T>
  struct ltint
  {
    /**
       binary operator (comparison using "<" operator)
    */
    bool operator()(const T s1, const T s2) const
    {
      return s1 < s2;
    }
  };


  class MeshPart;
  class MeshCut;
  class Mapping2D3D;
  class MeshList;
  class NLoop;
  class PLPath;

  /**
     @class Mesh
     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @brief Mesh structure (3d points and triangles)
  */
  class Mesh : public FileManipulator {
  private:
    /** Points contains on this mesh */
    std::vector<Point3D> points;
    /** Triangles contains on this mesh */
    std::vector<Triangle> triangles;

    /** Number of points */
    VertexID nbPoints;
    /** Number of triangles */
    TriangleID nbTriangles;

    static bool isOnGrid(const Point3D & value, double size);

    void initIsBoundary(Point3D & point, bool warning = true);

    void computeCC(VertexID startId, MeshPart & mp);

    /**
       Method used by addPointOnEdge(2) to update triangles at "idT" location.
    */
    void udpateTrianglesAddPointOnEdge(TriangleID idT, VertexID p1, VertexID p2, Point3D & newPoint);

    /**
       compute connected components using point flags
    */
    VertexID computeBCCUsingPoints(VertexID id, int iFlag, int fFlag);

    /**
       Adding the supported saving and loading formats
    */
    void addFormats();

    bool findObjectOBJ(std::ifstream & file, const std::string & objectName);

    bool loadOBJ(std::ifstream & file);

    bool checkFormatOFF(std::ifstream & file);

    bool loadOFF(std::ifstream & file);

    bool checkFormatPLY(std::ifstream & file);

    bool loadPLY(std::ifstream & file);

    bool checkFormatVTK(std::ifstream & file);

    bool loadVTK(std::ifstream & file);

    bool findMeshVRML(std::ifstream & infile, std::string objectName);

    bool loadPointsVRML(std::ifstream & infile);

    bool loadTrianglesVRML(std::ifstream & infile);

    void loadFirstMeshVRML(std::ifstream & infile);

    bool loadMeshVRML(std::ifstream & infile);

    bool loadASC(std::ifstream & file);

    bool findObjectASC(std::ifstream & file, const std::string & objectName);

    void updateNeighboursAndTriangles();

  public:


    /**
       Point iterator
    */
    typedef std::vector<Point3D>::iterator point_iterator;

    /**
       Point iterator (const)
    */
    typedef std::vector<Point3D>::const_iterator const_point_iterator;

    /**
       Triangle iterator
    */
    typedef std::vector<Triangle>::iterator triangle_iterator;

    /**
       Triangle iterator (const)
    */
    typedef std::vector<Triangle>::const_iterator const_triangle_iterator;

    /**
       Given Marching Cube result (points and primitives), build Mesh structure.
    */
    Mesh(const CImgList<double> & pts, CImgList<VertexID> & primitives, bool merge = false, const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Given Marching Cube result (points and primitives), build Mesh structure.
    */
    Mesh(const CImg<> & pts, CImgList<VertexID> & primitives, bool merge = false, const Taglut::IndentManager iManager = Taglut::IndentManager());


    /**
       Creating a mesh from a file
    */
    Mesh(const std::string & filename, const std::string & objectName = "");

    /**
       create a mesh from a 3D image, using marching cubes method
    */
    Mesh(const CImg<> & image);

#ifndef SWIG
    /**
       Creating a mesh from a file
    */
    Mesh(const char * filename, const char * objectName);
#endif

    /**
       Copy constructor
    */
    Mesh(const Mesh & mesh);

    /**
       Build a mesh using a given Mesh list
    */
    Mesh(const std::deque<Mesh> & meshList);



    /**
       Build a mesh using a given MeshPart
    */
    Mesh(const MeshPart & meshPart);

    /**
       Build a mesh using a given Mapping2D3D
       @param mapping The given Mapping2D3D
       @param flatMesh If true, use flatten locations (else use 3d location)
    */
    Mesh(Mapping2D3D & mapping, bool flatMesh = true);

    /**
       Build a mesh using a given MeshCut
    */
    Mesh(MeshCut & meshCut);


    /**
       Default constructor
    */
    Mesh();

    /**
       Destructor
    */
    ~Mesh();


#ifndef SWIG
    /**
       Copy operator
    */
    Mesh & operator=(const Mesh & mesh);

    /**
       Rescale operator
    */
    inline Mesh & operator*=(double ratio) {
      for(point_iterator p = point_begin(); p != point_end(); ++p)
	(*p) *= ratio;
      return *this;
    }

    /**
       translate operator
    */
    inline Mesh & operator+=(const Coord3D & c) {
      for(point_iterator p = point_begin(); p != point_end(); ++p)
	(*p) += c;
      return *this;
    }

#endif

    /**
       Given points and primitives
       build mesh (with Point3D and Triangle)
    */
    void buildMesh(const CImgList<double> & pts, const CImgList<VertexID> & primitives, bool warning = true);


    /**
       Given points and triangles
       build mesh (with Point3D and Triangle).
    */
    void buildMesh(const std::vector<Coord3D> & pts, const std::vector<Triangle> & tgles, bool warning = true);


    /**
       Given points and triangles
       build mesh (with Point3D and Triangle).
    */
    void buildMesh(const CImg<> & pts, const CImgList<VertexID> & primitives, bool warning = true);

    /**
       Given points and primitives
       build mesh (with Point3D and Triangle)
    */
    void buildMeshTriangulate(const CImgList<double> & pts, CImgList<VertexID> & primitives, bool warning = true);

    /**
       Given points and primitives
       build mesh (with Point3D and Triangle)
    */
    void buildMeshTriangulate(const CImg<> & pts, CImgList<VertexID> & primitives, bool warning = true);

    /**
       Given points and primitives (from marching cubes for example),
       build mesh (merging points if needed)
    */
    void buildMeshMergePoints(const CImgList<double> & pts, CImgList<VertexID> & primitives, bool warning = true, const Taglut::IndentManager iManager = Taglut::IndentManager());

    /**
       Given points and primitives (from marching cubes for example),
       build mesh (merging points if needed)
    */
    void buildMeshMergePoints(const CImg<> & pts, CImgList<VertexID> & primitives, bool warning = true, const Taglut::IndentManager iManager = Taglut::IndentManager());


    /**
       Given points and triangles, build mesh
    */
    void setPointsAndbuildMesh(const std::deque<Point3D> & oPoints, const std::deque<Triangle> & triangles);

    /**
       @brief Triangulate mesh
       @param primitives primitives contains on mesh
    */
    static void triangulateCImgMesh(CImgList<VertexID> & primitives);

    /**
       Compute value for each point (black = on grid, white: outside), using
       a grid with step given by "size" parameter.
    */
    void computeGridValues(CImgList<unsigned char>& values, double size);

    /**
       Create mesh from image using grayscale value as z location
    */
    template <typename T>
    void createFromImage(const CImg<T> & image);


    /** initialisation of each point's flag */
    void setPointFlag(int value);

    /** initialisation of each triangle's flag */
    void setTriangleFlag(int value);

    /**
       set the values of the flags to \p flag for every triangle and points crossed by the given path
    */
    void setTriangleAndPointFlag(const PLPath & path, int flag);

    /** given a 3D point, it returns the closest point in the mesh */
    VertexID getNearestPoint(const Point3D & center) const;

    /**
       Given a piecewise linear path and a position on it, return the closest vertex on the mesh
    */
    VertexID getClosestVertex(const PLPath & path, double value) const;

    /**
       Given a path and a position on it, return the closest vertex on the mesh
    */
    VertexID getClosestVertex(const std::vector<VertexID> & path, double value) const;

    /**
       Given a path and two positions on it, return the closest vertices on the mesh
    */
    std::pair<VertexID, VertexID> getClosestVertices(const std::vector<VertexID> & path, double value1, double value2) const;

    /** given a pathe an a position on it, returns the corresponding point on edge */
    PointOnEdge getPointOnPath(const std::vector<VertexID> & path, double value) const;

    /** given a 3D point, it returns the corresponding points on an edge (using \p epsilon as
	a measurement of the error).
	if \p onlyOnBorder = true, only point on border edges are taken into account */
    std::vector<PointOnEdge> getPointOnEdge(const Coord3D & coord, bool onlyOnBorder = false,
					    const double epsilon = 1e-5) const;

    /**
       Compute if needed and return CImg points associated to this mesh.
       Deprecated.
    */
    CImgList<double> getCImgPoints();

    /**
       Compute if needed and return CImg primitives (i.e. triangles) associated to this mesh.
       Deprecated.
    */
    CImgList<TriangleID> getCImgPrimitives();


    /**
       Compute and return and stl list of coords associated to the points of this mesh.
    */
    std::vector<Coord3D> getCoordList() const;

    /**
       Return points' number
    */
    inline VertexID getNbPoints() const { return nbPoints; }
    /**
       Return triangles' number
    */
    inline TriangleID getNbTriangles() const { return nbTriangles; }

    /**
       Return triangles' list
    */
    inline const std::vector<Triangle> & getTriangles() const { return triangles; }

    /**
       Return points' list
    */
    inline const std::vector<Point3D> & getPoints() const { return points; }

    /**
       Return point selected by id
    */
    inline const Point3D & point(VertexID id) const {
      if (id >= getNbPoints()) {
	std::cout << "id: " << id << std::endl;
	throw Exception(std::string("Mesh: wrong point id."));
      }
      return points[id];
    }

    /**
       Return point selected by id
    */
    inline Point3D & point(VertexID id) {
      if (id >= getNbPoints()) {
	std::cout << "id: " << id << std::endl;
	throw Exception(std::string("Mesh: wrong point id."));
      }
      return points[id];
    }

    /**
       For each edge of the current mesh, apply the functor \p em
    */
    void forEachEdge(Taglut::EdgeManipulator & em) const;

    /**
       Return an iterator to the first point
    */
    inline point_iterator point_begin() { return points.begin(); }

    /**
       Return an iterator after the last point
    */
    inline point_iterator point_end() { return points.end(); }

    /**
       Return an iterator to the first point
    */
    inline const_point_iterator point_begin() const { return points.begin(); }

    /**
       Return an iterator after the last point
    */
    inline  const_point_iterator point_end() const { return points.end(); }

    /**
       Return triangle selected by id
    */
    inline const Triangle & triangle(TriangleID id) const {
      if (id >= getNbTriangles())
	throw Exception(std::string("Mesh: wrong triangle id."));
      return triangles[id];
    }

    /**
       Return triangle selected by id
    */
    inline Triangle & triangle(TriangleID id) {
      if (id >= getNbTriangles())
	throw Exception(std::string("Mesh: wrong triangle id."));
      return triangles[id];
    }


    /**
       Return an iterator to the first triangle
    */
    inline triangle_iterator triangle_begin() { return triangles.begin(); }

    /**
       Return an iterator after the last triangle
    */
    inline triangle_iterator triangle_end() { return triangles.end(); }

    /**
       Return an iterator to the first triangle
    */
    inline const_triangle_iterator triangle_begin() const { return triangles.begin(); }

    /**
       Return an iterator after the last triangle
    */
    inline const_triangle_iterator triangle_end() const { return triangles.end(); }

    /**
       Return the number of incoherent points
    */
    VertexID getNbBadBPoints() const;


    /**
       Find triangle inside triangle list witch vertices are c1, c2 and c3.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param c3 third vertex
       \return triangle (c1, c2, c3)
    */
    Triangle & findTriangle(VertexID c1, VertexID c2, VertexID c3);

    /**
       Find triangle inside triangle list witch vertices are c1, c2 and c3.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param c3 third vertex
       \return triangle (c1, c2, c3)
    */
    const Triangle & findTriangle(VertexID c1, VertexID c2, VertexID c3) const;

    /**
       Find triangle inside triangle list witch vertices are c1, c2 and c3.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param c3 third vertex
       \return triangle (c1, c2, c3)
    */
    const Triangle & findTriangle(const Point3D & c1, const Point3D & c2, const Point3D & c3) const;

    /**
       Find triangle inside triangle list witch vertices id are c1, c2. Result is not t.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param t other triangle
       \return triangle (c1, c2, ?)
    */
    Triangle & findOtherTriangle(VertexID c1, VertexID c2, const Triangle & t);

    /**
       Find triangle inside triangle list witch vertices id are c1, c2. Result is not t.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param t other triangle
       \return triangle (c1, c2, ?)
    */
    Triangle & findOtherTriangle(VertexID c1, VertexID c2, TriangleID t);

    /**
       Find triangle inside triangle list witch vertices id are c1, c2. Result is not t.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param t other triangle
       \return triangle (c1, c2, ?)
    */
    const Triangle & findOtherTriangle(VertexID c1, VertexID c2, const Triangle & t) const;

    /**
       Find triangle inside triangle list witch vertices id are c1, c2. Result is not t.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \param t other triangle
       \return triangle (c1, c2, ?)
    */
    const Triangle & findOtherTriangle(VertexID c1, VertexID c2, TriangleID t) const;

    /**
       Find a triangle inside triangle list witch vertices id are c1, c2.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \return triangle (c1, c2, ?)
    */
    Triangle & findTriangle(VertexID c1, VertexID c2);

    /**
       Find a triangle inside triangle list witch vertices id are c1, c2.
       Throw exception if triangle not found.
       \param c1 first vertex
       \param c2 second vertex
       \return triangle (c1, c2, ?)
    */
    const Triangle & findTriangle(VertexID c1, VertexID c2) const;

    /**
       given two points located on an edge, return the corresponding triangle or an exception if
       it does not exists
     */
    const Triangle & findTriangle(const PointOnEdge & p1, const PointOnEdge & p2) const;


    /**
       Assuming that (u1, u2) and (v1, v2) are edges of a piecewise linear path on the current mesh, return true if
       they're intersecting.
     */
    bool intersects(const PointOnEdge & u1, const PointOnEdge & u2,
		    const PointOnEdge & v1, const PointOnEdge & v2) const;

    /**
       Given a point and an edge that contains this point, return an edge containing the given point, and next arround this point
    */
    std::pair<VertexID, VertexID> findNextEdge(const std::pair<VertexID, VertexID> & edge, VertexID point) const;

    /**
       Given a point and an edge that contains this point, return an edge containing the given point, and next arround this point,
       but not equal to the given otherEdge
    */
    std::pair<VertexID, VertexID> findOtherNextEdge(const std::pair<VertexID, VertexID> & edge, VertexID point,
						    const std::pair<VertexID, VertexID> & otherEdge) const;


    /**
       return number of triangle that contains idP1 and idP2
    */
    TriangleID getNbTriangles(VertexID idP1, VertexID idP2) const;

    /**
       Set points and compute associates triangles using points neighbours
    */
    void setPointsAndComputeTriangles(const std::deque<Point3D> & oPoints);

    /**
       Reorder triangle's vertices
    */
    void reorderTriangleVertices();

    /**
       Reorder points' neighbours
    */
    void reorderNeighbours();

    /**
       Remove all triangles and points from this mesh
    */
    void clear();

    /**
       Compute connected components
       \return A list of mesh part
    */
    std::deque<MeshPart> getConnectedComponents();

    /**
       Compute largest connected components
    */
    MeshPart getLargestCC();

    /**
       keep flag order but with smallest amplitude as possible
    */
    void stripTriangleFlags(int startValue);

    /**
       Switch flag values
    */
    void switchTriangleFlags(int f1, int f2);


    /**
       Set flag value:
       if flag value equal to f1, set value to f2
    */
    void setPointFlags(int f1, int f2);

    /**
       Build textured primitives do display border
       return 0 if border has not been found and border's color id otherwise
    */
    unsigned int buildBorderColor(CImgList<unsigned char> & colors, CImgList<VertexID> & primitives);


#ifndef SWIG
    /**
       Duplicate points given in parameter, add it at the end of the point list and
       increase their id. Return id of the first point added
       if simple=true, it add only points, not the corresponding neighbours and triangles
    */
    VertexID addPoints(const std::deque<VertexID> & aPoints, bool simple = false);
#endif

    /**
       Add points given in parameter
       if simple=true, it add only points, not the corresponding neighbours and triangles
    */
    template <class L>
    void addPoints(const L & pList, bool simple = false);

    /**
       add the point given in parameter at the end of the point list and
       increase their id. Return the id of the point added
    */
    VertexID addPoint(const Point3D & aPoint);

    /**
       add the triangle given in parameter at the end of the triangle list and
       increase their id. Return the id of the added triangle
    */
    TriangleID addTriangle(const Triangle & triangle);

    /**
       add the triangle given in parameter at the end of the triangle list and
       increase their id. Add the triangle to points contains by itself.
       Return the id of the added triangle
    */
    TriangleID addTriangleFull(const Triangle & triangle);

    /**
       Set flag value for each point inside boundary
    */
    void setBoundaryPointFlag(int value);

    /**
       Compute number of flag values
    */
    unsigned int getNbPointFlagValues() const;

    /**
       Return true if one of the given points is a boundary point
    */
    inline bool hasBoundaryPoint(const std::deque<VertexID> & pts) const {
      for(std::deque<VertexID>::const_iterator p = pts.begin(); p != pts.end(); ++p)
	if (point(*p).getIsBoundary())
	  return true;
      return false;
    }

    /**
       Trace point
    */
    void tracePoint(VertexID id) const;

    /**
       return true if the current point is a saddle point, using
       the sum of angles arround it
    */
    bool isSaddlePoint(VertexID id, double epsilon = 0.) const;

    /**
       return true if edge is a boundary one
    */
    bool isBoundaryEdge(VertexID idP1, VertexID idP2) const;

    bool isBoundaryEdge(const PointOnEdge & p) const;

    /**
       return true if the two given points are in the same boundary
    */
    bool inSameBoundary(VertexID idP1, VertexID idP2) const;

    /**
       return true if the given triangle is a boundary one (i.e. if it has two boundary edges)
    */
    bool isBoundaryTriangle(TriangleID id) const;

    /**
       return true if path is a boundary cycle
    */
    bool isBoundaryCycle(const std::deque<VertexID> & path) const;

    /**
       return true if path is a non separating cycle
    */
    bool isNonSeparatingCycle(const std::deque<VertexID> & path);

    /**
       Init isBoundary flag for each point
    */
    void initIsBoundaryFlag(bool warning = true);

    /**
       Compute path length
    */
    double getLength(const std::deque<VertexID> & path) const;

    /**
       Compute path length
    */
    double getLength(const std::vector<VertexID> & path) const;

    /**
       Compute NLoop length
    */
    double getLength(const NLoop & nloop) const;

    /**
       Compute the sum of the root of the lengths
    */
    double getSqrLength(const NLoop & nloop) const;

    /**
       Compute the sum of the square root of the lengths
    */
    double getSqrtLength(const NLoop & nloop) const;

    /**
       Compute path length using length method
    */
    double getLength(const std::deque<VertexID> & path, LengthEdge & lEdge) const;

    /**
       return the point corresponding to the middle of the given path
     */
    VertexID getMiddlePoint(const std::deque<VertexID> & path) const;

    /**
       return the point corresponding to the middle of the given path
     */
    VertexID getMiddlePoint(const std::vector<VertexID> & path) const;


    /**
       compute the normal of the triangle given in parameter
    */
    Coord3D computeTriangleNormal(TriangleID tId) const;

    /**
       compute the normal of the point in parameter (mean of the triangles' normal)
    */
    Coord3D computePointNormal(VertexID pId) const;

    /**
       compute the mean of the length of the edges where pId is taking part in.
    */
    double getMeanEdges(VertexID pId) const;

    /**
       Compute edge length
    */
    double getLength(const MultiEdge & e) const;

    /**
       Return the mean point of the given edge.
       If edge has middle, return middle, else return mean point (using euclidiean distance)
    */
    Coord3D getMeanPoint(const MultiEdge & e) const;

    /**
       Given two points compute triangles containing this edge
    */
    std::pair<TriangleID, TriangleID> findTriangles(VertexID idP1, VertexID idP2) const;

    /**
       Given two points compute triangles containing this edge
    */
    std::pair<TriangleID, TriangleID> findTriangles(const Point3D & p1, const Point3D & p2) const;

    /**
       Given two triangles, compute the angle defined by the two planes
       @param idT1 First triangle
       @param idT2 Second triangle
       @return Angle value (in [0, PI]) or higher value if cannot be computed (boundary edge)
    */
    double computeAngle(TriangleID idT1, TriangleID idT2) const;


    /**
       Given two edges, compute the angle defined by these two edges
       @param firstId Id of the first point (in the two edges)
       @param nextId1 Id of the other point on the first edge
       @param nextId2 Id of the other point on the second edge
       @return angle value
    */
    double computeAngle(VertexID firstId, VertexID nextId1, VertexID nextId2) const;


    /**
       compute mean curvature of edges starting from a given vertex
       @param idP point
       @return Angle value (in [0, PI]) or higher value if cannot be computed (boundary edge)
    */
    double computeMeanLocalCurvature(VertexID idP) const;

    /**
       compute curvature given edge
       @param idP1 First point of the edge
       @param idP2 Second point of the edge
       @return Angle value (in [0, PI]) or higher value if cannot be computed (boundary edge)
    */
    double computeLocalCurvature(VertexID idP1, VertexID idP2) const;

    /**
       compute curvature given a multi edge
       @param m multiedge
       @return Angle value (in [0, PI]) or higher value if cannot be computed (boundary edge)
    */
    inline double computeLocalCurvature(const MultiEdge & m) const {
      if (m.hasMiddlePoint())
	return (computeLocalCurvature(m.getFirstId(), m.getMiddleId()) +
		computeLocalCurvature(m.getMiddleId(), m.getLastId())) / 2;
      else
	return computeLocalCurvature(m.getFirstId(), m.getLastId());
    }

    /**
       Order neighbours of a given point
    */
    bool orderNeighbours(VertexID i, bool warning);

    /**
       Reorient triangles and neighbours
    */
    void reorient();

    /**
       given a point, return number of neighbours in boundary edges
    */
    VertexID nbBoundaryEdges(VertexID idP) const;


    /**
       Build textured triangles for display (using 3d image)
    */
    void buildTexturedTriangleList(const CImg<unsigned char> & image, CImgList<VertexID> & primitives, CImgList<unsigned char> & textures, double zratio = 1);

    /**
       Add triangles given in parameter.
       If addInPoints is false, assumes that triangles are already defined in points.
    */
    template <typename T>
    void addTriangles(const T & tList, bool addInPoints = true);

    /**
       Remove all non real points (points not inside original structure)
    */
    VertexID removeNonRealPoints();

    /**
       Set all points as real points
    */
    void setAllRealPoints();

    /**
       Remove a given point and all the associated triangles.
       If \p setIsBoundaryPoints = false, the boundary flag of the neighbours is not modified
    */
    void removePoint(VertexID pointId, bool setIsBoundaryPoints = true);

    /**
       Remove the given points and all the associated triangles
    */
    void removePoints(const std::vector<VertexID> & rPoints);

    /**
       Remove a given triangle (but do not remove associated points)
       @param triangleId Id of the triangle to be removed
       @param simple If true, only remove the triangle without removing it from the points (do not preserve
       the mesh coherency)
    */
    void removeTriangle(TriangleID triangleId, bool simple = false);

    /** replace a point by another in a triangle, and update in consequence the triangle lists of the points
     */
    inline void replacePointFromTriangle(TriangleID triangle_l, VertexID oldV, VertexID newV) {
      triangles[triangle_l].replacePoint(oldV, newV);
      points[oldV].removeTriangle(triangle_l);
      points[newV].addTriangle(triangle_l);
    }

    /** return the list of non real points (that has been added at a previous step)  */
    std::deque<VertexID> getNonRealPoints() const;

    /**
       return the number of "single" points (i.e. without neighbour)
     */
    inline VertexID getNbSinglePoints() const {
      VertexID result = 0;
      for(const_point_iterator p = point_begin(); p != point_end(); ++p)
	if ((*p).getNbNeighbours() == 0)
	  ++result;
      return result;
    }

    /**
       Return number of boundary points
    */
    VertexID getNbBoundaryPoints() const;

    /**
       Return the boundary points
    */
    std::deque<VertexID> getBoundaryPoints() const;

    /**
       return a boundary point, or an exception if the surface is closed
    */
    VertexID getBoundaryPoint() const;

    /**
       return the squeleton paths: i.e. paths of points with only 2 adjacent points (except on the begining or end of the paths)
     */
    std::vector<std::vector<Coord3D> > getSqueletonPaths();

    /**
       Return the length of the boundary edges
    */
    double getBoundaryLength() const;


    /* Limitations of the genus and Euler caracteristic computation method: the meshpart has to be:
       - orientable
       - without strange boundary configurations
       - only composed of one connected component
    */

    /**
       Return the genus of the surface
    */
    unsigned int getGenus();

    /**
       return true if has boundary point
    */
    bool hasBoundaryPoint() const;

    /**
       Return the number of edges of the mesh
    */
    unsigned int getNbEdges() const;

    /**
       Return the Euler caracteristic of the surface
    */
    int getEulerCaracteristic() const;

    /**
       Return the second Betti number
    */
    int getBettiNumber();

    /**
       Return number of boundaries
    */
    unsigned int getNbBoundaries();

    /**
       return number of connected components
    */
    unsigned int getNbCC();

    /** return true if the mesh contains no point */
    inline bool isEmpty() const { return (getNbPoints() == 0); }

    /** return the area of the surface */
    double getArea() const;

    /** return the area of the given triangle */
    double getArea(TriangleID tid) const;

    /**
       Given a boundary point, find next boundary point
       Resulting point has at least 1 triangle.
    */
    VertexID getNextBPoint(VertexID p) const;

    /**
       Given a boundary point, find next boundary point not equal to oldP.
       Resulting point has at least 1 triangle.
    */
    VertexID getNextBPoint(VertexID p, VertexID oldP) const;

    /**
       Return number of neighbours that are next on boundary
    */
    VertexID getNbNextBPoint(VertexID p) const;

    /**
       Return the next point in boundary. Resulting point has no triangle.
    */
    VertexID getNextBPointSingle(VertexID p) const;

    /**
       Return the next point in boundary not equal to oldP.
       Resulting point has no triangle.
    */
    VertexID getNextBPointSingle(VertexID p, VertexID oldP) const;

    /**
       Return the next point in boundary going toward the given point \p direction
    */
    VertexID getNextBPointToward(VertexID point, VertexID direction) const;

    /**
       Return true if p1 has neighbour with same location of p2
    */
    bool hasNeighbourSameLocation(VertexID p1, VertexID p2) const;

    /**
       Given two points defining an edge, flip this edge (like in delaunay triangulation algorithm)
       If the given points are not neighbours, the function produces an Exception.
       @param p1 First point of the original edge
       @param p2 Second point of the original edge
       @return the new edge
    */
    std::pair<VertexID, VertexID> flipEdge(VertexID p1, VertexID p2);

    /**
       Given two points defining an edge, add a point in the edge, using
       location \in [0, 1] to adjust its location. Two triangles
       and two edges are also added to keep the surface coherence.
       @param p1 First point of the original edge
       @param p2 Second point of the original edge
       @return The id of the new point
    */
    VertexID addPointOnEdge(VertexID p1, VertexID p2, double location = .5);


    /** given a point located on an edge, it adds the corresponding point (if it's located on the inner part
	of the edge) and add the required triangles and edges. */
    VertexID addPointOnEdge(const PointOnEdge & point);

    /**
       Given two points defining an edge, merge it as a new point.
       Neighbours and triangles are updated to keep the surface coherence.
       Some triangles can also be removed (e.g. triangles containing the edge defined by
       the two given points).
       The location of the new created point is the middle of the two given points.
       @param p1 First point of the original edge
       @param p2 Second point of the original edge
       @return The id of the resulting point
    */
    VertexID mergePointsOnMiddle(VertexID p1, VertexID p2);


    /**
       Given two points defining an edge, merge it as a new point.
       Neighbours and triangles are updated to keep the surface coherence.
       Some triangles can also be removed (e.g. triangles containing the edge defined by
       the two given points).
       The location of the new created point is the location of the first given point.
       @param p1 First point of the original edge
       @param p2 Second point of the original edge
       @return The id of the resulting point
    */
    VertexID mergePointsOnFirst(VertexID p1, VertexID p2);

    /**
       Given two points defining an edge, merge it as a new point.
       Neighbours and triangles are updated to keep the surface coherence.
       Some triangles can also be removed (e.g. triangles containing the edge defined by
       the two given points).
       @param p1 First point of the original edge
       @param p2 Second point of the original edge
       @param newLocation Coordinates of the new created point
       @return The id of the resulting point
    */
    VertexID mergePoints(VertexID p1, VertexID p2, const Coord3D & newLocation);

    /**
       Given a point and two neighbour points, compute list of neighbours from the first one to the second one.
       Assums that neigbours of the main point are ordered
    */
    std::deque<VertexID> getNeighboursBetweenPoints(VertexID idPoint, VertexID idPoint1, VertexID idPoint2) const;

    /**
       Return iso-barycenter of the current mesh
    */
    Coord3D getIsobarycenter() const;

    /**
       Return iso-barycenter of the points given in parameter
    */
    Coord3D getIsobarycenter(const std::deque<VertexID> & pts) const;

    /**
       Return iso-barycenter of a triangle (may not be a real triangle of the mesh)
    */
    Coord3D getIsobarycenter(const VTriangle & t) const;

    /**
       Return barycenter of the points given in parameter using the given weights
    */
    Coord3D getBarycenter(const std::deque<VertexID> & pts, const std::deque<double> & weights) const;


    /**
       Return a string description of the box including the mesh
    */
    std::string getBoxSize() const;

    /**
       Return the maximum distance between two bounds following the axes
    */
    double getMaxDistanceXYZ() const;

    /**
       Return a box including the mesh
    */
    Box3D getBox() const;

    /**
       Compute the barycenter coords of th point idP using the points (idP1, idP2)
    */
    Coord2D computeBarycenterCoords(VertexID idP, VertexID idP1, VertexID idP2) const;

    /**
       Compute the barycenter coords of th point idP using the points (idP1, idP2, idP3)
       idP should be inside the triangle (idP1, idP2, idP3)
    */
    Coord3D computeBarycenterCoords(VertexID idP, VertexID idP1, VertexID idP2, VertexID idP3) const;

    /**
       Find point nearest to this barycenter
    */
    const Point3D & computePseudoCenter() const;

    /**
       Compute the "middle" point. If the mesh has no boundary, return the pseudo center (\see computePseudoCenter). Otherwise,
       compute the point of the mesh nearest to the boundary (using number of edges as length)
    */
    const Point3D & getMiddlePoint();

    /**
       Compute sorted ACP axis (main in first)
    */
    std::deque<Coord3D> computePCAAxes() const;

    /**
       Return true if the given point is inside the triangle described by the id
       given in parameter
    */
    bool isInside(const Coord3D & coord, TriangleID idT) const;


    /**
       Return the last (according to the line's direction) intersection point of the mesh and
       the given line. Return nbPoints if no intersection point has been found
    */
    VertexID getLastIntersection(const Line3D & line) const;


    /**
       Return the first and last (according to the line's direction) intersection point of the mesh and
       the given line. Return nbPoints if no intersection point has been found
    */
    std::pair<VertexID, VertexID> getFirstLastIntersection(const Line3D & line) const;

    /**
       Return true if the given point is inside the mesh
       Assums that the mesh as no boundary (defining a close surface)
    */
    bool isInside(const Coord3D & coord) const;


    /**
       Return some informations about the mesh
    */
    std::string getInfos();

    /**
       Build a string description of a given path
    */
    std::string getStringFromPath(const std::deque<VertexID> & path) const;

    /**
       Build a string description of a given path
    */
    std::string getStringFromPath(const std::vector<VertexID> & path) const;

    /**
       Build a string description of a given path (with flags of points)
    */
    std::string getStringFromPathWithFlags(const std::deque<VertexID> & path) const;

    /**
       Build a string description of a given NLoop
    */
    std::string getStringFromNLoop(const NLoop & nloop) const;


    /**
       Load Mesh from file
    */
    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Save Mesh to file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "", const std::string & objectName  = "") const;

    /**
       Save Mesh to file (VRML format)
    */
    void saveVRML(const std::string & fileName, const std::string & objectName) const;

    /**
       Save Mesh to file (OBJ format)
    */
    void saveOBJ(const std::string & fileName, const std::string & objectName) const;


    /**
       Save Mesh to file (OFF format)
    */
    void saveOFF(const std::string & fileName, const std::string & objectName) const;

    /**
       Save Mesh to file (PLY format)
    */
    void savePLY(const std::string & fileName, const std::string & objectName) const;

    /**
       Save Mesh to file (VTK format)
    */
    void saveVTK(const std::string & fileName, const std::string & objectName) const;

    /**
       Return a string description (in VRML format) of the current mesh)
    */
    std::string toStringVRML(const std::string & objectName, unsigned int id = 0) const;


    /**
       Load Mesh from file (VRML format)
    */
    void loadVRML(const std::string & fileName, const std::string & object);

    /**
       Load Mesh from file (ASC format, 3DStudio)
    */
    void loadASC(const std::string & fileName, const std::string & object);

    /**
       Load Mesh from file (OBJ format)
    */
    void loadOBJ(const std::string & fileName, const std::string & objectName);

    /**
       Load Mesh from file (PLY format)
    */
    void loadPLY(const std::string & fileName);

    /**
       Load Mesh from file (XYZ format)
    */
    void loadXYZ(const std::string & fileName);

    /**
       Load Mesh from file (VTK format)
    */
    void loadVTK(const std::string & fileName);

    /**
       Load Mesh from file (volumic image, like analyze or dicom). Using marching cubes method
       to compute the surface level set. The selected value is the mean between maximum and minimum value contained
       into the image.
    */
    void loadFromVolumicImage(const std::string & fileName, double ratio = 1.);

    /**
       Load Mesh from file (volumic image, like analyze or dicom). Using marching cubes method
       to compute the surface level set. The selected value is the mean between maximum and minimum value contained
       into the image.
    */
    void createFromVolumicImage(const CImg<> & image, double ratio = 1.0);

    /**
       Load Mesh from file (OFF format)
    */
    void loadOFF(const std::string & fileName);

    /**
       Load Mesh from a stream (VRML format)
    */
    bool loadNextMeshVRML(std::ifstream & infile, bool skipFirst = false);

    /**
       Compute a set of points that are an estimation of the extrema of the surface
       @param epsilon is the error allowed between two points to be considered as an extrema for
       the both sources. This is expained in percentage of the maximum distance.
    */
    std::deque<VertexID> findExtrema(double epsilon = 0.05);

    /**
       Return the length of the smallest edge of the current mesh
    */
    double getLengthSmallestEdge() const;

    /**
       Return the geodesic distance between the two further vertices (further according to the Euclidean distance)
       of the current mesh
    */
    double estimateMaxGeodesicDistance();

    /**
       A static method that create a square mesh of side size \p size
    */
    static Mesh createSquare(unsigned int size);


    /**
       Create a isosphere of radius \p radius, cendered on \p center, using a recursive subdivision approach from an initial icosahedron (CImg method).
    */
    static Mesh createSphere(double radius = 1., unsigned int resolution = 2, const Coord3D & center = Coord3D(0.0, 0.0, 0.0));


    /**
       Return true if the given edge takes part into the given path
       TODO: find a better location for this method.
    */
    static bool isInsideEdge(const std::deque<VertexID> & path, VertexID v1, VertexID v2);

    /** return true if the given vertex is part of a triangle with a flag value different of
	the given one.
    */
    bool hasTriangleDifferentFlag(VertexID v, int flag) const;

    /** return multipoints: points that correspond to "corner" of connected components
	i.e. locations used by more than two cc, or by exactly two connected components, but along a boundary
	(TODO: this second configuration is not yet implemented)
     */
    std::vector<VertexID> computeMultiPoints() const;

    /** return multipoints: points that correspond to "corner" of connected components
	i.e. locations used by more than two cc, or by exactly two connected components, but along a boundary
	(TODO: this second configuration is not yet implemented)
	Resulting points are stored in a map structure using the coordinates as index.
     */
    std::map<Coord3D, std::vector<VertexID>, Coord3D::ltCoord> computeMultiPointsByCoords() const;

    /**
       Given a 3D plane, it computes a set of polygons corresponding
       to the intersection between the current mesh and the given plane.
       if nbSlices != 0, the function also return intersections between
       the other parallel planes of the first one, using the direction given by the
       vector, and \p space as step.
     */
    std::vector<PLPath> getIntersection(const Plane3D & plane,
					unsigned int nbSlices = 0, double space = 1.);

    /**
       return intersections between the current mesh and the planes
       defined by the direction, and regularly spaced inside the object
       .5 | 1 | 1 | ... | 1 | 0.5
       @param direction orthogonal direction
       @param nbSlices Number of slices regularly located
       @return Resulting polygons
     */
    std::vector<PLPath> getIntersectionByNumber(const Coord3D & direction,
					unsigned int nbSlices);

    /**
       return intersections between the current mesh and the planes
       defined by the direction, and regularly spaced inside the object
       .5 | 1 | 1 | ... | 1 | 0.5
       @param direction orthogonal direction
       @param space Space betwen slices
       @return Resulting polygons
     */
    std::vector<PLPath> getIntersectionBySpace(const Coord3D & direction,
					       double space);

    /**
       given a direction, it compute distance between the two tangent
       planes orthogonal to the direction and the
       plane in isobarycenter */
    std::pair<double, double> getExtrema(const Coord3D & direction) const;

    /**
       given a direction, it compute distance between the two tangent
       planes orthogonal to the direction, and containing all the points
       of the mesh /M/ */
    inline double getSize(const Coord3D & direction) const {
      std::pair<double, double> e = getExtrema(direction);
      return e.second - e.first;
    }

    /**
       Assuming that the current mesh and the given one have the same structure, and that they only differ
       with the embedding into the 3D space, it computes a conformal similarity indicator, computing the sum
       of errors on the angles (absolute values, and using a ratio to normalize the sum of angles in the 1-star
       of a point). This value is only computed for points in the inner part of the structure (not angles on
       borders). The more the return value is close to 0, the more surfaces are conform.
     */
    double conformalSimilarity(const Mesh & mesh) const;
  };

  template <class L>
  void Mesh::addPoints(const L & pList, bool simple) {
    points.reserve(nbPoints + pList.size());

    for(typename L::const_iterator p = pList.begin(); p != pList.end(); ++p) {
      points.push_back(*p);
      if (simple)
	points.back().clearNeighboursAndTriangles();
      points.back().setId(nbPoints);
      ++nbPoints;
    }

  }

  /**
     Display some informations about the given mesh
   */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Mesh & mesh) {
    f << "Mesh(nbPoints: " << mesh.getNbPoints() << ", nbTriangles: "
      << mesh.getNbTriangles() << ", isobarycenter: " << mesh.getIsobarycenter() << ")";

    return f;
  }


  /**
     Given 2d image, build 3d mesh using grayvalue as Z componant
     for each pixel.
  */
  template <typename T>
  void Mesh::createFromImage(const CImg<T> & image) {

    if ((image._depth != 1) || (image._spectrum != 1))
      throw Exception("Mesh: Wrong image format.");


    /* build points and init isBoundary flag */
    points.clear();
    points.insert(points.begin(), image.size(), Point3D());
    cimg_forXY(image, x, y) {
      points[x + y * image._width] = Point3D(x, y, image(x, y), x + y * image._width);
      points[x + y * image._width].setIsBoundary(false);
    }
    for(unsigned int i = 0; i < image._width; ++i) {
      points[i].setIsBoundary(true);
      points[image.size() - i - 1].setIsBoundary(true);
    }
    for(unsigned int i = 0; i < image._height; ++i) {
      points[i * image._width].setIsBoundary(true);
      points[(i + 1) * image._width - 1].setIsBoundary(true);
    }


    /* build triangle list and add neighbours to each Point3D */
    triangles.clear();
    triangles.insert(triangles.begin(), (image._width - 1) * (image._height - 1) * 2, Triangle());
    for(unsigned int x = 0; x < image._width - 1; ++x)
      for(unsigned int y = 0; y < image._height - 1; ++y) {
	int i = x + y * image._width;
	int id = (x + y * (image._width - 1)) * 2;

	triangles[id] = Triangle(i, i + 1, i + 1 + image._width, id);

	points[triangles[id].getP1()].addTriangle(triangles[id].getP2(), triangles[id].getP3(), id);
	points[triangles[id].getP2()].addTriangle(triangles[id].getP1(), triangles[id].getP3(), id);
	points[triangles[id].getP3()].addTriangle(triangles[id].getP1(), triangles[id].getP2(), id);

	triangles[id + 1] = Triangle(i, i + 1 + image._width, i + image._width, id + 1);

	points[triangles[id + 1].getP1()].addTriangle(triangles[id + 1].getP2(), triangles[id + 1].getP3(), id + 1);
	points[triangles[id + 1].getP2()].addTriangle(triangles[id + 1].getP1(), triangles[id + 1].getP3(), id + 1);
	points[triangles[id + 1].getP3()].addTriangle(triangles[id + 1].getP1(), triangles[id + 1].getP2(), id + 1);
      }


    // order neighbours
    for(std::vector<Point3D>::iterator p = points.begin(); p != points.end(); ++p)
      (*p).orderNeighbours(triangles);


    std::cout << "Point number: " << nbPoints << std::endl;
    std::cout << "Triangle number: " << nbPoints << std::endl;

  }





  template <typename T>
  void Mesh::addTriangles(const T & tList, bool addInPoints) {
    triangles.reserve(nbTriangles + tList.size());

    // add triangles to the list and to their points
    for(class T::const_iterator t = tList.begin(); t != tList.end(); ++t) {
      // add triangle to the list
      triangles.push_back(*t);
      triangles.back().setId(nbTriangles);
      if (addInPoints)
	for(unsigned int i = 0; i < 3; ++i) {
	  points[(*t).getVertexId(i)].addTriangle((*t).getVertexId((i + 1) % 3), (*t).getVertexId((i + 2) % 3), triangles.back().getId());
	  points[(*t).getVertexId(i)].setFlag(0);
	}
      ++nbTriangles;
    }
    // update point properties
    initIsBoundaryFlag();
  }
}



#endif
