#include "MeshMap.h"

#ifndef PLYLOADER_H
#define PLYLOADER_H

#include "FileManipulator.h"

#include <string>
#include <map>
#include <vector>

#include "Coord3D.h"

namespace Taglut {
class Mesh;
class Mapping2D3D;
class Triangle;

class PLY {
protected:
    std::map<std::string, std::vector<double> > properties;
    std::vector<Coord3D> vertices;
    std::vector<Triangle> triangles;
    void reset();
};

class PLYLoader : public FileManipulator, public PLY {
private:
    bool loadPLY(std::ifstream & file);

    void addFormats();
public:
    PLYLoader();
    PLYLoader(const std::string & filename, const std::string & objectName = "");

    /** no save function */
    virtual void saveAltFormat(const std::string &, const std::string & = "",
                               const std::string &  = "") const {
    }

    virtual void loadAltFormat(const std::string & fileName, const std::string & altFormat = "", const std::string & object = "");

    /**
       Load Mesh from file (PLY format)
    */
    void loadPLY(const std::string & fileName);

    /** given a mesh, it modifies it according to the data of the PLY file */
    void loadMeshFromPLY(Mesh & mesh) const;

    /** given a mapping2D3D, it modifies it according to the data of the PLY file */
    void loadMapping2D3DFromPLY(Mapping2D3D & mapping) const;

    /** given a mesh map, it modifies it according to the data of the PLY file */
    void loadMeshMapFromPLY(MeshMap &mmap) const;

};

class PLYSaver : public FileManipulator, public PLY {
private:
    void initDataFromMesh(const Mesh & mesh);
    void initDataFromMapping(const Mapping2D3D & mapping);
    void initDataFromMap(const MeshMap & mmap);
    void savePLY(const std::string & fileName) const;
    void addFormats();
public:
    PLYSaver();

    /** no load function */
    virtual void loadAltFormat(const std::string &, const std::string & = "", const std::string & = "") {
    }

    /**
       Save Mesh to file
    */
    virtual void saveAltFormat(const std::string & fileName, const std::string & format = "",
                               const std::string & objectName  = "") const;
    /**
       Save Mesh to file (PLY format)
    */
    void savePLY(const Mesh & mesh, const std::string & fileName);

    /**
       Save Mesh and meshmap to file (PLY format)
    */
    void savePLY(const MeshMap & mmap, const std::string & fileName);

    /**
       Save Mesh and Mapping2D3D to file (PLY format)
    */
    void savePLY(const Mapping2D3D & mapping, const std::string & fileName);

    /**
       Save Mesh, Mapping2D3D and meshmap to file (PLY format)
    */
    void savePLY(const Mapping2D3D & mapping, const MeshMap & mmap, const std::string & fileName);

};

}

#endif // PLYLOADER_H
