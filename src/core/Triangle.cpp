/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Triangle.h"

using namespace Taglut;

std::pair<VertexID, VertexID> VTriangle::getCommonEdge(const VTriangle & t) const {
  VertexID firstPoint;
  VertexID p1 = getP1();
  VertexID p2 = getP2();
  VertexID p3 = getP3();


  if (t.hasPoint(p1))
    firstPoint = p1;
  else if (t.hasPoint(p2))
    firstPoint = p2;
  else if (t.hasPoint(p3))
    firstPoint = p3;
  else
    throw Exception("getCommonEdge(1): the given triangles are not neighbours.");

  if ((t.hasPoint(p1)) && (p1 != firstPoint))
    return std::pair<VertexID, VertexID>(firstPoint, p1);
  else if ((t.hasPoint(p2)) && (p2 != firstPoint))
    return std::pair<VertexID, VertexID>(firstPoint, p2);
  else if ((t.hasPoint(p3)) && (p3 != firstPoint))
    return std::pair<VertexID, VertexID>(firstPoint, p3);
  else
    throw Exception("getCommonEdge(1): the given triangles are not neighbours.");

}

VertexID VTriangle::getOtherPoint(VertexID p) const {
  if (getP1() != p)
    return getP1();
  if (getP2() != p)
    return getP2();
  return getP3();
}


VertexID VTriangle::getOtherPoint(VertexID p1, VertexID p2) const {
  if (! ((getP1() == p1) || (getP1() == p2)))
    return getP1();

  if (! ((getP2() == p1) || (getP2() == p2)))
    return getP2();

  return getP3();
}


Triangle::Triangle() {
  idT = 0;
  tFlag = 0;
}

Triangle::Triangle(const Triangle & t) : VTriangle(t) {
  (*this) = t;
}

Triangle::Triangle(const MultiTriangle & t) : VTriangle(t) {
  (*this) = t;
}

Triangle::Triangle(VertexID p1_t, VertexID p2_t, VertexID p3_t, TriangleID idT_t): VTriangle(idT_t), p1(p1_t), p2(p2_t), p3(p3_t), tFlag(0) { }

Triangle& Triangle::operator=(const Triangle & t) {
  p1 = t.getP1();
  p2 = t.getP2();
  p3 = t.getP3();
  idT = t.getId();
  tFlag = t.getFlag();

  return *this;
}

Triangle& Triangle::operator=(const MultiTriangle & t) {
  p1 = t.getP1();
  p2 = t.getP2();
  p3 = t.getP3();
  idT = t.getId();
  tFlag = 0;

  return *this;
}


void Triangle::incIds(int oPoints, int oTriangles) {
  idT += oTriangles;
  p1 += oPoints;
  p2 += oPoints;
  p3 += oPoints;
}

bool Triangle::sameOrder(const Vertex & v1, const Vertex & v2, const Vertex & v3) const {
  return (((p1 == v1.getId()) && (p2 == v2.getId()) && (p3 == v3.getId())) ||
	  ((p1 == v2.getId()) && (p2 == v3.getId()) && (p3 == v1.getId())) ||
	  ((p1 == v3.getId()) && (p2 == v1.getId()) && (p3 == v2.getId())));
}

bool Triangle::sameOrder(VertexID v1, VertexID v2, VertexID v3) const {
  return (((p1 == v1) && (p2 == v2) && (p3 == v3)) ||
	  ((p1 == v2) && (p2 == v3) && (p3 == v1)) ||
	  ((p1 == v3) && (p2 == v1) && (p3 == v2)));
}

bool Triangle::sameOrder(VertexID v1, VertexID v2) const {
  return (((p1 == v1) && (p2 == v2)) ||
	  ((p2 == v1) && (p3 == v2)) ||
	  ((p3 == v1) && (p1 == v2)));
}


bool Triangle::isEqual(VertexID idP1, VertexID idP2, VertexID idP3) const {
  if (((idP1 != p1) && (idP1 != p2) && (idP1 != p3)) ||
      ((idP2 != p1) && (idP2 != p2) && (idP2 != p3)) ||
      ((idP3 != p1) && (idP3 != p2) && (idP3 != p3)))
    return false;

  return true;
}



void Triangle::reorder(VertexID idP1, VertexID idP2) {
  assert(hasPoint(idP1) && hasPoint(idP2));
  if ((p1 != idP1) && (p1 != idP2))
    p3 = p1;
  else if ((p2 != idP1) && (p2 != idP2))
    p3 = p2;

  p1 = idP1;
  p2 = idP2;
}


void Triangle::setPoints(VertexID pp1, VertexID pp2, VertexID pp3) {
  p1 = pp1;
  p2 = pp2;
  p3 = pp3;
}

void Triangle::replacePoint(VertexID oldP, VertexID newP) {
  if (oldP == newP) return;

  if (p1 == oldP) {
    p1 = newP;
  }
  if (p2 == oldP) {
    p2 = newP;
  }
  if (p3 == oldP) {
    p3 = newP;
  }
}



MultiEdge::MultiEdge() : idFirst(0), idLast(0), idMiddle(0), hasMiddle(false) {

}


MultiEdge::MultiEdge(VertexID idFirst_t, VertexID idLast_t) : idFirst(idFirst_t), idLast(idLast_t), idMiddle(0), hasMiddle(false) {

}

MultiEdge::MultiEdge(VertexID idFirst_t, VertexID idLast_t, VertexID idMiddle_t) : idFirst(idFirst_t), idLast(idLast_t), idMiddle(idMiddle_t), hasMiddle(true) {

}
MultiEdge::MultiEdge(const MultiEdge & mEdge) : idFirst(mEdge.idFirst), idLast(mEdge.idLast), idMiddle(mEdge.idMiddle), hasMiddle(mEdge.hasMiddle) {

}

MultiTriangle::MultiTriangle(const MultiEdge & e1, const MultiEdge & e2, const MultiEdge & e3) {
  edges[0] = e1;
  edges[1] = e2;
  edges[2] = e3;
  assert(edges[0].getFirstId() == edges[1].getFirstId());
  assert(edges[0].getLastId() == edges[2].getFirstId());
  assert(edges[1].getLastId() == edges[2].getLastId());
  forbiddenTriangles[0] = -1;
  forbiddenTriangles[1] = -1;
  forbiddenTriangles[2] = -1;
}

MultiTriangle::MultiTriangle() {
  for(unsigned int i = 0; i < 3; ++i) // no iterator available
    forbiddenTriangles[i] = -1;
}

MultiTriangle::MultiTriangle(const MultiTriangle & t) : VTriangle(t) {
  for(unsigned int i = 0; i < 3; ++i) { // no iterator avalaible
    edges[i] = t.edges[i];
    forbiddenTriangles[i] = t.forbiddenTriangles[i];
  }
  triangles = t.triangles;
}

MultiTriangle::MultiTriangle(VertexID p1, VertexID p2, VertexID p3)  {
  edges[0].setId(p1, p2);
  edges[1].setId(p2, p3);
  edges[2].setId(p3, p1);
  for(unsigned int i = 0; i < 3; ++i)
    forbiddenTriangles[i] = -1;
}

void MultiTriangle::addBPoint(VertexID id1, VertexID id2, VertexID p) {
  for(unsigned int i = 0; i < 3; ++i) // no iterator available
    if (((edges[i].getFirstId() == id1) && (edges[i].getFirstId() == id2)) ||
	((edges[i].getFirstId() == id2) && (edges[i].getFirstId() == id1))) {
      edges[i].setMiddleId(p);
      return;
    }
    throw Exception("Corner points not found in MultiTriangle.");
}


void MultiTriangle::invertOrder() {
  edges[0] = edges[0].getInverted();
  MultiEdge m = edges[1];
  edges[1] = edges[2];
  edges[2] = m;
}


