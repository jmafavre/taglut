/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <J-Marie.Favreau@u-clermont1.fr>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <algorithm>

#include "Path3D.h"
#include "PLPath.h"
#include "StringManipulation.h"

using namespace Taglut;


Path3D::Path3D() { }

Path3D::Path3D(const std::vector<Coord3D> & p) : std::vector<Coord3D>(p) {
}

Path3D::Path3D(const PLPath & p) {
  for(PLPath::const_iterator pp = p.begin(); pp != p.end(); ++pp)
    push_back(*pp);
}

Path3D::Path3D(const Path3D & p) : std::vector<Coord3D>(p) {
}

Path3D & Path3D::operator=(const Path3D & path) {
  std::vector<Coord3D>::operator=(path);
  return *this;
}

bool Path3D::isPolygon() const {
  return (size() > 2) && front() == back();
}


bool Path3D::isPlanar(double epsilon) const {
  Plane3D plane = getPlanarEstimation();

  double max = 0.;
  for(Path3D::const_iterator p = begin(); p != end(); ++p) {
    const double l = plane.distance(*p);
    if (l > max)
      max = l;
  }
  return max < epsilon;
}


std::vector<Path3D> Path3D::loadPolygons(const std::string & filename) {
  std::vector<Path3D> result;
  std::vector<Coord3D> coords;
  std::ifstream infile(filename.c_str(), std::ios::in);

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  std::string firstLine;
  getline(infile, firstLine);
  if ((firstLine != "OFF") && (firstLine != "OFF\r"))
    throw ExceptionUnknownFormat();

  std::string buf;
  VertexID nbPt = 0;
  TriangleID nbPrimitives = 0;


  /* find definition of vertices in the header */
  while(!infile.eof()) {
    getline(infile, buf);
    if ((buf.size() != 0) && (StringManipulation::strip(buf)[0] != '\n') && (buf[0] != '#')) {
      std::deque<std::string> values = StringManipulation::splitString(StringManipulation::removeCR(buf), ' ');
      if (values.size() != 3)
	throw ExceptionErrorDuringReadingFile();
      if (StringManipulation::isUnsignedInt(values[0]) &&
	  StringManipulation::isUnsignedInt(values[1]) &&
	  StringManipulation::isUnsignedInt(values[2])) {
	nbPt = StringManipulation::getInt(values[0]);
	nbPrimitives = StringManipulation::getInt(values[1]);
      }
      else {
	throw ExceptionErrorDuringReadingFile();
      }
      break;
    }
  }

  // then read points
  for(VertexID i = 0; i < nbPt; ++i) {
    double x, y , z;
    infile >> x >> y >> z;
    coords.push_back(Coord3D(x, y, z));
  }

  // and read polygons
  bool start1 = false;
  std::vector<std::vector<VertexID> > ids;
  for(TriangleID i = 0; i < nbPrimitives; ++i) {
    VertexID nbP;
    infile >> nbP;

    VertexID pId;
    ids.push_back(std::vector<VertexID>());
    for(unsigned int j = 0; j != nbP; ++j) {
      infile >> pId;
      if (pId == nbPt)
	start1 = true;
      ids.back().push_back(pId);
    }
  }

  // then build polygons
  for(std::vector<std::vector<VertexID> >::const_iterator il = ids.begin(); il != ids.end(); ++il) {
    result.push_back(Path3D());
    result.back().push_back(coords[start1 ? (*il).back() - 1 : (*il).back()]);
    for(std::vector<VertexID>::const_iterator i = (*il).begin(); i != (*il).end(); ++i)
      result.back().push_back(coords[start1 ? *i - 1 : *i]);
  }


  return result;
}


bool Path3D::savePolygons(const std::vector<Path3D> & polygons, const std::string & filename, bool onlyPoints) {
  std::ofstream outfile(filename.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw Exception("savePolyhedra(): cannot open output file");

  // write the header
  outfile << "OFF" << std::endl;
  unsigned int nbPoints = 0;
  unsigned int nbPath = 0;
  for(std::vector<Path3D>::const_iterator poly = polygons.begin();
      poly != polygons.end(); ++poly) {
    if ((*poly).isPolygon())
      nbPoints += (*poly).size() - 1;
    else {
      nbPoints += (*poly).size();
      std::cout << "Warning: path #" << nbPath << " is not a loop" << std::endl;
    }
    ++nbPath;
  }
  unsigned int nbFaces = onlyPoints ? 0 : polygons.size();
  outfile << nbPoints << " " << nbFaces << " 0" << std::endl;

  // write the coordinates
  for(std::vector<Path3D>::const_iterator poly = polygons.begin();
      poly != polygons.end(); ++poly) {
    typename Path3D::const_iterator p = (*poly).begin();
    if ((*poly).isPolygon())
      ++p;
    for(; p != (*poly).end(); ++p) {
      outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;
    }
  }

  if (!onlyPoints) {
    // write the polyhedtra structure
    unsigned int id = 0;
    for(std::vector<Path3D>::const_iterator poly = polygons.begin();
	poly != polygons.end(); ++poly) {
      typename Path3D::const_iterator p = (*poly).begin();
      if ((*poly).isPolygon()) {
	outfile << ((*poly).size() - 1);
	++p;
      }
      else
	outfile << (*poly).size();

      for(; p != (*poly).end(); ++p, ++id)
	outfile << " " << id;
      outfile << std::endl;
    }
  }

  outfile.close();

  return true;
}

std::vector<Path3D> Path3D::convert(const std::vector<PLPath> & paths) {
  std::vector<Path3D> result;
  for(std::vector<PLPath>::const_iterator p = paths.begin(); p != paths.end(); ++p)
    result.push_back(*p);
  return result;
}

Path3D & Path3D::subdivise(double s) {
  if (s == 0.)
    throw Exception("subdivise(1): zero value.");
  if (size() == 0)
    return *this;

  Path3D result;

  result.push_back(front());
  Path3D::const_iterator pred = begin();
  for(Path3D::const_iterator p = begin() + 1; p != end(); ++p, ++pred) {
    const double d = (*pred).distance(*p);
    unsigned int nbsteps = floor(d / s);
    if (nbsteps != 0) {
      Coord3D pp(*pred);
      Coord3D v((*p - *pred).normalize() * d / nbsteps);
      for(unsigned int i = 0; i != nbsteps; ++i) {
	pp += v;
	result.push_back(pp);
      }
    }
    result.push_back(*p);
    assert(result.size() >= 2);
    assert(result.back().distance(result[result.size() - 2]) <= s);
  }
  *this = result;
  return *this;
}


std::vector<Path3D> Path3D::subdivise(const std::vector<Path3D> & paths, double size) {
  std::vector<Path3D> result;
  for(std::vector<Path3D>::const_iterator p = paths.begin(); p != paths.end(); ++p)
    result.push_back((*p).getSubdivise(size));
  return result;
}

Coord3D Path3D::getIsobarycenter() const {
  Coord3D result(0, 0, 0);

  if (size() == 0)
    return result;

  for(Path3D::const_iterator p = begin(); p != end(); ++p) {
    result += *p;
  }

  result /= size();

  return result;
}


Plane3D Path3D::getPlanarEstimation() const {
  std::deque<Coord3D> result;
  CImg<double> matrix(3, 3);
  CImg<double> val(3), vec(3,3);
  double value;
  Coord3D isoB = getIsobarycenter();

  if (size() < 3)
    throw Exception("getPlanarEstimation(): not enough points to have a planar description");

  for(unsigned int i = 0; i < 3; ++i)
    for(unsigned int j = 0; j <= i; ++j) {
      value = 0.0;
      for(Path3D::const_iterator p1 = begin(); p1 != end(); ++p1) {
	Coord3D p = (*p1) - isoB;
	value += (p[i] * p[j]) / size();
      }
      matrix(i, j) = value;
      if (i != j)
	matrix(j, i) = value;
    }


  matrix.symmetric_eigen(val, vec);

  Coord3D c1(vec(0, 0), vec(0, 1), vec(0, 2));
  Coord3D c2(vec(1, 0), vec(1, 1), vec(1, 2));

  if ((c1.norm() == 0.) || (c2.norm() == 0.))
    throw Exception("getPlanarEstimation(): aligned points");

  return Plane3D(isoB, Coord3D::vectorialProduct(c1, c2).normalize());
}


double Path3D::length() const {
  if (size() < 2)
    return 0.;

  double result = 0.;
  Path3D::const_iterator pred = begin();
  for(Path3D::const_iterator p = begin() + 1; p != end(); ++p, ++pred)
    result += (*pred).distance(*p);

  return result;
}
