/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2009 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal /
 *                     Queen Mary, University of London
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <limits>

#include "IDTypes.h"
#include "Coord3D.h"
#include "Coord2D.h"
#include "Point3D.h"
#include "FileManipulator.h"
#include "PointCloud.h"
#include "Exception.h"
#include "FileExceptions.h"
#include "StringManipulation.h"
#include "CImgUse.h"
#include "Mesh.h"
#include "RandomGenerator.h"
#include "PointCloudOctree.h"

using namespace Taglut;

PointCloud::~PointCloud() {
}

void PointCloud::addFormats() {
  addFormat("xyz");
  addFormat("ply");
  addLoadFormat("xyzn");
  addLoadFormat("wrl");
  addLoadFormat("vrml");
  addLoadFormat("obj");
  addLoadFormat("asc");
  addLoadFormat("vtk");
  addLoadFormat("off");
}

PointCloud::PointCloud() {
  addFormats();
}

PointCloud::PointCloud(const PointCloud & pCloud) : FileManipulator(pCloud), points(pCloud.points) {
  addFormats();
}

PointCloud::PointCloud(const std::string & fileName, const std::string & objectName) {
  addFormats();
  load(fileName, objectName);
}

PointCloud::PointCloud(const Mesh & mesh) {
  addFormats();
  *this = mesh;
}

PointCloud::PointCloud(const std::vector<Coord3D> & points_t) {
  addFormats();
  *this = points_t;
}


PointCloud::PointCloud(const char * fileName) {
  addFormats();
  if (fileName != NULL)
    load(fileName);
}
void PointCloud::loadAltFormat(const std::string & fileName, const std::string & altFormat, const std::string & object) {
  if (altFormat == "xyz")
    loadXYZ(fileName);
  else if (altFormat == "xyzn")
    loadXYZN(fileName);
  else if (altFormat == "ply")
    loadPLY(fileName);
  else if ((altFormat == "wrl") || (altFormat == "vrml") || (altFormat == "obj") || (altFormat == "asc") ||
	   (altFormat == "off") || (altFormat == "vtk")) {
    Mesh mesh;
    mesh.loadAltFormat(fileName, altFormat, object);
    *this = mesh;
  }
  else
    throw Exception("Unknown format");
}

void PointCloud::saveAltFormat(const std::string & fileName, const std::string & format, const std::string &) const {
  if (format == "xyzn")
    saveXYZN(fileName);
  else if (format == "xyz")
    saveXYZ(fileName);
  else if (format == "ply")
    savePLY(fileName);
  else
    throw Exception("Unknown format");
}

PointCloud & PointCloud::operator= (const Mesh & mesh) {
  clear();
  points.reserve(mesh.getNbPoints());
  for(Mesh::const_point_iterator p = mesh.point_begin(); p != mesh.point_end(); ++p)
    points.push_back(*p);
  return *this;
}

PointCloud & PointCloud::operator= (const std::vector<Coord3D> & points_t) {
  clear();
  (*this).points.reserve(points_t.size());
  for(std::vector<Coord3D>::const_iterator p = points_t.begin(); p != points_t.end(); ++p)
    (*this).points.push_back(Point3D((*p).getX(), (*p).getY(), (*p).getZ(), (*this).points.size()));
  return *this;
}

void PointCloud::loadXYZN(const std::string & fileName) {
  std::ifstream infile(fileName.c_str());
  std::string buf;
  unsigned char idCoord = 0;
  double coord[3];
  VertexID id = 0;
  bool coords = true;

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  while (!infile.eof()) {
    infile >> buf;
    if (buf == "#")
      getline(infile, buf);
    else {
      if (StringManipulation::isSignedFloat(buf)) {
	coord[idCoord] = StringManipulation::getFloat(buf);
	++idCoord;
	if (idCoord == 3) {
	  if (coords) {
	    points.push_back(Point3D(coord[0], coord[1], coord[2], id));
	    coords = false;
	  }
	  else {
	    normals.push_back(Coord3D(coord[0], coord[1], coord[2]));
	    coords = true;
	    ++id;
	  }
	  idCoord = 0;
	}
      }
    }
  }
}
void PointCloud::loadXYZ(const std::string & fileName) {
  std::ifstream infile(fileName.c_str());
  std::string buf;
  unsigned char idCoord = 0;
  double coord[3];
  VertexID id = 0;

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  while (!infile.eof()) {
    infile >> buf;
    if (buf == "#")
      getline(infile, buf);
    else {
      if (StringManipulation::isSignedFloat(buf)) {
	coord[idCoord] = StringManipulation::getFloat(buf);
	++idCoord;
	if (idCoord == 3) {
	  points.push_back(Point3D(coord[0], coord[1], coord[2], id));
	  idCoord = 0;
	  ++id;
	}
      }
    }
  }
}

void PointCloud::saveXYZ(const std::string & fileName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "# Generated by Taglut" << std::endl;
  for(const_iterator p = begin(); p != end(); ++p)
    outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << std::endl;
  outfile.close();
}

void PointCloud::saveXYZN(const std::string & fileName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);
  if (!hasNormals())
    throw Exception("saveXYZN(1): unknown normals.");

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "# Generated by Taglut" << std::endl;
  std::vector<Coord3D>::const_iterator n = normals.begin();
  for(const_iterator p = begin(); p != end(); ++p, ++n)
    outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ() << " " << (*n).getX() << " " << (*n).getY() << " " << (*n).getZ() << std::endl;
  outfile.close();
}

double PointCloud::getSmallestDistance() const {
  double result = std::numeric_limits<double>::max();

  for(const_iterator p1 = begin(); p1 != end(); ++p1)
    for(const_iterator p2 = p1 + 1; p2 != end(); ++p2)
      if ((*p1).distance(*p2) < result)
	result = (*p1).distance(*p2);

  return result;
}

PointCloud PointCloud::createSphere(double radius, unsigned int resolution, const Coord3D & center) {
  PointCloud result;
  CImgList<VertexID> primitives;
  CImg<double> pts = CImg<double>::sphere3d(primitives, radius, resolution);

  result.points.reserve(pts._width);
  cimg_forX(pts, p) {
    result.points.push_back(Point3D(pts(p, 0) + center.getX(), pts(p, 1) + center.getY(), pts(p, 2), p + center.getZ()));
  }

  return result;
}

PointCloud & PointCloud::prune(double ratio) {
  RandomGeneratorUniform rGenerator(0.0, size());
  VertexID nbRm = size() * ratio;
  if (nbRm <= 0)
    return *this;
  if (nbRm > size())
    nbRm = size();

  for(VertexID i = 0; i < nbRm; ++i) {
    VertexID id = floor(rGenerator());
    if (id == size()) id = size() - 1;
    for(std::vector<Point3D>::iterator p = points.erase(points.begin() + id); p != points.end(); ++p)
      (*p).incId(-1);
    rGenerator.setMax(size());
  }

  return *this;
}

void PointCloud::setNormal(VertexID id, const Coord3D & normal) {
  if (id >= points.size())
    throw Exception("setNormal(2): wrong ID.");
  if (id >= normals.size()) {
    normals.clear();
    normals.insert(normals.begin(), points.size(), Coord3D(0.0, 0.0, 0.0));
  }
  normals[id] = normal;

}


void PointCloud::savePLY(const std::string & fileName) const {
  std::ofstream outfile(fileName.c_str(), std::ios::out);

  if (!outfile.is_open())
    throw ExceptionCannotOpenOutputFile();

  outfile << "ply" << std::endl;
  outfile << "format ascii 1.0" << std::endl;
  outfile << "comment   Generated by savePLY" << std::endl;

  outfile << "element vertex " << points.size() << std::endl;
  outfile << "property double x" << std::endl;
  outfile << "property double y" << std::endl;
  outfile << "property double z" << std::endl;
  if (hasNormals()) {
    outfile << "property double nx" << std::endl;
    outfile << "property double ny" << std::endl;
    outfile << "property double nz" << std::endl;
  }
  outfile << "end_header" << std::endl;

  std::vector<Coord3D>::const_iterator n = normals.begin();
  for(PointCloud::const_iterator p = points.begin(); p != points.end(); ++p, ++n) {
    outfile << (*p).getX() << " " << (*p).getY() << " " << (*p).getZ();
    if (hasNormals())
      outfile << " " << (*n).getX() << " " << (*n).getY() << " " << (*n).getZ();
    outfile<< std::endl;
  }


  outfile.close();
}


void PointCloud::loadPLY(const std::string & fileName) {
  points.clear();
  normals.clear();

  std::ifstream infile(fileName.c_str());

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  if (!checkFormatPLY(infile))
    throw ExceptionUnknownFormat();

  if (!loadPLY(infile))
    throw ExceptionErrorDuringReadingFile();

  infile.close();

}


bool PointCloud::checkFormatPLY(std::ifstream & file) const {
  std::string firstLine, secondLine;
  getline(file, firstLine);
  getline(file, secondLine);
  return (((firstLine == "ply") || (firstLine == "ply\r")) &&
	  ((secondLine == "format ascii 1.0") || (secondLine == "format ascii 1.0\r")));
}


bool PointCloud::loadPLY(std::ifstream & file) {
  std::string buf;
  VertexID nbPt = 0;
  unsigned int nbProp = 0;


  /* find definition of vertices in the header */
  while(!file.eof()) {
    file >> buf;
    if (buf == "element") {
      file >> buf;
      if (buf != "vertex")
	return false;
      file >> nbPt;
      break;
    }
  }
  if (file.eof())
    return false;

  /* compute number of properties for points */
  while(!file.eof()) {
    file >> buf;
    if (buf == "property") { // the point properties are not really checked, assuming that the 3 first properties are the coordinates and the 3 next are the normals
      file >> buf;
      if (buf == "list")
	file >> buf;
      file >> buf;
      ++nbProp;
    }
    else
      break;
  }
  if (file.eof())
    return false;
  bool ns = nbProp >= 6;
  unsigned int nbPropReal = ns ? 6 : 3;

  // skip the end of the header
  while (buf != "end_header") {
    file >> buf;
    if (file.eof())
      return false;
  }

  // then read points
  for(VertexID i = 0; i < nbPt; ++i) {
    double x, y , z, nx, ny, nz;
    file >> x >> y >> z;
    if (ns)
      file >> nx >> ny >> nz;
    for (unsigned int s = 0; s < nbProp - nbPropReal; ++s)
      file >> buf; // skip the other point's informations
    points.push_back(Point3D(x, y, z, points.size()));
    normals.push_back(Coord3D(nx, ny, nz));
  }

  assert((normals.size() == points.size()) || (normals.size() == 0));

  return true;
}

double PointCloud::getHausdorffDistance(const PointCloud & pCloud) const {
  PointCloudOctree octree1(*this, 18, 8, false);
  PointCloudOctree octree2(pCloud, 18, 8, false);

  double max = 0.;
  for(PointCloud::const_iterator p = (*this).begin(); p != (*this).end(); ++p) {
    const double d = octree2.findClosestPoint(*p).second;
    if (d > max)
      max = d;
  }

  for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p) {
    const double d = octree1.findClosestPoint(*p).second;
    if (d > max)
      max = d;
  }

  return max;
}

double PointCloud::getSimilarity(const PointCloud & pCloud, double epsilon) const {
  PointCloudOctree octree(*this, 18, 8, false);

  VertexID nbSimilar = 0;
  for(PointCloud::const_iterator p = pCloud.begin(); p != pCloud.end(); ++p) {
    const double d = octree.findClosestPoint(*p).second;
    if (d <= epsilon)
      ++nbSimilar;
  }

  return ((double)nbSimilar) / pCloud.size();
}
