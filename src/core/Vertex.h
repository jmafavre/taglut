/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VERTEX
#define VERTEX

#include <vector>
#include <deque>
#include <assert.h>

#include "IDTypes.h"
#include "Exception.h"
#include "Node.h"


namespace Taglut {
  class Triangle;


  /**
     @class Vertex

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2007-05-31
     @brief Point of a mesh
  */
  class Vertex : public Node {
  protected:
    /**
       true if point is a boundary one
    */
    bool isBoundary;

    /** Triangle list*/
    std::deque<TriangleID> triangles;


  public:

    /**
       Default constructor
    */
    Vertex();

    /**
       Copy constructor
    */
    Vertex(const Vertex & v);

#ifndef SWIG
    /**
       Set operator
    */
    Vertex & operator=(const Vertex & v);
#endif

    /**
       Return true if current point is a boundary one
    */
    inline bool getIsBoundary() const { return isBoundary; }


    /**
       Return number of triangles
    */
    inline unsigned int getNbTriangles() const { return  triangles.size(); }


    /**
       Return triangles list
    */
    inline const std::deque<TriangleID> & getTriangles() const { return triangles; }
    /**
       Return triangles list
    */
    inline std::deque<TriangleID> & getTriangles() { return triangles; }

    /**
       Clear neighbours and triangles (clear list)
    */
    void clearNeighboursAndTriangles();



    /**
       Set boundary state
    */
    inline void setIsBoundary(bool ib) { isBoundary = ib; }


    /**
       Increase ids using given offsets
    */
    void incIds(VertexID oPoints, TriangleID oTriangles);


    /* neighbours tools */


    /**
       Add a neighbour to current point. A point cannot be more that one time the neighbour of another point
    */
    bool addNeighbour(VertexID neighbour);

    /**
       Add a neighbour to current point. A point cannot be more that one time the neighbour of another point
    */
    bool addNeighbour(const Vertex & neighbour);

    /**
       Add a neighbour to current point, and the current point as neighbour for the new one.
       A point cannot be more that one time the neighbour of another point
    */
    inline bool addEdge(Vertex & nb) {
      return addNeighbour(nb) && nb.addNeighbour(*this);
    }

    /**
       Add a given point between two other neighbours
    */
    void addNeighbourBetweenPoints(VertexID neighbour, VertexID idN1, VertexID idN2);


    /**
       Change triangle
    */
    void replaceTriangle(TriangleID oldTriangle, TriangleID newTriangle, bool expt = false);



    /**
       remove triangle defined by id
    */
    void removeTriangle(TriangleID t, bool expt = false);



    /**
       Return true if the triangle t is current a neighbour of the current point
    */
    bool hasTriangle(TriangleID t) const ;

    /**
       Order neighbours using triangle list (topological order)
    */
    bool orderNeighbours(const std::vector<Triangle> & trianglesList, bool warning = true);

    /**
       Order neighbours using topological order
    */
    template <typename T>
    bool orderNeighbours(std::deque<T> & trianglesList, bool warning = true);


    /* triangle tools */
    /**
       Add a triangle to the current point
    */
    void addTriangle(const VertexID idP1, const VertexID idP2,
		     const TriangleID idT);

    /**
       Add a triangle to the current point
    */
    void addTriangle(const Triangle & t);

    /**
       Add a triangle to the current point
    */
    void addTriangle(const TriangleID idT);


    /**
       Given two points, find next point using ordered neighbour list
       @param idN1 First point
       @param idN2 Second point
       @return Third point
    */
    VertexID findNextNeighbour(const VertexID idN1, const VertexID idN2) const;

    /**
       Given a point, compute the vertex at the other side of the neighbour "disc"
    */
    VertexID getNeighbourOtherSide(VertexID & v) const;


    /**
       Inverse neighbour list
    */
    void inverseNeighbourList();

    /**
       Given a point and the list of triangles, return true if the current point
       and the given point are points of a boundary edge
    */
    template <typename T>
    bool isPartOfBoundaryEdge(VertexID v, const std::deque<T> & tList) const;

    /**
       Given an edge defined by two points, compute other point in the same side
       of the edge that the given third point.
       @param p1 first point
       @param p2 second point
       @param p3 third point
    */
    VertexID getPointSameSide(VertexID p1, VertexID p2, VertexID p3) const;

    /**
       return the next iterator on neighbours
    */
    inline std::deque<VertexID>::const_iterator nextNeighbourIt(const std::deque<VertexID>::const_iterator & it) const {
      std::deque<VertexID>::const_iterator result = it;
      ++result;
      if (result == neighbours.end())
	return neighbours.begin();
      else
	return result;
    }

    /**
       return the next reverse iterator on neighbours
    */
    inline std::deque<VertexID>::const_reverse_iterator nextNeighbourRIt(const std::deque<VertexID>::const_reverse_iterator & it) const {
      std::deque<VertexID>::const_reverse_iterator result = it;
      ++result;
      if (result == neighbours.rend())
	return neighbours.rbegin();
      else
	return result;
    }

    /**
       Set neighbours, triangles and isBoundary flag according to v's values
    */
    inline void setNeighgoursAndTriangles(const Vertex & v) {
      neighbours = v.neighbours;
      triangles = v.triangles;
      isBoundary = v.isBoundary;
    }

    /** return true if the given vertices are neighbours of *this,
	and if they are ordered as given (v1, v2), i.e. v2 is the next neigbour after v1 in the (cyclic) ordering
    */
    bool neighboursValidOrder(VertexID v1, VertexID v2) const;

  };

  template <typename T>
  bool Vertex::isPartOfBoundaryEdge(VertexID v, const std::deque<T> & tList) const {
    bool inside = false;

    for(std::deque<TriangleID>::const_iterator i = triangles.begin(); i != triangles.end(); ++i)
      if (tList[*i].hasPoint(v)) {
	if (inside) // another neighbour triangle has been found inside
	  return false;
	else
	  inside = true;
      }

    return inside;
  }


  /**
     Stream operator
  */
  template <typename T, typename S>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Vertex & p) {
    f << "Vertex(" << p.getId() << "; isBoundary: " << p.getIsBoundary() << "; nb neighbours: " << p.getNbNeighbours() << ")";
    return f;
  }



  template <typename T>
  bool Vertex::orderNeighbours(std::deque<T> & trianglesList, bool warning) {
    std::deque<VertexID> sortedNeighbours;
    bool brk = false;
    bool invert = false;

    /* order neighbours allows to compute more easily angles.
       Direct or indirect order is not important */

    if (neighbours.size() <= 1)
      return true;

    sortedNeighbours.push_back(neighbours.back());


    bool * seenTriangles = new bool[triangles.size()];
    for(TriangleID i = 0; i < triangles.size(); ++i)
      seenTriangles[i] = false;


    while(neighbours.size() != sortedNeighbours.size()) {
      /* find sortedNeighbours.back() neighbour in neighbours */
      VertexID c = sortedNeighbours.back();
      brk = false;
      TriangleID it = 0;
      for(std::deque<TriangleID>::const_iterator i = triangles.begin(); i != triangles.end(); ++i) {
	if (trianglesList[*i].hasPoint(c) && (!seenTriangles[it])) {
	  assert(*i == trianglesList[*i].getId());
	  seenTriangles[it] = true;
	  sortedNeighbours.push_back(trianglesList[*i].getOtherPoint(c, getId()));
	  brk = true;
	  break;
	}
	++it;
      }
      if (!brk) {
	if (invert) {
	  if (warning) {
	    std::cout << "Warning: neighbours cannot be ordered (" << getId() << ")" << std::endl;
	    std::cout <<   " neighbours: ";
	    for(std::deque<VertexID>::const_iterator i = getNeighbours().begin(); i != getNeighbours().end(); ++i)
	      std::cout << " " << *i;
	    std::cout <<  std::endl << " triangles: ";
	    for(std::deque<TriangleID>::const_iterator i = getTriangles().begin(); i != getTriangles().end(); ++i)
	      std::cout << " " << *i;
	    std::cout <<  std::endl;
	  }
	  return false; // neighbours couldn't be ordered
	}
	else {
	  invert = true;
	  std::deque<VertexID> sortedNeighboursBis;
	  for(std::deque<VertexID>::const_iterator j = sortedNeighbours.begin(); j != sortedNeighbours.end(); ++j)
	    sortedNeighboursBis.push_front(*j);
	  sortedNeighbours = sortedNeighboursBis;
	}
      }
    }

    delete [] seenTriangles;
    neighbours = sortedNeighbours;
    return true;

  }


}



#endif
