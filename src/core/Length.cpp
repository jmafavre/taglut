/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Messages.h"
#include "Mesh.h"
#include "Mapping2D3D.h"
#include "MeshMap.h"
#include "Length.h"
#include "MeshManipulator.h"
#include "WeightedEdges.h"

using namespace Taglut;

LengthEdge::LengthEdge(const Mesh & mesh_l) : mesh(mesh_l) {}

LengthEdge::~LengthEdge() {}

const Mesh & LengthEdge::getMesh() const { return mesh; }

LengthEdge * LengthEdge::cloneMeshMap(const MeshMap & meshMap) const {
  return clone(meshMap.getMesh());
}


LengthEdgeMergeBoundary::LengthEdgeMergeBoundary(const LengthEdge & lE_l) : LengthEdge(lE_l.getMesh()), lE(lE_l) {}

LengthEdgeMergeBoundary::LengthEdgeMergeBoundary(const LengthEdge & lE_l, const Mesh & m) : LengthEdge(m), lE(lE_l) {}

double LengthEdgeMergeBoundary::operator()(const VertexID p1, const VertexID p2) const {
  if (mesh.isBoundaryEdge(p1, p2))
    return 0;
  else
    return lE(p1,p2);
}

LengthEdge * LengthEdgeMergeBoundary::clone() const {
  return new LengthEdgeMergeBoundary(lE);
}

LengthEdge * LengthEdgeMergeBoundary::clone(const Mesh & m) const {
  return new LengthEdgeMergeBoundary(lE, m);
}

LengthEdgeEuclidean::LengthEdgeEuclidean(const Mesh & mesh_l) : LengthEdge(mesh_l) {}

double LengthEdgeEuclidean::operator()(const VertexID p1, const VertexID p2) const {
  return mesh.point(p1).distance(mesh.point(p2));
}

LengthEdge * LengthEdgeEuclidean::clone() const {
  return new LengthEdgeEuclidean(mesh);
}

LengthEdge * LengthEdgeEuclidean::clone(const Mesh & m) const {
  return new LengthEdgeEuclidean(m);
}

LengthEdgeCurvature::LengthEdgeCurvature(const Mesh & mesh_l, double ratio_l) : LengthEdge(mesh_l), lEdge(mesh_l), ratio(ratio_l) {
  if (ratio < 0) ratio = 0;
  if (ratio > 1) ratio = 1;
}

double LengthEdgeCurvature::operator()(const VertexID p1, const VertexID p2) const {
  double l = lEdge(p1, p2);
  double angle = mesh.computeLocalCurvature(p1, p2);
  if (angle > M_PI)
    return l;
  else {
    l *= 1 - ratio * f(angle);
    if (l < 0.0)
      return 0.0;
    else
      return l;
  }
}

LengthEdgeLinearCurvature::LengthEdgeLinearCurvature(const Mesh & mesh_l) : LengthEdgeCurvature(mesh_l, 0.6) { }

LengthEdgeLinearCurvature::LengthEdgeLinearCurvature(const Mesh & mesh_l, double ratio_l) : LengthEdgeCurvature(mesh_l, ratio_l) { }

LengthEdge * LengthEdgeLinearCurvature::clone() const {
  return new LengthEdgeLinearCurvature(mesh, ratio);
}


LengthEdge * LengthEdgeLinearCurvature::clone(const Mesh & m) const {
  return new LengthEdgeLinearCurvature(m, ratio);
}

double LengthEdgeLinearCurvature::f(double angle) const {
  if ((angle <= 0) || (angle >= 2 * M_PI))
    return 0;
  else if (angle < M_PI)
    return angle / M_PI;
  else
    return 2 * M_PI - angle / M_PI;
}



LengthEdgeCosineCurvature::LengthEdgeCosineCurvature(const Mesh & mesh_l) : LengthEdgeCurvature(mesh_l, 0.6) { }
  /**
     Constructor with mesh and ratio value
  */
LengthEdgeCosineCurvature::LengthEdgeCosineCurvature(const Mesh & mesh_l, double ratio_l) : LengthEdgeCurvature(mesh_l, ratio_l) { }

LengthEdge * LengthEdgeCosineCurvature::clone() const {
  return new LengthEdgeCosineCurvature(mesh, ratio);
}

LengthEdge * LengthEdgeCosineCurvature::clone(const Mesh & m) const {
  return new LengthEdgeCosineCurvature(m, ratio);
}

double LengthEdgeCosineCurvature::f(double angle) const {
  if ((angle <= 0) || (angle >= 2 * M_PI))
    return 0;
  else return (1 - cos(angle)) / 2;
}



LengthEdgeSineCurvature::LengthEdgeSineCurvature(const Mesh & mesh_l) : LengthEdgeCurvature(mesh_l, 0.6) { }

LengthEdgeSineCurvature::LengthEdgeSineCurvature(const Mesh & mesh_l, double ratio_l) : LengthEdgeCurvature(mesh_l, ratio_l) { }

LengthEdge * LengthEdgeSineCurvature::clone() const {
  return new LengthEdgeSineCurvature(mesh, ratio);
}

LengthEdge * LengthEdgeSineCurvature::clone(const Mesh & m) const {
  return new LengthEdgeSineCurvature(m, ratio);
}

double LengthEdgeSineCurvature::f(double angle) const {
  if ((angle <= 0) || (angle >= 2 * M_PI))
    return 0;
  else return sin(angle / 2);
}


LengthEdgeInverseCurvature::LengthEdgeInverseCurvature(const Mesh & mesh_l) : LengthEdge(mesh_l) { }

LengthEdge * LengthEdgeInverseCurvature::clone() const {
  return new LengthEdgeInverseCurvature(mesh);
}


LengthEdge * LengthEdgeInverseCurvature::clone(const Mesh & m) const {
  return new LengthEdgeInverseCurvature(m);
}

double LengthEdgeInverseCurvature::operator()(const VertexID p1, const VertexID p2) const {
  return f(mesh.computeLocalCurvature(p1, p2));
}

double LengthEdgeInverseCurvature::f(double angle) const {
  if ((angle <= 0) || (angle >= 2 * M_PI))
    return std::numeric_limits<double>::max();
  else if (angle < M_PI)
    return 1. / angle;
  else // angle > M_PI
    return 2 * M_PI - 1. / angle;
}

LengthEdgeMappingRatio::LengthEdgeMappingRatio(const Mapping2D3D & mapping_l, const Mesh & mesh_l) : LengthEdge(mesh_l), mapping(mapping_l) { }

LengthEdgeMappingRatio::LengthEdgeMappingRatio(const Mapping2D3D & mapping_l) : LengthEdge(mapping_l.getMesh()), mapping(mapping_l) { }

LengthEdge * LengthEdgeMappingRatio::clone() const {
  return new LengthEdgeMappingRatio(mapping);
}


LengthEdge * LengthEdgeMappingRatio::clone(const Mesh & m) const {
  if (m.getNbPoints() == mapping.size())
    return new LengthEdgeMappingRatio(mapping, m);
  else
    throw Exception("Wrong mesh size.");
}

double LengthEdgeMappingRatio::operator()(const VertexID p1, const VertexID p2) const {
  return mesh.point(p1).distance(mesh.point(p2)) / mapping[p1].distance2D(mapping[p2]);
}

LengthEdgeMappingInverseRatio::LengthEdgeMappingInverseRatio(const Mapping2D3D & mapping_l, const Mesh & mesh_l) : LengthEdge(mesh_l),
														   mapping(mapping_l) { }

LengthEdgeMappingInverseRatio::LengthEdgeMappingInverseRatio(const Mapping2D3D & mapping_l) : LengthEdge(mapping_l.getMesh()), mapping(mapping_l) { }

LengthEdge * LengthEdgeMappingInverseRatio::clone() const {
  return new LengthEdgeMappingInverseRatio(mapping);
}


LengthEdge * LengthEdgeMappingInverseRatio::clone(const Mesh & m) const {
  if (m.getNbPoints() == mapping.size())
    return new LengthEdgeMappingInverseRatio(mapping, m);
  else
    throw Exception("Wrong mesh size.");
}

double LengthEdgeMappingInverseRatio::operator()(const VertexID p1, const VertexID p2) const {
  return mapping[p1].distance2D(mapping[p2]) / mesh.point(p1).distance(mesh.point(p2));
}


LengthEdgeEuclidean2D::LengthEdgeEuclidean2D(const Mapping2D3D & mapping_l, const Mesh & mesh_l) : LengthEdge(mesh_l), mapping(mapping_l) { }

LengthEdgeEuclidean2D::LengthEdgeEuclidean2D(const Mapping2D3D & mapping_l) : LengthEdge(mapping_l.getMesh()), mapping(mapping_l) { }

LengthEdge * LengthEdgeEuclidean2D::clone() const {
  return new LengthEdgeEuclidean2D(mapping);
}


LengthEdge * LengthEdgeEuclidean2D::clone(const Mesh & m) const {
  if (m.getNbPoints() == mapping.size())
    return new LengthEdgeEuclidean2D(mapping, m);
  else
    throw Exception("Wrong mesh size.");
}

double LengthEdgeEuclidean2D::operator()(const VertexID p1, const VertexID p2) const {
  return mapping[p1].distance2D(mapping[p2]);
}

/*
  The Length factory definitions
*/

LengthFactory::LengthFactory() {
  length = "";
  lEdge = NULL;
  ratio = -1;
}

LengthFactory::LengthFactory(const std::string & length_l) : length(length_l) {
  lEdge = NULL;
  ratio = -1;
}

LengthFactory::~LengthFactory() {
  if (lEdge != NULL)
    delete lEdge;
}

void LengthFactory::setLength(const std::string & length_l) {
  (*this).length = length_l;
  if (lEdge != NULL) {
    delete lEdge;
    lEdge = NULL;
  }
}

void LengthFactory::setRatio(double ratio_l) {
  (*this).ratio = ratio_l;
}

bool LengthFactory::existsLength() const {
  if ((length ==  "euclidean") || (length == "") ||
      (length == "linear-curvature") || (length == "lc") ||
      (length == "cosine-curvature") || (length == "cc") ||
      (length == "sine-curvature") || (length == "sc") ||
      (length == "no-boundary") || (length == "scalar-map") ||
      (length == "lfs-map") || (length == "scalar-map"))
    return true;
  else
    return false;
}
bool LengthFactory::needMeshMap() const {
  if ((length == "lfs-map") || (length == "scalar-map"))
    return true;
  else
    return false;
}


const LengthEdge & LengthFactory::getLength(const Mesh & m) {
  if (lEdge == NULL) {
    if ((length == "euclidean") || (length == ""))
      lEdge = new LengthEdgeEuclidean(m);
    else if ((length == "linear-curvature") || (length == "lc"))
      if (ratio >= 0)
	lEdge = new LengthEdgeLinearCurvature(m, ratio);
      else
	lEdge = new LengthEdgeLinearCurvature(m);
    else if ((length == "cosine-curvature") || (length == "cc"))
      if (ratio >= 0)
	lEdge = new LengthEdgeCosineCurvature(m, ratio);
      else
	lEdge = new LengthEdgeCosineCurvature(m);
    else if ((length == "sine-curvature") || (length == "sc"))
      if (ratio >= 0)
	lEdge = new LengthEdgeSineCurvature(m, ratio);
      else
	lEdge = new LengthEdgeSineCurvature(m);
    else if ((length == "inverse-curvature") || (length == "ic"))
      lEdge = new LengthEdgeInverseCurvature(m);
    else if (length == "no-boundary")
      lEdge = new LengthEdgeNoBoundary(m);
    else if (length == "lfs-map")
      throw Exception(std::string("This length need a MeshMap to be defined."));
    else
      throw Exception(std::string("Length not found"));
  }
  return *lEdge;

}

const LengthEdge & LengthFactory::getLength(const MeshMap & mMap_l) {
  if (lEdge == NULL) {
    if (length == "lfs-map")
      lEdge = new LengthLocalFeatureSize(mMap_l);
    if (length == "scalar-map")
      lEdge = new LengthMeshMap(mMap_l);
    else
      return getLength(mMap_l.getMesh());
  }
  return *lEdge;
}

std::string LengthFactory::toString() const {
  Message result;
  result <<  "Available lengths:";
  result <<  "- \"euclidean\" (default value): use euclidean distance;";
  result <<  "- \"linear-curvature\" (abbr. \"lc\"): use euclidean distance modified according to the curvature with a linear function. Affected by ratio parameter;";
  result <<  "- \"cosine-curvature\" (abbr. \"cc\"): use euclidean distance modified according to the curvature with a cosine-based function (f(a) = (1 - cos(a)) / 2). Affected by ratio parameter;";
  result <<  "- \"sine-curvature\" (abbr. \"sc\"): use euclidean distance modified according to the curvature with a sine-based function (f(a) = sin(a / 2)). Affected by ratio parameter.";
  result <<  "- \"inverse-curvature\" (abbr. \"lc\"): use euclidean distance modified according to the curvature with an inverse function. Affected by ratio parameter;";
  result <<  "- \"no-boundary\": boundary edges are longer than the diameter of the surface;";
  result <<  "- \"scalar-map\": use a scalar function (stored int a MeshMap) to modify the distance. Warning: only available with a well defined MeshMap;";
  result <<  "- \"lfs-map\": use local feature sizes (stored int a MeshMap) to estimate the area of the triangles introduced by a patch process. Warning: only available with a well defined MeshMap;";
  return result.toString();
}


LengthMeshMap::LengthMeshMap(const MeshMap & mMap_l) : LengthEdge(mMap_l.getMesh()), mMap(mMap_l) {
}



LengthEdge * LengthMeshMap::clone() const {
  return new LengthMeshMap(mMap);
}

LengthEdge * LengthMeshMap::cloneMeshMap(const MeshMap & mMap_l) const {
  return new LengthMeshMap(mMap_l);
}

double LengthMeshMap::operator()(const VertexID p1, const VertexID p2) const {
  return mesh.point(p1).distance(mesh.point(p2)) * mMap[p1] * mMap[p2];
}


LengthMeshMapMean::LengthMeshMapMean(const MeshMap & mMap_l) : LengthEdge(mMap_l.getMesh()), mMap(mMap_l) {
}


LengthEdge * LengthMeshMapMean::clone() const {
  return new LengthMeshMapMean(mMap);
}

LengthEdge * LengthMeshMapMean::cloneMeshMap(const MeshMap & mMap_l) const {
  return new LengthMeshMapMean(mMap_l);
}

double LengthMeshMapMean::operator()(const VertexID p1, const VertexID p2) const {
  assert(p1 < mMap.getNbValues());
  assert(p2 < mMap.getNbValues());
  assert(p1 < mesh.getNbPoints());
  assert(p2 < mesh.getNbPoints());
  return .5 * mesh.point(p1).distance(mesh.point(p2)) * (mMap[p1] + mMap[p2]);
}

LengthMeshMapGradient::LengthMeshMapGradient(const MeshMap & mMap_l) : LengthEdge(mMap_l.getMesh()), mMap(mMap_l),
								       epsilon(.5 * mMap_l.getMinimumDifferenceOnEdge()) {
}

LengthMeshMapGradient::LengthMeshMapGradient(const MeshMap & mMap_l, double epsilon_l) : LengthEdge(mMap_l.getMesh()),
											 mMap(mMap_l), epsilon(epsilon_l) {
}

LengthEdge * LengthMeshMapGradient::clone() const {
  return new LengthMeshMapGradient(mMap, epsilon);
}

LengthEdge * LengthMeshMapGradient::cloneMeshMap(const MeshMap & mMap_l) const {
  return new LengthMeshMapGradient(mMap_l);
}

double LengthMeshMapGradient::operator()(const VertexID p1, const VertexID p2) const {
  assert(p1 < mMap.getNbValues());
  assert(p2 < mMap.getNbValues());
  assert(p1 < mesh.getNbPoints());
  assert(p2 < mesh.getNbPoints());
  if (mMap[p1] != mMap[p2])
    return mesh.point(p1).distance(mesh.point(p2)) / fabs(mMap[p1] - mMap[p2]);
  else
    return mesh.point(p1).distance(mesh.point(p2)) / epsilon;
}

LengthMeshMapSignedGradient::LengthMeshMapSignedGradient(const MeshMap & mMap_l,
							 double startValue_l, bool direction_l) : LengthEdge(mMap_l.getMesh()), mMap(mMap_l),
												  epsilon(.5 * mMap_l.getMinimumDifferenceOnEdge()),
												  startValue(startValue_l),
												  direction(direction_l),
												  maxDistance(1024 * mMap_l.getMesh().getMaxDistanceXYZ() / epsilon) {
}

LengthMeshMapSignedGradient::LengthMeshMapSignedGradient(const MeshMap & mMap_l, double startValue_l, bool direction_l,
							 double epsilon_l, double maxDistance_l) : LengthEdge(mMap_l.getMesh()), mMap(mMap_l),
												   epsilon(epsilon_l),
												   startValue(startValue_l),
												   direction(direction_l),
												   maxDistance(maxDistance_l < 0 ? mMap_l.getMesh().getMaxDistanceXYZ() : maxDistance_l) {
}

LengthEdge * LengthMeshMapSignedGradient::clone() const {
  return new LengthMeshMapSignedGradient(mMap, startValue, direction, epsilon, maxDistance);
}

LengthEdge * LengthMeshMapSignedGradient::cloneMeshMap(const MeshMap & mMap_l) const {
  return new LengthMeshMapSignedGradient(mMap_l, startValue, direction);
}

double LengthMeshMapSignedGradient::operator()(const VertexID p1, const VertexID p2) const {
  assert(p1 < mMap.getNbValues());
  assert(p2 < mMap.getNbValues());
  assert(p1 < mesh.getNbPoints());
  assert(p2 < mesh.getNbPoints());
  if ((direction && (mMap[p1] >= startValue) && (mMap[p2] >= startValue)) ||
      ((!direction) && (mMap[p1] <= startValue) && (mMap[p2] <= startValue))) {
    if (mMap[p1] != mMap[p2])
      return mesh.point(p1).distance(mesh.point(p2)) / fabs(mMap[p1] - mMap[p2]);
    else
      return mesh.point(p1).distance(mesh.point(p2)) / epsilon;
  }
  else {
    return maxDistance;
  }
}

LengthLocalFeatureSize::LengthLocalFeatureSize(const MeshMap & mMap_l) : LengthMeshMap(mMap_l) {
}

LengthEdge * LengthLocalFeatureSize::clone() const {
  return new LengthLocalFeatureSize(mMap);
}

LengthEdge * LengthLocalFeatureSize::cloneMeshMap(const MeshMap & mMap_l) const {
  return new LengthLocalFeatureSize(mMap_l);
}

double LengthLocalFeatureSize::operator()(const VertexID p1, const VertexID p2) const {
  return mesh.point(p1).distance(mesh.point(p2)) * (mMap[p1] + mMap[p2]) / 4;
}


LengthOtherDistance::LengthOtherDistance(const MeshManipulator & mManip_l, const Mesh & mesh_l) : LengthEdge(mesh_l), mManip(mManip_l) {

}


LengthEdge * LengthOtherDistance::clone() const {
  return new LengthOtherDistance(mManip, mesh);
}

LengthEdge * LengthOtherDistance::clone(const Mesh & m) const {
  if (m.getNbPoints() == mesh.getNbPoints())
    return new LengthOtherDistance(mManip, m);
  else
    throw Exception("Wrong mesh size.");
}


double LengthOtherDistance::operator()(const VertexID p1, const VertexID p2) const {
  return mesh.point(p1).distance(mesh.point(p2)) / (mManip.getLength(p2) * mManip.getLength(p1) + std::numeric_limits<double>::min());
}


LengthIsocurve::LengthIsocurve(const MeshMap & mMap_l, double value_l, const LengthEdge & lE_l) : LengthEdge(lE_l.getMesh()),
												  mMap(mMap_l), value(value_l), lE(lE_l) {

}

LengthIsocurve::LengthIsocurve(const MeshMap & mMap_l, double value_l,
			       const LengthEdge & lE_l, const Mesh & mesh_l) : LengthEdge(mesh_l), mMap(mMap_l), value(value_l), lE(lE_l) {

}


LengthEdge * LengthIsocurve::clone() const {
  return new LengthIsocurve(mMap, value, lE);
}

LengthEdge * LengthIsocurve::clone(const Mesh & m) const {
  return new LengthIsocurve(mMap, value, lE, m);
}


double LengthIsocurve::operator()(const VertexID p1, const VertexID p2) const {
  return lE(p1, p2) + ((abs(mMap[p2] - value) + abs(mMap[p1] - value)));
}



LengthEdgeNoBoundary::LengthEdgeNoBoundary(const Mesh & m) : LengthEdge(m), maxDistance(m.getMaxDistanceXYZ()) {}

double LengthEdgeNoBoundary::operator()(const VertexID p1, const VertexID p2) const {
  if (mesh.isBoundaryEdge(p1, p2))
    return maxDistance;
  else
    return mesh.point(p1).distance(mesh.point(p2));
}

LengthEdge * LengthEdgeNoBoundary::clone() const {
  return new LengthEdgeNoBoundary(mesh);
}

LengthEdge * LengthEdgeNoBoundary::clone(const Mesh & m) const {
  return new LengthEdgeNoBoundary(m);
}


LengthWeightedEdges::LengthWeightedEdges(const WeightedEdges & we_l) : LengthEdge(we_l.getMesh()), we(we_l) {

}


double LengthWeightedEdges::operator()(const VertexID p1, const VertexID p2) const {
  return we(p1, p2);
}

LengthEdge * LengthWeightedEdges::clone() const {
  return new LengthWeightedEdges(we);
}

LengthEdge * LengthWeightedEdges::clone(const Mesh & m) const {
  if (m.getNbPoints() != we.getMesh().getNbPoints())
    throw Exception("clone(1): unable to create new weights...");
  return new LengthWeightedEdges(we);
}
