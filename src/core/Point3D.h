/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT_3D
#define POINT_3D

#ifndef SWIG
#include <deque>
#endif

#include "Coord3D.h"
#include "Exception.h"
#include "Vertex.h"

namespace Taglut {

  /** cross definition */
  class Triangle;


  /**
     @class Point3DT

     @author Jean-Marie Favreau (LIMOS/ENS Cachan)
     @date 2006-09-19
     @brief Point of R^3 (defined as template)
  */
  template <typename T>
  class Point3DT : public Coord3DT<T>, public Vertex {
  protected:


    /**
       true if point was in surface at begining (false for center of patches for example)
    */
    bool isRealPoint;


  public:

    /**
       Default constructor
    */
    Point3DT() {
      isRealPoint = true;
    }

    /**
       Constructor using 3d location and id value */
    Point3DT(const T & x_l, const T & y_l, const T & z_l, unsigned int id_l = 0, bool isBoundary_l = false) {
      (*this).fill(x_l, y_l, z_l);
      isRealPoint = true;
      (*this).id = id_l;
      (*this).isBoundary = isBoundary_l;
    }

    /**
       Copy constructor
    */
    Point3DT(const Point3DT<T> & p) : Coord3DT<T>(p), Vertex(p) {
      isRealPoint = p.getIsRealPoint();
    }


#ifndef SWIG
    /**
       Set operator
    */
    Point3DT<T>& operator=(const Point3DT<T>& p) {
      (*((Coord3DT<T>*)this)) = p;
      (*((Vertex*)this)) = p;
      isRealPoint = p.getIsRealPoint();
      return *this;
    }
#endif

    /* **** accessors */

    /**
       Return true if current point was inside original surface
    */
    inline bool getIsRealPoint() const { return isRealPoint; }

    /* **** modifiers */

    /**
       Set realsurface point value
    */
    inline void setIsRealPoint(bool irp) { isRealPoint = irp; }


  };

  /**
     Stream operator
  */
  template <typename T, typename S, typename U>
  std::basic_ostream<T, S> & operator<<(std::basic_ostream<T, S> & f, const Point3DT<U> & p) {
    f << "Point(" << p.getX() << ", "
      << p.getY() << ", " << p.getZ() << "; id = " << p.getId() << "; isBoundary: " << p.getIsBoundary() << "; nb neighbours: " << p.getNbNeighbours() << "; flag: " << p.getFlag() << ")";

    return f;
  }


  /**
     Define an alias to Point3DT with double
  */
  typedef Point3DT<double> Point3D;
}

#endif
