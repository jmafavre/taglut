/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2011 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    Université d'Auvergne
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef PATH_3D
#define PATH_3D

#include <vector>
#include "Coord3D.h"

namespace Taglut {

  class PLPath;

  class Path3D : public std::vector<Coord3D> {
  public:
    /** default constructor (empty path) */
    Path3D();

    /** build a path given a list of coords */
    Path3D(const std::vector<Coord3D> & p);

    /** create the path using a PLPath */
    Path3D(const PLPath & p);

    /** copy constructor */
    Path3D(const Path3D & p);

    /** destructor */
    ~Path3D() { }

#ifndef SWIG
    /** copy operator */
    Path3D & operator=(const Path3D & path);
#endif

    /** return true if the current path is a polygon, i.e. if the first point is the last one */
    bool isPolygon() const;

    /** given a lsit of polygons, save it in an OFF file */
    static bool savePolygons(const std::vector<Path3D> & polygons, const std::string & filename, bool onlyPoints = false);

    /** Load polygons from a file */
    static std::vector<Path3D> loadPolygons(const std::string & filename);

    /** return true if the given path is planar */
    bool isPlanar(const double epsilon = 1e-5) const;

    /** convert a list of PLPath ina list of Path3D */
    static std::vector<Path3D> convert(const std::vector<PLPath> & paths);

    /**
       subdivise the current path, with less than size distance between two succesive
       points
     */
    Path3D & subdivise(double s);

    /**
       return a path described by coords, and corresponding to a subdivision
       of the current path, with less than size distance between two succesive
       points
     */
    Path3D getSubdivise(double s) const {
      Path3D result(*this);
      return result.subdivise(s);
    }

    /** subdivision of a set of paths */
    static std::vector<Path3D> subdivise(const std::vector<Path3D> & paths, double size);

    /** return isobarycenter of the current path */
    Coord3D getIsobarycenter() const;

    /** return a planar estimation of the points (using PCA) */
    Plane3D getPlanarEstimation() const;

    /** return the length of the current path */
    double length() const;
  };


}


#endif
