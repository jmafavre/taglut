/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef NLOOP_H
#define NLOOP_H

#include <vector>
#include "IDTypes.h"
#include "Exception.h"
#include "Mesh.h"
#include "PLPath.h"

namespace Taglut {

  /**
     @class NLoop
     Description of an n-loop.
  */
  class NLoop {

  private:

    /** base-point of the n-loop */
    VertexID basePoint;
    /** list of paths from the last point to the base-point  */
    std::vector<std::vector<VertexID> > paths;

  public:

    /** Default constructor with no defined basepoint */
    NLoop();

    /** Default constructor */
    NLoop(VertexID basePoint);


    /** Copy constructor */
    NLoop(const NLoop & nloop);

    /** Destructor */
    ~NLoop();

    /**
       Accessor for the base-point
    */
    inline VertexID getBasePoint() const { return basePoint; }

    /**
       Accessor for the last point
    */
    VertexID getLastPoint() const;

    /**
       Return the number of paths contained in the NLoop
    */
    inline unsigned int getNbPaths() const { return paths.size(); }

    /**
       Access to the paths describing the n-loop
    */
    inline const std::vector<std::vector<VertexID> > & getPaths() const { return paths; }

    /**
       Return the wanted path.
    */
    inline const std::vector<VertexID> & getPath(unsigned int pid) const { return paths[pid]; }

    /**
       Add a path to the current n-loop. Assuming that the last point of \p path
       is the basePoint of the current n-loop, and the first point of \p path is the
       last point of the current n-loop (if exists).
    */
    void addPath(const std::vector<VertexID> & path);


    /**
       Return true if the given edge takes part into the current NLoop
    */
    bool isInsideEdge(VertexID v1, VertexID v2) const;

    /**
       Clear the NLoop removing all paths
    */
    void clear();
  };


  /**
     @class NLoop
     Description of an n-loop described by piecewise-linear paths.
  */
  class PLNLoop {
  private:
    std::vector<PLPath> paths;

  public:
    /** default constructor */
    PLNLoop() { }

    /** add a path to the current structure */
    void addPath(const PLPath & path) {
      if (paths.empty())
	paths.push_back(path);
      else
	if ((paths.front().front() == path.front()) &&
	    (paths.front().back() == path.back()))
	  paths.push_back(path);
	else if ((paths.front().front() == path.back()) &&
		 (paths.front().back() == path.front())) {
	  PLPath pathReverse(path);
	  paths.push_back(pathReverse.reversePath());
	}
	else {
	  throw Exception("addPath(1): wrong extrema points.");
	}
    }

    /** return the number of paths contained in this structure
     */
    inline unsigned int getNbPaths() const {
      return paths.size();
    }

    /**
       Access to the paths describing the n-loop
    */
    inline const std::vector<PLPath> & getPaths() const { return paths; }

    /** convert the current structure to an NLoop */
    NLoop convert() const;
  };


  /**
     @class NLoopEvaluator
     Virtual class for NLoop comparison and evaluation. Each NLoop can be evaluated using a NLoopEvaluator function, that returns a
     scalar value describing the "quality" of the NLoop.
  */
  class NLoopEvaluator {
  public:
    /**
       Default constructor
    */
    NLoopEvaluator()  {}

    virtual ~NLoopEvaluator();

    /**
       Comparison operator. Returns 0 if the two nloops are equal, are a signed value
       according to the ordering.
    */
    inline double operator()(const Mesh & mesh, const NLoop & nloop1, const NLoop & nloop2) const {
      return (*this)(mesh, nloop1) - (*this)(mesh, nloop2);
    }

    /**
       Evaluation of the given NLoop
    */
    virtual double operator()(const Mesh & mesh, const NLoop & nloop) const = 0;

  };

  /**
     @class NLoopEvaluatorMean
     NLoop evaluatioon using the mean of the length of the paths that takes part into the NLoop.
  */
  class NLoopEvaluatorMean : public NLoopEvaluator {
  public:
    /**
       Default constructor
    */
    NLoopEvaluatorMean() {}

    /**
       Evaluation operator.
    */
    inline virtual double operator()(const Mesh & mesh, const NLoop & nloop) const {
      if (nloop.getNbPaths() == 0)
	return 1.;
      else
	return mesh.getLength(nloop) / nloop.getNbPaths();
    }
  };

  /**
     @class NLoopEvaluatorLength
     NLoop evaluation using the sum of the length of the paths that takes part into the NLoop.
  */
  class NLoopEvaluatorLength : public NLoopEvaluator {
  public:
    /**
       Default constructor
    */
    NLoopEvaluatorLength() {}

    /**
       Evaluation operator.
    */
    inline virtual double operator()(const Mesh & mesh, const NLoop & nloop) const {
      return mesh.getLength(nloop);
    }
  };

  /**
     @class NLoopEvaluatorMax
     NLoop evaluation using the maximume value of the length of the paths that takes part into the NLoop.
  */
  class NLoopEvaluatorMax : public NLoopEvaluator {
  private:
  public:
    /**
       Default constructor
    */
    NLoopEvaluatorMax() {}

    /**
       Evaluation operator.
    */
    inline virtual double operator()(const Mesh & mesh, const NLoop & nloop) const {
      double result = -std::numeric_limits<double>::max();

      for(std::vector<std::vector<VertexID> >::const_iterator p = nloop.getPaths().begin(); p != nloop.getPaths().end(); ++p) {
	const double length = mesh.getLength(*p);
	if (result < length)
	  result = length;
      }
      return result;
    }
  };

}

#endif
