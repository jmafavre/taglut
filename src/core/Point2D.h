/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef POINT2D
#define POINT2D

#include "Point3D.h"
#include "Coord2D.h"

namespace Taglut {

  /**
     @class Point2DT

     @author Jean-Marie Favreau (CNRS / Univ. Blaise Pascal)
     @date 2008-11-11
     @brief 2D point
  */
  template <typename T>
  class Point2DT : public Coord2DT<T> {
  private:
    /** id of the current point */
    unsigned int id;

  public:
    /** default constructor */
    inline Point2DT() : Coord2DT<T>(), id(0) { }

    /** constructor using only id */
    inline Point2DT(unsigned int id_t) : Coord2DT<T>(), id(id_t) { }

    /** copy constructor */
    inline Point2DT(const Point2DT<T> & p) : Coord2DT<T>(p), id(p.id) { }

    /** Constructor with 2d location */
    inline Point2DT(const T & x, const T & y) : Coord2DT<T>(x, y), id(0) { }

    /** id accessor */
    inline unsigned int getId() const { return id; }

    /** id modifier */
    inline void setId(unsigned int i) { id = i; }

  };

  /**
     Define an alias to Point2DT with double
  */
  typedef Point2DT<double> Point2D;
}

#endif
