/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <popt.h>
#include <sstream>
#include "CImgUse.h"

#include "Messages.h"

using namespace std;
using namespace Taglut;

static char* filename  = NULL;
static int   sizex = 300;
static int   sizey = 300;
static bool  v3d = false;
static int   help = 0;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename, 0, "Input image", NULL},
  { "3d", 'd', POPT_ARG_NONE, &v3d, 0, "Use 3d display", NULL},
  { NULL, 'x', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizex, 0, "3d window size (x)", NULL},
  { NULL, 'y', POPT_ARG_INT | POPT_ARGFLAG_SHOW_DEFAULT, &sizey, 0, "3d window size (y)", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};




int main (int argc, const char **argv) {
  poptContext context = poptGetContext("maskViewer", argc, argv, options, 0);

  /* check values */

  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Mask viewer", "Display binary mask (2d or 3d view).");
    hMsg << "Input: image to view (available format: dicom)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if ((sizex <= 0) || (sizey <= 0)) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: 3d window size must be positive." << endl;
    return 1;
  }


  try {
    /* load image */
    CImg<> img(filename);


    if (v3d) {
      /* build 3d mesh */
      CImg<> points;
      CImgList<unsigned int> faces;

      points = img.get_isosurface3d(faces, .5, (int)img._width / 2, (int)img._height / 2, (int)img._depth / 2);
      const CImgList<unsigned char> colors(faces._width, CImg<unsigned char>::vector(100, 200, 255));

      CImgDisplay disp3d(sizex, sizey, "3D Visualisation");

      CImg<unsigned char> visu3d(disp3d._width, disp3d._height, 1, 3, 0);

      visu3d.display_object3d(disp3d, points, faces, colors, true, 3, -1, true).display(disp3d);

    }

    else {

      CImgDisplay disp(img._width + img._depth, img._height + img._depth, "Visualisation");
      int X = img._width / 2, Y = img._height / 2, Z = img._depth / 2, area = 0;
      bool write = false;

      while (!disp._is_closed) {
	CImg<unsigned char> visu(disp._width, disp._height, 1, 3, 0);
	unsigned char white[3] = {255, 255, 255};

	visu = img.get_projections2d(X,Y,Z).normalize(0, 255).resize(disp._width,disp._height,1,cimg::min((unsigned int)3, img._spectrum));
	if (write) {
	  std::ostringstream msg;
	  msg << "(" << X << ", " << Y << ", " << Z << ")" << std::endl;
	  if (img._depth > 1)
	    visu.draw_text(img._width + 10, img._height + 10, msg.str().c_str(), white);
	  else
	    visu.draw_text(10, img._height - 10, msg.str().c_str(), white);
	}

	visu.display(disp);

	if (disp._button != 0) {
	  const int mx = disp._mouse_x, my = disp._mouse_y;
	  if ((mx >= 0) && (my >= 0)) {
	    const int mX = mx*(img._width+(img._depth>1?img._depth:0))/disp._width, mY = my*(img._height+(img._depth>1?img._depth:0))/disp._height;
	    if ((unsigned int)mX < img._width && (unsigned int)mY < img._height)   { area = 1; X=mX; Y = mY; }
	    if ((unsigned int)mX < img._width && (unsigned int)mY >= img._height)  { area = 2; X=mX; Z = mY - img._height; }
	    if ((unsigned int)mX >= img._width && (unsigned int)mY < img._height)  { area = 3; Y=mY; Z = mX - img._width;  }
	    if ((unsigned int)mX >= img._width && (unsigned int)mY >= img._height) { X = 0; Y = 0; Z = 0; }
	    if ((unsigned int)mX < img._width && (unsigned int)mY < img._height)   { area = 1; X = mX; Y = mY; }
	  }
	  write = true;
	  if (disp._is_resized) disp.resize();
	}
	else
	  write = false;

      }
    }
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  poptFreeContext(context);
  return 0;
}


