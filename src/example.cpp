/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Mesh.h"
#include "MeshMap.h"
#include "MeshPart.h"
#include "PLY.h"
#include "Point3D.h"

using namespace Taglut;

int main (int, const char **) {

    PLYLoader plyLoader("/home/jm/recherche/data/coeur/MRI/surface-colored.ply");
    Mesh mesh;
    MeshMap mmap(mesh);
    plyLoader.loadMeshFromPLY(mesh);
    plyLoader.loadMeshMapFromPLY(mmap);

    Point3D center(-3.5, -2.72, 25.78);
    MeshPart mpart(mesh, center, 10, true);

    Mesh disc = mpart.buildCropMesh();
    std::cout << disc.getNbPoints() << " vs " << mesh.getNbPoints() << std::endl;
    MeshMap discmap (disc, mpart, mmap);

  return 0;
}
