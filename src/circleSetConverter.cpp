/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>

#include "CImgUse.h"
#include "Mesh.h"
#include "CPMethod.h"
#include "Mapping2D3D.h"
#include "Exception.h"
#include "FileTools.h"
#include "FileTools.h"

#include "Messages.h"

using namespace std;
using namespace Taglut;

static char*  filename_input  = NULL;
static char*  filename_output  = NULL;
static char*  filename_texture  = NULL;
static int    border = 0;
static int    borderTriangles = 0;
static int    xsize = 100;
static int    ysize = 100;
static int    noCirclePacking = 0;
static double epsilon = 0.000000000001;
static int    maxIter = 40000;
static int    mesh3D = 0;
static int    help = 0;


struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input cst file", NULL},
  { "output", 'o', POPT_ARG_STRING, &filename_output, 0, "Output image or mesh file.", NULL},
  { "maptexture", 'm', POPT_ARG_STRING, &filename_texture, 0, "Input image (map texture)", NULL},
  { "border", 'b', POPT_ARG_NONE, &border, 0, "Draw border.", NULL},
  { "triangle-border", 't', POPT_ARG_NONE, &borderTriangles, 0, "Draw border for each triangle.", NULL},
  { "xsize", 'x', POPT_ARG_INT, &xsize, 0, "Output size (x size).", NULL},
  { "ysize", 'y', POPT_ARG_INT, &ysize, 0, "Output size (y size).", NULL},
  { "no-circle-packing", 'n', POPT_ARG_NONE, &noCirclePacking, 0, "Not use circle packing algorithm after loading to correct round error...", NULL},
  { "epsilon", 'e', POPT_ARG_DOUBLE, &epsilon, 0, "Epsilon (used in circle packing algorithm)", NULL},
  { "maxIter", 'm', POPT_ARG_INT, &maxIter, 0, "Maximum iteration (used in circle packing algorithm)", NULL},
  { "3d", '3', POPT_ARG_NONE, &mesh3D, 0, "Generated mesh is a 3d mesh.", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("circleSetConverter", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }

  if (help != 0) {
    HelpMessage hMsg("Circle set converter", "Convert a circle set to image or mesh file.");
    hMsg << "Input: circle set to convert (available format: cset). Optional: texture image.";
    hMsg << "Output: resulting mesh or image (available format: VRML, image)";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }


  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }

  if (filename_output == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an output file." << endl;
    return 1;
  }

  try {
    /* load image */
    cout << "Loading circle set..." << endl;
    Mesh mesh;
    Mapping2D3D mapping(mesh);
    CPMethod cset(mapping);

    try {
      cset.load(filename_input);
    } catch (Exception e) {
      cerr << e << endl;
      return -1;
    }


    if ((noCirclePacking == 0) && (mesh3D == 0)) {
      cout << "Circle packing: unfolding..." << endl;
      /* unfold using circle packing */
      int nbIter = cset.processUnfolding(epsilon, maxIter, false);
      cout << " Iteration number: " << nbIter << endl;
    }

    CImg<unsigned char> result(xsize, ysize);
    result.fill(std::numeric_limits<unsigned char>::max());

    // if output file is a mesh
    if (mesh.isSaveFormat(filename_output)) {
      cout << "Saving VRML..." << endl;
      CImgList<VertexID> primitives = mapping.getCImgPrimitives();
      if (mesh3D == 0) {
	Mesh resultMesh(mapping);

	resultMesh.save(filename_output);
      }
      else {
	Mesh resultMesh(mapping, false);

	resultMesh.save(filename_output);
      }
    }
    else {
      // else output file should be an image
      if (filename_texture == NULL) {
	cout << "Conversion to image..." << endl;
	mapping.toImageRandom(result);
      }
      else {
	cout << "Loading texture image..." << endl;
	CImg<unsigned char> src(filename_texture);

	cout << "Grayscale texture image conversion..." << endl;
	CImg<unsigned char> image(src._height, src._width);
	cimg_forXY(src, x, y)
	  image(x, y) = src(x, y);

	cout << "Conversion to image..." << endl;
	mapping.toImage(result, image);
      }

      if (border != 0)
	mapping.toImageDrawBorder(result, (unsigned char)0);

      if (borderTriangles != 0)
	mapping.toImageDrawBorderTriangles(result, (unsigned char)0);


      /* save result image */
      cout << "Saving image..." << endl;
      result.save(filename_output);
    }
  }
  catch(Exception e) {
    cerr << e << "Abort." << endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }

  return 0;
}
