/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <popt.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "Point3D.h"
#include "PointCloud.h"

#include <unistd.h>
#include "Messages.h"
#include "CImgUse.h"
#include "Display3D.h"
#include "DisplayGL.h"

using namespace Taglut;

static char*  filename_input = NULL;
static int    help           = 0;
static double sphere_size    = -1;

struct poptOption options[] = {
  { "input", 'i', POPT_ARG_STRING, &filename_input, 0, "Input VRML file", NULL},
  { "sphere-size", 's', POPT_ARG_DOUBLE, &sphere_size, 0, "Sphere size. Default: automatic.", NULL},
  { "help", '?', POPT_ARG_NONE, &help, 0, "Show this help message", NULL},
  POPT_TABLEEND
};

using namespace std;


int main (int argc, const char **argv) {
  poptContext context = poptGetContext("Point cloud viewer", argc, argv, options, 0);

  /* parse values */
  if (poptGetNextOpt(context) != -1) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Invalid argument." << endl;
    return 1;
  }


  if (help != 0) {
    HelpMessage hMsg("pointCloudViewer", "Point cloud viewer.");
    hMsg << "Input: point cloud to use. Format: \"x1 y1 z1 ... xn yn zn\". Separators can be space, tabulation, new line. Lines starting with \"#\" are ignored. 3D mesh format can also be used (PLY, VRML, etc.).";
    cout << hMsg << endl;

    poptPrintHelp(context, stdout, 0);
    return 0;
  }

  if (filename_input == NULL) {
    poptPrintUsage(context, stderr, 0);
    cerr << "Error: Specify an input file." << endl;
    return 1;
  }


  try {
    cout << "Loading point cloud..." << std::endl;
    PointCloud cloud(filename_input);
    if (cloud.size() == 0) {
      std::cout << "The cloud point is empty. Abort." << std::endl;
      return 1;
    }
    cout << " " << cloud.size() << " points has been loaded";
    if (cloud.hasNormals())
      cout << " (with normals)";
    cout << std::endl;


    std::cout << "Visualization" << std::endl;
    std::ostringstream title;
    title << "3D visualization (" << filename_input << ")";
    DisplayPointCLoudGL d(512, 512, title.str(), cloud, sphere_size, true);
    d.disp();

  }
  catch(Exception e) {
    cerr << "Error: " << e << std::endl;
    return -1;
  }
  catch (CImgException e) {
    cerr << e.what() << endl;
    return 3;
  }


  return 0;
}
