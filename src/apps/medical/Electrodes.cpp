/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Electrodes.h"
#include <iostream>
#include<fstream>
#include "FileExceptions.h"

using namespace Taglut;

ElectrodeList::ElectrodeList() {
  filename = "";
}

ElectrodeList::ElectrodeList(const std::string & filename_t, double zratio) : filename(filename_t) {
  std::ifstream infile(filename_t.c_str());
  Point3D point;
  bool iof = false;

  if (!infile.is_open())
    throw ExceptionFileNotFound();

  while(!iof) {
    double x, y, z;
    std::string s;


    while (s != "[E:") {
      infile >> s;
      if (infile.eof()) {
	iof = true;
	break; // last point
      }
    }

    if (!iof) {
      while (s != "x:")
	infile >> s;
      infile >> x;
      if (infile.eof())
	throw ExceptionErrorDuringReadingFile();
      while (s != "y:")
	infile >> s;
      infile >> y;
      if (infile.eof())
	throw ExceptionErrorDuringReadingFile();
      while (s != "z:")
	infile >> s;
      infile >> z;
      if (infile.eof())
	throw ExceptionErrorDuringReadingFile();
      point(0) = x;
      point(1) = y;
      point(2) = z * zratio;
      push_back(point);
    }

  }
  infile.close();
}
