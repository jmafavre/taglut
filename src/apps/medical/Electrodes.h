/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef ELECTRODES
#define ELECTRODES

#include "Point3D.h"

namespace Taglut {
  /**
     @class ElectrodeList

     @author Jean-Marie Favreau
     @brief List of Electrode
  */
  class ElectrodeList : public std::deque<Point3D> {
  private:
    std::string filename;

  public:

    /**
       Default constructor
    */
    ElectrodeList();

    /**
       Constructor using a file to load data
    */
    ElectrodeList(const std::string & filename, double zratio = 1.0);

  };
}

#endif
