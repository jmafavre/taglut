==== TAGLUT : Topological And Geometrical Library - a Useful Toolkit ====


=== Licence ===
Taglut est distribué sous la licence GPL v2.
Voir LICENSE.txt et src/include/opennl/doc/OpenNL_License.txt

=== Auteur ===
L'auteur principal de Taglut est Jean-Marie Favreau (http://jmfavreau.info).
Taglut est un outil dont le développement a commencé en 2006 pour répondre aux
besoins de sa thèse.

=== Comment compiler Taglut ===
Il est nécessaire d'avoir installé différents outils :
- le compilateur g++
- une version de la librairie CImg récente (la version 1.3.2 fonctionnera, ou
  la version courante)
- les librairies libpng-dev, libjpeg-dev
- doxygen pour la génération de la documentation des classes
- cmake pour la compilation

D'autres outils sont optionnels :
- libx11-dev pour l'affichage graphique
- libpopt-dev pour les binaires
- glut pour l'affichage OpenGL
- libxml2 pour l'interpréteur de script xml
- swig et python-dev pour le binding python
- openmp pour une version parallélisée de Circle Packing

Comment procéder :
- Lancer "ccmake ." et configurer éventuellement les paramètres nécessaires
- Lancer "make" qui compile les librairies et binaires du projet
- Éventuellement utiliser "make doc" qui construit la documentation doxygen
- Lancer "make install" pour installer les libraires et binaires.
Pour un usage local, ajouter à LD_LIBRARY_PATH le répertoire lib/.

=== Note ===
OpenNL est une librairie pour construire et résoudre facilement des systèmes linéaires
C'était initialement une partie de blender (http://www.blender.org).
Une grande partie du code source de l'implémentation d'ABF (src/unfolding/abf/)
faisait aussi partie de blender.
