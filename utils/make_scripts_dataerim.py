# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


# Taglut : Topological And Geometrical Library - a Useful Toolkit
# Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
#                    CNRS / Univ. Blaise Pascal
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


/* Taglut : Topological And Geometrical Library - a Useful Toolkit
 * Copyright (C) 2008 Jean-Marie Favreau <jean-marie.favreau@ens-cachan.org>
 *                    CNRS / Univ. Blaise Pascal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#!/usr/bin/python

import re, sys, os

if len(sys.argv) != 9:
    print "Usage: "
    print "   make_scripts_dataerim.py output.xml electrodes.txt mapcenter.txt mask.fsf mri.fsf bone_length.fsf zratio output.wrl"
    sys.exit("Parameter(s) missing")


# compute center of electrodes
x = 0
y = 0
z = 0
if (os.path.exists(sys.argv[3])):
    f = open(sys.argv[3], 'r')
    line = f.readline()
    x = float(line.split(' ')[1]);
    y = float(line.split(' ')[3]);
    z = float(line.split(' ')[5]);
else:
    i = 0
    f = open(sys.argv[2], 'r')
    for line in f.readlines():
        if (i < 8):
            x += float(line.split(' ')[6]);
            y += float(line.split(' ')[8]);
            z += float(line.split(' ')[10]);
            i += 1
            x /= 8
            y /= 8
            z /= 8

f.close()

# write results
f = open(sys.argv[1], 'w')

f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
f.write("<script>\n")
f.write("  <exec cache=\"" + sys.argv[4] + ".cst\">\n")
f.write("    <exec cache=\"" + sys.argv[4] + ".cut.wrl\">\n")
f.write("      <exec cache=\"" + sys.argv[4] + ".patch.wrl\">\n")
f.write("        <exec cache=\"" + sys.argv[4] + ".crop.wrl\">\n")
f.write("          <exec cache=\"" + sys.argv[4] + ".wrl\">\n")
f.write("            <input filename=\"" + sys.argv[4] + "\" zratio=\"" + sys.argv[7] + "\" />\n")
f.write("          </exec>\n")
f.write("          <crop method=\"ball2d\" radius=\"80\" pointx=\"" + str(x) +"\" pointy=\"" + str(y) +"\" pointz=\"" + str(z) +"\" />\n")
f.write("          <clean />\n")
f.write("          <select method=\"size\" />\n")
f.write("        </exec>\n")
f.write("        <output method=\"infos\"/>\n")
f.write("        <patch length-method=\"linear-curvature\" ratio-length=\"0.8\" max-length=\"15\"  />\n")
f.write("      </exec>\n")
f.write("      <output method=\"infos\" />\n")
f.write("      <cut length-method=\"linear-curvature\" ratio-length=\"0.8\" />\n")
f.write("    </exec>\n")
f.write("    <!-- output method=\"display\" options=\"border\" / -->\n")
f.write("    <unfold method=\"circle-packing\" epsilon=\"0.000000000001\" max-iteration=\"40000\" />\n")
f.write("  </exec>\n")
f.write("  <output method=\"display\" options=\"dynamic-med\" filename=\"" + sys.argv[5] +"\" texture=\""+ sys.argv[6] +"\" zratio=\"" + sys.argv[7] + "\" />\n")
f.write("  <output method=\"display\" options=\"dynamic-med\" filename=\"" + sys.argv[5] +"\" input-data=\""+ sys.argv[2] +"\" zratio=\"" + sys.argv[7] + "\" />\n")
f.write("  <output method=\"file\" filename=\"" + sys.argv[8] + "\" />\n")
f.write("  <output method=\"file\" options=\"texture\" filename=\"" + sys.argv[8] + ".jpg\" texture=\""+ sys.argv[6] +"\" zratio=\"" + sys.argv[7] + "\" />\n")
f.write("  <output method=\"file\" options=\"distance-electrodes\" filename=\"" + sys.argv[8] + ".jpg\" input-data=\""+ sys.argv[2] +"\" zratio=\"" + sys.argv[7] + "\" />\n")
f.write("</script>\n")

f.close()



