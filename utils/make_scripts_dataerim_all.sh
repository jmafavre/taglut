#!/bin/bash

PATH_FILES=/home/jm/data/erim
PATH_SCRIPTS=../scripts/erim

for i in amouroux auteroche dognin ray sol vialatte arnald bouron godfroy simon tagnon villeneuve; do
    zratio=`cat $PATH_FILES/${i}.fsf|head -n 6|tail -n 1|sed "s/\r//"`
    ./make_scripts_dataerim.py $PATH_SCRIPTS/unfold_brain_$i.xml $PATH_FILES/${i}_electrodes.txt $PATH_FILES/${i}_mapcenter.txt \
	$PATH_FILES/${i}_csMask.fsf $PATH_FILES/${i}.fsf $PATH_FILES/${i}_boneLengthMap.fsf $zratio $PATH_FILES/$i.wrl

done